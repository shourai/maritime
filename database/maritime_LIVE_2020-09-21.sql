# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.2.10-MariaDB)
# Database: maritime
# Generation Time: 2020-09-21 02:03:35 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table dtb_authority_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dtb_authority_role`;

CREATE TABLE `dtb_authority_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `authority_id` smallint(5) unsigned DEFAULT NULL,
  `creator_id` int(10) unsigned DEFAULT NULL,
  `deny_url` varchar(4000) NOT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_4A1F70B181EC865B` (`authority_id`),
  KEY `IDX_4A1F70B161220EA6` (`creator_id`),
  CONSTRAINT `FK_4A1F70B161220EA6` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`id`),
  CONSTRAINT `FK_4A1F70B181EC865B` FOREIGN KEY (`authority_id`) REFERENCES `mtb_authority` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table dtb_base_info
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dtb_base_info`;

CREATE TABLE `dtb_base_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `country_id` smallint(5) unsigned DEFAULT NULL,
  `pref_id` smallint(5) unsigned DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `company_kana` varchar(255) DEFAULT NULL,
  `postal_code` varchar(8) DEFAULT NULL,
  `addr01` varchar(255) DEFAULT NULL,
  `addr02` varchar(255) DEFAULT NULL,
  `phone_number` varchar(14) DEFAULT NULL,
  `business_hour` varchar(255) DEFAULT NULL,
  `email01` varchar(255) DEFAULT NULL,
  `email02` varchar(255) DEFAULT NULL,
  `email03` varchar(255) DEFAULT NULL,
  `email04` varchar(255) DEFAULT NULL,
  `shop_name` varchar(255) DEFAULT NULL,
  `shop_kana` varchar(255) DEFAULT NULL,
  `shop_name_eng` varchar(255) DEFAULT NULL,
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `good_traded` varchar(4000) DEFAULT NULL,
  `message` varchar(4000) DEFAULT NULL,
  `delivery_free_amount` decimal(12,2) unsigned DEFAULT NULL,
  `delivery_free_quantity` int(10) unsigned DEFAULT NULL,
  `option_mypage_order_status_display` tinyint(1) NOT NULL DEFAULT 1,
  `option_nostock_hidden` tinyint(1) NOT NULL DEFAULT 0,
  `option_favorite_product` tinyint(1) NOT NULL DEFAULT 1,
  `option_product_delivery_fee` tinyint(1) NOT NULL DEFAULT 0,
  `option_product_tax_rule` tinyint(1) NOT NULL DEFAULT 0,
  `option_customer_activate` tinyint(1) NOT NULL DEFAULT 1,
  `option_remember_me` tinyint(1) NOT NULL DEFAULT 1,
  `authentication_key` varchar(255) DEFAULT NULL,
  `php_path` varchar(255) DEFAULT NULL,
  `option_point` tinyint(1) NOT NULL DEFAULT 1,
  `basic_point_rate` decimal(10,0) unsigned DEFAULT 1,
  `point_conversion_rate` decimal(10,0) unsigned DEFAULT 1,
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1D3655F4F92F3E70` (`country_id`),
  KEY `IDX_1D3655F4E171EF5F` (`pref_id`),
  CONSTRAINT `FK_1D3655F4E171EF5F` FOREIGN KEY (`pref_id`) REFERENCES `mtb_pref` (`id`),
  CONSTRAINT `FK_1D3655F4F92F3E70` FOREIGN KEY (`country_id`) REFERENCES `mtb_country` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dtb_base_info` WRITE;
/*!40000 ALTER TABLE `dtb_base_info` DISABLE KEYS */;

INSERT INTO `dtb_base_info` (`id`, `country_id`, `pref_id`, `company_name`, `company_kana`, `postal_code`, `addr01`, `addr02`, `phone_number`, `business_hour`, `email01`, `email02`, `email03`, `email04`, `shop_name`, `shop_kana`, `shop_name_eng`, `update_date`, `good_traded`, `message`, `delivery_free_amount`, `delivery_free_quantity`, `option_mypage_order_status_display`, `option_nostock_hidden`, `option_favorite_product`, `option_product_delivery_fee`, `option_product_tax_rule`, `option_customer_activate`, `option_remember_me`, `authentication_key`, `php_path`, `option_point`, `basic_point_rate`, `point_conversion_rate`, `discriminator_type`)
VALUES
	(1,NULL,28,'株式会社メディファイン','メディファイン','6500013','神戸市中央区花隈町','5-21-1110号','0787777000','10:00～19:00（水曜定休）','info@maritime-cbd.com','info@maritime-cbd.com','info@maritime-cbd.com','info@maritime-cbd.com','MARITIME','マリタイム','MARITIME','2020-08-06 12:45:44','admin.good_traded_txt','admin.shop_message_txt',NULL,NULL,1,0,1,0,0,1,1,'5b5acb396ec335b1e72cbb94033e406721949a47',NULL,1,1,1,'baseinfo');

/*!40000 ALTER TABLE `dtb_base_info` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dtb_block
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dtb_block`;

CREATE TABLE `dtb_block` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `device_type_id` smallint(5) unsigned DEFAULT NULL,
  `block_name` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) NOT NULL,
  `use_controller` tinyint(1) NOT NULL DEFAULT 0,
  `deletable` tinyint(1) NOT NULL DEFAULT 1,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `device_type_id` (`device_type_id`,`file_name`),
  KEY `IDX_6B54DCBD4FFA550E` (`device_type_id`),
  CONSTRAINT `FK_6B54DCBD4FFA550E` FOREIGN KEY (`device_type_id`) REFERENCES `mtb_device_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dtb_block` WRITE;
/*!40000 ALTER TABLE `dtb_block` DISABLE KEYS */;

INSERT INTO `dtb_block` (`id`, `device_type_id`, `block_name`, `file_name`, `use_controller`, `deletable`, `create_date`, `update_date`, `discriminator_type`)
VALUES
	(1,10,'カート','cart',0,0,'2017-03-07 10:14:52','2017-03-07 10:14:52','block'),
	(2,10,'カテゴリ','category',0,0,'2017-03-07 10:14:52','2017-03-07 10:14:52','block'),
	(3,10,'カテゴリナビ(PC)','category_nav_pc',0,0,'2017-03-07 10:14:52','2017-03-07 10:14:52','block'),
	(4,10,'カテゴリナビ(SP)','category_nav_sp',0,0,'2017-03-07 10:14:52','2017-03-07 10:14:52','block'),
	(5,10,'新入荷商品特集','eyecatch',0,0,'2017-03-07 10:14:52','2017-03-07 10:14:52','block'),
	(6,10,'フッター','footer',0,0,'2017-03-07 10:14:52','2017-03-07 10:14:52','block'),
	(7,10,'ヘッダー(トップページ）','header',0,0,'2017-03-07 10:14:52','2020-04-29 00:54:14','block'),
	(8,10,'ログインナビ(共通)','login',0,0,'2017-03-07 10:14:52','2017-03-07 10:14:52','block'),
	(9,10,'ログインナビ(SP)','login_sp',0,0,'2017-03-07 10:14:52','2017-03-07 10:14:52','block'),
	(10,10,'ロゴ','logo',0,0,'2017-03-07 10:14:52','2017-03-07 10:14:52','block'),
	(11,10,'新着商品','new_item',0,0,'2017-03-07 10:14:52','2017-03-07 10:14:52','block'),
	(12,10,'新着情報(original)','news',0,0,'2017-03-07 10:14:52','2020-05-11 09:46:27','block'),
	(13,10,'商品検索','search_product',1,0,'2017-03-07 10:14:52','2017-03-07 10:14:52','block'),
	(14,10,'トピック','topic',0,0,'2017-03-07 10:14:52','2017-03-07 10:14:52','block'),
	(15,10,'CBD Oils','product_slide',0,1,'2020-04-26 04:57:04','2020-04-29 00:31:11','block'),
	(16,10,'CBD Water','water_product',0,1,'2020-04-26 04:58:38','2020-04-26 04:58:38','block'),
	(17,10,'How it works','how_it_works',0,1,'2020-04-26 05:00:39','2020-04-26 05:00:39','block'),
	(18,10,'Join newsletter','join_newsletter',0,1,'2020-04-26 05:01:45','2020-04-29 00:27:57','block'),
	(19,10,'Maritime goal','owner_message',0,1,'2020-04-26 06:32:38','2020-04-26 06:32:38','block'),
	(20,10,'Usage recommendation','usage_recommendation',0,1,'2020-04-26 06:34:03','2020-04-26 06:35:48','block'),
	(21,10,'Introduction','introduction',0,1,'2020-04-26 06:36:52','2020-04-26 06:36:52','block'),
	(22,10,'ヘッダー(商品検索・ログインナビ・カート)','header_default',0,1,'2020-04-29 00:56:03','2020-04-29 00:56:03','block'),
	(23,10,'新着情報一覧','news_list',0,1,'2020-05-11 09:47:22','2020-05-11 10:09:02','block'),
	(24,10,'ブログ一覧','blog_list',0,1,'2020-05-11 10:09:54','2020-05-11 10:09:54','block'),
	(25,10,'カテゴリー商品一覧','category_products',0,1,'2020-05-13 09:04:25','2020-05-13 09:04:25','block'),
	(26,10,'PayPalロゴ','paypal_logo',0,1,'2020-05-24 08:43:48','2020-05-24 08:43:48','block'),
	(28,10,'メールマガジン(Popup)','newsletter',0,1,'2020-08-21 00:28:41','2020-08-21 00:28:41','block'),
	(29,10,'パンくずプラグイン','BreadcrumbList4',1,1,'2020-09-04 04:47:03','2020-09-04 04:47:03','block'),
	(30,10,'Video Introduction','video_intro',0,1,'2020-09-10 03:25:00','2020-09-10 03:25:00','block');

/*!40000 ALTER TABLE `dtb_block` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dtb_block_position
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dtb_block_position`;

CREATE TABLE `dtb_block_position` (
  `section` int(10) unsigned NOT NULL,
  `block_id` int(10) unsigned NOT NULL,
  `layout_id` int(10) unsigned NOT NULL,
  `block_row` int(10) unsigned DEFAULT NULL,
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`section`,`block_id`,`layout_id`),
  KEY `IDX_35DCD731E9ED820C` (`block_id`),
  KEY `IDX_35DCD7318C22AA1A` (`layout_id`),
  CONSTRAINT `FK_35DCD7318C22AA1A` FOREIGN KEY (`layout_id`) REFERENCES `dtb_layout` (`id`),
  CONSTRAINT `FK_35DCD731E9ED820C` FOREIGN KEY (`block_id`) REFERENCES `dtb_block` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dtb_block_position` WRITE;
/*!40000 ALTER TABLE `dtb_block_position` DISABLE KEYS */;

INSERT INTO `dtb_block_position` (`section`, `block_id`, `layout_id`, `block_row`, `discriminator_type`)
VALUES
	(3,3,0,2,'blockposition'),
	(3,7,0,0,'blockposition'),
	(3,7,1,0,'blockposition'),
	(3,7,2,0,'blockposition'),
	(3,7,3,0,'blockposition'),
	(3,7,4,0,'blockposition'),
	(3,7,5,0,'blockposition'),
	(3,10,0,1,'blockposition'),
	(4,2,3,1,'blockposition'),
	(4,29,3,0,'blockposition'),
	(6,29,2,0,'blockposition'),
	(6,29,4,0,'blockposition'),
	(6,29,5,0,'blockposition'),
	(7,15,0,2,'blockposition'),
	(7,15,1,3,'blockposition'),
	(7,16,0,3,'blockposition'),
	(7,16,1,4,'blockposition'),
	(7,17,0,4,'blockposition'),
	(7,17,1,5,'blockposition'),
	(7,17,4,0,'blockposition'),
	(7,18,0,6,'blockposition'),
	(7,19,0,5,'blockposition'),
	(7,19,1,6,'blockposition'),
	(7,20,0,1,'blockposition'),
	(7,20,1,2,'blockposition'),
	(7,21,0,0,'blockposition'),
	(7,21,1,0,'blockposition'),
	(7,23,1,8,'blockposition'),
	(7,24,1,7,'blockposition'),
	(7,25,4,1,'blockposition'),
	(7,30,1,1,'blockposition'),
	(9,18,1,0,'blockposition'),
	(9,18,2,0,'blockposition'),
	(9,18,3,0,'blockposition'),
	(9,18,4,0,'blockposition'),
	(9,18,5,0,'blockposition'),
	(10,6,0,0,'blockposition'),
	(10,6,1,0,'blockposition'),
	(10,6,2,0,'blockposition'),
	(10,6,3,0,'blockposition'),
	(10,6,4,0,'blockposition'),
	(10,6,5,0,'blockposition'),
	(11,4,0,1,'blockposition'),
	(11,4,1,1,'blockposition'),
	(11,4,2,1,'blockposition'),
	(11,4,3,1,'blockposition'),
	(11,4,4,1,'blockposition'),
	(11,4,5,1,'blockposition'),
	(11,9,0,2,'blockposition'),
	(11,9,1,2,'blockposition'),
	(11,9,2,2,'blockposition'),
	(11,9,3,2,'blockposition'),
	(11,9,4,2,'blockposition'),
	(11,9,5,2,'blockposition'),
	(11,13,0,0,'blockposition'),
	(11,13,1,0,'blockposition'),
	(11,13,2,0,'blockposition'),
	(11,13,3,0,'blockposition'),
	(11,13,4,0,'blockposition'),
	(11,13,5,0,'blockposition'),
	(11,28,1,3,'blockposition');

/*!40000 ALTER TABLE `dtb_block_position` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dtb_cart
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dtb_cart`;

CREATE TABLE `dtb_cart` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `cart_key` varchar(255) DEFAULT NULL,
  `pre_order_id` varchar(255) DEFAULT NULL,
  `total_price` decimal(12,2) unsigned NOT NULL DEFAULT 0.00,
  `delivery_fee_total` decimal(12,2) unsigned NOT NULL DEFAULT 0.00,
  `sort_no` smallint(5) unsigned DEFAULT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `add_point` decimal(12,0) unsigned NOT NULL DEFAULT 0,
  `use_point` decimal(12,0) unsigned NOT NULL DEFAULT 0,
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dtb_cart_pre_order_id_idx` (`pre_order_id`),
  KEY `IDX_FC3C24F09395C3F3` (`customer_id`),
  KEY `dtb_cart_update_date_idx` (`update_date`),
  CONSTRAINT `FK_FC3C24F09395C3F3` FOREIGN KEY (`customer_id`) REFERENCES `dtb_customer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dtb_cart` WRITE;
/*!40000 ALTER TABLE `dtb_cart` DISABLE KEYS */;

INSERT INTO `dtb_cart` (`id`, `customer_id`, `cart_key`, `pre_order_id`, `total_price`, `delivery_fee_total`, `sort_no`, `create_date`, `update_date`, `add_point`, `use_point`, `discriminator_type`)
VALUES
	(1,NULL,'BNDL09REKT9dnmD2ICCqwdpMcxxzs3Vw_1',NULL,10799.00,0.00,NULL,'2020-04-29 04:33:02','2020-04-29 04:33:02',0,0,'cart'),
	(2,NULL,'tGGqFkJCshTNzcg3FDp3ezFUINHtPJgQ_1',NULL,5400.00,0.00,NULL,'2020-05-01 08:24:56','2020-05-01 08:24:56',0,0,'cart'),
	(4,NULL,'x4GgGkr6ymKwAuGz439LZhmSNytgmspn_1',NULL,10799.00,0.00,NULL,'2020-05-01 10:20:24','2020-05-01 10:21:18',0,0,'cart'),
	(6,NULL,'2vVF5ONb4jLrSNUyIkkF5ecq54cMyPgK_1',NULL,10799.00,0.00,NULL,'2020-05-08 05:10:14','2020-05-08 05:10:14',0,0,'cart'),
	(28,NULL,'SDBzDucGfcjzPIW1Mp8gNVZkm2xwYWzN_1',NULL,21998.00,0.00,NULL,'2020-05-10 09:43:31','2020-05-10 10:06:39',0,0,'cart'),
	(31,NULL,'wyxYOWI3T1jho6DdplijbAZ7LnwOvNYa_2',NULL,8800.00,0.00,NULL,'2020-05-15 00:54:22','2020-05-15 00:54:25',0,0,'cart'),
	(41,NULL,'4gtzeOui0L1Ienc4JZTw8YoEYQBGdYJg_2',NULL,8800.00,0.00,NULL,'2020-05-15 01:05:12','2020-05-15 01:05:36',0,0,'cart'),
	(44,NULL,'adBzIWxfkUHnm1DM3vKhqPCvV7nJCbgC_2',NULL,8800.00,0.00,NULL,'2020-05-15 05:48:13','2020-05-15 05:48:17',0,0,'cart'),
	(50,NULL,'bHo8kOfR5Xqi4JmhyqmlVBKQ1k5tt14l_1',NULL,15400.00,0.00,NULL,'2020-05-15 09:48:30','2020-05-15 09:48:34',0,0,'cart'),
	(54,NULL,'LgT4DaKKMRlMfSyl4Oyaw9Nm0iNYjP6j_2',NULL,8800.00,0.00,NULL,'2020-05-17 02:09:32','2020-05-17 02:11:24',0,0,'cart'),
	(55,NULL,'LgT4DaKKMRlMfSyl4Oyaw9Nm0iNYjP6j_1','3370ac78203036923bc6c36f05993a65e6df5812',13200.00,0.00,NULL,'2020-05-17 02:09:32','2020-05-17 02:11:24',0,0,'cart'),
	(57,NULL,'U9mBndmge9qXP8rraAgElJyOevt2YwoW_1',NULL,2200.00,0.00,NULL,'2020-05-22 03:39:03','2020-05-22 03:44:05',0,0,'cart'),
	(58,NULL,'U9mBndmge9qXP8rraAgElJyOevt2YwoW_2',NULL,8800.00,0.00,NULL,'2020-05-22 03:39:03','2020-05-22 03:44:05',0,0,'cart'),
	(74,NULL,'BqJ9cpszKky2qkDLu8wtX8HduHvTostT_1',NULL,1276.00,0.00,NULL,'2020-05-28 23:49:43','2020-05-28 23:49:46',0,0,'cart'),
	(79,NULL,'ucWWWnOgLQYvA2jJFK6bUpNeUEWRF9wQ_1',NULL,7518.00,0.00,NULL,'2020-06-03 06:07:01','2020-06-03 06:11:16',0,0,'cart'),
	(164,NULL,'kjcoRfiPMeeqWj30tXRGkYsgHwbXbzTV_1',NULL,1253.00,0.00,NULL,'2020-06-05 09:24:09','2020-06-05 09:24:56',0,0,'cart'),
	(167,NULL,'ybwyxaAZwOfxu6voOt5kgBeoJb7egFgq_1',NULL,5184.00,0.00,NULL,'2020-06-17 08:21:16','2020-06-17 08:23:28',0,0,'cart'),
	(175,NULL,'V0nymyHknnVOvRjAoE0EW219H4s0yUma_2',NULL,24300.00,0.00,NULL,'2020-06-19 03:26:04','2020-06-19 03:26:08',0,0,'cart'),
	(181,NULL,'a3ZFDoOrMtcyrGaQpjojW3WQt50Ol4RH_1',NULL,51300.00,0.00,NULL,'2020-06-26 01:47:24','2020-06-26 01:47:27',0,0,'cart'),
	(186,NULL,'Q1PXWDzwOe450s78ACfNcsdTYp022TVE_1',NULL,51300.00,0.00,NULL,'2020-06-30 10:57:47','2020-06-30 10:58:13',0,0,'cart'),
	(188,NULL,'VkoPOks3eOkTt7NeyaroGvN1UAYPF08a_1',NULL,5184.00,0.00,NULL,'2020-06-30 11:50:57','2020-07-01 07:28:33',0,0,'cart'),
	(190,NULL,'jErlZaelnqZA3IY4mo8axafKiZiasQQ6_1',NULL,5184.00,0.00,NULL,'2020-07-01 19:03:15','2020-07-01 19:03:17',0,0,'cart'),
	(194,9,'9_1','5c067121a611687840ef8b21dfa29377b9b067d8',27760.00,0.00,NULL,'2020-07-02 03:45:08','2020-07-02 03:45:32',0,0,'cart'),
	(195,NULL,'FogpbdMKZ3Ha53EP3Mb8uLwzHYEkcLDX_1',NULL,9828.00,0.00,NULL,'2020-07-02 06:30:58','2020-07-02 06:31:47',0,0,'cart'),
	(197,NULL,'bThZDVeHc1ThV9bogsCXTC4dmwTgFGxx_1',NULL,19656.00,0.00,NULL,'2020-07-03 08:38:31','2020-07-03 08:38:33',0,0,'cart'),
	(208,NULL,'c8fHZ2aiaYF5ifcdwMyYeibF0z964csm_1',NULL,1253.00,0.00,NULL,'2020-07-21 01:29:06','2020-07-21 01:29:09',0,0,'cart'),
	(217,NULL,'57DwZHTCHdq2PFOdNIEcYPzepb4bmj6D_1',NULL,1253.00,0.00,NULL,'2020-07-23 08:22:05','2020-07-23 08:22:11',0,0,'cart'),
	(218,7,'7_1',NULL,5640.00,0.00,NULL,'2020-07-23 16:06:20','2020-07-23 16:06:20',0,0,'cart'),
	(229,NULL,'ZtkhCYFSai3orDSSAZU86KxhUtXKsNbD_1',NULL,1253.00,0.00,NULL,'2020-07-26 20:10:39','2020-07-26 20:10:45',0,0,'cart'),
	(230,NULL,'XMTHQhnJ2HlluSMz31kO2zL5G7EFxaSq_1',NULL,5184.00,0.00,NULL,'2020-07-30 07:48:18','2020-07-30 07:48:20',0,0,'cart'),
	(231,NULL,'035cPtyJgciiIP9ZYwpdtI3WUAEx8NN0_1',NULL,9828.00,0.00,NULL,'2020-07-30 15:30:01','2020-07-30 15:30:04',0,0,'cart'),
	(233,NULL,'kPBCeN6jKnO3tlDy5Vd2O3KDGfIa0uAN_1',NULL,9828.00,0.00,NULL,'2020-08-03 03:07:08','2020-08-03 03:07:08',0,0,'cart'),
	(234,NULL,'kPBCeN6jKnO3tlDy5Vd2O3KDGfIa0uAN_2',NULL,0.00,0.00,NULL,'2020-08-03 03:07:08','2020-08-03 03:07:08',0,0,'cart'),
	(235,NULL,'0gpJqgRrVyBPN5uoS4tLcUFA6xHDhcTG_1',NULL,5184.00,0.00,NULL,'2020-08-03 03:10:46','2020-08-03 03:10:50',0,0,'cart'),
	(240,NULL,'gRWPs7QQaSrfQmuIKT4NJea0A7Cje1w1_1',NULL,7517.00,0.00,NULL,'2020-08-05 09:48:55','2020-08-05 09:49:11',0,0,'cart'),
	(245,NULL,'90rjbFwUj4cUbWWuV4VexM7oVYFKY0vk_1',NULL,9828.00,0.00,NULL,'2020-08-05 10:49:42','2020-08-05 10:56:52',0,0,'cart'),
	(248,NULL,'wM9xAudcDpUvvmSjbhJG9udXnTsZsZ8w_1',NULL,5184.00,0.00,NULL,'2020-08-06 13:29:35','2020-08-06 13:29:37',0,0,'cart'),
	(256,NULL,'SZKejZyw2KVCg1GuRqeqEIbz0CJIRdFu_1','88e633478db93d3ce1f2ea5e4eb200427bb91259',5184.00,0.00,NULL,'2020-08-10 09:59:30','2020-08-10 10:00:13',0,0,'cart'),
	(259,4,'4_1','a6557d4f7048fc64433922d72e18615928df43e1',5184.00,0.00,NULL,'2020-08-10 10:07:32','2020-08-10 10:07:33',0,0,'cart'),
	(266,NULL,'7fmJsLPzpArVQR5gfEj44s9M84IrnGUx_1',NULL,7776.00,0.00,NULL,'2020-08-14 09:04:39','2020-08-14 09:04:40',0,0,'cart'),
	(267,10,'10_1','df6d7c9abe84875beacd2aa6ff618851ae4571ca',6998.00,0.00,NULL,'2020-08-14 09:04:48','2020-08-14 09:04:48',0,0,'cart'),
	(270,NULL,'Q1Nb0LwuTGvRDh7XOTlGPqWQM9QScKxF_1','6dcc9019b10416c22de91c0f352f0679e4fc5ddb',2506.00,0.00,NULL,'2020-08-17 19:14:22','2020-08-17 19:14:27',0,0,'cart'),
	(271,NULL,'AUpBIz8NxB5VAogQs0D2UwkMBnCz4Fsi_1',NULL,9828.00,0.00,NULL,'2020-08-17 20:48:22','2020-08-17 20:48:25',0,0,'cart'),
	(272,NULL,'KiR9sIdLedkswP8RMQhJwp9NlDweUBWY_1',NULL,7776.00,0.00,NULL,'2020-08-18 17:19:30','2020-08-18 17:19:31',0,0,'cart'),
	(273,8,'8_1','69b08410253ed92c60961cc98c9fa9cbfe297288',15843.00,0.00,NULL,'2020-08-18 17:19:43','2020-08-18 17:19:44',0,0,'cart'),
	(274,NULL,'mCDYy0WYkFAjIOuMO5oMy5xMMeRt8ow6_1',NULL,5184.00,0.00,NULL,'2020-08-19 04:19:43','2020-08-19 04:19:44',0,0,'cart'),
	(275,NULL,'ybjfwMtajcoxLEQ1Z0dxWx8zAFCX1Zq1_1',NULL,1253.00,0.00,NULL,'2020-08-21 03:31:44','2020-08-21 03:31:49',0,0,'cart'),
	(276,NULL,'e2G6Ej6meqZoeKB3YJcgA5BR6KjK14dz_1',NULL,5184.00,0.00,NULL,'2020-08-21 04:08:53','2020-08-21 04:09:02',0,0,'cart'),
	(279,NULL,'hal0yA64dNvfSRl7XzKQTmlip8ocTgNA_1',NULL,5184.00,0.00,NULL,'2020-08-21 06:58:05','2020-08-21 06:58:29',0,0,'cart'),
	(286,NULL,'FApenB5L37sAJCPwumOss0ltH5uZq7y8_1',NULL,20196.00,0.00,NULL,'2020-08-21 08:26:08','2020-08-21 08:26:08',0,0,'cart'),
	(288,NULL,'hTyJpJiT03J2NZr3QAsr44d2GTxYPlWX_1',NULL,35100.00,0.00,NULL,'2020-08-24 06:26:17','2020-08-24 06:26:22',0,0,'cart'),
	(289,NULL,'fapUaUfLZbeYMFDi5Ti1Ckt4EOwokduE_1',NULL,10368.00,0.00,NULL,'2020-08-25 13:36:07','2020-08-25 13:36:11',0,0,'cart'),
	(293,NULL,'vSmU91QvhpqR8bkNbIN4hb1J4bZHE23K_1','932c2ecbff4ba07ed7c3da1b14c0f34510a7a261',10368.00,0.00,NULL,'2020-08-25 14:38:13','2020-08-25 14:43:42',0,0,'cart'),
	(295,NULL,'WBi22dhtImZHWtFk9Ain0S15Y1QTm4ie_1',NULL,9828.00,0.00,NULL,'2020-08-27 12:55:02','2020-08-27 12:55:07',0,0,'cart'),
	(296,NULL,'eno9EcQRIDQHNrpOi4Lh1X5gsQAvjKCt_1',NULL,5184.00,0.00,NULL,'2020-08-27 13:36:38','2020-08-27 13:36:43',0,0,'cart'),
	(299,NULL,'wPJ0zgFWcEkjhH13vawkPp7lBcMiQmSk_1',NULL,51300.00,0.00,NULL,'2020-08-28 04:57:35','2020-08-28 05:04:47',0,0,'cart'),
	(307,NULL,'clsYqKSRpceRnICLvEfvEpY4eVkuVKyT_1',NULL,7517.00,0.00,NULL,'2020-08-29 17:02:51','2020-08-29 17:02:56',0,0,'cart'),
	(310,NULL,'fH0d4BqsM1qdpHCyYvIJcUELjH9JJGNP_1','c1414aa7e6376547ef7e29192ece89bd218582a7',5184.00,0.00,NULL,'2020-09-01 11:10:41','2020-09-01 11:11:13',0,0,'cart'),
	(311,NULL,'C9k9tHEanywg6NdMIXEReAPJIj9SlnlQ_1',NULL,5184.00,0.00,NULL,'2020-09-03 06:48:08','2020-09-03 06:48:13',0,0,'cart'),
	(312,NULL,'qbuRgZdCmGeeH64glshSH4dVfwml8r0w_1',NULL,51300.00,0.00,NULL,'2020-09-03 13:33:04','2020-09-03 13:33:04',0,0,'cart'),
	(313,NULL,'sElGI48GY0JyhndIscQWjkEvHEIrm8NG_1',NULL,9828.00,0.00,NULL,'2020-09-03 20:04:52','2020-09-03 20:04:52',0,0,'cart'),
	(314,NULL,'ImMLPkvROCHOmPkexEOSFaJOPUTjx2TC_1',NULL,28512.00,0.00,NULL,'2020-09-06 00:14:01','2020-09-06 00:14:05',0,0,'cart'),
	(316,NULL,'IrXmROAvtiXlMLVt5VeCjPLi7wsFFfFr_1',NULL,10368.00,0.00,NULL,'2020-09-06 08:52:25','2020-09-06 08:52:27',0,0,'cart'),
	(318,NULL,'2reUxDmcDHgvMjCURfEmfwYQhNMOaxzp_1',NULL,51300.00,0.00,NULL,'2020-09-06 12:07:35','2020-09-06 12:07:53',0,0,'cart'),
	(326,NULL,'dmeZkKOCr66dRZQMqJjPnIrAfqzyjm25_1',NULL,51300.00,0.00,NULL,'2020-09-06 12:24:38','2020-09-06 12:32:51',0,0,'cart'),
	(328,NULL,'i56XpplKk9hGRMxbyzKbWVE3yvUP27Kx_1',NULL,10368.00,0.00,NULL,'2020-09-06 14:49:55','2020-09-06 14:50:02',0,0,'cart'),
	(330,NULL,'UlZ4KluFNhXbGiWg3vVKyLUjuT6hF5tQ_1',NULL,9828.00,0.00,NULL,'2020-09-06 16:20:21','2020-09-06 16:20:31',0,0,'cart'),
	(335,NULL,'gvf3FxSnMk03rvv31kL8kzIcmeYUS8Dr_1',NULL,9828.00,0.00,NULL,'2020-09-06 17:05:31','2020-09-06 17:05:45',0,0,'cart'),
	(336,NULL,'KPfFRQKpdD3TQRyTkSqv5KXLo3eyQDdg_1',NULL,10368.00,0.00,NULL,'2020-09-06 17:26:12','2020-09-06 17:26:15',0,0,'cart'),
	(338,NULL,'keUvgkq349ZtzNPhChEnFQ1acobbbPAL_1',NULL,9828.00,0.00,NULL,'2020-09-07 16:11:46','2020-09-07 16:11:57',0,0,'cart'),
	(340,NULL,'MRu5RAAK9Xa7dMi8Ljr0O6RxXO6q1kqH_1',NULL,5184.00,0.00,NULL,'2020-09-09 03:32:13','2020-09-09 03:32:13',0,0,'cart'),
	(344,NULL,'av5UaDPU71a2kbhhA0dAzq63gcNd6nzB_1',NULL,8770.00,0.00,NULL,'2020-09-09 15:27:17','2020-09-09 15:27:17',0,0,'cart'),
	(347,NULL,'cttAtGsWZGXdJF5UHwclshoix6gIctYp_1',NULL,7776.00,0.00,NULL,'2020-09-10 14:37:44','2020-09-10 14:37:48',0,0,'cart'),
	(348,NULL,'ZMjILiG5JWOHUcQw75pDqsRidbQxqYL4_1',NULL,35100.00,0.00,NULL,'2020-09-10 18:04:31','2020-09-10 18:04:40',0,0,'cart'),
	(349,NULL,'QcZRviCNgZFmsqXZxyL4mVMwfVrMpkux_1',NULL,9828.00,0.00,NULL,'2020-09-10 22:18:05','2020-09-10 22:18:09',0,0,'cart'),
	(350,1,'1_1',NULL,4666.00,0.00,NULL,'2020-09-11 07:12:15','2020-09-11 07:12:15',0,0,'cart'),
	(351,NULL,'AQ1VsKmCKHJ0viASi9CQES7KZKLjL6ri_1',NULL,5184.00,0.00,NULL,'2020-09-12 12:45:48','2020-09-12 12:45:52',0,0,'cart'),
	(353,NULL,'1wMyT4nMd9HLbiNdu1DyGEHOu4fhxPYk_1',NULL,5184.00,0.00,NULL,'2020-09-15 20:51:42','2020-09-15 20:51:46',0,0,'cart'),
	(354,6,'6_1',NULL,1128.00,0.00,NULL,'2020-09-16 01:18:04','2020-09-16 01:18:04',0,0,'cart'),
	(355,18,'18_1','bb4ff0412ed5928325b821d610b240021dccd4c6',8845.00,0.00,NULL,'2020-09-17 16:51:15','2020-09-17 16:51:39',0,0,'cart'),
	(356,NULL,'QKySpCYbAr4rWd74hkLlyq5A7IA3ASut_1',NULL,28512.00,0.00,NULL,'2020-09-17 23:29:20','2020-09-17 23:29:20',0,0,'cart'),
	(360,NULL,'i4ZenxcYwn8hHfwb00D19KyDYjBmAlP9_1',NULL,7517.00,0.00,NULL,'2020-09-18 04:49:02','2020-09-18 04:49:04',0,0,'cart'),
	(361,NULL,'Z0ZTaXcMxOeJsKuArJPzQDV4AoGnE3Pa_1',NULL,9828.00,0.00,NULL,'2020-09-18 10:05:20','2020-09-18 10:05:24',0,0,'cart'),
	(362,NULL,'IK0JRiUPUtTxYfnQz6rkjHDL1xyGWoqD_1',NULL,28512.00,0.00,NULL,'2020-09-19 03:59:54','2020-09-19 03:59:54',0,0,'cart'),
	(365,NULL,'wo1Hc665i8zIy5TvaZ9jiRVhN64YmivJ_1','0c498cdd3e754b91f11a0611265e48bdf4a643c1',18144.00,0.00,NULL,'2020-09-19 13:01:56','2020-09-19 13:03:48',0,0,'cart'),
	(368,NULL,'aLd33xpDtNZdqZQGCEwvvxAY8RcMhRpO_1',NULL,9828.00,0.00,NULL,'2020-09-20 05:48:09','2020-09-20 05:48:11',0,0,'cart');

/*!40000 ALTER TABLE `dtb_cart` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dtb_cart_item
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dtb_cart_item`;

CREATE TABLE `dtb_cart_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_class_id` int(10) unsigned DEFAULT NULL,
  `cart_id` int(10) unsigned DEFAULT NULL,
  `price` decimal(12,2) NOT NULL DEFAULT 0.00,
  `quantity` decimal(10,0) NOT NULL DEFAULT 0,
  `point_rate` decimal(10,0) unsigned DEFAULT NULL,
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B0228F7421B06187` (`product_class_id`),
  KEY `IDX_B0228F741AD5CDBF` (`cart_id`),
  CONSTRAINT `FK_B0228F741AD5CDBF` FOREIGN KEY (`cart_id`) REFERENCES `dtb_cart` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_B0228F7421B06187` FOREIGN KEY (`product_class_id`) REFERENCES `dtb_product_class` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dtb_cart_item` WRITE;
/*!40000 ALTER TABLE `dtb_cart_item` DISABLE KEYS */;

INSERT INTO `dtb_cart_item` (`id`, `product_class_id`, `cart_id`, `price`, `quantity`, `point_rate`, `discriminator_type`)
VALUES
	(1,11,1,10799.00,1,NULL,'cartitem'),
	(5,11,6,10799.00,1,NULL,'cartitem'),
	(44,11,28,10999.00,1,NULL,'cartitem'),
	(61,37,41,8800.00,1,NULL,'cartitem'),
	(64,37,44,8800.00,1,NULL,'cartitem'),
	(70,38,50,2200.00,1,NULL,'cartitem'),
	(71,36,50,13200.00,1,NULL,'cartitem'),
	(75,37,54,8800.00,1,NULL,'cartitem'),
	(76,36,55,13200.00,1,NULL,'cartitem'),
	(78,38,57,2200.00,1,NULL,'cartitem'),
	(79,37,58,8800.00,1,NULL,'cartitem'),
	(95,38,74,1276.00,1,NULL,'cartitem'),
	(203,49,175,24300.00,1,NULL,'cartitem'),
	(220,54,194,6998.00,1,NULL,'cartitem'),
	(221,42,194,13997.00,1,NULL,'cartitem'),
	(222,59,194,6765.00,1,NULL,'cartitem'),
	(223,46,195,9828.00,1,NULL,'cartitem'),
	(236,38,208,1253.00,1,NULL,'cartitem'),
	(248,38,217,1253.00,1,NULL,'cartitem'),
	(249,38,218,1128.00,5,NULL,'cartitem'),
	(269,38,229,1253.00,1,NULL,'cartitem'),
	(270,44,230,5184.00,1,NULL,'cartitem'),
	(271,46,231,9828.00,1,NULL,'cartitem'),
	(273,46,233,9828.00,1,NULL,'cartitem'),
	(274,44,235,5184.00,1,NULL,'cartitem'),
	(285,46,245,9828.00,1,NULL,'cartitem'),
	(299,44,259,5184.00,1,NULL,'cartitem'),
	(306,54,267,6998.00,1,NULL,'cartitem'),
	(310,38,270,1253.00,2,NULL,'cartitem'),
	(311,46,271,9828.00,1,NULL,'cartitem'),
	(312,54,273,6998.00,1,NULL,'cartitem'),
	(313,46,273,8845.00,1,NULL,'cartitem'),
	(314,44,274,5184.00,1,NULL,'cartitem'),
	(315,38,275,1253.00,1,NULL,'cartitem'),
	(316,44,276,5184.00,1,NULL,'cartitem'),
	(319,44,279,5184.00,1,NULL,'cartitem'),
	(327,44,286,5184.00,2,NULL,'cartitem'),
	(328,46,286,9828.00,1,NULL,'cartitem'),
	(330,48,288,35100.00,1,NULL,'cartitem'),
	(331,56,289,10368.00,1,NULL,'cartitem'),
	(335,56,293,10368.00,1,NULL,'cartitem'),
	(336,46,295,9828.00,1,NULL,'cartitem'),
	(337,44,296,5184.00,1,NULL,'cartitem'),
	(340,36,299,51300.00,1,NULL,'cartitem'),
	(349,59,307,7517.00,1,NULL,'cartitem'),
	(352,44,310,5184.00,1,NULL,'cartitem'),
	(353,44,311,5184.00,1,NULL,'cartitem'),
	(354,36,312,51300.00,1,NULL,'cartitem'),
	(355,46,313,9828.00,1,NULL,'cartitem'),
	(356,40,314,28512.00,1,NULL,'cartitem'),
	(358,56,316,10368.00,1,NULL,'cartitem'),
	(360,36,318,51300.00,1,NULL,'cartitem'),
	(371,46,330,9828.00,1,NULL,'cartitem'),
	(376,46,335,9828.00,1,NULL,'cartitem'),
	(377,56,336,10368.00,1,NULL,'cartitem'),
	(380,44,340,5184.00,1,NULL,'cartitem'),
	(384,38,344,1253.00,1,NULL,'cartitem'),
	(385,59,344,7517.00,1,NULL,'cartitem'),
	(389,54,347,7776.00,1,NULL,'cartitem'),
	(390,48,348,35100.00,1,NULL,'cartitem'),
	(391,46,349,9828.00,1,NULL,'cartitem'),
	(392,44,350,4666.00,1,NULL,'cartitem'),
	(393,44,351,5184.00,1,NULL,'cartitem'),
	(395,44,353,5184.00,1,NULL,'cartitem'),
	(396,38,354,1128.00,1,NULL,'cartitem'),
	(397,46,355,8845.00,1,NULL,'cartitem'),
	(398,40,356,28512.00,1,NULL,'cartitem'),
	(399,59,360,7517.00,1,NULL,'cartitem'),
	(400,46,361,9828.00,1,NULL,'cartitem'),
	(401,40,362,28512.00,1,NULL,'cartitem'),
	(404,54,365,7776.00,1,NULL,'cartitem'),
	(405,56,365,10368.00,1,NULL,'cartitem'),
	(410,46,368,9828.00,1,NULL,'cartitem');

/*!40000 ALTER TABLE `dtb_cart_item` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dtb_category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dtb_category`;

CREATE TABLE `dtb_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_category_id` int(10) unsigned DEFAULT NULL,
  `creator_id` int(10) unsigned DEFAULT NULL,
  `category_name` varchar(255) NOT NULL,
  `hierarchy` int(10) unsigned NOT NULL,
  `sort_no` int(11) NOT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_5ED2C2B796A8F92` (`parent_category_id`),
  KEY `IDX_5ED2C2B61220EA6` (`creator_id`),
  CONSTRAINT `FK_5ED2C2B61220EA6` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`id`),
  CONSTRAINT `FK_5ED2C2B796A8F92` FOREIGN KEY (`parent_category_id`) REFERENCES `dtb_category` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dtb_category` WRITE;
/*!40000 ALTER TABLE `dtb_category` DISABLE KEYS */;

INSERT INTO `dtb_category` (`id`, `parent_category_id`, `creator_id`, `category_name`, `hierarchy`, `sort_no`, `create_date`, `update_date`, `discriminator_type`)
VALUES
	(1,NULL,1,'CBD商品',1,13,'2017-03-07 10:14:52','2020-06-23 08:27:34','category'),
	(3,1,1,'CBDオイル',2,12,'2017-03-07 10:14:52','2020-05-09 10:31:10','category'),
	(7,1,1,'CBDウォーター',2,2,'2020-04-28 23:49:55','2020-05-15 06:55:57','category'),
	(15,1,1,'その他',2,1,'2020-05-09 10:31:04','2020-05-09 10:31:10','category'),
	(19,31,1,'10ml',4,9,'2020-05-10 03:48:48','2020-06-17 05:16:41','category'),
	(20,31,1,'30ml',4,3,'2020-05-10 03:48:54','2020-06-17 05:16:41','category'),
	(21,32,1,'10%（高濃度をお求めの方）',4,4,'2020-05-10 03:50:11','2020-06-17 10:21:54','category'),
	(22,32,1,'5%（実感がなく少し物足らない方）',4,5,'2020-05-10 03:50:19','2020-06-17 05:22:30','category'),
	(23,32,1,'3%（一番人気がありMARITIMEお勧め商品です）',4,6,'2020-05-10 03:50:24','2020-06-17 05:22:06','category'),
	(24,32,1,'2%（はじめての方と薄めがいい方）',4,7,'2020-05-10 03:50:31','2020-06-17 05:21:38','category'),
	(25,32,1,'1%（はじめての方に）',4,8,'2020-05-10 03:50:37','2020-06-17 05:21:17','category'),
	(31,3,1,'内容量',3,10,'2020-06-17 05:16:14','2020-06-17 05:16:14','category'),
	(32,3,1,'CBD％',3,11,'2020-06-17 05:16:22','2020-06-17 05:16:22','category');

/*!40000 ALTER TABLE `dtb_category` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dtb_class_category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dtb_class_category`;

CREATE TABLE `dtb_class_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_name_id` int(10) unsigned DEFAULT NULL,
  `creator_id` int(10) unsigned DEFAULT NULL,
  `backend_name` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` int(10) unsigned NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT 1,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_9B0D1DBAB462FB2A` (`class_name_id`),
  KEY `IDX_9B0D1DBA61220EA6` (`creator_id`),
  CONSTRAINT `FK_9B0D1DBA61220EA6` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`id`),
  CONSTRAINT `FK_9B0D1DBAB462FB2A` FOREIGN KEY (`class_name_id`) REFERENCES `dtb_class_name` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dtb_class_category` WRITE;
/*!40000 ALTER TABLE `dtb_class_category` DISABLE KEYS */;

INSERT INTO `dtb_class_category` (`id`, `class_name_id`, `creator_id`, `backend_name`, `name`, `sort_no`, `visible`, `create_date`, `update_date`, `discriminator_type`)
VALUES
	(1,1,1,'小','小',2,0,'2020-05-10 04:38:27','2020-05-15 01:44:59','classcategory'),
	(2,1,1,'大','大',1,0,'2020-05-10 04:38:41','2020-05-15 01:44:55','classcategory'),
	(3,2,1,'通常購入','通常購入',2,1,'2020-05-14 22:22:16','2020-05-14 22:28:19','classcategory'),
	(4,2,1,'定期購買','定期購買',1,1,'2020-05-14 22:22:42','2020-05-14 22:28:35','classcategory');

/*!40000 ALTER TABLE `dtb_class_category` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dtb_class_name
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dtb_class_name`;

CREATE TABLE `dtb_class_name` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `creator_id` int(10) unsigned DEFAULT NULL,
  `backend_name` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` int(10) unsigned NOT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_187C95AD61220EA6` (`creator_id`),
  CONSTRAINT `FK_187C95AD61220EA6` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dtb_class_name` WRITE;
/*!40000 ALTER TABLE `dtb_class_name` DISABLE KEYS */;

INSERT INTO `dtb_class_name` (`id`, `creator_id`, `backend_name`, `name`, `sort_no`, `create_date`, `update_date`, `discriminator_type`)
VALUES
	(1,1,'サイズ','サイズ',1,'2020-05-10 04:36:55','2020-05-10 04:37:46','classname'),
	(2,1,'販売種別','販売種別',2,'2020-05-14 22:21:43','2020-05-14 22:21:43','classname');

/*!40000 ALTER TABLE `dtb_class_name` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dtb_csv
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dtb_csv`;

CREATE TABLE `dtb_csv` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `csv_type_id` smallint(5) unsigned DEFAULT NULL,
  `creator_id` int(10) unsigned DEFAULT NULL,
  `entity_name` varchar(255) NOT NULL,
  `field_name` varchar(255) NOT NULL,
  `reference_field_name` varchar(255) DEFAULT NULL,
  `disp_name` varchar(255) NOT NULL,
  `sort_no` smallint(5) unsigned NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F55F48C3E8507796` (`csv_type_id`),
  KEY `IDX_F55F48C361220EA6` (`creator_id`),
  CONSTRAINT `FK_F55F48C361220EA6` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`id`),
  CONSTRAINT `FK_F55F48C3E8507796` FOREIGN KEY (`csv_type_id`) REFERENCES `mtb_csv_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dtb_csv` WRITE;
/*!40000 ALTER TABLE `dtb_csv` DISABLE KEYS */;

INSERT INTO `dtb_csv` (`id`, `csv_type_id`, `creator_id`, `entity_name`, `field_name`, `reference_field_name`, `disp_name`, `sort_no`, `enabled`, `create_date`, `update_date`, `discriminator_type`)
VALUES
	(1,1,NULL,'Eccube\\\\Entity\\\\Product','id',NULL,'商品ID',1,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(2,1,NULL,'Eccube\\\\Entity\\\\Product','Status','id','公開ステータス(ID)',2,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(3,1,NULL,'Eccube\\\\Entity\\\\Product','Status','name','公開ステータス(名称)',3,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(4,1,NULL,'Eccube\\\\Entity\\\\Product','name',NULL,'商品名',4,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(5,1,NULL,'Eccube\\\\Entity\\\\Product','note',NULL,'ショップ用メモ欄',5,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(6,1,NULL,'Eccube\\\\Entity\\\\Product','description_list',NULL,'商品説明(一覧)',6,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(7,1,NULL,'Eccube\\\\Entity\\\\Product','description_detail',NULL,'商品説明(詳細)',7,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(8,1,NULL,'Eccube\\\\Entity\\\\Product','search_word',NULL,'検索ワード',8,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(9,1,NULL,'Eccube\\\\Entity\\\\Product','free_area',NULL,'フリーエリア',9,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(10,1,NULL,'Eccube\\\\Entity\\\\ProductClass','id',NULL,'商品規格ID',10,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(11,1,NULL,'Eccube\\\\Entity\\\\ProductClass','SaleType','id','販売種別(ID)',11,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(12,1,NULL,'Eccube\\\\Entity\\\\ProductClass','SaleType','name','販売種別(名称)',12,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(13,1,NULL,'Eccube\\\\Entity\\\\ProductClass','ClassCategory1','id','規格分類1(ID)',13,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(14,1,NULL,'Eccube\\\\Entity\\\\ProductClass','ClassCategory1','name','規格分類1(名称)',14,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(15,1,NULL,'Eccube\\\\Entity\\\\ProductClass','ClassCategory2','id','規格分類2(ID)',15,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(16,1,NULL,'Eccube\\\\Entity\\\\ProductClass','ClassCategory2','name','規格分類2(名称)',16,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(17,1,NULL,'Eccube\\\\Entity\\\\ProductClass','DeliveryDuration','id','発送日目安(ID)',17,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(18,1,NULL,'Eccube\\\\Entity\\\\ProductClass','DeliveryDuration','name','発送日目安(名称)',18,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(19,1,NULL,'Eccube\\\\Entity\\\\ProductClass','code',NULL,'商品コード',19,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(20,1,NULL,'Eccube\\\\Entity\\\\ProductClass','stock',NULL,'在庫数',20,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(21,1,NULL,'Eccube\\\\Entity\\\\ProductClass','stock_unlimited',NULL,'在庫数無制限フラグ',21,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(22,1,NULL,'Eccube\\\\Entity\\\\ProductClass','sale_limit',NULL,'販売制限数',22,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(23,1,NULL,'Eccube\\\\Entity\\\\ProductClass','price01',NULL,'通常価格',23,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(24,1,NULL,'Eccube\\\\Entity\\\\ProductClass','price02',NULL,'販売価格',24,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(25,1,1,'Eccube\\\\Entity\\\\ProductClass','delivery_fee',NULL,'送料',30,1,'2017-03-07 10:14:00','2020-06-17 02:56:25','csv'),
	(26,1,1,'Eccube\\\\Entity\\\\Product','ProductImage','file_name','商品画像',25,1,'2017-03-07 10:14:00','2020-06-17 02:56:25','csv'),
	(27,1,1,'Eccube\\\\Entity\\\\Product','ProductCategories','category_id','商品カテゴリ(ID)',26,1,'2017-03-07 10:14:00','2020-06-17 02:56:25','csv'),
	(28,1,1,'Eccube\\\\Entity\\\\Product','ProductCategories','Category','商品カテゴリ(名称)',27,1,'2017-03-07 10:14:00','2020-06-17 02:56:25','csv'),
	(29,1,1,'Eccube\\\\Entity\\\\Product','ProductTag','tag_id','タグ(ID)',28,1,'2017-03-07 10:14:00','2020-06-17 02:56:25','csv'),
	(30,1,1,'Eccube\\\\Entity\\\\Product','ProductTag','Tag','タグ(名称)',29,1,'2017-03-07 10:14:00','2020-06-17 02:56:25','csv'),
	(31,2,NULL,'Eccube\\\\Entity\\\\Customer','id',NULL,'会員ID',1,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(32,2,NULL,'Eccube\\\\Entity\\\\Customer','name01',NULL,'お名前(姓)',2,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(33,2,NULL,'Eccube\\\\Entity\\\\Customer','name02',NULL,'お名前(名)',3,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(34,2,NULL,'Eccube\\\\Entity\\\\Customer','kana01',NULL,'お名前(セイ)',4,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(35,2,NULL,'Eccube\\\\Entity\\\\Customer','kana02',NULL,'お名前(メイ)',5,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(36,2,NULL,'Eccube\\\\Entity\\\\Customer','company_name',NULL,'会社名',6,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(37,2,NULL,'Eccube\\\\Entity\\\\Customer','postal_code',NULL,'郵便番号',7,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(38,2,NULL,'Eccube\\\\Entity\\\\Customer','Pref','id','都道府県(ID)',9,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(39,2,NULL,'Eccube\\\\Entity\\\\Customer','Pref','name','都道府県(名称)',10,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(40,2,NULL,'Eccube\\\\Entity\\\\Customer','addr01',NULL,'住所1',11,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(41,2,NULL,'Eccube\\\\Entity\\\\Customer','addr02',NULL,'住所2',12,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(42,2,NULL,'Eccube\\\\Entity\\\\Customer','email',NULL,'メールアドレス',13,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(43,2,NULL,'Eccube\\\\Entity\\\\Customer','phone_number',NULL,'TEL',14,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(44,2,NULL,'Eccube\\\\Entity\\\\Customer','Sex','id','性別(ID)',20,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(45,2,NULL,'Eccube\\\\Entity\\\\Customer','Sex','name','性別(名称)',21,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(46,2,NULL,'Eccube\\\\Entity\\\\Customer','Job','id','職業(ID)',22,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(47,2,NULL,'Eccube\\\\Entity\\\\Customer','Job','name','職業(名称)',23,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(48,2,NULL,'Eccube\\\\Entity\\\\Customer','birth',NULL,'誕生日',24,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(49,2,NULL,'Eccube\\\\Entity\\\\Customer','first_buy_date',NULL,'初回購入日',25,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(50,2,NULL,'Eccube\\\\Entity\\\\Customer','last_buy_date',NULL,'最終購入日',26,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(51,2,NULL,'Eccube\\\\Entity\\\\Customer','buy_times',NULL,'購入回数',27,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(52,2,NULL,'Eccube\\\\Entity\\\\Customer','note',NULL,'ショップ用メモ欄',28,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(53,2,NULL,'Eccube\\\\Entity\\\\Customer','Status','id','会員ステータス(ID)',29,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(54,2,NULL,'Eccube\\\\Entity\\\\Customer','Status','name','会員ステータス(名称)',30,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(55,2,NULL,'Eccube\\\\Entity\\\\Customer','create_date',NULL,'登録日',31,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(56,2,NULL,'Eccube\\\\Entity\\\\Customer','update_date',NULL,'更新日',32,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(57,3,NULL,'Eccube\\\\Entity\\\\Order','id',NULL,'注文ID',1,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(58,3,NULL,'Eccube\\\\Entity\\\\Order','order_no',NULL,'注文番号',2,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(59,3,NULL,'Eccube\\\\Entity\\\\Order','Customer','id','会員ID',3,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(60,3,NULL,'Eccube\\\\Entity\\\\Order','name01',NULL,'お名前(姓)',4,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(61,3,NULL,'Eccube\\\\Entity\\\\Order','name02',NULL,'お名前(名)',5,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(62,3,NULL,'Eccube\\\\Entity\\\\Order','kana01',NULL,'お名前(セイ)',6,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(63,3,NULL,'Eccube\\\\Entity\\\\Order','kana02',NULL,'お名前(メイ)',7,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(64,3,NULL,'Eccube\\\\Entity\\\\Order','company_name',NULL,'会社名',8,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(65,3,NULL,'Eccube\\\\Entity\\\\Order','postal_code',NULL,'郵便番号',9,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(66,3,NULL,'Eccube\\\\Entity\\\\Order','Pref','id','都道府県(ID)',10,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(67,3,NULL,'Eccube\\\\Entity\\\\Order','Pref','name','都道府県(名称)',11,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(68,3,NULL,'Eccube\\\\Entity\\\\Order','addr01',NULL,'住所1',12,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(69,3,NULL,'Eccube\\\\Entity\\\\Order','addr02',NULL,'住所2',13,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(70,3,NULL,'Eccube\\\\Entity\\\\Order','email',NULL,'メールアドレス',14,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(71,3,NULL,'Eccube\\\\Entity\\\\Order','phone_number',NULL,'TEL',15,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(72,3,NULL,'Eccube\\\\Entity\\\\Order','Sex','id','性別(ID)',16,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(73,3,NULL,'Eccube\\\\Entity\\\\Order','Sex','name','性別(名称)',17,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(74,3,NULL,'Eccube\\\\Entity\\\\Order','Job','id','職業(ID)',18,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(75,3,NULL,'Eccube\\\\Entity\\\\Order','Job','name','職業(名称)',19,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(76,3,NULL,'Eccube\\\\Entity\\\\Order','birth',NULL,'誕生日',20,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(77,3,NULL,'Eccube\\\\Entity\\\\Order','note',NULL,'ショップ用メモ欄',21,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(78,3,NULL,'Eccube\\\\Entity\\\\Order','subtotal',NULL,'小計',22,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(79,3,NULL,'Eccube\\\\Entity\\\\Order','discount',NULL,'値引き',23,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(80,3,NULL,'Eccube\\\\Entity\\\\Order','delivery_fee_total',NULL,'送料',24,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(81,3,NULL,'Eccube\\\\Entity\\\\Order','tax',NULL,'税金',25,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(82,3,NULL,'Eccube\\\\Entity\\\\Order','total',NULL,'合計',26,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(83,3,NULL,'Eccube\\\\Entity\\\\Order','payment_total',NULL,'支払合計',27,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(84,3,NULL,'Eccube\\\\Entity\\\\Order','OrderStatus','id','対応状況(ID)',28,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(85,3,NULL,'Eccube\\\\Entity\\\\Order','OrderStatus','name','対応状況(名称)',29,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(86,3,NULL,'Eccube\\\\Entity\\\\Order','Payment','id','支払方法(ID)',30,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(87,3,NULL,'Eccube\\\\Entity\\\\Order','payment_method',NULL,'支払方法(名称)',31,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(88,3,NULL,'Eccube\\\\Entity\\\\Order','order_date',NULL,'受注日',32,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(89,3,NULL,'Eccube\\\\Entity\\\\Order','payment_date',NULL,'入金日',33,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(90,3,NULL,'Eccube\\\\Entity\\\\OrderItem','id',NULL,'注文詳細ID',34,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(91,3,NULL,'Eccube\\\\Entity\\\\OrderItem','Product','id','商品ID',35,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(92,3,NULL,'Eccube\\\\Entity\\\\OrderItem','ProductClass','id','商品規格ID',36,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(93,3,NULL,'Eccube\\\\Entity\\\\OrderItem','product_name',NULL,'商品名',37,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(94,3,NULL,'Eccube\\\\Entity\\\\OrderItem','product_code',NULL,'商品コード',38,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(95,3,NULL,'Eccube\\\\Entity\\\\OrderItem','class_name1',NULL,'規格名1',39,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(96,3,NULL,'Eccube\\\\Entity\\\\OrderItem','class_name2',NULL,'規格名2',40,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(97,3,NULL,'Eccube\\\\Entity\\\\OrderItem','class_category_name1',NULL,'規格分類名1',41,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(98,3,NULL,'Eccube\\\\Entity\\\\OrderItem','class_category_name2',NULL,'規格分類名2',42,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(99,3,NULL,'Eccube\\\\Entity\\\\OrderItem','price',NULL,'価格',43,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(100,3,NULL,'Eccube\\\\Entity\\\\OrderItem','quantity',NULL,'個数',44,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(101,3,NULL,'Eccube\\\\Entity\\\\OrderItem','tax_rate',NULL,'税率',45,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(102,3,NULL,'Eccube\\\\Entity\\\\OrderItem','tax_rule',NULL,'税率ルール(ID)',46,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(103,3,NULL,'Eccube\\\\Entity\\\\OrderItem','OrderItemType','id','明細区分(ID)',47,1,'2018-07-23 09:00:00','2018-07-23 09:00:00','csv'),
	(104,3,NULL,'Eccube\\\\Entity\\\\OrderItem','OrderItemType','name','明細区分(名称)',48,1,'2018-07-23 09:00:00','2018-07-23 09:00:00','csv'),
	(105,3,NULL,'Eccube\\\\Entity\\\\Shipping','id',NULL,'配送ID',49,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(106,3,NULL,'Eccube\\\\Entity\\\\Shipping','name01',NULL,'配送先_お名前(姓)',50,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(107,3,NULL,'Eccube\\\\Entity\\\\Shipping','name02',NULL,'配送先_お名前(名)',51,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(108,3,NULL,'Eccube\\\\Entity\\\\Shipping','kana01',NULL,'配送先_お名前(セイ)',52,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(109,3,NULL,'Eccube\\\\Entity\\\\Shipping','kana02',NULL,'配送先_お名前(メイ)',53,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(110,3,NULL,'Eccube\\\\Entity\\\\Shipping','company_name',NULL,'配送先_会社名',54,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(111,3,NULL,'Eccube\\\\Entity\\\\Shipping','postal_code',NULL,'配送先_郵便番号',55,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(112,3,NULL,'Eccube\\\\Entity\\\\Shipping','Pref','id','配送先_都道府県(ID)',56,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(113,3,NULL,'Eccube\\\\Entity\\\\Shipping','Pref','name','配送先_都道府県(名称)',57,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(114,3,NULL,'Eccube\\\\Entity\\\\Shipping','addr01',NULL,'配送先_住所1',58,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(115,3,NULL,'Eccube\\\\Entity\\\\Shipping','addr02',NULL,'配送先_住所2',59,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(116,3,NULL,'Eccube\\\\Entity\\\\Shipping','phone_number',NULL,'配送先_TEL',60,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(117,3,NULL,'Eccube\\\\Entity\\\\Shipping','Delivery','id','配送業者(ID)',61,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(118,3,NULL,'Eccube\\\\Entity\\\\Shipping','shipping_delivery_name',NULL,'配送業者(名称)',62,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(119,3,NULL,'Eccube\\\\Entity\\\\Shipping','DeliveryTime','id','お届け時間ID',63,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(120,3,NULL,'Eccube\\\\Entity\\\\Shipping','shipping_delivery_time',NULL,'お届け時間(名称)',64,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(121,3,NULL,'Eccube\\\\Entity\\\\Shipping','shipping_delivery_date',NULL,'お届け希望日',65,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(122,3,NULL,'Eccube\\\\Entity\\\\Shipping','DeliveryFee','id','送料ID',66,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(123,3,NULL,'Eccube\\\\Entity\\\\Shipping','shipping_delivery_fee',NULL,'送料',67,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(124,3,NULL,'Eccube\\\\Entity\\\\Shipping','shipping_date',NULL,'発送日',68,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(125,3,NULL,'Eccube\\\\Entity\\\\Shipping','tracking_number',NULL,'出荷伝票番号',69,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(126,3,NULL,'Eccube\\\\Entity\\\\Shipping','note',NULL,'配達用メモ',70,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(127,3,NULL,'Eccube\\\\Entity\\\\Shipping','mail_send_date',NULL,'出荷メール送信日',71,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(128,4,NULL,'Eccube\\\\Entity\\\\Order','id',NULL,'注文ID',1,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(129,4,NULL,'Eccube\\\\Entity\\\\Order','order_no',NULL,'注文番号',2,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(130,4,NULL,'Eccube\\\\Entity\\\\Order','Customer','id','会員ID',3,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(131,4,NULL,'Eccube\\\\Entity\\\\Order','name01',NULL,'お名前(姓)',4,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(132,4,NULL,'Eccube\\\\Entity\\\\Order','name02',NULL,'お名前(名)',5,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(133,4,NULL,'Eccube\\\\Entity\\\\Order','kana01',NULL,'お名前(セイ)',6,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(134,4,NULL,'Eccube\\\\Entity\\\\Order','kana02',NULL,'お名前(メイ)',7,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(135,4,NULL,'Eccube\\\\Entity\\\\Order','company_name',NULL,'会社名',8,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(136,4,NULL,'Eccube\\\\Entity\\\\Order','postal_code',NULL,'郵便番号',9,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(137,4,NULL,'Eccube\\\\Entity\\\\Order','Pref','id','都道府県(ID)',10,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(138,4,NULL,'Eccube\\\\Entity\\\\Order','Pref','name','都道府県(名称)',11,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(139,4,NULL,'Eccube\\\\Entity\\\\Order','addr01',NULL,'住所1',12,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(140,4,NULL,'Eccube\\\\Entity\\\\Order','addr02',NULL,'住所2',13,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(141,4,NULL,'Eccube\\\\Entity\\\\Order','email',NULL,'メールアドレス',14,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(142,4,NULL,'Eccube\\\\Entity\\\\Order','phone_number',NULL,'TEL',15,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(143,4,NULL,'Eccube\\\\Entity\\\\Order','Sex','id','性別(ID)',16,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(144,4,NULL,'Eccube\\\\Entity\\\\Order','Sex','name','性別(名称)',17,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(145,4,NULL,'Eccube\\\\Entity\\\\Order','Job','id','職業(ID)',18,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(146,4,NULL,'Eccube\\\\Entity\\\\Order','Job','name','職業(名称)',19,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(147,4,NULL,'Eccube\\\\Entity\\\\Order','birth',NULL,'誕生日',20,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(148,4,NULL,'Eccube\\\\Entity\\\\Order','note',NULL,'ショップ用メモ欄',21,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(149,4,NULL,'Eccube\\\\Entity\\\\Order','subtotal',NULL,'小計',22,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(150,4,NULL,'Eccube\\\\Entity\\\\Order','discount',NULL,'値引き',23,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(151,4,NULL,'Eccube\\\\Entity\\\\Order','delivery_fee_total',NULL,'送料',24,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(152,4,NULL,'Eccube\\\\Entity\\\\Order','tax',NULL,'税金',25,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(153,4,NULL,'Eccube\\\\Entity\\\\Order','total',NULL,'合計',26,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(154,4,NULL,'Eccube\\\\Entity\\\\Order','payment_total',NULL,'支払合計',27,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(155,4,NULL,'Eccube\\\\Entity\\\\Order','OrderStatus','id','対応状況(ID)',28,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(156,4,NULL,'Eccube\\\\Entity\\\\Order','OrderStatus','name','対応状況(名称)',29,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(157,4,NULL,'Eccube\\\\Entity\\\\Order','Payment','id','支払方法(ID)',30,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(158,4,NULL,'Eccube\\\\Entity\\\\Order','payment_method',NULL,'支払方法(名称)',31,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(159,4,NULL,'Eccube\\\\Entity\\\\Order','order_date',NULL,'受注日',32,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(160,4,NULL,'Eccube\\\\Entity\\\\Order','payment_date',NULL,'入金日',33,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(161,4,NULL,'Eccube\\\\Entity\\\\OrderItem','id',NULL,'注文詳細ID',34,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(162,4,NULL,'Eccube\\\\Entity\\\\OrderItem','Product','id','商品ID',35,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(163,4,NULL,'Eccube\\\\Entity\\\\OrderItem','ProductClass','id','商品規格ID',36,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(164,4,NULL,'Eccube\\\\Entity\\\\OrderItem','product_name',NULL,'商品名',37,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(165,4,NULL,'Eccube\\\\Entity\\\\OrderItem','product_code',NULL,'商品コード',38,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(166,4,NULL,'Eccube\\\\Entity\\\\OrderItem','class_name1',NULL,'規格名1',39,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(167,4,NULL,'Eccube\\\\Entity\\\\OrderItem','class_name2',NULL,'規格名2',40,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(168,4,NULL,'Eccube\\\\Entity\\\\OrderItem','class_category_name1',NULL,'規格分類名1',41,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(169,4,NULL,'Eccube\\\\Entity\\\\OrderItem','class_category_name2',NULL,'規格分類名2',42,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(170,4,NULL,'Eccube\\\\Entity\\\\OrderItem','price',NULL,'価格',43,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(171,4,NULL,'Eccube\\\\Entity\\\\OrderItem','quantity',NULL,'個数',44,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(172,4,NULL,'Eccube\\\\Entity\\\\OrderItem','tax_rate',NULL,'税率',45,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(173,4,NULL,'Eccube\\\\Entity\\\\OrderItem','tax_rule',NULL,'税率ルール(ID)',46,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(174,4,NULL,'Eccube\\\\Entity\\\\OrderItem','OrderItemType','id','明細区分(ID)',47,1,'2018-07-23 09:00:00','2018-07-23 09:00:00','csv'),
	(175,4,NULL,'Eccube\\\\Entity\\\\OrderItem','OrderItemType','name','明細区分(名称)',48,1,'2018-07-23 09:00:00','2018-07-23 09:00:00','csv'),
	(176,4,NULL,'Eccube\\\\Entity\\\\Shipping','id',NULL,'配送ID',49,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(177,4,NULL,'Eccube\\\\Entity\\\\Shipping','name01',NULL,'配送先_お名前(姓)',50,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(178,4,NULL,'Eccube\\\\Entity\\\\Shipping','name02',NULL,'配送先_お名前(名)',51,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(179,4,NULL,'Eccube\\\\Entity\\\\Shipping','kana01',NULL,'配送先_お名前(セイ)',52,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(180,4,NULL,'Eccube\\\\Entity\\\\Shipping','kana02',NULL,'配送先_お名前(メイ)',53,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(181,4,NULL,'Eccube\\\\Entity\\\\Shipping','company_name',NULL,'配送先_会社名',54,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(182,4,NULL,'Eccube\\\\Entity\\\\Shipping','postal_code',NULL,'配送先_郵便番号',55,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(183,4,NULL,'Eccube\\\\Entity\\\\Shipping','Pref','id','配送先_都道府県(ID)',56,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(184,4,NULL,'Eccube\\\\Entity\\\\Shipping','Pref','name','配送先_都道府県(名称)',57,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(185,4,NULL,'Eccube\\\\Entity\\\\Shipping','addr01',NULL,'配送先_住所1',58,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(186,4,NULL,'Eccube\\\\Entity\\\\Shipping','addr02',NULL,'配送先_住所2',59,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(187,4,NULL,'Eccube\\\\Entity\\\\Shipping','phone_number',NULL,'配送先_TEL',60,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(188,4,NULL,'Eccube\\\\Entity\\\\Shipping','Delivery','id','配送業者(ID)',61,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(189,4,NULL,'Eccube\\\\Entity\\\\Shipping','shipping_delivery_name',NULL,'配送業者(名称)',62,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(190,4,NULL,'Eccube\\\\Entity\\\\Shipping','DeliveryTime','id','お届け時間ID',63,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(191,4,NULL,'Eccube\\\\Entity\\\\Shipping','shipping_delivery_time',NULL,'お届け時間(名称)',64,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(192,4,NULL,'Eccube\\\\Entity\\\\Shipping','shipping_delivery_date',NULL,'お届け希望日',65,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(193,4,NULL,'Eccube\\\\Entity\\\\Shipping','DeliveryFee','id','送料ID',66,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(194,4,NULL,'Eccube\\\\Entity\\\\Shipping','shipping_delivery_fee',NULL,'送料',67,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(195,4,NULL,'Eccube\\\\Entity\\\\Shipping','shipping_date',NULL,'発送日',68,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(196,4,NULL,'Eccube\\\\Entity\\\\Shipping','tracking_number',NULL,'出荷伝票番号',69,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(197,4,NULL,'Eccube\\\\Entity\\\\Shipping','note',NULL,'配達用メモ',70,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(198,4,NULL,'Eccube\\\\Entity\\\\Shipping','mail_send_date',NULL,'出荷メール送信日',71,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(199,5,NULL,'Eccube\\\\Entity\\\\Category','id',NULL,'カテゴリID',1,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(200,5,NULL,'Eccube\\\\Entity\\\\Category','sort_no',NULL,'表示ランク',2,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(201,5,NULL,'Eccube\\\\Entity\\\\Category','name',NULL,'カテゴリ名',3,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(202,5,NULL,'Eccube\\\\Entity\\\\Category','Parent','id','親カテゴリID',4,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(203,5,NULL,'Eccube\\\\Entity\\\\Category','level',NULL,'階層',5,1,'2017-03-07 10:14:00','2017-03-07 10:14:00','csv'),
	(204,1,1,'Eccube\\\\Entity\\\\ProductClass','TaxRule','tax_rate','税率',31,1,'2017-03-07 10:14:00','2020-06-17 02:56:25','csv'),
	(205,6,1,'Plugin\\ProductReview4\\Entity\\ProductReview','Product','name','商品名',1,1,'2020-05-06 11:18:12','2020-05-06 11:18:12','csv'),
	(206,6,1,'Plugin\\ProductReview4\\Entity\\ProductReview','Status','name','公開・非公開',2,1,'2020-05-06 11:18:12','2020-05-06 11:18:12','csv'),
	(207,6,1,'Plugin\\ProductReview4\\Entity\\ProductReview','create_date','create_date','投稿日',3,1,'2020-05-06 11:18:12','2020-05-06 11:18:12','csv'),
	(208,6,1,'Plugin\\ProductReview4\\Entity\\ProductReview','reviewer_name','reviewer_name','投稿者名',4,1,'2020-05-06 11:18:12','2020-05-06 11:18:12','csv'),
	(209,6,1,'Plugin\\ProductReview4\\Entity\\ProductReview','reviewer_url','reviewer_url','投稿者URL',5,1,'2020-05-06 11:18:12','2020-05-06 11:18:12','csv'),
	(210,6,1,'Plugin\\ProductReview4\\Entity\\ProductReview','Sex','name','性別',6,1,'2020-05-06 11:18:12','2020-05-06 11:18:12','csv'),
	(211,6,1,'Plugin\\ProductReview4\\Entity\\ProductReview','recommend_level','recommend_level','おすすめレベル',7,1,'2020-05-06 11:18:12','2020-05-06 11:18:12','csv'),
	(212,6,1,'Plugin\\ProductReview4\\Entity\\ProductReview','title','title','タイトル',8,1,'2020-05-06 11:18:12','2020-05-06 11:18:12','csv'),
	(213,6,1,'Plugin\\ProductReview4\\Entity\\ProductReview','comment','comment','コメント',9,1,'2020-05-06 11:18:12','2020-05-06 11:18:12','csv'),
	(214,2,1,'Eccube\\\\Entity\\\\Customer','plgCcpCustomerClass','name','特定会員種別',9999,1,'2020-05-27 01:41:47','2020-05-27 01:41:47','csv');

/*!40000 ALTER TABLE `dtb_csv` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dtb_customer
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dtb_customer`;

CREATE TABLE `dtb_customer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_status_id` smallint(5) unsigned DEFAULT NULL,
  `sex_id` smallint(5) unsigned DEFAULT NULL,
  `job_id` smallint(5) unsigned DEFAULT NULL,
  `country_id` smallint(5) unsigned DEFAULT NULL,
  `pref_id` smallint(5) unsigned DEFAULT NULL,
  `name01` varchar(255) NOT NULL,
  `name02` varchar(255) NOT NULL,
  `kana01` varchar(255) DEFAULT NULL,
  `kana02` varchar(255) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `postal_code` varchar(8) DEFAULT NULL,
  `addr01` varchar(255) DEFAULT NULL,
  `addr02` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `phone_number` varchar(14) DEFAULT NULL,
  `birth` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `secret_key` varchar(255) NOT NULL,
  `first_buy_date` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `last_buy_date` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `buy_times` decimal(10,0) unsigned DEFAULT 0,
  `buy_total` decimal(12,2) unsigned DEFAULT 0.00,
  `note` varchar(4000) DEFAULT NULL,
  `reset_key` varchar(255) DEFAULT NULL,
  `reset_expire` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `point` decimal(12,0) NOT NULL DEFAULT 0,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL,
  `plg_mailmagazine_flg` smallint(5) unsigned DEFAULT 0,
  `plg_ccp_customer_class_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `secret_key` (`secret_key`),
  KEY `IDX_8298BBE3C00AF8A7` (`customer_status_id`),
  KEY `IDX_8298BBE35A2DB2A0` (`sex_id`),
  KEY `IDX_8298BBE3BE04EA9` (`job_id`),
  KEY `IDX_8298BBE3F92F3E70` (`country_id`),
  KEY `IDX_8298BBE3E171EF5F` (`pref_id`),
  KEY `dtb_customer_buy_times_idx` (`buy_times`),
  KEY `dtb_customer_buy_total_idx` (`buy_total`),
  KEY `dtb_customer_create_date_idx` (`create_date`),
  KEY `dtb_customer_update_date_idx` (`update_date`),
  KEY `dtb_customer_last_buy_date_idx` (`last_buy_date`),
  KEY `dtb_customer_email_idx` (`email`),
  KEY `IDX_8298BBE39A406193` (`plg_ccp_customer_class_id`),
  CONSTRAINT `FK_8298BBE35A2DB2A0` FOREIGN KEY (`sex_id`) REFERENCES `mtb_sex` (`id`),
  CONSTRAINT `FK_8298BBE39A406193` FOREIGN KEY (`plg_ccp_customer_class_id`) REFERENCES `plg_ccp_customer_class` (`id`),
  CONSTRAINT `FK_8298BBE3BE04EA9` FOREIGN KEY (`job_id`) REFERENCES `mtb_job` (`id`),
  CONSTRAINT `FK_8298BBE3C00AF8A7` FOREIGN KEY (`customer_status_id`) REFERENCES `mtb_customer_status` (`id`),
  CONSTRAINT `FK_8298BBE3E171EF5F` FOREIGN KEY (`pref_id`) REFERENCES `mtb_pref` (`id`),
  CONSTRAINT `FK_8298BBE3F92F3E70` FOREIGN KEY (`country_id`) REFERENCES `mtb_country` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dtb_customer` WRITE;
/*!40000 ALTER TABLE `dtb_customer` DISABLE KEYS */;

INSERT INTO `dtb_customer` (`id`, `customer_status_id`, `sex_id`, `job_id`, `country_id`, `pref_id`, `name01`, `name02`, `kana01`, `kana02`, `company_name`, `postal_code`, `addr01`, `addr02`, `email`, `phone_number`, `birth`, `password`, `salt`, `secret_key`, `first_buy_date`, `last_buy_date`, `buy_times`, `buy_total`, `note`, `reset_key`, `reset_expire`, `point`, `create_date`, `update_date`, `discriminator_type`, `plg_mailmagazine_flg`, `plg_ccp_customer_class_id`)
VALUES
	(1,2,1,2,NULL,14,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'2220032','横浜市港北区大豆戸町171-1','ネイバーズ菊名311','djfre2000@hotmail.com','07026558129','1985-04-05 15:00:00','7c9ea39fd3474276c69536871e28c6728e9db7f1a035a7184e90f8c43e311165','7c1dd9a845','65aEW8rhYIMr8z7yjLLp5kzvM6W6jITM',NULL,NULL,0,0.00,NULL,'Gp6QNsS1fARrzgSOTWDqFKVoclpchY3A','2020-09-07 03:33:05',500,'2020-05-01 08:27:19','2020-09-15 23:54:10','customer',1,1),
	(2,1,NULL,NULL,NULL,13,'Test','Test','カナ','カナ',NULL,'1000001','千代田区千代田','1-1-1','05049pyj@gmail.com','11111111111',NULL,'03e21171469d774c9c42c7fca44d0d0af889868a31a257b136884e5de1bd7874','d2827277e5','GcNh0olbKL14ABg4J5ckxWE5hfBCsLDw',NULL,NULL,0,0.00,NULL,NULL,NULL,0,'2020-05-15 10:05:22','2020-05-27 02:47:36','customer',0,1),
	(4,2,NULL,NULL,NULL,13,'Test2','Test','カナ','カナ',NULL,'1000001','千代田区千代田','1-1-1','05049pyj+1@gmail.com','11122223333',NULL,'3c2794b8a78cd97e315703e1dc85298dd1478393a9ef5b8f6273354cfe0999f8','5b2c76e586','4D1HRfhtYtDLhgPpsvDY7JuaZxm9H3vf',NULL,NULL,0,0.00,NULL,NULL,'2020-08-05 14:40:31',0,'2020-05-28 14:50:38','2020-08-05 14:31:46','customer',0,NULL),
	(5,2,NULL,NULL,NULL,13,'Test3','Test','カナ','カナ',NULL,'1000001','千代田区千代田','1-1-1','05049pyj+2@gmail.com','11122223333',NULL,'7adee46e0dfb87864adebb94b3ae1d722f2dd648e1acd5edf25ba8028e44b565','c4a96a1ee0','7yzVwgLYL7yJiUxFOOiWWfhHZJKCeOMS',NULL,NULL,0,0.00,NULL,'x6z507CjaXvlSsA5cCpe0DeHlAllaUZa','2020-09-07 10:57:00',0,'2020-05-28 15:13:20','2020-09-07 10:47:00','customer',0,1),
	(6,2,1,NULL,NULL,13,'Frederic','Test','フレデリック','ヴェルヴァーク',NULL,'1570077','世田谷区鎌田','テスト900','frederic@shourai.io','0123456789',NULL,'211f1daa7581d85a78c21ff4039d4afd75cd91d1ea6a12b4271c47938c149e9f','2f0a537e17','2CMsuAK14sp5A8yLnbEkzVom8ZNaKhgA',NULL,NULL,0,0.00,NULL,NULL,NULL,0,'2020-05-29 00:00:39','2020-09-16 01:17:59','customer',1,1),
	(7,2,NULL,NULL,NULL,13,'杉森','浩一郎','スギモリ','コウイチロウ',NULL,'1520004','目黒区鷹番','1-8-17','cedarforest@me.com','0337128123',NULL,'75969a324a1d953e463a2403a449f4a0c3b9a584a179d23fd247e4d790e49ab1','591afb2678','U0RNX849axz8gU9gU86kTWqnjZo9cl7B',NULL,NULL,0,0.00,NULL,NULL,NULL,0,'2020-06-17 08:22:57','2020-06-17 08:23:28','customer',1,1),
	(8,2,1,2,NULL,13,'杉森','浩一郎','スギモリ','コウイチロウ',NULL,'1520004','目黒区鷹番','1-8-17','ko@thepmi.net','0337128235',NULL,'9d8b9a10115ea6a19f7a7a3aeb9291600720123f925ed0590b1d5487264f0dd6','0c09dd0ca9','6hxH5B5uV8UJAqBR63nVB7urL2V4qYWQ','2020-06-26 09:16:09','2020-07-23 16:12:23',2,26118.00,NULL,'bV5HLn4aA3mHo6fant242EgvRCCVJABc','2020-08-05 11:02:28',386,'2020-06-26 09:11:25','2020-09-07 02:55:18','customer',1,1),
	(9,2,1,7,NULL,28,'永井','光弘','ナガイ','ミツヒロ',NULL,'6780005','相生市大石町','1-27','ny42nagai@gmail.com','09083772905','1964-02-29 15:00:00','1e137718c7803e40d8e55a96b14b6a0b9ec04b250d4c4319199354dc551c4081','76b4eb2228','VecCAMlGaLnxuc0ojukFD7F83LgWKmbc',NULL,NULL,0,0.00,NULL,NULL,NULL,0,'2020-07-02 03:30:35','2020-07-02 03:30:59','customer',1,1),
	(10,2,1,9,NULL,26,'藤永','朋久','フジナガ','トモヒサ','(株)リード企画','6170837','長岡京市久貝2丁目','16-31','harutoua@gmail.com','09050517243','1976-05-05 15:00:00','66e841ce09def858179c697e23d87d356bd24f06a65c82e205afda09ff58f479','f6333e0517','vCbDIOOEdNWyRRHaboLZDWqKo62EMy3Z',NULL,NULL,0,0.00,NULL,NULL,NULL,0,'2020-07-24 02:52:03','2020-07-24 02:52:27','customer',1,1),
	(11,2,1,14,NULL,28,'内藤','武久','ナイトウ','タケヒサ',NULL,'6580073','神戸市東灘区西岡本','2ー7ー4 オ-キッドコ-ト湖南館623号','naito090939@gmail.com','09098766383','1960-11-23 15:00:00','80db84b0c22d4715e88701240e2617f3da88f1ee162902100cfece58ccb5a30e','6b423f8acb','v9vnN8oJmenmsQbhexf4ykMd3wlZ1Ykd',NULL,NULL,0,0.00,NULL,NULL,'2020-09-06 16:45:44',0,'2020-08-08 20:37:18','2020-09-06 16:37:39','customer',1,1),
	(12,2,2,17,NULL,1,'関','歌織','セキ','カオリ',NULL,'0630001','札幌市西区山の手一条8丁目','2-6','kaononko@icloud.com','09062618809','1970-09-03 15:00:00','e39531a3a35211e01997c25ff827f969c701dccb7113487beddb0dd3376f4b80','d9b964c336','BIGkQDOUW6BlWOPdkcEoCGxC3NetMkM8','2020-09-06 14:51:18','2020-09-06 14:51:18',1,9331.00,NULL,NULL,NULL,0,'2020-08-25 13:39:45','2020-09-15 03:18:12','customer',1,1),
	(13,2,1,18,NULL,27,'増田','良枝','マスダ','ヨシエ',NULL,'5320002','大阪市淀川区東三国','6-3-44','wannyasya@gmail.com','08053469756','1983-09-29 15:00:00','7dc688821b62837d4f3ec92b193812e2b0c33df9f8eee293a70e6acd656f005e','cc470824f9','AH7uc3DZ4dxahW3lAvsStq99fvaf7cnk',NULL,NULL,0,0.00,NULL,NULL,NULL,0,'2020-08-28 05:02:16','2020-08-28 05:02:33','customer',0,1),
	(14,2,2,17,NULL,28,'金','海鏡','キム','ヘキョン',NULL,'6580073','神戸市東灘区西岡本','2丁目7ー4','usacelinenaito@gmail.com','08053698131','1969-02-27 15:00:00','6fac0800eb5a73248163af2e6d57ff55de170841b003aebabe396abcf31922aa','6bfb77e051','ZKhXwUsl6aNqw2gDssdbeYDp78praU4E',NULL,NULL,0,0.00,NULL,NULL,NULL,0,'2020-08-29 01:04:09','2020-08-29 03:02:42','customer',1,1),
	(15,2,1,14,NULL,14,'熊澤','智義','クマザワ','トモヨシ','株式会社ウルスマイル','2160012','川崎市宮前区水沢','2-15-24','marugeri@icloud.com','09065153851','1969-06-11 15:00:00','d601957036acf8ad4170c0a0af5d3c89766e89e91ac317660976525b436863f6','3319409dda','rOaKzxQMX1LCDOFsuRBR2I3GkCULeVXL','2020-09-06 12:40:19','2020-09-06 12:40:19',1,41553.00,NULL,NULL,NULL,382,'2020-09-06 12:29:48','2020-09-07 05:25:02','customer',0,1),
	(18,2,1,18,NULL,28,'GIM','HAESOO','キム','ヘソ',NULL,'6580073','神戸市東灘区西岡本2-7-4','オーキッドコート湖南館623','rlagotnz@naver.com','09098766383','1960-07-01 15:00:00','72c9c53fff1aa9409ce34eda88b280f42157e4f1e33dc05d2ac68cbc637f981c','dedd189eeb','lN5gQfrdDArHKxRKMPJKv6ZyPCaBySCN',NULL,NULL,0,0.00,NULL,NULL,NULL,0,'2020-09-07 16:08:04','2020-09-07 16:14:25','customer',1,1);

/*!40000 ALTER TABLE `dtb_customer` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dtb_customer_address
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dtb_customer_address`;

CREATE TABLE `dtb_customer_address` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `country_id` smallint(5) unsigned DEFAULT NULL,
  `pref_id` smallint(5) unsigned DEFAULT NULL,
  `name01` varchar(255) NOT NULL,
  `name02` varchar(255) NOT NULL,
  `kana01` varchar(255) DEFAULT NULL,
  `kana02` varchar(255) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `postal_code` varchar(8) DEFAULT NULL,
  `addr01` varchar(255) DEFAULT NULL,
  `addr02` varchar(255) DEFAULT NULL,
  `phone_number` varchar(14) DEFAULT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_6C38C0F89395C3F3` (`customer_id`),
  KEY `IDX_6C38C0F8F92F3E70` (`country_id`),
  KEY `IDX_6C38C0F8E171EF5F` (`pref_id`),
  CONSTRAINT `FK_6C38C0F89395C3F3` FOREIGN KEY (`customer_id`) REFERENCES `dtb_customer` (`id`),
  CONSTRAINT `FK_6C38C0F8E171EF5F` FOREIGN KEY (`pref_id`) REFERENCES `mtb_pref` (`id`),
  CONSTRAINT `FK_6C38C0F8F92F3E70` FOREIGN KEY (`country_id`) REFERENCES `mtb_country` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dtb_customer_address` WRITE;
/*!40000 ALTER TABLE `dtb_customer_address` DISABLE KEYS */;

INSERT INTO `dtb_customer_address` (`id`, `customer_id`, `country_id`, `pref_id`, `name01`, `name02`, `kana01`, `kana02`, `company_name`, `postal_code`, `addr01`, `addr02`, `phone_number`, `create_date`, `update_date`, `discriminator_type`)
VALUES
	(1,8,NULL,13,'小野','冨美枝','オノ','フミエ',NULL,'1520004','目黒区鷹番','1-8-17','0337128235','2020-06-26 09:14:35','2020-06-26 09:14:35','customeraddress'),
	(2,8,NULL,13,'杉森','勝代','スギモリ','カツヨ',NULL,'1520004','目黒区鷹番','1-8-17','0337128235','2020-07-23 16:11:12','2020-07-23 16:11:12','customeraddress'),
	(3,15,NULL,14,'鈴木','晴美','スズキ','ハルミ',NULL,'2430431','海老名市上今泉','6-53-15','08067574821','2020-09-06 12:35:41','2020-09-06 12:35:41','customeraddress');

/*!40000 ALTER TABLE `dtb_customer_address` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dtb_customer_favorite_product
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dtb_customer_favorite_product`;

CREATE TABLE `dtb_customer_favorite_product` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `product_id` int(10) unsigned DEFAULT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_ED6313839395C3F3` (`customer_id`),
  KEY `IDX_ED6313834584665A` (`product_id`),
  CONSTRAINT `FK_ED6313834584665A` FOREIGN KEY (`product_id`) REFERENCES `dtb_product` (`id`),
  CONSTRAINT `FK_ED6313839395C3F3` FOREIGN KEY (`customer_id`) REFERENCES `dtb_customer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dtb_customer_favorite_product` WRITE;
/*!40000 ALTER TABLE `dtb_customer_favorite_product` DISABLE KEYS */;

INSERT INTO `dtb_customer_favorite_product` (`id`, `customer_id`, `product_id`, `create_date`, `update_date`, `discriminator_type`)
VALUES
	(1,1,2,'2020-05-01 08:32:09','2020-05-01 08:32:09','customerfavoriteproduct');

/*!40000 ALTER TABLE `dtb_customer_favorite_product` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dtb_delivery
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dtb_delivery`;

CREATE TABLE `dtb_delivery` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `creator_id` int(10) unsigned DEFAULT NULL,
  `sale_type_id` smallint(5) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `service_name` varchar(255) DEFAULT NULL,
  `description` varchar(4000) DEFAULT NULL,
  `confirm_url` varchar(4000) DEFAULT NULL,
  `sort_no` int(10) unsigned DEFAULT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT 1,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_3420D9FA61220EA6` (`creator_id`),
  KEY `IDX_3420D9FAB0524E01` (`sale_type_id`),
  CONSTRAINT `FK_3420D9FA61220EA6` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`id`),
  CONSTRAINT `FK_3420D9FAB0524E01` FOREIGN KEY (`sale_type_id`) REFERENCES `mtb_sale_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dtb_delivery` WRITE;
/*!40000 ALTER TABLE `dtb_delivery` DISABLE KEYS */;

INSERT INTO `dtb_delivery` (`id`, `creator_id`, `sale_type_id`, `name`, `service_name`, `description`, `confirm_url`, `sort_no`, `visible`, `create_date`, `update_date`, `discriminator_type`)
VALUES
	(1,1,1,'佐川急便','佐川急便（通常購入）',NULL,'https://k2k.sagawa-exp.co.jp/p/sagawa/web/okurijoinput.jsp',2,1,'2017-03-07 10:14:52','2020-07-24 06:41:27','delivery'),
	(2,1,2,'佐川急便','佐川急便 (定期購買)',NULL,'https://k2k.sagawa-exp.co.jp/p/sagawa/web/okurijoinput.jsp',1,0,'2017-03-07 10:14:52','2020-07-24 06:42:01','delivery');

/*!40000 ALTER TABLE `dtb_delivery` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dtb_delivery_duration
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dtb_delivery_duration`;

CREATE TABLE `dtb_delivery_duration` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `duration` smallint(5) unsigned NOT NULL DEFAULT 0,
  `sort_no` int(10) unsigned NOT NULL,
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dtb_delivery_duration` WRITE;
/*!40000 ALTER TABLE `dtb_delivery_duration` DISABLE KEYS */;

INSERT INTO `dtb_delivery_duration` (`id`, `name`, `duration`, `sort_no`, `discriminator_type`)
VALUES
	(1,'即日',0,0,'deliveryduration'),
	(2,'1～2日後',1,1,'deliveryduration'),
	(3,'3～4日後',3,2,'deliveryduration'),
	(4,'1週間以降',7,3,'deliveryduration'),
	(5,'2週間以降',14,4,'deliveryduration'),
	(6,'3週間以降',21,5,'deliveryduration'),
	(7,'1ヶ月以降',30,6,'deliveryduration'),
	(8,'2ヶ月以降',60,7,'deliveryduration'),
	(9,'お取り寄せ(商品入荷後)',0,8,'deliveryduration');

/*!40000 ALTER TABLE `dtb_delivery_duration` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dtb_delivery_fee
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dtb_delivery_fee`;

CREATE TABLE `dtb_delivery_fee` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `delivery_id` int(10) unsigned DEFAULT NULL,
  `pref_id` smallint(5) unsigned DEFAULT NULL,
  `fee` decimal(12,2) unsigned NOT NULL,
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_491552412136921` (`delivery_id`),
  KEY `IDX_4915524E171EF5F` (`pref_id`),
  CONSTRAINT `FK_491552412136921` FOREIGN KEY (`delivery_id`) REFERENCES `dtb_delivery` (`id`),
  CONSTRAINT `FK_4915524E171EF5F` FOREIGN KEY (`pref_id`) REFERENCES `mtb_pref` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dtb_delivery_fee` WRITE;
/*!40000 ALTER TABLE `dtb_delivery_fee` DISABLE KEYS */;

INSERT INTO `dtb_delivery_fee` (`id`, `delivery_id`, `pref_id`, `fee`, `discriminator_type`)
VALUES
	(1,1,1,600.00,'deliveryfee'),
	(2,1,2,600.00,'deliveryfee'),
	(3,1,3,600.00,'deliveryfee'),
	(4,1,4,600.00,'deliveryfee'),
	(5,1,5,600.00,'deliveryfee'),
	(6,1,6,600.00,'deliveryfee'),
	(7,1,7,600.00,'deliveryfee'),
	(8,1,8,600.00,'deliveryfee'),
	(9,1,9,600.00,'deliveryfee'),
	(10,1,10,600.00,'deliveryfee'),
	(11,1,11,600.00,'deliveryfee'),
	(12,1,12,600.00,'deliveryfee'),
	(13,1,13,600.00,'deliveryfee'),
	(14,1,14,600.00,'deliveryfee'),
	(15,1,15,600.00,'deliveryfee'),
	(16,1,16,600.00,'deliveryfee'),
	(17,1,17,600.00,'deliveryfee'),
	(18,1,18,600.00,'deliveryfee'),
	(19,1,19,600.00,'deliveryfee'),
	(20,1,20,600.00,'deliveryfee'),
	(21,1,21,600.00,'deliveryfee'),
	(22,1,22,600.00,'deliveryfee'),
	(23,1,23,600.00,'deliveryfee'),
	(24,1,24,600.00,'deliveryfee'),
	(25,1,25,600.00,'deliveryfee'),
	(26,1,26,600.00,'deliveryfee'),
	(27,1,27,600.00,'deliveryfee'),
	(28,1,28,600.00,'deliveryfee'),
	(29,1,29,600.00,'deliveryfee'),
	(30,1,30,600.00,'deliveryfee'),
	(31,1,31,600.00,'deliveryfee'),
	(32,1,32,600.00,'deliveryfee'),
	(33,1,33,600.00,'deliveryfee'),
	(34,1,34,600.00,'deliveryfee'),
	(35,1,35,600.00,'deliveryfee'),
	(36,1,36,600.00,'deliveryfee'),
	(37,1,37,600.00,'deliveryfee'),
	(38,1,38,600.00,'deliveryfee'),
	(39,1,39,600.00,'deliveryfee'),
	(40,1,40,600.00,'deliveryfee'),
	(41,1,41,600.00,'deliveryfee'),
	(42,1,42,600.00,'deliveryfee'),
	(43,1,43,600.00,'deliveryfee'),
	(44,1,44,600.00,'deliveryfee'),
	(45,1,45,600.00,'deliveryfee'),
	(46,1,46,600.00,'deliveryfee'),
	(47,1,47,600.00,'deliveryfee'),
	(48,2,1,600.00,'deliveryfee'),
	(49,2,2,600.00,'deliveryfee'),
	(50,2,3,600.00,'deliveryfee'),
	(51,2,4,600.00,'deliveryfee'),
	(52,2,5,600.00,'deliveryfee'),
	(53,2,6,600.00,'deliveryfee'),
	(54,2,7,600.00,'deliveryfee'),
	(55,2,8,600.00,'deliveryfee'),
	(56,2,9,600.00,'deliveryfee'),
	(57,2,10,600.00,'deliveryfee'),
	(58,2,11,600.00,'deliveryfee'),
	(59,2,12,600.00,'deliveryfee'),
	(60,2,13,600.00,'deliveryfee'),
	(61,2,14,600.00,'deliveryfee'),
	(62,2,15,600.00,'deliveryfee'),
	(63,2,16,600.00,'deliveryfee'),
	(64,2,17,600.00,'deliveryfee'),
	(65,2,18,600.00,'deliveryfee'),
	(66,2,19,600.00,'deliveryfee'),
	(67,2,20,600.00,'deliveryfee'),
	(68,2,21,600.00,'deliveryfee'),
	(69,2,22,600.00,'deliveryfee'),
	(70,2,23,600.00,'deliveryfee'),
	(71,2,24,600.00,'deliveryfee'),
	(72,2,25,600.00,'deliveryfee'),
	(73,2,26,600.00,'deliveryfee'),
	(74,2,27,600.00,'deliveryfee'),
	(75,2,28,600.00,'deliveryfee'),
	(76,2,29,600.00,'deliveryfee'),
	(77,2,30,600.00,'deliveryfee'),
	(78,2,31,600.00,'deliveryfee'),
	(79,2,32,600.00,'deliveryfee'),
	(80,2,33,600.00,'deliveryfee'),
	(81,2,34,600.00,'deliveryfee'),
	(82,2,35,600.00,'deliveryfee'),
	(83,2,36,600.00,'deliveryfee'),
	(84,2,37,600.00,'deliveryfee'),
	(85,2,38,600.00,'deliveryfee'),
	(86,2,39,600.00,'deliveryfee'),
	(87,2,40,600.00,'deliveryfee'),
	(88,2,41,600.00,'deliveryfee'),
	(89,2,42,600.00,'deliveryfee'),
	(90,2,43,600.00,'deliveryfee'),
	(91,2,44,600.00,'deliveryfee'),
	(92,2,45,600.00,'deliveryfee'),
	(93,2,46,600.00,'deliveryfee'),
	(94,2,47,600.00,'deliveryfee');

/*!40000 ALTER TABLE `dtb_delivery_fee` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dtb_delivery_time
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dtb_delivery_time`;

CREATE TABLE `dtb_delivery_time` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `delivery_id` int(10) unsigned DEFAULT NULL,
  `delivery_time` varchar(255) NOT NULL,
  `sort_no` smallint(5) unsigned NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT 1,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E80EE3A612136921` (`delivery_id`),
  CONSTRAINT `FK_E80EE3A612136921` FOREIGN KEY (`delivery_id`) REFERENCES `dtb_delivery` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dtb_delivery_time` WRITE;
/*!40000 ALTER TABLE `dtb_delivery_time` DISABLE KEYS */;

INSERT INTO `dtb_delivery_time` (`id`, `delivery_id`, `delivery_time`, `sort_no`, `visible`, `create_date`, `update_date`, `discriminator_type`)
VALUES
	(3,2,'午前',1,0,'2020-05-15 06:19:49','2020-07-24 06:42:01','deliverytime'),
	(4,2,'午後',2,0,'2020-05-15 06:19:49','2020-07-24 06:42:01','deliverytime'),
	(5,1,'午前中（8時～12時）',1,1,'2020-07-21 08:35:06','2020-08-30 07:38:07','deliverytime'),
	(6,1,'12時～14時',2,1,'2020-07-21 08:35:06','2020-08-30 07:38:07','deliverytime'),
	(7,1,'14時～16時',3,1,'2020-07-21 08:35:06','2020-08-30 07:38:07','deliverytime'),
	(8,1,'16時～18時',4,1,'2020-07-21 08:35:06','2020-08-30 07:38:07','deliverytime'),
	(9,1,'18時～21時',5,1,'2020-07-21 08:35:06','2020-08-30 07:38:07','deliverytime'),
	(10,1,'19時～21時',6,1,'2020-07-21 08:35:06','2020-08-30 07:38:07','deliverytime');

/*!40000 ALTER TABLE `dtb_delivery_time` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dtb_layout
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dtb_layout`;

CREATE TABLE `dtb_layout` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `device_type_id` smallint(5) unsigned DEFAULT NULL,
  `layout_name` varchar(255) DEFAULT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_5A62AA7C4FFA550E` (`device_type_id`),
  CONSTRAINT `FK_5A62AA7C4FFA550E` FOREIGN KEY (`device_type_id`) REFERENCES `mtb_device_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dtb_layout` WRITE;
/*!40000 ALTER TABLE `dtb_layout` DISABLE KEYS */;

INSERT INTO `dtb_layout` (`id`, `device_type_id`, `layout_name`, `create_date`, `update_date`, `discriminator_type`)
VALUES
	(0,10,'トップページ用レイアウト','2017-03-07 10:14:52','2020-04-26 06:48:24','layout'),
	(1,10,'トップページ用レイアウト','2017-03-07 10:14:52','2017-03-07 10:14:52','layout'),
	(2,10,'下層ページ用レイアウト','2017-03-07 10:14:52','2017-03-07 10:14:52','layout'),
	(3,10,'商品一覧レイアウト','2020-04-29 00:44:53','2020-04-29 00:44:53','layout'),
	(4,10,'商品詳細レイアウト','2020-05-13 09:30:15','2020-05-13 09:30:15','layout'),
	(5,10,'代理店レイアウト','2020-05-16 11:50:24','2020-05-23 09:14:24','layout'),
	(6,10,'Japanese LP','2020-08-21 00:30:23','2020-08-21 00:30:23','layout');

/*!40000 ALTER TABLE `dtb_layout` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dtb_mail_history
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dtb_mail_history`;

CREATE TABLE `dtb_mail_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned DEFAULT NULL,
  `creator_id` int(10) unsigned DEFAULT NULL,
  `send_date` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `mail_subject` varchar(255) DEFAULT NULL,
  `mail_body` longtext DEFAULT NULL,
  `mail_html_body` longtext DEFAULT NULL,
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_4870AB118D9F6D38` (`order_id`),
  KEY `IDX_4870AB1161220EA6` (`creator_id`),
  CONSTRAINT `FK_4870AB1161220EA6` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`id`),
  CONSTRAINT `FK_4870AB118D9F6D38` FOREIGN KEY (`order_id`) REFERENCES `dtb_order` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dtb_mail_history` WRITE;
/*!40000 ALTER TABLE `dtb_mail_history` DISABLE KEYS */;

INSERT INTO `dtb_mail_history` (`id`, `order_id`, `creator_id`, `send_date`, `mail_subject`, `mail_body`, `mail_html_body`, `discriminator_type`)
VALUES
	(1,48,NULL,'2020-06-17 11:17:58','[MARITIME] ご注文ありがとうございます','Frederic Vervaecke 様\n\nこの度はご注文いただき誠にありがとうございます。下記ご注文内容にお間違えがないかご確認下さい。\n\n************************************************\n　ご請求金額\n************************************************\n\nご注文日時：2020/06/17 20:12:30\nご注文番号：48\nお支払い合計：￥9,668\nお支払い方法：PayPal決済\nご利用ポイント：500 pt\nお問い合わせ：test\n\n\n************************************************\n　ご注文商品明細\n************************************************\n\n商品コード：cbd-water-500\n商品名：MARITIME CBD ウォーター 500ml  通常購入  \n単価：￥1,096\n数量：1\n\n商品コード：cbd-100\n商品名：MARITIME CBDオイル 10ml (1%)  通常購入  \n単価：￥9,072\n数量：2\n\n\n-------------------------------------------------\n小　計：￥10,168\n\n手数料：￥0\n送　料：￥0\n値引き：-￥500\n============================================\n合　計：￥9,668\n\n************************************************\n　ご注文者情報\n************************************************\nお名前：Frederic Vervaecke 様\nお名前(カナ)：フレデリック ヴェルヴァーク 様\n郵便番号：〒1570077\n住所：東京都世田谷区鎌田123建物\n電話番号：0123456789\nメールアドレス：djfre2000@hotmail.com\n\n************************************************\n　配送情報\n************************************************\n\n◎お届け先\nお名前：Frederic Vervaecke 様\nお名前(カナ)：フレデリック ヴェルヴァーク 様\n郵便番号：〒1570077\n住所：東京都世田谷区鎌田123建物\n電話番号：0123456789\n\n配送方法：スタンダード宅配\nお届け日：指定なし\nお届け時間：指定なし\n\n商品コード：cbd-water-500\n商品名：MARITIME CBD ウォーター 500ml  通常購入  \n数量：1\n\n商品コード：cbd-100\n商品名：MARITIME CBDオイル 10ml (1%)  通常購入  \n数量：2\n\n\n\n============================================\n\nこのメッセージはお客様へのお知らせ専用ですので、\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。\nご了承ください。\n','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"title\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">この度はご注文いただき誠にありがとうございます。</div>\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            下記ご注文内容にお間違えがないかご確認下さい。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご請求金額<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご注文日時：2020/06/17 20:12:30<br/>\n                            ご注文番号：48<br/>\n                            お支払い合計：￥9,668<br/>\n                            お支払い方法：PayPal決済<br/>\n                                                        ご利用ポイント：500 pt<br/>\n                                                        お問い合わせ：test<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご注文商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                            商品コード：cbd-water-500<br/>\n                                商品名：MARITIME CBD ウォーター 500ml  通常購入  <br/>\n                                単価：￥1,096<br/>\n                                数量：1<br/>\n                                <br/>\n                                                            商品コード：cbd-100<br/>\n                                商品名：MARITIME CBDオイル 10ml (1%)  通常購入  <br/>\n                                単価：￥9,072<br/>\n                                数量：2<br/>\n                                <br/>\n                                                        <hr style=\"border-top: 2px dashed #8c8b8b;\">\n                            小　計：￥10,168<br/>\n                            <br/>\n                            手数料：￥0<br/>\n                            送　料：￥0<br/>\n                                                            値引き：-￥500<br/>\n                                                        <hr style=\"border-top: 1px dotted #8c8b8b;\">\n                            合　計：￥9,668<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご注文者情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            お名前：Frederic Vervaecke 様<br/>\n                            お名前(カナ)：フレデリック ヴェルヴァーク 様<br/>\n                                                        郵便番号：〒1570077<br/>\n                            住所：東京都世田谷区鎌田123建物<br/>\n                            電話番号：0123456789<br/>\n                            メールアドレス：djfre2000@hotmail.com<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　配送情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n\n                                                            ◎お届け先<br/>\n                                <br/>\n                                お名前：Frederic Vervaecke 様<br/>\n                                お名前(カナ)：フレデリック ヴェルヴァーク 様<br/>\n                                                                郵便番号：〒1570077<br/>\n                                住所：東京都世田谷区鎌田123建物<br/>\n                                電話番号：0123456789<br/>\n                                <br/>\n                                配送方法：スタンダード宅配<br/>\n                                お届け日：指定なし<br/>\n                                お届け時間：指定なし<br/>\n                                <br/>\n                                                                    商品コード：cbd-water-500<br/>\n                                    商品名：MARITIME CBD ウォーター 500ml  通常購入  <br/>\n                                    数量：1<br/>\n                                    <br/>\n                                                                    商品コード：cbd-100<br/>\n                                    商品名：MARITIME CBDオイル 10ml (1%)  通常購入  <br/>\n                                    数量：2<br/>\n                                    <br/>\n                                                                                                                    <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br/>\n                            このメッセージへの返信としてご質問をお送りいただいても回答できません。<br/>\n                            ご了承ください。<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n','mailhistory'),
	(2,48,1,'2020-06-17 11:31:29','[MARITIME] ご注文ありがとうございます','Frederic Vervaecke 様\r\n\r\nこの度はご注文いただき誠にありがとうございます。下記ご注文内容にお間違えがないかご確認下さい。\r\n\r\n************************************************\r\n　ご請求金額\r\n************************************************\r\n\r\nご注文日時：2020/06/17 20:12:30\r\nご注文番号：48\r\nお支払い合計：￥9,668\r\nお支払い方法：PayPal決済\r\nご利用ポイント：500 pt\r\nお問い合わせ：test\r\n\r\n\r\n************************************************\r\n　ご注文商品明細\r\n************************************************\r\n\r\n商品コード：cbd-water-500\r\n商品名：MARITIME CBD ウォーター 500ml  通常購入  \r\n単価：￥1,096\r\n数量：1\r\n\r\n商品コード：cbd-100\r\n商品名：MARITIME CBDオイル 10ml (1%)  通常購入  \r\n単価：￥9,072\r\n数量：2\r\n\r\n\r\n-------------------------------------------------\r\n小　計：￥10,168\r\n\r\n手数料：￥0\r\n送　料：￥0\r\n値引き：-￥500\r\n============================================\r\n合　計：￥9,668\r\n\r\n************************************************\r\n　ご注文者情報\r\n************************************************\r\nお名前：Frederic Vervaecke 様\r\nお名前(カナ)：フレデリック ヴェルヴァーク 様\r\n郵便番号：〒1570077\r\n住所：東京都世田谷区鎌田123建物\r\n電話番号：0123456789\r\nメールアドレス：djfre2000@hotmail.com\r\n\r\n************************************************\r\n　配送情報\r\n************************************************\r\n\r\n◎お届け先\r\nお名前：Frederic Vervaecke 様\r\nお名前(カナ)：フレデリック ヴェルヴァーク 様\r\n郵便番号：〒1570077\r\n住所：東京都世田谷区鎌田123建物\r\n電話番号：0123456789\r\n\r\n配送方法：スタンダード宅配\r\nお届け日：指定なし\r\nお届け時間：指定なし\r\n\r\n商品コード：cbd-water-500\r\n商品名：MARITIME CBD ウォーター 500ml  通常購入  \r\n数量：1\r\n\r\n商品コード：cbd-100\r\n商品名：MARITIME CBDオイル 10ml (1%)  通常購入  \r\n数量：2\r\n\r\n\r\n\r\n============================================\r\n\r\nこのメッセージはお客様へのお知らせ専用ですので、\r\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。\r\nご了承ください。',NULL,'mailhistory'),
	(3,49,NULL,'2020-06-18 10:38:42','[MARITIME] ご注文ありがとうございます','Frederic Vervaecke 様\n\nこの度はご注文いただき誠にありがとうございます。下記ご注文内容にお間違えがないかご確認下さい。\n\n************************************************\n　ご請求金額\n************************************************\n\nご注文日時：2020/06/17 20:45:49\nご注文番号：49\nお支払い合計：￥1,740\nお支払い方法：PayPal決済\nご利用ポイント：0 pt\nお問い合わせ：\n\n\n************************************************\n　ご注文商品明細\n************************************************\n\n商品コード：cbd-water-500-s\n商品名：MARITIME CBD ウォーター 500ml  定期購買  \n単価：￥940\n数量：1\n\n\n-------------------------------------------------\n小　計：￥940\n\n手数料：￥0\n送　料：￥800\n============================================\n合　計：￥1,740\n\n************************************************\n　ご注文者情報\n************************************************\nお名前：Frederic Vervaecke 様\nお名前(カナ)：フレデリック ヴェルヴァーク 様\n郵便番号：〒1570077\n住所：東京都世田谷区鎌田123建物\n電話番号：0123456789\nメールアドレス：djfre2000@hotmail.com\n\n************************************************\n　配送情報\n************************************************\n\n◎お届け先\nお名前：Frederic Vervaecke 様\nお名前(カナ)：フレデリック ヴェルヴァーク 様\n郵便番号：〒1570077\n住所：東京都世田谷区鎌田123建物\n電話番号：0123456789\n\n配送方法：スタンダード宅配\nお届け日：指定なし\nお届け時間：指定なし\n\n商品コード：cbd-water-500-s\n商品名：MARITIME CBD ウォーター 500ml  定期購買  \n数量：1\n\n\n\n============================================\n\nこのメッセージはお客様へのお知らせ専用ですので、\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。\nご了承ください。\n','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"title\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">この度はご注文いただき誠にありがとうございます。</div>\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            下記ご注文内容にお間違えがないかご確認下さい。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご請求金額<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご注文日時：2020/06/17 20:45:49<br/>\n                            ご注文番号：49<br/>\n                            お支払い合計：￥1,740<br/>\n                            お支払い方法：PayPal決済<br/>\n                                                        ご利用ポイント：0 pt<br/>\n                                                        お問い合わせ：<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご注文商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                            商品コード：cbd-water-500-s<br/>\n                                商品名：MARITIME CBD ウォーター 500ml  定期購買  <br/>\n                                単価：￥940<br/>\n                                数量：1<br/>\n                                <br/>\n                                                        <hr style=\"border-top: 2px dashed #8c8b8b;\">\n                            小　計：￥940<br/>\n                            <br/>\n                            手数料：￥0<br/>\n                            送　料：￥800<br/>\n                                                        <hr style=\"border-top: 1px dotted #8c8b8b;\">\n                            合　計：￥1,740<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご注文者情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            お名前：Frederic Vervaecke 様<br/>\n                            お名前(カナ)：フレデリック ヴェルヴァーク 様<br/>\n                                                        郵便番号：〒1570077<br/>\n                            住所：東京都世田谷区鎌田123建物<br/>\n                            電話番号：0123456789<br/>\n                            メールアドレス：djfre2000@hotmail.com<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　配送情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n\n                                                            ◎お届け先<br/>\n                                <br/>\n                                お名前：Frederic Vervaecke 様<br/>\n                                お名前(カナ)：フレデリック ヴェルヴァーク 様<br/>\n                                                                郵便番号：〒1570077<br/>\n                                住所：東京都世田谷区鎌田123建物<br/>\n                                電話番号：0123456789<br/>\n                                <br/>\n                                配送方法：スタンダード宅配<br/>\n                                お届け日：指定なし<br/>\n                                お届け時間：指定なし<br/>\n                                <br/>\n                                                                    商品コード：cbd-water-500-s<br/>\n                                    商品名：MARITIME CBD ウォーター 500ml  定期購買  <br/>\n                                    数量：1<br/>\n                                    <br/>\n                                                                                                                    <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br/>\n                            このメッセージへの返信としてご質問をお送りいただいても回答できません。<br/>\n                            ご了承ください。<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n','mailhistory'),
	(4,54,NULL,'2020-06-26 09:16:11','[MARITIME] ご注文ありがとうございます','杉森 浩一郎 様\n\nこの度はご注文いただき誠にありがとうございます。下記ご注文内容にお間違えがないかご確認下さい。\n\n************************************************\n　ご請求金額\n************************************************\n\nご注文日時：2020/06/26 18:12:42\nご注文番号：54\nお支払い合計：￥15,659\nお支払い方法：PayPal決済\nご利用ポイント：0 pt\nお問い合わせ：杉森からの本番オーダーです。よろしくお願いします。\n\n\n************************************************\n　ご注文商品明細\n************************************************\n\n商品コード：cbd-600\n商品名：MARITIME CBDオイル 30ml (2%)  通常購入  \n単価：￥14,531\n数量：1\n\n商品コード：cbd-water-500\n商品名：MARITIME CBD ウォーター 500ml  通常購入  \n単価：￥1,128\n数量：1\n\n\n-------------------------------------------------\n小　計：￥15,659\n\n手数料：￥0\n送　料：￥0\n============================================\n合　計：￥15,659\n\n************************************************\n　ご注文者情報\n************************************************\nお名前：杉森 浩一郎 様\nお名前(カナ)：スギモリ コウイチロウ 様\n郵便番号：〒1520004\n住所：東京都目黒区鷹番1-8-17\n電話番号：0337128235\nメールアドレス：ko@thepmi.net\n\n************************************************\n　配送情報\n************************************************\n\n◎お届け先\nお名前：小野 冨美枝 様\nお名前(カナ)：オノ フミエ 様\n郵便番号：〒1520004\n住所：東京都目黒区鷹番1-8-17\n電話番号：0337128235\n\n配送方法：スタンダード宅配\nお届け日：指定なし\nお届け時間：指定なし\n\n商品コード：cbd-600\n商品名：MARITIME CBDオイル 30ml (2%)  通常購入  \n数量：1\n\n商品コード：cbd-water-500\n商品名：MARITIME CBD ウォーター 500ml  通常購入  \n数量：1\n\n\n\n============================================\n\nこのメッセージはお客様へのお知らせ専用ですので、\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。\nご了承ください。\n','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"title\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">この度はご注文いただき誠にありがとうございます。</div>\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            杉森 浩一郎 様<br/>\n                            <br/>\n                            下記ご注文内容にお間違えがないかご確認下さい。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご請求金額<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご注文日時：2020/06/26 18:12:42<br/>\n                            ご注文番号：54<br/>\n                            お支払い合計：￥15,659<br/>\n                            お支払い方法：PayPal決済<br/>\n                                                        ご利用ポイント：0 pt<br/>\n                                                        お問い合わせ：杉森からの本番オーダーです。よろしくお願いします。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご注文商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                            商品コード：cbd-600<br/>\n                                商品名：MARITIME CBDオイル 30ml (2%)  通常購入  <br/>\n                                単価：￥14,531<br/>\n                                数量：1<br/>\n                                <br/>\n                                                            商品コード：cbd-water-500<br/>\n                                商品名：MARITIME CBD ウォーター 500ml  通常購入  <br/>\n                                単価：￥1,128<br/>\n                                数量：1<br/>\n                                <br/>\n                                                        <hr style=\"border-top: 2px dashed #8c8b8b;\">\n                            小　計：￥15,659<br/>\n                            <br/>\n                            手数料：￥0<br/>\n                            送　料：￥0<br/>\n                                                        <hr style=\"border-top: 1px dotted #8c8b8b;\">\n                            合　計：￥15,659<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご注文者情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            お名前：杉森 浩一郎 様<br/>\n                            お名前(カナ)：スギモリ コウイチロウ 様<br/>\n                                                        郵便番号：〒1520004<br/>\n                            住所：東京都目黒区鷹番1-8-17<br/>\n                            電話番号：0337128235<br/>\n                            メールアドレス：ko@thepmi.net<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　配送情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n\n                                                            ◎お届け先<br/>\n                                <br/>\n                                お名前：小野 冨美枝 様<br/>\n                                お名前(カナ)：オノ フミエ 様<br/>\n                                                                郵便番号：〒1520004<br/>\n                                住所：東京都目黒区鷹番1-8-17<br/>\n                                電話番号：0337128235<br/>\n                                <br/>\n                                配送方法：スタンダード宅配<br/>\n                                お届け日：指定なし<br/>\n                                お届け時間：指定なし<br/>\n                                <br/>\n                                                                    商品コード：cbd-600<br/>\n                                    商品名：MARITIME CBDオイル 30ml (2%)  通常購入  <br/>\n                                    数量：1<br/>\n                                    <br/>\n                                                                    商品コード：cbd-water-500<br/>\n                                    商品名：MARITIME CBD ウォーター 500ml  通常購入  <br/>\n                                    数量：1<br/>\n                                    <br/>\n                                                                                                                    <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br/>\n                            このメッセージへの返信としてご質問をお送りいただいても回答できません。<br/>\n                            ご了承ください。<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n','mailhistory'),
	(5,67,NULL,'2020-07-23 16:12:25','[MARITIME] ご注文ありがとうございます','杉森 浩一郎 様\n\nこの度はご注文いただき誠にありがとうございます。下記ご注文内容にお間違いがないかご確認下さい。\n\n************************************************\n　ご請求金額\n************************************************\n\nご注文日時：2020/07/24 1:10:05\nご注文番号：67\nお支払い合計：￥10,459\nお支払い方法：PayPal決済\nご利用ポイント：0 pt\nお問い合わせ：\n\n\n************************************************\n　ご注文商品明細\n************************************************\n\n商品コード：cbd-300-10\n商品名：MARITIME CBDオイル 10ml (3%)  通常購入  \n単価：￥9,331\n数量：1\n\n商品コード：cbd-water-500\n商品名：MARITIME CBD ウォーター 500ml  通常購入  \n単価：￥1,128\n数量：1\n\n\n-------------------------------------------------\n小　計：￥10,459\n\n手数料：￥0\n送　料：￥0\n============================================\n合　計：￥10,459\n\n************************************************\n　ご注文者情報\n************************************************\nお名前：杉森 浩一郎 様\nお名前(カナ)：スギモリ コウイチロウ 様\n郵便番号：〒1520004\n住所：東京都目黒区鷹番1-8-17\n電話番号：0337128235\nメールアドレス：ko@thepmi.net\n\n************************************************\n　配送情報\n************************************************\n\n◎お届け先\nお名前：杉森 勝代 様\nお名前(カナ)：スギモリ カツヨ 様\n郵便番号：〒1520004\n住所：東京都目黒区鷹番1-8-17\n電話番号：0337128235\n\n配送方法：スタンダード宅配\nお届け日：指定なし\nお届け時間：指定なし\n\n商品コード：cbd-300-10\n商品名：MARITIME CBDオイル 10ml (3%)  通常購入  \n数量：1\n\n商品コード：cbd-water-500\n商品名：MARITIME CBD ウォーター 500ml  通常購入  \n数量：1\n\n\n\n============================================\n\nこのメッセージはお客様へのお知らせ専用ですので、\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。\nご了承ください。\n','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"title\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">この度はご注文いただき誠にありがとうございます。</div>\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            杉森 浩一郎 様<br/>\n                            <br/>\n                            下記ご注文内容にお間違いがないかご確認下さい。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご請求金額<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご注文日時：2020/07/24 1:10:05<br/>\n                            ご注文番号：67<br/>\n                            お支払い合計：￥10,459<br/>\n                            お支払い方法：PayPal決済<br/>\n                                                        ご利用ポイント：0 pt<br/>\n                                                        お問い合わせ：<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご注文商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                            商品コード：cbd-300-10<br/>\n                                商品名：MARITIME CBDオイル 10ml (3%)  通常購入  <br/>\n                                単価：￥9,331<br/>\n                                数量：1<br/>\n                                <br/>\n                                                            商品コード：cbd-water-500<br/>\n                                商品名：MARITIME CBD ウォーター 500ml  通常購入  <br/>\n                                単価：￥1,128<br/>\n                                数量：1<br/>\n                                <br/>\n                                                        <hr style=\"border-top: 2px dashed #8c8b8b;\">\n                            小　計：￥10,459<br/>\n                            <br/>\n                            手数料：￥0<br/>\n                            送　料：￥0<br/>\n                                                        <hr style=\"border-top: 1px dotted #8c8b8b;\">\n                            合　計：￥10,459<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご注文者情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            お名前：杉森 浩一郎 様<br/>\n                            お名前(カナ)：スギモリ コウイチロウ 様<br/>\n                                                        郵便番号：〒1520004<br/>\n                            住所：東京都目黒区鷹番1-8-17<br/>\n                            電話番号：0337128235<br/>\n                            メールアドレス：ko@thepmi.net<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　配送情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n\n                                                            ◎お届け先<br/>\n                                <br/>\n                                お名前：杉森 勝代 様<br/>\n                                お名前(カナ)：スギモリ カツヨ 様<br/>\n                                                                郵便番号：〒1520004<br/>\n                                住所：東京都目黒区鷹番1-8-17<br/>\n                                電話番号：0337128235<br/>\n                                <br/>\n                                配送方法：スタンダード宅配<br/>\n                                お届け日：指定なし<br/>\n                                お届け時間：指定なし<br/>\n                                <br/>\n                                                                    商品コード：cbd-300-10<br/>\n                                    商品名：MARITIME CBDオイル 10ml (3%)  通常購入  <br/>\n                                    数量：1<br/>\n                                    <br/>\n                                                                    商品コード：cbd-water-500<br/>\n                                    商品名：MARITIME CBD ウォーター 500ml  通常購入  <br/>\n                                    数量：1<br/>\n                                    <br/>\n                                                                                                                    <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br/>\n                            このメッセージへの返信としてご質問をお送りいただいても回答できません。<br/>\n                            ご了承ください。<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n','mailhistory'),
	(6,67,3,'2020-07-24 03:22:03','[MARITIME] 商品出荷のお知らせ','杉森 勝代 様\n\nお客さまがご注文された以下の商品を発送いたしました。商品の到着まで、今しばらくお待ちください。\n\nお問い合わせ番号：403921650193\n\n************************************************\n　ご注文商品明細\n************************************************\n\n商品コード：cbd-300-10\n商品名：MARITIME CBDオイル 10ml (3%)  通常購入  \n数量：1\n\n商品コード：cbd-water-500\n商品名：MARITIME CBD ウォーター 500ml  通常購入  \n数量：1\n\n\n============================================\n\n************************************************\n　ご注文者情報\n************************************************\nお名前：杉森 浩一郎 様\nお名前(カナ)：スギモリ コウイチロウ 様\n郵便番号：〒1520004\n住所：東京都目黒区鷹番1-8-17\n電話番号：0337128235\n\n************************************************\n　配送情報\n************************************************\n\nお名前：杉森 勝代 様\nお名前(カナ)：スギモリ カツヨ 様\n郵便番号：〒1520004\n住所：東京都目黒区鷹番1-8-17\n電話番号：0337128235\n\nお届け日：指定なし\nお届け時間：指定なし\n','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"title\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:600;color:#374550;\">商品を発送いたしました。</div>\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            杉森 勝代 様<br>\n                            <br>\n                            MARITIMEでございます。<br/>\n                            お客さまがご注文された以下の商品を発送いたしました。商品の到着まで、今しばらくお待ちください。<br/>\n                            <br/>\n                                                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                お問い合わせ番号：403921650193\n                                                                <br/>\n                                                        <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご注文商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                            商品コード：cbd-300-10<br/>\n                                商品名：MARITIME CBDオイル 10ml (3%)  通常購入  <br/>\n                                数量：1<br/>\n                                <br/>\n                                                            商品コード：cbd-water-500<br/>\n                                商品名：MARITIME CBD ウォーター 500ml  通常購入  <br/>\n                                数量：1<br/>\n                                <br/>\n                                                        <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご注文者情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            お名前：杉森 浩一郎 様<br/>\n                            お名前(カナ)：スギモリ コウイチロウ 様<br/>\n                                                        郵便番号：〒1520004<br/>\n                            住所：東京都目黒区鷹番1-8-17<br/>\n                            電話番号：0337128235<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　配送情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            お名前：杉森 勝代 様<br/>\n                            お名前(カナ)：スギモリ カツヨ 様<br/>\n                                                        郵便番号：〒1520004<br/>\n                            住所：東京都目黒区鷹番1-8-17<br/>\n                            電話番号：0337128235<br/>\n                            <br/>\n                            お届け日：指定なし<br/>\n                            お届け時間：指定なし<br/>\n                            <br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n\n','mailhistory'),
	(7,67,3,'2020-07-24 03:22:42','[MARITIME] 商品出荷のお知らせ','杉森 勝代 様\n\nお客さまがご注文された以下の商品を発送いたしました。商品の到着まで、今しばらくお待ちください。\n\nお問い合わせ番号：403921650193\n\n************************************************\n　ご注文商品明細\n************************************************\n\n商品コード：cbd-300-10\n商品名：MARITIME CBDオイル 10ml (3%)  通常購入  \n数量：1\n\n商品コード：cbd-water-500\n商品名：MARITIME CBD ウォーター 500ml  通常購入  \n数量：1\n\n\n============================================\n\n************************************************\n　ご注文者情報\n************************************************\nお名前：杉森 浩一郎 様\nお名前(カナ)：スギモリ コウイチロウ 様\n郵便番号：〒1520004\n住所：東京都目黒区鷹番1-8-17\n電話番号：0337128235\n\n************************************************\n　配送情報\n************************************************\n\nお名前：杉森 勝代 様\nお名前(カナ)：スギモリ カツヨ 様\n郵便番号：〒1520004\n住所：東京都目黒区鷹番1-8-17\n電話番号：0337128235\n\nお届け日：指定なし\nお届け時間：指定なし\n','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"title\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:600;color:#374550;\">商品を発送いたしました。</div>\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            杉森 勝代 様<br>\n                            <br>\n                            MARITIMEでございます。<br/>\n                            お客さまがご注文された以下の商品を発送いたしました。商品の到着まで、今しばらくお待ちください。<br/>\n                            <br/>\n                                                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                お問い合わせ番号：403921650193\n                                                                <br/>\n                                                        <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご注文商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                            商品コード：cbd-300-10<br/>\n                                商品名：MARITIME CBDオイル 10ml (3%)  通常購入  <br/>\n                                数量：1<br/>\n                                <br/>\n                                                            商品コード：cbd-water-500<br/>\n                                商品名：MARITIME CBD ウォーター 500ml  通常購入  <br/>\n                                数量：1<br/>\n                                <br/>\n                                                        <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご注文者情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            お名前：杉森 浩一郎 様<br/>\n                            お名前(カナ)：スギモリ コウイチロウ 様<br/>\n                                                        郵便番号：〒1520004<br/>\n                            住所：東京都目黒区鷹番1-8-17<br/>\n                            電話番号：0337128235<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　配送情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            お名前：杉森 勝代 様<br/>\n                            お名前(カナ)：スギモリ カツヨ 様<br/>\n                                                        郵便番号：〒1520004<br/>\n                            住所：東京都目黒区鷹番1-8-17<br/>\n                            電話番号：0337128235<br/>\n                            <br/>\n                            お届け日：指定なし<br/>\n                            お届け時間：指定なし<br/>\n                            <br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n\n','mailhistory'),
	(8,100,NULL,'2020-08-28 05:57:47','[MARITIME] ご注文ありがとうございます','Frederic Vervaecke 様\nこの度はご注文いただき誠にありがとうございます。下記ご注文内容にお間違いがないかご確認下さい。\n\n************************************************\n　ご請求金額\n************************************************\n\nご注文日時：2020/08/28 14:56:17\nご注文番号：100\nお支払い合計：￥1,728\nお支払い方法：クレジット決済\nご利用ポイント：0 pt\nお問い合わせ内容：\n\n\n************************************************\n　ご注文商品明細\n************************************************\n\n商品コード：cbd-water-500\n商品名：MARITIME CBD ウォーター 500ml  通常購入  \n単価：￥1,128\n数量：1\n\n\n-------------------------------------------------\n小　計：￥1,128\n\n手数料：￥0\n送　料：￥600\n============================================\n合　計：￥1,728\n\n************************************************\n　ご注文者情報\n************************************************\nお名前：Frederic Vervaecke 様お名前(カナ)：フレデリック ヴェルヴァーク 様郵便番号：〒2220032\n住所：神奈川県横浜市港北区大豆戸町171-1ネイバーズ菊名311\n電話番号：07026558129\nメールアドレス：djfre2000@hotmail.com\n\n************************************************\n　配送情報\n************************************************\n\n◎お届け先\nお名前：Frederic Vervaecke 様お名前(カナ)：フレデリック ヴェルヴァーク 様郵便番号：〒2220032\n住所：神奈川県横浜市港北区大豆戸町171-1ネイバーズ菊名311\n電話番号：07026558129\n\n配送方法：佐川急便\nお届け日：指定なし\nお届け時間：指定なし\n\n商品コード：cbd-water-500\n商品名：MARITIME CBD ウォーター 500ml  通常購入  \n数量：1\n\n\nクレジット決済\n承認番号: 0649950\n\n\n\n============================================\n\nこのメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n\n','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"title\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">この度はご注文いただき誠にありがとうございます。</div>\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            下記ご注文内容にお間違いがないかご確認下さい。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご請求金額<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご注文日時：2020/08/28 14:56:17<br/>\n                            ご注文番号：100<br/>\n                            お支払い合計：￥1,728<br/>\n                            お支払い方法：クレジット決済<br/>\n                                                        ご利用ポイント：0 pt<br/>\n                                                        お問い合わせ内容：<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご注文商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                            商品コード：cbd-water-500<br/>\n                                商品名：MARITIME CBD ウォーター 500ml  通常購入  <br/>\n                                単価：￥1,128<br/>\n                                数量：1<br/>\n                                <br/>\n                                                        <hr style=\"border-top: 2px dashed #8c8b8b;\">\n                            小　計：￥1,128<br/>\n                            <br/>\n                            手数料：￥0<br/>\n                            送　料：￥600<br/>\n                                                        <hr style=\"border-top: 1px dotted #8c8b8b;\">\n                            合　計：￥1,728<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご注文者情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            お名前：Frederic Vervaecke 様<br/>\n                            お名前(カナ)：フレデリック ヴェルヴァーク 様<br/>\n                                                        郵便番号：〒2220032<br/>\n                            住所：神奈川県横浜市港北区大豆戸町171-1ネイバーズ菊名311<br/>\n                            電話番号：07026558129<br/>\n                            メールアドレス：djfre2000@hotmail.com<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　配送情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n\n                                                            ◎お届け先<br/>\n                                <br/>\n                                お名前：Frederic Vervaecke 様<br/>\n                                お名前(カナ)：フレデリック ヴェルヴァーク 様<br/>\n                                                                郵便番号：〒2220032<br/>\n                                住所：神奈川県横浜市港北区大豆戸町171-1ネイバーズ菊名311<br/>\n                                電話番号：07026558129<br/>\n                                <br/>\n                                配送方法：佐川急便<br/>\n                                お届け日：指定なし<br/>\n                                お届け時間：指定なし<br/>\n                                <br/>\n                                                                    商品コード：cbd-water-500<br/>\n                                    商品名：MARITIME CBD ウォーター 500ml  通常購入  <br/>\n                                    数量：1<br/>\n                                    <br/>\n                                                                                                                        クレジット決済\n承認番号: 0649950\n\n<br/>\n                                                        <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n','mailhistory'),
	(9,102,NULL,'2020-08-28 06:17:50','[MARITIME] ご注文ありがとうございます','Frederic Vervaecke 様\nこの度はご注文いただき誠にありがとうございます。下記ご注文内容にお間違いがないかご確認下さい。\n\n************************************************\n　ご請求金額\n************************************************\n\nご注文日時：2020/08/28 15:03:54\nご注文番号：102\nお支払い合計：￥6,998\nお支払い方法：クレジット決済\nご利用ポイント：0 pt\nお問い合わせ内容：テスト注文、キャンセルしてください\n\n\n************************************************\n　ご注文商品明細\n************************************************\n\n商品コード：cbd-200\n商品名：MARITIME CBDオイル 10ml (2%)  通常購入  \n単価：￥6,998\n数量：1\n\n\n-------------------------------------------------\n小　計：￥6,998\n\n手数料：￥0\n送　料：￥0\n============================================\n合　計：￥6,998\n\n************************************************\n　ご注文者情報\n************************************************\nお名前：Frederic Vervaecke 様お名前(カナ)：フレデリック ヴェルヴァーク 様郵便番号：〒2220032\n住所：神奈川県横浜市港北区大豆戸町171-1ネイバーズ菊名311\n電話番号：07026558129\nメールアドレス：djfre2000@hotmail.com\n\n************************************************\n　配送情報\n************************************************\n\n◎お届け先\nお名前：Frederic Vervaecke 様お名前(カナ)：フレデリック ヴェルヴァーク 様郵便番号：〒2220032\n住所：神奈川県横浜市港北区大豆戸町171-1ネイバーズ菊名311\n電話番号：07026558129\n\n配送方法：佐川急便\nお届け日：指定なし\nお届け時間：指定なし\n\n商品コード：cbd-200\n商品名：MARITIME CBDオイル 10ml (2%)  通常購入  \n数量：1\n\n\nクレジット決済\n承認番号: 0651230\n\n\n\n============================================\n\nこのメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n\n','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"title\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">この度はご注文いただき誠にありがとうございます。</div>\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            下記ご注文内容にお間違いがないかご確認下さい。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご請求金額<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご注文日時：2020/08/28 15:03:54<br/>\n                            ご注文番号：102<br/>\n                            お支払い合計：￥6,998<br/>\n                            お支払い方法：クレジット決済<br/>\n                                                        ご利用ポイント：0 pt<br/>\n                                                        お問い合わせ内容：テスト注文、キャンセルしてください<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご注文商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                            商品コード：cbd-200<br/>\n                                商品名：MARITIME CBDオイル 10ml (2%)  通常購入  <br/>\n                                単価：￥6,998<br/>\n                                数量：1<br/>\n                                <br/>\n                                                        <hr style=\"border-top: 2px dashed #8c8b8b;\">\n                            小　計：￥6,998<br/>\n                            <br/>\n                            手数料：￥0<br/>\n                            送　料：￥0<br/>\n                                                        <hr style=\"border-top: 1px dotted #8c8b8b;\">\n                            合　計：￥6,998<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご注文者情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            お名前：Frederic Vervaecke 様<br/>\n                            お名前(カナ)：フレデリック ヴェルヴァーク 様<br/>\n                                                        郵便番号：〒2220032<br/>\n                            住所：神奈川県横浜市港北区大豆戸町171-1ネイバーズ菊名311<br/>\n                            電話番号：07026558129<br/>\n                            メールアドレス：djfre2000@hotmail.com<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　配送情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n\n                                                            ◎お届け先<br/>\n                                <br/>\n                                お名前：Frederic Vervaecke 様<br/>\n                                お名前(カナ)：フレデリック ヴェルヴァーク 様<br/>\n                                                                郵便番号：〒2220032<br/>\n                                住所：神奈川県横浜市港北区大豆戸町171-1ネイバーズ菊名311<br/>\n                                電話番号：07026558129<br/>\n                                <br/>\n                                配送方法：佐川急便<br/>\n                                お届け日：指定なし<br/>\n                                お届け時間：指定なし<br/>\n                                <br/>\n                                                                    商品コード：cbd-200<br/>\n                                    商品名：MARITIME CBDオイル 10ml (2%)  通常購入  <br/>\n                                    数量：1<br/>\n                                    <br/>\n                                                                                                                        クレジット決済\n承認番号: 0651230\n\n<br/>\n                                                        <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n','mailhistory'),
	(10,104,NULL,'2020-08-30 07:40:42','[MARITIME] ご注文ありがとうございます','Frederic Vervaecke 様\nこの度はご注文いただき誠にありがとうございます。下記ご注文内容にお間違いがないかご確認下さい。\n\n************************************************\n　ご請求金額\n************************************************\n\nご注文日時：2020/08/30 16:39:44\nご注文番号：104\nお支払い合計：￥4,666\nお支払い方法：銀行振込\nご利用ポイント：0 pt\nお問い合わせ内容：テスト注文！\n\n************************************************\n　銀行口座情報\n************************************************\n\n銀行名：神戸信用金庫\n支店名：中央支店\n口座番号：0500384\n預金種別：普通\n口座名義：株式会社メディファイン\n\n************************************************\n　ご注文商品明細\n************************************************\n\n商品コード：cbd-100\n商品名：MARITIME CBDオイル 10ml (1%)  通常購入  \n単価：￥4,666\n数量：1\n\n\n-------------------------------------------------\n小　計：￥4,666\n\n手数料：￥0\n送　料：￥0\n============================================\n合　計：￥4,666\n\n************************************************\n　ご注文者情報\n************************************************\nお名前：Frederic Vervaecke 様お名前(カナ)：フレデリック ヴェルヴァーク 様郵便番号：〒2220032\n住所：神奈川県横浜市港北区大豆戸町171-1ネイバーズ菊名311\n電話番号：07026558129\nメールアドレス：djfre2000@hotmail.com\n\n************************************************\n　配送情報\n************************************************\n\n◎お届け先\nお名前：Frederic Vervaecke 様お名前(カナ)：フレデリック ヴェルヴァーク 様郵便番号：〒2220032\n住所：神奈川県横浜市港北区大豆戸町171-1ネイバーズ菊名311\n電話番号：07026558129\n\n配送方法：佐川急便\nお届け日：指定なし\nお届け時間：指定なし\n\n商品コード：cbd-100\n商品名：MARITIME CBDオイル 10ml (1%)  通常購入  \n数量：1\n\n\n\n============================================\n\nこのメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n\n','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"title\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">この度はご注文いただき誠にありがとうございます。</div>\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            下記ご注文内容にお間違いがないかご確認下さい。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご請求金額<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご注文日時：2020/08/30 16:39:44<br/>\n                            ご注文番号：104<br/>\n                            お支払い合計：￥4,666<br/>\n                            お支払い方法：銀行振込<br/>\n                                                        ご利用ポイント：0 pt<br/>\n                                                        お問い合わせ内容：テスト注文！<br/>\n                            <br/>\n                                                                                    <hr style=\"border-top: 3px double #8c8b8b;\">\n                            銀行口座情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            <br/>\n                            銀行名：神戸信用金庫<br/>\n                            支店名：中央支店<br/>\n                            口座番号：0500384<br/>\n                            預金種別：普通<br/>\n                            口座名義：株式会社メディファイン<br/>\n                            <br/>\n                                                        <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご注文商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                            商品コード：cbd-100<br/>\n                                商品名：MARITIME CBDオイル 10ml (1%)  通常購入  <br/>\n                                単価：￥4,666<br/>\n                                数量：1<br/>\n                                <br/>\n                                                        <hr style=\"border-top: 2px dashed #8c8b8b;\">\n                            小　計：￥4,666<br/>\n                            <br/>\n                            手数料：￥0<br/>\n                            送　料：￥0<br/>\n                                                        <hr style=\"border-top: 1px dotted #8c8b8b;\">\n                            合　計：￥4,666<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご注文者情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            お名前：Frederic Vervaecke 様<br/>\n                            お名前(カナ)：フレデリック ヴェルヴァーク 様<br/>\n                                                        郵便番号：〒2220032<br/>\n                            住所：神奈川県横浜市港北区大豆戸町171-1ネイバーズ菊名311<br/>\n                            電話番号：07026558129<br/>\n                            メールアドレス：djfre2000@hotmail.com<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　配送情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n\n                                                            ◎お届け先<br/>\n                                <br/>\n                                お名前：Frederic Vervaecke 様<br/>\n                                お名前(カナ)：フレデリック ヴェルヴァーク 様<br/>\n                                                                郵便番号：〒2220032<br/>\n                                住所：神奈川県横浜市港北区大豆戸町171-1ネイバーズ菊名311<br/>\n                                電話番号：07026558129<br/>\n                                <br/>\n                                配送方法：佐川急便<br/>\n                                お届け日：指定なし<br/>\n                                お届け時間：指定なし<br/>\n                                <br/>\n                                                                    商品コード：cbd-100<br/>\n                                    商品名：MARITIME CBDオイル 10ml (1%)  通常購入  <br/>\n                                    数量：1<br/>\n                                    <br/>\n                                                                                                                    <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n','mailhistory'),
	(11,107,1,'2020-09-04 04:05:13','[MARITIME] カートの中の商品があります','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            ご確認をお願いいたします。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 10ml (1%)  通常購入  <br/>\n                            単価：￥4,666<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            ご確認をお願いいたします。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 10ml (1%)  通常購入  <br/>\n                            単価：￥4,666<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n','mailhistory'),
	(12,107,1,'2020-09-04 04:08:11','[MARITIME] ご注文ありがとうございます','Frederic Vervaecke 様\r\nこの度はご注文いただき誠にありがとうございます。下記ご注文内容にお間違いがないかご確認下さい。\r\n\r\n************************************************\r\n　ご請求金額\r\n************************************************\r\n\r\nご注文日時：2020/09/02 16:03:29\r\nご注文番号：107\r\nお支払い合計：￥4,666\r\nお支払い方法：クレジット決済\r\nご利用ポイント：0 pt\r\nお問い合わせ内容：\r\n\r\n************************************************\r\n　ご注文商品明細\r\n************************************************\r\n\r\n商品コード：cbd-100\r\n商品名：MARITIME CBDオイル 10ml (1%)  通常購入  \r\n単価：￥4,666\r\n数量：1\r\n\r\n\r\n-------------------------------------------------\r\n小　計：￥4,666\r\n\r\n手数料：￥0\r\n送　料：￥0\r\n============================================\r\n合　計：￥4,666\r\n\r\n************************************************\r\n　ご注文者情報\r\n************************************************\r\nお名前：Frederic Vervaecke 様お名前(カナ)：フレデリック ヴェルヴァーク 様郵便番号：〒2220032\r\n住所：神奈川県横浜市港北区大豆戸町171-1ネイバーズ菊名311\r\n電話番号：07026558129\r\nメールアドレス：djfre2000@hotmail.com\r\n\r\n************************************************\r\n　配送情報\r\n************************************************\r\n\r\n◎お届け先\r\nお名前：Frederic Vervaecke 様お名前(カナ)：フレデリック ヴェルヴァーク 様郵便番号：〒2220032\r\n住所：神奈川県横浜市港北区大豆戸町171-1ネイバーズ菊名311\r\n電話番号：07026558129\r\n\r\n配送方法：佐川急便\r\nお届け日：指定なし\r\nお届け時間：指定なし\r\n\r\n商品コード：cbd-100\r\n商品名：MARITIME CBDオイル 10ml (1%)  通常購入  \r\n数量：1\r\n\r\n\r\n\r\n============================================\r\n\r\nこのメッセージはお客様へのお知らせ専用ですので、<br />\r\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\r\nご了承ください。<br />',NULL,'mailhistory'),
	(13,104,1,'2020-09-04 04:17:33','[MARITIME] ご注文ありがとうございます','Frederic Vervaecke 様\r\nこの度はご注文いただき誠にありがとうございます。下記ご注文内容にお間違いがないかご確認下さい。\r\n\r\n************************************************\r\n　ご請求金額\r\n************************************************\r\n\r\nご注文日時：2020/08/30 16:39:44\r\nご注文番号：104\r\nお支払い合計：￥4,666\r\nお支払い方法：銀行振込\r\nご利用ポイント：0 pt\r\nお問い合わせ内容：テスト注文！\r\n\r\n************************************************\r\n　銀行口座情報\r\n************************************************\r\n\r\n銀行名：神戸信用金庫\r\n支店名：中央支店\r\n口座番号：0500384\r\n預金種別：普通\r\n口座名義：株式会社メディファイン\r\n\r\n************************************************\r\n　ご注文商品明細\r\n************************************************\r\n\r\n商品コード：cbd-100\r\n商品名：MARITIME CBDオイル 10ml (1%)  通常購入  \r\n単価：￥4,666\r\n数量：1\r\n\r\n\r\n-------------------------------------------------\r\n小　計：￥4,666\r\n\r\n手数料：￥0\r\n送　料：￥0\r\n============================================\r\n合　計：￥4,666\r\n\r\n************************************************\r\n　ご注文者情報\r\n************************************************\r\nお名前：Frederic Vervaecke 様お名前(カナ)：フレデリック ヴェルヴァーク 様郵便番号：〒2220032\r\n住所：神奈川県横浜市港北区大豆戸町171-1ネイバーズ菊名311\r\n電話番号：07026558129\r\nメールアドレス：djfre2000@hotmail.com\r\n\r\n************************************************\r\n　配送情報\r\n************************************************\r\n\r\n◎お届け先\r\nお名前：Frederic Vervaecke 様お名前(カナ)：フレデリック ヴェルヴァーク 様郵便番号：〒2220032\r\n住所：神奈川県横浜市港北区大豆戸町171-1ネイバーズ菊名311\r\n電話番号：07026558129\r\n\r\n配送方法：佐川急便\r\nお届け日：指定なし\r\nお届け時間：指定なし\r\n\r\n商品コード：cbd-100\r\n商品名：MARITIME CBDオイル 10ml (1%)  通常購入  \r\n数量：1\r\n\r\n\r\n\r\n============================================\r\n\r\nこのメッセージはお客様へのお知らせ専用ですので、<br />\r\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\r\nご了承ください。<br />',NULL,'mailhistory'),
	(14,108,1,'2020-09-04 07:10:35','[MARITIME] カートの中の商品があります','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            ご確認をお願いいたします。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 10ml (1%)  通常購入  <br/>\n                            単価：￥4,666<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            ご確認をお願いいたします。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 10ml (1%)  通常購入  <br/>\n                            単価：￥4,666<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n','mailhistory'),
	(15,NULL,1,'2020-09-04 08:39:04','[MARITIME] カートの中の商品があります','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            ご確認をお願いいたします。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                                                                                        商品名：MARITIME CBDオイル 10ml (1%)\n                                                                    販売種別：通常購入\n                                                                <br/>\n                            単価：￥4,666<br/>\n                            数量：1<br/>\n                                <br/>\n                                \n                                                        <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            ご確認をお願いいたします。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                                                                                        商品名：MARITIME CBDオイル 10ml (1%)\n                                                                    販売種別：通常購入\n                                                                <br/>\n                            単価：￥4,666<br/>\n                            数量：1<br/>\n                                <br/>\n                                \n                                                        <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n','mailhistory'),
	(16,110,NULL,'2020-09-06 12:40:19','[MARITIME] ご注文ありがとうございます','熊澤 智義 様\nこの度はご注文いただき誠にありがとうございます。下記ご注文内容にお間違いがないかご確認下さい。\n\n************************************************\n　ご請求金額\n************************************************\n\nご注文日時：2020/09/06 21:33:06\nご注文番号：110\nお支払い合計：￥41,553\nお支払い方法：クレジット決済\nご利用ポイント：0 pt\nお問い合わせ内容：\n\n************************************************\n　ご注文商品明細\n************************************************\n\n商品コード：cbd-3000\n商品名：MARITIME CBDオイル 30ml (10%)  通常購入  \n単価：￥46,170\n数量：1\n\n\n-------------------------------------------------\n小　計：￥46,170\n\n手数料：￥0\n送　料：￥0\n値引き：-￥4,617\n============================================\n合　計：￥41,553\n\n************************************************\n　ご注文者情報\n************************************************\nお名前：熊澤 智義 様お名前(カナ)：クマザワ トモヨシ 様会社名：株式会社ウルスマイル\n郵便番号：〒2160012\n住所：神奈川県川崎市宮前区水沢2-15-24\n電話番号：09065153851\nメールアドレス：marugeri@icloud.com\n\n************************************************\n　配送情報\n************************************************\n\n◎お届け先\nお名前：鈴木 晴美 様お名前(カナ)：スズキ ハルミ 様郵便番号：〒2430431\n住所：神奈川県海老名市上今泉6-53-15\n電話番号：08067574821\n\n配送方法：佐川急便\nお届け日：指定なし\nお届け時間：指定なし\n\n商品コード：cbd-3000\n商品名：MARITIME CBDオイル 30ml (10%)  通常購入  \n数量：1\n\n\n***********************************************\n　クーポン情報                                 \n***********************************************\n\nクーポンコード: LINE10 LINE用10%割引きオファー\nクレジット決済\n承認番号: 0000061\n\n\n\n============================================\n\nこのメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n\n','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"title\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">この度はご注文いただき誠にありがとうございます。</div>\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            熊澤 智義 様<br/>\n                            <br/>\n                            下記ご注文内容にお間違いがないかご確認下さい。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご請求金額<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご注文日時：2020/09/06 21:33:06<br/>\n                            ご注文番号：110<br/>\n                            お支払い合計：￥41,553<br/>\n                            お支払い方法：クレジット決済<br/>\n                                                        ご利用ポイント：0 pt<br/>\n                                                        お問い合わせ内容：<br/>\n                            <br/>\n                                                                                    <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご注文商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                            商品コード：cbd-3000<br/>\n                                商品名：MARITIME CBDオイル 30ml (10%)  通常購入  <br/>\n                                単価：￥46,170<br/>\n                                数量：1<br/>\n                                <br/>\n                                                        <hr style=\"border-top: 2px dashed #8c8b8b;\">\n                            小　計：￥46,170<br/>\n                            <br/>\n                            手数料：￥0<br/>\n                            送　料：￥0<br/>\n                                                            値引き：-￥4,617<br/>\n                                                        <hr style=\"border-top: 1px dotted #8c8b8b;\">\n                            合　計：￥41,553<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご注文者情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            お名前：熊澤 智義 様<br/>\n                            お名前(カナ)：クマザワ トモヨシ 様<br/>\n                                                            会社名：株式会社ウルスマイル<br/>\n                                                        郵便番号：〒2160012<br/>\n                            住所：神奈川県川崎市宮前区水沢2-15-24<br/>\n                            電話番号：09065153851<br/>\n                            メールアドレス：marugeri@icloud.com<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　配送情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n\n                                                            ◎お届け先<br/>\n                                <br/>\n                                お名前：鈴木 晴美 様<br/>\n                                お名前(カナ)：スズキ ハルミ 様<br/>\n                                                                郵便番号：〒2430431<br/>\n                                住所：神奈川県海老名市上今泉6-53-15<br/>\n                                電話番号：08067574821<br/>\n                                <br/>\n                                配送方法：佐川急便<br/>\n                                お届け日：指定なし<br/>\n                                お届け時間：指定なし<br/>\n                                <br/>\n                                                                    商品コード：cbd-3000<br/>\n                                    商品名：MARITIME CBDオイル 30ml (10%)  通常購入  <br/>\n                                    数量：1<br/>\n                                    <br/>\n                                                                                                                        ***********************************************\n　クーポン情報                                 \n***********************************************\n\nクーポンコード: LINE10 LINE用10%割引きオファー\nクレジット決済\n承認番号: 0000061\n\n<br/>\n                                                        <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n','mailhistory'),
	(17,111,NULL,'2020-09-06 14:51:18','[MARITIME] ご注文ありがとうございます','関 歌織 様\nこの度はご注文いただき誠にありがとうございます。下記ご注文内容にお間違いがないかご確認下さい。\n\n************************************************\n　ご請求金額\n************************************************\n\nご注文日時：2020/09/06 23:50:30\nご注文番号：111\nお支払い合計：￥9,331\nお支払い方法：銀行振込\nご利用ポイント：0 pt\nお問い合わせ内容：\n\n************************************************\n　銀行口座情報\n************************************************\n\n銀行名：神戸信用金庫\n支店名：中央支店\n口座番号：0500384\n預金種別：普通\n口座名義：株式会社メディファイン\n\n************************************************\n　ご注文商品明細\n************************************************\n\n商品コード：cbd-300-10\n商品名：MARITIME CBDオイル 10ml (3%)  通常購入  \n単価：￥9,331\n数量：1\n\n\n-------------------------------------------------\n小　計：￥9,331\n\n手数料：￥0\n送　料：￥0\n============================================\n合　計：￥9,331\n\n************************************************\n　ご注文者情報\n************************************************\nお名前：関 歌織 様お名前(カナ)：セキ カオリ 様郵便番号：〒0630001\n住所：北海道札幌市西区山の手一条8丁目2-6\n電話番号：09062618809\nメールアドレス：kaononko@icloud.com\n\n************************************************\n　配送情報\n************************************************\n\n◎お届け先\nお名前：関 歌織 様お名前(カナ)：セキ カオリ 様郵便番号：〒0630001\n住所：北海道札幌市西区山の手一条8丁目2-6\n電話番号：09062618809\n\n配送方法：佐川急便\nお届け日：指定なし\nお届け時間：18時～21時\n\n商品コード：cbd-300-10\n商品名：MARITIME CBDオイル 10ml (3%)  通常購入  \n数量：1\n\n\n\n============================================\n\nこのメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n\n','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"title\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">この度はご注文いただき誠にありがとうございます。</div>\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            関 歌織 様<br/>\n                            <br/>\n                            下記ご注文内容にお間違いがないかご確認下さい。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご請求金額<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご注文日時：2020/09/06 23:50:30<br/>\n                            ご注文番号：111<br/>\n                            お支払い合計：￥9,331<br/>\n                            お支払い方法：銀行振込<br/>\n                                                        ご利用ポイント：0 pt<br/>\n                                                        お問い合わせ内容：<br/>\n                            <br/>\n                                                                                    <hr style=\"border-top: 3px double #8c8b8b;\">\n                            銀行口座情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            <br/>\n                            銀行名：神戸信用金庫<br/>\n                            支店名：中央支店<br/>\n                            口座番号：0500384<br/>\n                            預金種別：普通<br/>\n                            口座名義：株式会社メディファイン<br/>\n                            <br/>\n                                                        <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご注文商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                            商品コード：cbd-300-10<br/>\n                                商品名：MARITIME CBDオイル 10ml (3%)  通常購入  <br/>\n                                単価：￥9,331<br/>\n                                数量：1<br/>\n                                <br/>\n                                                        <hr style=\"border-top: 2px dashed #8c8b8b;\">\n                            小　計：￥9,331<br/>\n                            <br/>\n                            手数料：￥0<br/>\n                            送　料：￥0<br/>\n                                                        <hr style=\"border-top: 1px dotted #8c8b8b;\">\n                            合　計：￥9,331<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご注文者情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            お名前：関 歌織 様<br/>\n                            お名前(カナ)：セキ カオリ 様<br/>\n                                                        郵便番号：〒0630001<br/>\n                            住所：北海道札幌市西区山の手一条8丁目2-6<br/>\n                            電話番号：09062618809<br/>\n                            メールアドレス：kaononko@icloud.com<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　配送情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n\n                                                            ◎お届け先<br/>\n                                <br/>\n                                お名前：関 歌織 様<br/>\n                                お名前(カナ)：セキ カオリ 様<br/>\n                                                                郵便番号：〒0630001<br/>\n                                住所：北海道札幌市西区山の手一条8丁目2-6<br/>\n                                電話番号：09062618809<br/>\n                                <br/>\n                                配送方法：佐川急便<br/>\n                                お届け日：指定なし<br/>\n                                お届け時間：18時～21時<br/>\n                                <br/>\n                                                                    商品コード：cbd-300-10<br/>\n                                    商品名：MARITIME CBDオイル 10ml (3%)  通常購入  <br/>\n                                    数量：1<br/>\n                                    <br/>\n                                                                                                                    <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n','mailhistory'),
	(18,110,3,'2020-09-07 05:24:45','[MARITIME] 商品出荷のお知らせ','鈴木 晴美 様\nお客さまがご注文された以下の商品を発送いたしました。商品の到着まで、今しばらくお待ちください。\n\nお問い合わせ番号：403921650775\nお問い合わせURL：https://k2k.sagawa-exp.co.jp/p/sagawa/web/okurijoinput.jsp\n\n************************************************\n　ご注文商品明細\n************************************************\n\n商品コード：cbd-3000\n商品名：MARITIME CBDオイル 30ml (10%)  通常購入  \n数量：1\n\n\n============================================\n\n************************************************\n　ご注文者情報\n************************************************\nお名前：熊澤 智義 様お名前(カナ)：クマザワ トモヨシ 様会社名：株式会社ウルスマイル\n郵便番号：〒2160012\n住所：神奈川県川崎市宮前区水沢2-15-24\n電話番号：09065153851\n\n************************************************\n　配送情報\n************************************************\n\nお名前：鈴木 晴美 様お名前(カナ)：スズキ ハルミ 様郵便番号：〒2430431\n住所：神奈川県海老名市上今泉6-53-15\n電話番号：08067574821\n\nお届け日：指定なし\nお届け時間：指定なし\n','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"title\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:600;color:#374550;\">商品を発送いたしました。</div>\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            鈴木 晴美 様<br>\n                            <br>\n                            MARITIMEでございます。<br/>\n                            お客さまがご注文された以下の商品を発送いたしました。商品の到着まで、今しばらくお待ちください。<br/>\n                            <br/>\n                                                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                お問い合わせ番号：403921650775\n                                                                    <br/>\n                                    お問い合わせURL：https://k2k.sagawa-exp.co.jp/p/sagawa/web/okurijoinput.jsp\n                                                                <br/>\n                                                        <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご注文商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                            商品コード：cbd-3000<br/>\n                                商品名：MARITIME CBDオイル 30ml (10%)  通常購入  <br/>\n                                数量：1<br/>\n                                <br/>\n                                                        <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご注文者情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            お名前：熊澤 智義 様<br/>\n                            お名前(カナ)：クマザワ トモヨシ 様<br/>\n                                                            会社名：株式会社ウルスマイル<br/>\n                                                        郵便番号：〒2160012<br/>\n                            住所：神奈川県川崎市宮前区水沢2-15-24<br/>\n                            電話番号：09065153851<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　配送情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            お名前：鈴木 晴美 様<br/>\n                            お名前(カナ)：スズキ ハルミ 様<br/>\n                                                        郵便番号：〒2430431<br/>\n                            住所：神奈川県海老名市上今泉6-53-15<br/>\n                            電話番号：08067574821<br/>\n                            <br/>\n                            お届け日：指定なし<br/>\n                            お届け時間：指定なし<br/>\n                            <br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n\n','mailhistory'),
	(19,109,1,'2020-09-07 07:26:05','[MARITIME] カートの中の商品があります','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            下記URLにご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime-cbd.com/cart\" target=\"_blank\">https://maritime-cbd.com/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 10ml (1%)  通常購入  <br/>\n                            単価：￥4,666<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            下記URLにご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime-cbd.com/cart\" target=\"_blank\">https://maritime-cbd.com/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 10ml (1%)  通常購入  <br/>\n                            単価：￥4,666<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n','mailhistory'),
	(20,89,1,'2020-09-07 11:26:20','[MARITIME] カートの中の商品があります','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            杉森 浩一郎 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            下記URLにご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime-cbd.com/cart\" target=\"_blank\">https://maritime-cbd.com/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 10ml (2%)  通常購入  <br/>\n                            単価：￥6,998<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                            商品名：MARITIME CBDオイル 30ml (1%)  通常購入  <br/>\n                            単価：￥8,845<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            杉森 浩一郎 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            下記URLにご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime-cbd.com/cart\" target=\"_blank\">https://maritime-cbd.com/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 10ml (2%)  通常購入  <br/>\n                            単価：￥6,998<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                            商品名：MARITIME CBDオイル 30ml (1%)  通常購入  <br/>\n                            単価：￥8,845<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n','mailhistory'),
	(21,86,1,'2020-09-07 11:26:20','[MARITIME] カートの中の商品があります','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            藤永 朋久 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            下記URLにご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime-cbd.com/cart\" target=\"_blank\">https://maritime-cbd.com/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 10ml (2%)  通常購入  <br/>\n                            単価：￥6,998<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            藤永 朋久 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            下記URLにご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime-cbd.com/cart\" target=\"_blank\">https://maritime-cbd.com/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 10ml (2%)  通常購入  <br/>\n                            単価：￥6,998<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n','mailhistory'),
	(22,88,1,'2020-09-07 11:26:20','[MARITIME] カートの中の商品があります','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            上野 和子 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            下記URLにご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime-cbd.com/cart\" target=\"_blank\">https://maritime-cbd.com/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBD ウォーター 500ml  通常購入  <br/>\n                            単価：￥1,253<br/>\n                            数量：2<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            上野 和子 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            下記URLにご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime-cbd.com/cart\" target=\"_blank\">https://maritime-cbd.com/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBD ウォーター 500ml  通常購入  <br/>\n                            単価：￥1,253<br/>\n                            数量：2<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n','mailhistory'),
	(23,58,1,'2020-09-07 11:26:20','[MARITIME] カートの中の商品があります','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            永井 光弘 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            下記URLにご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime-cbd.com/cart\" target=\"_blank\">https://maritime-cbd.com/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 10ml (2%)  通常購入  <br/>\n                            単価：￥6,998<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                            商品名：MARITIME CBDオイル 10ml (5%)  通常購入  <br/>\n                            単価：￥13,997<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                            商品名：MARITIME CBD ウォーター 500ml ６本セット  通常購入  <br/>\n                            単価：￥6,765<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            永井 光弘 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            下記URLにご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime-cbd.com/cart\" target=\"_blank\">https://maritime-cbd.com/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 10ml (2%)  通常購入  <br/>\n                            単価：￥6,998<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                            商品名：MARITIME CBDオイル 10ml (5%)  通常購入  <br/>\n                            単価：￥13,997<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                            商品名：MARITIME CBD ウォーター 500ml ６本セット  通常購入  <br/>\n                            単価：￥6,765<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n','mailhistory'),
	(24,NULL,1,'2020-09-09 09:55:15','[MARITIME] ショッピングカートに入ったままのアイテムがあります','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Test 様<br/>\n                            <br/>\n                            マリタイムCBDショップをご利用いただき誠にありがとうございます。お買い忘れはありませんか？ショッピングカートに入ったままの商品があるようです。<br/>\n                            <br/>\n                            下記URLからご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime-cbd.com/cart\" target=\"_blank\">https://maritime-cbd.com/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                                                                                        商品名：MARITIME CBD ウォーター 500ml\n                                                                    販売種別：通常購入\n                                                                <br/>\n                            単価：￥1,128<br/>\n                            数量：1<br/>\n                                <br/>\n                                \n                                                        <hr style=\"border-top: 2px double #8c8b8b;\">\n                            当店のCBD商品はメイド・イン・ジャパンの品質、オリーブオイルを使った天然由来CBDオイルです。「安心して毎日飲み続けられるCBDオイルをつくりたい。」そんな想いから生まれました。<br/>\n                            <br/>\n                            ショッピングカートの内容は2日間保存しています。今ならCBDオイルをご購入のお客さまにもれなく、マリタイムCBDウォーターをプレゼントのキャンペーン中です。この機会にぜひお買い求めください。<br/>\n                            <br/>\n                            何かお困りのことはありませんか？お問い合わせは <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> まで。CBDオイルのよくある質問はこちらからご覧ください。（<a href=\"https://maritime-cbd.com/user_data/faq\" style=\"color:#aaaaaa;text-decoration:none;\">CBDオイルのよくある質問</a>）<br/>\n                            <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Test 様<br/>\n                            <br/>\n                            マリタイムCBDショップをご利用いただき誠にありがとうございます。お買い忘れはありませんか？ショッピングカートに入ったままの商品があるようです。<br/>\n                            <br/>\n                            下記URLからご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime-cbd.com/cart\" target=\"_blank\">https://maritime-cbd.com/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                                                                                        商品名：MARITIME CBD ウォーター 500ml\n                                                                    販売種別：通常購入\n                                                                <br/>\n                            単価：￥1,128<br/>\n                            数量：1<br/>\n                                <br/>\n                                \n                                                        <hr style=\"border-top: 2px double #8c8b8b;\">\n                            当店のCBD商品はメイド・イン・ジャパンの品質、オリーブオイルを使った天然由来CBDオイルです。「安心して毎日飲み続けられるCBDオイルをつくりたい。」そんな想いから生まれました。<br/>\n                            <br/>\n                            ショッピングカートの内容は2日間保存しています。今ならCBDオイルをご購入のお客さまにもれなく、マリタイムCBDウォーターをプレゼントのキャンペーン中です。この機会にぜひお買い求めください。<br/>\n                            <br/>\n                            何かお困りのことはありませんか？お問い合わせは <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> まで。CBDオイルのよくある質問はこちらからご覧ください。（<a href=\"https://maritime-cbd.com/user_data/faq\" style=\"color:#aaaaaa;text-decoration:none;\">CBDオイルのよくある質問</a>）<br/>\n                            <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>','mailhistory'),
	(25,114,NULL,'2020-09-09 15:37:04','[MARITIME] ご注文ありがとうございます','山田 秀人 様\nこの度はご注文いただき誠にありがとうございます。下記ご注文内容にお間違いがないかご確認下さい。\n\n************************************************\n　ご請求金額\n************************************************\n\nご注文日時：2020/09/10 0:28:39\nご注文番号：114\nお支払い合計：￥6,765\nお支払い方法：PayPal決済\nお問い合わせ内容：\n\n************************************************\n　ご注文商品明細\n************************************************\n\n商品コード：cbd-water-500-set\n商品名：MARITIME CBD ウォーター 500ml ６本セット  通常購入  \n単価：￥7,517\n数量：1\n\n\n-------------------------------------------------\n小　計：￥7,517\n\n手数料：￥0\n送　料：￥0\n値引き：-￥752\n============================================\n合　計：￥6,765\n\n************************************************\n　ご注文者情報\n************************************************\nお名前：山田 秀人 様お名前(カナ)：ヤマダ ヒデト 様郵便番号：〒1530062\n住所：東京都目黒区三田1-3-21-210\n電話番号：09083193987\nメールアドレス：eightbear+ml@gmail.com\n\n************************************************\n　配送情報\n************************************************\n\n◎お届け先\nお名前：山田 秀人 様お名前(カナ)：ヤマダ ヒデト 様郵便番号：〒6060801\n住所：京都府京都市左京区下鴨宮河町55-4\n電話番号：09083193987\n\n配送方法：佐川急便\nお届け日：2020/09/11\nお届け時間：午前中（8時～12時）\n\n商品コード：cbd-water-500-set\n商品名：MARITIME CBD ウォーター 500ml ６本セット  通常購入  \n数量：1\n\n\n***********************************************\n　クーポン情報                                 \n***********************************************\n\nクーポンコード: MARITIME10 初回10％割引オファー\n\n\n============================================\n\nこのメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n\n','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://www.maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"title\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">この度はご注文いただき誠にありがとうございます。</div>\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            山田 秀人 様<br/>\n                            <br/>\n                            下記ご注文内容にお間違いがないかご確認下さい。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご請求金額<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご注文日時：2020/09/10 0:28:39<br/>\n                            ご注文番号：114<br/>\n                            お支払い合計：￥6,765<br/>\n                            お支払い方法：PayPal決済<br/>\n                                                        お問い合わせ内容：<br/>\n                            <br/>\n                                                                                    <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご注文商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                            商品コード：cbd-water-500-set<br/>\n                                商品名：MARITIME CBD ウォーター 500ml ６本セット  通常購入  <br/>\n                                単価：￥7,517<br/>\n                                数量：1<br/>\n                                <br/>\n                                                        <hr style=\"border-top: 2px dashed #8c8b8b;\">\n                            小　計：￥7,517<br/>\n                            <br/>\n                            手数料：￥0<br/>\n                            送　料：￥0<br/>\n                                                            値引き：-￥752<br/>\n                                                        <hr style=\"border-top: 1px dotted #8c8b8b;\">\n                            合　計：￥6,765<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご注文者情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            お名前：山田 秀人 様<br/>\n                            お名前(カナ)：ヤマダ ヒデト 様<br/>\n                                                        郵便番号：〒1530062<br/>\n                            住所：東京都目黒区三田1-3-21-210<br/>\n                            電話番号：09083193987<br/>\n                            メールアドレス：eightbear+ml@gmail.com<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　配送情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n\n                                                            ◎お届け先<br/>\n                                <br/>\n                                お名前：山田 秀人 様<br/>\n                                お名前(カナ)：ヤマダ ヒデト 様<br/>\n                                                                郵便番号：〒6060801<br/>\n                                住所：京都府京都市左京区下鴨宮河町55-4<br/>\n                                電話番号：09083193987<br/>\n                                <br/>\n                                配送方法：佐川急便<br/>\n                                お届け日：2020/09/11<br/>\n                                お届け時間：午前中（8時～12時）<br/>\n                                <br/>\n                                                                    商品コード：cbd-water-500-set<br/>\n                                    商品名：MARITIME CBD ウォーター 500ml ６本セット  通常購入  <br/>\n                                    数量：1<br/>\n                                    <br/>\n                                                                                                                        ***********************************************\n　クーポン情報                                 \n***********************************************\n\nクーポンコード: MARITIME10 初回10％割引オファー\n<br/>\n                                                        <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://www.maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n','mailhistory'),
	(26,114,3,'2020-09-10 09:55:12','[MARITIME] 商品出荷のお知らせ','山田 秀人 様\nお客さまがご注文された以下の商品を発送いたしました。商品の到着まで、今しばらくお待ちください。\n\nお問い合わせ番号：403921650834\nお問い合わせURL：https://k2k.sagawa-exp.co.jp/p/sagawa/web/okurijoinput.jsp\n\n************************************************\n　ご注文商品明細\n************************************************\n\n商品コード：cbd-water-500-set\n商品名：MARITIME CBD ウォーター 500ml ６本セット  通常購入  \n数量：1\n\n\n============================================\n\n************************************************\n　ご注文者情報\n************************************************\nお名前：山田 秀人 様お名前(カナ)：ヤマダ ヒデト 様郵便番号：〒1530062\n住所：東京都目黒区三田1-3-21-210\n電話番号：09083193987\n\n************************************************\n　配送情報\n************************************************\n\nお名前：山田 秀人 様お名前(カナ)：ヤマダ ヒデト 様郵便番号：〒6060801\n住所：京都府京都市左京区下鴨宮河町55-4\n電話番号：09083193987\n\nお届け日：2020/09/11\nお届け時間：午前中（8時～12時）\n','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"title\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:600;color:#374550;\">商品を発送いたしました。</div>\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            山田 秀人 様<br>\n                            <br>\n                            MARITIMEでございます。<br/>\n                            お客さまがご注文された以下の商品を発送いたしました。商品の到着まで、今しばらくお待ちください。<br/>\n                            <br/>\n                                                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                お問い合わせ番号：403921650834\n                                                                    <br/>\n                                    お問い合わせURL：https://k2k.sagawa-exp.co.jp/p/sagawa/web/okurijoinput.jsp\n                                                                <br/>\n                                                        <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご注文商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                            商品コード：cbd-water-500-set<br/>\n                                商品名：MARITIME CBD ウォーター 500ml ６本セット  通常購入  <br/>\n                                数量：1<br/>\n                                <br/>\n                                                        <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご注文者情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            お名前：山田 秀人 様<br/>\n                            お名前(カナ)：ヤマダ ヒデト 様<br/>\n                                                        郵便番号：〒1530062<br/>\n                            住所：東京都目黒区三田1-3-21-210<br/>\n                            電話番号：09083193987<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　配送情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            お名前：山田 秀人 様<br/>\n                            お名前(カナ)：ヤマダ ヒデト 様<br/>\n                                                        郵便番号：〒6060801<br/>\n                            住所：京都府京都市左京区下鴨宮河町55-4<br/>\n                            電話番号：09083193987<br/>\n                            <br/>\n                            お届け日：2020/09/11<br/>\n                            お届け時間：午前中（8時～12時）<br/>\n                            <br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n\n','mailhistory'),
	(27,NULL,1,'2020-09-12 23:48:37','[MARITIME] ショッピングカートに入ったままのアイテムがあります','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            マリタイムCBDショップをご利用いただき誠にありがとうございます。お買い忘れはありませんか？ショッピングカートに入ったままの商品があるようです。<br/>\n                            <br/>\n                            下記URLからご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime-cbd.com/cart\" target=\"_blank\">https://maritime-cbd.com/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                                                                                        商品名：MARITIME CBDオイル 10ml (1%)\n                                                                    販売種別：通常購入\n                                                                <br/>\n                            単価：￥4,666<br/>\n                            数量：1<br/>\n                                <br/>\n                                \n                                                        <hr style=\"border-top: 2px double #8c8b8b;\">\n                            当店のCBD商品はメイド・イン・ジャパンの品質、オリーブオイルを使った天然由来CBDオイルです。「安心して毎日飲み続けられるCBDオイルをつくりたい。」そんな想いから生まれました。<br/>\n                            <br/>\n                            ショッピングカートの内容は2日間保存しています。今ならCBDオイルをご購入のお客さまにもれなく、マリタイムCBDウォーターをプレゼントのキャンペーン中です。この機会にぜひお買い求めください。<br/>\n                            <br/>\n                            何かお困りのことはありませんか？お問い合わせは <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> まで。CBDオイルのよくある質問はこちらからご覧ください。（<a href=\"https://maritime-cbd.com/user_data/faq\" style=\"color:#aaaaaa;text-decoration:none;\">CBDオイルのよくある質問</a>）<br/>\n                            <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            マリタイムCBDショップをご利用いただき誠にありがとうございます。お買い忘れはありませんか？ショッピングカートに入ったままの商品があるようです。<br/>\n                            <br/>\n                            下記URLからご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime-cbd.com/cart\" target=\"_blank\">https://maritime-cbd.com/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                                                                                        商品名：MARITIME CBDオイル 10ml (1%)\n                                                                    販売種別：通常購入\n                                                                <br/>\n                            単価：￥4,666<br/>\n                            数量：1<br/>\n                                <br/>\n                                \n                                                        <hr style=\"border-top: 2px double #8c8b8b;\">\n                            当店のCBD商品はメイド・イン・ジャパンの品質、オリーブオイルを使った天然由来CBDオイルです。「安心して毎日飲み続けられるCBDオイルをつくりたい。」そんな想いから生まれました。<br/>\n                            <br/>\n                            ショッピングカートの内容は2日間保存しています。今ならCBDオイルをご購入のお客さまにもれなく、マリタイムCBDウォーターをプレゼントのキャンペーン中です。この機会にぜひお買い求めください。<br/>\n                            <br/>\n                            何かお困りのことはありませんか？お問い合わせは <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> まで。CBDオイルのよくある質問はこちらからご覧ください。（<a href=\"https://maritime-cbd.com/user_data/faq\" style=\"color:#aaaaaa;text-decoration:none;\">CBDオイルのよくある質問</a>）<br/>\n                            <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>','mailhistory'),
	(28,81,1,'2020-09-12 23:48:37','[MARITIME] ショッピングカートに入ったままのアイテムがあります','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Test2 Test 様<br/>\n                            <br/>\n                            マリタイムCBDショップをご利用いただき誠にありがとうございます。お買い忘れはありませんか？ショッピングカートに入ったままの商品があるようです。<br/>\n                            <br/>\n                            下記URLからご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime-cbd.com/cart\" target=\"_blank\">https://maritime-cbd.com/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 10ml (1%)  通常購入  <br/>\n                            単価：￥5,184<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                        <hr style=\"border-top: 2px double #8c8b8b;\">\n                            当店のCBD商品はメイド・イン・ジャパンの品質、オリーブオイルを使った天然由来CBDオイルです。「安心して毎日飲み続けられるCBDオイルをつくりたい。」そんな想いから生まれました。<br/>\n                            <br/>\n                            ショッピングカートの内容は2日間保存しています。今ならCBDオイルをご購入のお客さまにもれなく、マリタイムCBDウォーターをプレゼントのキャンペーン中です。この機会にぜひお買い求めください。<br/>\n                            <br/>\n                            何かお困りのことはありませんか？お問い合わせは <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> まで。CBDオイルのよくある質問はこちらからご覧ください。（<a href=\"https://maritime-cbd.com/user_data/faq\" style=\"color:#aaaaaa;text-decoration:none;\">CBDオイルのよくある質問</a>）<br/>\n                            <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Test2 Test 様<br/>\n                            <br/>\n                            マリタイムCBDショップをご利用いただき誠にありがとうございます。お買い忘れはありませんか？ショッピングカートに入ったままの商品があるようです。<br/>\n                            <br/>\n                            下記URLからご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime-cbd.com/cart\" target=\"_blank\">https://maritime-cbd.com/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 10ml (1%)  通常購入  <br/>\n                            単価：￥5,184<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                        <hr style=\"border-top: 2px double #8c8b8b;\">\n                            当店のCBD商品はメイド・イン・ジャパンの品質、オリーブオイルを使った天然由来CBDオイルです。「安心して毎日飲み続けられるCBDオイルをつくりたい。」そんな想いから生まれました。<br/>\n                            <br/>\n                            ショッピングカートの内容は2日間保存しています。今ならCBDオイルをご購入のお客さまにもれなく、マリタイムCBDウォーターをプレゼントのキャンペーン中です。この機会にぜひお買い求めください。<br/>\n                            <br/>\n                            何かお困りのことはありませんか？お問い合わせは <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> まで。CBDオイルのよくある質問はこちらからご覧ください。（<a href=\"https://maritime-cbd.com/user_data/faq\" style=\"color:#aaaaaa;text-decoration:none;\">CBDオイルのよくある質問</a>）<br/>\n                            <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>','mailhistory'),
	(29,106,1,'2020-09-12 23:48:37','[MARITIME] ショッピングカートに入ったままのアイテムがあります','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Test Test 様<br/>\n                            <br/>\n                            マリタイムCBDショップをご利用いただき誠にありがとうございます。お買い忘れはありませんか？ショッピングカートに入ったままの商品があるようです。<br/>\n                            <br/>\n                            下記URLからご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime-cbd.com/cart\" target=\"_blank\">https://maritime-cbd.com/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 10ml (1%)  通常購入  <br/>\n                            単価：￥5,184<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                        <hr style=\"border-top: 2px double #8c8b8b;\">\n                            当店のCBD商品はメイド・イン・ジャパンの品質、オリーブオイルを使った天然由来CBDオイルです。「安心して毎日飲み続けられるCBDオイルをつくりたい。」そんな想いから生まれました。<br/>\n                            <br/>\n                            ショッピングカートの内容は2日間保存しています。今ならCBDオイルをご購入のお客さまにもれなく、マリタイムCBDウォーターをプレゼントのキャンペーン中です。この機会にぜひお買い求めください。<br/>\n                            <br/>\n                            何かお困りのことはありませんか？お問い合わせは <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> まで。CBDオイルのよくある質問はこちらからご覧ください。（<a href=\"https://maritime-cbd.com/user_data/faq\" style=\"color:#aaaaaa;text-decoration:none;\">CBDオイルのよくある質問</a>）<br/>\n                            <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Test Test 様<br/>\n                            <br/>\n                            マリタイムCBDショップをご利用いただき誠にありがとうございます。お買い忘れはありませんか？ショッピングカートに入ったままの商品があるようです。<br/>\n                            <br/>\n                            下記URLからご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime-cbd.com/cart\" target=\"_blank\">https://maritime-cbd.com/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 10ml (1%)  通常購入  <br/>\n                            単価：￥5,184<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                        <hr style=\"border-top: 2px double #8c8b8b;\">\n                            当店のCBD商品はメイド・イン・ジャパンの品質、オリーブオイルを使った天然由来CBDオイルです。「安心して毎日飲み続けられるCBDオイルをつくりたい。」そんな想いから生まれました。<br/>\n                            <br/>\n                            ショッピングカートの内容は2日間保存しています。今ならCBDオイルをご購入のお客さまにもれなく、マリタイムCBDウォーターをプレゼントのキャンペーン中です。この機会にぜひお買い求めください。<br/>\n                            <br/>\n                            何かお困りのことはありませんか？お問い合わせは <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> まで。CBDオイルのよくある質問はこちらからご覧ください。（<a href=\"https://maritime-cbd.com/user_data/faq\" style=\"color:#aaaaaa;text-decoration:none;\">CBDオイルのよくある質問</a>）<br/>\n                            <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>','mailhistory'),
	(30,79,1,'2020-09-12 23:48:37','[MARITIME] ショッピングカートに入ったままのアイテムがあります','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Test Test 様<br/>\n                            <br/>\n                            マリタイムCBDショップをご利用いただき誠にありがとうございます。お買い忘れはありませんか？ショッピングカートに入ったままの商品があるようです。<br/>\n                            <br/>\n                            下記URLからご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime-cbd.com/cart\" target=\"_blank\">https://maritime-cbd.com/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 10ml (1%)  通常購入  <br/>\n                            単価：￥5,184<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                        <hr style=\"border-top: 2px double #8c8b8b;\">\n                            当店のCBD商品はメイド・イン・ジャパンの品質、オリーブオイルを使った天然由来CBDオイルです。「安心して毎日飲み続けられるCBDオイルをつくりたい。」そんな想いから生まれました。<br/>\n                            <br/>\n                            ショッピングカートの内容は2日間保存しています。今ならCBDオイルをご購入のお客さまにもれなく、マリタイムCBDウォーターをプレゼントのキャンペーン中です。この機会にぜひお買い求めください。<br/>\n                            <br/>\n                            何かお困りのことはありませんか？お問い合わせは <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> まで。CBDオイルのよくある質問はこちらからご覧ください。（<a href=\"https://maritime-cbd.com/user_data/faq\" style=\"color:#aaaaaa;text-decoration:none;\">CBDオイルのよくある質問</a>）<br/>\n                            <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Test Test 様<br/>\n                            <br/>\n                            マリタイムCBDショップをご利用いただき誠にありがとうございます。お買い忘れはありませんか？ショッピングカートに入ったままの商品があるようです。<br/>\n                            <br/>\n                            下記URLからご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime-cbd.com/cart\" target=\"_blank\">https://maritime-cbd.com/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 10ml (1%)  通常購入  <br/>\n                            単価：￥5,184<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                        <hr style=\"border-top: 2px double #8c8b8b;\">\n                            当店のCBD商品はメイド・イン・ジャパンの品質、オリーブオイルを使った天然由来CBDオイルです。「安心して毎日飲み続けられるCBDオイルをつくりたい。」そんな想いから生まれました。<br/>\n                            <br/>\n                            ショッピングカートの内容は2日間保存しています。今ならCBDオイルをご購入のお客さまにもれなく、マリタイムCBDウォーターをプレゼントのキャンペーン中です。この機会にぜひお買い求めください。<br/>\n                            <br/>\n                            何かお困りのことはありませんか？お問い合わせは <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> まで。CBDオイルのよくある質問はこちらからご覧ください。（<a href=\"https://maritime-cbd.com/user_data/faq\" style=\"color:#aaaaaa;text-decoration:none;\">CBDオイルのよくある質問</a>）<br/>\n                            <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>','mailhistory'),
	(31,112,1,'2020-09-12 23:48:37','[MARITIME] ショッピングカートに入ったままのアイテムがあります','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            GIM HAESOO 様<br/>\n                            <br/>\n                            マリタイムCBDショップをご利用いただき誠にありがとうございます。お買い忘れはありませんか？ショッピングカートに入ったままの商品があるようです。<br/>\n                            <br/>\n                            下記URLからご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime-cbd.com/cart\" target=\"_blank\">https://maritime-cbd.com/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 30ml (1%)  通常購入  <br/>\n                            単価：￥8,845<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                        <hr style=\"border-top: 2px double #8c8b8b;\">\n                            当店のCBD商品はメイド・イン・ジャパンの品質、オリーブオイルを使った天然由来CBDオイルです。「安心して毎日飲み続けられるCBDオイルをつくりたい。」そんな想いから生まれました。<br/>\n                            <br/>\n                            ショッピングカートの内容は2日間保存しています。今ならCBDオイルをご購入のお客さまにもれなく、マリタイムCBDウォーターをプレゼントのキャンペーン中です。この機会にぜひお買い求めください。<br/>\n                            <br/>\n                            何かお困りのことはありませんか？お問い合わせは <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> まで。CBDオイルのよくある質問はこちらからご覧ください。（<a href=\"https://maritime-cbd.com/user_data/faq\" style=\"color:#aaaaaa;text-decoration:none;\">CBDオイルのよくある質問</a>）<br/>\n                            <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            GIM HAESOO 様<br/>\n                            <br/>\n                            マリタイムCBDショップをご利用いただき誠にありがとうございます。お買い忘れはありませんか？ショッピングカートに入ったままの商品があるようです。<br/>\n                            <br/>\n                            下記URLからご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime-cbd.com/cart\" target=\"_blank\">https://maritime-cbd.com/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 30ml (1%)  通常購入  <br/>\n                            単価：￥8,845<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                        <hr style=\"border-top: 2px double #8c8b8b;\">\n                            当店のCBD商品はメイド・イン・ジャパンの品質、オリーブオイルを使った天然由来CBDオイルです。「安心して毎日飲み続けられるCBDオイルをつくりたい。」そんな想いから生まれました。<br/>\n                            <br/>\n                            ショッピングカートの内容は2日間保存しています。今ならCBDオイルをご購入のお客さまにもれなく、マリタイムCBDウォーターをプレゼントのキャンペーン中です。この機会にぜひお買い求めください。<br/>\n                            <br/>\n                            何かお困りのことはありませんか？お問い合わせは <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> まで。CBDオイルのよくある質問はこちらからご覧ください。（<a href=\"https://maritime-cbd.com/user_data/faq\" style=\"color:#aaaaaa;text-decoration:none;\">CBDオイルのよくある質問</a>）<br/>\n                            <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>','mailhistory'),
	(32,96,1,'2020-09-12 23:48:37','[MARITIME] ショッピングカートに入ったままのアイテムがあります','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            関 歌織 様<br/>\n                            <br/>\n                            マリタイムCBDショップをご利用いただき誠にありがとうございます。お買い忘れはありませんか？ショッピングカートに入ったままの商品があるようです。<br/>\n                            <br/>\n                            下記URLからご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime-cbd.com/cart\" target=\"_blank\">https://maritime-cbd.com/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 10ml (3%)  通常購入  <br/>\n                            単価：￥10,368<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                        <hr style=\"border-top: 2px double #8c8b8b;\">\n                            当店のCBD商品はメイド・イン・ジャパンの品質、オリーブオイルを使った天然由来CBDオイルです。「安心して毎日飲み続けられるCBDオイルをつくりたい。」そんな想いから生まれました。<br/>\n                            <br/>\n                            ショッピングカートの内容は2日間保存しています。今ならCBDオイルをご購入のお客さまにもれなく、マリタイムCBDウォーターをプレゼントのキャンペーン中です。この機会にぜひお買い求めください。<br/>\n                            <br/>\n                            何かお困りのことはありませんか？お問い合わせは <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> まで。CBDオイルのよくある質問はこちらからご覧ください。（<a href=\"https://maritime-cbd.com/user_data/faq\" style=\"color:#aaaaaa;text-decoration:none;\">CBDオイルのよくある質問</a>）<br/>\n                            <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            関 歌織 様<br/>\n                            <br/>\n                            マリタイムCBDショップをご利用いただき誠にありがとうございます。お買い忘れはありませんか？ショッピングカートに入ったままの商品があるようです。<br/>\n                            <br/>\n                            下記URLからご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime-cbd.com/cart\" target=\"_blank\">https://maritime-cbd.com/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 10ml (3%)  通常購入  <br/>\n                            単価：￥10,368<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                        <hr style=\"border-top: 2px double #8c8b8b;\">\n                            当店のCBD商品はメイド・イン・ジャパンの品質、オリーブオイルを使った天然由来CBDオイルです。「安心して毎日飲み続けられるCBDオイルをつくりたい。」そんな想いから生まれました。<br/>\n                            <br/>\n                            ショッピングカートの内容は2日間保存しています。今ならCBDオイルをご購入のお客さまにもれなく、マリタイムCBDウォーターをプレゼントのキャンペーン中です。この機会にぜひお買い求めください。<br/>\n                            <br/>\n                            何かお困りのことはありませんか？お問い合わせは <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> まで。CBDオイルのよくある質問はこちらからご覧ください。（<a href=\"https://maritime-cbd.com/user_data/faq\" style=\"color:#aaaaaa;text-decoration:none;\">CBDオイルのよくある質問</a>）<br/>\n                            <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>','mailhistory');

INSERT INTO `dtb_mail_history` (`id`, `order_id`, `creator_id`, `send_date`, `mail_subject`, `mail_body`, `mail_html_body`, `discriminator_type`)
VALUES
	(33,NULL,1,'2020-09-12 23:48:38','[MARITIME] ショッピングカートに入ったままのアイテムがあります','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            杉森 浩一郎 様<br/>\n                            <br/>\n                            マリタイムCBDショップをご利用いただき誠にありがとうございます。お買い忘れはありませんか？ショッピングカートに入ったままの商品があるようです。<br/>\n                            <br/>\n                            下記URLからご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime-cbd.com/cart\" target=\"_blank\">https://maritime-cbd.com/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                                                                                        商品名：MARITIME CBD ウォーター 500ml\n                                                                    販売種別：通常購入\n                                                                <br/>\n                            単価：￥1,128<br/>\n                            数量：5<br/>\n                                <br/>\n                                \n                                                        <hr style=\"border-top: 2px double #8c8b8b;\">\n                            当店のCBD商品はメイド・イン・ジャパンの品質、オリーブオイルを使った天然由来CBDオイルです。「安心して毎日飲み続けられるCBDオイルをつくりたい。」そんな想いから生まれました。<br/>\n                            <br/>\n                            ショッピングカートの内容は2日間保存しています。今ならCBDオイルをご購入のお客さまにもれなく、マリタイムCBDウォーターをプレゼントのキャンペーン中です。この機会にぜひお買い求めください。<br/>\n                            <br/>\n                            何かお困りのことはありませんか？お問い合わせは <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> まで。CBDオイルのよくある質問はこちらからご覧ください。（<a href=\"https://maritime-cbd.com/user_data/faq\" style=\"color:#aaaaaa;text-decoration:none;\">CBDオイルのよくある質問</a>）<br/>\n                            <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            杉森 浩一郎 様<br/>\n                            <br/>\n                            マリタイムCBDショップをご利用いただき誠にありがとうございます。お買い忘れはありませんか？ショッピングカートに入ったままの商品があるようです。<br/>\n                            <br/>\n                            下記URLからご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime-cbd.com/cart\" target=\"_blank\">https://maritime-cbd.com/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                                                                                        商品名：MARITIME CBD ウォーター 500ml\n                                                                    販売種別：通常購入\n                                                                <br/>\n                            単価：￥1,128<br/>\n                            数量：5<br/>\n                                <br/>\n                                \n                                                        <hr style=\"border-top: 2px double #8c8b8b;\">\n                            当店のCBD商品はメイド・イン・ジャパンの品質、オリーブオイルを使った天然由来CBDオイルです。「安心して毎日飲み続けられるCBDオイルをつくりたい。」そんな想いから生まれました。<br/>\n                            <br/>\n                            ショッピングカートの内容は2日間保存しています。今ならCBDオイルをご購入のお客さまにもれなく、マリタイムCBDウォーターをプレゼントのキャンペーン中です。この機会にぜひお買い求めください。<br/>\n                            <br/>\n                            何かお困りのことはありませんか？お問い合わせは <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> まで。CBDオイルのよくある質問はこちらからご覧ください。（<a href=\"https://maritime-cbd.com/user_data/faq\" style=\"color:#aaaaaa;text-decoration:none;\">CBDオイルのよくある質問</a>）<br/>\n                            <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>','mailhistory'),
	(34,18,1,'2020-09-12 23:48:38','[MARITIME] ショッピングカートに入ったままのアイテムがあります','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Gues User 様<br/>\n                            <br/>\n                            マリタイムCBDショップをご利用いただき誠にありがとうございます。お買い忘れはありませんか？ショッピングカートに入ったままの商品があるようです。<br/>\n                            <br/>\n                            下記URLからご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime-cbd.com/cart\" target=\"_blank\">https://maritime-cbd.com/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 30ml (10%)  通常購入  <br/>\n                            単価：￥13,200<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                        <hr style=\"border-top: 2px double #8c8b8b;\">\n                            当店のCBD商品はメイド・イン・ジャパンの品質、オリーブオイルを使った天然由来CBDオイルです。「安心して毎日飲み続けられるCBDオイルをつくりたい。」そんな想いから生まれました。<br/>\n                            <br/>\n                            ショッピングカートの内容は2日間保存しています。今ならCBDオイルをご購入のお客さまにもれなく、マリタイムCBDウォーターをプレゼントのキャンペーン中です。この機会にぜひお買い求めください。<br/>\n                            <br/>\n                            何かお困りのことはありませんか？お問い合わせは <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> まで。CBDオイルのよくある質問はこちらからご覧ください。（<a href=\"https://maritime-cbd.com/user_data/faq\" style=\"color:#aaaaaa;text-decoration:none;\">CBDオイルのよくある質問</a>）<br/>\n                            <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Gues User 様<br/>\n                            <br/>\n                            マリタイムCBDショップをご利用いただき誠にありがとうございます。お買い忘れはありませんか？ショッピングカートに入ったままの商品があるようです。<br/>\n                            <br/>\n                            下記URLからご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime-cbd.com/cart\" target=\"_blank\">https://maritime-cbd.com/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 30ml (10%)  通常購入  <br/>\n                            単価：￥13,200<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                        <hr style=\"border-top: 2px double #8c8b8b;\">\n                            当店のCBD商品はメイド・イン・ジャパンの品質、オリーブオイルを使った天然由来CBDオイルです。「安心して毎日飲み続けられるCBDオイルをつくりたい。」そんな想いから生まれました。<br/>\n                            <br/>\n                            ショッピングカートの内容は2日間保存しています。今ならCBDオイルをご購入のお客さまにもれなく、マリタイムCBDウォーターをプレゼントのキャンペーン中です。この機会にぜひお買い求めください。<br/>\n                            <br/>\n                            何かお困りのことはありませんか？お問い合わせは <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> まで。CBDオイルのよくある質問はこちらからご覧ください。（<a href=\"https://maritime-cbd.com/user_data/faq\" style=\"color:#aaaaaa;text-decoration:none;\">CBDオイルのよくある質問</a>）<br/>\n                            <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>','mailhistory'),
	(35,NULL,1,'2020-09-14 08:06:11','[MARITIME] ショッピングカートに入ったままのアイテムがあります','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            マリタイムCBDショップをご利用いただき誠にありがとうございます。お買い忘れはありませんか？ショッピングカートに入ったままの商品があるようです。<br/>\n                            <br/>\n                            下記URLからご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime-cbd.com/cart\" target=\"_blank\">https://maritime-cbd.com/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                                                                                        商品名：MARITIME CBDオイル 10ml (1%)\n                                                                    販売種別：通常購入\n                                                                <br/>\n                            単価：￥4,666<br/>\n                            数量：1<br/>\n                                <br/>\n                                \n                                                        <hr style=\"border-top: 2px double #8c8b8b;\">\n                            当店のCBD商品はメイド・イン・ジャパンの品質、オリーブオイルを使った天然由来CBDオイルです。「安心して毎日飲み続けられるCBDオイルをつくりたい。」そんな想いから生まれました。<br/>\n                            <br/>\n                            ショッピングカートの内容は2日間保存しています。今ならCBDオイルをご購入のお客さまにもれなく、マリタイムCBDウォーターをプレゼントのキャンペーン中です。この機会にぜひお買い求めください。<br/>\n                            <br/>\n                            何かお困りのことはありませんか？お問い合わせは <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> まで。CBDオイルのよくある質問はこちらからご覧ください。（<a href=\"https://maritime-cbd.com/user_data/faq\" style=\"color:#aaaaaa;text-decoration:none;\">CBDオイルのよくある質問</a>）<br/>\n                            <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            マリタイムCBDショップをご利用いただき誠にありがとうございます。お買い忘れはありませんか？ショッピングカートに入ったままの商品があるようです。<br/>\n                            <br/>\n                            下記URLからご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime-cbd.com/cart\" target=\"_blank\">https://maritime-cbd.com/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                                                                                        商品名：MARITIME CBDオイル 10ml (1%)\n                                                                    販売種別：通常購入\n                                                                <br/>\n                            単価：￥4,666<br/>\n                            数量：1<br/>\n                                <br/>\n                                \n                                                        <hr style=\"border-top: 2px double #8c8b8b;\">\n                            当店のCBD商品はメイド・イン・ジャパンの品質、オリーブオイルを使った天然由来CBDオイルです。「安心して毎日飲み続けられるCBDオイルをつくりたい。」そんな想いから生まれました。<br/>\n                            <br/>\n                            ショッピングカートの内容は2日間保存しています。今ならCBDオイルをご購入のお客さまにもれなく、マリタイムCBDウォーターをプレゼントのキャンペーン中です。この機会にぜひお買い求めください。<br/>\n                            <br/>\n                            何かお困りのことはありませんか？お問い合わせは <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> まで。CBDオイルのよくある質問はこちらからご覧ください。（<a href=\"https://maritime-cbd.com/user_data/faq\" style=\"color:#aaaaaa;text-decoration:none;\">CBDオイルのよくある質問</a>）<br/>\n                            <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>','mailhistory'),
	(36,NULL,1,'2020-09-14 08:21:49','[MARITIME] ショッピングカートに入ったままのアイテムがあります','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            マリタイムCBDショップをご利用いただき誠にありがとうございます。お買い忘れはありませんか？ショッピングカートに入ったままの商品があるようです。<br/>\n                            <br/>\n                            下記URLからご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime-cbd.com/cart\" target=\"_blank\">https://maritime-cbd.com/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                                                                                        商品名：MARITIME CBDオイル 10ml (1%)\n                                                                    販売種別：通常購入\n                                                                <br/>\n                            単価：￥4,666<br/>\n                            数量：1<br/>\n                                <br/>\n                                \n                                                        <hr style=\"border-top: 2px double #8c8b8b;\">\n                            当店のCBD商品はメイド・イン・ジャパンの品質、オリーブオイルを使った天然由来CBDオイルです。「安心して毎日飲み続けられるCBDオイルをつくりたい。」そんな想いから生まれました。<br/>\n                            <br/>\n                            ショッピングカートの内容は2日間保存しています。今ならCBDオイルをご購入のお客さまにもれなく、マリタイムCBDウォーターをプレゼントのキャンペーン中です。この機会にぜひお買い求めください。<br/>\n                            <br/>\n                            何かお困りのことはありませんか？お問い合わせは <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> まで。CBDオイルのよくある質問はこちらからご覧ください。（<a href=\"https://maritime-cbd.com/user_data/faq\" style=\"color:#aaaaaa;text-decoration:none;\">CBDオイルのよくある質問</a>）<br/>\n                            <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            マリタイムCBDショップをご利用いただき誠にありがとうございます。お買い忘れはありませんか？ショッピングカートに入ったままの商品があるようです。<br/>\n                            <br/>\n                            下記URLからご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime-cbd.com/cart\" target=\"_blank\">https://maritime-cbd.com/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                                                                                        商品名：MARITIME CBDオイル 10ml (1%)\n                                                                    販売種別：通常購入\n                                                                <br/>\n                            単価：￥4,666<br/>\n                            数量：1<br/>\n                                <br/>\n                                \n                                                        <hr style=\"border-top: 2px double #8c8b8b;\">\n                            当店のCBD商品はメイド・イン・ジャパンの品質、オリーブオイルを使った天然由来CBDオイルです。「安心して毎日飲み続けられるCBDオイルをつくりたい。」そんな想いから生まれました。<br/>\n                            <br/>\n                            ショッピングカートの内容は2日間保存しています。今ならCBDオイルをご購入のお客さまにもれなく、マリタイムCBDウォーターをプレゼントのキャンペーン中です。この機会にぜひお買い求めください。<br/>\n                            <br/>\n                            何かお困りのことはありませんか？お問い合わせは <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> まで。CBDオイルのよくある質問はこちらからご覧ください。（<a href=\"https://maritime-cbd.com/user_data/faq\" style=\"color:#aaaaaa;text-decoration:none;\">CBDオイルのよくある質問</a>）<br/>\n                            <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>','mailhistory'),
	(37,48,1,'2020-09-15 02:41:53','[MARITIME] Test frederic','Test send email',NULL,'mailhistory'),
	(38,111,3,'2020-09-15 03:19:20','[MARITIME] ご注文ありがとうございます','関 歌織 様\r\nこの度はご注文いただき誠にありがとうございます。下記ご注文内容にお間違いがないかご確認下さい。\r\nまた、ご入金の確認が取れておりませんが何か問題でもございましたでしょうか。\r\nお気軽にお問い合わせくださいませ。\r\n\r\n************************************************\r\n　ご請求金額\r\n************************************************\r\n\r\nご注文日時：2020/09/06 23:50:30\r\nご注文番号：111\r\nお支払い合計：￥9,331\r\nお支払い方法：銀行振込\r\nご利用ポイント：0 pt\r\nお問い合わせ内容：\r\n\r\n************************************************\r\n　銀行口座情報\r\n************************************************\r\n\r\n銀行名：神戸信用金庫\r\n支店名：中央支店\r\n口座番号：0500384\r\n預金種別：普通\r\n口座名義：株式会社メディファイン\r\n\r\n************************************************\r\n　ご注文商品明細\r\n************************************************\r\n\r\n商品コード：cbd-300-10\r\n商品名：MARITIME CBDオイル 10ml (3%)  通常購入  \r\n単価：￥9,331\r\n数量：1\r\n\r\n\r\n-------------------------------------------------\r\n小　計：￥9,331\r\n\r\n手数料：￥0\r\n送　料：￥0\r\n============================================\r\n合　計：￥9,331\r\n\r\n************************************************\r\n　ご注文者情報\r\n************************************************\r\nお名前：関 歌織 様お名前(カナ)：セキ カオリ 様郵便番号：〒0630001\r\n住所：北海道札幌市西区山の手一条8丁目2-6\r\n電話番号：09062618809\r\nメールアドレス：kaononko@icloud.com\r\n\r\n************************************************\r\n　配送情報\r\n************************************************\r\n\r\n◎お届け先\r\nお名前：関 歌織 様お名前(カナ)：セキ カオリ 様郵便番号：〒0630001\r\n住所：北海道札幌市西区山の手一条8丁目2-6\r\n電話番号：09062618809\r\n\r\n配送方法：佐川急便\r\nお届け日：指定なし\r\nお届け時間：18時～21時\r\n\r\n商品コード：cbd-300-10\r\n商品名：MARITIME CBDオイル 10ml (3%)  通常購入  \r\n数量：1\r\n\r\n\r\n\r\n============================================\r\n\r\nこのメッセージはお客様へのお知らせ専用ですので、<br />\r\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\r\nご了承ください。<br />',NULL,'mailhistory'),
	(39,119,NULL,'2020-09-19 13:10:35','[MARITIME] ご注文ありがとうございます','大谷 昭子 様\nこの度はご注文いただき誠にありがとうございます。下記ご注文内容にお間違いがないかご確認下さい。\n\n************************************************\n　ご請求金額\n************************************************\n\nご注文日時：2020/09/19 22:07:02\nご注文番号：119\nお支払い合計：￥32,832\nお支払い方法：PayPal決済\nお問い合わせ内容：\n\n************************************************\n　ご注文商品明細\n************************************************\n\n商品コード：cbd-900\n商品名：MARITIME CBDオイル 30ml (3%)  通常購入  \n単価：￥22,464\n数量：1\n\n商品コード：cbd-300-10\n商品名：MARITIME CBDオイル 10ml (3%)  通常購入  \n単価：￥10,368\n数量：1\n\n\n-------------------------------------------------\n小　計：￥32,832\n\n手数料：￥0\n送　料：￥0\n============================================\n合　計：￥32,832\n\n************************************************\n　ご注文者情報\n************************************************\nお名前：大谷 昭子 様お名前(カナ)：オオタニ アキコ 様郵便番号：〒6892544\n住所：鳥取県東伯郡琴浦町箆津336-3\n電話番号：09022958296\nメールアドレス：zurich2010spring2@gmail.com\n\n************************************************\n　配送情報\n************************************************\n\n◎お届け先\nお名前：大谷 昭子 様お名前(カナ)：オオタニ アキコ 様郵便番号：〒6892544\n住所：鳥取県東伯郡琴浦町箆津336-3\n電話番号：09022958296\n\n配送方法：佐川急便\nお届け日：指定なし\nお届け時間：指定なし\n\n商品コード：cbd-900\n商品名：MARITIME CBDオイル 30ml (3%)  通常購入  \n数量：1\n\n商品コード：cbd-300-10\n商品名：MARITIME CBDオイル 10ml (3%)  通常購入  \n数量：1\n\n\n\n============================================\n\nこのメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n\n','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"title\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">この度はご注文いただき誠にありがとうございます。</div>\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            大谷 昭子 様<br/>\n                            <br/>\n                            下記ご注文内容にお間違いがないかご確認下さい。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご請求金額<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご注文日時：2020/09/19 22:07:02<br/>\n                            ご注文番号：119<br/>\n                            お支払い合計：￥32,832<br/>\n                            お支払い方法：PayPal決済<br/>\n                                                        お問い合わせ内容：<br/>\n                            <br/>\n                                                                                    <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご注文商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                            商品コード：cbd-900<br/>\n                                商品名：MARITIME CBDオイル 30ml (3%)  通常購入  <br/>\n                                単価：￥22,464<br/>\n                                数量：1<br/>\n                                <br/>\n                                                            商品コード：cbd-300-10<br/>\n                                商品名：MARITIME CBDオイル 10ml (3%)  通常購入  <br/>\n                                単価：￥10,368<br/>\n                                数量：1<br/>\n                                <br/>\n                                                        <hr style=\"border-top: 2px dashed #8c8b8b;\">\n                            小　計：￥32,832<br/>\n                            <br/>\n                            手数料：￥0<br/>\n                            送　料：￥0<br/>\n                                                        <hr style=\"border-top: 1px dotted #8c8b8b;\">\n                            合　計：￥32,832<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご注文者情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            お名前：大谷 昭子 様<br/>\n                            お名前(カナ)：オオタニ アキコ 様<br/>\n                                                        郵便番号：〒6892544<br/>\n                            住所：鳥取県東伯郡琴浦町箆津336-3<br/>\n                            電話番号：09022958296<br/>\n                            メールアドレス：zurich2010spring2@gmail.com<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　配送情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n\n                                                            ◎お届け先<br/>\n                                <br/>\n                                お名前：大谷 昭子 様<br/>\n                                お名前(カナ)：オオタニ アキコ 様<br/>\n                                                                郵便番号：〒6892544<br/>\n                                住所：鳥取県東伯郡琴浦町箆津336-3<br/>\n                                電話番号：09022958296<br/>\n                                <br/>\n                                配送方法：佐川急便<br/>\n                                お届け日：指定なし<br/>\n                                お届け時間：指定なし<br/>\n                                <br/>\n                                                                    商品コード：cbd-900<br/>\n                                    商品名：MARITIME CBDオイル 30ml (3%)  通常購入  <br/>\n                                    数量：1<br/>\n                                    <br/>\n                                                                    商品コード：cbd-300-10<br/>\n                                    商品名：MARITIME CBDオイル 10ml (3%)  通常購入  <br/>\n                                    数量：1<br/>\n                                    <br/>\n                                                                                                                    <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n','mailhistory'),
	(40,119,3,'2020-09-20 03:58:16','[MARITIME] 商品出荷のお知らせ','大谷 昭子 様\nお客さまがご注文された以下の商品を発送いたしました。商品の到着まで、今しばらくお待ちください。\n\nお問い合わせ番号：403921652120\nお問い合わせURL：https://k2k.sagawa-exp.co.jp/p/sagawa/web/okurijoinput.jsp\n\n************************************************\n　ご注文商品明細\n************************************************\n\n商品コード：cbd-900\n商品名：MARITIME CBDオイル 30ml (3%)  通常購入  \n数量：1\n\n商品コード：cbd-300-10\n商品名：MARITIME CBDオイル 10ml (3%)  通常購入  \n数量：1\n\n\n============================================\n\n************************************************\n　ご注文者情報\n************************************************\nお名前：大谷 昭子 様お名前(カナ)：オオタニ アキコ 様郵便番号：〒6892544\n住所：鳥取県東伯郡琴浦町箆津336-3\n電話番号：09022958296\n\n************************************************\n　配送情報\n************************************************\n\nお名前：大谷 昭子 様お名前(カナ)：オオタニ アキコ 様郵便番号：〒6892544\n住所：鳥取県東伯郡琴浦町箆津336-3\n電話番号：09022958296\n\nお届け日：指定なし\nお届け時間：指定なし\n','<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime-cbd.com/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"title\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:600;color:#374550;\">商品を発送いたしました。</div>\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            大谷 昭子 様<br>\n                            <br>\n                            MARITIMEでございます。<br/>\n                            お客さまがご注文された以下の商品を発送いたしました。商品の到着まで、今しばらくお待ちください。<br/>\n                            <br/>\n                                                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                お問い合わせ番号：403921652120\n                                                                    <br/>\n                                    お問い合わせURL：https://k2k.sagawa-exp.co.jp/p/sagawa/web/okurijoinput.jsp\n                                                                <br/>\n                                                        <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご注文商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                            商品コード：cbd-900<br/>\n                                商品名：MARITIME CBDオイル 30ml (3%)  通常購入  <br/>\n                                数量：1<br/>\n                                <br/>\n                                                            商品コード：cbd-300-10<br/>\n                                商品名：MARITIME CBDオイル 10ml (3%)  通常購入  <br/>\n                                数量：1<br/>\n                                <br/>\n                                                        <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご注文者情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            お名前：大谷 昭子 様<br/>\n                            お名前(カナ)：オオタニ アキコ 様<br/>\n                                                        郵便番号：〒6892544<br/>\n                            住所：鳥取県東伯郡琴浦町箆津336-3<br/>\n                            電話番号：09022958296<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　配送情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            お名前：大谷 昭子 様<br/>\n                            お名前(カナ)：オオタニ アキコ 様<br/>\n                                                        郵便番号：〒6892544<br/>\n                            住所：鳥取県東伯郡琴浦町箆津336-3<br/>\n                            電話番号：09022958296<br/>\n                            <br/>\n                            お届け日：指定なし<br/>\n                            お届け時間：指定なし<br/>\n                            <br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime-cbd.com/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n\n','mailhistory');

/*!40000 ALTER TABLE `dtb_mail_history` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dtb_mail_template
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dtb_mail_template`;

CREATE TABLE `dtb_mail_template` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `creator_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `mail_subject` varchar(255) DEFAULT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1CB16DB261220EA6` (`creator_id`),
  CONSTRAINT `FK_1CB16DB261220EA6` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dtb_mail_template` WRITE;
/*!40000 ALTER TABLE `dtb_mail_template` DISABLE KEYS */;

INSERT INTO `dtb_mail_template` (`id`, `creator_id`, `name`, `file_name`, `mail_subject`, `create_date`, `update_date`, `discriminator_type`)
VALUES
	(1,NULL,'注文受付メール','Mail/order.twig','ご注文ありがとうございます','2017-03-07 10:14:52','2017-03-07 10:14:52','mailtemplate'),
	(2,NULL,'会員仮登録メール','Mail/entry_confirm.twig','会員登録のご確認','2017-03-07 10:14:52','2017-03-07 10:14:52','mailtemplate'),
	(3,NULL,'会員本登録メール','Mail/entry_complete.twig','会員登録が完了しました。','2017-03-07 10:14:52','2017-03-07 10:14:52','mailtemplate'),
	(4,NULL,'会員退会メール','Mail/customer_withdraw_mail.twig','退会手続きのご完了','2017-03-07 10:14:52','2017-03-07 10:14:52','mailtemplate'),
	(5,NULL,'問合受付メール','Mail/contact_mail.twig','お問い合わせを受け付けました。','2017-03-07 10:14:52','2017-03-07 10:14:52','mailtemplate'),
	(6,NULL,'パスワードリセット','Mail/forgot_mail.twig','パスワード変更のご確認','2017-03-07 10:14:52','2017-03-07 10:14:52','mailtemplate'),
	(7,NULL,'パスワードリマインダー','Mail/reset_complete_mail.twig','パスワード変更のお知らせ','2017-03-07 10:14:52','2017-03-07 10:14:52','mailtemplate'),
	(8,NULL,'出荷通知メール','Mail/shipping_notify.twig','商品出荷のお知らせ','2017-03-07 10:14:52','2017-03-07 10:14:52','mailtemplate'),
	(11,1,'商品予約メール','Reserve4/order_reservation.twig','ご予約注文ありがとうございます','2020-07-15 10:48:17','2020-07-15 10:48:17','mailtemplate'),
	(12,1,'発売予定日変更通知メール','Reserve4/reservation_shipping_change.twig','予約商品の発送予定日変更のお知らせ','2020-07-15 10:48:18','2020-07-15 10:48:18','mailtemplate'),
	(16,1,'カゴ落ちメール','CheckKagoochi/Resource/template/default/Mail/kagoochi_notify.twig','ショッピングカートに入ったままのアイテムがあります','2020-09-04 04:02:12','2020-09-09 09:30:24','mailtemplate'),
	(17,1,'代理店問合受付メール','Mail/partner_mail.twig','代理店お問い合わせを受け付けました。','2020-09-04 03:50:40','2020-09-09 09:30:47','mailtemplate'),
	(18,1,'レビューメール','Mail/review_reminder.twig','いつもご利用ありがとうございます','2020-09-19 11:41:20','2020-09-19 11:41:20','mailtemplate');

/*!40000 ALTER TABLE `dtb_mail_template` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dtb_member
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dtb_member`;

CREATE TABLE `dtb_member` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `work_id` smallint(5) unsigned DEFAULT NULL,
  `authority_id` smallint(5) unsigned DEFAULT NULL,
  `creator_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  `login_id` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `sort_no` smallint(5) unsigned NOT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `login_date` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_10BC3BE6BB3453DB` (`work_id`),
  KEY `IDX_10BC3BE681EC865B` (`authority_id`),
  KEY `IDX_10BC3BE661220EA6` (`creator_id`),
  CONSTRAINT `FK_10BC3BE661220EA6` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`id`),
  CONSTRAINT `FK_10BC3BE681EC865B` FOREIGN KEY (`authority_id`) REFERENCES `mtb_authority` (`id`),
  CONSTRAINT `FK_10BC3BE6BB3453DB` FOREIGN KEY (`work_id`) REFERENCES `mtb_work` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dtb_member` WRITE;
/*!40000 ALTER TABLE `dtb_member` DISABLE KEYS */;

INSERT INTO `dtb_member` (`id`, `work_id`, `authority_id`, `creator_id`, `name`, `department`, `login_id`, `password`, `salt`, `sort_no`, `create_date`, `update_date`, `login_date`, `discriminator_type`)
VALUES
	(1,1,0,1,'管理者','admin','admin','9b989631c240ba0c36b9b766a6f6b7b48deb94f72a2073ae4be5c30d73e0505c','rXj52vFKQ8q1gJPLSWt65N9kqi1sAbkB',1,'2020-04-22 10:19:03','2020-09-20 03:52:18','2020-09-20 03:52:18','member'),
	(2,1,0,2,'Shourai','Shourai Ltd','shourai','05781e8c0ace3fc14c77695a8bba8fd2fd015ff7b756fe3d3d78679213f14db0','e14ef5cdf6',2,'2020-06-27 03:01:20','2020-07-16 06:54:36','2020-07-16 06:54:36','member'),
	(3,1,1,3,'maritime','Maritime','maritime','0c9ebf3da8eea07e193d8a4a1ba0c9bae66b505219efbf4cc81338c578b679e3','6bd961e649',3,'2020-06-27 03:03:37','2020-09-20 01:20:06','2020-09-20 01:20:06','member');

/*!40000 ALTER TABLE `dtb_member` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dtb_news
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dtb_news`;

CREATE TABLE `dtb_news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `creator_id` int(10) unsigned DEFAULT NULL,
  `publish_date` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `title` varchar(255) NOT NULL,
  `description` longtext DEFAULT NULL,
  `url` varchar(4000) DEFAULT NULL,
  `link_method` tinyint(1) NOT NULL DEFAULT 0,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `visible` tinyint(1) NOT NULL DEFAULT 1,
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_EA4C351761220EA6` (`creator_id`),
  CONSTRAINT `FK_EA4C351761220EA6` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table dtb_order
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dtb_order`;

CREATE TABLE `dtb_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `country_id` smallint(5) unsigned DEFAULT NULL,
  `pref_id` smallint(5) unsigned DEFAULT NULL,
  `sex_id` smallint(5) unsigned DEFAULT NULL,
  `job_id` smallint(5) unsigned DEFAULT NULL,
  `payment_id` int(10) unsigned DEFAULT NULL,
  `device_type_id` smallint(5) unsigned DEFAULT NULL,
  `pre_order_id` varchar(255) DEFAULT NULL,
  `order_no` varchar(255) DEFAULT NULL,
  `message` varchar(4000) DEFAULT NULL,
  `name01` varchar(255) NOT NULL,
  `name02` varchar(255) NOT NULL,
  `kana01` varchar(255) DEFAULT NULL,
  `kana02` varchar(255) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone_number` varchar(14) DEFAULT NULL,
  `postal_code` varchar(8) DEFAULT NULL,
  `addr01` varchar(255) DEFAULT NULL,
  `addr02` varchar(255) DEFAULT NULL,
  `birth` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `subtotal` decimal(12,2) unsigned NOT NULL DEFAULT 0.00,
  `discount` decimal(12,2) unsigned NOT NULL DEFAULT 0.00,
  `delivery_fee_total` decimal(12,2) unsigned NOT NULL DEFAULT 0.00,
  `charge` decimal(12,2) unsigned NOT NULL DEFAULT 0.00,
  `tax` decimal(12,2) unsigned NOT NULL DEFAULT 0.00,
  `total` decimal(12,2) unsigned NOT NULL DEFAULT 0.00,
  `payment_total` decimal(12,2) unsigned NOT NULL DEFAULT 0.00,
  `payment_method` varchar(255) DEFAULT NULL,
  `note` varchar(4000) DEFAULT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `order_date` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `payment_date` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `currency_code` varchar(255) DEFAULT NULL,
  `complete_message` longtext DEFAULT NULL,
  `complete_mail_message` longtext DEFAULT NULL,
  `add_point` decimal(12,0) unsigned NOT NULL DEFAULT 0,
  `use_point` decimal(12,0) unsigned NOT NULL DEFAULT 0,
  `order_status_id` smallint(5) unsigned DEFAULT NULL,
  `discriminator_type` varchar(255) NOT NULL,
  `gmo_payment_gateway_payment_status` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dtb_order_pre_order_id_idx` (`pre_order_id`),
  KEY `IDX_1D66D8079395C3F3` (`customer_id`),
  KEY `IDX_1D66D807F92F3E70` (`country_id`),
  KEY `IDX_1D66D807E171EF5F` (`pref_id`),
  KEY `IDX_1D66D8075A2DB2A0` (`sex_id`),
  KEY `IDX_1D66D807BE04EA9` (`job_id`),
  KEY `IDX_1D66D8074C3A3BB` (`payment_id`),
  KEY `IDX_1D66D8074FFA550E` (`device_type_id`),
  KEY `IDX_1D66D807D7707B45` (`order_status_id`),
  KEY `dtb_order_email_idx` (`email`),
  KEY `dtb_order_order_date_idx` (`order_date`),
  KEY `dtb_order_payment_date_idx` (`payment_date`),
  KEY `dtb_order_update_date_idx` (`update_date`),
  KEY `dtb_order_order_no_idx` (`order_no`),
  CONSTRAINT `FK_1D66D8074C3A3BB` FOREIGN KEY (`payment_id`) REFERENCES `dtb_payment` (`id`),
  CONSTRAINT `FK_1D66D8074FFA550E` FOREIGN KEY (`device_type_id`) REFERENCES `mtb_device_type` (`id`),
  CONSTRAINT `FK_1D66D8075A2DB2A0` FOREIGN KEY (`sex_id`) REFERENCES `mtb_sex` (`id`),
  CONSTRAINT `FK_1D66D8079395C3F3` FOREIGN KEY (`customer_id`) REFERENCES `dtb_customer` (`id`),
  CONSTRAINT `FK_1D66D807BE04EA9` FOREIGN KEY (`job_id`) REFERENCES `mtb_job` (`id`),
  CONSTRAINT `FK_1D66D807E171EF5F` FOREIGN KEY (`pref_id`) REFERENCES `mtb_pref` (`id`),
  CONSTRAINT `FK_1D66D807F92F3E70` FOREIGN KEY (`country_id`) REFERENCES `mtb_country` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dtb_order` WRITE;
/*!40000 ALTER TABLE `dtb_order` DISABLE KEYS */;

INSERT INTO `dtb_order` (`id`, `customer_id`, `country_id`, `pref_id`, `sex_id`, `job_id`, `payment_id`, `device_type_id`, `pre_order_id`, `order_no`, `message`, `name01`, `name02`, `kana01`, `kana02`, `company_name`, `email`, `phone_number`, `postal_code`, `addr01`, `addr02`, `birth`, `subtotal`, `discount`, `delivery_fee_total`, `charge`, `tax`, `total`, `payment_total`, `payment_method`, `note`, `create_date`, `update_date`, `order_date`, `payment_date`, `currency_code`, `complete_message`, `complete_mail_message`, `add_point`, `use_point`, `order_status_id`, `discriminator_type`, `gmo_payment_gateway_payment_status`)
VALUES
	(1,1,NULL,13,1,2,2,10,'3405d10759c7e3ee9457fa85c02887c2dab9924e','1',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',5400.00,0.00,1000.00,0.00,474.00,6400.00,6400.00,'現金書留',NULL,'2020-05-01 08:34:15','2020-05-01 08:35:02',NULL,NULL,'JPY',NULL,NULL,50,0,8,'order',NULL),
	(2,1,NULL,13,1,2,1,10,'3f31a6a5b4d84e18496f8a9673ade4b2c029b58e',NULL,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',16199.00,0.00,1000.00,0.00,1274.00,17199.00,17199.00,'郵便振替',NULL,'2020-05-01 10:21:41','2020-05-01 10:21:41',NULL,NULL,'JPY',NULL,NULL,150,0,8,'order',NULL),
	(3,1,NULL,13,1,2,5,10,'10049bd9a0d867fe8648860e5e6dcfd84c21683a','3',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',8800.00,0.00,0.00,0.00,800.00,8800.00,8800.00,'クレジット決済',NULL,'2020-05-15 00:56:37','2020-05-15 01:00:32',NULL,NULL,'JPY',NULL,NULL,80,0,8,'order',NULL),
	(11,1,NULL,13,1,2,5,10,'550567c8624190c11b209c8204181fcd5a870140',NULL,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',17600.00,0.00,0.00,0.00,1600.00,17600.00,17600.00,'クレジット決済',NULL,'2020-05-15 05:56:00','2020-05-15 05:56:00',NULL,NULL,'JPY',NULL,NULL,160,0,8,'order',NULL),
	(12,1,NULL,13,1,2,5,10,'4756e56604e04c982ef97cedcab34c7fbe00c3f1','12',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',8800.00,0.00,0.00,0.00,800.00,8800.00,8800.00,'クレジット決済',NULL,'2020-05-15 05:58:24','2020-05-15 05:59:22',NULL,NULL,'JPY',NULL,NULL,80,0,8,'order',NULL),
	(13,1,NULL,13,1,2,5,10,'6d86fbfcfd4922520daeab7e96266f82c7c013ed','13',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',8800.00,0.00,1000.00,0.00,891.00,9800.00,9800.00,'クレジット決済',NULL,'2020-05-15 06:17:31','2020-05-15 09:14:03',NULL,NULL,'JPY',NULL,NULL,80,0,8,'order',NULL),
	(14,1,NULL,13,1,2,12,10,'480aadd3e52b07d34bb96f7222ce783ef09c5dca','14',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',2200.00,0.00,1000.00,0.00,291.00,3200.00,3200.00,'楽天ペイ',NULL,'2020-05-15 06:21:21','2020-05-15 09:14:24',NULL,NULL,'JPY',NULL,NULL,20,0,8,'order',NULL),
	(15,1,NULL,13,1,2,5,10,'c53920c7faf8b9a68a80c0580a621d9c185f7c57','15',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',8800.00,0.00,1000.00,0.00,891.00,9800.00,9800.00,'クレジット決済',NULL,'2020-05-16 10:34:59','2020-05-16 10:37:36',NULL,NULL,'JPY',NULL,NULL,80,0,8,'order',NULL),
	(16,1,NULL,13,1,2,12,10,'daafa2f8f6e88400a43802d6e07fbf85a4198289',NULL,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',2200.00,0.00,1000.00,0.00,291.00,3200.00,3200.00,'楽天ペイ',NULL,'2020-05-16 10:38:03','2020-05-16 10:38:03',NULL,NULL,'JPY',NULL,NULL,20,0,8,'order',NULL),
	(17,NULL,NULL,13,NULL,NULL,5,10,'fc6dd8474325aaf74d4910b9cb7a96bf019be48e',NULL,NULL,'Gues','User','グエスト','ユーザ',NULL,'freddy@shourai.io','0123456789','1570077','世田谷区鎌田','Test, 99',NULL,8800.00,0.00,1000.00,0.00,891.00,9800.00,9800.00,'クレジット決済',NULL,'2020-05-17 02:08:21','2020-05-17 02:08:21',NULL,NULL,'JPY',NULL,NULL,0,0,8,'order',NULL),
	(18,NULL,NULL,13,NULL,NULL,5,10,'3370ac78203036923bc6c36f05993a65e6df5812','18',NULL,'Gues','User','グエスト','ユーザ',NULL,'freddy@shourai.io','0123456789','1570077','世田谷区鎌田','Test, 99',NULL,13200.00,0.00,1000.00,0.00,1291.00,14200.00,14200.00,'クレジット決済',NULL,'2020-05-17 02:09:43','2020-05-17 02:13:33',NULL,NULL,'JPY',NULL,NULL,0,0,8,'order',NULL),
	(19,1,NULL,13,1,2,5,10,'0989047a9e7cc5adc9c6399cff60ff1243f83705','19',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',8800.00,880.00,1000.00,0.00,891.00,8920.00,8920.00,'クレジット決済',NULL,'2020-05-22 03:44:49','2020-05-23 07:20:45',NULL,NULL,'JPY',NULL,'***********************************************\n　クーポン情報                                 \n***********************************************\n\nクーポンコード: maritime10 初回10％割引オファー\n',71,0,8,'order',NULL),
	(20,1,NULL,13,1,2,5,10,'c649f5cd3b96fb09e1ea93442980dfaa4eb4516d',NULL,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',2200.00,0.00,1000.00,0.00,291.00,3200.00,3200.00,'クレジット決済',NULL,'2020-05-23 07:21:25','2020-05-23 07:21:25',NULL,NULL,'JPY',NULL,NULL,20,0,8,'order',NULL),
	(21,1,NULL,13,1,2,14,10,'fe227529f9ab22a80cc545330ab1b50cf883e99d','21',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',1117.00,0.00,1000.00,0.00,193.00,2117.00,2117.00,'PayPal決済',NULL,'2020-05-24 08:47:10','2020-05-24 08:47:51',NULL,NULL,'JPY',NULL,NULL,10,0,8,'order',NULL),
	(22,1,NULL,13,1,2,13,10,'d7210bcfb2f6606e439575d5c378bfc5b6430a3e','22',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',8800.00,0.00,1000.00,0.00,891.00,9800.00,9800.00,'クレジット決済「定期購入」',NULL,'2020-05-26 12:05:15','2020-05-27 00:49:54',NULL,NULL,'JPY',NULL,NULL,80,0,8,'order',NULL),
	(23,1,NULL,13,1,2,13,10,'74539e45951df6064bdef5809dbc29425045b889','23',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',31350.00,3135.00,1000.00,0.00,2941.00,29215.00,29215.00,'クレジット決済「定期購入」',NULL,'2020-05-27 11:46:11','2020-05-27 12:19:05',NULL,NULL,'JPY',NULL,'***********************************************\n　クーポン情報                                 \n***********************************************\n\nクーポンコード: MARITIME10 初回10％割引オファー\n',254,0,8,'order',NULL),
	(24,1,NULL,13,1,2,13,10,'6e13fd81af63c888cfe1d0f8a2e0e8b83709be16','24',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',30780.00,0.00,800.00,0.00,2339.00,31580.00,31580.00,'クレジット決済「定期購入」',NULL,'2020-06-03 07:17:59','2020-06-03 07:22:39',NULL,NULL,'JPY',NULL,NULL,285,0,8,'order',NULL),
	(25,1,NULL,13,1,2,13,10,'1bcf6c4ff255168f80ce3bb029a91695f68e0114','25',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',61560.00,0.00,1600.00,0.00,4678.00,63160.00,63160.00,'クレジット決済「定期購入」',NULL,'2020-06-03 07:24:32','2020-06-04 04:25:36',NULL,NULL,'JPY',NULL,NULL,570,0,8,'order',NULL),
	(26,4,NULL,13,NULL,NULL,5,10,'4edcf3ce6b3fa55036eae826e76db1aa8cf55068',NULL,NULL,'Test2','Test','カナ','カナ',NULL,'05049pyj+1@gmail.com','11122223333','1000001','千代田区千代田','1-1-1',NULL,12960.00,0.00,800.00,0.00,1019.00,13760.00,13760.00,'クレジット決済',NULL,'2020-06-03 10:21:34','2020-06-03 10:21:34',NULL,NULL,'JPY',NULL,NULL,120,0,8,'order',NULL),
	(27,4,NULL,13,NULL,NULL,5,10,'073d1b69375f57705cd91d5a2a6ca349fa20e8c8',NULL,NULL,'Test2','Test','カナ','カナ',NULL,'05049pyj+1@gmail.com','11122223333','1000001','千代田区千代田','1-1-1',NULL,15552.00,0.00,800.00,0.00,1211.00,16352.00,16352.00,'クレジット決済',NULL,'2020-06-03 14:27:51','2020-06-03 14:27:51',NULL,NULL,'JPY',NULL,NULL,144,0,8,'order',NULL),
	(28,4,NULL,13,NULL,NULL,5,10,'1909097e66d89d39bb29e5bd310dad8d23773403',NULL,NULL,'Test2','Test','カナ','カナ',NULL,'05049pyj+1@gmail.com','11122223333','1000001','千代田区千代田','1-1-1',NULL,10368.00,0.00,800.00,0.00,827.00,11168.00,11168.00,'クレジット決済',NULL,'2020-06-03 14:52:55','2020-06-03 14:52:55',NULL,NULL,'JPY',NULL,NULL,96,0,8,'order',NULL),
	(29,4,NULL,13,NULL,NULL,5,10,'32df25ce8635602c52c6449fd942996169ff2d88',NULL,NULL,'Test2','Test','カナ','カナ',NULL,'05049pyj+1@gmail.com','11122223333','1000001','千代田区千代田','1-1-1',NULL,15552.00,0.00,800.00,0.00,1211.00,16352.00,16352.00,'クレジット決済',NULL,'2020-06-03 14:53:38','2020-06-03 14:53:38',NULL,NULL,'JPY',NULL,NULL,144,0,8,'order',NULL),
	(30,4,NULL,13,NULL,NULL,5,10,'b58fee34a2a7091f1bafcf35b9aea0f1e8b96e69',NULL,NULL,'Test2','Test','カナ','カナ',NULL,'05049pyj+1@gmail.com','11122223333','1000001','千代田区千代田','1-1-1',NULL,10368.00,0.00,800.00,0.00,827.00,11168.00,11168.00,'クレジット決済',NULL,'2020-06-03 14:55:48','2020-06-03 14:55:48',NULL,NULL,'JPY',NULL,NULL,96,0,8,'order',NULL),
	(31,4,NULL,13,NULL,NULL,5,10,'473e86b7fdd62a87ef4fdbd1d85a5f45622b7793',NULL,NULL,'Test2','Test','カナ','カナ',NULL,'05049pyj+1@gmail.com','11122223333','1000001','千代田区千代田','1-1-1',NULL,15552.00,0.00,800.00,0.00,1211.00,16352.00,16352.00,'クレジット決済',NULL,'2020-06-03 15:04:08','2020-06-03 15:04:08',NULL,NULL,'JPY',NULL,NULL,144,0,8,'order',NULL),
	(32,4,NULL,13,NULL,NULL,5,10,'0e434821127e4e9eacbd1cae13673becf2b760a5','32',NULL,'Test2','Test','カナ','カナ',NULL,'05049pyj+1@gmail.com','11122223333','1000001','千代田区千代田','1-1-1',NULL,20736.00,0.00,0.00,0.00,1536.00,20736.00,20736.00,'クレジット決済',NULL,'2020-06-03 15:05:19','2020-06-03 15:06:01',NULL,NULL,'JPY',NULL,NULL,192,0,8,'order',NULL),
	(33,4,NULL,13,NULL,NULL,5,10,'47b98c2d77fef10df637dce0b1db99be4ac64d37',NULL,NULL,'Test2','Test','カナ','カナ',NULL,'05049pyj+1@gmail.com','11122223333','1000001','千代田区千代田','1-1-1',NULL,10368.00,0.00,1600.00,0.00,886.00,11968.00,11968.00,'クレジット決済',NULL,'2020-06-03 15:06:18','2020-06-03 15:06:18',NULL,NULL,'JPY',NULL,NULL,96,0,8,'order',NULL),
	(34,4,NULL,13,NULL,NULL,5,10,'97650a8d5a7563484a2cc0f7552c3dacfcf17579',NULL,NULL,'Test2','Test','カナ','カナ',NULL,'05049pyj+1@gmail.com','11122223333','1000001','千代田区千代田','1-1-1',NULL,12874.00,0.00,0.00,0.00,954.00,12874.00,12874.00,'クレジット決済',NULL,'2020-06-03 15:07:46','2020-06-03 15:07:46',NULL,NULL,'JPY',NULL,NULL,120,0,8,'order',NULL),
	(35,4,NULL,13,NULL,NULL,13,10,'6ac5a3bafb2d37882d7c10c17dfd8ef518bb6e5a',NULL,NULL,'Test2','Test','カナ','カナ',NULL,'05049pyj+1@gmail.com','11122223333','1000001','千代田区千代田','1-1-1',NULL,1880.00,0.00,1600.00,0.00,258.00,3480.00,3480.00,'クレジット決済「定期購入」',NULL,'2020-06-03 15:08:05','2020-06-03 15:08:05',NULL,NULL,'JPY',NULL,NULL,18,0,8,'order',NULL),
	(36,4,NULL,13,NULL,NULL,5,10,'197db9774c89e3eab46733e49a395ff1fee5a871',NULL,NULL,'Test2','Test','カナ','カナ',NULL,'05049pyj+1@gmail.com','11122223333','1000001','千代田区千代田','1-1-1',NULL,5012.00,0.00,0.00,0.00,372.00,5012.00,5012.00,'クレジット決済',NULL,'2020-06-03 15:55:19','2020-06-03 15:55:19',NULL,NULL,'JPY',NULL,NULL,48,0,8,'order',NULL),
	(37,1,NULL,13,1,2,5,10,'dd9f417b8728fa7e1b175fbc09cdb4c1232b80a9',NULL,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',4536.00,0.00,800.00,0.00,395.00,5336.00,5336.00,'クレジット決済',NULL,'2020-06-04 06:42:16','2020-06-04 06:42:16',NULL,NULL,'JPY',NULL,NULL,42,0,8,'order',NULL),
	(38,1,NULL,13,1,2,5,10,'01a197b164dac61880fc4e775e1ba14753225413',NULL,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',9072.00,0.00,1600.00,0.00,790.00,10672.00,10672.00,'クレジット決済',NULL,'2020-06-04 06:42:56','2020-06-04 06:42:56',NULL,NULL,'JPY',NULL,NULL,84,0,8,'order',NULL),
	(39,1,NULL,13,1,2,5,10,'3218fee57e5b4cf18f5e69190d2a999bb28fd0d8',NULL,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',27216.00,0.00,0.00,0.00,2016.00,27216.00,27216.00,'クレジット決済',NULL,'2020-06-04 06:43:15','2020-06-04 06:43:15',NULL,NULL,'JPY',NULL,NULL,252,0,8,'order',NULL),
	(40,1,NULL,13,1,2,5,2,'65218a9de6bcc8763b6c4adb493a4bdaa6e37b4d','40',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',4536.00,0.00,800.00,0.00,395.00,5336.00,5336.00,'クレジット決済',NULL,'2020-06-05 08:12:29','2020-06-05 08:12:58',NULL,NULL,'JPY',NULL,NULL,42,0,8,'order',NULL),
	(41,1,NULL,13,1,2,5,10,'344e80fdfcbe4796379b988856c2669fe1c04855',NULL,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',5632.00,0.00,1600.00,0.00,535.00,7232.00,7232.00,'クレジット決済',NULL,'2020-06-05 09:06:16','2020-06-05 09:06:16',NULL,NULL,'JPY',NULL,NULL,52,0,8,'order',NULL),
	(42,1,NULL,13,1,2,5,10,'6190095275e11343f58e229a928bc6910323702c',NULL,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',10168.00,0.00,0.00,0.00,753.00,10168.00,10168.00,'クレジット決済',NULL,'2020-06-05 09:06:42','2020-06-05 09:06:42',NULL,NULL,'JPY',NULL,NULL,94,0,8,'order',NULL),
	(43,1,NULL,13,1,2,5,10,'a37fbd87f4a0ff8b2e220aedb07fb8ff9a8f69b3','43',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',9072.00,0.00,1600.00,0.00,790.00,10672.00,10672.00,'クレジット決済',NULL,'2020-06-05 09:10:56','2020-06-05 09:16:54',NULL,NULL,'JPY',NULL,NULL,84,0,8,'order',NULL),
	(44,1,NULL,13,1,2,13,10,'b5194dcaed88ff2ed937ce4897cd8251a34fd2ba',NULL,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',940.00,0.00,800.00,0.00,129.00,1740.00,1740.00,'クレジット決済「定期購入」',NULL,'2020-06-05 09:19:05','2020-06-05 09:19:05',NULL,NULL,'JPY',NULL,NULL,9,0,8,'order',NULL),
	(45,1,NULL,13,1,2,5,2,'b2a7b573eac6251775f68b71a36e18cebe25f69f',NULL,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',10168.00,0.00,0.00,0.00,753.00,10168.00,10168.00,'クレジット決済',NULL,'2020-06-05 09:25:32','2020-06-05 09:25:32',NULL,NULL,'JPY',NULL,NULL,94,0,8,'order',NULL),
	(46,7,NULL,13,NULL,NULL,14,10,'33b328a85cf1e7aeab428d07e5dce8d56a711871','46',NULL,'杉森','浩一郎','スギモリ','コウイチロウ',NULL,'cedarforest@me.com','0337128123','1520004','目黒区鷹番','1-8-17',NULL,4536.00,0.00,0.00,0.00,336.00,4536.00,4536.00,'PayPal決済',NULL,'2020-06-17 08:23:58','2020-06-17 08:26:06',NULL,NULL,'JPY',NULL,NULL,42,0,8,'order',NULL),
	(47,7,NULL,13,NULL,NULL,14,10,'bb25d59a008c58762b117e2f362cfa258841f038','47',NULL,'杉森','浩一郎','スギモリ','コウイチロウ',NULL,'cedarforest@me.com','0337128123','1520004','目黒区鷹番','1-8-17',NULL,5480.00,0.00,0.00,0.00,405.00,5480.00,5480.00,'PayPal決済',NULL,'2020-06-17 08:30:59','2020-06-17 08:31:54',NULL,NULL,'JPY',NULL,NULL,50,0,8,'order',NULL),
	(48,1,NULL,13,1,2,14,10,'fd271c8c7a1cd18ade251779dd4775b2a4ff207e','48','test','Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'dungutsi@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',10168.00,500.00,0.00,0.00,753.00,9668.00,9668.00,'PayPal決済',NULL,'2020-06-17 11:12:30','2020-09-15 02:41:26','2020-06-17 11:17:55','2020-06-17 11:17:55','JPY',NULL,NULL,89,500,3,'order',NULL),
	(49,1,NULL,13,1,2,14,10,'8013d103cd30ad6350fb7f4ad3f655dbacc66497','49',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',940.00,0.00,800.00,0.00,129.00,1740.00,1740.00,'PayPal決済',NULL,'2020-06-17 11:45:49','2020-06-27 02:55:37','2020-06-18 10:38:39','2020-06-18 10:38:39','JPY',NULL,NULL,9,0,3,'order',NULL),
	(50,1,NULL,13,1,2,14,10,'f7d371af0848946a9ec960bda35441eab18b9cd5','50',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',0.00,0.00,0.00,0.00,0.00,0.00,0.00,'PayPal決済',NULL,'2020-06-19 07:12:32','2020-06-19 07:13:07',NULL,NULL,'JPY',NULL,NULL,0,0,8,'order',NULL),
	(51,1,NULL,13,1,2,14,10,'c829e3b8a3711df82a7b0d5c8ce07329470ba219','51',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',4666.00,0.00,0.00,0.00,346.00,4666.00,4666.00,'PayPal決済',NULL,'2020-06-19 07:16:19','2020-06-19 07:18:07',NULL,NULL,'JPY',NULL,NULL,43,0,8,'order',NULL),
	(52,1,NULL,13,1,2,14,2,'8c0e829d3b863abf01aaa647d508b1863c5b27af','52','よろしく','Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',50836.00,0.00,0.00,0.00,3766.00,50836.00,50836.00,'PayPal決済',NULL,'2020-06-26 01:47:50','2020-06-26 01:48:22',NULL,NULL,'JPY',NULL,NULL,471,0,8,'order',NULL),
	(53,1,NULL,13,1,2,14,2,'a810de0e38c4157fcf7128ad41338c49b7940611','53',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',50836.00,0.00,0.00,0.00,3766.00,50836.00,50836.00,'PayPal決済',NULL,'2020-06-26 01:54:54','2020-06-26 02:09:09',NULL,NULL,'JPY',NULL,NULL,471,0,8,'order',NULL),
	(54,8,NULL,13,1,2,14,10,'dc13e6c8e8d8d3a0ca8dc6c688e892d9a203bca4','54','杉森からの本番オーダーです。よろしくお願いします。','杉森','浩一郎','スギモリ','コウイチロウ',NULL,'ko@thepmi.net','0337128235','1520004','目黒区鷹番','1-8-17',NULL,15659.00,0.00,0.00,0.00,1160.00,15659.00,15659.00,'PayPal決済',NULL,'2020-06-26 09:12:42','2020-08-03 02:06:03','2020-06-26 09:16:09','2020-06-26 09:16:09','JPY',NULL,NULL,145,0,5,'order',NULL),
	(55,1,NULL,13,1,2,14,10,'aa9440ce767e366b79ab2fc9a5d54bee640153bb','55',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',46170.00,0.00,0.00,0.00,3420.00,46170.00,46170.00,'PayPal決済',NULL,'2020-06-30 10:59:21','2020-06-30 11:00:31',NULL,NULL,'JPY',NULL,NULL,428,0,8,'order',NULL),
	(56,1,NULL,13,1,2,5,10,'ac22d7d336c8d32d13609287a6d6364bdb45f789','56',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',50836.00,0.00,0.00,0.00,3766.00,50836.00,50836.00,'クレジット決済',NULL,'2020-07-01 07:28:50','2020-07-02 03:00:55',NULL,NULL,'JPY',NULL,NULL,471,0,8,'order',NULL),
	(57,8,NULL,13,1,2,14,10,'e44fc663c3f80416bc26ae5afcd0ea744b25f531','57',NULL,'杉森','浩一郎','スギモリ','コウイチロウ',NULL,'ko@thepmi.net','0337128235','1520004','目黒区鷹番','1-8-17',NULL,4666.00,0.00,0.00,0.00,346.00,4666.00,4666.00,'PayPal決済',NULL,'2020-07-01 19:04:09','2020-07-01 19:04:33',NULL,NULL,'JPY',NULL,NULL,43,0,8,'order',NULL),
	(58,9,NULL,28,1,7,14,10,'5c067121a611687840ef8b21dfa29377b9b067d8','58',NULL,'永井','光弘','ナガイ','ミツヒロ',NULL,'ny42nagai@gmail.com','09083772905','6780005','相生市大石町','1-27','1964-02-29 15:00:00',27760.00,0.00,0.00,0.00,2056.00,27760.00,27760.00,'PayPal決済',NULL,'2020-07-02 03:45:32','2020-07-02 03:49:18',NULL,NULL,'JPY',NULL,NULL,258,0,8,'order',NULL),
	(59,8,NULL,13,1,2,14,10,'d8a88d948c105686945f15767f35d8b3c01635bf','59',NULL,'杉森','浩一郎','スギモリ','コウイチロウ',NULL,'ko@thepmi.net','0337128235','1520004','目黒区鷹番','1-8-17',NULL,22356.00,0.00,0.00,0.00,1656.00,22356.00,22356.00,'PayPal決済',NULL,'2020-07-03 08:38:44','2020-07-03 08:38:54',NULL,NULL,'JPY',NULL,NULL,207,0,8,'order',NULL),
	(60,1,NULL,13,1,2,14,10,'5bfce8981f89eef86110ec87b8084ad09ed93ca7','60',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',46170.00,0.00,0.00,0.00,3420.00,46170.00,46170.00,'PayPal決済',NULL,'2020-07-10 03:36:53','2020-07-10 03:39:53',NULL,NULL,'JPY',NULL,NULL,428,0,8,'order',NULL),
	(61,1,NULL,13,1,2,14,10,'b72750e1f5de7bafc2c33e3dfda33cb47320e739','61',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',92340.00,0.00,0.00,0.00,6840.00,92340.00,92340.00,'PayPal決済',NULL,'2020-07-16 09:16:24','2020-07-16 09:17:19',NULL,NULL,'JPY',NULL,NULL,856,0,8,'order',NULL),
	(62,1,NULL,13,1,2,15,10,'3ec7a33133b63cab20849460f32c4f65fb3a6e6b',NULL,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',92340.00,0.00,0.00,0.00,6840.00,92340.00,92340.00,'かんたん銀行決済(PayPal)',NULL,'2020-07-17 02:37:36','2020-07-17 02:37:36',NULL,NULL,'JPY',NULL,NULL,856,0,8,'order',NULL),
	(63,1,NULL,13,1,2,14,10,'8bbd45270984032d6fa318cf6616ec12771ed0d2',NULL,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',92340.00,0.00,0.00,0.00,6840.00,92340.00,92340.00,'PayPal決済',NULL,'2020-07-17 02:54:57','2020-07-17 02:54:57',NULL,NULL,'JPY',NULL,NULL,856,0,8,'order',NULL),
	(64,1,NULL,13,1,2,15,10,'c672cb4956c234de815895c1315ee502a5be8a4f','64',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',1128.00,0.00,600.00,0.00,128.00,1728.00,1728.00,'かんたん銀行決済(PayPal)',NULL,'2020-07-17 08:38:26','2020-07-17 08:38:31',NULL,NULL,'JPY',NULL,NULL,10,0,8,'order',NULL),
	(65,1,NULL,13,1,2,15,10,'b70d0d81a4fea63d6b4c0adf964477cb18969a97',NULL,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',1128.00,0.00,600.00,0.00,128.00,1728.00,1728.00,'かんたん銀行決済(PayPal)',NULL,'2020-07-22 04:13:56','2020-07-22 04:13:56',NULL,NULL,'JPY',NULL,NULL,10,0,8,'order',NULL),
	(66,1,NULL,13,1,2,14,10,'9c544bcef641555e7a765183bd1a10061b0b5a8d','66',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',4666.00,0.00,0.00,0.00,346.00,4666.00,4666.00,'PayPal決済',NULL,'2020-07-22 04:17:31','2020-07-24 09:04:19',NULL,NULL,'JPY',NULL,NULL,43,0,8,'order',NULL),
	(67,8,NULL,13,1,2,14,10,'593c52ade2ab62198a2c991edeabc2baa5bdb8ec','67',NULL,'杉森','浩一郎','スギモリ','コウイチロウ',NULL,'ko@thepmi.net','0337128235','1520004','目黒区鷹番','1-8-17',NULL,10459.00,0.00,0.00,0.00,775.00,10459.00,10459.00,'PayPal決済',NULL,'2020-07-23 16:10:05','2020-09-07 02:55:18','2020-07-23 16:12:23','2020-07-23 16:12:23','JPY',NULL,NULL,96,0,5,'order',NULL),
	(68,1,NULL,13,1,2,12,10,'f63c336e41a4b79ebde8a07f8cc0c36c8c333bc5','68','注意！テスト注文！','Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',4666.00,0.00,0.00,0.00,346.00,4666.00,4666.00,'楽天ペイ',NULL,'2020-08-03 13:22:56','2020-08-03 13:23:40',NULL,NULL,'JPY',NULL,NULL,43,0,8,'order',NULL),
	(69,1,NULL,13,1,2,5,10,'5e2a70461d757a4fdf3e623d1e3e68d189e318d6','69','注意！テスト注文！！','Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',20218.00,0.00,0.00,0.00,1498.00,20218.00,20218.00,'クレジット決済',NULL,'2020-08-04 03:27:01','2020-08-04 03:29:31',NULL,NULL,'JPY',NULL,NULL,187,0,8,'order',NULL),
	(70,8,NULL,13,1,2,5,10,'97d404b05f0b1354ecf805a45948fe1e8f64fe7e','70',NULL,'杉森','浩一郎','スギモリ','コウイチロウ',NULL,'ko@thepmi.net','0337128235','1520004','目黒区鷹番','1-8-17',NULL,6765.00,0.00,0.00,0.00,501.00,6765.00,6765.00,'クレジット決済',NULL,'2020-08-05 09:49:25','2020-08-05 09:50:29',NULL,NULL,'JPY',NULL,NULL,63,0,8,'order',NULL),
	(71,1,NULL,13,1,2,15,10,'32cb26237fb5877bb036bb3b417ecafbd09e3228','71',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',20218.00,0.00,0.00,0.00,1498.00,20218.00,20218.00,'かんたん銀行決済(PayPal)',NULL,'2020-08-05 10:08:13','2020-08-05 10:16:25',NULL,NULL,'JPY',NULL,NULL,187,0,8,'order',NULL),
	(72,8,NULL,13,1,2,5,10,'6a8a892f3237f2555e13ca5b7ca25e7a8b062b17','72',NULL,'杉森','浩一郎','スギモリ','コウイチロウ',NULL,'ko@thepmi.net','0337128235','1520004','目黒区鷹番','1-8-17',NULL,13763.00,0.00,0.00,0.00,1019.00,13763.00,13763.00,'クレジット決済',NULL,'2020-08-05 10:23:05','2020-08-05 10:34:48',NULL,NULL,'JPY',NULL,'',128,0,8,'order',NULL),
	(73,8,NULL,13,1,2,5,10,'2581b3f395ae34dbe9d17bcc0a10ba86063f57c6','73',NULL,'杉森','浩一郎','スギモリ','コウイチロウ',NULL,'ko@thepmi.net','0337128235','1520004','目黒区鷹番','1-8-17',NULL,8845.00,0.00,0.00,0.00,655.00,8845.00,8845.00,'クレジット決済',NULL,'2020-08-05 10:51:25','2020-08-05 10:55:31',NULL,NULL,'JPY',NULL,'',82,0,8,'order',NULL),
	(74,1,NULL,13,1,2,14,10,'92d5cb607ce5782201b1e016662fbeb554c563bb','74',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',24884.00,0.00,0.00,0.00,1844.00,24884.00,24884.00,'PayPal決済',NULL,'2020-08-06 13:29:51','2020-08-06 13:50:57',NULL,NULL,'JPY',NULL,NULL,230,0,8,'order',NULL),
	(75,1,NULL,13,1,2,14,10,'ff53ecccbdb15ef43cc9278b4345259f67ce285a','75',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',24884.00,0.00,0.00,0.00,1844.00,24884.00,24884.00,'PayPal決済',NULL,'2020-08-06 14:02:24','2020-08-06 14:53:09',NULL,NULL,'JPY',NULL,NULL,230,0,8,'order',NULL),
	(76,1,NULL,13,1,2,14,10,'a6fbcf6cb87f49288805742821ca89f65cfc4c80','76',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',4666.00,0.00,0.00,0.00,346.00,4666.00,4666.00,'PayPal決済',NULL,'2020-08-06 14:55:54','2020-08-06 15:13:59',NULL,NULL,'JPY',NULL,'',43,0,8,'order',NULL),
	(77,1,NULL,13,1,2,14,10,'f6b38557a4c5c336612f8e23e2ca0478f91d0eb1','77',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',4666.00,0.00,0.00,0.00,346.00,4666.00,4666.00,'PayPal決済',NULL,'2020-08-07 02:28:56','2020-08-07 02:29:28',NULL,NULL,'JPY',NULL,'',43,0,8,'order',NULL),
	(78,1,NULL,13,1,2,14,10,'403bf0927f820863514cfb364a46b1e52e831bda','78',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',4666.00,0.00,0.00,0.00,346.00,4666.00,4666.00,'PayPal決済',NULL,'2020-08-10 03:46:20','2020-08-12 09:37:57',NULL,NULL,'JPY',NULL,'',43,0,8,'order',NULL),
	(79,NULL,NULL,13,NULL,NULL,5,10,'88e633478db93d3ce1f2ea5e4eb200427bb91259','79',NULL,'Test','Test','テスト','テスト',NULL,'test@test.com','11122223333','1000001','千代田区千代田','1-1-1',NULL,5184.00,0.00,0.00,0.00,384.00,5184.00,5184.00,'クレジット決済',NULL,'2020-08-10 10:00:13','2020-08-10 10:00:19',NULL,NULL,'JPY',NULL,NULL,0,0,8,'order',NULL),
	(80,4,NULL,13,NULL,NULL,5,10,'1c94bf800574b5a41aa2b07d625f1e0b0b80cc1a','80',NULL,'Test2','Test','カナ','カナ',NULL,'05049pyj+1@gmail.com','11122223333','1000001','千代田区千代田','1-1-1',NULL,12702.00,1270.00,0.00,0.00,942.00,11432.00,11432.00,'クレジット決済',NULL,'2020-08-10 10:07:12','2020-08-10 10:07:19',NULL,NULL,'JPY',NULL,'***********************************************\n　クーポン情報                                 \n***********************************************\n\nクーポンコード: MARITIME10 初回10％割引オファー\n',107,0,8,'order',NULL),
	(81,4,NULL,13,NULL,NULL,5,10,'a6557d4f7048fc64433922d72e18615928df43e1','81',NULL,'Test2','Test','カナ','カナ',NULL,'05049pyj+1@gmail.com','11122223333','1000001','千代田区千代田','1-1-1',NULL,5184.00,518.00,0.00,0.00,384.00,4666.00,4666.00,'クレジット決済',NULL,'2020-08-10 10:07:33','2020-08-10 10:07:40',NULL,NULL,'JPY',NULL,'***********************************************\n　クーポン情報                                 \n***********************************************\n\nクーポンコード: MARITIME10 初回10％割引オファー\n',43,0,8,'order',NULL),
	(82,1,NULL,13,1,2,5,10,'96d3405cf477a4a183fbca30cd6317dcbe720d1d','82',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',4666.00,0.00,0.00,0.00,346.00,4666.00,4666.00,'クレジット決済',NULL,'2020-08-12 09:44:19','2020-08-12 09:44:29',NULL,NULL,'JPY',NULL,'',43,0,8,'order',NULL),
	(83,1,NULL,13,1,2,5,10,'14410139b26e1f398376cab649ef0c85e30b411e','83',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',4666.00,0.00,0.00,0.00,346.00,4666.00,4666.00,'クレジット決済',NULL,'2020-08-12 09:44:56','2020-08-12 09:53:52',NULL,NULL,'JPY',NULL,'',43,0,8,'order',NULL),
	(84,1,NULL,13,1,2,5,10,'97c68b34c1dc8a5d4bc2d7e7ee9233c60d9b9d4b','84',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',4666.00,467.00,0.00,0.00,346.00,4199.00,4199.00,'クレジット決済',NULL,'2020-08-13 11:22:56','2020-08-13 11:24:41',NULL,NULL,'JPY',NULL,'***********************************************\n　クーポン情報                                 \n***********************************************\n\nクーポンコード: MARITIME10 初回10％割引オファー\n',38,0,8,'order',NULL),
	(85,1,NULL,13,1,2,5,10,'e819ef3ce95012d030991ea7679a39303b387e07','85',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',4666.00,467.00,0.00,0.00,346.00,4199.00,4199.00,'クレジット決済',NULL,'2020-08-13 12:05:05','2020-08-17 09:55:08',NULL,NULL,'JPY',NULL,'***********************************************\n　クーポン情報                                 \n***********************************************\n\nクーポンコード: MARITIME10 初回10％割引オファー\n',38,0,8,'order',NULL),
	(86,10,NULL,26,1,9,5,10,'df6d7c9abe84875beacd2aa6ff618851ae4571ca','86',NULL,'藤永','朋久','フジナガ','トモヒサ','(株)リード企画','harutoua@gmail.com','09050517243','6170837','長岡京市久貝2丁目','16-31','1976-05-05 15:00:00',6998.00,0.00,0.00,0.00,518.00,6998.00,6998.00,'クレジット決済',NULL,'2020-08-14 09:04:48','2020-08-14 09:05:17',NULL,NULL,'JPY',NULL,NULL,65,0,8,'order',NULL),
	(87,NULL,NULL,13,NULL,NULL,5,2,'050a4f9cc84ed1d39e977baf3f53da39cf79a7b2',NULL,NULL,'上野','和子','ウエノ','カズコ',NULL,'angelb333@docomo.ne.jp','08011983685','1420063','品川区荏原','2-7-5',NULL,7690.00,0.00,0.00,0.00,570.00,7690.00,7690.00,'クレジット決済',NULL,'2020-08-17 19:13:20','2020-08-17 19:13:20',NULL,NULL,'JPY',NULL,NULL,0,0,8,'order',NULL),
	(88,NULL,NULL,13,NULL,NULL,5,2,'6dcc9019b10416c22de91c0f352f0679e4fc5ddb',NULL,NULL,'上野','和子','ウエノ','カズコ',NULL,'angelb333@docomo.ne.jp','08011983685','1420063','品川区荏原','2-7-5',NULL,2506.00,0.00,600.00,0.00,230.00,3106.00,3106.00,'クレジット決済',NULL,'2020-08-17 19:14:27','2020-08-17 19:14:27',NULL,NULL,'JPY',NULL,NULL,0,0,8,'order',NULL),
	(89,8,NULL,13,1,2,5,10,'69b08410253ed92c60961cc98c9fa9cbfe297288','89',NULL,'杉森','浩一郎','スギモリ','コウイチロウ',NULL,'ko@thepmi.net','0337128235','1520004','目黒区鷹番','1-8-17',NULL,15843.00,1584.00,0.00,0.00,1173.00,14259.00,14259.00,'クレジット決済',NULL,'2020-08-18 17:19:44','2020-08-18 17:26:08',NULL,NULL,'JPY',NULL,'\n***********************************************\n　クーポン情報                                 \n***********************************************\n\nクーポンコード: JAPANLP 日本語ランディングページ経由の10%割引\n',131,0,8,'order',NULL),
	(90,1,NULL,13,1,2,5,10,'a9f6ce9b73a9e06630f07b9d015b069692f5cc18',NULL,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',9332.00,0.00,0.00,0.00,692.00,9332.00,9332.00,'クレジット決済',NULL,'2020-08-21 04:09:14','2020-08-21 04:09:14',NULL,NULL,'JPY',NULL,NULL,86,0,8,'order',NULL),
	(91,1,NULL,13,1,2,14,10,'7ec9be23ea371bac005f9d9f02bd792d50c2b407','91',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',9332.00,0.00,0.00,0.00,692.00,9332.00,9332.00,'PayPal決済',NULL,'2020-08-21 06:58:49','2020-08-21 06:59:12',NULL,NULL,'JPY',NULL,NULL,86,0,8,'order',NULL),
	(92,NULL,NULL,1,NULL,NULL,5,2,'14c8c16d1a7b45640937a25d31e72e9dade7ae89','92','会員登録したんですが、メールが、届かなかったため一般購入させて頂きます。','関','歌織','セキ','カオリ',NULL,'kaononko@icloud.com','09062618809','0630001','札幌市西区山の手一条8丁目','2-9',NULL,10368.00,0.00,0.00,0.00,768.00,10368.00,10368.00,'クレジット決済',NULL,'2020-08-25 13:45:08','2020-08-25 13:48:35',NULL,NULL,'JPY',NULL,NULL,0,0,8,'order',NULL),
	(93,NULL,NULL,1,NULL,NULL,5,2,'f80f30bfc42bc374b3a38d96993e69d5cbe88dc0','93','メールが返信されないため一般購入させて頂きます。','関','歌織','セキ','カオリ',NULL,'kaononko@icloud.com','09062618809','0630001','札幌市西区山の手一条8丁目','2-9',NULL,10368.00,0.00,0.00,0.00,768.00,10368.00,10368.00,'クレジット決済',NULL,'2020-08-25 13:49:11','2020-08-25 13:51:34',NULL,NULL,'JPY',NULL,NULL,0,0,8,'order',NULL),
	(94,NULL,NULL,1,NULL,NULL,5,2,'250058777a4670ab3569958af9931734d5448668','94','返信がこないので会員登録できませんでした。','関','歌織','セキ','カオリ',NULL,'kaononko@icloud.com','09062618809','0630001','札幌市西区山の手一条8丁目','2-9',NULL,10368.00,0.00,0.00,0.00,768.00,10368.00,10368.00,'クレジット決済',NULL,'2020-08-25 13:54:14','2020-08-25 13:58:40',NULL,NULL,'JPY',NULL,NULL,0,0,8,'order',NULL),
	(95,NULL,NULL,1,NULL,NULL,5,2,'264a2b95e240b26312b0df94247e80d9416c6af2','95','返信がこないので会員登録できませんでした。','関','歌織','セキ','カオリ',NULL,'kaononko@icloud.com','09062618809','0630001','札幌市西区山の手一条8丁目','2-9',NULL,10368.00,0.00,0.00,0.00,768.00,10368.00,10368.00,'クレジット決済',NULL,'2020-08-25 14:38:30','2020-08-25 14:40:39',NULL,NULL,'JPY',NULL,NULL,0,0,8,'order',NULL),
	(96,NULL,NULL,1,NULL,NULL,15,2,'932c2ecbff4ba07ed7c3da1b14c0f34510a7a261','96',NULL,'関','歌織','セキ','カオリ',NULL,'kaononko@icloud.com','09062618809','0630001','札幌市西区山の手一条8丁目','2-9',NULL,10368.00,0.00,0.00,0.00,768.00,10368.00,10368.00,'かんたん銀行決済(PayPal)',NULL,'2020-08-25 14:43:42','2020-08-25 14:45:06',NULL,NULL,'JPY',NULL,NULL,0,0,8,'order',NULL),
	(97,1,NULL,13,1,2,15,10,'44d469ac1ac68be82264df03d952c076fbb8e003','97',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',4666.00,0.00,0.00,0.00,346.00,4666.00,4666.00,'かんたん銀行決済(PayPal)',NULL,'2020-08-28 03:04:19','2020-08-28 03:04:39',NULL,NULL,'JPY',NULL,NULL,43,0,8,'order',NULL),
	(98,1,NULL,13,1,2,14,10,'3253941ef335c566fdd727d661122c0e28534532','98',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',4666.00,0.00,0.00,0.00,346.00,4666.00,4666.00,'PayPal決済',NULL,'2020-08-28 03:35:57','2020-08-28 03:38:15',NULL,NULL,'JPY',NULL,NULL,43,0,8,'order',NULL),
	(99,1,NULL,13,1,2,5,2,'2043d25134ad9ff973748e77db747b43de846713','99',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','0123456789','1570077','世田谷区鎌田','123建物','1985-04-05 15:00:00',4666.00,0.00,0.00,0.00,346.00,4666.00,4666.00,'クレジット決済',NULL,'2020-08-28 05:41:47','2020-08-28 05:46:35',NULL,NULL,'JPY',NULL,NULL,43,0,8,'order',NULL),
	(100,1,NULL,14,1,2,5,10,'9945c35cbe7b2af9976bc59467010edf96d60296','100',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','07026558129','2220032','横浜市港北区大豆戸町171-1','ネイバーズ菊名311','1985-04-05 15:00:00',1128.00,0.00,600.00,0.00,128.00,1728.00,1728.00,'クレジット決済',NULL,'2020-08-28 05:56:17','2020-08-28 09:25:11','2020-08-28 05:57:47',NULL,'JPY','クレジット決済<br />\n承認番号: 0649950<br />\n<br />\n','クレジット決済\n承認番号: 0649950\n\n',10,0,3,'order',14),
	(101,1,NULL,14,1,2,5,2,'a4789b62208ccc70924ad7e2f894d125a6d97c18','101',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','07026558129','2220032','横浜市港北区大豆戸町171-1','ネイバーズ菊名311','1985-04-05 15:00:00',1128.00,0.00,600.00,0.00,128.00,1728.00,1728.00,'クレジット決済',NULL,'2020-08-28 06:01:02','2020-08-28 06:02:20',NULL,NULL,'JPY',NULL,NULL,10,0,8,'order',NULL),
	(102,1,NULL,14,1,2,5,2,'08e305a6d5f8d8acea8716199ac84edba1e6458c','102','テスト注文、キャンセルしてください','Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','07026558129','2220032','横浜市港北区大豆戸町171-1','ネイバーズ菊名311','1985-04-05 15:00:00',6998.00,0.00,0.00,0.00,518.00,6998.00,6998.00,'クレジット決済',NULL,'2020-08-28 06:03:54','2020-08-28 09:24:38','2020-08-28 06:17:50',NULL,'JPY','クレジット決済<br />\n承認番号: 0651230<br />\n<br />\n','クレジット決済\n承認番号: 0651230\n\n',65,0,3,'order',14),
	(103,1,NULL,14,1,2,3,10,'d6e7790f5e566c284164ac39679bce2bb5af5422','103',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','07026558129','2220032','横浜市港北区大豆戸町171-1','ネイバーズ菊名311','1985-04-05 15:00:00',13511.00,0.00,0.00,0.00,1001.00,13511.00,13511.00,'銀行振込',NULL,'2020-08-30 07:38:55','2020-08-30 07:39:02',NULL,NULL,'JPY',NULL,NULL,125,0,8,'order',NULL),
	(104,1,NULL,14,1,2,3,10,'4d150e36487c48959943f9464402636cf7fb5059','104','テスト注文！','Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','07026558129','2220032','横浜市港北区大豆戸町171-1','ネイバーズ菊名311','1985-04-05 15:00:00',4666.00,0.00,0.00,0.00,346.00,4666.00,4666.00,'銀行振込',NULL,'2020-08-30 07:39:44','2020-08-30 07:41:18','2020-08-30 07:40:42',NULL,'JPY',NULL,NULL,43,0,3,'order',NULL),
	(105,1,NULL,14,1,2,3,10,'e4649647e09b518935d2d722a87740f633d2496c','105',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','07026558129','2220032','横浜市港北区大豆戸町171-1','ネイバーズ菊名311','1985-04-05 15:00:00',4666.00,0.00,0.00,0.00,346.00,4666.00,4666.00,'銀行振込',NULL,'2020-08-30 07:43:27','2020-09-02 03:09:05',NULL,NULL,'JPY',NULL,NULL,43,0,8,'order',NULL),
	(106,NULL,NULL,13,NULL,NULL,3,10,'c1414aa7e6376547ef7e29192ece89bd218582a7','106',NULL,'Test','Test','テスト','テスト',NULL,'05049pyj+test@gmail.com','11122223333','1000001','千代田区千代田','1-1-1',NULL,5184.00,0.00,0.00,0.00,384.00,5184.00,5184.00,'銀行振込',NULL,'2020-09-01 11:11:13','2020-09-01 11:11:28',NULL,NULL,'JPY',NULL,NULL,0,0,8,'order',NULL),
	(107,1,NULL,14,1,2,5,10,'47f5168a1771b8fec9a4f0690ff05ef8db84c4de','107',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','07026558129','2220032','横浜市港北区大豆戸町171-1','ネイバーズ菊名311','1985-04-05 15:00:00',4666.00,0.00,0.00,0.00,346.00,4666.00,4666.00,'クレジット決済',NULL,'2020-09-02 07:03:29','2020-09-02 07:09:49',NULL,NULL,'JPY',NULL,NULL,43,0,8,'order',NULL),
	(108,1,NULL,14,1,2,3,10,'e4ad1a62dbfa43c581ea08ff3035d73a597fb765','108',NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','07026558129','2220032','横浜市港北区大豆戸町171-1','ネイバーズ菊名311','1985-04-05 15:00:00',4666.00,0.00,0.00,0.00,346.00,4666.00,4666.00,'銀行振込',NULL,'2020-09-04 05:00:38','2020-09-04 08:32:23',NULL,NULL,'JPY',NULL,NULL,43,0,8,'order',NULL),
	(109,1,NULL,14,1,2,5,10,'87df01f4c876a1742cd82e3fe7a578204ab9d757',NULL,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'djfre2000@hotmail.com','07026558129','2220032','横浜市港北区大豆戸町171-1','ネイバーズ菊名311','1985-04-05 15:00:00',4666.00,0.00,0.00,0.00,346.00,4666.00,4666.00,'クレジット決済',NULL,'2020-09-06 11:16:06','2020-09-06 11:16:06',NULL,NULL,'JPY',NULL,NULL,43,0,8,'order',NULL),
	(110,15,NULL,14,1,14,5,2,'f1745fbe2f17b8462b2b88a1f8f00b0a1fbf310d','110',NULL,'熊澤','智義','クマザワ','トモヨシ','株式会社ウルスマイル','marugeri@icloud.com','09065153851','2160012','川崎市宮前区水沢','2-15-24','1969-06-11 15:00:00',46170.00,4617.00,0.00,0.00,3420.00,41553.00,41553.00,'クレジット決済',NULL,'2020-09-06 12:33:06','2020-09-07 05:25:02','2020-09-06 12:40:19',NULL,'JPY','クレジット決済<br />\n承認番号: 0000061<br />\n<br />\n','***********************************************\n　クーポン情報                                 \n***********************************************\n\nクーポンコード: LINE10 LINE用10%割引きオファー\nクレジット決済\n承認番号: 0000061\n\n',382,0,5,'order',13),
	(111,12,NULL,1,2,17,3,2,'3a2749cd5a110f89094c94de82124c0b3bb5bee6','111',NULL,'関','歌織','セキ','カオリ',NULL,'kaononko@icloud.com','09062618809','0630001','札幌市西区山の手一条8丁目','2-6','1970-09-03 15:00:00',9331.00,0.00,0.00,0.00,691.00,9331.00,9331.00,'銀行振込',NULL,'2020-09-06 14:50:30','2020-09-15 03:18:12','2020-09-06 14:51:18',NULL,'JPY',NULL,NULL,86,0,1,'order',NULL),
	(112,18,NULL,28,1,18,5,2,'f3a3228a6a53a01c5a5ccb470e093ce35928bc60','112',NULL,'GIM','HAESOO','キム','ヘソ',NULL,'rlagotnz@naver.com','09098766383','6580073','神戸市東灘区西岡本2-7-4','オーキッドコート湖南館623','1960-07-01 15:00:00',8845.00,0.00,0.00,0.00,655.00,8845.00,8845.00,'クレジット決済',NULL,'2020-09-07 16:14:50','2020-09-07 16:26:52',NULL,NULL,'JPY',NULL,NULL,82,0,8,'order',NULL),
	(113,NULL,NULL,13,NULL,NULL,5,10,'89afae9bcc609a8694cb269d17427eb06ae57e00','113',NULL,'山田','秀人','ヤマダ','ヒデト',NULL,'eightbear+ml@gmail.com','09083193987','1530062','目黒区三田','1-3-21-210',NULL,1253.00,125.00,600.00,0.00,137.00,1728.00,1728.00,'クレジット決済',NULL,'2020-09-09 15:24:09','2020-09-09 15:25:32',NULL,NULL,'JPY',NULL,'***********************************************\n　クーポン情報                                 \n***********************************************\n\nクーポンコード: MARITIME10 初回10％割引オファー\n',0,0,8,'order',NULL),
	(114,NULL,NULL,13,NULL,NULL,14,10,'514f5b8ee0a20863207f1fb84aaec459c52fcd61','114',NULL,'山田','秀人','ヤマダ','ヒデト',NULL,'eightbear+ml@gmail.com','09083193987','1530062','目黒区三田','1-3-21-210',NULL,7517.00,752.00,0.00,0.00,557.00,6765.00,6765.00,'PayPal決済',NULL,'2020-09-09 15:28:39','2020-09-10 09:55:20','2020-09-09 15:37:01','2020-09-09 15:37:01','JPY',NULL,'***********************************************\n　クーポン情報                                 \n***********************************************\n\nクーポンコード: MARITIME10 初回10％割引オファー\n',0,0,5,'order',NULL),
	(115,NULL,NULL,34,NULL,NULL,5,2,'05edcd55f1c97d0647b3378e3344c44c36f9d78e',NULL,NULL,'赤池','晋弥','アカイケ','シンヤ',NULL,'shinya.smnyya.13@docomo.ne.jp','08083852625','7340064','広島市南区小磯町','2-60',NULL,7776.00,0.00,0.00,0.00,576.00,7776.00,7776.00,'クレジット決済',NULL,'2020-09-14 15:24:59','2020-09-14 15:24:59',NULL,NULL,'JPY',NULL,NULL,0,0,8,'order',NULL),
	(116,18,NULL,28,1,18,5,2,'bb4ff0412ed5928325b821d610b240021dccd4c6',NULL,NULL,'GIM','HAESOO','キム','ヘソ',NULL,'rlagotnz@naver.com','09098766383','6580073','神戸市東灘区西岡本2-7-4','オーキッドコート湖南館623','1960-07-01 15:00:00',8845.00,0.00,0.00,0.00,655.00,8845.00,8845.00,'クレジット決済',NULL,'2020-09-17 16:51:15','2020-09-17 16:51:15',NULL,NULL,'JPY',NULL,NULL,82,0,8,'order',NULL),
	(117,NULL,NULL,31,NULL,NULL,14,2,'182152ad44d6bd78a353147391b850e12a400a8c','117',NULL,'大谷','昭子','オオタニ','アキコ',NULL,'zurich2010spring2@gmail.com','09022958296','6892544','東伯郡琴浦町箆津','336-3',NULL,22464.00,0.00,0.00,0.00,1664.00,22464.00,22464.00,'PayPal決済',NULL,'2020-09-19 12:43:50','2020-09-19 13:05:32',NULL,NULL,'JPY',NULL,NULL,0,0,8,'order',NULL),
	(118,NULL,NULL,31,NULL,NULL,14,2,'0c498cdd3e754b91f11a0611265e48bdf4a643c1','118',NULL,'大谷','昭子','オオタニ','アキコ',NULL,'zurich2010spring2@gmail.com','09023958296','6892544','東伯郡琴浦町箆津','336-3',NULL,18144.00,0.00,0.00,0.00,1344.00,18144.00,18144.00,'PayPal決済',NULL,'2020-09-19 13:03:48','2020-09-19 13:04:23',NULL,NULL,'JPY',NULL,NULL,0,0,8,'order',NULL),
	(119,NULL,NULL,31,NULL,NULL,14,2,'42b416cfc81cd86970ccff72ccc884ed624bca69','119',NULL,'大谷','昭子','オオタニ','アキコ',NULL,'zurich2010spring2@gmail.com','09022958296','6892544','東伯郡琴浦町箆津','336-3',NULL,32832.00,0.00,0.00,0.00,2432.00,32832.00,32832.00,'PayPal決済',NULL,'2020-09-19 13:07:02','2020-09-20 03:58:34','2020-09-19 13:10:32','2020-09-19 13:10:32','JPY',NULL,NULL,0,0,5,'order',NULL);

/*!40000 ALTER TABLE `dtb_order` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dtb_order_item
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dtb_order_item`;

CREATE TABLE `dtb_order_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned DEFAULT NULL,
  `product_id` int(10) unsigned DEFAULT NULL,
  `product_class_id` int(10) unsigned DEFAULT NULL,
  `shipping_id` int(10) unsigned DEFAULT NULL,
  `rounding_type_id` smallint(5) unsigned DEFAULT NULL,
  `tax_type_id` smallint(5) unsigned DEFAULT NULL,
  `tax_display_type_id` smallint(5) unsigned DEFAULT NULL,
  `order_item_type_id` smallint(5) unsigned DEFAULT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_code` varchar(255) DEFAULT NULL,
  `class_name1` varchar(255) DEFAULT NULL,
  `class_name2` varchar(255) DEFAULT NULL,
  `class_category_name1` varchar(255) DEFAULT NULL,
  `class_category_name2` varchar(255) DEFAULT NULL,
  `price` decimal(12,2) NOT NULL DEFAULT 0.00,
  `quantity` decimal(10,0) NOT NULL DEFAULT 0,
  `tax` decimal(10,0) NOT NULL DEFAULT 0,
  `tax_rate` decimal(10,0) unsigned NOT NULL DEFAULT 0,
  `tax_adjust` decimal(10,0) unsigned NOT NULL DEFAULT 0,
  `tax_rule_id` smallint(5) unsigned DEFAULT NULL,
  `currency_code` varchar(255) DEFAULT NULL,
  `processor_name` varchar(255) DEFAULT NULL,
  `point_rate` decimal(10,0) unsigned DEFAULT NULL,
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_A0C8C3ED8D9F6D38` (`order_id`),
  KEY `IDX_A0C8C3ED4584665A` (`product_id`),
  KEY `IDX_A0C8C3ED21B06187` (`product_class_id`),
  KEY `IDX_A0C8C3ED4887F3F8` (`shipping_id`),
  KEY `IDX_A0C8C3ED1BD5C574` (`rounding_type_id`),
  KEY `IDX_A0C8C3ED84042C99` (`tax_type_id`),
  KEY `IDX_A0C8C3EDA2505856` (`tax_display_type_id`),
  KEY `IDX_A0C8C3EDCAD13EAD` (`order_item_type_id`),
  CONSTRAINT `FK_A0C8C3ED1BD5C574` FOREIGN KEY (`rounding_type_id`) REFERENCES `mtb_rounding_type` (`id`),
  CONSTRAINT `FK_A0C8C3ED21B06187` FOREIGN KEY (`product_class_id`) REFERENCES `dtb_product_class` (`id`),
  CONSTRAINT `FK_A0C8C3ED4584665A` FOREIGN KEY (`product_id`) REFERENCES `dtb_product` (`id`),
  CONSTRAINT `FK_A0C8C3ED4887F3F8` FOREIGN KEY (`shipping_id`) REFERENCES `dtb_shipping` (`id`),
  CONSTRAINT `FK_A0C8C3ED84042C99` FOREIGN KEY (`tax_type_id`) REFERENCES `mtb_tax_type` (`id`),
  CONSTRAINT `FK_A0C8C3ED8D9F6D38` FOREIGN KEY (`order_id`) REFERENCES `dtb_order` (`id`),
  CONSTRAINT `FK_A0C8C3EDA2505856` FOREIGN KEY (`tax_display_type_id`) REFERENCES `mtb_tax_display_type` (`id`),
  CONSTRAINT `FK_A0C8C3EDCAD13EAD` FOREIGN KEY (`order_item_type_id`) REFERENCES `mtb_order_item_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dtb_order_item` WRITE;
/*!40000 ALTER TABLE `dtb_order_item` DISABLE KEYS */;

INSERT INTO `dtb_order_item` (`id`, `order_id`, `product_id`, `product_class_id`, `shipping_id`, `rounding_type_id`, `tax_type_id`, `tax_display_type_id`, `order_item_type_id`, `product_name`, `product_code`, `class_name1`, `class_name2`, `class_category_name1`, `class_category_name2`, `price`, `quantity`, `tax`, `tax_rate`, `tax_adjust`, `tax_rule_id`, `currency_code`, `processor_name`, `point_rate`, `discriminator_type`)
VALUES
	(1,1,4,23,1,1,1,1,1,'Maritime CBD Water','cbd-water-500',NULL,NULL,NULL,NULL,5000.00,1,400,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(3,1,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(5,1,NULL,NULL,1,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,1000.00,1,74,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(6,2,2,11,2,1,1,1,1,'MARITIME CBD 3000','cbd-3000',NULL,NULL,NULL,NULL,9999.00,1,800,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(7,2,4,23,2,1,1,1,1,'Maritime CBD Water','cbd-water-500',NULL,NULL,NULL,NULL,5000.00,1,400,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(8,2,NULL,NULL,2,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,1000.00,1,74,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(9,2,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(10,3,14,37,3,1,1,1,1,'MARITIME CBD 3000','cbd-3000-s','販売種別',NULL,'定期購買',NULL,8000.00,1,800,10,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(12,3,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,10,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(14,3,NULL,NULL,3,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,10,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(39,11,14,37,11,1,1,1,1,'MARITIME CBDオイル 30ml (10%)','cbd-3000-s','販売種別',NULL,'定期購買',NULL,8000.00,2,800,10,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(40,11,NULL,NULL,11,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,10,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(41,11,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,10,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(42,12,14,37,12,1,1,1,1,'MARITIME CBDオイル 30ml (10%)','cbd-3000-s','販売種別',NULL,'定期購買',NULL,8000.00,1,800,10,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(44,12,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,10,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(45,12,NULL,NULL,12,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,10,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(46,13,14,37,13,1,1,1,1,'MARITIME CBDオイル 30ml (10%)','cbd-3000-s','販売種別',NULL,'定期購買',NULL,8000.00,1,800,10,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(48,13,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,10,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(51,14,4,38,14,1,1,1,1,'MARITIME CBD Water 500ml','cbd-water-500','販売種別',NULL,'通常購入',NULL,2000.00,1,200,10,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(53,14,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,10,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(55,13,NULL,NULL,13,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,1000.00,1,91,10,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(56,14,NULL,NULL,14,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,1000.00,1,91,10,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(57,15,14,37,15,1,1,1,1,'MARITIME CBDオイル 30ml (10%)','cbd-3000-s','販売種別',NULL,'定期購買',NULL,8000.00,1,800,10,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(59,15,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,10,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(60,15,NULL,NULL,15,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,1000.00,1,91,10,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(61,16,4,38,16,1,1,1,1,'MARITIME CBD ウォーター 500ml','cbd-water-500','販売種別',NULL,'通常購入',NULL,2000.00,1,200,10,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(62,16,NULL,NULL,16,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,1000.00,1,91,10,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(63,16,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,10,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(64,17,14,37,17,1,1,1,1,'MARITIME CBDオイル 30ml (10%)','cbd-3000-s','販売種別',NULL,'定期購買',NULL,8000.00,1,800,10,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(65,17,NULL,NULL,17,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,1000.00,1,91,10,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(66,17,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,10,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(67,18,14,36,18,1,1,1,1,'MARITIME CBDオイル 30ml (10%)','cbd-3000','販売種別',NULL,'通常購入',NULL,12000.00,1,1200,10,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(69,18,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,10,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(75,18,NULL,NULL,18,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,1000.00,1,91,10,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(76,19,14,37,19,1,1,1,1,'MARITIME CBDオイル 30ml (10%)','cbd-3000-s','販売種別',NULL,'定期購買',NULL,8000.00,1,800,10,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(78,19,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,10,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(97,19,NULL,NULL,19,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,1000.00,1,91,10,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(98,19,NULL,NULL,NULL,NULL,2,2,4,'初回10％割引オファー',NULL,NULL,NULL,NULL,NULL,-880.00,1,0,0,0,NULL,'JPY','Plugin\\Coupon4\\Service\\PurchaseFlow\\Processor\\CouponProcessor',NULL,'orderitem'),
	(99,20,4,38,20,1,1,1,1,'MARITIME CBD ウォーター 500ml','cbd-water-500','販売種別',NULL,'通常購入',NULL,2000.00,1,200,10,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(100,20,NULL,NULL,20,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,1000.00,1,91,10,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(101,20,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,10,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(102,21,4,38,21,1,1,1,1,'MARITIME CBD ウォーター 500ml','cbd-water-500','販売種別',NULL,'通常購入',NULL,1015.00,1,102,10,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(104,21,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,10,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(107,21,NULL,NULL,21,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,1000.00,1,91,10,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(108,22,14,37,22,1,1,1,1,'MARITIME CBDオイル 30ml (10%)','cbd-3000-s','販売種別',NULL,'定期購買',NULL,8000.00,1,800,10,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(110,22,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,10,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(113,22,NULL,NULL,22,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,1000.00,1,91,10,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(114,23,14,37,23,1,1,1,1,'MARITIME CBDオイル 30ml (10%)','cbd-3000-s','販売種別',NULL,'定期購買',NULL,28500.00,1,2850,10,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(116,23,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,10,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(126,23,NULL,NULL,23,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,1000.00,1,91,10,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(127,23,NULL,NULL,NULL,NULL,2,2,4,'初回10％割引オファー',NULL,NULL,NULL,NULL,NULL,-3135.00,1,0,0,0,NULL,'JPY','Plugin\\Coupon4\\Service\\PurchaseFlow\\Processor\\CouponProcessor',NULL,'orderitem'),
	(128,24,14,37,24,1,1,1,1,'MARITIME CBDオイル 30ml (10%)','cbd-3000-s','販売種別',NULL,'定期購買',NULL,28500.00,1,2280,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(130,24,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(135,24,NULL,NULL,24,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,800.00,1,59,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(136,25,14,37,25,1,1,1,1,'MARITIME CBDオイル 30ml (10%)','cbd-3000-s','販売種別',NULL,'定期購買',NULL,28500.00,2,2280,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(138,25,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(139,26,9,44,26,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4800.00,1,384,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(140,26,8,54,26,1,1,1,1,'MARITIME CBDオイル 10ml (2%)','cbd-200','販売種別',NULL,'通常購入',NULL,7200.00,1,576,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(141,26,NULL,NULL,26,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,800.00,1,59,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(142,26,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(143,27,9,44,27,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4800.00,3,384,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(144,27,NULL,NULL,27,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,800.00,1,59,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(145,27,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(146,28,9,44,28,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4800.00,2,384,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(147,28,NULL,NULL,28,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,800.00,1,59,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(148,28,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(149,29,9,44,29,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4800.00,3,384,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(150,29,NULL,NULL,29,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,800.00,1,59,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(151,29,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(152,30,9,44,30,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4800.00,2,384,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(153,30,NULL,NULL,30,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,800.00,1,59,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(154,30,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(155,31,9,44,31,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4800.00,3,384,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(156,31,NULL,NULL,31,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,800.00,1,59,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(157,31,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(158,32,9,44,32,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4800.00,4,384,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(160,32,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(161,32,NULL,NULL,32,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,800.00,0,59,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(162,33,9,44,33,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4800.00,2,384,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(163,33,NULL,NULL,33,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,800.00,2,59,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(164,33,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(165,34,9,44,34,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4800.00,2,384,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(166,34,4,38,34,1,1,1,1,'MARITIME CBD ウォーター 500ml','cbd-water-500','販売種別',NULL,'通常購入',NULL,1160.00,2,93,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(167,34,NULL,NULL,34,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,800.00,0,59,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(168,34,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(169,35,4,39,35,1,1,1,1,'MARITIME CBD ウォーター 500ml','cbd-water-500-s','販売種別',NULL,'定期購買',NULL,870.00,2,70,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(170,35,NULL,NULL,35,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,800.00,2,59,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(171,35,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(172,36,4,38,36,1,1,1,1,'MARITIME CBD ウォーター 500ml','cbd-water-500','販売種別',NULL,'通常購入',NULL,1160.00,4,93,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(173,36,NULL,NULL,36,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,800.00,0,59,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(174,36,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(175,25,NULL,NULL,25,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,800.00,2,59,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(176,37,9,44,37,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4200.00,1,336,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(177,37,NULL,NULL,37,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,800.00,1,59,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(178,37,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(179,38,9,44,38,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4200.00,2,336,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(180,38,NULL,NULL,38,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,800.00,2,59,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(181,38,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(182,39,9,44,39,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4200.00,6,336,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(183,39,NULL,NULL,39,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,800.00,0,59,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(184,39,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(185,40,9,44,40,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4200.00,1,336,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(187,40,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(190,40,NULL,NULL,40,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,800.00,1,59,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(191,41,9,44,41,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4200.00,1,336,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(192,41,4,38,41,1,1,1,1,'MARITIME CBD ウォーター 500ml','cbd-water-500','販売種別',NULL,'通常購入',NULL,1015.00,1,81,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(193,41,NULL,NULL,41,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,800.00,2,59,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(194,41,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(195,42,9,44,42,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4200.00,2,336,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(196,42,4,38,42,1,1,1,1,'MARITIME CBD ウォーター 500ml','cbd-water-500','販売種別',NULL,'通常購入',NULL,1015.00,1,81,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(197,42,NULL,NULL,42,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,800.00,0,59,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(198,42,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(199,43,9,44,43,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4200.00,2,336,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(201,43,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(202,43,NULL,NULL,43,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,800.00,2,59,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(203,44,4,39,44,1,1,1,1,'MARITIME CBD ウォーター 500ml','cbd-water-500-s','販売種別',NULL,'定期購買',NULL,870.00,1,70,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(204,44,NULL,NULL,44,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,800.00,1,59,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(205,44,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(206,45,4,38,45,1,1,1,1,'MARITIME CBD ウォーター 500ml','cbd-water-500','販売種別',NULL,'通常購入',NULL,1015.00,1,81,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(207,45,9,44,45,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4200.00,2,336,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(208,45,NULL,NULL,45,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,800.00,0,59,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(209,45,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(210,46,9,44,46,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4200.00,1,336,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(212,46,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(222,46,NULL,NULL,46,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,800.00,0,59,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(223,47,4,38,47,1,1,1,1,'MARITIME CBD ウォーター 500ml','cbd-water-500','販売種別',NULL,'通常購入',NULL,1015.00,5,81,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(225,47,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(231,47,NULL,NULL,47,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,800.00,0,59,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(232,48,4,38,48,1,1,1,1,'MARITIME CBD ウォーター 500ml','cbd-water-500','販売種別',NULL,'通常購入',NULL,1015.00,1,81,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(233,48,9,44,48,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4200.00,2,336,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(235,48,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(245,48,NULL,NULL,48,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,800.00,0,59,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(248,49,4,39,49,1,1,1,1,'MARITIME CBD ウォーター 500ml','cbd-water-500-s','販売種別',NULL,'定期購買',NULL,870.00,1,70,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(250,49,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(254,49,NULL,NULL,49,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,800.00,1,59,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(257,50,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(258,51,9,44,51,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4320.00,1,346,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(260,51,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(265,51,NULL,NULL,51,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,800.00,0,59,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(266,52,14,36,52,1,1,1,1,'MARITIME CBDオイル 30ml (10%)','cbd-3000','販売種別',NULL,'通常購入',NULL,42750.00,1,3420,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(267,52,9,44,52,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4320.00,1,346,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(269,52,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(270,52,NULL,NULL,52,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,800.00,0,59,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(272,53,14,36,53,1,1,1,1,'MARITIME CBDオイル 30ml (10%)','cbd-3000','販売種別',NULL,'通常購入',NULL,42750.00,1,3420,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(273,53,9,44,53,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4320.00,1,346,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(275,53,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(278,53,NULL,NULL,53,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,800.00,0,59,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(279,54,12,50,54,1,1,1,1,'MARITIME CBDオイル 30ml (2%)','cbd-600','販売種別',NULL,'通常購入',NULL,13455.00,1,1076,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(280,54,4,38,54,1,1,1,1,'MARITIME CBD ウォーター 500ml','cbd-water-500','販売種別',NULL,'通常購入',NULL,1044.00,1,84,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(282,54,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(289,54,NULL,NULL,54,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(290,55,14,36,55,1,1,1,1,'MARITIME CBDオイル 30ml (10%)','cbd-3000','販売種別',NULL,'通常購入',NULL,42750.00,1,3420,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(292,55,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(293,55,NULL,NULL,55,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(294,56,9,44,56,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4320.00,1,346,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(295,56,14,36,56,1,1,1,1,'MARITIME CBDオイル 30ml (10%)','cbd-3000','販売種別',NULL,'通常購入',NULL,42750.00,1,3420,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(297,56,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(298,57,9,44,57,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4320.00,1,346,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(300,57,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(301,57,NULL,NULL,57,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(306,56,NULL,NULL,56,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(307,58,8,54,58,1,1,1,1,'MARITIME CBDオイル 10ml (2%)','cbd-200','販売種別',NULL,'通常購入',NULL,6480.00,1,518,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(308,58,6,42,58,1,1,1,1,'MARITIME CBDオイル 10ml (5%)','cbd-500','販売種別',NULL,'通常購入',NULL,12960.00,1,1037,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(309,58,15,59,58,1,1,1,1,'MARITIME CBD ウォーター 500ml ６本セット','cbd-water-500-set','販売種別',NULL,'通常購入',NULL,6264.00,1,501,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(311,58,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(316,58,NULL,NULL,58,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(317,59,13,46,59,1,1,1,1,'MARITIME CBDオイル 30ml (1%)','cbd-300-30','販売種別',NULL,'通常購入',NULL,8190.00,2,655,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(318,59,9,44,59,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4320.00,1,346,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(320,59,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(321,59,NULL,NULL,59,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(322,60,14,36,60,1,1,1,1,'MARITIME CBDオイル 30ml (10%)','cbd-3000','販売種別',NULL,'通常購入',NULL,42750.00,1,3420,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(324,60,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(328,60,NULL,NULL,60,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(329,61,14,36,61,1,1,1,1,'MARITIME CBDオイル 30ml (10%)','cbd-3000','販売種別',NULL,'通常購入',NULL,42750.00,2,3420,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(331,61,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(332,61,NULL,NULL,61,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(333,62,14,36,62,1,1,1,1,'MARITIME CBDオイル 30ml (10%)','cbd-3000','販売種別',NULL,'通常購入',NULL,42750.00,2,3420,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(334,62,NULL,NULL,62,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(335,62,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(336,63,14,36,63,1,1,1,1,'MARITIME CBDオイル 30ml (10%)','cbd-3000','販売種別',NULL,'通常購入',NULL,42750.00,2,3420,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(337,63,NULL,NULL,63,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(338,63,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(339,64,4,38,64,1,1,1,1,'MARITIME CBD ウォーター 500ml','cbd-water-500','販売種別',NULL,'通常購入',NULL,1044.00,1,84,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(341,64,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(342,64,NULL,NULL,64,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,1,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(343,65,4,38,65,1,1,1,1,'MARITIME CBD ウォーター 500ml','cbd-water-500','販売種別',NULL,'通常購入',NULL,1044.00,1,84,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(344,65,NULL,NULL,65,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,1,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(345,65,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(346,66,9,44,66,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4320.00,1,346,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(348,66,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(349,67,7,56,67,1,1,1,1,'MARITIME CBDオイル 10ml (3%)','cbd-300-10','販売種別',NULL,'通常購入',NULL,8640.00,1,691,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(350,67,4,38,67,1,1,1,1,'MARITIME CBD ウォーター 500ml','cbd-water-500','販売種別',NULL,'通常購入',NULL,1044.00,1,84,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(352,67,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(357,67,NULL,NULL,67,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(362,66,NULL,NULL,66,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(363,68,9,44,68,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4320.00,1,346,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(365,68,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(368,68,NULL,NULL,68,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(370,69,11,52,69,1,1,1,1,'MARITIME CBDオイル 30ml (3%)','cbd-900','販売種別',NULL,'通常購入',NULL,18720.00,1,1498,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(372,69,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(373,69,NULL,NULL,69,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(374,70,15,59,70,1,1,1,1,'MARITIME CBD ウォーター 500ml ６本セット','cbd-water-500-set','販売種別',NULL,'通常購入',NULL,6264.00,1,501,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(376,70,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(378,70,NULL,NULL,70,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(379,71,11,52,71,1,1,1,1,'MARITIME CBDオイル 30ml (3%)','cbd-900','販売種別',NULL,'通常購入',NULL,18720.00,1,1498,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(381,71,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(384,71,NULL,NULL,71,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(385,72,15,59,72,1,1,1,1,'MARITIME CBD ウォーター 500ml ６本セット','cbd-water-500-set','販売種別',NULL,'通常購入',NULL,6264.00,1,501,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(386,72,8,54,72,1,1,1,1,'MARITIME CBDオイル 10ml (2%)','cbd-200','販売種別',NULL,'通常購入',NULL,6480.00,1,518,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(388,72,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(408,72,NULL,NULL,72,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(409,73,13,46,73,1,1,1,1,'MARITIME CBDオイル 30ml (1%)','cbd-300-30','販売種別',NULL,'通常購入',NULL,8190.00,1,655,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(411,73,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(417,73,NULL,NULL,73,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(418,74,9,44,74,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4320.00,1,346,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(419,74,11,52,74,1,1,1,1,'MARITIME CBDオイル 30ml (3%)','cbd-900','販売種別',NULL,'通常購入',NULL,18720.00,1,1498,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(421,74,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(437,74,NULL,NULL,74,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(438,75,9,44,75,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4320.00,1,346,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(439,75,11,52,75,1,1,1,1,'MARITIME CBDオイル 30ml (3%)','cbd-900','販売種別',NULL,'通常購入',NULL,18720.00,1,1498,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(441,75,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(448,75,NULL,NULL,75,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(449,76,9,44,76,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4320.00,1,346,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(451,76,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(461,76,NULL,NULL,76,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(462,77,9,44,77,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4320.00,1,346,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(464,77,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(469,77,NULL,NULL,77,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(470,78,9,44,78,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4320.00,1,346,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(472,78,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(484,79,9,44,79,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4800.00,1,384,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(486,79,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(488,79,NULL,NULL,79,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(489,80,9,44,80,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4800.00,1,384,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(490,80,4,38,80,1,1,1,1,'MARITIME CBD ウォーター 500ml','cbd-water-500','販売種別',NULL,'通常購入',NULL,1160.00,6,93,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(492,80,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(495,80,NULL,NULL,80,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(496,80,NULL,NULL,NULL,NULL,2,2,4,'初回10％割引オファー',NULL,NULL,NULL,NULL,NULL,-1270.00,1,0,0,0,NULL,'JPY','Plugin\\Coupon4\\Service\\PurchaseFlow\\Processor\\CouponProcessor',NULL,'orderitem'),
	(497,81,9,44,81,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4800.00,1,384,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(499,81,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(502,81,NULL,NULL,81,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(503,81,NULL,NULL,NULL,NULL,2,2,4,'初回10％割引オファー',NULL,NULL,NULL,NULL,NULL,-518.00,1,0,0,0,NULL,'JPY','Plugin\\Coupon4\\Service\\PurchaseFlow\\Processor\\CouponProcessor',NULL,'orderitem'),
	(507,78,NULL,NULL,78,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(508,82,9,44,82,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4320.00,1,346,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(510,82,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(513,82,NULL,NULL,82,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(514,83,9,44,83,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4320.00,1,346,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(516,83,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(523,83,NULL,NULL,83,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(524,84,9,44,84,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4320.00,1,346,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(526,84,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(532,84,NULL,NULL,84,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(533,84,NULL,NULL,NULL,NULL,2,2,4,'初回10％割引オファー',NULL,NULL,NULL,NULL,NULL,-467.00,1,0,0,0,NULL,'JPY','Plugin\\Coupon4\\Service\\PurchaseFlow\\Processor\\CouponProcessor',NULL,'orderitem'),
	(534,85,9,44,85,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4320.00,1,346,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(536,85,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(551,86,8,54,86,1,1,1,1,'MARITIME CBDオイル 10ml (2%)','cbd-200','販売種別',NULL,'通常購入',NULL,6480.00,1,518,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(553,86,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(557,86,NULL,NULL,86,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(558,85,NULL,NULL,85,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(559,85,NULL,NULL,NULL,NULL,2,2,4,'初回10％割引オファー',NULL,NULL,NULL,NULL,NULL,-467.00,1,0,0,0,NULL,'JPY','Plugin\\Coupon4\\Service\\PurchaseFlow\\Processor\\CouponProcessor',NULL,'orderitem'),
	(560,87,4,38,87,1,1,1,1,'MARITIME CBD ウォーター 500ml','cbd-water-500','販売種別',NULL,'通常購入',NULL,1160.00,2,93,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(561,87,9,44,87,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4800.00,1,384,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(562,87,NULL,NULL,87,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(563,87,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(564,88,4,38,88,1,1,1,1,'MARITIME CBD ウォーター 500ml','cbd-water-500','販売種別',NULL,'通常購入',NULL,1160.00,2,93,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(565,88,NULL,NULL,88,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,1,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(566,88,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(567,89,8,54,89,1,1,1,1,'MARITIME CBDオイル 10ml (2%)','cbd-200','販売種別',NULL,'通常購入',NULL,6480.00,1,518,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(568,89,13,46,89,1,1,1,1,'MARITIME CBDオイル 30ml (1%)','cbd-300-30','販売種別',NULL,'通常購入',NULL,8190.00,1,655,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(570,89,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(575,89,NULL,NULL,89,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(576,89,NULL,NULL,NULL,NULL,2,2,4,'日本語ランディングページ経由の10%割引',NULL,NULL,NULL,NULL,NULL,-1584.00,1,0,0,0,NULL,'JPY','Plugin\\Coupon4\\Service\\PurchaseFlow\\Processor\\CouponProcessor',NULL,'orderitem'),
	(577,90,9,44,90,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4320.00,2,346,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(578,90,NULL,NULL,90,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(579,90,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(580,91,9,44,91,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4320.00,2,346,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(582,91,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(586,91,NULL,NULL,91,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(587,92,7,56,92,1,1,1,1,'MARITIME CBDオイル 10ml (3%)','cbd-300-10','販売種別',NULL,'通常購入',NULL,9600.00,1,768,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(589,92,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(592,92,NULL,NULL,92,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(594,93,7,56,93,1,1,1,1,'MARITIME CBDオイル 10ml (3%)','cbd-300-10','販売種別',NULL,'通常購入',NULL,9600.00,1,768,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(596,93,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(597,93,NULL,NULL,93,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(599,94,7,56,94,1,1,1,1,'MARITIME CBDオイル 10ml (3%)','cbd-300-10','販売種別',NULL,'通常購入',NULL,9600.00,1,768,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(601,94,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(605,94,NULL,NULL,94,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(607,95,7,56,95,1,1,1,1,'MARITIME CBDオイル 10ml (3%)','cbd-300-10','販売種別',NULL,'通常購入',NULL,9600.00,1,768,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(609,95,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(616,95,NULL,NULL,95,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(618,96,7,56,96,1,1,1,1,'MARITIME CBDオイル 10ml (3%)','cbd-300-10','販売種別',NULL,'通常購入',NULL,9600.00,1,768,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(620,96,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(626,96,NULL,NULL,96,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(627,97,9,44,97,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4320.00,1,346,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(629,97,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(632,97,NULL,NULL,97,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(633,98,9,44,98,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4320.00,1,346,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(635,98,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(642,98,NULL,NULL,98,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(643,99,9,44,99,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4320.00,1,346,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(645,99,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(655,99,NULL,NULL,99,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(656,100,4,38,100,1,1,1,1,'MARITIME CBD ウォーター 500ml','cbd-water-500','販売種別',NULL,'通常購入',NULL,1044.00,1,84,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(658,100,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(660,100,NULL,NULL,100,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,1,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(661,101,4,38,101,1,1,1,1,'MARITIME CBD ウォーター 500ml','cbd-water-500','販売種別',NULL,'通常購入',NULL,1044.00,1,84,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(663,101,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(664,101,NULL,NULL,101,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,1,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(665,102,8,54,102,1,1,1,1,'MARITIME CBDオイル 10ml (2%)','cbd-200','販売種別',NULL,'通常購入',NULL,6480.00,1,518,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(667,102,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(675,102,NULL,NULL,102,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(676,103,9,44,103,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4320.00,1,346,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(677,103,13,46,103,1,1,1,1,'MARITIME CBDオイル 30ml (1%)','cbd-300-30','販売種別',NULL,'通常購入',NULL,8190.00,1,655,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(679,103,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(681,103,NULL,NULL,103,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(682,104,9,44,104,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4320.00,1,346,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(684,104,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(692,104,NULL,NULL,104,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(693,105,9,44,105,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4320.00,1,346,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(695,105,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(701,106,9,44,106,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4800.00,1,384,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(703,106,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(708,106,NULL,NULL,106,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(710,105,NULL,NULL,105,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(711,107,9,44,107,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4320.00,1,346,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(713,107,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(718,107,NULL,NULL,107,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(719,108,9,44,108,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4320.00,1,346,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(721,108,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(727,108,NULL,NULL,108,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(728,109,9,44,109,1,1,1,1,'MARITIME CBDオイル 10ml (1%)','cbd-100','販売種別',NULL,'通常購入',NULL,4320.00,1,346,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(729,109,NULL,NULL,109,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(730,109,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(731,110,14,36,110,1,1,1,1,'MARITIME CBDオイル 30ml (10%)','cbd-3000','販売種別',NULL,'通常購入',NULL,42750.00,1,3420,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(733,110,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(743,110,NULL,NULL,110,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(744,110,NULL,NULL,NULL,NULL,2,2,4,'LINE用10%割引きオファー',NULL,NULL,NULL,NULL,NULL,-4617.00,1,0,0,0,NULL,'JPY','Plugin\\Coupon4\\Service\\PurchaseFlow\\Processor\\CouponProcessor',NULL,'orderitem'),
	(745,111,7,56,111,1,1,1,1,'MARITIME CBDオイル 10ml (3%)','cbd-300-10','販売種別',NULL,'通常購入',NULL,8640.00,1,691,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(747,111,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(751,111,NULL,NULL,111,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(752,112,13,46,112,1,1,1,1,'MARITIME CBDオイル 30ml (1%)','cbd-300-30','販売種別',NULL,'通常購入',NULL,8190.00,1,655,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(754,112,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(758,112,NULL,NULL,112,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(759,113,4,38,113,1,1,1,1,'MARITIME CBD ウォーター 500ml','cbd-water-500','販売種別',NULL,'通常購入',NULL,1160.00,1,93,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(761,113,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(766,113,NULL,NULL,113,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,1,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(767,113,NULL,NULL,NULL,NULL,2,2,4,'初回10％割引オファー',NULL,NULL,NULL,NULL,NULL,-125.00,1,0,0,0,NULL,'JPY','Plugin\\Coupon4\\Service\\PurchaseFlow\\Processor\\CouponProcessor',NULL,'orderitem'),
	(768,114,15,59,114,1,1,1,1,'MARITIME CBD ウォーター 500ml ６本セット','cbd-water-500-set','販売種別',NULL,'通常購入',NULL,6960.00,1,557,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(770,114,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(791,114,NULL,NULL,114,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(792,114,NULL,NULL,NULL,NULL,2,2,4,'初回10％割引オファー',NULL,NULL,NULL,NULL,NULL,-752.00,1,0,0,0,NULL,'JPY','Plugin\\Coupon4\\Service\\PurchaseFlow\\Processor\\CouponProcessor',NULL,'orderitem'),
	(793,115,8,54,115,1,1,1,1,'MARITIME CBDオイル 10ml (2%)','cbd-200','販売種別',NULL,'通常購入',NULL,7200.00,1,576,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(794,115,NULL,NULL,115,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(795,115,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(796,48,NULL,NULL,NULL,NULL,2,2,6,'ポイント',NULL,NULL,NULL,NULL,NULL,-500.00,1,0,0,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PointProcessor',NULL,'orderitem'),
	(797,116,13,46,116,1,1,1,1,'MARITIME CBDオイル 30ml (1%)','cbd-300-30','販売種別',NULL,'通常購入',NULL,8190.00,1,655,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(798,116,NULL,NULL,116,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(799,116,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(800,117,11,52,117,1,1,1,1,'MARITIME CBDオイル 30ml (3%)','cbd-900','販売種別',NULL,'通常購入',NULL,20800.00,1,1664,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(802,117,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(812,118,8,54,118,1,1,1,1,'MARITIME CBDオイル 10ml (2%)','cbd-200','販売種別',NULL,'通常購入',NULL,7200.00,1,576,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(813,118,7,56,118,1,1,1,1,'MARITIME CBDオイル 10ml (3%)','cbd-300-10','販売種別',NULL,'通常購入',NULL,9600.00,1,768,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(815,118,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(819,118,NULL,NULL,118,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(821,117,NULL,NULL,117,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem'),
	(822,119,11,52,119,1,1,1,1,'MARITIME CBDオイル 30ml (3%)','cbd-900','販売種別',NULL,'通常購入',NULL,20800.00,1,1664,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(823,119,7,56,119,1,1,1,1,'MARITIME CBDオイル 10ml (3%)','cbd-300-10','販売種別',NULL,'通常購入',NULL,9600.00,1,768,8,0,NULL,'JPY',NULL,NULL,'orderitem'),
	(825,119,NULL,NULL,NULL,1,1,2,3,'手数料',NULL,NULL,NULL,NULL,NULL,0.00,1,0,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor',NULL,'orderitem'),
	(829,119,NULL,NULL,119,1,1,2,2,'送料',NULL,NULL,NULL,NULL,NULL,600.00,0,44,8,0,NULL,'JPY','Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor',NULL,'orderitem');

/*!40000 ALTER TABLE `dtb_order_item` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dtb_order_pdf
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dtb_order_pdf`;

CREATE TABLE `dtb_order_pdf` (
  `member_id` int(10) unsigned NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `message1` varchar(255) DEFAULT NULL,
  `message2` varchar(255) DEFAULT NULL,
  `message3` varchar(255) DEFAULT NULL,
  `note1` varchar(255) DEFAULT NULL,
  `note2` varchar(255) DEFAULT NULL,
  `note3` varchar(255) DEFAULT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `visible` tinyint(1) NOT NULL DEFAULT 1,
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table dtb_page
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dtb_page`;

CREATE TABLE `dtb_page` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `master_page_id` int(10) unsigned DEFAULT NULL,
  `page_name` varchar(255) DEFAULT NULL,
  `url` varchar(255) NOT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `edit_type` smallint(5) unsigned NOT NULL DEFAULT 1,
  `author` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `keyword` varchar(255) DEFAULT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `meta_robots` varchar(255) DEFAULT NULL,
  `meta_tags` varchar(4000) DEFAULT NULL,
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E3951A67D0618E8C` (`master_page_id`),
  KEY `dtb_page_url_idx` (`url`),
  CONSTRAINT `FK_E3951A67D0618E8C` FOREIGN KEY (`master_page_id`) REFERENCES `dtb_page` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dtb_page` WRITE;
/*!40000 ALTER TABLE `dtb_page` DISABLE KEYS */;

INSERT INTO `dtb_page` (`id`, `master_page_id`, `page_name`, `url`, `file_name`, `edit_type`, `author`, `description`, `keyword`, `create_date`, `update_date`, `meta_robots`, `meta_tags`, `discriminator_type`)
VALUES
	(0,NULL,'プレビューデータ','preview',NULL,1,NULL,NULL,NULL,'2017-03-07 10:14:52','2017-03-07 10:14:52',NULL,NULL,'page'),
	(1,NULL,'ホーム','homepage','index',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。CBDオイルは1%から10%までの高濃度をご用意。コスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターもおすすめです。','MARITIME, マリタイム, マリタイムおすすめのCBDウォーター、カンナビノイド効果を今すぐ試そう,cbdオイル, cbdオイル 効果, cbdオイル おすすめ,cbdオイル 日本, cbdオイル 使い方, cbdオイル 使い方,cbdオイルとは, cbdオイル 口コミ, cbdオイル 通販,','2017-03-07 10:14:52','2020-09-04 05:17:15',NULL,NULL,'page'),
	(2,NULL,'商品一覧','product_list','Product/list',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの商品一覧です。','MARITIME, マリタイム, マリタイム商品一覧ページ, おすすめのCBDウォーター、カンナビノイド効果を今すぐ試そう,cbdオイル, cbdオイル 効果, cbdオイル おすすめ,cbdオイル 日本, cbdオイル 使い方, cbdオイル 使い方,cbdオイルとは, cbdオイル 口コミ, cbdオイル 通販','2017-03-07 10:14:52','2020-09-04 05:17:31',NULL,NULL,'page'),
	(3,NULL,'商品詳細','product_detail','Product/detail',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの商品詳細です。','MARITIME, マリタイム, マリタイム商品詳細ページ, おすすめのCBDウォーター、カンナビノイド効果を今すぐ試そう,cbdオイル, cbdオイル 効果, cbdオイル おすすめ,cbdオイル 日本, cbdオイル 使い方, cbdオイル 使い方,cbdオイルとは, cbdオイル 口コミ, cbdオイル 通販','2017-03-07 10:14:52','2020-09-04 05:17:51',NULL,NULL,'page'),
	(4,NULL,'マイページ','mypage','Mypage/index',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターのMYページです。','MARITIME, マリタイム, cbdオイル, cbd効果, cbd飲料, cbdとは, cbdオイル 使い方,cbdオイル 効果, cbd ショップ, cbd hemp おすすめ, cbdウオーター, cbdオイル高濃度, cbd オイル %, cbd おすすめ, cbdオイル 購入, cbd 日本 合法, cbdカンナビジオール, cbdオイル おすすめ, cbd コスパ, カンナビジオール, カンナビジオール 効果, カンナビジオール 厚生労働省','2017-03-07 10:14:52','2020-09-04 05:18:04','noindex',NULL,'page'),
	(5,NULL,'会員登録内容変更(入力ページ)','mypage_change','Mypage/change',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの会員登録内容変更(入力ページ)です。','MARITIME, マリタイム, cbdオイル, cbd効果, cbd飲料, cbdとは, cbdオイル 使い方,cbdオイル 効果, cbd ショップ, cbd hemp おすすめ, cbdウオーター, cbdオイル高濃度, cbd オイル %, cbd おすすめ, cbdオイル 購入, cbd 日本 合法, cbdカンナビジオール, cbdオイル おすすめ, cbd コスパ, カンナビジオール, カンナビジオール 効果, カンナビジオール 厚生労働省','2017-03-07 10:14:52','2020-09-04 05:18:20','noindex',NULL,'page'),
	(6,NULL,'会員登録内容変更(完了ページ)','mypage_change_complete','Mypage/change_complete',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの会員登録内容変更(完了ページ)です。','MARITIME, マリタイム, cbdオイル, cbd効果, cbd飲料, cbdとは, cbdオイル 使い方,cbdオイル 効果, cbd ショップ, cbd hemp おすすめ, cbdウオーター, cbdオイル高濃度, cbd オイル %, cbd おすすめ, cbdオイル 購入, cbd 日本 合法, cbdカンナビジオール, cbdオイル おすすめ, cbd コスパ, カンナビジオール, カンナビジオール 効果, カンナビジオール 厚生労働省','2017-03-07 10:14:52','2020-09-04 05:18:32','noindex',NULL,'page'),
	(7,NULL,'お届け先一覧','mypage_delivery','Mypage/delivery',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターのお届け先一覧ページです。','MARITIME, マリタイム, cbdオイル, cbd効果, cbd飲料, cbdとは, cbdオイル 使い方,cbdオイル 効果, cbd ショップ, cbd hemp おすすめ, cbdウオーター, cbdオイル高濃度, cbd オイル %, cbd おすすめ, cbdオイル 購入, cbd 日本 合法, cbdカンナビジオール, cbdオイル おすすめ, cbd コスパ, カンナビジオール, カンナビジオール 効果, カンナビジオール 厚生労働省','2017-03-07 10:14:52','2020-09-04 05:18:46','noindex',NULL,'page'),
	(8,NULL,'お届け先追加','mypage_delivery_new','Mypage/delivery_edit',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターのお届け先追加ページです。','MARITIME, マリタイム, cbdオイル, cbd効果, cbd飲料, cbdとは, cbdオイル 使い方,cbdオイル 効果, cbd ショップ, cbd hemp おすすめ, cbdウオーター, cbdオイル高濃度, cbd オイル %, cbd おすすめ, cbdオイル 購入, cbd 日本 合法, cbdカンナビジオール, cbdオイル おすすめ, cbd コスパ, カンナビジオール, カンナビジオール 効果, カンナビジオール 厚生労働省','2017-03-07 10:14:52','2020-09-04 05:19:05','noindex',NULL,'page'),
	(9,NULL,'お気に入り一覧','mypage_favorite','Mypage/favorite',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターのお気に入り一覧ページです。','MARITIME, マリタイム, cbdオイル, cbd効果, cbd飲料, cbdとは, cbdオイル 使い方,cbdオイル 効果, cbd ショップ, cbd hemp おすすめ, cbdウオーター, cbdオイル高濃度, cbd オイル %, cbd おすすめ, cbdオイル 購入, cbd 日本 合法, cbdカンナビジオール, cbdオイル おすすめ, cbd コスパ, カンナビジオール, カンナビジオール 効果, カンナビジオール 厚生労働省','2017-03-07 10:14:52','2020-09-04 05:19:14','noindex',NULL,'page'),
	(10,NULL,'購入履歴詳細','mypage_history','Mypage/history',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの購入履歴詳細ページです。','MARITIME, マリタイム, cbdオイル, cbd効果, cbd飲料, cbdとは, cbdオイル 使い方,cbdオイル 効果, cbd ショップ, cbd hemp おすすめ, cbdウオーター, cbdオイル高濃度, cbd オイル %, cbd おすすめ, cbdオイル 購入, cbd 日本 合法, cbdカンナビジオール, cbdオイル おすすめ, cbd コスパ, カンナビジオール, カンナビジオール 効果, カンナビジオール 厚生労働省','2017-03-07 10:14:52','2020-09-04 05:19:30','noindex',NULL,'page'),
	(11,NULL,'ログイン','mypage_login','Mypage/login',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターのログインページです。','MARITIME, マリタイム, cbdオイル, cbd効果, cbd飲料, cbdとは, cbdオイル 使い方,cbdオイル 効果, cbd ショップ, cbd hemp おすすめ, cbdウオーター, cbdオイル高濃度, cbd オイル %, cbd おすすめ, cbdオイル 購入, cbd 日本 合法, cbdカンナビジオール, cbdオイル おすすめ, cbd コスパ, カンナビジオール, カンナビジオール 効果, カンナビジオール 厚生労働省','2017-03-07 10:14:52','2020-09-04 05:19:46','noindex',NULL,'page'),
	(12,NULL,'退会手続き(入力ページ)','mypage_withdraw','Mypage/withdraw',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの退会手続き(入力ページ)ページです。','MARITIME, マリタイム, cbdオイル, cbd効果, cbd飲料, cbdとは, cbdオイル 使い方,cbdオイル 効果, cbd ショップ, cbd hemp おすすめ, cbdウオーター, cbdオイル高濃度, cbd オイル %, cbd おすすめ, cbdオイル 購入, cbd 日本 合法, cbdカンナビジオール, cbdオイル おすすめ, cbd コスパ, カンナビジオール, カンナビジオール 効果, カンナビジオール 厚生労働省','2017-03-07 10:14:52','2020-09-04 05:20:01','noindex',NULL,'page'),
	(13,NULL,'退会手続き(完了ページ)','mypage_withdraw_complete','Mypage/withdraw_complete',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの退会手続き(完了ページ)ページです。','MARITIME, マリタイム, cbdオイル, cbd効果, cbd飲料, cbdとは, cbdオイル 使い方,cbdオイル 効果, cbd ショップ, cbd hemp おすすめ, cbdウオーター, cbdオイル高濃度, cbd オイル %, cbd おすすめ, cbdオイル 購入, cbd 日本 合法, cbdカンナビジオール, cbdオイル おすすめ, cbd コスパ, カンナビジオール, カンナビジオール 効果, カンナビジオール 厚生労働省','2017-03-07 10:14:52','2020-09-04 05:20:11','noindex',NULL,'page'),
	(14,NULL,'当サイトについて','help_about','Help/about',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの会社紹介ページです。','MARITIME, マリタイム,マリタイムサイトについて, おすすめのCBDウォーター、カンナビノイド効果を今すぐ試そう,cbdオイル, cbdオイル 効果, cbdオイル おすすめ,cbdオイル 日本, cbdオイル 使い方, cbdオイル 使い方,cbdオイルとは, cbdオイル 口コミ, cbdオイル 通販','2017-03-07 10:14:52','2020-09-04 05:20:32',NULL,NULL,'page'),
	(15,NULL,'現在のカゴの中','cart','Cart/index',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの現在のショッピングカートの中のページです。','MARITIME, マリタイム, cbdオイル, cbd効果, cbd飲料, cbdとは, cbdオイル 使い方,cbdオイル 効果, cbd ショップ, cbd hemp おすすめ, cbdウオーター, cbdオイル高濃度, cbd オイル %, cbd おすすめ, cbdオイル 購入, cbd 日本 合法, cbdカンナビジオール, cbdオイル おすすめ, cbd コスパ, カンナビジオール, カンナビジオール 効果, カンナビジオール 厚生労働省','2017-03-07 10:14:52','2020-09-04 05:20:47','noindex',NULL,'page'),
	(16,NULL,'お問い合わせ(入力ページ)','contact','Contact/index',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターのお問い合わせ(入力ページ)です。','MARITIME, マリタイム, cbdオイル, cbd効果, cbd飲料, cbdとは, cbdオイル 使い方,cbdオイル 効果, cbd ショップ, cbd hemp おすすめ, cbdウオーター, cbdオイル高濃度, cbd オイル %, cbd おすすめ, cbdオイル 購入, cbd 日本 合法, cbdカンナビジオール, cbdオイル おすすめ, cbd コスパ, カンナビジオール, カンナビジオール 効果, カンナビジオール 厚生労働省','2017-03-07 10:14:52','2020-09-04 05:21:00',NULL,NULL,'page'),
	(17,NULL,'お問い合わせ(完了ページ)','contact_complete','Contact/complete',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターのお問い合わせ(完了ページ)です。','MARITIME, マリタイム, cbdオイル, cbd効果, cbd飲料, cbdとは, cbdオイル 使い方,cbdオイル 効果, cbd ショップ, cbd hemp おすすめ, cbdウオーター, cbdオイル高濃度, cbd オイル %, cbd おすすめ, cbdオイル 購入, cbd 日本 合法, cbdカンナビジオール, cbdオイル おすすめ, cbd コスパ, カンナビジオール, カンナビジオール 効果, カンナビジオール 厚生労働省','2017-03-07 10:14:52','2020-09-04 05:21:14',NULL,NULL,'page'),
	(18,NULL,'会員登録(入力ページ)','entry','Entry/index',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの会員登録(入力ページ)です。','MARITIME, マリタイム, cbdオイル, cbd効果, cbd飲料, cbdとは, cbdオイル 使い方,cbdオイル 効果, cbd ショップ, cbd hemp おすすめ, cbdウオーター, cbdオイル高濃度, cbd オイル %, cbd おすすめ, cbdオイル 購入, cbd 日本 合法, cbdカンナビジオール, cbdオイル おすすめ, cbd コスパ, カンナビジオール, カンナビジオール 効果, カンナビジオール 厚生労働省','2017-03-07 10:14:52','2020-09-04 05:21:26',NULL,NULL,'page'),
	(19,NULL,'ご利用規約','help_agreement','Help/agreement',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターのご利用規約ページです。','MARITIME, マリタイム,マリタイムご利用規約, おすすめのCBDウォーター、カンナビノイド効果を今すぐ試そう,cbdオイル, cbdオイル 効果, cbdオイル おすすめ,cbdオイル 日本, cbdオイル 使い方, cbdオイル 使い方,cbdオイルとは, cbdオイル 口コミ, cbdオイル 通販','2017-03-07 10:14:52','2020-09-04 05:21:44',NULL,NULL,'page'),
	(20,NULL,'会員登録(完了ページ)','entry_complete','Entry/complete',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの会員登録(完了ページ)です。','MARITIME, マリタイム, cbdオイル, cbd効果, cbd飲料, cbdとは, cbdオイル 使い方,cbdオイル 効果, cbd ショップ, cbd hemp おすすめ, cbdウオーター, cbdオイル高濃度, cbd オイル %, cbd おすすめ, cbdオイル 購入, cbd 日本 合法, cbdカンナビジオール, cbdオイル おすすめ, cbd コスパ, カンナビジオール, カンナビジオール 効果, カンナビジオール 厚生労働省','2017-03-07 10:14:52','2020-09-04 05:22:18',NULL,NULL,'page'),
	(21,NULL,'特定商取引法に基づく表記','help_tradelaw','Help/tradelaw',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの特定商取引に関する法律に基づく表記ページです。','MARITIME, マリタイム,マリタイム　特定商取引に関する法律に基づく表記, マリタイムご利用規約, おすすめのCBDウォーター、カンナビノイド効果を今すぐ試そう,cbdオイル, cbdオイル 効果, cbdオイル おすすめ,cbdオイル 日本, cbdオイル 使い方, cbdオイル 使い方,cbdオイルとは, cbdオイル 口コミ, cbdオイル 通販','2017-03-07 10:14:52','2020-09-04 05:22:49',NULL,NULL,'page'),
	(22,NULL,'本会員登録(完了ページ)','entry_activate','Entry/activate',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの本会員登録(完了ページ)です。','MARITIME, マリタイム, cbdオイル, cbd効果, cbd飲料, cbdとは, cbdオイル 使い方,cbdオイル 効果, cbd ショップ, cbd hemp おすすめ, cbdウオーター, cbdオイル高濃度, cbd オイル %, cbd おすすめ, cbdオイル 購入, cbd 日本 合法, cbdカンナビジオール, cbdオイル おすすめ, cbd コスパ, カンナビジオール, カンナビジオール 効果, カンナビジオール 厚生労働省','2017-03-07 10:14:52','2020-09-04 05:23:06',NULL,NULL,'page'),
	(23,NULL,'商品購入','shopping','Shopping/index',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの商品購入ページです。','MARITIME, マリタイム, cbdオイル, cbd効果, cbd飲料, cbdとは, cbdオイル 使い方,cbdオイル 効果, cbd ショップ, cbd hemp おすすめ, cbdウオーター, cbdオイル高濃度, cbd オイル %, cbd おすすめ, cbdオイル 購入, cbd 日本 合法, cbdカンナビジオール, cbdオイル おすすめ, cbd コスパ, カンナビジオール, カンナビジオール 効果, カンナビジオール 厚生労働省','2017-03-07 10:14:52','2020-09-04 05:23:22','noindex',NULL,'page'),
	(24,NULL,'商品購入/お届け先の指定','shopping_shipping','Shopping/shipping',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの商品購入/お届け先の指定ページです。','MARITIME, マリタイム, cbdオイル, cbd効果, cbd飲料, cbdとは, cbdオイル 使い方,cbdオイル 効果, cbd ショップ, cbd hemp おすすめ, cbdウオーター, cbdオイル高濃度, cbd オイル %, cbd おすすめ, cbdオイル 購入, cbd 日本 合法, cbdカンナビジオール, cbdオイル おすすめ, cbd コスパ, カンナビジオール, カンナビジオール 効果, カンナビジオール 厚生労働省','2017-03-07 10:14:52','2020-09-04 05:23:37','noindex',NULL,'page'),
	(25,NULL,'商品購入/お届け先の複数指定','shopping_shipping_multiple','Shopping/shipping_multiple',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの商品購入/お届け先の複数指定ページです。','MARITIME, マリタイム, cbdオイル, cbd効果, cbd飲料, cbdとは, cbdオイル 使い方,cbdオイル 効果, cbd ショップ, cbd hemp おすすめ, cbdウオーター, cbdオイル高濃度, cbd オイル %, cbd おすすめ, cbdオイル 購入, cbd 日本 合法, cbdカンナビジオール, cbdオイル おすすめ, cbd コスパ, カンナビジオール, カンナビジオール 効果, カンナビジオール 厚生労働省','2017-03-07 10:14:52','2020-09-04 05:23:50','noindex',NULL,'page'),
	(28,NULL,'商品購入/ご注文完了','shopping_complete','Shopping/complete',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの商品購入/ご注文完了ページです。','MARITIME, マリタイム, cbdオイル, cbd効果, cbd飲料, cbdとは, cbdオイル 使い方,cbdオイル 効果, cbd ショップ, cbd hemp おすすめ, cbdウオーター, cbdオイル高濃度, cbd オイル %, cbd おすすめ, cbdオイル 購入, cbd 日本 合法, cbdカンナビジオール, cbdオイル おすすめ, cbd コスパ, カンナビジオール, カンナビジオール 効果, カンナビジオール 厚生労働省','2017-03-07 10:14:52','2020-09-04 05:24:02','noindex',NULL,'page'),
	(29,NULL,'プライバシーポリシー','help_privacy','Help/privacy',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターのプライバシーポリシーページです。','MARITIME, マリタイム, マリタイムプライバシーポリシー, おすすめのCBDウォーター、カンナビノイド効果を今すぐ試そう,cbdオイル, cbdオイル 効果, cbdオイル おすすめ,cbdオイル 日本, cbdオイル 使い方, cbdオイル 使い方,cbdオイルとは, cbdオイル 口コミ, cbdオイル 通販','2017-03-07 10:14:52','2020-09-04 05:24:48',NULL,NULL,'page'),
	(30,NULL,'商品購入/ログイン','shopping_login','Shopping/login',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの商品購入ログインページです。','MARITIME, マリタイム, cbdオイル, cbd効果, cbd飲料, cbdとは, cbdオイル 使い方,cbdオイル 効果, cbd ショップ, cbd hemp おすすめ, cbdウオーター, cbdオイル高濃度, cbd オイル %, cbd おすすめ, cbdオイル 購入, cbd 日本 合法, cbdカンナビジオール, cbdオイル おすすめ, cbd コスパ, カンナビジオール, カンナビジオール 効果, カンナビジオール 厚生労働省','2017-03-07 10:14:52','2020-09-04 05:26:31',NULL,NULL,'page'),
	(31,NULL,'ゲスト購入情報入力','shopping_nonmember','Shopping/nonmember',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターのゲスト購入情報入力ページです。','MARITIME, マリタイム, マリタイムゲスト購入, cbdオイル, cbd効果, cbd飲料, cbdとは, cbdオイル 使い方,cbdオイル 効果, cbd ショップ, cbd hemp おすすめ, cbdウオーター, cbdオイル高濃度, cbd オイル %, cbd おすすめ, cbdオイル 購入, cbd 日本 合法, cbdカンナビジオール, cbdオイル おすすめ, cbd コスパ, カンナビジオール, カンナビジオール 効果, カンナビジオール 厚生労働省','2017-03-07 10:14:52','2020-09-04 05:27:41',NULL,NULL,'page'),
	(32,NULL,'商品購入/お届け先の追加','shopping_shipping_edit','Shopping/shipping_edit',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの商品購入/お届け先の追加ページです。','MARITIME, マリタイム, cbdオイル, cbd効果, cbd飲料, cbdとは, cbdオイル 使い方,cbdオイル 効果, cbd ショップ, cbd hemp おすすめ, cbdウオーター, cbdオイル高濃度, cbd オイル %, cbd おすすめ, cbdオイル 購入, cbd 日本 合法, cbdカンナビジオール, cbdオイル おすすめ, cbd コスパ, カンナビジオール, カンナビジオール 効果, カンナビジオール 厚生労働省','2017-03-07 01:15:02','2020-09-04 05:27:56','noindex',NULL,'page'),
	(33,NULL,'商品購入/お届け先の複数指定(お届け先の追加)','shopping_shipping_multiple_edit','Shopping/shipping_multiple_edit',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの商品購入/お届け先の複数指定(お届け先の追加)ページです。','MARITIME, マリタイム, cbdオイル, cbd効果, cbd飲料, cbdとは, cbdオイル 使い方,cbdオイル 効果, cbd ショップ, cbd hemp おすすめ, cbdウオーター, cbdオイル高濃度, cbd オイル %, cbd おすすめ, cbdオイル 購入, cbd 日本 合法, cbdカンナビジオール, cbdオイル おすすめ, cbd コスパ, カンナビジオール, カンナビジオール 効果, カンナビジオール 厚生労働省','2017-03-07 01:15:02','2020-09-04 05:28:07','noindex',NULL,'page'),
	(34,NULL,'商品購入/購入エラー','shopping_error','Shopping/shopping_error',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの商品購入/購入エラーページです。','MARITIME, マリタイム, cbdオイル, cbd効果, cbd飲料, cbdとは, cbdオイル 使い方,cbdオイル 効果, cbd ショップ, cbd hemp おすすめ, cbdウオーター, cbdオイル高濃度, cbd オイル %, cbd おすすめ, cbdオイル 購入, cbd 日本 合法, cbdカンナビジオール, cbdオイル おすすめ, cbd コスパ, カンナビジオール, カンナビジオール 効果, カンナビジオール 厚生労働省','2017-03-07 01:15:02','2020-09-04 05:28:17','noindex',NULL,'page'),
	(35,NULL,'ご利用ガイド','help_guide','Help/guide',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターのご利用ガイドページです。','MARITIME, マリタイム, マリタイムご利用ガイド, おすすめのCBDウォーター、カンナビノイド効果を今すぐ試そう,cbdオイル, cbdオイル 効果, cbdオイル おすすめ,cbdオイル 日本, cbdオイル 使い方, cbdオイル 使い方,cbdオイルとは, cbdオイル 口コミ, cbdオイル 通販','2017-03-07 01:15:02','2020-09-04 05:28:33',NULL,NULL,'page'),
	(36,NULL,'パスワード再発行(入力ページ)','forgot','Forgot/index',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターのパスワード再発行(入力ページ)です。','MARITIME, マリタイム, cbdオイル, cbd効果, cbd飲料, cbdとは, cbdオイル 使い方,cbdオイル 効果, cbd ショップ, cbd hemp おすすめ, cbdウオーター, cbdオイル高濃度, cbd オイル %, cbd おすすめ, cbdオイル 購入, cbd 日本 合法, cbdカンナビジオール, cbdオイル おすすめ, cbd コスパ, カンナビジオール, カンナビジオール 効果, カンナビジオール 厚生労働省','2017-03-07 01:15:02','2020-09-04 05:28:45',NULL,NULL,'page'),
	(37,NULL,'パスワード再発行(完了ページ)','forgot_complete','Forgot/complete',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターのパスワード再発行(完了ページ)です。','MARITIME, マリタイム, cbdオイル, cbd効果, cbd飲料, cbdとは, cbdオイル 使い方,cbdオイル 効果, cbd ショップ, cbd hemp おすすめ, cbdウオーター, cbdオイル高濃度, cbd オイル %, cbd おすすめ, cbdオイル 購入, cbd 日本 合法, cbdカンナビジオール, cbdオイル おすすめ, cbd コスパ, カンナビジオール, カンナビジオール 効果, カンナビジオール 厚生労働省','2017-03-07 01:15:02','2020-09-04 05:28:56','noindex',NULL,'page'),
	(38,NULL,'パスワード再発行(再設定ページ)','forgot_reset','Forgot/reset',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターのパスワード再発行(再設定ページ)です。','MARITIME, マリタイム, cbdオイル, cbd効果, cbd飲料, cbdとは, cbdオイル 使い方,cbdオイル 効果, cbd ショップ, cbd hemp おすすめ, cbdウオーター, cbdオイル高濃度, cbd オイル %, cbd おすすめ, cbdオイル 購入, cbd 日本 合法, cbdカンナビジオール, cbdオイル おすすめ, cbd コスパ, カンナビジオール, カンナビジオール 効果, カンナビジオール 厚生労働省','2017-03-07 01:15:02','2020-09-04 05:29:07','noindex',NULL,'page'),
	(42,NULL,'商品購入/遷移','shopping_redirect_to','Shopping/index',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターのパスワード商品購入/遷移ページです。','MARITIME, マリタイム, cbdオイル, cbd効果, cbd飲料, cbdとは, cbdオイル 使い方,cbdオイル 効果, cbd ショップ, cbd hemp おすすめ, cbdウオーター, cbdオイル高濃度, cbd オイル %, cbd おすすめ, cbdオイル 購入, cbd 日本 合法, cbdカンナビジオール, cbdオイル おすすめ, cbd コスパ, カンナビジオール, カンナビジオール 効果, カンナビジオール 厚生労働省','2017-03-07 01:15:03','2020-09-04 05:29:18','noindex',NULL,'page'),
	(44,8,'MYページ/お届け先編集','mypage_delivery_edit','Mypage/delivery_edit',2,NULL,NULL,NULL,'2017-03-07 01:15:05','2017-03-07 01:15:05','noindex',NULL,'page'),
	(45,NULL,'商品購入/ご注文確認','shopping_confirm','Shopping/confirm',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの商品購入/ご注文確認ページです。','MARITIME, マリタイム, cbdオイル, cbd効果, cbd飲料, cbdとは, cbdオイル 使い方,cbdオイル 効果, cbd ショップ, cbd hemp おすすめ, cbdウオーター, cbdオイル高濃度, cbd オイル %, cbd おすすめ, cbdオイル 購入, cbd 日本 合法, cbdカンナビジオール, cbdオイル おすすめ, cbd コスパ, カンナビジオール, カンナビジオール 効果, カンナビジオール 厚生労働省','2017-03-07 01:15:03','2020-09-04 05:29:29','noindex',NULL,'page'),
	(46,NULL,'レビューを投稿','product_review_index','@ProductReview4/default/index',2,NULL,NULL,NULL,'2020-05-06 11:18:12','2020-05-06 11:18:12',NULL,NULL,'page'),
	(47,NULL,'レビューを投稿(完了)','product_review_complete','@ProductReview4/default/index',2,NULL,NULL,NULL,'2020-05-06 11:18:12','2020-05-06 11:18:12',NULL,NULL,'page'),
	(48,NULL,'MYページ/カード情報編集','gmo_mypage_card_edit','@GmoPaymentGateway4/mypage_card',2,NULL,NULL,NULL,'2020-05-14 22:31:28','2020-05-14 22:31:28','noindex',NULL,'page'),
	(50,NULL,'代理店ページ','partner','partner',0,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの代理店ページです。','MARITIME, マリタイム, マリタイム代理店ページ, おすすめのCBDウォーター、カンナビノイド効果を今すぐ試そう,cbdオイル, cbdオイル 効果, cbdオイル おすすめ,cbdオイル 日本, cbdオイル 使い方, cbdオイル 使い方,cbdオイルとは, cbdオイル 口コミ, cbdオイル 通販','2020-05-16 11:47:45','2020-09-04 05:29:43',NULL,NULL,'page'),
	(51,NULL,'商品購入/ご注文確認','paypal_confirm','Shopping/confirm',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの商品購入/ご注文確認ページです。','MARITIME, マリタイム, cbdオイル, cbd効果, cbd飲料, cbdとは, cbdオイル 使い方,cbdオイル 効果, cbd ショップ, cbd hemp おすすめ, cbdウオーター, cbdオイル高濃度, cbd オイル %, cbd おすすめ, cbdオイル 購入, cbd 日本 合法, cbdカンナビジオール, cbdオイル おすすめ, cbd コスパ, カンナビジオール, カンナビジオール 効果, カンナビジオール 厚生労働省','2020-05-24 08:43:48','2020-09-04 05:38:39','noindex',NULL,'page'),
	(53,NULL,'FAQページ','faq','faq',0,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターのFAQ、よくある質問ページです。','MARITIME, マリタイム, マリタイムFAQ, マリタイムよくある質問, おすすめのCBDウォーター、カンナビノイド効果を今すぐ試そう,cbdオイル, cbdオイル 効果, cbdオイル おすすめ,cbdオイル 日本, cbdオイル 使い方, cbdオイル 使い方,cbdオイルとは, cbdオイル 口コミ, cbdオイル 通販','2020-06-12 01:05:01','2020-09-04 05:38:57',NULL,NULL,'page'),
	(54,NULL,'CBD成分分析表','analysis','analysis',0,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターのCBD成分分析表ページです。','MARITIME, マリタイム, マリタイムCBD成分分析表, おすすめのCBDウォーター、カンナビノイド効果を今すぐ試そう,cbdオイル, cbdオイル 効果, cbdオイル おすすめ,cbdオイル 日本, cbdオイル 使い方, cbdオイル 使い方,cbdオイルとは, cbdオイル 口コミ, cbdオイル 通販','2020-06-19 06:45:58','2020-09-04 05:39:12',NULL,NULL,'page'),
	(56,NULL,'CBD摂取量ページ','cbd_intake','cbd_intake',0,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターのCBD摂取量ページです。','MARITIME, マリタイム, マリタイムCBD摂取量ページ, マリタイムCBD摂取量ガガイド, マリタイムCBD摂取量目安, CBDおすすめのCBDウォーター、カンナビノイド効果を今すぐ試そう,cbdオイル, cbdオイル 効果, cbdオイル おすすめ,cbdオイル 日本, cbdオイル 使い方, cbdオイル 使い方,cbdオイルとは, cbdオイル 口コミ, cbdオイル 通販','2020-07-06 02:40:37','2020-09-04 05:39:40',NULL,NULL,'page'),
	(59,NULL,'商品購入/クーポン利用','plugin_coupon_shopping','Coupon4/Resource/template/default/shopping_coupon',2,NULL,NULL,NULL,'2020-08-06 14:49:14','2020-08-06 14:49:14','noindex',NULL,'page'),
	(60,NULL,'100種類のCBDオイルを飲み比べ、辿り着いたこの味。  メイド・イン・ジャパンの品質、オリーブオイルを使った天然由来CBDオイル','maritime_lp_jp','maritime_lp_jp',0,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。CBDオイルは1%から10%までの高濃度をご用意。コスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターもおすすめです。','MARITIME, マリタイム, マリタイムおすすめのCBDウォーター、カンナビノイド効果を今すぐ試そう,cbdオイル, cbdオイル 効果, cbdオイル おすすめ,cbdオイル 日本, cbdオイル 使い方, cbdオイル 使い方,cbdオイルとは, cbdオイル 口コミ, cbdオイル 通販,','2020-08-21 00:32:16','2020-09-04 05:39:53',NULL,NULL,'page'),
	(62,NULL,'お問い合わせ(入力ページ)','partner_entry','Partner/index',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターのお問い合わせ(入力ページ)です。','MARITIME, マリタイム, cbdオイル, cbd効果, cbd飲料, cbdとは, cbdオイル 使い方,cbdオイル 効果, cbd ショップ, cbd hemp おすすめ, cbdウオーター, cbdオイル高濃度, cbd オイル %, cbd おすすめ, cbdオイル 購入, cbd 日本 合法, cbdカンナビジオール, cbdオイル おすすめ, cbd コスパ, カンナビジオール, カンナビジオール 効果, カンナビジオール 厚生労働省','2017-03-07 10:14:52','2020-09-04 05:21:00',NULL,NULL,'page'),
	(63,NULL,'お問い合わせ(完了ページ)','partner_complete','Partner/complete',2,NULL,'日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターのお問い合わせ(完了ページ)です。','MARITIME, マリタイム, cbdオイル, cbd効果, cbd飲料, cbdとは, cbdオイル 使い方,cbdオイル 効果, cbd ショップ, cbd hemp おすすめ, cbdウオーター, cbdオイル高濃度, cbd オイル %, cbd おすすめ, cbdオイル 購入, cbd 日本 合法, cbdカンナビジオール, cbdオイル おすすめ, cbd コスパ, カンナビジオール, カンナビジオール 効果, カンナビジオール 厚生労働省','2017-03-07 10:14:52','2020-09-04 05:21:14',NULL,NULL,'page');

/*!40000 ALTER TABLE `dtb_page` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dtb_page_layout
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dtb_page_layout`;

CREATE TABLE `dtb_page_layout` (
  `page_id` int(10) unsigned NOT NULL,
  `layout_id` int(10) unsigned NOT NULL,
  `sort_no` smallint(5) unsigned NOT NULL,
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`page_id`,`layout_id`),
  KEY `IDX_F2799941C4663E4` (`page_id`),
  KEY `IDX_F27999418C22AA1A` (`layout_id`),
  CONSTRAINT `FK_F27999418C22AA1A` FOREIGN KEY (`layout_id`) REFERENCES `dtb_layout` (`id`),
  CONSTRAINT `FK_F2799941C4663E4` FOREIGN KEY (`page_id`) REFERENCES `dtb_page` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dtb_page_layout` WRITE;
/*!40000 ALTER TABLE `dtb_page_layout` DISABLE KEYS */;

INSERT INTO `dtb_page_layout` (`page_id`, `layout_id`, `sort_no`, `discriminator_type`)
VALUES
	(0,0,2,'pagelayout'),
	(1,1,43,'pagelayout'),
	(2,3,43,'pagelayout'),
	(3,4,43,'pagelayout'),
	(4,2,43,'pagelayout'),
	(5,2,43,'pagelayout'),
	(6,2,43,'pagelayout'),
	(7,2,43,'pagelayout'),
	(8,2,43,'pagelayout'),
	(9,2,43,'pagelayout'),
	(10,2,43,'pagelayout'),
	(11,2,43,'pagelayout'),
	(12,2,43,'pagelayout'),
	(13,2,43,'pagelayout'),
	(14,2,43,'pagelayout'),
	(15,2,43,'pagelayout'),
	(16,2,43,'pagelayout'),
	(17,2,43,'pagelayout'),
	(18,2,43,'pagelayout'),
	(19,2,43,'pagelayout'),
	(20,2,43,'pagelayout'),
	(21,2,43,'pagelayout'),
	(22,2,43,'pagelayout'),
	(23,2,43,'pagelayout'),
	(24,2,43,'pagelayout'),
	(25,2,43,'pagelayout'),
	(28,2,43,'pagelayout'),
	(29,2,43,'pagelayout'),
	(30,2,43,'pagelayout'),
	(31,2,43,'pagelayout'),
	(32,2,43,'pagelayout'),
	(33,2,43,'pagelayout'),
	(34,2,43,'pagelayout'),
	(35,2,43,'pagelayout'),
	(36,2,43,'pagelayout'),
	(37,2,43,'pagelayout'),
	(38,2,43,'pagelayout'),
	(42,2,43,'pagelayout'),
	(44,2,40,'pagelayout'),
	(45,2,43,'pagelayout'),
	(46,2,0,'pagelayout'),
	(47,2,0,'pagelayout'),
	(48,2,42,'pagelayout'),
	(50,5,43,'pagelayout'),
	(51,2,43,'pagelayout'),
	(53,5,43,'pagelayout'),
	(54,5,43,'pagelayout'),
	(56,5,43,'pagelayout'),
	(59,2,0,'pagelayout'),
	(60,6,43,'pagelayout'),
	(62,2,0,'pagelayout'),
	(63,2,0,'pagelayout');

/*!40000 ALTER TABLE `dtb_page_layout` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dtb_payment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dtb_payment`;

CREATE TABLE `dtb_payment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `creator_id` int(10) unsigned DEFAULT NULL,
  `payment_method` varchar(255) DEFAULT NULL,
  `charge` decimal(12,2) unsigned DEFAULT 0.00,
  `rule_max` decimal(12,2) unsigned DEFAULT NULL,
  `sort_no` smallint(5) unsigned DEFAULT NULL,
  `fixed` tinyint(1) NOT NULL DEFAULT 1,
  `payment_image` varchar(255) DEFAULT NULL,
  `rule_min` decimal(12,2) unsigned DEFAULT NULL,
  `method_class` varchar(255) DEFAULT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT 1,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_7AFF628F61220EA6` (`creator_id`),
  CONSTRAINT `FK_7AFF628F61220EA6` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dtb_payment` WRITE;
/*!40000 ALTER TABLE `dtb_payment` DISABLE KEYS */;

INSERT INTO `dtb_payment` (`id`, `creator_id`, `payment_method`, `charge`, `rule_max`, `sort_no`, `fixed`, `payment_image`, `rule_min`, `method_class`, `visible`, `create_date`, `update_date`, `discriminator_type`)
VALUES
	(1,1,'郵便振替',0.00,NULL,1,1,NULL,0.00,'Eccube\\Service\\Payment\\Method\\Cash',0,'2017-03-07 10:14:52','2020-08-30 07:38:21','payment'),
	(2,1,'現金書留',0.00,NULL,1,1,NULL,0.00,'Eccube\\Service\\Payment\\Method\\Cash',0,'2017-03-07 10:14:52','2020-08-30 07:38:21','payment'),
	(3,1,'銀行振込',0.00,NULL,9,1,NULL,0.00,'Eccube\\Service\\Payment\\Method\\Cash',1,'2017-03-07 10:14:52','2020-08-30 07:38:25','payment'),
	(4,1,'代金引換',0.00,NULL,1,1,NULL,0.00,'Eccube\\Service\\Payment\\Method\\Cash',0,'2017-03-07 10:14:52','2020-08-30 07:38:21','payment'),
	(5,1,'クレジット決済',0.00,NULL,13,1,NULL,1.00,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',1,'2020-05-14 22:31:28','2020-08-30 07:38:21','payment'),
	(6,1,'コンビニ決済',0.00,299999.00,7,1,NULL,1.00,'Plugin\\GmoPaymentGateway4\\Service\\Method\\Cvs',0,'2020-05-14 22:31:28','2020-08-30 07:38:21','payment'),
	(7,1,'Pay-easy決済(銀行ATM)',0.00,999999.00,2,1,NULL,1.00,'Plugin\\GmoPaymentGateway4\\Service\\Method\\PayEasyAtm',0,'2020-05-14 22:31:28','2020-08-30 07:38:21','payment'),
	(8,1,'Pay-easy決済(ネットバンク)',0.00,999999.00,3,1,NULL,1.00,'Plugin\\GmoPaymentGateway4\\Service\\Method\\PayEasyNet',0,'2020-05-14 22:31:28','2020-08-30 07:38:21','payment'),
	(9,1,'auかんたん決済',0.00,9999999.00,4,1,NULL,1.00,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CarAu',0,'2020-05-14 22:31:28','2020-08-30 07:38:21','payment'),
	(10,1,'ドコモケータイ払い',0.00,30000.00,5,1,NULL,1.00,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CarDocomo',0,'2020-05-14 22:31:28','2020-08-30 07:38:21','payment'),
	(11,1,'ソフトバンクまとめて支払い',0.00,100000.00,6,1,NULL,1.00,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CarSoftbank',0,'2020-05-14 22:31:28','2020-08-30 07:38:21','payment'),
	(12,1,'楽天ペイ',0.00,99999999.00,8,1,NULL,100.00,'Plugin\\GmoPaymentGateway4\\Service\\Method\\RakutenPay',0,'2020-05-14 22:31:28','2020-08-30 07:38:21','payment'),
	(13,1,'クレジット決済「定期購入」',0.00,NULL,12,1,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',0,'2020-05-23 07:17:19','2020-08-30 07:38:21','payment'),
	(14,1,'PayPal決済',0.00,1000000.00,11,1,NULL,1.00,'Plugin\\PayPalCheckout\\Service\\Method\\CreditCard',1,'2020-05-24 08:43:48','2020-08-30 07:38:21','payment'),
	(15,1,'かんたん銀行決済(PayPal)',0.00,1000000.00,10,1,NULL,1.00,'Plugin\\PayPalCheckout\\Service\\Method\\BankTransfer',1,'2020-05-24 08:43:48','2020-08-30 07:38:21','payment');

/*!40000 ALTER TABLE `dtb_payment` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dtb_payment_option
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dtb_payment_option`;

CREATE TABLE `dtb_payment_option` (
  `delivery_id` int(10) unsigned NOT NULL,
  `payment_id` int(10) unsigned NOT NULL,
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`delivery_id`,`payment_id`),
  KEY `IDX_5631540D12136921` (`delivery_id`),
  KEY `IDX_5631540D4C3A3BB` (`payment_id`),
  CONSTRAINT `FK_5631540D12136921` FOREIGN KEY (`delivery_id`) REFERENCES `dtb_delivery` (`id`),
  CONSTRAINT `FK_5631540D4C3A3BB` FOREIGN KEY (`payment_id`) REFERENCES `dtb_payment` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dtb_payment_option` WRITE;
/*!40000 ALTER TABLE `dtb_payment_option` DISABLE KEYS */;

INSERT INTO `dtb_payment_option` (`delivery_id`, `payment_id`, `discriminator_type`)
VALUES
	(1,3,'paymentoption'),
	(1,5,'paymentoption'),
	(1,12,'paymentoption'),
	(1,14,'paymentoption'),
	(1,15,'paymentoption'),
	(2,13,'paymentoption');

/*!40000 ALTER TABLE `dtb_payment_option` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dtb_plugin
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dtb_plugin`;

CREATE TABLE `dtb_plugin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 0,
  `version` varchar(255) NOT NULL,
  `source` varchar(255) NOT NULL,
  `initialized` tinyint(1) NOT NULL DEFAULT 0,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dtb_plugin` WRITE;
/*!40000 ALTER TABLE `dtb_plugin` DISABLE KEYS */;

INSERT INTO `dtb_plugin` (`id`, `name`, `code`, `enabled`, `version`, `source`, `initialized`, `create_date`, `update_date`, `discriminator_type`)
VALUES
	(1,'taba app CMSプラグイン','TabaCMS',1,'1.0.8','1719',1,'2020-05-06 09:37:40','2020-05-15 05:35:57','plugin'),
	(2,'商品レビュー管理プラグイン','ProductReview4',1,'4.0.1','1756',1,'2020-05-06 11:16:46','2020-05-10 07:56:47','plugin'),
	(3,'商品問い合わせ for EC-CUBE4','ProductContact4',1,'1.0.3','2005',1,'2020-05-09 00:00:10','2020-05-09 02:41:47','plugin'),
	(4,'ソーシャルボタンを追加','MGSocialButton',1,'1.0.2','1776',1,'2020-05-09 00:02:09','2020-05-09 02:42:28','plugin'),
	(5,'お問い合わせ管理 for EC-CUBE4','ContactManagement4',1,'1.0.3','2004',1,'2020-05-09 00:07:13','2020-05-09 02:43:00','plugin'),
	(6,'PGマルチペイメントサービス決済プラグイン','GmoPaymentGateway4',1,'1.0.4','1797',1,'2020-05-14 22:12:27','2020-05-16 10:43:48','plugin'),
	(8,'メールマガジンプラグイン','MailMagazine4',1,'4.0.2','1760',1,'2020-05-16 10:30:32','2020-05-16 10:43:06','plugin'),
	(9,'ペイパルチェックアウト決済プラグイン(4.0系)','PayPalCheckout',1,'1.0.5','1930',1,'2020-05-24 08:13:13','2020-05-24 08:43:48','plugin'),
	(10,'特定会員価格プラグイン for EC-CUBE4','CustomerClassPrice4',1,'1.0.5','1929',1,'2020-05-27 01:36:39','2020-05-27 01:41:47','plugin'),
	(13,'商品おすすめ順並び替えプラグイン for EC-CUBE4','ProductDisplayRank4',1,'1.0.1','2020',1,'2020-06-24 10:28:48','2020-06-26 08:07:28','plugin'),
	(18,'予約商品プラグイン','ProductReserve4',1,'1.0.10','2011',1,'2020-07-15 10:36:30','2020-07-15 10:48:18','plugin'),
	(19,'Coupon Plugin for EC-CUBE4','Coupon4',1,'4.0.7','1923',1,'2020-07-15 15:24:58','2020-08-06 14:49:14','plugin'),
	(21,'カゴ落ちチェックプラグイン','CheckKagoochi',1,'1.0.1','2087',1,'2020-09-04 03:55:52','2020-09-04 04:02:12','plugin'),
	(22,'全ページ対応パンくずリスト表示プラグイン for EC-CUBE4','BreadcrumbList4',1,'2.0.0','1762',1,'2020-09-04 04:45:13','2020-09-04 04:47:03','plugin');

/*!40000 ALTER TABLE `dtb_plugin` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dtb_product
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dtb_product`;

CREATE TABLE `dtb_product` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `creator_id` int(10) unsigned DEFAULT NULL,
  `product_status_id` smallint(5) unsigned DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `note` varchar(4000) DEFAULT NULL,
  `description_list` varchar(4000) DEFAULT NULL,
  `description_detail` varchar(4000) DEFAULT NULL,
  `search_word` varchar(4000) DEFAULT NULL,
  `free_area` longtext DEFAULT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL,
  `plg_ccp_enabled_discount` tinyint(1) DEFAULT 1,
  `display_rank` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `IDX_C49DE22F61220EA6` (`creator_id`),
  KEY `IDX_C49DE22F557B630` (`product_status_id`),
  CONSTRAINT `FK_C49DE22F557B630` FOREIGN KEY (`product_status_id`) REFERENCES `mtb_product_status` (`id`),
  CONSTRAINT `FK_C49DE22F61220EA6` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dtb_product` WRITE;
/*!40000 ALTER TABLE `dtb_product` DISABLE KEYS */;

INSERT INTO `dtb_product` (`id`, `creator_id`, `product_status_id`, `name`, `note`, `description_list`, `description_detail`, `search_word`, `free_area`, `create_date`, `update_date`, `discriminator_type`, `plg_ccp_enabled_discount`, `display_rank`)
VALUES
	(2,1,3,'MARITIME CBDオイル 30ml (10%)',NULL,NULL,'10%\r\n30ml/3000mg\r\nトライアル用\r\n\r\nまずはMaritimeの確かな品質をCBD1%のトライアルオイルでお試しください。\r\nその他用途に合わせて５タイプの濃度と２タイプのサイズからお選びいただけます。\r\n常に健康に気を配りたい方へのサブスク（定期購買）システムにも対応しています。','Maritime CBD 3000 3000mg 30ml 10% CBDオイル',NULL,'2018-09-28 10:14:52','2020-06-17 05:19:05','product',1,0),
	(4,1,1,'MARITIME CBD ウォーター 500ml','スライダー順番：1',NULL,'front.water_description','MARITIME CBD Infused mineral water ウォーター 500ml CBD 24mg',NULL,'2020-04-29 00:06:19','2020-08-06 12:27:29','product',1,110),
	(5,1,1,'MARITIME CBDオイル 10ml (10%)','スライダー順番：8',NULL,'front.cbd_1000_description','MARITIME CBD1000 1000mg 10ml 10% CBDオイル',NULL,'2020-05-10 03:52:28','2020-08-06 12:38:00','product',1,80),
	(6,1,1,'MARITIME CBDオイル 10ml (5%)','スライダー順番：7',NULL,'front.cbd_500_description','MARITIME CBD500 500mg 10ml 5% CBDオイル',NULL,'2020-05-10 04:01:39','2020-08-06 12:36:28','product',1,60),
	(7,3,1,'MARITIME CBDオイル 10ml (3%)','スライダー順番：6',NULL,'front.cbd_300_10_description','MARITIME CBD300 300mg 10ml 3% CBDオイル',NULL,'2020-05-10 04:13:02','2020-09-06 17:25:26','product',1,50),
	(8,1,1,'MARITIME CBDオイル 10ml (2%)','スライダー順番：5',NULL,'front.cbd_200_description','MARITIME CBD200 200mg 10ml 2% CBDオイル',NULL,'2020-05-10 04:14:21','2020-08-06 12:30:29','product',1,30),
	(9,1,1,'MARITIME CBDオイル 10ml (1%)','スライダー順番：4',NULL,'front.cbd_100_description','MARITIME CBD100 100mg 10ml 1% CBDオイル',NULL,'2020-05-10 04:21:25','2020-08-06 12:32:41','product',1,10),
	(10,1,1,'MARITIME CBDオイル 30ml (5%)','スライダー順番：2',NULL,'front.cbd_1500_description','MARITIME CBD 1500 1500mg 30ml 5% CBDオイル',NULL,'2020-05-10 04:23:31','2020-08-06 12:38:56','product',1,90),
	(11,1,1,'MARITIME CBDオイル 30ml (3%)','スライダー順番：1',NULL,'front.cbd_900_description','MARITIME CBD900 900mg 30ml 3% CBDオイル',NULL,'2020-05-10 04:24:53','2020-08-06 12:37:13','product',1,70),
	(12,1,1,'MARITIME CBDオイル 30ml (2%)','スライダー順番：最後',NULL,'front.cbd_600_description','MARITIME CBD 600 3000mg 30ml 2% CBDオイル',NULL,'2020-05-10 04:28:10','2020-08-06 12:28:48','product',1,40),
	(13,1,1,'MARITIME CBDオイル 30ml (1%)','スライダー順番：9',NULL,'front.cbd_300_30_description','MARITIME CBD300 300mg 30ml 1% CBDオイル',NULL,'2020-05-10 04:31:16','2020-08-06 12:29:34','product',1,20),
	(14,3,1,'MARITIME CBDオイル 30ml (10%)','スライダー順番：3',NULL,'front.cbd_3000_description','MARITIME CBD 3000 3000mg 30ml 10% CBDオイル',NULL,'2020-05-10 04:36:03','2020-09-06 17:24:53','product',1,100),
	(15,1,1,'MARITIME CBD ウォーター 500ml ６本セット','スライダー順番：2',NULL,'front.water_set_description','MARITIME CBD Infused mineral water ウォーター 500ml ６本セット CBD 24mg',NULL,'2020-06-16 23:57:38','2020-08-06 12:26:50','product',1,120);

/*!40000 ALTER TABLE `dtb_product` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dtb_product_category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dtb_product_category`;

CREATE TABLE `dtb_product_category` (
  `product_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`product_id`,`category_id`),
  KEY `IDX_B05778914584665A` (`product_id`),
  KEY `IDX_B057789112469DE2` (`category_id`),
  CONSTRAINT `FK_B057789112469DE2` FOREIGN KEY (`category_id`) REFERENCES `dtb_category` (`id`),
  CONSTRAINT `FK_B05778914584665A` FOREIGN KEY (`product_id`) REFERENCES `dtb_product` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dtb_product_category` WRITE;
/*!40000 ALTER TABLE `dtb_product_category` DISABLE KEYS */;

INSERT INTO `dtb_product_category` (`product_id`, `category_id`, `discriminator_type`)
VALUES
	(4,1,'productcategory'),
	(4,7,'productcategory'),
	(5,1,'productcategory'),
	(5,3,'productcategory'),
	(5,19,'productcategory'),
	(5,21,'productcategory'),
	(5,31,'productcategory'),
	(5,32,'productcategory'),
	(6,1,'productcategory'),
	(6,3,'productcategory'),
	(6,19,'productcategory'),
	(6,22,'productcategory'),
	(6,31,'productcategory'),
	(6,32,'productcategory'),
	(7,1,'productcategory'),
	(7,3,'productcategory'),
	(7,19,'productcategory'),
	(7,23,'productcategory'),
	(7,31,'productcategory'),
	(7,32,'productcategory'),
	(8,1,'productcategory'),
	(8,3,'productcategory'),
	(8,19,'productcategory'),
	(8,24,'productcategory'),
	(8,31,'productcategory'),
	(8,32,'productcategory'),
	(9,1,'productcategory'),
	(9,3,'productcategory'),
	(9,19,'productcategory'),
	(9,25,'productcategory'),
	(9,31,'productcategory'),
	(9,32,'productcategory'),
	(10,1,'productcategory'),
	(10,3,'productcategory'),
	(10,20,'productcategory'),
	(10,22,'productcategory'),
	(10,31,'productcategory'),
	(10,32,'productcategory'),
	(11,1,'productcategory'),
	(11,3,'productcategory'),
	(11,20,'productcategory'),
	(11,23,'productcategory'),
	(11,31,'productcategory'),
	(11,32,'productcategory'),
	(12,1,'productcategory'),
	(12,3,'productcategory'),
	(12,20,'productcategory'),
	(12,24,'productcategory'),
	(12,31,'productcategory'),
	(12,32,'productcategory'),
	(13,1,'productcategory'),
	(13,3,'productcategory'),
	(13,20,'productcategory'),
	(13,25,'productcategory'),
	(13,31,'productcategory'),
	(13,32,'productcategory'),
	(14,1,'productcategory'),
	(14,3,'productcategory'),
	(14,20,'productcategory'),
	(14,21,'productcategory'),
	(14,31,'productcategory'),
	(14,32,'productcategory'),
	(15,1,'productcategory'),
	(15,7,'productcategory');

/*!40000 ALTER TABLE `dtb_product_category` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dtb_product_class
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dtb_product_class`;

CREATE TABLE `dtb_product_class` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned DEFAULT NULL,
  `sale_type_id` smallint(5) unsigned DEFAULT NULL,
  `class_category_id1` int(10) unsigned DEFAULT NULL,
  `class_category_id2` int(10) unsigned DEFAULT NULL,
  `delivery_duration_id` int(10) unsigned DEFAULT NULL,
  `creator_id` int(10) unsigned DEFAULT NULL,
  `product_code` varchar(255) DEFAULT NULL,
  `stock` decimal(10,0) DEFAULT NULL,
  `stock_unlimited` tinyint(1) NOT NULL DEFAULT 0,
  `sale_limit` decimal(10,0) unsigned DEFAULT NULL,
  `price01` decimal(12,2) DEFAULT NULL,
  `price02` decimal(12,2) NOT NULL,
  `delivery_fee` decimal(12,2) unsigned DEFAULT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT 1,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `currency_code` varchar(255) DEFAULT NULL,
  `point_rate` decimal(10,0) unsigned DEFAULT NULL,
  `discriminator_type` varchar(255) NOT NULL,
  `subscription_pricing` int(10) unsigned DEFAULT NULL,
  `use_subscription` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `IDX_1A11D1BA4584665A` (`product_id`),
  KEY `IDX_1A11D1BAB0524E01` (`sale_type_id`),
  KEY `IDX_1A11D1BA248D128` (`class_category_id1`),
  KEY `IDX_1A11D1BA9B418092` (`class_category_id2`),
  KEY `IDX_1A11D1BABA4269E` (`delivery_duration_id`),
  KEY `IDX_1A11D1BA61220EA6` (`creator_id`),
  KEY `dtb_product_class_price02_idx` (`price02`),
  KEY `dtb_product_class_stock_stock_unlimited_idx` (`stock`,`stock_unlimited`),
  CONSTRAINT `FK_1A11D1BA248D128` FOREIGN KEY (`class_category_id1`) REFERENCES `dtb_class_category` (`id`),
  CONSTRAINT `FK_1A11D1BA4584665A` FOREIGN KEY (`product_id`) REFERENCES `dtb_product` (`id`),
  CONSTRAINT `FK_1A11D1BA61220EA6` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`id`),
  CONSTRAINT `FK_1A11D1BA9B418092` FOREIGN KEY (`class_category_id2`) REFERENCES `dtb_class_category` (`id`),
  CONSTRAINT `FK_1A11D1BAB0524E01` FOREIGN KEY (`sale_type_id`) REFERENCES `mtb_sale_type` (`id`),
  CONSTRAINT `FK_1A11D1BABA4269E` FOREIGN KEY (`delivery_duration_id`) REFERENCES `dtb_delivery_duration` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dtb_product_class` WRITE;
/*!40000 ALTER TABLE `dtb_product_class` DISABLE KEYS */;

INSERT INTO `dtb_product_class` (`id`, `product_id`, `sale_type_id`, `class_category_id1`, `class_category_id2`, `delivery_duration_id`, `creator_id`, `product_code`, `stock`, `stock_unlimited`, `sale_limit`, `price01`, `price02`, `delivery_fee`, `visible`, `create_date`, `update_date`, `currency_code`, `point_rate`, `discriminator_type`, `subscription_pricing`, `use_subscription`)
VALUES
	(11,2,1,NULL,NULL,4,1,'cbd-3000',100,0,5,NULL,9999.00,NULL,1,'2017-03-07 10:14:52','2020-06-17 05:19:05','JPY',NULL,'productclass',NULL,0),
	(23,4,1,NULL,NULL,4,1,'cbd-water-500',100,0,NULL,NULL,5000.00,NULL,0,'2020-04-29 00:06:19','2020-05-15 01:41:10','JPY',NULL,'productclass',NULL,0),
	(24,5,1,NULL,NULL,4,1,'cbd-1000',100,0,5,NULL,9999.00,NULL,0,'2020-05-10 03:52:28','2020-05-15 01:46:01','JPY',NULL,'productclass',NULL,0),
	(25,6,1,NULL,NULL,4,1,'cbd-500',100,0,5,NULL,9999.00,NULL,0,'2020-05-10 04:01:39','2020-05-15 01:47:02','JPY',NULL,'productclass',NULL,0),
	(26,7,1,NULL,NULL,4,1,'cbd-300',100,0,5,NULL,9999.00,NULL,0,'2020-05-10 04:13:02','2020-05-15 01:56:25','JPY',NULL,'productclass',NULL,0),
	(27,8,1,NULL,NULL,4,1,'cbd-200',100,0,5,NULL,9999.00,NULL,0,'2020-05-10 04:14:21','2020-05-15 01:55:04','JPY',NULL,'productclass',NULL,0),
	(28,9,1,NULL,NULL,4,1,'cbd-100',100,0,5,NULL,9999.00,NULL,0,'2020-05-10 04:21:25','2020-05-15 01:47:58','JPY',NULL,'productclass',NULL,0),
	(29,10,1,NULL,NULL,4,1,'cbd-1500',100,0,5,NULL,9999.00,NULL,0,'2020-05-10 04:23:31','2020-05-15 01:51:51','JPY',NULL,'productclass',NULL,0),
	(30,11,1,NULL,NULL,4,1,'cbd-900',100,0,5,NULL,9999.00,NULL,0,'2020-05-10 04:24:53','2020-05-15 01:53:59','JPY',NULL,'productclass',NULL,0),
	(31,12,1,NULL,NULL,4,1,'cbd-600',100,0,5,NULL,9999.00,NULL,0,'2020-05-10 04:28:10','2020-05-15 01:52:49','JPY',NULL,'productclass',NULL,0),
	(32,13,1,NULL,NULL,4,1,'cbd-300',100,0,5,NULL,9999.00,NULL,0,'2020-05-10 04:31:16','2020-05-15 01:50:03','JPY',NULL,'productclass',NULL,0),
	(33,14,1,NULL,NULL,4,1,'cbd-3000',100,0,5,NULL,9999.00,NULL,0,'2020-05-10 04:36:03','2020-05-14 22:30:04','JPY',NULL,'productclass',NULL,0),
	(34,14,1,1,NULL,4,1,'cbd-1000',5,0,NULL,NULL,4000.00,NULL,0,'2020-05-10 04:42:02','2020-05-14 22:23:53','JPY',NULL,'productclass',NULL,0),
	(35,14,1,2,NULL,4,1,'cbd-3000',5,0,NULL,NULL,6999.00,NULL,0,'2020-05-10 04:42:02','2020-05-14 22:23:53','JPY',NULL,'productclass',NULL,0),
	(36,14,1,3,NULL,2,3,'cbd-3000',2,0,10,47500.00,47500.00,NULL,1,'2020-05-14 22:26:10','2020-09-07 05:24:19',NULL,NULL,'productclass',NULL,0),
	(37,14,2,4,NULL,2,3,'cbd-3000-s',1,0,10,NULL,38000.00,NULL,1,'2020-05-14 22:26:10','2020-09-06 17:25:05',NULL,NULL,'productclass',NULL,0),
	(38,4,1,3,NULL,2,1,'cbd-water-500',4,0,60,1160.00,1160.00,NULL,1,'2020-05-15 01:41:10','2020-09-15 02:41:26','JPY',NULL,'productclass',NULL,0),
	(39,4,2,4,NULL,2,1,'cbd-water-500-s',4,0,60,NULL,928.00,NULL,1,'2020-05-15 01:41:10','2020-08-21 04:00:32','JPY',NULL,'productclass',NULL,0),
	(40,5,1,3,NULL,2,1,'cbd-1000',1,0,10,26400.00,26400.00,NULL,1,'2020-05-15 01:46:01','2020-08-21 04:02:16','JPY',NULL,'productclass',NULL,0),
	(41,5,2,4,NULL,2,1,'cbd-1000-s',1,0,10,NULL,21120.00,NULL,1,'2020-05-15 01:46:01','2020-08-21 04:02:16','JPY',NULL,'productclass',NULL,0),
	(42,6,1,3,NULL,2,1,'cbd-500',1,0,10,14400.00,14400.00,NULL,1,'2020-05-15 01:47:02','2020-08-21 04:04:52','JPY',NULL,'productclass',NULL,0),
	(43,6,2,4,NULL,2,1,'cbd-500-s',1,0,10,NULL,11520.00,NULL,1,'2020-05-15 01:47:02','2020-08-21 04:04:52','JPY',NULL,'productclass',NULL,0),
	(44,9,1,3,NULL,2,1,'cbd-100',2,0,10,4800.00,4800.00,NULL,1,'2020-05-15 01:47:58','2020-09-15 02:41:26','JPY',NULL,'productclass',NULL,0),
	(45,9,2,4,NULL,2,1,'cbd-100-s',2,0,10,NULL,3840.00,NULL,1,'2020-05-15 01:47:58','2020-08-21 04:04:29','JPY',NULL,'productclass',NULL,0),
	(46,13,1,3,NULL,2,1,'cbd-300-30',1,0,10,9100.00,9100.00,NULL,1,'2020-05-15 01:50:03','2020-08-21 04:01:11','JPY',NULL,'productclass',NULL,0),
	(47,13,2,4,NULL,2,1,'cbd-300-30-s',1,0,10,NULL,7280.00,NULL,1,'2020-05-15 01:50:03','2020-08-21 04:01:11','JPY',NULL,'productclass',NULL,0),
	(48,10,1,3,NULL,2,1,'cbd-1500',1,0,10,32500.00,32500.00,NULL,1,'2020-05-15 01:51:51','2020-08-21 04:02:33','JPY',NULL,'productclass',NULL,0),
	(49,10,2,4,NULL,2,1,'cbd-1500-s',1,0,10,NULL,26000.00,NULL,1,'2020-05-15 01:51:51','2020-08-21 04:02:33','JPY',NULL,'productclass',NULL,0),
	(50,12,1,3,NULL,2,1,'cbd-600',1,0,10,14950.00,14950.00,NULL,1,'2020-05-15 01:52:49','2020-08-21 04:00:54','JPY',NULL,'productclass',NULL,0),
	(51,12,2,4,NULL,2,1,'cbd-600-s',1,0,10,NULL,11960.00,NULL,1,'2020-05-15 01:52:49','2020-08-21 04:00:54','JPY',NULL,'productclass',NULL,0),
	(52,11,1,3,NULL,2,3,'cbd-900',0,0,10,20800.00,20800.00,NULL,1,'2020-05-15 01:53:59','2020-09-20 03:58:34','JPY',NULL,'productclass',NULL,0),
	(53,11,2,4,NULL,2,1,'cbd-900-s',1,0,10,NULL,16640.00,NULL,1,'2020-05-15 01:53:59','2020-08-21 04:01:57','JPY',NULL,'productclass',NULL,0),
	(54,8,1,3,NULL,2,3,'cbd-200',1,0,10,7200.00,7200.00,NULL,1,'2020-05-15 01:55:04','2020-08-28 09:24:31','JPY',NULL,'productclass',NULL,0),
	(55,8,2,4,NULL,2,1,'cbd-200-s',1,0,10,NULL,5760.00,NULL,1,'2020-05-15 01:55:04','2020-08-21 04:01:25','JPY',NULL,'productclass',NULL,0),
	(56,7,1,3,NULL,2,3,'cbd-300-10',1,0,10,9600.00,9600.00,NULL,1,'2020-05-15 01:56:25','2020-09-20 03:58:34','JPY',NULL,'productclass',NULL,0),
	(57,7,2,4,NULL,2,1,'cbd-300-10-s',1,0,10,NULL,7680.00,NULL,1,'2020-05-15 01:56:25','2020-09-06 23:55:49','JPY',NULL,'productclass',NULL,0),
	(58,15,1,NULL,NULL,NULL,1,NULL,NULL,1,NULL,NULL,6960.00,NULL,0,'2020-06-16 23:57:38','2020-06-16 23:59:14','JPY',NULL,'productclass',NULL,NULL),
	(59,15,1,3,NULL,2,1,'cbd-water-500-set',2,0,10,6960.00,6960.00,NULL,1,'2020-06-16 23:59:14','2020-09-10 10:56:23','JPY',NULL,'productclass',NULL,NULL),
	(60,15,2,4,NULL,2,1,'cbd-water-500-set-s',2,0,10,NULL,5568.00,NULL,1,'2020-06-16 23:59:14','2020-09-10 10:56:23','JPY',NULL,'productclass',NULL,NULL);

/*!40000 ALTER TABLE `dtb_product_class` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dtb_product_image
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dtb_product_image`;

CREATE TABLE `dtb_product_image` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned DEFAULT NULL,
  `creator_id` int(10) unsigned DEFAULT NULL,
  `file_name` varchar(255) NOT NULL,
  `sort_no` smallint(5) unsigned NOT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_3267CC7A4584665A` (`product_id`),
  KEY `IDX_3267CC7A61220EA6` (`creator_id`),
  CONSTRAINT `FK_3267CC7A4584665A` FOREIGN KEY (`product_id`) REFERENCES `dtb_product` (`id`),
  CONSTRAINT `FK_3267CC7A61220EA6` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dtb_product_image` WRITE;
/*!40000 ALTER TABLE `dtb_product_image` DISABLE KEYS */;

INSERT INTO `dtb_product_image` (`id`, `product_id`, `creator_id`, `file_name`, `sort_no`, `create_date`, `discriminator_type`)
VALUES
	(31,2,1,'0429091111_5ea8c61f1a69b.jpg',2,'2020-04-29 00:11:53','productimage'),
	(83,2,1,'0515084828_5ebdd8cc00c6c.png',1,'2020-05-14 23:48:35','productimage'),
	(490,7,3,'0611180459_5ee1f3bb85579.png',1,'2020-06-19 03:55:57','productimage'),
	(491,7,3,'0611180454_5ee1f3b64454c.png',2,'2020-06-19 03:55:57','productimage'),
	(492,7,3,'0611180501_5ee1f3bd20f76.png',3,'2020-06-19 03:55:57','productimage'),
	(493,7,3,'0611180428_5ee1f39c0c05d.png',4,'2020-06-19 03:55:57','productimage'),
	(504,11,1,'0611180734_5ee1f456d2c22.png',1,'2020-06-19 03:55:57','productimage'),
	(505,11,1,'0611180733_5ee1f4557f227.png',2,'2020-06-19 03:55:57','productimage'),
	(506,11,1,'0611180736_5ee1f4581ec4f.png',3,'2020-06-19 03:55:57','productimage'),
	(507,11,1,'0611180737_5ee1f459ede13.png',4,'2020-06-19 03:55:57','productimage'),
	(508,11,1,'0611180739_5ee1f45b8e4d3.png',5,'2020-06-19 03:55:57','productimage'),
	(517,5,1,'0611180346_5ee1f37281399.png',1,'2020-06-19 03:55:57','productimage'),
	(518,5,1,'0611180342_5ee1f36e3c868.png',2,'2020-06-19 03:55:57','productimage'),
	(519,5,1,'0611180347_5ee1f373a9912.png',3,'2020-06-19 03:55:57','productimage'),
	(520,5,1,'0611180335_5ee1f3677ba69.png',4,'2020-06-19 03:55:57','productimage'),
	(537,14,3,'0611181141_5ee1f54e001e1.png',1,'2020-06-19 03:55:58','productimage'),
	(538,14,3,'0611181140_5ee1f54c952a3.png',2,'2020-06-19 03:55:58','productimage'),
	(539,14,3,'0611181143_5ee1f54f4da6f.png',3,'2020-06-19 03:55:58','productimage'),
	(540,14,3,'0611181145_5ee1f551f385d.png',4,'2020-06-19 03:55:58','productimage'),
	(549,13,1,'0611180951_5ee1f4dfccea4.png',1,'2020-06-19 03:55:58','productimage'),
	(550,13,1,'0611180950_5ee1f4dea01b8.png',2,'2020-06-19 03:55:58','productimage'),
	(551,13,1,'0611180953_5ee1f4e1415f4.png',3,'2020-06-19 03:55:58','productimage'),
	(552,13,1,'0611180959_5ee1f4e790f1c.png',4,'2020-06-19 03:55:58','productimage'),
	(561,12,1,'0611171916_5ee1e90471919.png',1,'2020-06-19 03:55:58','productimage'),
	(562,12,1,'0611175955_5ee1f28b6b8a6.png',2,'2020-06-19 03:55:58','productimage'),
	(563,12,1,'0611171921_5ee1e9096eefe.png',3,'2020-06-19 03:55:58','productimage'),
	(564,12,1,'0611172003_5ee1e933d6541.png',4,'2020-06-19 03:55:58','productimage'),
	(573,10,1,'0611181057_5ee1f521901aa.png',1,'2020-06-19 03:55:58','productimage'),
	(574,10,1,'0611181054_5ee1f51e8c2a7.png',2,'2020-06-19 03:55:58','productimage'),
	(575,10,1,'0611181059_5ee1f523647b8.png',3,'2020-06-19 03:55:58','productimage'),
	(576,10,1,'0611181036_5ee1f50c9867e.png',4,'2020-06-19 03:55:58','productimage'),
	(585,9,1,'0611180052_5ee1f2c495385.png',1,'2020-06-19 03:55:58','productimage'),
	(586,9,1,'0611180047_5ee1f2bfa3699.png',2,'2020-06-19 03:55:58','productimage'),
	(587,9,1,'0611180053_5ee1f2c5c043f.png',3,'2020-06-19 03:55:58','productimage'),
	(588,9,1,'0611180125_5ee1f2e5e008e.png',4,'2020-06-19 03:55:58','productimage'),
	(597,6,1,'0611180238_5ee1f32e300ab.png',1,'2020-06-19 03:55:58','productimage'),
	(598,6,1,'0611180234_5ee1f32a5d8e3.png',2,'2020-06-19 03:55:58','productimage'),
	(599,6,1,'0611180240_5ee1f3301aada.png',3,'2020-06-19 03:55:58','productimage'),
	(600,6,1,'0611180225_5ee1f321ed961.png',4,'2020-06-19 03:55:58','productimage'),
	(609,8,1,'0611180639_5ee1f41f3cb19.png',1,'2020-06-19 03:55:58','productimage'),
	(610,8,1,'0611180637_5ee1f41dc3fb1.png',2,'2020-06-19 03:55:58','productimage'),
	(611,8,1,'0611180640_5ee1f420cf168.png',3,'2020-06-19 03:55:58','productimage'),
	(612,8,1,'0611180645_5ee1f425003a1.png',4,'2020-06-19 03:55:58','productimage'),
	(619,4,1,'0606154607_5edb3bafce37b.png',1,'2020-06-19 03:55:58','productimage'),
	(620,4,1,'0613120457_5ee4425941906.png',2,'2020-06-19 03:55:58','productimage'),
	(621,4,1,'0613120613_5ee442a55c319.png',3,'2020-06-19 03:55:58','productimage'),
	(628,15,1,'0611161225_5ee1d9591a4c7.png',1,'2020-06-19 03:55:58','productimage'),
	(629,15,1,'0610183312_5ee0a8d8ca2bf.png',2,'2020-06-19 03:55:58','productimage'),
	(630,15,1,'0610183312_5ee0a8d8cd318.png',3,'2020-06-19 03:55:58','productimage');

/*!40000 ALTER TABLE `dtb_product_image` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dtb_product_stock
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dtb_product_stock`;

CREATE TABLE `dtb_product_stock` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_class_id` int(10) unsigned DEFAULT NULL,
  `creator_id` int(10) unsigned DEFAULT NULL,
  `stock` decimal(10,0) DEFAULT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_BC6C9E4521B06187` (`product_class_id`),
  KEY `IDX_BC6C9E4561220EA6` (`creator_id`),
  CONSTRAINT `FK_BC6C9E4521B06187` FOREIGN KEY (`product_class_id`) REFERENCES `dtb_product_class` (`id`),
  CONSTRAINT `FK_BC6C9E4561220EA6` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dtb_product_stock` WRITE;
/*!40000 ALTER TABLE `dtb_product_stock` DISABLE KEYS */;

INSERT INTO `dtb_product_stock` (`id`, `product_class_id`, `creator_id`, `stock`, `create_date`, `update_date`, `discriminator_type`)
VALUES
	(11,11,1,100,'2017-03-07 10:14:52','2020-06-17 05:19:05','productstock'),
	(23,23,1,100,'2020-04-29 00:06:19','2020-05-15 01:39:40','productstock'),
	(24,24,1,100,'2020-05-10 03:52:28','2020-05-15 01:42:53','productstock'),
	(25,25,1,100,'2020-05-10 04:01:39','2020-05-15 01:46:30','productstock'),
	(26,26,1,100,'2020-05-10 04:13:02','2020-05-15 01:55:37','productstock'),
	(27,27,1,100,'2020-05-10 04:14:21','2020-05-15 01:54:36','productstock'),
	(28,28,1,100,'2020-05-10 04:21:25','2020-05-15 01:47:25','productstock'),
	(29,29,1,100,'2020-05-10 04:23:31','2020-05-15 01:51:12','productstock'),
	(30,30,1,100,'2020-05-10 04:24:53','2020-05-15 01:53:16','productstock'),
	(31,31,1,100,'2020-05-10 04:28:10','2020-05-15 01:52:16','productstock'),
	(32,32,1,100,'2020-05-10 04:31:16','2020-05-15 01:48:51','productstock'),
	(33,33,1,100,'2020-05-10 04:36:03','2020-05-14 22:29:13','productstock'),
	(34,34,1,5,'2020-05-10 04:42:02','2020-05-10 04:42:11','productstock'),
	(35,35,1,5,'2020-05-10 04:42:02','2020-05-10 04:42:11','productstock'),
	(36,36,3,2,'2020-05-14 22:26:10','2020-09-07 05:24:19','productstock'),
	(37,37,3,1,'2020-05-14 22:26:10','2020-09-06 17:25:05','productstock'),
	(38,36,1,5,'2020-05-14 22:30:04','2020-05-14 22:30:04','productstock'),
	(39,37,1,NULL,'2020-05-14 22:30:04','2020-05-14 22:30:04','productstock'),
	(40,38,1,4,'2020-05-15 01:41:10','2020-09-15 02:41:26','productstock'),
	(41,39,1,4,'2020-05-15 01:41:10','2020-08-21 04:00:32','productstock'),
	(42,40,1,1,'2020-05-15 01:46:01','2020-08-21 04:02:16','productstock'),
	(43,41,1,1,'2020-05-15 01:46:01','2020-08-21 04:02:16','productstock'),
	(44,42,1,1,'2020-05-15 01:47:02','2020-08-21 04:04:52','productstock'),
	(45,43,1,1,'2020-05-15 01:47:02','2020-08-21 04:04:52','productstock'),
	(46,44,1,2,'2020-05-15 01:47:58','2020-09-15 02:41:26','productstock'),
	(47,45,1,2,'2020-05-15 01:47:58','2020-08-21 04:04:29','productstock'),
	(48,46,1,1,'2020-05-15 01:50:03','2020-08-21 04:01:11','productstock'),
	(49,47,1,1,'2020-05-15 01:50:03','2020-08-21 04:01:11','productstock'),
	(50,48,1,1,'2020-05-15 01:51:51','2020-08-21 04:02:33','productstock'),
	(51,49,1,1,'2020-05-15 01:51:51','2020-08-21 04:02:33','productstock'),
	(52,50,1,1,'2020-05-15 01:52:49','2020-08-21 04:00:54','productstock'),
	(53,51,1,1,'2020-05-15 01:52:49','2020-08-21 04:00:54','productstock'),
	(54,52,3,0,'2020-05-15 01:53:59','2020-09-20 03:58:34','productstock'),
	(55,53,1,1,'2020-05-15 01:53:59','2020-08-21 04:01:57','productstock'),
	(56,54,3,1,'2020-05-15 01:55:04','2020-08-28 09:24:31','productstock'),
	(57,55,1,1,'2020-05-15 01:55:04','2020-08-21 04:01:25','productstock'),
	(58,56,3,1,'2020-05-15 01:56:25','2020-09-20 03:58:34','productstock'),
	(59,57,1,1,'2020-05-15 01:56:25','2020-09-06 23:55:49','productstock'),
	(60,58,1,NULL,'2020-06-16 23:57:38','2020-06-16 23:57:38','productstock'),
	(61,59,1,2,'2020-06-16 23:59:14','2020-09-10 10:56:23','productstock'),
	(62,60,1,2,'2020-06-16 23:59:14','2020-09-10 10:56:23','productstock');

/*!40000 ALTER TABLE `dtb_product_stock` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dtb_product_tag
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dtb_product_tag`;

CREATE TABLE `dtb_product_tag` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned DEFAULT NULL,
  `tag_id` int(10) unsigned DEFAULT NULL,
  `creator_id` int(10) unsigned DEFAULT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_4433E7214584665A` (`product_id`),
  KEY `IDX_4433E721BAD26311` (`tag_id`),
  KEY `IDX_4433E72161220EA6` (`creator_id`),
  CONSTRAINT `FK_4433E7214584665A` FOREIGN KEY (`product_id`) REFERENCES `dtb_product` (`id`),
  CONSTRAINT `FK_4433E72161220EA6` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`id`),
  CONSTRAINT `FK_4433E721BAD26311` FOREIGN KEY (`tag_id`) REFERENCES `dtb_tag` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dtb_product_tag` WRITE;
/*!40000 ALTER TABLE `dtb_product_tag` DISABLE KEYS */;

INSERT INTO `dtb_product_tag` (`id`, `product_id`, `tag_id`, `creator_id`, `create_date`, `discriminator_type`)
VALUES
	(326,2,2,1,'2020-06-17 05:19:05','producttag'),
	(403,4,6,1,'2020-08-06 12:27:29','producttag'),
	(404,4,5,1,'2020-08-06 12:27:29','producttag'),
	(405,4,4,1,'2020-08-06 12:27:29','producttag'),
	(406,4,3,1,'2020-08-06 12:27:29','producttag'),
	(407,12,2,1,'2020-08-06 12:28:48','producttag'),
	(408,13,2,1,'2020-08-06 12:29:34','producttag'),
	(409,8,2,1,'2020-08-06 12:30:29','producttag'),
	(410,9,2,1,'2020-08-06 12:32:41','producttag'),
	(411,6,2,1,'2020-08-06 12:36:28','producttag'),
	(412,11,2,1,'2020-08-06 12:37:13','producttag'),
	(413,5,2,1,'2020-08-06 12:38:00','producttag'),
	(414,10,2,1,'2020-08-06 12:38:56','producttag'),
	(416,14,2,3,'2020-09-06 17:24:53','producttag');

/*!40000 ALTER TABLE `dtb_product_tag` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dtb_shipping
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dtb_shipping`;

CREATE TABLE `dtb_shipping` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned DEFAULT NULL,
  `country_id` smallint(5) unsigned DEFAULT NULL,
  `pref_id` smallint(5) unsigned DEFAULT NULL,
  `delivery_id` int(10) unsigned DEFAULT NULL,
  `creator_id` int(10) unsigned DEFAULT NULL,
  `name01` varchar(255) NOT NULL,
  `name02` varchar(255) NOT NULL,
  `kana01` varchar(255) DEFAULT NULL,
  `kana02` varchar(255) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `phone_number` varchar(14) DEFAULT NULL,
  `postal_code` varchar(8) DEFAULT NULL,
  `addr01` varchar(255) DEFAULT NULL,
  `addr02` varchar(255) DEFAULT NULL,
  `delivery_name` varchar(255) DEFAULT NULL,
  `time_id` int(10) unsigned DEFAULT NULL,
  `delivery_time` varchar(255) DEFAULT NULL,
  `delivery_date` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `shipping_date` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `tracking_number` varchar(255) DEFAULT NULL,
  `note` varchar(4000) DEFAULT NULL,
  `sort_no` smallint(5) unsigned DEFAULT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `mail_send_date` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_2EBD22CE8D9F6D38` (`order_id`),
  KEY `IDX_2EBD22CEF92F3E70` (`country_id`),
  KEY `IDX_2EBD22CEE171EF5F` (`pref_id`),
  KEY `IDX_2EBD22CE12136921` (`delivery_id`),
  KEY `IDX_2EBD22CE61220EA6` (`creator_id`),
  CONSTRAINT `FK_2EBD22CE12136921` FOREIGN KEY (`delivery_id`) REFERENCES `dtb_delivery` (`id`),
  CONSTRAINT `FK_2EBD22CE61220EA6` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`id`),
  CONSTRAINT `FK_2EBD22CE8D9F6D38` FOREIGN KEY (`order_id`) REFERENCES `dtb_order` (`id`),
  CONSTRAINT `FK_2EBD22CEE171EF5F` FOREIGN KEY (`pref_id`) REFERENCES `mtb_pref` (`id`),
  CONSTRAINT `FK_2EBD22CEF92F3E70` FOREIGN KEY (`country_id`) REFERENCES `mtb_country` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dtb_shipping` WRITE;
/*!40000 ALTER TABLE `dtb_shipping` DISABLE KEYS */;

INSERT INTO `dtb_shipping` (`id`, `order_id`, `country_id`, `pref_id`, `delivery_id`, `creator_id`, `name01`, `name02`, `kana01`, `kana02`, `company_name`, `phone_number`, `postal_code`, `addr01`, `addr02`, `delivery_name`, `time_id`, `delivery_time`, `delivery_date`, `shipping_date`, `tracking_number`, `note`, `sort_no`, `create_date`, `update_date`, `mail_send_date`, `discriminator_type`)
VALUES
	(1,1,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','サンプル業者',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-05-01 08:34:15','2020-05-01 08:34:15',NULL,'shipping'),
	(2,2,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','サンプル業者',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-05-01 10:21:41','2020-05-01 10:21:41',NULL,'shipping'),
	(3,3,NULL,13,2,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','サンプル宅配 (定期購買)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-05-15 00:56:37','2020-05-15 01:00:32',NULL,'shipping'),
	(11,11,NULL,13,2,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','サンプル宅配 (定期購買)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-05-15 05:56:00','2020-05-15 05:56:00',NULL,'shipping'),
	(12,12,NULL,13,2,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','サンプル宅配 (定期購買)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-05-15 05:58:24','2020-05-15 05:58:24',NULL,'shipping'),
	(13,13,NULL,13,2,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','サンプル宅配 (定期購買)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-05-15 06:17:31','2020-05-15 06:17:31',NULL,'shipping'),
	(14,14,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','サンプル宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-05-15 06:21:21','2020-05-15 06:21:21',NULL,'shipping'),
	(15,15,NULL,13,2,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','サンプル宅配 (定期購買)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-05-16 10:34:59','2020-05-16 10:34:59',NULL,'shipping'),
	(16,16,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','サンプル宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-05-16 10:38:03','2020-05-16 10:38:03',NULL,'shipping'),
	(17,17,NULL,13,2,NULL,'Gues','User','グエスト','ユーザ',NULL,'0123456789','1570077','世田谷区鎌田','Test, 99','サンプル宅配 (定期購買)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-05-17 02:08:21','2020-05-17 02:08:21',NULL,'shipping'),
	(18,18,NULL,13,1,NULL,'Gues','User','グエスト','ユーザ',NULL,'0123456789','1570077','世田谷区鎌田','Test, 99','サンプル宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-05-17 02:09:43','2020-05-17 02:09:43',NULL,'shipping'),
	(19,19,NULL,13,2,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','サンプル宅配 (定期購買)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-05-22 03:44:49','2020-05-22 03:44:49',NULL,'shipping'),
	(20,20,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','サンプル宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-05-23 07:21:25','2020-05-23 07:21:25',NULL,'shipping'),
	(21,21,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','サンプル宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-05-24 08:47:10','2020-05-24 08:47:10',NULL,'shipping'),
	(22,22,NULL,13,2,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','サンプル宅配 (定期購買)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-05-26 12:05:15','2020-05-26 12:05:15',NULL,'shipping'),
	(23,23,NULL,13,2,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','サンプル宅配 (定期購買)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-05-27 11:46:11','2020-05-27 11:46:11',NULL,'shipping'),
	(24,24,NULL,13,2,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','サンプル宅配 (定期購買)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-03 07:17:59','2020-06-03 07:17:59',NULL,'shipping'),
	(25,25,NULL,13,2,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','スタンダード宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-03 07:24:32','2020-06-03 07:24:32',NULL,'shipping'),
	(26,26,NULL,13,1,NULL,'Test2','Test','カナ','カナ',NULL,'11122223333','1000001','千代田区千代田','1-1-1','スタンダード宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-03 10:21:34','2020-06-03 10:21:34',NULL,'shipping'),
	(27,27,NULL,13,1,NULL,'Test2','Test','カナ','カナ',NULL,'11122223333','1000001','千代田区千代田','1-1-1','スタンダード宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-03 14:27:51','2020-06-03 14:27:51',NULL,'shipping'),
	(28,28,NULL,13,1,NULL,'Test2','Test','カナ','カナ',NULL,'11122223333','1000001','千代田区千代田','1-1-1','スタンダード宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-03 14:52:55','2020-06-03 14:52:55',NULL,'shipping'),
	(29,29,NULL,13,1,NULL,'Test2','Test','カナ','カナ',NULL,'11122223333','1000001','千代田区千代田','1-1-1','スタンダード宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-03 14:53:38','2020-06-03 14:53:38',NULL,'shipping'),
	(30,30,NULL,13,1,NULL,'Test2','Test','カナ','カナ',NULL,'11122223333','1000001','千代田区千代田','1-1-1','スタンダード宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-03 14:55:48','2020-06-03 14:55:48',NULL,'shipping'),
	(31,31,NULL,13,1,NULL,'Test2','Test','カナ','カナ',NULL,'11122223333','1000001','千代田区千代田','1-1-1','スタンダード宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-03 15:04:08','2020-06-03 15:04:08',NULL,'shipping'),
	(32,32,NULL,13,1,NULL,'Test2','Test','カナ','カナ',NULL,'11122223333','1000001','千代田区千代田','1-1-1','スタンダード宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-03 15:05:19','2020-06-03 15:05:19',NULL,'shipping'),
	(33,33,NULL,13,1,NULL,'Test2','Test','カナ','カナ',NULL,'11122223333','1000001','千代田区千代田','1-1-1','スタンダード宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-03 15:06:18','2020-06-03 15:06:18',NULL,'shipping'),
	(34,34,NULL,13,1,NULL,'Test2','Test','カナ','カナ',NULL,'11122223333','1000001','千代田区千代田','1-1-1','スタンダード宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-03 15:07:46','2020-06-03 15:07:46',NULL,'shipping'),
	(35,35,NULL,13,2,NULL,'Test2','Test','カナ','カナ',NULL,'11122223333','1000001','千代田区千代田','1-1-1','スタンダード宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-03 15:08:05','2020-06-03 15:08:05',NULL,'shipping'),
	(36,36,NULL,13,1,NULL,'Test2','Test','カナ','カナ',NULL,'11122223333','1000001','千代田区千代田','1-1-1','スタンダード宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-03 15:55:19','2020-06-03 15:55:19',NULL,'shipping'),
	(37,37,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','スタンダード宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-04 06:42:16','2020-06-04 06:42:16',NULL,'shipping'),
	(38,38,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','スタンダード宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-04 06:42:56','2020-06-04 06:42:56',NULL,'shipping'),
	(39,39,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','スタンダード宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-04 06:43:15','2020-06-04 06:43:15',NULL,'shipping'),
	(40,40,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','スタンダード宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-05 08:12:29','2020-06-05 08:12:29',NULL,'shipping'),
	(41,41,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','スタンダード宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-05 09:06:16','2020-06-05 09:06:16',NULL,'shipping'),
	(42,42,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','スタンダード宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-05 09:06:42','2020-06-05 09:06:42',NULL,'shipping'),
	(43,43,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','スタンダード宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-05 09:10:56','2020-06-05 09:10:56',NULL,'shipping'),
	(44,44,NULL,13,2,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','スタンダード宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-05 09:19:05','2020-06-05 09:19:05',NULL,'shipping'),
	(45,45,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','スタンダード宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-05 09:25:32','2020-06-05 09:25:32',NULL,'shipping'),
	(46,46,NULL,13,1,NULL,'杉森','浩一郎','スギモリ','コウイチロウ',NULL,'0337128123','1520004','目黒区鷹番','1-8-17','スタンダード宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-17 08:23:58','2020-06-17 08:23:58',NULL,'shipping'),
	(47,47,NULL,13,1,NULL,'杉森','浩一郎','スギモリ','コウイチロウ',NULL,'0337128123','1520004','目黒区鷹番','1-8-17','スタンダード宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-17 08:30:59','2020-06-17 08:30:59',NULL,'shipping'),
	(48,48,NULL,13,1,1,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','佐川急便',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-17 11:12:30','2020-09-15 02:41:26',NULL,'shipping'),
	(49,49,NULL,13,2,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','スタンダード宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-17 11:45:49','2020-06-17 11:45:49',NULL,'shipping'),
	(51,51,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','スタンダード宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-19 07:16:19','2020-06-19 07:16:19',NULL,'shipping'),
	(52,52,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','スタンダード宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-26 01:47:50','2020-06-26 01:47:50',NULL,'shipping'),
	(53,53,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','スタンダード宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-26 01:54:54','2020-06-26 01:54:54',NULL,'shipping'),
	(54,54,NULL,13,1,1,'小野','冨美枝','オノ','フミエ',NULL,'0337128235','1520004','目黒区鷹番','1-8-17','スタンダード宅配',NULL,NULL,NULL,'2020-08-03 02:06:03',NULL,NULL,NULL,'2020-06-26 09:12:42','2020-08-03 02:06:03',NULL,'shipping'),
	(55,55,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','スタンダード宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-30 10:59:21','2020-06-30 10:59:21',NULL,'shipping'),
	(56,56,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','スタンダード宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-07-01 07:28:50','2020-07-01 07:28:50',NULL,'shipping'),
	(57,57,NULL,13,1,NULL,'杉森','浩一郎','スギモリ','コウイチロウ',NULL,'0337128235','1520004','目黒区鷹番','1-8-17','スタンダード宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-07-01 19:04:09','2020-07-01 19:04:09',NULL,'shipping'),
	(58,58,NULL,28,1,NULL,'永井','光弘','ナガイ','ミツヒロ',NULL,'09083772905','6780005','相生市大石町','1-27','スタンダード宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-07-02 03:45:32','2020-07-02 03:45:32',NULL,'shipping'),
	(59,59,NULL,13,1,NULL,'杉森','浩一郎','スギモリ','コウイチロウ',NULL,'0337128235','1520004','目黒区鷹番','1-8-17','スタンダード宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-07-03 08:38:44','2020-07-03 08:38:44',NULL,'shipping'),
	(60,60,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','スタンダード宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-07-10 03:36:53','2020-07-10 03:36:53',NULL,'shipping'),
	(61,61,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','スタンダード宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-07-16 09:16:24','2020-07-16 09:16:24',NULL,'shipping'),
	(62,62,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','スタンダード宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-07-17 02:37:36','2020-07-17 02:37:36',NULL,'shipping'),
	(63,63,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','スタンダード宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-07-17 02:54:57','2020-07-17 02:54:57',NULL,'shipping'),
	(64,64,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','スタンダード宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-07-17 08:38:26','2020-07-17 08:38:26',NULL,'shipping'),
	(65,65,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','スタンダード宅配',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-07-22 04:13:56','2020-07-22 04:13:56',NULL,'shipping'),
	(66,66,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','佐川急便',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-07-22 04:17:31','2020-07-24 09:04:19',NULL,'shipping'),
	(67,67,NULL,13,1,1,'杉森','勝代','スギモリ','カツヨ',NULL,'0337128235','1520004','目黒区鷹番','1-8-17','佐川急便',NULL,NULL,NULL,'2020-07-24 03:22:42','403921650193',NULL,NULL,'2020-07-23 16:10:05','2020-08-03 02:07:20','2020-07-24 03:22:42','shipping'),
	(68,68,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','佐川急便',10,'19時～21時','2020-08-23 15:00:00',NULL,NULL,NULL,NULL,'2020-08-03 13:22:56','2020-08-03 13:23:40',NULL,'shipping'),
	(69,69,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','佐川急便',10,'19時～21時','2020-08-24 15:00:00',NULL,NULL,NULL,NULL,'2020-08-04 03:27:01','2020-08-04 03:29:31',NULL,'shipping'),
	(70,70,NULL,13,1,NULL,'杉森','浩一郎','スギモリ','コウイチロウ',NULL,'0337128235','1520004','目黒区鷹番','1-8-17','佐川急便',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-08-05 09:49:25','2020-08-05 09:49:25',NULL,'shipping'),
	(71,71,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','佐川急便',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-08-05 10:08:13','2020-08-05 10:08:13',NULL,'shipping'),
	(72,72,NULL,13,1,NULL,'杉森','浩一郎','スギモリ','コウイチロウ',NULL,'0337128235','1520004','目黒区鷹番','1-8-17','佐川急便',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-08-05 10:23:05','2020-08-05 10:23:05',NULL,'shipping'),
	(73,73,NULL,13,1,NULL,'杉森','浩一郎','スギモリ','コウイチロウ',NULL,'0337128235','1520004','目黒区鷹番','1-8-17','佐川急便',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-08-05 10:51:25','2020-08-05 10:51:25',NULL,'shipping'),
	(74,74,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','佐川急便',10,'19時～21時','2020-08-26 15:00:00',NULL,NULL,NULL,NULL,'2020-08-06 13:29:51','2020-08-06 13:50:57',NULL,'shipping'),
	(75,75,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','佐川急便',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-08-06 14:02:24','2020-08-06 14:02:24',NULL,'shipping'),
	(76,76,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','佐川急便',6,'12時～14時','2020-08-22 15:00:00',NULL,NULL,NULL,NULL,'2020-08-06 14:55:54','2020-08-06 15:13:59',NULL,'shipping'),
	(77,77,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','佐川急便',9,'18時～21時','2020-08-19 15:00:00',NULL,NULL,NULL,NULL,'2020-08-07 02:28:56','2020-08-07 02:29:17',NULL,'shipping'),
	(78,78,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','佐川急便',8,'16時～18時','2020-08-30 15:00:00',NULL,NULL,NULL,NULL,'2020-08-10 03:46:20','2020-08-12 09:37:51',NULL,'shipping'),
	(79,79,NULL,13,1,NULL,'Test','Test','テスト','テスト',NULL,'11122223333','1000001','千代田区千代田','1-1-1','佐川急便',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-08-10 10:00:13','2020-08-10 10:00:13',NULL,'shipping'),
	(80,80,NULL,13,1,NULL,'Test2','Test','カナ','カナ',NULL,'11122223333','1000001','千代田区千代田','1-1-1','佐川急便',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-08-10 10:07:12','2020-08-10 10:07:12',NULL,'shipping'),
	(81,81,NULL,13,1,NULL,'Test2','Test','カナ','カナ',NULL,'11122223333','1000001','千代田区千代田','1-1-1','佐川急便',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-08-10 10:07:33','2020-08-10 10:07:33',NULL,'shipping'),
	(82,82,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','佐川急便',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-08-12 09:44:19','2020-08-12 09:44:19',NULL,'shipping'),
	(83,83,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','佐川急便',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-08-12 09:44:56','2020-08-12 09:44:56',NULL,'shipping'),
	(84,84,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','佐川急便',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-08-13 11:22:56','2020-08-13 11:22:56',NULL,'shipping'),
	(85,85,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','佐川急便',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-08-13 12:05:05','2020-08-13 12:05:05',NULL,'shipping'),
	(86,86,NULL,26,1,NULL,'藤永','朋久','フジナガ','トモヒサ','(株)リード企画','09050517243','6170837','長岡京市久貝2丁目','16-31','佐川急便',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-08-14 09:04:48','2020-08-14 09:04:48',NULL,'shipping'),
	(87,87,NULL,13,1,NULL,'上野','和子','ウエノ','カズコ',NULL,'08011983685','1420063','品川区荏原','2-7-5','佐川急便',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-08-17 19:13:20','2020-08-17 19:13:20',NULL,'shipping'),
	(88,88,NULL,13,1,NULL,'上野','和子','ウエノ','カズコ',NULL,'08011983685','1420063','品川区荏原','2-7-5','佐川急便',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-08-17 19:14:27','2020-08-17 19:14:27',NULL,'shipping'),
	(89,89,NULL,13,1,NULL,'杉森','浩一郎','スギモリ','コウイチロウ',NULL,'0337128235','1520004','目黒区鷹番','1-8-17','佐川急便',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-08-18 17:19:44','2020-08-18 17:19:44',NULL,'shipping'),
	(90,90,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','佐川急便',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-08-21 04:09:14','2020-08-21 04:09:14',NULL,'shipping'),
	(91,91,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','佐川急便',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-08-21 06:58:49','2020-08-21 06:58:49',NULL,'shipping'),
	(92,92,NULL,1,1,NULL,'関','歌織','セキ','カオリ',NULL,'09062618809','0630001','札幌市西区山の手一条8丁目','2-9','佐川急便',9,'18時～21時',NULL,NULL,NULL,NULL,NULL,'2020-08-25 13:45:08','2020-08-25 13:45:29',NULL,'shipping'),
	(93,93,NULL,1,1,NULL,'関','歌織','セキ','カオリ',NULL,'09062618809','0630001','札幌市西区山の手一条8丁目','2-9','佐川急便',9,'18時～21時',NULL,NULL,NULL,NULL,NULL,'2020-08-25 13:49:11','2020-08-25 13:51:34',NULL,'shipping'),
	(94,94,NULL,1,1,NULL,'関','歌織','セキ','カオリ',NULL,'09062618809','0630001','札幌市西区山の手一条8丁目','2-9','佐川急便',9,'18時～21時',NULL,NULL,NULL,NULL,NULL,'2020-08-25 13:54:14','2020-08-25 13:58:40',NULL,'shipping'),
	(95,95,NULL,1,1,NULL,'関','歌織','セキ','カオリ',NULL,'09062618809','0630001','札幌市西区山の手一条8丁目','2-9','佐川急便',9,'18時～21時',NULL,NULL,NULL,NULL,NULL,'2020-08-25 14:38:30','2020-08-25 14:38:46',NULL,'shipping'),
	(96,96,NULL,1,1,NULL,'関','歌織','セキ','カオリ',NULL,'09062618809','0630001','札幌市西区山の手一条8丁目','2-9','佐川急便',9,'18時～21時',NULL,NULL,NULL,NULL,NULL,'2020-08-25 14:43:42','2020-08-25 14:44:05',NULL,'shipping'),
	(97,97,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','佐川急便',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-08-28 03:04:19','2020-08-28 03:04:19',NULL,'shipping'),
	(98,98,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','佐川急便',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-08-28 03:35:57','2020-08-28 03:35:57',NULL,'shipping'),
	(99,99,NULL,13,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'0123456789','1570077','世田谷区鎌田','123建物','佐川急便',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-08-28 05:41:47','2020-08-28 05:41:47',NULL,'shipping'),
	(100,100,NULL,14,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'07026558129','2220032','横浜市港北区大豆戸町171-1','ネイバーズ菊名311','佐川急便',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-08-28 05:56:17','2020-08-28 05:56:17',NULL,'shipping'),
	(101,101,NULL,14,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'07026558129','2220032','横浜市港北区大豆戸町171-1','ネイバーズ菊名311','佐川急便',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-08-28 06:01:02','2020-08-28 06:01:02',NULL,'shipping'),
	(102,102,NULL,14,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'07026558129','2220032','横浜市港北区大豆戸町171-1','ネイバーズ菊名311','佐川急便',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-08-28 06:03:54','2020-08-28 06:03:54',NULL,'shipping'),
	(103,103,NULL,14,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'07026558129','2220032','横浜市港北区大豆戸町171-1','ネイバーズ菊名311','佐川急便',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-08-30 07:38:55','2020-08-30 07:38:55',NULL,'shipping'),
	(104,104,NULL,14,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'07026558129','2220032','横浜市港北区大豆戸町171-1','ネイバーズ菊名311','佐川急便',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-08-30 07:39:44','2020-08-30 07:39:44',NULL,'shipping'),
	(105,105,NULL,14,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'07026558129','2220032','横浜市港北区大豆戸町171-1','ネイバーズ菊名311','佐川急便',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-08-30 07:43:27','2020-08-30 07:43:27',NULL,'shipping'),
	(106,106,NULL,13,1,NULL,'Test','Test','テスト','テスト',NULL,'11122223333','1000001','千代田区千代田','1-1-1','佐川急便',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-09-01 11:11:13','2020-09-01 11:11:13',NULL,'shipping'),
	(107,107,NULL,14,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'07026558129','2220032','横浜市港北区大豆戸町171-1','ネイバーズ菊名311','佐川急便',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-09-02 07:03:29','2020-09-02 07:03:29',NULL,'shipping'),
	(108,108,NULL,14,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'07026558129','2220032','横浜市港北区大豆戸町171-1','ネイバーズ菊名311','佐川急便',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-09-04 05:00:38','2020-09-04 05:00:38',NULL,'shipping'),
	(109,109,NULL,14,1,NULL,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク',NULL,'07026558129','2220032','横浜市港北区大豆戸町171-1','ネイバーズ菊名311','佐川急便',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-09-06 11:16:06','2020-09-06 11:16:06',NULL,'shipping'),
	(110,110,NULL,14,1,3,'鈴木','晴美','スズキ','ハルミ',NULL,'08067574821','2430431','海老名市上今泉','6-53-15','佐川急便',NULL,NULL,NULL,'2020-09-07 05:25:02','403921650775',NULL,NULL,'2020-09-06 12:33:06','2020-09-07 05:25:02','2020-09-07 05:24:45','shipping'),
	(111,111,NULL,1,1,NULL,'関','歌織','セキ','カオリ',NULL,'09062618809','0630001','札幌市西区山の手一条8丁目','2-6','佐川急便',9,'18時～21時',NULL,NULL,NULL,NULL,NULL,'2020-09-06 14:50:30','2020-09-06 14:50:54',NULL,'shipping'),
	(112,112,NULL,28,1,NULL,'GIM','HAESOO','キム','ヘソ',NULL,'09098766383','6580073','神戸市東灘区西岡本2-7-4','オーキッドコート湖南館623','佐川急便',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-09-07 16:14:50','2020-09-07 16:14:50',NULL,'shipping'),
	(113,113,NULL,13,1,NULL,'山田','秀人','ヤマダ','ヒデト',NULL,'09083193987','1530062','目黒区三田','1-3-21-210','佐川急便',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-09-09 15:24:09','2020-09-09 15:24:09',NULL,'shipping'),
	(114,114,NULL,26,1,3,'山田','秀人','ヤマダ','ヒデト',NULL,'09083193987','6060801','京都市左京区下鴨宮河町','55-4','佐川急便',5,'午前中（8時～12時）','2020-09-10 15:00:00','2020-09-10 09:55:20','403921650834',NULL,NULL,'2020-09-09 15:28:39','2020-09-10 09:55:20','2020-09-10 09:55:12','shipping'),
	(115,115,NULL,34,1,NULL,'赤池','晋弥','アカイケ','シンヤ',NULL,'08083852625','7340064','広島市南区小磯町','2-60','佐川急便',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-09-14 15:24:59','2020-09-14 15:24:59',NULL,'shipping'),
	(116,116,NULL,28,1,NULL,'GIM','HAESOO','キム','ヘソ',NULL,'09098766383','6580073','神戸市東灘区西岡本2-7-4','オーキッドコート湖南館623','佐川急便',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-09-17 16:51:15','2020-09-17 16:51:15',NULL,'shipping'),
	(117,117,NULL,31,1,NULL,'大谷','昭子','オオタニ','アキコ',NULL,'09022958296','6892544','東伯郡琴浦町箆津','336-3','佐川急便',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-09-19 12:43:50','2020-09-19 12:43:50',NULL,'shipping'),
	(118,118,NULL,31,1,NULL,'大谷','昭子','オオタニ','アキコ',NULL,'09023958296','6892544','東伯郡琴浦町箆津','336-3','佐川急便',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-09-19 13:03:48','2020-09-19 13:03:48',NULL,'shipping'),
	(119,119,NULL,31,1,3,'大谷','昭子','オオタニ','アキコ',NULL,'09022958296','6892544','東伯郡琴浦町箆津','336-3','佐川急便',NULL,NULL,NULL,'2020-09-20 03:58:34','403921652120',NULL,NULL,'2020-09-19 13:07:02','2020-09-20 03:58:34','2020-09-20 03:58:16','shipping');

/*!40000 ALTER TABLE `dtb_shipping` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dtb_tag
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dtb_tag`;

CREATE TABLE `dtb_tag` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) unsigned NOT NULL,
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dtb_tag` WRITE;
/*!40000 ALTER TABLE `dtb_tag` DISABLE KEYS */;

INSERT INTO `dtb_tag` (`id`, `name`, `sort_no`, `discriminator_type`)
VALUES
	(1,'新商品',1,'tag'),
	(2,'おすすめ商品',2,'tag'),
	(3,'限定品',3,'tag'),
	(4,'甘味料ゼロ',4,'tag'),
	(5,'香料ゼロ',5,'tag'),
	(6,'カロリーゼロ',6,'tag'),
	(7,'10ml',7,'tag'),
	(8,'30ml',8,'tag'),
	(9,'1%',9,'tag'),
	(10,'2%',10,'tag'),
	(11,'3%',11,'tag'),
	(12,'5%',12,'tag'),
	(13,'10%',13,'tag');

/*!40000 ALTER TABLE `dtb_tag` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dtb_tax_rule
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dtb_tax_rule`;

CREATE TABLE `dtb_tax_rule` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_class_id` int(10) unsigned DEFAULT NULL,
  `creator_id` int(10) unsigned DEFAULT NULL,
  `country_id` smallint(5) unsigned DEFAULT NULL,
  `pref_id` smallint(5) unsigned DEFAULT NULL,
  `product_id` int(10) unsigned DEFAULT NULL,
  `rounding_type_id` smallint(5) unsigned DEFAULT NULL,
  `tax_rate` decimal(10,0) unsigned NOT NULL DEFAULT 8,
  `tax_adjust` decimal(10,0) unsigned NOT NULL DEFAULT 0,
  `apply_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_59F696DE21B06187` (`product_class_id`),
  KEY `IDX_59F696DE61220EA6` (`creator_id`),
  KEY `IDX_59F696DEF92F3E70` (`country_id`),
  KEY `IDX_59F696DEE171EF5F` (`pref_id`),
  KEY `IDX_59F696DE4584665A` (`product_id`),
  KEY `IDX_59F696DE1BD5C574` (`rounding_type_id`),
  CONSTRAINT `FK_59F696DE1BD5C574` FOREIGN KEY (`rounding_type_id`) REFERENCES `mtb_rounding_type` (`id`),
  CONSTRAINT `FK_59F696DE21B06187` FOREIGN KEY (`product_class_id`) REFERENCES `dtb_product_class` (`id`),
  CONSTRAINT `FK_59F696DE4584665A` FOREIGN KEY (`product_id`) REFERENCES `dtb_product` (`id`),
  CONSTRAINT `FK_59F696DE61220EA6` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`id`),
  CONSTRAINT `FK_59F696DEE171EF5F` FOREIGN KEY (`pref_id`) REFERENCES `mtb_pref` (`id`),
  CONSTRAINT `FK_59F696DEF92F3E70` FOREIGN KEY (`country_id`) REFERENCES `mtb_country` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dtb_tax_rule` WRITE;
/*!40000 ALTER TABLE `dtb_tax_rule` DISABLE KEYS */;

INSERT INTO `dtb_tax_rule` (`id`, `product_class_id`, `creator_id`, `country_id`, `pref_id`, `product_id`, `rounding_type_id`, `tax_rate`, `tax_adjust`, `apply_date`, `create_date`, `update_date`, `discriminator_type`)
VALUES
	(1,NULL,1,NULL,NULL,NULL,1,8,0,'2017-03-07 10:14:52','2017-03-07 10:14:52','2020-05-29 08:51:15','taxrule');

/*!40000 ALTER TABLE `dtb_tax_rule` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dtb_template
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dtb_template`;

CREATE TABLE `dtb_template` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `device_type_id` smallint(5) unsigned DEFAULT NULL,
  `template_code` varchar(255) NOT NULL,
  `template_name` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_94C12A694FFA550E` (`device_type_id`),
  CONSTRAINT `FK_94C12A694FFA550E` FOREIGN KEY (`device_type_id`) REFERENCES `mtb_device_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dtb_template` WRITE;
/*!40000 ALTER TABLE `dtb_template` DISABLE KEYS */;

INSERT INTO `dtb_template` (`id`, `device_type_id`, `template_code`, `template_name`, `create_date`, `update_date`, `discriminator_type`)
VALUES
	(1,10,'default','デフォルト','2017-03-07 10:14:52','2017-03-07 10:14:52','template'),
	(2,10,'maritime','Maritime Template','2020-04-26 04:12:10','2020-04-26 04:12:10','template');

/*!40000 ALTER TABLE `dtb_template` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migration_ContactManagement4
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migration_ContactManagement4`;

CREATE TABLE `migration_ContactManagement4` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `migration_ContactManagement4` WRITE;
/*!40000 ALTER TABLE `migration_ContactManagement4` DISABLE KEYS */;

INSERT INTO `migration_ContactManagement4` (`version`)
VALUES
	('20190703085414');

/*!40000 ALTER TABLE `migration_ContactManagement4` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migration_ProductReserve4
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migration_ProductReserve4`;

CREATE TABLE `migration_ProductReserve4` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table mtb_authority
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mtb_authority`;

CREATE TABLE `mtb_authority` (
  `id` smallint(5) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) unsigned NOT NULL,
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `mtb_authority` WRITE;
/*!40000 ALTER TABLE `mtb_authority` DISABLE KEYS */;

INSERT INTO `mtb_authority` (`id`, `name`, `sort_no`, `discriminator_type`)
VALUES
	(0,'システム管理者',0,'authority'),
	(1,'店舗オーナー',1,'authority');

/*!40000 ALTER TABLE `mtb_authority` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mtb_country
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mtb_country`;

CREATE TABLE `mtb_country` (
  `id` smallint(5) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) unsigned NOT NULL,
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `mtb_country` WRITE;
/*!40000 ALTER TABLE `mtb_country` DISABLE KEYS */;

INSERT INTO `mtb_country` (`id`, `name`, `sort_no`, `discriminator_type`)
VALUES
	(4,'アフガニスタン',4,'country'),
	(8,'アルバニア',12,'country'),
	(10,'南極',149,'country'),
	(12,'アルジェリア',9,'country'),
	(16,'アメリカ領サモア',7,'country'),
	(20,'アンドラ',17,'country'),
	(24,'アンゴラ',15,'country'),
	(28,'アンティグア・バーブーダ',16,'country'),
	(31,'アゼルバイジャン',3,'country'),
	(32,'アルゼンチン',10,'country'),
	(36,'オーストラリア',39,'country'),
	(40,'オーストリア',40,'country'),
	(44,'バハマ',167,'country'),
	(48,'バーレーン',161,'country'),
	(50,'バングラデシュ',175,'country'),
	(51,'アルメニア',13,'country'),
	(52,'バルバドス',172,'country'),
	(56,'ベルギー',201,'country'),
	(60,'バミューダ諸島|バミューダ',169,'country'),
	(64,'ブータン',181,'country'),
	(68,'ボリビア|ボリビア多民族国',206,'country'),
	(70,'ボスニア・ヘルツェゴビナ',203,'country'),
	(72,'ボツワナ',204,'country'),
	(74,'ブーベ島',182,'country'),
	(76,'ブラジル',186,'country'),
	(84,'ベリーズ',199,'country'),
	(86,'イギリス領インド洋地域',20,'country'),
	(90,'ソロモン諸島',121,'country'),
	(92,'イギリス領ヴァージン諸島',21,'country'),
	(96,'ブルネイ|ブルネイ・ダルサラーム',193,'country'),
	(100,'ブルガリア',191,'country'),
	(104,'ミャンマー',224,'country'),
	(108,'ブルンジ',194,'country'),
	(112,'ベラルーシ',198,'country'),
	(116,'カンボジア',55,'country'),
	(120,'カメルーン',53,'country'),
	(124,'カナダ',51,'country'),
	(132,'カーボベルデ',45,'country'),
	(136,'ケイマン諸島',75,'country'),
	(140,'中央アフリカ共和国',130,'country'),
	(144,'スリランカ',108,'country'),
	(148,'チャド',129,'country'),
	(152,'チリ',134,'country'),
	(156,'中華人民共和国|中国',131,'country'),
	(158,'台湾',125,'country'),
	(162,'クリスマス島 (オーストラリア)|クリスマス島',71,'country'),
	(166,'ココス諸島|ココス（キーリング）諸島',78,'country'),
	(170,'コロンビア',81,'country'),
	(174,'コモロ',80,'country'),
	(175,'マヨット',214,'country'),
	(178,'コンゴ共和国',82,'country'),
	(180,'コンゴ民主共和国',83,'country'),
	(184,'クック諸島',69,'country'),
	(188,'コスタリカ',79,'country'),
	(191,'クロアチア',74,'country'),
	(192,'キューバ',60,'country'),
	(196,'キプロス',59,'country'),
	(203,'チェコ',128,'country'),
	(204,'ベナン',196,'country'),
	(208,'デンマーク',136,'country'),
	(212,'ドミニカ国',141,'country'),
	(214,'ドミニカ共和国',140,'country'),
	(218,'エクアドル',33,'country'),
	(222,'エルサルバドル',38,'country'),
	(226,'赤道ギニア',113,'country'),
	(231,'エチオピア',36,'country'),
	(232,'エリトリア',37,'country'),
	(233,'エストニア',35,'country'),
	(234,'フェロー諸島',184,'country'),
	(238,'フォークランド諸島|フォークランド（マルビナス）諸島',185,'country'),
	(239,'サウスジョージア・サウスサンドウィッチ諸島',85,'country'),
	(242,'フィジー',178,'country'),
	(246,'フィンランド',180,'country'),
	(248,'オーランド諸島',41,'country'),
	(250,'フランス',187,'country'),
	(254,'フランス領ギアナ',188,'country'),
	(258,'フランス領ポリネシア',189,'country'),
	(260,'フランス領南方・南極地域',190,'country'),
	(262,'ジブチ',94,'country'),
	(266,'ガボン',52,'country'),
	(268,'グルジア',72,'country'),
	(270,'ガンビア',54,'country'),
	(275,'パレスチナ',173,'country'),
	(276,'ドイツ',137,'country'),
	(288,'ガーナ',44,'country'),
	(292,'ジブラルタル',95,'country'),
	(296,'キリバス',63,'country'),
	(300,'ギリシャ',62,'country'),
	(304,'グリーンランド',70,'country'),
	(308,'グレナダ',73,'country'),
	(312,'グアドループ',66,'country'),
	(316,'グアム',67,'country'),
	(320,'グアテマラ',65,'country'),
	(324,'ギニア',57,'country'),
	(328,'ガイアナ',47,'country'),
	(332,'ハイチ',162,'country'),
	(334,'ハード島とマクドナルド諸島',160,'country'),
	(336,'バチカン|バチカン市国',164,'country'),
	(340,'ホンジュラス',209,'country'),
	(344,'香港',208,'country'),
	(348,'ハンガリー',174,'country'),
	(352,'アイスランド',1,'country'),
	(356,'インド',26,'country'),
	(360,'インドネシア',27,'country'),
	(364,'イラン|イラン・イスラム共和国',25,'country'),
	(368,'イラク',24,'country'),
	(372,'アイルランド',2,'country'),
	(376,'イスラエル',22,'country'),
	(380,'イタリア',23,'country'),
	(384,'コートジボワール',77,'country'),
	(388,'ジャマイカ',97,'country'),
	(392,'日本',153,'country'),
	(398,'カザフスタン',48,'country'),
	(400,'ヨルダン',236,'country'),
	(404,'ケニア',76,'country'),
	(408,'朝鮮民主主義人民共和国',133,'country'),
	(410,'大韓民国',124,'country'),
	(414,'クウェート',68,'country'),
	(417,'キルギス',64,'country'),
	(418,'ラオス|ラオス人民民主共和国',237,'country'),
	(422,'レバノン',247,'country'),
	(426,'レソト',246,'country'),
	(428,'ラトビア',238,'country'),
	(430,'リベリア',242,'country'),
	(434,'リビア',240,'country'),
	(438,'リヒテンシュタイン',241,'country'),
	(440,'リトアニア',239,'country'),
	(442,'ルクセンブルク',244,'country'),
	(446,'マカオ',211,'country'),
	(450,'マダガスカル',213,'country'),
	(454,'マラウイ',215,'country'),
	(458,'マレーシア',219,'country'),
	(462,'モルディブ',230,'country'),
	(466,'マリ共和国|マリ',216,'country'),
	(470,'マルタ',217,'country'),
	(474,'マルティニーク',218,'country'),
	(478,'モーリタニア',227,'country'),
	(480,'モーリシャス',226,'country'),
	(484,'メキシコ',225,'country'),
	(492,'モナコ',229,'country'),
	(496,'モンゴル国|モンゴル',233,'country'),
	(498,'モルドバ|モルドバ共和国',231,'country'),
	(499,'モンテネグロ',234,'country'),
	(500,'モントセラト',235,'country'),
	(504,'モロッコ',232,'country'),
	(508,'モザンビーク',228,'country'),
	(512,'オマーン',42,'country'),
	(516,'ナミビア',148,'country'),
	(520,'ナウル',147,'country'),
	(524,'ネパール',157,'country'),
	(528,'オランダ',43,'country'),
	(531,'キュラソー島|キュラソー',61,'country'),
	(533,'アルバ',11,'country'),
	(534,'シント・マールテン|シント・マールテン（オランダ領）',100,'country'),
	(535,'BES諸島|ボネール、シント・ユースタティウスおよびサバ',205,'country'),
	(540,'ニューカレドニア',155,'country'),
	(548,'バヌアツ',166,'country'),
	(554,'ニュージーランド',156,'country'),
	(558,'ニカラグア',151,'country'),
	(562,'ニジェール',152,'country'),
	(566,'ナイジェリア',146,'country'),
	(570,'ニウエ',150,'country'),
	(574,'ノーフォーク島',158,'country'),
	(578,'ノルウェー',159,'country'),
	(580,'北マリアナ諸島',56,'country'),
	(581,'合衆国領有小離島',50,'country'),
	(583,'ミクロネシア連邦',221,'country'),
	(584,'マーシャル諸島',210,'country'),
	(585,'パラオ',170,'country'),
	(586,'パキスタン',163,'country'),
	(591,'パナマ',165,'country'),
	(598,'パプアニューギニア',168,'country'),
	(600,'パラグアイ',171,'country'),
	(604,'ペルー',200,'country'),
	(608,'フィリピン',179,'country'),
	(612,'ピトケアン諸島|ピトケアン',177,'country'),
	(616,'ポーランド',202,'country'),
	(620,'ポルトガル',207,'country'),
	(624,'ギニアビサウ',58,'country'),
	(626,'東ティモール',176,'country'),
	(630,'プエルトリコ',183,'country'),
	(634,'カタール',49,'country'),
	(638,'レユニオン',248,'country'),
	(642,'ルーマニア',243,'country'),
	(643,'ロシア|ロシア連邦',249,'country'),
	(646,'ルワンダ',245,'country'),
	(652,'サン・バルテルミー島|サン・バルテルミー',88,'country'),
	(654,'セントヘレナ・アセンションおよびトリスタンダクーニャ',118,'country'),
	(659,'セントクリストファー・ネイビス',116,'country'),
	(660,'アンギラ',14,'country'),
	(662,'セントルシア',119,'country'),
	(663,'サン・マルタン (西インド諸島)|サン・マルタン（フランス領）',92,'country'),
	(666,'サンピエール島・ミクロン島',90,'country'),
	(670,'セントビンセント・グレナディーン|セントビンセントおよびグレナディーン諸島',117,'country'),
	(674,'サンマリノ',91,'country'),
	(678,'サントメ・プリンシペ',87,'country'),
	(682,'サウジアラビア',84,'country'),
	(686,'セネガル',114,'country'),
	(688,'セルビア',115,'country'),
	(690,'セーシェル',112,'country'),
	(694,'シエラレオネ',93,'country'),
	(702,'シンガポール',99,'country'),
	(703,'スロバキア',109,'country'),
	(704,'ベトナム',195,'country'),
	(705,'スロベニア',110,'country'),
	(706,'ソマリア',120,'country'),
	(710,'南アフリカ共和国|南アフリカ',222,'country'),
	(716,'ジンバブエ',101,'country'),
	(724,'スペイン',106,'country'),
	(728,'南スーダン',223,'country'),
	(729,'スーダン',104,'country'),
	(732,'西サハラ',154,'country'),
	(740,'スリナム',107,'country'),
	(744,'スヴァールバル諸島およびヤンマイエン島',105,'country'),
	(748,'スワジランド',111,'country'),
	(752,'スウェーデン',103,'country'),
	(756,'スイス',102,'country'),
	(760,'シリア|シリア・アラブ共和国',98,'country'),
	(762,'タジキスタン',126,'country'),
	(764,'タイ王国|タイ',123,'country'),
	(768,'トーゴ',138,'country'),
	(772,'トケラウ',139,'country'),
	(776,'トンガ',145,'country'),
	(780,'トリニダード・トバゴ',142,'country'),
	(784,'アラブ首長国連邦',8,'country'),
	(788,'チュニジア',132,'country'),
	(792,'トルコ',144,'country'),
	(795,'トルクメニスタン',143,'country'),
	(796,'タークス・カイコス諸島',122,'country'),
	(798,'ツバル',135,'country'),
	(800,'ウガンダ',29,'country'),
	(804,'ウクライナ',30,'country'),
	(807,'マケドニア共和国|マケドニア旧ユーゴスラビア共和国',212,'country'),
	(818,'エジプト',34,'country'),
	(826,'イギリス',19,'country'),
	(831,'ガーンジー',46,'country'),
	(832,'ジャージー',96,'country'),
	(833,'マン島',220,'country'),
	(834,'タンザニア',127,'country'),
	(840,'アメリカ合衆国',5,'country'),
	(850,'アメリカ領ヴァージン諸島',6,'country'),
	(854,'ブルキナファソ',192,'country'),
	(858,'ウルグアイ',32,'country'),
	(860,'ウズベキスタン',31,'country'),
	(862,'ベネズエラ|ベネズエラ・ボリバル共和国',197,'country'),
	(876,'ウォリス・フツナ',28,'country'),
	(882,'サモア',86,'country'),
	(887,'イエメン',18,'country'),
	(894,'ザンビア',89,'country');

/*!40000 ALTER TABLE `mtb_country` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mtb_csv_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mtb_csv_type`;

CREATE TABLE `mtb_csv_type` (
  `id` smallint(5) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) unsigned NOT NULL,
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `mtb_csv_type` WRITE;
/*!40000 ALTER TABLE `mtb_csv_type` DISABLE KEYS */;

INSERT INTO `mtb_csv_type` (`id`, `name`, `sort_no`, `discriminator_type`)
VALUES
	(1,'商品CSV',3,'csvtype'),
	(2,'会員CSV',4,'csvtype'),
	(3,'受注CSV',1,'csvtype'),
	(4,'配送CSV',1,'csvtype'),
	(5,'カテゴリCSV',5,'csvtype'),
	(6,'商品レビューCSV',6,'csvtype');

/*!40000 ALTER TABLE `mtb_csv_type` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mtb_customer_order_status
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mtb_customer_order_status`;

CREATE TABLE `mtb_customer_order_status` (
  `id` smallint(5) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) unsigned NOT NULL,
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `mtb_customer_order_status` WRITE;
/*!40000 ALTER TABLE `mtb_customer_order_status` DISABLE KEYS */;

INSERT INTO `mtb_customer_order_status` (`id`, `name`, `sort_no`, `discriminator_type`)
VALUES
	(1,'注文受付',0,'customerorderstatus'),
	(3,'注文取消し',4,'customerorderstatus'),
	(4,'注文受付',3,'customerorderstatus'),
	(5,'発送済み',6,'customerorderstatus'),
	(6,'注文受付',2,'customerorderstatus'),
	(7,'注文受付',1,'customerorderstatus'),
	(8,'注文未完了',5,'customerorderstatus'),
	(9,'返品',7,'customerorderstatus'),
	(10,'注文受付（予約注文）',8,'customerorderstatus');

/*!40000 ALTER TABLE `mtb_customer_order_status` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mtb_customer_status
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mtb_customer_status`;

CREATE TABLE `mtb_customer_status` (
  `id` smallint(5) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) unsigned NOT NULL,
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `mtb_customer_status` WRITE;
/*!40000 ALTER TABLE `mtb_customer_status` DISABLE KEYS */;

INSERT INTO `mtb_customer_status` (`id`, `name`, `sort_no`, `discriminator_type`)
VALUES
	(1,'仮会員',0,'customerstatus'),
	(2,'本会員',1,'customerstatus'),
	(3,'退会',2,'customerstatus');

/*!40000 ALTER TABLE `mtb_customer_status` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mtb_device_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mtb_device_type`;

CREATE TABLE `mtb_device_type` (
  `id` smallint(5) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) unsigned NOT NULL,
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `mtb_device_type` WRITE;
/*!40000 ALTER TABLE `mtb_device_type` DISABLE KEYS */;

INSERT INTO `mtb_device_type` (`id`, `name`, `sort_no`, `discriminator_type`)
VALUES
	(2,'モバイル',0,'devicetype'),
	(10,'PC',1,'devicetype');

/*!40000 ALTER TABLE `mtb_device_type` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mtb_job
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mtb_job`;

CREATE TABLE `mtb_job` (
  `id` smallint(5) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) unsigned NOT NULL,
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `mtb_job` WRITE;
/*!40000 ALTER TABLE `mtb_job` DISABLE KEYS */;

INSERT INTO `mtb_job` (`id`, `name`, `sort_no`, `discriminator_type`)
VALUES
	(1,'公務員',0,'job'),
	(2,'コンサルタント',1,'job'),
	(3,'コンピューター関連技術職',2,'job'),
	(4,'コンピューター関連以外の技術職',3,'job'),
	(5,'金融関係',4,'job'),
	(6,'医師',5,'job'),
	(7,'弁護士',6,'job'),
	(8,'総務・人事・事務',7,'job'),
	(9,'営業・販売',8,'job'),
	(10,'研究・開発',9,'job'),
	(11,'広報・宣伝',10,'job'),
	(12,'企画・マーケティング',11,'job'),
	(13,'デザイン関係',12,'job'),
	(14,'会社経営・役員',13,'job'),
	(15,'出版・マスコミ関係',14,'job'),
	(16,'学生・フリーター',15,'job'),
	(17,'主婦',16,'job'),
	(18,'その他',17,'job');

/*!40000 ALTER TABLE `mtb_job` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mtb_order_item_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mtb_order_item_type`;

CREATE TABLE `mtb_order_item_type` (
  `id` smallint(5) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) unsigned NOT NULL,
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `mtb_order_item_type` WRITE;
/*!40000 ALTER TABLE `mtb_order_item_type` DISABLE KEYS */;

INSERT INTO `mtb_order_item_type` (`id`, `name`, `sort_no`, `discriminator_type`)
VALUES
	(1,'商品',0,'orderitemtype'),
	(2,'送料',1,'orderitemtype'),
	(3,'手数料',2,'orderitemtype'),
	(4,'割引',3,'orderitemtype'),
	(5,'税',4,'orderitemtype'),
	(6,'ポイント',5,'orderitemtype');

/*!40000 ALTER TABLE `mtb_order_item_type` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mtb_order_status
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mtb_order_status`;

CREATE TABLE `mtb_order_status` (
  `id` smallint(5) unsigned NOT NULL,
  `display_order_count` tinyint(1) NOT NULL DEFAULT 0,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) unsigned NOT NULL,
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `mtb_order_status` WRITE;
/*!40000 ALTER TABLE `mtb_order_status` DISABLE KEYS */;

INSERT INTO `mtb_order_status` (`id`, `display_order_count`, `name`, `sort_no`, `discriminator_type`)
VALUES
	(1,1,'新規受付',0,'orderstatus'),
	(3,0,'注文取消し',3,'orderstatus'),
	(4,1,'対応中',2,'orderstatus'),
	(5,0,'発送済み',4,'orderstatus'),
	(6,1,'入金済み',1,'orderstatus'),
	(7,0,'決済処理中',6,'orderstatus'),
	(8,0,'購入処理中',5,'orderstatus'),
	(9,0,'返品',7,'orderstatus'),
	(10,0,'新規受付（予約注文）',8,'orderstatus');

/*!40000 ALTER TABLE `mtb_order_status` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mtb_order_status_color
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mtb_order_status_color`;

CREATE TABLE `mtb_order_status_color` (
  `id` smallint(5) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) unsigned NOT NULL,
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `mtb_order_status_color` WRITE;
/*!40000 ALTER TABLE `mtb_order_status_color` DISABLE KEYS */;

INSERT INTO `mtb_order_status_color` (`id`, `name`, `sort_no`, `discriminator_type`)
VALUES
	(1,'#437ec4',0,'orderstatuscolor'),
	(3,'#C04949',3,'orderstatuscolor'),
	(4,'#EEB128',2,'orderstatuscolor'),
	(5,'#25B877',4,'orderstatuscolor'),
	(6,'#25B877',1,'orderstatuscolor'),
	(7,'#A3A3A3',6,'orderstatuscolor'),
	(8,'#A3A3A3',5,'orderstatuscolor'),
	(9,'#C04949',7,'orderstatuscolor'),
	(10,'#437ec4',8,'orderstatuscolor');

/*!40000 ALTER TABLE `mtb_order_status_color` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mtb_page_max
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mtb_page_max`;

CREATE TABLE `mtb_page_max` (
  `id` smallint(5) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) unsigned NOT NULL,
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `mtb_page_max` WRITE;
/*!40000 ALTER TABLE `mtb_page_max` DISABLE KEYS */;

INSERT INTO `mtb_page_max` (`id`, `name`, `sort_no`, `discriminator_type`)
VALUES
	(10,'10',0,'pagemax'),
	(20,'20',1,'pagemax'),
	(30,'30',2,'pagemax'),
	(40,'40',3,'pagemax'),
	(50,'50',4,'pagemax'),
	(60,'60',5,'pagemax'),
	(70,'70',6,'pagemax'),
	(80,'80',7,'pagemax'),
	(90,'90',8,'pagemax'),
	(100,'100',9,'pagemax');

/*!40000 ALTER TABLE `mtb_page_max` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mtb_pref
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mtb_pref`;

CREATE TABLE `mtb_pref` (
  `id` smallint(5) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) unsigned NOT NULL,
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `mtb_pref` WRITE;
/*!40000 ALTER TABLE `mtb_pref` DISABLE KEYS */;

INSERT INTO `mtb_pref` (`id`, `name`, `sort_no`, `discriminator_type`)
VALUES
	(1,'北海道',1,'pref'),
	(2,'青森県',2,'pref'),
	(3,'岩手県',3,'pref'),
	(4,'宮城県',4,'pref'),
	(5,'秋田県',5,'pref'),
	(6,'山形県',6,'pref'),
	(7,'福島県',7,'pref'),
	(8,'茨城県',8,'pref'),
	(9,'栃木県',9,'pref'),
	(10,'群馬県',10,'pref'),
	(11,'埼玉県',11,'pref'),
	(12,'千葉県',12,'pref'),
	(13,'東京都',13,'pref'),
	(14,'神奈川県',14,'pref'),
	(15,'新潟県',15,'pref'),
	(16,'富山県',16,'pref'),
	(17,'石川県',17,'pref'),
	(18,'福井県',18,'pref'),
	(19,'山梨県',19,'pref'),
	(20,'長野県',20,'pref'),
	(21,'岐阜県',21,'pref'),
	(22,'静岡県',22,'pref'),
	(23,'愛知県',23,'pref'),
	(24,'三重県',24,'pref'),
	(25,'滋賀県',25,'pref'),
	(26,'京都府',26,'pref'),
	(27,'大阪府',27,'pref'),
	(28,'兵庫県',28,'pref'),
	(29,'奈良県',29,'pref'),
	(30,'和歌山県',30,'pref'),
	(31,'鳥取県',31,'pref'),
	(32,'島根県',32,'pref'),
	(33,'岡山県',33,'pref'),
	(34,'広島県',34,'pref'),
	(35,'山口県',35,'pref'),
	(36,'徳島県',36,'pref'),
	(37,'香川県',37,'pref'),
	(38,'愛媛県',38,'pref'),
	(39,'高知県',39,'pref'),
	(40,'福岡県',40,'pref'),
	(41,'佐賀県',41,'pref'),
	(42,'長崎県',42,'pref'),
	(43,'熊本県',43,'pref'),
	(44,'大分県',44,'pref'),
	(45,'宮崎県',45,'pref'),
	(46,'鹿児島県',46,'pref'),
	(47,'沖縄県',47,'pref');

/*!40000 ALTER TABLE `mtb_pref` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mtb_product_list_max
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mtb_product_list_max`;

CREATE TABLE `mtb_product_list_max` (
  `id` smallint(5) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) unsigned NOT NULL,
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `mtb_product_list_max` WRITE;
/*!40000 ALTER TABLE `mtb_product_list_max` DISABLE KEYS */;

INSERT INTO `mtb_product_list_max` (`id`, `name`, `sort_no`, `discriminator_type`)
VALUES
	(20,'20件',0,'productlistmax'),
	(40,'40件',1,'productlistmax'),
	(60,'60件',2,'productlistmax');

/*!40000 ALTER TABLE `mtb_product_list_max` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mtb_product_list_order_by
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mtb_product_list_order_by`;

CREATE TABLE `mtb_product_list_order_by` (
  `id` smallint(5) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) unsigned NOT NULL,
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `mtb_product_list_order_by` WRITE;
/*!40000 ALTER TABLE `mtb_product_list_order_by` DISABLE KEYS */;

INSERT INTO `mtb_product_list_order_by` (`id`, `name`, `sort_no`, `discriminator_type`)
VALUES
	(1,'価格が低い順',3,'productlistorderby'),
	(2,'新着順',5,'productlistorderby'),
	(3,'価格が高い順',4,'productlistorderby'),
	(4,'おすすめ順',2,'productlistorderby');

/*!40000 ALTER TABLE `mtb_product_list_order_by` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mtb_product_status
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mtb_product_status`;

CREATE TABLE `mtb_product_status` (
  `id` smallint(5) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) unsigned NOT NULL,
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `mtb_product_status` WRITE;
/*!40000 ALTER TABLE `mtb_product_status` DISABLE KEYS */;

INSERT INTO `mtb_product_status` (`id`, `name`, `sort_no`, `discriminator_type`)
VALUES
	(1,'公開',0,'productstatus'),
	(2,'非公開',1,'productstatus'),
	(3,'廃止',2,'productstatus');

/*!40000 ALTER TABLE `mtb_product_status` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mtb_rounding_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mtb_rounding_type`;

CREATE TABLE `mtb_rounding_type` (
  `id` smallint(5) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) unsigned NOT NULL,
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `mtb_rounding_type` WRITE;
/*!40000 ALTER TABLE `mtb_rounding_type` DISABLE KEYS */;

INSERT INTO `mtb_rounding_type` (`id`, `name`, `sort_no`, `discriminator_type`)
VALUES
	(1,'四捨五入',0,'roundingtype'),
	(2,'切り捨て',1,'roundingtype'),
	(3,'切り上げ',2,'roundingtype');

/*!40000 ALTER TABLE `mtb_rounding_type` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mtb_sale_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mtb_sale_type`;

CREATE TABLE `mtb_sale_type` (
  `id` smallint(5) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) unsigned NOT NULL,
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `mtb_sale_type` WRITE;
/*!40000 ALTER TABLE `mtb_sale_type` DISABLE KEYS */;

INSERT INTO `mtb_sale_type` (`id`, `name`, `sort_no`, `discriminator_type`)
VALUES
	(1,'通常購入',0,'saletype'),
	(2,'定期購買',1,'saletype');

/*!40000 ALTER TABLE `mtb_sale_type` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mtb_sex
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mtb_sex`;

CREATE TABLE `mtb_sex` (
  `id` smallint(5) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) unsigned NOT NULL,
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `mtb_sex` WRITE;
/*!40000 ALTER TABLE `mtb_sex` DISABLE KEYS */;

INSERT INTO `mtb_sex` (`id`, `name`, `sort_no`, `discriminator_type`)
VALUES
	(1,'男性',0,'sex'),
	(2,'女性',1,'sex');

/*!40000 ALTER TABLE `mtb_sex` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mtb_tax_display_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mtb_tax_display_type`;

CREATE TABLE `mtb_tax_display_type` (
  `id` smallint(5) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) unsigned NOT NULL,
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `mtb_tax_display_type` WRITE;
/*!40000 ALTER TABLE `mtb_tax_display_type` DISABLE KEYS */;

INSERT INTO `mtb_tax_display_type` (`id`, `name`, `sort_no`, `discriminator_type`)
VALUES
	(1,'税抜',0,'taxdisplaytype'),
	(2,'税込',1,'taxdisplaytype');

/*!40000 ALTER TABLE `mtb_tax_display_type` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mtb_tax_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mtb_tax_type`;

CREATE TABLE `mtb_tax_type` (
  `id` smallint(5) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) unsigned NOT NULL,
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `mtb_tax_type` WRITE;
/*!40000 ALTER TABLE `mtb_tax_type` DISABLE KEYS */;

INSERT INTO `mtb_tax_type` (`id`, `name`, `sort_no`, `discriminator_type`)
VALUES
	(1,'課税',0,'taxtype'),
	(2,'不課税',1,'taxtype'),
	(3,'非課税',2,'taxtype');

/*!40000 ALTER TABLE `mtb_tax_type` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mtb_work
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mtb_work`;

CREATE TABLE `mtb_work` (
  `id` smallint(5) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) unsigned NOT NULL,
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `mtb_work` WRITE;
/*!40000 ALTER TABLE `mtb_work` DISABLE KEYS */;

INSERT INTO `mtb_work` (`id`, `name`, `sort_no`, `discriminator_type`)
VALUES
	(0,'非稼働',0,'work'),
	(1,'稼働',1,'work');

/*!40000 ALTER TABLE `mtb_work` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table plg_ccp_config
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plg_ccp_config`;

CREATE TABLE `plg_ccp_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rounding_type_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_5BAA81411BD5C574` (`rounding_type_id`),
  CONSTRAINT `FK_5BAA81411BD5C574` FOREIGN KEY (`rounding_type_id`) REFERENCES `mtb_rounding_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `plg_ccp_config` WRITE;
/*!40000 ALTER TABLE `plg_ccp_config` DISABLE KEYS */;

INSERT INTO `plg_ccp_config` (`id`, `rounding_type_id`)
VALUES
	(1,1);

/*!40000 ALTER TABLE `plg_ccp_config` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table plg_ccp_customer_class
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plg_ccp_customer_class`;

CREATE TABLE `plg_ccp_customer_class` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `discount_rate` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `plg_ccp_customer_class` WRITE;
/*!40000 ALTER TABLE `plg_ccp_customer_class` DISABLE KEYS */;

INSERT INTO `plg_ccp_customer_class` (`id`, `name`, `discount_rate`)
VALUES
	(1,'会員価格',NULL);

/*!40000 ALTER TABLE `plg_ccp_customer_class` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table plg_ccp_customer_class_price
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plg_ccp_customer_class_price`;

CREATE TABLE `plg_ccp_customer_class_price` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_class_id` int(10) unsigned DEFAULT NULL,
  `product_class_id` int(10) unsigned DEFAULT NULL,
  `price` decimal(12,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_26A264FBC6C5483F` (`customer_class_id`),
  KEY `IDX_26A264FB21B06187` (`product_class_id`),
  CONSTRAINT `FK_26A264FB21B06187` FOREIGN KEY (`product_class_id`) REFERENCES `dtb_product_class` (`id`),
  CONSTRAINT `FK_26A264FBC6C5483F` FOREIGN KEY (`customer_class_id`) REFERENCES `plg_ccp_customer_class` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `plg_ccp_customer_class_price` WRITE;
/*!40000 ALTER TABLE `plg_ccp_customer_class_price` DISABLE KEYS */;

INSERT INTO `plg_ccp_customer_class_price` (`id`, `customer_class_id`, `product_class_id`, `price`)
VALUES
	(1,1,38,1044.00),
	(2,1,39,928.00),
	(3,1,56,8640.00),
	(4,1,57,7680.00),
	(5,1,54,6480.00),
	(6,1,55,5760.00),
	(7,1,52,18720.00),
	(8,1,53,16640.00),
	(9,1,50,13455.00),
	(10,1,51,11960.00),
	(11,1,48,29250.00),
	(12,1,49,26000.00),
	(13,1,46,8190.00),
	(14,1,47,7280.00),
	(15,1,44,4320.00),
	(16,1,45,3840.00),
	(17,1,42,12960.00),
	(18,1,43,11520.00),
	(19,1,40,23760.00),
	(20,1,41,21120.00),
	(21,1,36,42750.00),
	(22,1,37,38000.00),
	(23,1,58,6090.00),
	(24,1,59,6264.00),
	(25,1,60,5568.00),
	(26,1,11,NULL);

/*!40000 ALTER TABLE `plg_ccp_customer_class_price` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table plg_check_kagoochi_sendedmail
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plg_check_kagoochi_sendedmail`;

CREATE TABLE `plg_check_kagoochi_sendedmail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target_id` int(11) NOT NULL,
  `target_kbn` varchar(1) NOT NULL DEFAULT '1',
  `customer_id` int(11) NOT NULL,
  `send_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  PRIMARY KEY (`id`),
  KEY `target_kbn` (`target_kbn`,`target_id`),
  KEY `send_date` (`send_date`),
  KEY `target_id` (`target_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `plg_check_kagoochi_sendedmail` WRITE;
/*!40000 ALTER TABLE `plg_check_kagoochi_sendedmail` DISABLE KEYS */;

INSERT INTO `plg_check_kagoochi_sendedmail` (`id`, `target_id`, `target_kbn`, `customer_id`, `send_date`)
VALUES
	(1,107,'o',1,'2020-09-04 04:05:13'),
	(2,108,'o',1,'2020-09-04 07:10:35'),
	(3,309,'c',1,'2020-09-04 08:39:04'),
	(4,109,'o',1,'2020-09-07 07:26:05'),
	(5,89,'o',8,'2020-09-07 11:26:20'),
	(6,86,'o',10,'2020-09-07 11:26:20'),
	(7,88,'o',0,'2020-09-07 11:26:20'),
	(8,58,'o',9,'2020-09-07 11:26:20'),
	(9,341,'c',6,'2020-09-09 09:55:15'),
	(10,350,'c',1,'2020-09-12 23:48:37'),
	(11,81,'o',4,'2020-09-12 23:48:37'),
	(12,106,'o',0,'2020-09-12 23:48:37'),
	(13,79,'o',0,'2020-09-12 23:48:37'),
	(14,112,'o',18,'2020-09-12 23:48:37'),
	(15,96,'o',0,'2020-09-12 23:48:37'),
	(16,218,'c',7,'2020-09-12 23:48:38'),
	(17,18,'o',0,'2020-09-12 23:48:38'),
	(18,350,'c',1,'2020-09-14 08:06:11'),
	(19,350,'c',1,'2020-09-14 08:21:49');

/*!40000 ALTER TABLE `plg_check_kagoochi_sendedmail` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table plg_contact_management4_config
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plg_contact_management4_config`;

CREATE TABLE `plg_contact_management4_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table plg_contact_management4_contact
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plg_contact_management4_contact`;

CREATE TABLE `plg_contact_management4_contact` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `staff_id` int(10) unsigned DEFAULT NULL,
  `product_id` int(10) unsigned DEFAULT NULL,
  `pref_id` smallint(5) unsigned DEFAULT NULL,
  `contact_status_id` smallint(5) unsigned DEFAULT NULL,
  `name01` varchar(255) NOT NULL,
  `name02` varchar(255) NOT NULL,
  `kana01` varchar(255) DEFAULT NULL,
  `kana02` varchar(255) DEFAULT NULL,
  `postal_code` varchar(8) DEFAULT NULL,
  `addr01` varchar(255) DEFAULT NULL,
  `addr02` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `phone_number` varchar(14) DEFAULT NULL,
  `contents` longtext DEFAULT NULL,
  `note` longtext DEFAULT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  PRIMARY KEY (`id`),
  KEY `IDX_53DDB4B9395C3F3` (`customer_id`),
  KEY `IDX_53DDB4BD4D57CD` (`staff_id`),
  KEY `IDX_53DDB4B4584665A` (`product_id`),
  KEY `IDX_53DDB4BE171EF5F` (`pref_id`),
  KEY `IDX_53DDB4B50623C6` (`contact_status_id`),
  CONSTRAINT `FK_53DDB4B4584665A` FOREIGN KEY (`product_id`) REFERENCES `dtb_product` (`id`),
  CONSTRAINT `FK_53DDB4B50623C6` FOREIGN KEY (`contact_status_id`) REFERENCES `plg_contact_management4_contact_status` (`id`),
  CONSTRAINT `FK_53DDB4B9395C3F3` FOREIGN KEY (`customer_id`) REFERENCES `dtb_customer` (`id`),
  CONSTRAINT `FK_53DDB4BD4D57CD` FOREIGN KEY (`staff_id`) REFERENCES `dtb_member` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_53DDB4BE171EF5F` FOREIGN KEY (`pref_id`) REFERENCES `mtb_pref` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `plg_contact_management4_contact` WRITE;
/*!40000 ALTER TABLE `plg_contact_management4_contact` DISABLE KEYS */;

INSERT INTO `plg_contact_management4_contact` (`id`, `customer_id`, `staff_id`, `product_id`, `pref_id`, `contact_status_id`, `name01`, `name02`, `kana01`, `kana02`, `postal_code`, `addr01`, `addr02`, `email`, `phone_number`, `contents`, `note`, `create_date`, `update_date`)
VALUES
	(1,1,NULL,NULL,13,1,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク','1570077','世田谷区鎌田','123建物','djfre2000@hotmail.com','0123456789','Test Contact form',NULL,'2020-05-11 12:10:48','2020-05-11 12:10:48'),
	(2,NULL,1,NULL,13,2,'多炉','ゆう','タロ','ユウ','1570077','世田谷区鎌田',NULL,'djfre2000@hotmail.com',NULL,'Test Frederic contact form',NULL,'2020-05-15 01:15:21','2020-05-15 02:14:52'),
	(3,1,NULL,NULL,13,1,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク','1570077','世田谷区鎌田','123建物','djfre2000@hotmail.com','0123456789','テストメール',NULL,'2020-05-16 00:38:52','2020-05-16 00:38:52'),
	(4,1,NULL,NULL,13,1,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク','1570077','世田谷区鎌田','123建物','djfre2000@hotmail.com','0123456789','テスト2',NULL,'2020-05-16 00:45:45','2020-05-16 00:45:45'),
	(5,NULL,NULL,NULL,13,1,'鈴木','真広','スズキ','マサヒロ','1010032','千代田区岩本町1丁目8番15号','岩本町喜多ビル３F　B号室','kuwa-36@kuwatechno.com','08097222090','突然のメール失礼致します。私、株式会社桑原/鈴木と申します。\r\n御社にて取扱いしている化粧品容器に関して、当社サービスのご提案ができればと思い失礼を承知でメールさせて頂きました。\r\n\r\n当社は国内外にて衣料品、雑貨関連の検品(軽作業、流通加工含む)、修整を行っている会社になります。当社事業として国内外にて要望が大きい分野として化粧品容器の品質改善等、その他関連商品での第三者検品サービスを提供させて頂きたくご案内をさせて頂きます。\r\n\r\n当社ホームページ：http：//www.kuwatechno.jp　ご参考までに確認頂けると幸いです。\r\n\r\n化粧品容器のほとんどが中国、その他海外または国内生産の容器メーカーによって製造されていますが、調査したところ海外生産容器のおよそ２０％に品質の問題があり返品対応もしくは廃棄処分となります。主な問題は、容器の内側と外側の清潔さ、ボトルの外側のロゴの摩耗、プラスチック容器の溶解、糸とキャップの間の接続、ポンプの柔軟性、およびボトルの外側の傷など不良ロスが大変多く、 現在、海外工場にて容器検査を工場内にて実施する会社は非常に少なく、中小メーカーの多くは自社で製品を検査することができず容器の品質に問題を抱えたまま商品を出荷、国内薬剤注入作業時に容器の品質を保証することができません。 多くの化粧品メーカーは独自の検査ワークショップを日本に配置していますが、海外での第三社機関による検査品質保証、アフターサービスなど非常に高いサービスを達成できる場合、国内での容器不良に対してのロスが大幅にカットできるメリットがございます。それに伴い多くの化粧品メーカーも日本での検査ワークショップの縮小、人件費とスペースを削減することが可能となります。容器品質関連にてお困りの際はご連絡を頂けますと幸いです。\r\n当社、国内外のネットワークを駆使してご相談に乗らせて頂きたいと思います。\r\n長文のご連絡となり失礼しました。\r\n\r\nなにもないことが一番ですが、生産には予期せぬことがつきものです、その際には何なりとお申しつけください。宜しくお願い申し上げます。',NULL,'2020-07-09 04:57:42','2020-07-09 04:57:42'),
	(6,NULL,NULL,NULL,13,1,'和田','幸大','ワダ','ユキヒロ','1010051','千代田区神田神保町','1-3-5　寿ビル','wada@bunka-ad.jp','0332916041','お忙しいところ突然のメールで失礼を致します。\r\nマガジンハウス社の代理店をしております、\r\n文化アディックの和田と申します。\r\n\r\n弊社が『an・an』という雑誌の8月26日発売「新しい暮らしのカタチ」特集号のなかで、\r\n有料のご紹介企画を作っております。\r\n\r\n『an・an』誌（マガジンハウス）とは、\r\n毎週水曜日に全国の書店やコンビニで、毎号平均17万部発行の人気の女性週刊誌です。\r\n高い知名度と、特集によってはネットニュースやTVにも取り上げられるほど影響力や話題性のある雑誌です。\r\n8月26日発売号が「暮らしのカタチ」特集で発売になり、\r\n外出を控えないといけない昨今の新しい生活様式に向けて、お家時間をより快適に実りある時間にするための、\r\n趣味・食事・インテリア・ファッションなどを『an・an』目線でご紹介します。\r\n\r\n弊社がこの特集に合わせて、\r\nお家時間を有意義に過ごすためにおすすめしたい、アイテム・ショップ・サービス等をニュース記事のようにご紹介する連合広告を実施します。\r\nHPを拝見し、ご都合が合えばこちらで貴社商品のご紹介ができればと思い、ご案内にあがりました。\r\n※1件様1/8Pスペースからリーズナブルに反響をお試しいただけます。\r\n\r\n人気知名度のある『an・an』のため、雑誌を見ての直接の反響はもちろん、\r\n掲載誌されたことを表紙画像と一緒にHPやSNSにアップしていただくと、\r\n新規のお客様への長期的かつ効果的な販促ツールとしてもご期待いただけます。\r\n\r\n添付ファイルで閲覧していただける企画資料をご用意しております。\r\nご興味がございましたら、御見積を含めた詳細内容をメールにてお送りさせていただきますので、\r\nご返信いただけますと幸いです。\r\n\r\n突然のご案内となりますが、\r\n何卒宜しくお願い申し上げます。\r\n\r\n\r\n※他の者よりご連絡がありましたり、\r\n　既に本誌に掲載実績がおありの場合、失礼の段をお許しください。\r\n\r\n株式会社文化アディック\r\n担当　和田',NULL,'2020-07-17 01:58:04','2020-07-17 01:58:04'),
	(7,NULL,NULL,NULL,13,1,'中川','理恵','ナカガワ','リエ','1060032','港区六本木',NULL,'nakagawa@tankpub.co.jp',NULL,'【ポコチェ9月号】へ出稿のご提案\r\n\r\n\r\n株式会社メディファイン\r\nご担当者様\r\n\r\nはじめまして。\r\n私、出版社タンクパブリケーションズ ポコチェ企画部の中川と申します。\r\n弊社では毎月女性情報誌「Poco’ce（ポコチェ）」を\r\n都心で働く30代女性に向け、都営地下鉄や定期購読にて\r\n毎月8万部発行しております。\r\n\r\nこの度は、ポコチェ9月号(8/25発行 表紙：水川あさみ）にて\r\n「マリタイムのCBDウォーター」をご紹介させていただきたく思っております。\r\n\r\n◆電子ブックで誌面をご覧頂けます。\r\nwww.pococe.com/actibook/pococe205/_SWF_Window.html?pagecode=01\r\n\r\n一部費用を頂く、タイアップでの記事掲載となりますが、\r\nよろしければ企画書を一度ご覧いただけないでしょうか。\r\n\r\n大変恐縮ですが、ご返信頂けますと幸いです。\r\n何卒よろしくお願い致します。\r\n\r\n\r\n****************　Poco\'ce　**********\r\n\r\n株式会社タンクパブリケーションズ\r\nTANK PUBLICATIONS INC.\r\n　企画部　　中川　理恵（nakagawa rie）\r\n\r\n〒 106-0032 \r\n東京都港区六本木2-3-9ユニオン六本木ビル5F\r\n【TEL】 03-6277-8290　\r\n【携帯】080-7702-2841\r\n【Mail】nakagawa@tankpub.co.jp\r\n【公式HP】www.pococe.com/\r\n\r\n**************************************',NULL,'2020-07-22 06:12:22','2020-07-22 06:12:22'),
	(8,NULL,NULL,NULL,13,1,'髙田','美憂','タカタ','ミユウ','1066224','港区六本木住友不動産六本木グランドタワー２４階',NULL,'takata-miyu@dmm.com','0355449914','マーケティング・広報ご担当者様\r\n\r\nお世話になります。\r\n合同会社DMM.com貴社担当高田でございます。\r\n突然のご連絡申し訳ございません。\r\n\r\n株式会社メディファイン様特別にご連絡させて頂いております。\r\n \r\nTwitterや、弊社クイズアプリ（AQUIZ）にて、貴社の認知促進と更なるCBDオイル拡散の起爆剤としてご活用いただければと考え、ご提案させて頂きます。\r\n \r\n現在、大手飲料メーカー、ゲーム会社など様々な企業からサービスの理解促進や、購買率向上の目的でご利用頂いております。\r\n\r\n認知→関心→集客→ユーザー獲得のフェーズにおいて、\r\n貴社ブランドのタイミングに合わせ、マッチングしたサービスのご提案を\r\nさせて頂ければと考えております。\r\n\r\n弊社サービスは大きく２つございます。\r\n\r\n①【理解保証】(料金確定型プラン)\r\n→AQUIZユーザーに貴社の商品・サービスに関するクイズを10問解き、\r\nブランドの認知・理解を促すサービスです。\r\nブランド認知・購買率向上に効果的です。\r\nhttps://f.hubspotusercontent40.net/hubfs/7146949/mediaguide/AQUIZ%20Proposal%20material.pdf\r\n\r\n②【BuzzForce】(完全成果報酬型プラン)\r\n→貴社の商品・サービスをTwitterで拡散し、トレンド入りさせるサービスです。\r\n集客目的で、効果的です。\r\nhttps://f.hubspotusercontent40.net/hubfs/7146949/mediaguide/BuzzForce%20Proposal%20material.pdf\r\n\r\n現況、Twitterトレンド枠へ広告掲載する場合、多額の費用が必要です。\r\n弊社BuzzForceプランでは、完全成果報酬型となっており、100万円で実施可能です。\r\nリスクや広告費用を押さえた上で、Twitterユーザーに圧倒的に拡散する事が可能です。\r\n\r\n※過去実績では、Twitterトレンド入り2〜6位内で、100%ランクインしております。\r\n\r\n弊社AQUIZアプリ会員数42万人、1日利用者数は、3.5万人です。\r\n\r\nご興味をお持ち頂けましたら、\r\nオンラインにてご商談の機会を頂けますと幸いです。\r\n\r\n-------------------------------------------------\r\n【日程候補】\r\n8/5(水)　　12:00-15:00\r\n8/6(木)　　14:00-16:00　17:00-19:00\r\n8/7(金)　　13:00-19:00\r\n-------------------------------------------------\r\n\r\n※ご興味ございますサービス①、②合わせて、メールにてご返信頂けますと幸いです。 \r\n\r\nご連絡お待ちしております。\r\n宜しくお願い致します。',NULL,'2020-08-03 02:38:21','2020-08-03 02:38:21'),
	(9,NULL,NULL,NULL,13,1,'川崎','隼輝','カワサキ','ジュンキ','1500002','渋谷区渋谷','2-4-6　野村ビル2F','junki@azeal-e.jp','09024183851','株式会社メディファイン\r\n代表　内藤 海鏡 様\r\n\r\nお世話になります。\r\nガールズハッピースタイル番組編集部の川崎隼輝で御座います。\r\n\r\n突然のご連絡、恐縮で御座います。\r\nまだ企画段階のものではありますが、9 or 10月に放送予定の番組企画で「2020年今話題のCBD特集（仮）」を予定しております。企画の中でCBDへのこだわりや魅力、効果のありそうなパワーワードやインパクトを持ったCBD企業をリサーチしていたところ、MARITIME様が気になりましてご連絡させて頂きました。\r\n構成もまだまだこれからにはなりますが、これから購入するユーザー側の意見を取り入れてトークを展開していくといった流れで検討しております。\r\n今回の企画は番組側でも一大企画で制作費に関してもかなり調整を入れる事の出来る枠になります。\r\n少し変わった形式の番組になりますので、企画にご賛同いただけたらご連絡頂ければと思います。\r\nまた、当番組媒体では、お取り上げしましたブランドを各方面で飛躍する事を最大限のご協力をしておりますので、お取引の際はどうぞ宜しくお願い申し上げます。\r\n\r\n連絡先：090-2418-3851\r\n\r\n\r\n平行して編集部でリサーチは続けておりますのでご賛同頂けましたら、8/19(水)までにご連絡頂きますようお願い致します。\r\n\r\n\r\n\r\n株式会社アジールブランディング\r\n川崎隼輝',NULL,'2020-08-11 10:15:49','2020-08-11 10:15:49'),
	(10,NULL,NULL,NULL,13,1,'林','輝実','ハヤシ','テルミ','1080073','港区','三田3-2-8　Net2三田ビル','t-hayashi@m-gfp.com','0368236257','ご担当者様\r\n\r\n突然のご連絡失礼いたします。\r\nGrowth Factor Projectの林と申します。\r\n\r\n私どもは、日本人から採取した幹細胞(脂肪由来)を、\r\n国内の施設にて培養した純国産・原液の培養上清を取り扱っております。\r\n表示名称は「ヒト脂肪細胞順化培養液」になります。\r\n\r\n培養上清にはエクソソームや成長因子、サイトカインが豊富に含まれており、\r\n美容面では肌のターンオーバーの促進に効果が期待できます。\r\n\r\n化粧品業界で使われている培養上清の実情は、\r\n安全性・品質が疑われるものも多くあります。\r\n\r\n・幹細胞ではなく、通常細胞を培養しただけ\r\n・培地液を販売している\r\n・水などで希釈している\r\n・外国人由来の細胞を国内で培養し国産と謳っている\r\n・動物由来成分が含まれる培地で培養している　\r\n\r\n原液・国産・安全の培養上清を使うことが、他社との差別化にもつながります。\r\n\r\n下記、一つでも当てはまるようでしたら、必ず良いご提案ができますので、\r\nご連絡くださいませ。\r\n\r\n・ヒト幹細胞培養上清を探している\r\n・100%原液を使いたい\r\n・ヒト脂肪細胞順化培養液エキスを使っている\r\n・培養液と培養上清の違いがわからない\r\n・日本製ではない\r\n・本当に品質の良いものを扱いたい\r\n\r\n私どもで取り扱う幹細胞培養上清に関して、詳しくはこちらをご覧ください。\r\nHP:www.m-gfp.com  \r\n\r\nお時間を10分ほどいただければ、\r\nお電話にてより詳細なご案内も可能です。\r\n\r\nまた、弊社では既存の生産ラインのままで、\r\nローコストでウルトラファインバブルを\r\n大量に製造できるポンプの取り扱いも行っております。\r\n\r\nこの機会にぜひご検討ください。\r\n\r\n最後までお読みいただきまして、ありがとうございます。\r\nご返信、お待ちしております。 \r\n---\r\n株式会社マインドシェア\r\nGrowth Factor Project\r\n林　輝実\r\n\r\nTEL:03-6823-6257　/　FAX:03-5232-0586\r\n〒108-0073東京都港区三田3-2-8Net2三田ビル6階\r\nHP：www.m-gfp.com\r\n加盟団体：国際抗老化再生医療学会（WAARM）',NULL,'2020-08-14 02:59:17','2020-08-14 02:59:17'),
	(11,NULL,NULL,NULL,13,1,'伊賀','皓平','イガ','コウヘイ','1600022','新宿区新宿','6-24-20　KDX新宿六丁目ビル9F','k.iga@kizna.co.jp','08048911557','株式会社メディファイン\r\nご担当者様\r\n\r\nはじめまして、絆ホールディングス株式会社の伊賀と申します。\r\n突然のご連絡にて失礼いたします。\r\n\r\nこの度、貴社の事業内容をHPで拝見させていただきまして、\r\n弊社の事業、業界ランキング調査「No.1リサーチ」によるNo.1表記のご活用が\r\n貴社にお役立ていただけるのではと思い、ご連絡をさせていただきました。\r\n\r\n【No.1リサーチの特徴】\r\n・広告、採用活動などでNo.1表記をご活用いただける調査\r\n・客観性の高いインターネットリサーチ\r\n・1位をご獲得いただいた際に成果とする完全成果報酬型\r\n\r\n【No.1リサーチでできること】\r\n・企業のオリジナルフレーズで第1位を取得\r\n・サービスや商品の強みをダイレクトに訴求（消費者とのマッチング）\r\n・ブランディング力の向上、競合との差別化\r\n\r\n【事例】\r\n第一三共ヘルスケア株式会社(サプリメント)：https://regain-suppli.jp/stc/sp/campaign/ad_PA/0001/default.aspx\r\nシックスセンスラボ株式会社(サプリメント)：https://www.sixthsenselab.jp/beauty_sleep/\r\n株式会社アトラス(スーパーフード)： https://www.delishorganics.jp/jp\r\nベストアメニティ株式会社(甘酒)：https://zakkokuhonke.jp/lp/ichinichi01.html\r\n\r\nご関心がございましたら、他社様の運用事例などをご紹介させていただけますと幸いです。\r\n以下の電話番号かメールでご連絡くださいませ。\r\n\r\nどうぞよろしくお願い申し上げます。\r\n\r\n絆ホールディングス株式会社　営業統括本部\r\n伊賀　皓平\r\nTEL：080-4891-1557\r\nMAIL：k.iga@kizna.co.jp',NULL,'2020-08-19 10:12:15','2020-08-19 10:12:15'),
	(12,NULL,NULL,4,NULL,1,'三田','晃生','ミタ','コウセイ',NULL,NULL,NULL,'kmita0905@gmail.com',NULL,'「MARITIME CBD ウォーター 500ml」についての問い合わせです。\r\n\r\n・CBDの原料はどこから仕入れたものでしょうか。\r\n・またCBD以外の含有成分（テルペン、CBN、CBG等）ありましたらご教示ください。\r\n・このCBDは合成でしょうか、天然でしょうか\r\n\r\n・本ウォーター、オイル共に、購入しようか悩んでおりますが、\r\n実際にどんな悩みを抱えている方が買うのでしょうか\r\n（やはり睡眠障害でしょうか）\r\n\r\nどうぞよろしくお願いいたします。\r\n\r\n三田',NULL,'2020-08-20 02:03:58','2020-08-20 02:03:58'),
	(13,NULL,NULL,NULL,13,1,'牧野','京子','マキノ','キョウコ','1500046','渋谷区松濤1-28-2','ワークコート渋谷松濤','makino@diamode.jp','0368762302','ご担当者様\r\n\r\nお世話になります。\r\nDIA MODE株式会社の牧野と申します。\r\n\r\n弊社はテレビ朝日様、AbemaTV様、サイバーエージェントグループ様などが主要取引先のタレント・Youtuber・インフルエンサーのソーシャルメディアマーケティング(SNS、インフルエンサーPR)、広告撮影製作、イベントの企画運営などに特化したPR会社です。\r\n\r\nPRTIMESで御社のHPを拝見させて頂き、弊社のサービスを用い御社に、お力添え出来ればと思いご連絡させていただきました。\r\nインフルエンサーを活用したSNS上での商品PR、イベントPRなどにご興味はありませんでしょうか？\r\n\r\n弊社はモデルのSNSでのインサイト(ユーザー情報)を全て握っております。\r\nしたがってアプローチしたい層にダイレクトに訴求を強くかけることが可能です。事務所所属タレント、フリーモデル共に低価格でご提案させていただき、結果にコミットできる自信があります。\r\n\r\nまた大手取引先との案件で培った、数値に基づくレポート作成など施策が終わった後のケアにも自信がございます。\r\n具体的な案や提案も可能ですので、一度お打ち合わせさせていただけないでしょうか？\r\nお手数ですが何卒ご確認のほどよろしくお願い致します。',NULL,'2020-08-20 06:54:22','2020-08-20 06:54:22'),
	(14,NULL,NULL,NULL,27,1,'長野','有里子','ナガノ','ユリコ','5410053','大阪市中央区本町2-5-7','2-5-7','ynagano@pasona-marketing.co.jp','0676366211','【アフターコロナにおける市場拡大、商品PR支援のご案内】\r\n突然のご連絡失礼致します。\r\nパソナマーケティングの長野と申します。\r\n\r\nこの度は貴社化粧品のPR、またオンラインショップへの\r\nお客様流入のご支援をさせて頂きたくご連絡いたしました。\r\n\r\n新型コロナウイルス下において、百貨店の営業自粛や店舗のテスター撤去を受け\r\nSNSの個人投稿や口コミサイトの情報からECサイトで購入する流れが拡大しております。\r\n\r\n当社は主にInstagramにてインフルエンサーを活用したSNS支援を行っております。\r\n美容に詳しいインフルエンサーによる投稿は、非常に信頼度が高く\r\n多くのエンゲージメントを集めており、今後ますます拡大すると考えております。\r\n\r\n【支援内容】\r\n・企業公式アカウントの運用代行・分析\r\n・貴社商品のイメージに合うインフルエンサーのPR投稿\r\n(様々なジャンルのインフルエンサー2万人の中から面接にてお選び頂けます)\r\n・インフルエンサーによるリアルのPR活動(店舗・イベント)\r\n\r\n事例紹介)大手アパレルメーカー様\r\n■課題:ECサイトの売上獲得の為、Instagramを開設したが、いいね数やフォロワーが増加しない\r\n■施策:ブランドターゲット層の20代女性をフォロワーに抱え\r\n物撮りが得意なインフルエンサーを5名起用。\r\n指定ハッシュタグ、URLを添付したストーリー投稿を継続的に行い\r\nブランドのファンか促進ECサイトへの流入施策を実施\r\n\r\n■結果:ストーリー投稿によるEC流入率も３～５％と高い数値を継続しており\r\nEC売上額はサービス導入前に比べ330％となった。\r\n\r\n【当社HP】\r\nインフルエンサーによるファン形成\r\nSNSマーケティングは広告から「ファン形成時代へ」\r\nhttp://www.pasona-marketing.co.jp/lp2#casestudy\r\n\r\nWEB会議にてこれまでご利用頂いた事例やプランのご案内をさせて頂ければ幸いです。\r\nどうぞよろしくお願いいたします。',NULL,'2020-08-20 07:35:39','2020-08-20 07:35:39'),
	(15,NULL,NULL,NULL,13,1,'山代','容平','ヤマシロ','ヨウヘイ','1631090','東京都新宿区','西新宿3-7-1新宿パークタワーセンターN30階','y.yamashiro@bljapan.com','0353263140','初めまして、BAN LOONG BIO TECHNOLOGY株式会社の山代と申します。\r\n今回、PR TIMESの記事を拝見し、連絡させて頂きました。\r\n弊社共、GMP認証を持った中国雲南省にある、雲南白葯グループにて、CBD原料の栽培から製造を行い、\r\n国内にてそのCBD原料を販売している会社でございます。\r\n御社製品や取組についてウェブサイト含め拝見させて頂き、大変感銘を受けました。\r\nもし宜しければ、一度、情報共有という形で、お話する事は出来ますでしょうか。直接お話するのは、このご時世、中々難しいと思いますので、ZOOMのような、リモートでの機会をぜひ、設けさせて頂ければと存じます。\r\n\r\nお手数おかけいたしますが、ぜひ、ご都合を伺えればと存じますので、\r\nご教示の程、宜しくお願い致します。',NULL,'2020-08-26 04:19:16','2020-08-26 04:19:16'),
	(16,NULL,NULL,NULL,13,1,'西岡','勇人','ニシオカ','ユウト','1500013','渋谷区桜丘町','２０－１, インフォスタワー二階','creativity9966@gmail.com','09083449966','ルーデンホールディングスの西岡勇人と申します。\r\n現在、ｃｂｄ製品、アイソレートの輸入事業をしております。 アメリカの信頼できる工場3社と提携しており、安定した供給を心がけております。 私自身がアメリカに10年間居たこともあり、工場との連携はもちろん、真正な書類と法律面に最新の注意を払っております。THCが検出されないように二重検査などもしております。\r\nご興味がございましたらご連絡ください。',NULL,'2020-08-26 06:45:11','2020-08-26 06:45:11'),
	(17,NULL,NULL,NULL,13,1,'石田','ゆり','イシダ','ユリ','1410022','品川区東五反田','5-21-4 2F','monicam@zero18.co.jp','0334416018','CBD\r\n広告宣伝ご担当者様\r\n\r\nお世話になります。\r\n株式会社０１８(ゼロワンエイト)の石田ゆりと申します。\r\n\r\n弊社はコスメ商品に特化した日本最大級のモニターサイト\r\n「モニキャン」を運営しております。\r\n\r\n今回、9月限定で【CBDオイル】を10～50個ほど\r\nご提供いただけましたら\r\nその他費用は一切かからずクチコミ施策を\r\nご実施いただける特別なご案内です。\r\n\r\n□monicam（モニキャン）\r\nhttps://monicam.jp/\r\n・クチコミ集約ページ　広告主：カネボウ化粧品様\r\nhttps://monicam.jp/fanpage/product/56\r\n\r\n■ 無料掲載のご案内 ■\r\n□monicam（モニキャン）概要　\r\n媒体概要、キャンペーン詳細は下記よりご確認くださいませ。\r\nhttps://drive.google.com/file/d/18xQmWYQPORS-T6BnW3ib3N0Xbx5GGknr/view\r\n\r\n+++++モニキャン通常実施費+++++\r\n掲載費30万円 ＋ 配布人数×2,000円\r\n\r\n―――実施スケジュール―――\r\n・募集期間：10/1(木)～11/8(日)\r\n・SNS投稿期間：11/14(土)～11/29(日)\r\n―――――――――――――――――\r\n【お申込み締切日】　9/15（火）12：00迄\r\n【お申込みメール】 monicam@zero18.co.jp\r\n※今回、30枠が埋まってしまった場合は11月のご掲載も承っております。\r\n\r\nぜひご検討いただけましたら幸いです。\r\nご質問やご不明点等ございましたらお気軽にお申し付けくださいませ。\r\n\r\n何卒よろしくお願い申し上げます。\r\n\r\n∞∞株式会社０１８ 会社概要資料∞∞\r\nhttps://drive.google.com/file/d/13n7CdMgWPEX4zOjdZZAo_UxhQuwlvgcd/view\r\n\r\n∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞\r\n株式会社０１８（ゼロワンエイト）\r\n〒141-0022　東京都品川区東五反田5丁目21-4-2F\r\nTEL:03-3441-6018\r\n【HP】https://zero18.co.jp/\r\n∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞',NULL,'2020-08-31 03:28:07','2020-08-31 03:28:07'),
	(18,NULL,NULL,NULL,14,1,'佐々木','千浩','ササキ','チヒロ',NULL,'横浜市鶴見区鶴見中央','3-3-37N-stage鶴見308','acsmoking420@gmail.com','08038377472','はじめまして。\r\n佐々木と申します。\r\n\r\n今後個人でネット販売から始めたいと考えておりますので、詳しく知りたいと思い、ご連絡させて頂きました。\r\n\r\n最小ロット及び価格等についてお伺いさせて頂ければ幸いです。\r\n\r\nご返信お待ちしております。\r\n\r\n佐々木',NULL,'2020-09-01 08:05:55','2020-09-01 08:05:55'),
	(19,NULL,NULL,NULL,13,1,'阿部','峻一朗','アベ','シュンイチロウ','1500044','渋谷区円山町','3－6 E・スペースタワー8階','shun_abe@fullspeed.co.jp','09035967363','株式会社メディファイン\r\nご担当者様\r\nお世話になります。\r\n\r\n株式会社フルスピードの阿部と申します。\r\nこの度ご連絡させて頂きましたのは、\r\n貴社サイト情報を拝見したためとなります。\r\n\r\n 貴社業界のアフィリエイト広告を運用する際の「リスク」\r\nを解消するツールを用意し、\r\n クリーンかつ効率よい広告運用に貢献できないかと考えております。\r\n\r\n 一度、お打ち合わせのご機会を頂けないでしょうか。\r\n不躾なご連絡となり、大変恐縮ではありますが、\r\nどうぞ、よろしくお願いいたします。\r\n\r\nチャットワークID：absn0613',NULL,'2020-09-02 09:12:32','2020-09-02 09:12:32'),
	(20,NULL,NULL,NULL,33,1,'氏房','幸子','ウジフサ','サチコ','7000811','岡山市北区番町','1-1-29 サンジェルマン番町303','tochio12@gmail.com','08054126738','友人から聞いてこちらのサイトに辿りつきました。高齢の父が癌のためウォーターの購入を検討しています。抗がん剤治療を始めるのですが\r\n\r\nどのくらいを目安に飲ませてみたらいいのでしょうか\r\n顧客様の例でも教えていただけたら幸いです\r\nよろしくお願いします。',NULL,'2020-09-04 00:46:08','2020-09-04 00:46:08'),
	(21,1,NULL,NULL,14,1,'Frederic','Vervaecke','フレデリック','ヴェルヴァーク','2220032','横浜市港北区大豆戸町171-1','ネイバーズ菊名311','djfre2000@hotmail.com','07026558129','テスト',NULL,'2020-09-04 04:23:24','2020-09-04 04:23:24'),
	(22,NULL,NULL,NULL,27,1,'藤井','裕真','フジイ','ユウマ','5420012','大阪市中央区谷町','7-1-33シティコープ谷町1002','linkingentertainment@gmail.com','08031115297','初めまして。\r\n大阪市内でヘルスケアスペースやBARを運営している藤井と申します。\r\n\r\n今回、CBDの販売にあたって卸売パッケージに興味があります。\r\n\r\n値段や諸条件を教えて頂きたいです。\r\nよろしくお願いいたします。\r\n\r\n藤井',NULL,'2020-09-05 02:01:12','2020-09-05 02:01:12'),
	(23,NULL,NULL,NULL,13,1,'柴田','有香','シバタ','ユカ','1006051','千代田区霞が関','３－２－５霞が関ビル２３階','y_shibata060@jtb.com','0367379322','株式会社メディファイン\r\nご担当者様\r\n\r\nいつも大変お世話になっております。JTBで法人営業を担当しております柴田有香と申します。\r\n\r\n今回貴社が販売しております、清涼飲料水「マリタイムCBDウォーター」に関する記事を拝見し\r\n御社製品の販路拡大にあたり弊社の「旅」というソリューションを活かして何かお力になれればと考え、こちらのフォームよりご連絡させていただいた次第です。\r\n\r\nつきましては是非一度弊社のプロモーション関連資料・他社事例を交え、WEB会議ができればと考えているのですがいかがでしょうか。\r\n\r\n安直なアイデアかとは存じますが、弊社とお付き合いのあるホテルでの客室レンタルや実店舗並びに空港でのポップアップ、その他各種メディアでの記事発信等多岐に渡るソリューションをご紹介させて頂ければ幸いです。\r\n ※ホテルでの食品・飲料サンプリングに関しても、\r\n現在進めている案件がございますので、他社様の事例も含めてご紹介ができればと考えております。\r\n\r\nお忙しい中恐縮ですが、\r\n貴社にて実施されているプロモーションに関してもお伺いしたく、是非一度意見交換の時間をいただけますと幸いです。\r\n\r\nご検討の程何卒よろしくお願い致します。\r\n\r\n柴田',NULL,'2020-09-09 01:41:10','2020-09-09 01:41:10'),
	(24,NULL,NULL,NULL,28,1,'高光','惠儀','タカミツ','ケイギ','6570821','神戸市灘区赤坂通','5-3-4','huiyitakamitsu@msn.com','08038604146','初めまして、佳莱株式会社　社長秘書の高光惠儀と申します。\r\n御社のCBD商品の卸売パッケージについての詳細を教えて頂きたいです。\r\n今朝10時ごろ御社に電話で問い合わせよう思って、掛けてみましたが、留守電でした。\r\n本日の午後3時以降神戸市にある御社へ訪問したいですが、\r\n可能でしょうか？\r\nご返答をお待ちしております。\r\n宜しくお願い致します。',NULL,'2020-09-14 03:21:49','2020-09-14 03:21:49'),
	(25,NULL,NULL,NULL,27,1,'田上','千恵','タガミ','チエ','5500015','大阪市西区南堀江','4-15-7プレジオ南堀江206号室','tenten.0610@icloud.com','09077606680','お世話になっております。\r\nEspoir de fleurの田上と申します。\r\n\r\n先日インテックス大阪で開催された国際化粧品展にて、CBDオイルと水を購入させて頂きました。\r\n\r\nオイルと水の禁忌はございますでしょうか？食べ合わせや飲み合わせでいけない物等ございましたら教えて下さい。\r\n\r\n宜しくお願い申し上げます。',NULL,'2020-09-17 00:57:06','2020-09-17 00:57:06'),
	(26,NULL,NULL,NULL,13,1,'菊地','優斗','キクチ','マサト','1340088','江戸川区西葛西5-2-3','NEXTAGE西葛西5F','client@rentracks.co.jp','05054726319','株式会社メディファイン\r\nWeb集客ご担当者様\r\n\r\nお世話になっております。\r\n株式会社レントラックスの菊地と申します。\r\n\r\n先程は、お忙しい中お時間いただきありがとうございました。\r\n\r\n弊社は、東証マザーズ市場に上場しておりまして\r\n主に完全成果報酬型広告のインターネット広告代理店業(ASP)\r\nを行っている企業でございます。\r\n \r\n弊社概要について記載いたしましたので、\r\nご確認のほど何卒宜しくお願い申し上げます。\r\n\r\nーーーーーーーーーーーーーーーーーーーーーーーー\r\n\r\n●完全成果報酬型サービスとなっておりまして、\r\nWEB広告において「導入費」「月額費」「施策費」は頂いておらず、\r\n違約金等も一切発生いたしません。\r\n\r\n●貴社の広告がクリックされても手数料は発生致しませんので、\r\n商品/サービスの認知度を高める上でも有効です。\r\n\r\n●広告面では特に、YouTube、ラップ、YDN、GDNに\r\n強いメディア様もいらっしゃいまして、多面的な獲得も可能でございます。\r\n※YouTubeでは単月2億円以上の販売実績もございます。\r\n\r\n●サービスの導入検討に関わらず、\r\n10分程度にてWEB集客の動向について\r\nお話しをさせて頂くだけで構いません。\r\n\r\nその中で有益な情報をお伝えし、\r\nWEB収益を最大限に上げるキッカケになれれば幸いでございます。\r\n\r\nーーーーーーーーーーーーーーーーーーーーーーーー\r\n\r\n■ 弊社サービスの3つの特徴\r\n\r\n1. 導入費用・固定費・施策運用費等は一切頂きません！\r\n\r\n弊社は完全成果報酬型ですので、\r\n他社ASPをご利用いただくよりもノーリスクで取り組んで頂けます。\r\n\r\n2. 一般的な公募はしておりません！\r\n\r\nご登録頂いているサイト運営者様の多くは\r\n塾などの既存の方からのご紹介でございます。\r\nそのため、有力なサイト運営者様や法人のメディア様が多数いらっしゃいます。\r\n\r\n3. SEOの検索順位の高いアフィリエイター様、\r\n実績の高いアド媒体様にご登録頂いております！\r\n\r\n上位表示されること、露出を増やすことにより、\r\nより効率的に掲載面が増やすことが出来ます。\r\nそのためユーザー様からの良質な申し込みが期待できます。\r\n\r\nーーーーーーーーーーーー ーーーーーーーーーーーー\r\n\r\nご覧頂き、サービスの詳細についてお伺い頂ける場合は\r\nご一報頂けますと幸いでございます。\r\n\r\n※リモートワーク中のため、ご連絡頂ける際は\r\nメール又は携帯050-5472-6319にご連絡頂けますと幸いでございます。\r\n\r\nご質問等ございましたら遠慮なくご相談ください。\r\nご確認の程よろしくお願いいたします。\r\n\r\n○●----------------------------------------------------●○\r\n株式会社レントラックス東京都江戸川区西葛西5‐2‐3　NEXTAGE西葛西5F　\r\n（https://www.rentracks.co.jp/）\r\nメディア事業本部\r\n菊地　優斗\r\nメールアドレス：client@rentracks.co.jp\r\nTEL：03‐3878-4159\r\nFAX：03-5696-0155\r\n※恐縮ではございますが、\r\nリモートワーク実施につき電話対応いたしかねますので、\r\nメールにてご連絡をいただけますと幸いでございます。\r\n○●----------------------------------------------------●○',NULL,'2020-09-18 06:27:10','2020-09-18 06:27:10');

/*!40000 ALTER TABLE `plg_contact_management4_contact` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table plg_contact_management4_contact_status
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plg_contact_management4_contact_status`;

CREATE TABLE `plg_contact_management4_contact_status` (
  `id` smallint(5) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) unsigned NOT NULL,
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `plg_contact_management4_contact_status` WRITE;
/*!40000 ALTER TABLE `plg_contact_management4_contact_status` DISABLE KEYS */;

INSERT INTO `plg_contact_management4_contact_status` (`id`, `name`, `sort_no`, `discriminator_type`)
VALUES
	(1,'未対応',1,'contactstatus'),
	(2,'対応中',2,'contactstatus'),
	(99,'対応完了',99,'contactstatus');

/*!40000 ALTER TABLE `plg_contact_management4_contact_status` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table plg_coupon
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plg_coupon`;

CREATE TABLE `plg_coupon` (
  `coupon_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `coupon_cd` varchar(20) DEFAULT NULL,
  `coupon_type` smallint(6) DEFAULT NULL,
  `coupon_name` varchar(50) DEFAULT NULL,
  `discount_type` smallint(6) DEFAULT NULL,
  `coupon_use_time` int(11) DEFAULT NULL,
  `discount_price` decimal(12,2) unsigned DEFAULT 0.00,
  `discount_rate` decimal(10,0) unsigned DEFAULT 0,
  `enable_flag` tinyint(1) NOT NULL DEFAULT 1,
  `available_from_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `available_to_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `visible` tinyint(1) NOT NULL DEFAULT 1,
  `coupon_member` tinyint(1) NOT NULL DEFAULT 0,
  `coupon_lower_limit` decimal(12,2) unsigned DEFAULT 0.00,
  `coupon_release` int(11) NOT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`coupon_id`),
  UNIQUE KEY `UNIQ_755A31039C2A7D91` (`coupon_cd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `plg_coupon` WRITE;
/*!40000 ALTER TABLE `plg_coupon` DISABLE KEYS */;

INSERT INTO `plg_coupon` (`coupon_id`, `coupon_cd`, `coupon_type`, `coupon_name`, `discount_type`, `coupon_use_time`, `discount_price`, `discount_rate`, `enable_flag`, `available_from_date`, `available_to_date`, `visible`, `coupon_member`, `coupon_lower_limit`, `coupon_release`, `create_date`, `update_date`, `discriminator_type`)
VALUES
	(1,'MARITIME10',3,'初回10％割引オファー',2,99998,NULL,10,1,'2020-06-30 15:00:00','2024-12-30 15:00:00',1,0,NULL,99999,'2020-08-03 02:10:25','2020-09-09 15:37:01','coupon'),
	(2,'LINE10',3,'LINE用10%割引きオファー',2,99998,NULL,10,1,'2020-08-04 15:00:00','2021-12-30 15:00:00',1,0,NULL,99999,'2020-08-05 09:45:12','2020-09-09 09:39:29','coupon'),
	(3,'SHINDAN',3,'オンライン診断 10%割引',2,999,NULL,10,1,'2020-08-16 15:00:00','2021-12-30 15:00:00',1,0,NULL,999,'2020-08-17 15:48:54','2020-09-09 09:39:19','coupon'),
	(4,'JAPANLP',3,'日本語ランディングページ経由の10%割引',2,999,NULL,10,1,'2020-08-17 15:00:00','2021-12-30 15:00:00',1,0,NULL,999,'2020-08-18 13:11:21','2020-09-09 09:39:02','coupon'),
	(5,'INSTA10',3,'インスタグラム経由での10%割引',2,9999,NULL,10,1,'2020-08-26 15:00:00','2021-12-30 15:00:00',1,0,0.00,9999,'2020-08-27 08:34:53','2020-09-09 09:38:49','coupon'),
	(6,'REVIEW15',3,'レビュー15％割引クーポン',2,9999,NULL,15,1,'2020-09-17 15:00:00','2024-12-30 15:00:00',1,0,NULL,9999,'2020-09-18 06:18:43','2020-09-18 06:18:43','coupon');

/*!40000 ALTER TABLE `plg_coupon` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table plg_coupon_detail
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plg_coupon_detail`;

CREATE TABLE `plg_coupon_detail` (
  `coupon_detail_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `coupon_id` int(10) unsigned DEFAULT NULL,
  `product_id` int(10) unsigned DEFAULT NULL,
  `category_id` int(10) unsigned DEFAULT NULL,
  `coupon_type` smallint(6) DEFAULT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT 1,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`coupon_detail_id`),
  KEY `IDX_7B9D14166C5951B` (`coupon_id`),
  KEY `IDX_7B9D1414584665A` (`product_id`),
  KEY `IDX_7B9D14112469DE2` (`category_id`),
  CONSTRAINT `FK_7B9D14112469DE2` FOREIGN KEY (`category_id`) REFERENCES `dtb_category` (`id`),
  CONSTRAINT `FK_7B9D1414584665A` FOREIGN KEY (`product_id`) REFERENCES `dtb_product` (`id`),
  CONSTRAINT `FK_7B9D14166C5951B` FOREIGN KEY (`coupon_id`) REFERENCES `plg_coupon` (`coupon_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table plg_coupon_order
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plg_coupon_order`;

CREATE TABLE `plg_coupon_order` (
  `coupon_order_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `coupon_id` int(10) unsigned NOT NULL,
  `coupon_cd` varchar(20) DEFAULT NULL,
  `coupon_name` varchar(50) DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `order_id` int(10) unsigned NOT NULL,
  `pre_order_id` varchar(255) DEFAULT NULL,
  `order_date` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `order_item_id` int(10) unsigned DEFAULT NULL,
  `discount` decimal(12,2) unsigned NOT NULL DEFAULT 0.00,
  `visible` tinyint(1) NOT NULL DEFAULT 1,
  `order_change_status` tinyint(1) NOT NULL DEFAULT 1,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL,
  PRIMARY KEY (`coupon_order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `plg_coupon_order` WRITE;
/*!40000 ALTER TABLE `plg_coupon_order` DISABLE KEYS */;

INSERT INTO `plg_coupon_order` (`coupon_order_id`, `coupon_id`, `coupon_cd`, `coupon_name`, `user_id`, `email`, `order_id`, `pre_order_id`, `order_date`, `order_item_id`, `discount`, `visible`, `order_change_status`, `create_date`, `update_date`, `discriminator_type`)
VALUES
	(12,1,'MARITIME10','初回10％割引オファー',4,NULL,80,'1c94bf800574b5a41aa2b07d625f1e0b0b80cc1a',NULL,NULL,1270.00,1,0,'2020-08-10 10:07:19','2020-08-10 10:07:19','couponorder'),
	(13,1,'MARITIME10','初回10％割引オファー',4,NULL,81,'a6557d4f7048fc64433922d72e18615928df43e1',NULL,NULL,518.00,1,0,'2020-08-10 10:07:40','2020-08-10 10:07:40','couponorder'),
	(18,1,'MARITIME10','初回10％割引オファー',1,NULL,84,'97c68b34c1dc8a5d4bc2d7e7ee9233c60d9b9d4b',NULL,NULL,467.00,1,0,'2020-08-13 11:24:41','2020-08-13 11:24:41','couponorder'),
	(19,1,'MARITIME10','初回10％割引オファー',1,NULL,85,'e819ef3ce95012d030991ea7679a39303b387e07',NULL,NULL,467.00,1,0,'2020-08-13 12:05:20','2020-08-13 12:05:20','couponorder'),
	(20,4,'JAPANLP','日本語ランディングページ経由の10%割引',8,NULL,89,'69b08410253ed92c60961cc98c9fa9cbfe297288',NULL,NULL,1584.00,1,0,'2020-08-18 17:25:21','2020-08-18 17:26:08','couponorder'),
	(21,2,'LINE10','LINE用10%割引きオファー',15,NULL,110,'f1745fbe2f17b8462b2b88a1f8f00b0a1fbf310d','2020-09-06 12:40:15',NULL,4617.00,1,0,'2020-09-06 12:38:05','2020-09-06 12:40:15','couponorder'),
	(22,1,'MARITIME10','初回10％割引オファー',NULL,'eightbear+ml@gmail.com',113,'89afae9bcc609a8694cb269d17427eb06ae57e00',NULL,NULL,125.00,1,0,'2020-09-09 15:24:40','2020-09-09 15:24:40','couponorder'),
	(23,1,'MARITIME10','初回10％割引オファー',NULL,'eightbear+ml@gmail.com',114,'514f5b8ee0a20863207f1fb84aaec459c52fcd61','2020-09-09 15:37:01',NULL,752.00,1,0,'2020-09-09 15:28:58','2020-09-09 15:37:01','couponorder');

/*!40000 ALTER TABLE `plg_coupon_order` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table plg_gmo_payment_gateway_config
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plg_gmo_payment_gateway_config`;

CREATE TABLE `plg_gmo_payment_gateway_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `connect_server_type` int(10) unsigned NOT NULL,
  `server_url` varchar(255) NOT NULL,
  `kanri_server_url` varchar(255) NOT NULL,
  `site_id` varchar(16) NOT NULL,
  `site_pass` varchar(16) NOT NULL,
  `shop_id` varchar(16) NOT NULL,
  `shop_pass` varchar(16) NOT NULL,
  `card_regist_flg` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `plg_gmo_payment_gateway_config` WRITE;
/*!40000 ALTER TABLE `plg_gmo_payment_gateway_config` DISABLE KEYS */;

INSERT INTO `plg_gmo_payment_gateway_config` (`id`, `connect_server_type`, `server_url`, `kanri_server_url`, `site_id`, `site_pass`, `shop_id`, `shop_pass`, `card_regist_flg`)
VALUES
	(1,2,'https://p01.mul-pay.jp/payment/','https://k01.mul-pay.jp/kanri/','mst2000023988','czsmw5mk','9200002712272','br59be55',1);

/*!40000 ALTER TABLE `plg_gmo_payment_gateway_config` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table plg_gmo_payment_gateway_member
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plg_gmo_payment_gateway_member`;

CREATE TABLE `plg_gmo_payment_gateway_member` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned NOT NULL,
  `member_id` longtext DEFAULT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `plg_gmo_payment_gateway_member` WRITE;
/*!40000 ALTER TABLE `plg_gmo_payment_gateway_member` DISABLE KEYS */;

INSERT INTO `plg_gmo_payment_gateway_member` (`id`, `customer_id`, `member_id`, `create_date`, `update_date`)
VALUES
	(1,1,'a14e0fac50668a5fda28e33b46cd906c180617d1','2020-07-10 06:18:27','2020-07-10 06:18:27'),
	(2,15,'06e4e7538eff5759eb0442d26210c6f919730c35','2020-09-06 12:40:18','2020-09-06 12:40:18');

/*!40000 ALTER TABLE `plg_gmo_payment_gateway_member` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table plg_gmo_payment_gateway_order_payment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plg_gmo_payment_gateway_order_payment`;

CREATE TABLE `plg_gmo_payment_gateway_order_payment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned NOT NULL,
  `memo01` longtext DEFAULT NULL,
  `memo02` longtext DEFAULT NULL,
  `memo03` longtext DEFAULT NULL,
  `memo04` longtext DEFAULT NULL,
  `memo05` longtext DEFAULT NULL,
  `memo06` longtext DEFAULT NULL,
  `memo07` longtext DEFAULT NULL,
  `memo08` longtext DEFAULT NULL,
  `memo09` longtext DEFAULT NULL,
  `memo10` longtext DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `plg_gmo_payment_gateway_order_payment` WRITE;
/*!40000 ALTER TABLE `plg_gmo_payment_gateway_order_payment` DISABLE KEYS */;

INSERT INTO `plg_gmo_payment_gateway_order_payment` (`id`, `order_id`, `memo01`, `memo02`, `memo03`, `memo04`, `memo05`, `memo06`, `memo07`, `memo08`, `memo09`, `memo10`)
VALUES
	(11,11,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(12,12,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(13,13,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(14,14,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\RakutenPay',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(15,17,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(16,18,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(17,19,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(18,20,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(19,21,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(20,22,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(21,23,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(22,24,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(23,25,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(24,26,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(25,27,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(26,28,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(27,29,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(28,30,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(29,31,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(30,32,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(31,33,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(32,34,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(33,35,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(34,36,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(35,37,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(36,38,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(37,39,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(38,40,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(39,41,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(40,42,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(41,43,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(42,44,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(43,45,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(44,46,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(45,47,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(46,48,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(47,56,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(48,68,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\RakutenPay',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(49,69,'{\"payment_type\":\"0\",\"token\":\"88fe50d8c635e176d2bb15af11fb7a0aaf83c16a077c3b9a474f2c9b6bfb58b3\",\"mask_card_no\":\"*************936\",\"expire_month\":\"06\",\"expire_year\":\"21\",\"card_name1\":\"Frederic\",\"card_name2\":\"Vervaecke\",\"security_code\":\"***\",\"credit_pay_methods\":\"1-0\",\"register_card\":true,\"CardSeq\":null,\"credit_pay_methods2\":null,\"Convenience\":\"\"}',NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(50,70,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(51,71,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(52,72,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(53,73,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(54,74,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(55,75,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(56,76,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(57,77,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(58,78,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(59,79,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(60,80,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(61,81,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(62,82,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(63,83,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(64,84,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(65,85,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(66,86,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(67,87,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(68,88,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(69,89,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(70,90,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(71,91,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(72,92,'{\"payment_type\":\"0\",\"token\":\"ac36b9da7253827f810c1accb6e9e5bbe0640aa0d5b6715bcbff010722b4fcbd\",\"mask_card_no\":\"*************217\",\"expire_month\":\"08\",\"expire_year\":\"23\",\"card_name1\":\"KAORI\",\"card_name2\":\"SEKI\",\"security_code\":\"***\",\"credit_pay_methods\":\"1-0\",\"register_card\":false,\"CardSeq\":null,\"credit_pay_methods2\":null,\"Convenience\":\"\"}',NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(73,93,'{\"payment_type\":\"0\",\"token\":\"95c9ad6db497db742fc536c4498cc44d7ae0b4a5ee163b778622c86bc3cda123\",\"mask_card_no\":\"*************901\",\"expire_month\":\"03\",\"expire_year\":\"24\",\"card_name1\":\"KAORI\",\"card_name2\":\"SEKI\",\"security_code\":\"***\",\"credit_pay_methods\":\"1-0\",\"register_card\":false,\"CardSeq\":null,\"credit_pay_methods2\":null,\"Convenience\":\"\"}',NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(74,94,'{\"payment_type\":\"0\",\"token\":\"218bc09e5e13e5156351e9ee30bc91f2249c3fd80066352cda02886b645fba61\",\"mask_card_no\":\"*************118\",\"expire_month\":\"06\",\"expire_year\":\"24\",\"card_name1\":\"KAORI\",\"card_name2\":\"SEKI\",\"security_code\":\"***\",\"credit_pay_methods\":\"1-0\",\"register_card\":false,\"CardSeq\":null,\"credit_pay_methods2\":null,\"Convenience\":\"\"}',NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(75,95,'{\"payment_type\":\"0\",\"token\":\"d61880a1b0838954694be26d0eeff0d35768418e7df1d244e2dd6e93b7f865a8\",\"mask_card_no\":\"*************118\",\"expire_month\":\"06\",\"expire_year\":\"24\",\"card_name1\":\"KAORI\",\"card_name2\":\"SEKI\",\"security_code\":\"***\",\"credit_pay_methods\":\"1-0\",\"register_card\":false,\"CardSeq\":null,\"credit_pay_methods2\":null,\"Convenience\":\"\"}',NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(76,96,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(77,97,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(78,98,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(79,99,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(80,100,'{\"payment_type\":\"0\",\"token\":\"cac21b7447c92ebc7a9ba4f53ddcf734a9b0a26b5684f9581902c164a1bbdcce\",\"mask_card_no\":\"*************981\",\"expire_month\":\"08\",\"expire_year\":\"25\",\"card_name1\":\"Frederic\",\"card_name2\":\"Vervaecke\",\"security_code\":\"***\",\"credit_pay_methods\":\"1-0\",\"register_card\":true,\"CardSeq\":null,\"credit_pay_methods2\":null,\"Convenience\":\"\"}','{\"Approve\":{\"name\":\"\\u627f\\u8a8d\\u756a\\u53f7\",\"value\":\"0649950\"},\"title\":{\"value\":\"1\",\"name\":\"\\u30af\\u30ec\\u30b8\\u30c3\\u30c8\\u6c7a\\u6e08\"},\"lf\":{\"name\":\"\",\"value\":\"\"}}','Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard','14','{\"AccessID\":\"025a83b1f5a7b00fd37f9a90d35c0332\",\"AccessPass\":\"052b904a2fe81ba2330b2196a1dc4d47\",\"OrderID\":\"100-28145744\",\"Amount\":\"1728\",\"JobCd\":\"VOID\",\"action_status\":6,\"pay_status\":14,\"ACS\":\"0\",\"Forward\":\"2S49631\",\"Method\":\"1\",\"PayTimes\":\"\",\"Approve\":\"0016216\",\"TranID\":\"2008281825602257249993820002\",\"TranDate\":\"20200828182511\",\"CheckString\":\"718f343c0140c153876a72446388c8a7\"}',NULL,NULL,NULL,'[{\"2020-08-28 14:57:44\":{\"AccessID\":\"025a83b1f5a7b00fd37f9a90d35c0332\",\"AccessPass\":\"052b904a2fe81ba2330b2196a1dc4d47\",\"OrderID\":\"100-28145744\",\"Amount\":\"1728\",\"JobCd\":\"CAPTURE\",\"action_status\":1,\"pay_status\":\"\"}},{\"2020-08-28 14:57:46\":{\"ACS\":\"0\",\"OrderID\":\"100-28145744\",\"Forward\":\"2S49631\",\"Method\":\"1\",\"PayTimes\":\"\",\"Approve\":\"0649950\",\"TranID\":\"2008281457602257249993810001\",\"TranDate\":\"20200828145745\",\"CheckString\":\"718f343c0140c153876a72446388c8a7\",\"Amount\":\"1728\",\"JobCd\":\"CAPTURE\",\"action_status\":3,\"pay_status\":13}},{\"2020-08-28 18:25:11\":{\"AccessID\":\"025a83b1f5a7b00fd37f9a90d35c0332\",\"AccessPass\":\"052b904a2fe81ba2330b2196a1dc4d47\",\"Forward\":\"2S49631\",\"Approve\":\"0016216\",\"TranID\":\"2008281825602257249993820002\",\"TranDate\":\"20200828182511\",\"JobCd\":\"VOID\",\"action_status\":6,\"pay_status\":14}}]',NULL),
	(81,101,'{\"payment_type\":\"1\",\"token\":null,\"mask_card_no\":\"*************981\",\"expire_month\":\"08\",\"expire_year\":\"25\",\"card_name1\":\"Frederic Vervaecke\",\"card_name2\":null,\"security_code\":null,\"credit_pay_methods\":\"1-0\",\"register_card\":false,\"CardSeq\":\"0\",\"credit_pay_methods2\":\"1-0\",\"Convenience\":\"\"}',NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(82,102,'{\"payment_type\":\"1\",\"token\":null,\"mask_card_no\":\"*************981\",\"expire_month\":\"08\",\"expire_year\":\"25\",\"card_name1\":\"Frederic Vervaecke\",\"card_name2\":null,\"security_code\":null,\"credit_pay_methods\":\"1-0\",\"register_card\":false,\"CardSeq\":\"0\",\"credit_pay_methods2\":\"1-0\",\"Convenience\":\"\"}','{\"Approve\":{\"name\":\"\\u627f\\u8a8d\\u756a\\u53f7\",\"value\":\"0651230\"},\"title\":{\"value\":\"1\",\"name\":\"\\u30af\\u30ec\\u30b8\\u30c3\\u30c8\\u6c7a\\u6e08\"},\"lf\":{\"name\":\"\",\"value\":\"\"}}','Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard','14','{\"AccessID\":\"bf2c77ea351540fedf89fc305aaf194c\",\"AccessPass\":\"45f00aa1c10530d30fee5b4317044759\",\"OrderID\":\"102-28151748\",\"Amount\":\"6998\",\"JobCd\":\"VOID\",\"CardSeq\":\"0\",\"action_status\":6,\"pay_status\":14,\"ACS\":\"0\",\"Forward\":\"2S49631\",\"Method\":\"1\",\"PayTimes\":\"\",\"Approve\":\"0016077\",\"TranID\":\"2008281824602257249993830001\",\"TranDate\":\"20200828182438\",\"CheckString\":\"30b8734eb286416b8f330315fe19d115\",\"ClientField1\":\"\",\"ClientField2\":\"\",\"ClientField3\":\"EC-CUBE4(1.0.4)\",\"Status\":\"VOID\",\"ProcessDate\":\"20200828182438\",\"ItemCode\":\"0000990\",\"Tax\":\"0\",\"SiteID\":\"mst2000023988\",\"MemberID\":\"a14e0fac50668a5fda28e33b46cd906c180617d1\",\"CardNo\":\"************7981\",\"Expire\":\"2508\"}',NULL,NULL,NULL,'[{\"2020-08-28 15:17:49\":{\"AccessID\":\"bf2c77ea351540fedf89fc305aaf194c\",\"AccessPass\":\"45f00aa1c10530d30fee5b4317044759\",\"OrderID\":\"102-28151748\",\"Amount\":\"6998\",\"JobCd\":\"CAPTURE\",\"CardSeq\":\"0\",\"action_status\":1,\"pay_status\":\"\"}},{\"2020-08-28 15:17:50\":{\"ACS\":\"0\",\"OrderID\":\"102-28151748\",\"Forward\":\"2S49631\",\"Method\":\"1\",\"PayTimes\":\"\",\"Approve\":\"0651230\",\"TranID\":\"2008281517602257249993820001\",\"TranDate\":\"20200828151750\",\"CheckString\":\"30b8734eb286416b8f330315fe19d115\",\"ClientField1\":\"\",\"ClientField2\":\"\",\"ClientField3\":\"EC-CUBE4(1.0.4)\",\"Amount\":\"6998\",\"JobCd\":\"CAPTURE\",\"CardSeq\":\"0\",\"action_status\":3,\"pay_status\":13}},{\"2020-08-28 18:24:38\":{\"AccessID\":\"bf2c77ea351540fedf89fc305aaf194c\",\"AccessPass\":\"45f00aa1c10530d30fee5b4317044759\",\"Forward\":\"2S49631\",\"Approve\":\"0016077\",\"TranID\":\"2008281824602257249993830001\",\"TranDate\":\"20200828182438\",\"JobCd\":\"VOID\",\"action_status\":6,\"pay_status\":14}},{\"2020-08-28 18:25:30\":{\"OrderID\":\"102-28151748\",\"Status\":\"VOID\",\"ProcessDate\":\"20200828182438\",\"JobCd\":\"VOID\",\"AccessID\":\"bf2c77ea351540fedf89fc305aaf194c\",\"AccessPass\":\"45f00aa1c10530d30fee5b4317044759\",\"ItemCode\":\"0000990\",\"Amount\":\"6998\",\"Tax\":\"0\",\"SiteID\":\"mst2000023988\",\"MemberID\":\"a14e0fac50668a5fda28e33b46cd906c180617d1\",\"CardNo\":\"************7981\",\"Expire\":\"2508\",\"Method\":\"1\",\"PayTimes\":\"\",\"Forward\":\"2S49631\",\"TranID\":\"2008281824602257249993830001\",\"Approve\":\"0016077\",\"ClientField1\":\"\",\"ClientField2\":\"\",\"ClientField3\":\"EC-CUBE4(1.0.4)\",\"action_status\":6,\"pay_status\":14}}]',NULL),
	(83,103,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(84,104,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(85,105,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(86,106,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(87,107,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(88,108,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(89,109,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(90,110,'{\"payment_type\":\"0\",\"token\":\"29e7300ea3b031a5bcc037a405899e4f27c377d9533507ea8bf659152d674bc9\",\"mask_card_no\":\"************004\",\"expire_month\":\"11\",\"expire_year\":\"21\",\"card_name1\":\"TOMOYOSHI\",\"card_name2\":\"KUMAZAWA\",\"security_code\":\"****\",\"credit_pay_methods\":\"1-0\",\"register_card\":true,\"CardSeq\":null,\"credit_pay_methods2\":null,\"Convenience\":\"\"}','{\"Approve\":{\"name\":\"\\u627f\\u8a8d\\u756a\\u53f7\",\"value\":\"0000061\"},\"title\":{\"value\":\"1\",\"name\":\"\\u30af\\u30ec\\u30b8\\u30c3\\u30c8\\u6c7a\\u6e08\"},\"lf\":{\"name\":\"\",\"value\":\"\"}}','Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard','13','{\"AccessID\":\"2a997980dd86a2ca1197534976d16a8e\",\"AccessPass\":\"b91002b31390c109f312ae4262895f46\",\"OrderID\":\"110-06214015\",\"Amount\":\"41553\",\"JobCd\":\"CAPTURE\",\"action_status\":6,\"pay_status\":13,\"ACS\":\"0\",\"Forward\":\"2a99661\",\"Method\":\"1\",\"PayTimes\":\"\",\"Approve\":\"0000061\",\"TranID\":\"2009062140602257249993830002\",\"TranDate\":\"20200906214017\",\"CheckString\":\"fb10991c3c6c77d3c3299fcd4ef7f8fb\",\"Status\":\"CAPTURE\",\"ProcessDate\":\"20200906214017\",\"ItemCode\":\"0000990\",\"Tax\":\"0\",\"SiteID\":\"\",\"MemberID\":\"\",\"CardNo\":\"***********2004\",\"Expire\":\"2111\",\"ClientField1\":\"\",\"ClientField2\":\"\",\"ClientField3\":\"EC-CUBE4(1.0.4)\"}',NULL,NULL,NULL,'[{\"2020-09-06 21:40:16\":{\"AccessID\":\"2a997980dd86a2ca1197534976d16a8e\",\"AccessPass\":\"b91002b31390c109f312ae4262895f46\",\"OrderID\":\"110-06214015\",\"Amount\":\"41553\",\"JobCd\":\"CAPTURE\",\"action_status\":1,\"pay_status\":\"\"}},{\"2020-09-06 21:40:18\":{\"ACS\":\"0\",\"OrderID\":\"110-06214015\",\"Forward\":\"2a99661\",\"Method\":\"1\",\"PayTimes\":\"\",\"Approve\":\"0000061\",\"TranID\":\"2009062140602257249993830002\",\"TranDate\":\"20200906214017\",\"CheckString\":\"fb10991c3c6c77d3c3299fcd4ef7f8fb\",\"Amount\":\"41553\",\"JobCd\":\"CAPTURE\",\"action_status\":3,\"pay_status\":13}},{\"2020-09-07 12:25:31\":{\"OrderID\":\"110-06214015\",\"Status\":\"CAPTURE\",\"ProcessDate\":\"20200906214017\",\"JobCd\":\"CAPTURE\",\"AccessID\":\"2a997980dd86a2ca1197534976d16a8e\",\"AccessPass\":\"b91002b31390c109f312ae4262895f46\",\"ItemCode\":\"0000990\",\"Amount\":\"41553\",\"Tax\":\"0\",\"SiteID\":\"\",\"MemberID\":\"\",\"CardNo\":\"***********2004\",\"Expire\":\"2111\",\"Method\":\"1\",\"PayTimes\":\"\",\"Forward\":\"2a99661\",\"TranID\":\"2009062140602257249993830002\",\"Approve\":\"0000061\",\"ClientField1\":\"\",\"ClientField2\":\"\",\"ClientField3\":\"EC-CUBE4(1.0.4)\",\"action_status\":6,\"pay_status\":13}}]',NULL),
	(91,111,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(92,112,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(93,113,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(94,114,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(95,115,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(96,116,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(97,117,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(98,118,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(99,119,NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `plg_gmo_payment_gateway_order_payment` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table plg_gmo_payment_gateway_payment_method
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plg_gmo_payment_gateway_payment_method`;

CREATE TABLE `plg_gmo_payment_gateway_payment_method` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `payment_id` int(10) unsigned NOT NULL,
  `payment_method` longtext NOT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `memo01` longtext DEFAULT NULL,
  `memo02` longtext DEFAULT NULL,
  `memo03` longtext DEFAULT NULL,
  `memo04` longtext DEFAULT NULL,
  `memo05` longtext DEFAULT NULL,
  `memo06` longtext DEFAULT NULL,
  `memo07` longtext DEFAULT NULL,
  `memo08` longtext DEFAULT NULL,
  `memo09` longtext DEFAULT NULL,
  `memo10` longtext DEFAULT NULL,
  `plugin_code` longtext DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `plg_gmo_payment_gateway_payment_method` WRITE;
/*!40000 ALTER TABLE `plg_gmo_payment_gateway_payment_method` DISABLE KEYS */;

INSERT INTO `plg_gmo_payment_gateway_payment_method` (`id`, `payment_id`, `payment_method`, `create_date`, `update_date`, `memo01`, `memo02`, `memo03`, `memo04`, `memo05`, `memo06`, `memo07`, `memo08`, `memo09`, `memo10`, `plugin_code`)
VALUES
	(1,5,'クレジット決済','2020-05-14 22:31:28','2020-07-02 03:02:53',NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard',NULL,'{\"JobCd\":\"CAPTURE\",\"credit_pay_methods\":[\"1-0\"],\"use_securitycd\":1,\"use_securitycd_option\":0,\"TdFlag\":0,\"TdTenantName\":null,\"order_mail_title1\":\"\\u304a\\u652f\\u6255\\u3044\\u306b\\u3064\\u3044\\u3066\",\"order_mail_body1\":null,\"ClientField1\":null,\"ClientField2\":null}',NULL,NULL,NULL,NULL,NULL,'GmoPaymentGateway4'),
	(2,6,'コンビニ決済','2020-05-14 22:31:28','2020-05-14 22:31:28',NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\Cvs',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'GmoPaymentGateway4'),
	(3,7,'Pay-easy決済(銀行ATM)','2020-05-14 22:31:28','2020-05-14 22:31:28',NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\PayEasyAtm',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'GmoPaymentGateway4'),
	(4,8,'Pay-easy決済(ネットバンク)','2020-05-14 22:31:28','2020-05-14 22:31:28',NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\PayEasyNet',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'GmoPaymentGateway4'),
	(5,9,'auかんたん決済','2020-05-14 22:31:28','2020-05-14 22:31:28',NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CarAu',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'GmoPaymentGateway4'),
	(6,10,'ドコモケータイ払い','2020-05-14 22:31:28','2020-05-14 22:31:28',NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CarDocomo',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'GmoPaymentGateway4'),
	(7,11,'ソフトバンクまとめて支払い','2020-05-14 22:31:28','2020-05-14 22:31:28',NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\CarSoftbank',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'GmoPaymentGateway4'),
	(8,12,'楽天ペイ','2020-05-14 22:31:28','2020-05-14 22:31:28',NULL,NULL,'Plugin\\GmoPaymentGateway4\\Service\\Method\\RakutenPay',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'GmoPaymentGateway4');

/*!40000 ALTER TABLE `plg_gmo_payment_gateway_payment_method` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table plg_mailmaga_send_history
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plg_mailmaga_send_history`;

CREATE TABLE `plg_mailmaga_send_history` (
  `send_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `creator_id` int(10) unsigned DEFAULT NULL,
  `mail_method` smallint(6) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `body` longtext DEFAULT NULL,
  `html_body` longtext DEFAULT NULL,
  `send_count` int(10) unsigned DEFAULT NULL,
  `complete_count` int(10) unsigned DEFAULT 0,
  `error_count` int(10) unsigned DEFAULT 0,
  `start_date` datetime DEFAULT NULL COMMENT '(DC2Type:datetime)',
  `end_date` datetime DEFAULT NULL COMMENT '(DC2Type:datetime)',
  `search_data` longtext DEFAULT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetime)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetime)',
  PRIMARY KEY (`send_id`),
  KEY `IDX_424AD01261220EA6` (`creator_id`),
  CONSTRAINT `FK_424AD01261220EA6` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table plg_mailmaga_template
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plg_mailmaga_template`;

CREATE TABLE `plg_mailmaga_template` (
  `template_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL,
  `body` longtext NOT NULL,
  `html_body` longtext NOT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetime)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetime)',
  PRIMARY KEY (`template_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table plg_paypal_config
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plg_paypal_config`;

CREATE TABLE `plg_paypal_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` varchar(255) NOT NULL,
  `client_secret` varchar(255) NOT NULL,
  `use_sandbox` tinyint(1) NOT NULL DEFAULT 1,
  `paypal_logo` varchar(255) NOT NULL,
  `payment_paypal_logo` varchar(255) NOT NULL,
  `use_express_btn` tinyint(1) NOT NULL DEFAULT 0,
  `reference_day` int(11) DEFAULT NULL,
  `cut_off_day` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `plg_paypal_config` WRITE;
/*!40000 ALTER TABLE `plg_paypal_config` DISABLE KEYS */;

INSERT INTO `plg_paypal_config` (`id`, `client_id`, `client_secret`, `use_sandbox`, `paypal_logo`, `payment_paypal_logo`, `use_express_btn`, `reference_day`, `cut_off_day`)
VALUES
	(1,'AUpacXUkfhAau5pmLLCPFmMfB6qnb_PZs0Jc2umj5BTmxKXD-cQjiFNCNZ6AoYkdpt_1VTWGpH4NhL-1','EFpQFGWxvy0ASPDhkA8JlbLRiq3-WMBzA5fvk4qEHjbNNtHKylH7Vn6Ax8LfEIpsCcF934oXOOGvUuIU',0,'1','1',0,2,15);

/*!40000 ALTER TABLE `plg_paypal_config` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table plg_paypal_subscribing_customer
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plg_paypal_subscribing_customer`;

CREATE TABLE `plg_paypal_subscribing_customer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned NOT NULL,
  `product_class_id` int(10) unsigned NOT NULL,
  `reference_transaction_id` int(10) unsigned DEFAULT NULL,
  `primary_shipping_address` text DEFAULT NULL COMMENT '(DC2Type:json_array)',
  `error_message` varchar(255) DEFAULT NULL,
  `next_payment_date` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `contracted_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `withdrawal_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  PRIMARY KEY (`id`),
  KEY `IDX_7E353C69395C3F3` (`customer_id`),
  KEY `IDX_7E353C621B06187` (`product_class_id`),
  KEY `IDX_7E353C676500E2D` (`reference_transaction_id`),
  CONSTRAINT `FK_7E353C621B06187` FOREIGN KEY (`product_class_id`) REFERENCES `dtb_product_class` (`id`),
  CONSTRAINT `FK_7E353C676500E2D` FOREIGN KEY (`reference_transaction_id`) REFERENCES `plg_paypal_transaction` (`id`),
  CONSTRAINT `FK_7E353C69395C3F3` FOREIGN KEY (`customer_id`) REFERENCES `dtb_customer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table plg_paypal_transaction
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plg_paypal_transaction`;

CREATE TABLE `plg_paypal_transaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned DEFAULT NULL,
  `status_code` varchar(255) DEFAULT NULL,
  `paypal_debug_id` varchar(255) DEFAULT NULL,
  `billing_agreement_id` varchar(255) DEFAULT NULL,
  `capture_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_2F162BF78D9F6D38` (`order_id`),
  CONSTRAINT `FK_2F162BF78D9F6D38` FOREIGN KEY (`order_id`) REFERENCES `dtb_order` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `plg_paypal_transaction` WRITE;
/*!40000 ALTER TABLE `plg_paypal_transaction` DISABLE KEYS */;

INSERT INTO `plg_paypal_transaction` (`id`, `order_id`, `status_code`, `paypal_debug_id`, `billing_agreement_id`, `capture_id`)
VALUES
	(1,48,'201','ee5986cad6ab0',NULL,'7ST25558BH4370715'),
	(2,49,'201','f9748b3935996',NULL,'8XS431485A2264318'),
	(3,54,'201','b49d5cb6a56df',NULL,'1EF72250CX106752V'),
	(4,67,'201','5eda84906a4fc',NULL,'05U672616A3433725'),
	(5,114,'201','8f37d42b87ce2',NULL,'41775826E5156653B'),
	(6,119,'201','cdc63cd619fdc',NULL,'8NB98248A9798971M');

/*!40000 ALTER TABLE `plg_paypal_transaction` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table plg_product_class_reserve4_extra
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plg_product_class_reserve4_extra`;

CREATE TABLE `plg_product_class_reserve4_extra` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_class_id` int(10) unsigned DEFAULT NULL,
  `class_category_id1` int(10) unsigned DEFAULT NULL,
  `class_category_id2` int(10) unsigned DEFAULT NULL,
  `product_id` int(10) unsigned DEFAULT NULL,
  `is_allowed` int(11) NOT NULL DEFAULT 0,
  `start_date` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `end_date` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `shipping_date` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `shipping_date_changed` int(11) NOT NULL DEFAULT 0,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  PRIMARY KEY (`id`),
  KEY `IDX_CF38906921B06187` (`product_class_id`),
  KEY `IDX_CF389069248D128` (`class_category_id1`),
  KEY `IDX_CF3890699B418092` (`class_category_id2`),
  KEY `IDX_CF3890694584665A` (`product_id`),
  CONSTRAINT `FK_CF38906921B06187` FOREIGN KEY (`product_class_id`) REFERENCES `dtb_product_class` (`id`),
  CONSTRAINT `FK_CF389069248D128` FOREIGN KEY (`class_category_id1`) REFERENCES `dtb_class_category` (`id`),
  CONSTRAINT `FK_CF3890694584665A` FOREIGN KEY (`product_id`) REFERENCES `dtb_product` (`id`),
  CONSTRAINT `FK_CF3890699B418092` FOREIGN KEY (`class_category_id2`) REFERENCES `dtb_class_category` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table plg_product_contact4_config
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plg_product_contact4_config`;

CREATE TABLE `plg_product_contact4_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table plg_product_display_rank4_config
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plg_product_display_rank4_config`;

CREATE TABLE `plg_product_display_rank4_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_list_order_by_id` smallint(5) unsigned NOT NULL,
  `type` smallint(5) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `plg_product_display_rank4_config` WRITE;
/*!40000 ALTER TABLE `plg_product_display_rank4_config` DISABLE KEYS */;

INSERT INTO `plg_product_display_rank4_config` (`id`, `product_list_order_by_id`, `type`, `name`)
VALUES
	(1,4,2,'おすすめ順'),
	(2,4,1,'おすすめ順');

/*!40000 ALTER TABLE `plg_product_display_rank4_config` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table plg_product_reserve4_config
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plg_product_reserve4_config`;

CREATE TABLE `plg_product_reserve4_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `plg_product_reserve4_config` WRITE;
/*!40000 ALTER TABLE `plg_product_reserve4_config` DISABLE KEYS */;

INSERT INTO `plg_product_reserve4_config` (`id`, `name`, `value`)
VALUES
	(1,'mail_order_reservation','11'),
	(2,'mail_reservation_shipping_change','12'),
	(3,'order_status','6'),
	(4,'payments','14,15');

/*!40000 ALTER TABLE `plg_product_reserve4_config` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table plg_product_reserve4_extra
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plg_product_reserve4_extra`;

CREATE TABLE `plg_product_reserve4_extra` (
  `product_id` int(10) unsigned NOT NULL,
  `is_allowed` int(11) NOT NULL DEFAULT 0,
  `start_date` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `end_date` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `shipping_date` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `plg_product_reserve4_extra` WRITE;
/*!40000 ALTER TABLE `plg_product_reserve4_extra` DISABLE KEYS */;

INSERT INTO `plg_product_reserve4_extra` (`product_id`, `is_allowed`, `start_date`, `end_date`, `shipping_date`, `create_date`, `update_date`)
VALUES
	(4,0,'2020-07-09 15:00:00',NULL,'2020-07-29 15:00:00','2020-07-17 03:01:50','2020-08-06 12:27:29'),
	(5,0,NULL,NULL,NULL,'2020-08-06 12:38:00','2020-08-06 12:38:00'),
	(6,0,NULL,NULL,NULL,'2020-08-06 12:36:28','2020-08-06 12:36:28'),
	(7,0,NULL,NULL,NULL,'2020-08-06 12:35:44','2020-09-06 17:25:26'),
	(8,0,NULL,NULL,NULL,'2020-08-06 12:30:29','2020-08-06 12:30:29'),
	(9,0,NULL,NULL,NULL,'2020-08-06 12:32:41','2020-08-06 12:32:41'),
	(10,0,NULL,NULL,NULL,'2020-08-06 12:38:56','2020-08-06 12:38:56'),
	(11,0,NULL,NULL,NULL,'2020-08-06 12:37:13','2020-08-06 12:37:13'),
	(12,0,NULL,NULL,NULL,'2020-08-06 12:28:48','2020-08-06 12:28:48'),
	(13,0,NULL,NULL,NULL,'2020-08-06 12:29:34','2020-08-06 12:29:34'),
	(14,0,NULL,NULL,NULL,'2020-08-06 12:39:41','2020-09-06 17:24:53'),
	(15,0,'2020-07-09 15:00:00',NULL,'2020-07-29 15:00:00','2020-07-17 03:02:22','2020-08-06 12:26:50');

/*!40000 ALTER TABLE `plg_product_reserve4_extra` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table plg_product_reserve4_order
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plg_product_reserve4_order`;

CREATE TABLE `plg_product_reserve4_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned DEFAULT NULL,
  `product_id` int(10) unsigned DEFAULT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime)',
  PRIMARY KEY (`id`),
  KEY `IDX_322E96EE8D9F6D38` (`order_id`),
  KEY `IDX_322E96EE4584665A` (`product_id`),
  CONSTRAINT `FK_322E96EE4584665A` FOREIGN KEY (`product_id`) REFERENCES `dtb_product` (`id`),
  CONSTRAINT `FK_322E96EE8D9F6D38` FOREIGN KEY (`order_id`) REFERENCES `dtb_order` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table plg_product_review
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plg_product_review`;

CREATE TABLE `plg_product_review` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sex_id` smallint(5) unsigned DEFAULT NULL,
  `product_id` int(10) unsigned DEFAULT NULL,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `status_id` smallint(5) unsigned DEFAULT NULL,
  `reviewer_name` varchar(255) NOT NULL,
  `reviewer_url` longtext DEFAULT NULL,
  `title` varchar(50) NOT NULL,
  `comment` longtext NOT NULL,
  `recommend_level` smallint(6) NOT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  PRIMARY KEY (`id`),
  KEY `IDX_9CA38FA25A2DB2A0` (`sex_id`),
  KEY `IDX_9CA38FA24584665A` (`product_id`),
  KEY `IDX_9CA38FA29395C3F3` (`customer_id`),
  KEY `IDX_9CA38FA26BF700BD` (`status_id`),
  CONSTRAINT `FK_9CA38FA24584665A` FOREIGN KEY (`product_id`) REFERENCES `dtb_product` (`id`),
  CONSTRAINT `FK_9CA38FA25A2DB2A0` FOREIGN KEY (`sex_id`) REFERENCES `mtb_sex` (`id`),
  CONSTRAINT `FK_9CA38FA26BF700BD` FOREIGN KEY (`status_id`) REFERENCES `plg_product_review_status` (`id`),
  CONSTRAINT `FK_9CA38FA29395C3F3` FOREIGN KEY (`customer_id`) REFERENCES `dtb_customer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `plg_product_review` WRITE;
/*!40000 ALTER TABLE `plg_product_review` DISABLE KEYS */;

INSERT INTO `plg_product_review` (`id`, `sex_id`, `product_id`, `customer_id`, `status_id`, `reviewer_name`, `reviewer_url`, `title`, `comment`, `recommend_level`, `create_date`, `update_date`)
VALUES
	(3,1,11,NULL,1,'くましん',NULL,'目に見えての効果は感じられない','友人からこちらをプレゼントしてもらい、最近使っています。\r\n\r\n正直目に見えての効果は感じられません。\r\n寝る前に使用して朝起きると確かに寝起きがいいと言われればいい。\r\nけど、体調がただ良いかもしれない。。。。\r\n\r\nまだ使い始めたばかりなのでその内効果が出てくるかもしれません。\r\n\r\nただ、お酒と一緒に使うと効果がはっきりと感じられますが、少し危険です。\r\n私の場合は最初気持ち悪くなったのですが、2時間ぐらい経つとすごいリラックス状態になりました。\r\n例えるとサウナのととのう状態です。\r\nしかしお酒との使用はダメな場合もあるので気を付ける必要があります。\r\n\r\n継続して使ってみようと思います。',4,'2020-07-15 00:37:18','2020-07-19 09:41:04'),
	(4,1,4,NULL,1,'マサ',NULL,'飲みやすい、お得なオプションが欲しいです！','とても飲みやすいですね。たくさん買うともっと安くなるお得なシステムがあるといい。',4,'2020-08-05 06:25:40','2020-08-05 06:30:21'),
	(5,2,15,NULL,1,'カツ',NULL,'１本じゃ物足りない。','１本買ったのですが、あまり効き目が感じらなかったので、６本まとめて買いに来ました。毎週のダンスのレッスンに持って行ってます。爽やか系飲み物です。ココナッツウォーターのような味かな。',3,'2020-08-05 06:28:00','2020-08-05 06:30:32'),
	(6,1,7,NULL,1,'たかし',NULL,'最初にいいと勧められて','CBDを試してみたくて。でも少し危険そうで怖かったのですが、昔から使用している友人に教えてもらって３％からはじめています。日頃感じてた、節々の痛みは和らいでいるかな。。。。効果がわかるまで約1週間半くらいかかりましたけど。この自粛期間中で気分が少し上がった気もします。',4,'2020-08-05 06:36:14','2020-08-05 06:36:26'),
	(7,NULL,9,NULL,1,'かつ',NULL,'オリーブオイルを飲むのが初めてなので','実は今までCBDオイルはもちろんオリーブオイルだけを口に入れた事がないので匂いと味になれるまで少し時間がかかりました。でもまだ始めたばかりので続けてみます。ひとつ、便通が良くなったと思います。',3,'2020-08-07 05:44:25','2020-08-07 05:48:54'),
	(8,NULL,4,NULL,1,'ふみ',NULL,'もったいない','美味しい水なのですが、値段のこともあって、少し遠慮して飲んでしまいます。犬の散歩のお共になってます。カルピスみたいな濃縮タイプで、水道水に薄めて飲めるタイプがあれば良いと思いました。',4,'2020-08-07 05:46:10','2020-08-07 05:49:04'),
	(9,NULL,13,NULL,1,'マサ',NULL,'ちょっと薄いかな','はじめて試したので１％からいきましたが、ちょっと薄いかな。次回はもう少し濃いものを挑戦してみようと思います。',3,'2020-08-07 05:48:00','2020-08-07 05:49:13'),
	(10,1,7,NULL,1,'haruto',NULL,'「人生で初めて！」','初めての購入でしたのでショップにどのCBDオイルがいいか問合せをして購入しました。\r\n性別、身長などで私にあったCBDオイルの容量や％など教えて頂き丁寧に対応頂きました。\r\n次回はもう少し濃くしてみるつもりです。',5,'2020-08-07 09:25:29','2020-08-14 07:20:29'),
	(11,1,10,NULL,1,'TAKE123',NULL,'飲んで実感‼️','最初は半信半疑でしたが飲み始めて少量から合わしていくうちに睡眠の質が変わって起きた時にスッキリ。本当に飲んで実感‼️',5,'2020-08-08 20:27:38','2020-08-10 06:14:42'),
	(13,2,15,1,1,'Haruna',NULL,'ペットボトルに巻いてあるフィルム','いま毎日1/2本ずつ飲んでいるんですが、特に体感がなく追加で買いに来ました。全然関係ないかもですが、ひとつ思った所は、ペットボトルに巻いてあるフィルムが切れ目がなくて、取りづらい点でしょうか。',3,'2020-08-10 06:36:41','2020-08-11 06:29:13'),
	(14,1,14,NULL,1,'よっし',NULL,'高濃度','3%で効き目が感じられなかったので、ここまでたどり着きました。（汗）10%さすがです。お値段少し張りますが、飲む量も少なくて良いので、コスパ的には同じくらいですかね。',5,'2020-08-11 05:59:00','2020-08-11 06:13:44'),
	(15,1,6,NULL,1,'Taka',NULL,'どれを飲んだら良いかわからない','％の種類がたくさんありすぎて、自分にはどれが良いのか正直わかりません。できれば「オンライン診断」のような感じでアンケート形式で自分にはどれが合うか確認するシステムがあれば良いかもしれません。',3,'2020-08-11 06:07:41','2020-08-11 06:14:02'),
	(16,1,8,NULL,1,'にし',NULL,'効果実感出来ました！','海外製は不安で日本製のCBDオイルを探していましたので安心して買うことが出来ました。私は夜に飲んでいますが、リラックス効果は実感しました。妻も良く眠れると言っていました。少しずつ濃度の高いオイルにチャレンジしようと思います。',5,'2020-08-11 19:20:53','2020-08-11 19:40:32'),
	(17,1,10,NULL,1,'Tak',NULL,'オリーブオイルが口臭にも効果的','長い間　不眠症と言うか深い睡眠がとれず疲れが溜まる毎日でしたがMARITIME5%を使いだして睡眠の質が変わった気がします。オリーブオイルが口臭にも効果とのテレビで紹介しているのをみてW効果きたいしてます。',5,'2020-08-14 05:40:59','2020-08-14 05:41:22'),
	(18,1,13,NULL,1,'Warren',NULL,'cbd를 처음 이용해 보았습니다','지난 6월 초에 구입해서 현재까지 사용중입니다\r\n자기전에 매일 스포이트로 약 30%를 복용중입니다\r\n호기심에 시작해 보았는데 이제는 꼭 있어야 할것 같습니다 \r\n전에는 아침에 일어나면 몸이 개운하지를 않았는데 요즘은 몸이 아주 가볍고 기운이 납니다 \r\n제가 전에는 변비가 있었는데 요즘은 화장실 갔다오면 기분이 아주 좋습니다 \r\n그리고 최근에 이사실을 알았는데 그렇게 심했던 입의 구취가 거의 없어졌습니다 \r\n이 제품은 여러모로 저에게는 많은 도움이 됩니다',5,'2020-08-15 16:12:50','2020-08-17 06:25:12'),
	(19,1,12,NULL,1,'さく',NULL,'睡眠導入剤の替わりに','今年のはじめからの様々なストレスで、なかなか寝付けなくなってしまい、睡眠導入剤を飲んでいました。飲み過ぎはからだによくないのかなと思い始め、オーガニックなCBDオイルに少しずつ移行しています。今のところ調子は50/50という感じです。次はもう少し％をあげてみます。',3,'2020-08-17 06:32:16','2020-08-17 06:37:27'),
	(20,1,5,NULL,1,'Masa087',NULL,'ちょっと高いけど効果てきめん','なかなかの値段なので、躊躇しましたが、さすが１０％は効果てき面でした。少し強すぎる濃度なので、口に入れる量を少なめに調整しています。もうひとつ濃度を下げてもいいかなと思ってます。',4,'2020-08-17 06:37:12','2020-08-17 06:37:35'),
	(21,1,9,NULL,1,'ヒロタカ',NULL,'試しに1%を買いました！','飲んだ感じはオリーブオイルそのものでした。寝つきが悪いので夜寝る前に飲んでいます。知らない間に寝ているのでいい感じです。',4,'2020-08-21 20:32:00','2020-08-25 07:25:49'),
	(22,2,7,NULL,1,'マキ',NULL,'５％使ってからここに戻ってきました。','５％をトライしたのですが、少し女性には強めな感じがしたので、３％に戻ってきました。％はこの辺りがおすすめです。',3,'2020-08-25 07:27:32','2020-08-25 07:38:54'),
	(23,1,14,NULL,1,'オリオ',NULL,'なかなか良し','いろんなストレスで眠れない日々を過ごしていたのですが、 友人に誘われてcbdを試して見たら、あっというまに虜になってしまいました。今はどこに行くにも離せない存在になりました。特に睡眠環境が変わるので、旅行には必需品で、持っていってます。',5,'2020-08-25 07:30:26','2020-08-25 07:39:04'),
	(24,2,13,NULL,1,'かな',NULL,'初心者用にはバッチリ','cbdをはじめたくて、挑戦してみました。いまいち効果がまだカラダに伝わっていませんが、とりあえずなくなるまで使い続けます！',2,'2020-08-25 07:32:09','2020-08-25 07:39:11'),
	(25,2,15,NULL,1,'美紀',NULL,'毎朝、朝一番の水として飲んでいます','無味無臭でお水が柔らかく、飲みやすい。毎朝、朝一番の水として飲んでいましたが、体に負担がかからず浸透されていく感じがします。飲んでからの体感としては、特に感じられませんでしたが、強いていえば睡眠の質があがったように感じました。',5,'2020-08-25 07:33:13','2020-08-25 07:39:18'),
	(26,1,4,NULL,1,'マ〜さ',NULL,'就寝前にCBDウオーターをコップに一杯','私は寝入りはとても良いのですが寝起き時に胸部から頭にかけての、特に背面の重だるさに毎朝悩まされています、就寝前にCBDウオーターをコップに一杯飲み就寝することにしました。覚醒時の軽い目眩が和らいでほとんど気にならない事に驚きました。',4,'2020-08-25 07:34:52','2020-08-25 07:39:24'),
	(27,1,4,NULL,1,'光',NULL,'やはりCBDウオーターの影響','気起床時の背面の気怠さが軽減されていました、特に夏場ですので就寝前には寝汗を鑑みてサーバーの水を同量飲んでおりましたがそれらに関しては特に変化を感じた事がありませんのでやはりCBDウオーターの影響と考えるのが自然ではないかと考えております。',4,'2020-08-25 07:36:01','2020-08-25 07:39:32'),
	(28,1,7,NULL,1,'まちゃ',NULL,'寝起き時のなんとも言えない不快なだるさが軽減','私はお昼休憩時に飲む事にしました。3時間の休憩時間がありますので飲んだ後に午後に備えて仮眠を取るのですが\r\n日中の仮眠はリズムが崩れますとなかなか睡眠状態に至らないのですがオイルの風味を口内に残しながら仮眠に入り通常の夜間の睡眠同様寝起き時のなんとも言えない不快なだるさが軽減されている事が実感出来ます。',4,'2020-08-25 07:36:56','2020-08-25 07:39:38'),
	(29,NULL,11,NULL,1,'雅美',NULL,'朝は強烈に機嫌が悪く、重だるさにイライラしていました。','旦那のオイルを一緒に飲んでます。私はすぐに眠りますが朝は強烈に機嫌が悪く、重だるさにイライラしています。それらが軽減されていくと言った感じです。これからも飲み続けます。',4,'2020-08-25 07:38:36','2020-08-25 07:39:43'),
	(30,2,11,NULL,1,'購入者',NULL,'効果あり','おすすめと聞き最初は小瓶を試して今回購入しました。イライラした時と寝る前に使っていますが熟睡することが多くなってきました。',5,'2020-08-27 17:00:22','2020-08-31 03:06:29'),
	(31,1,11,NULL,1,'購入者',NULL,'2回目の購入です。','最初は小さいのを買って今回が2回目です睡眠の質が変わっています。私の場合は効果ありです。',5,'2020-08-27 17:21:36','2020-08-31 03:06:45'),
	(32,1,12,NULL,1,'Sea',NULL,'건강','코로나에 대한 저항력이 생긴것 같아요 \r\n컨디션이 좋으니까 힘도 세지고 피곤한 줄을 모르겠어요 모두가 건강하게 살기를 바랍니다',5,'2020-08-28 17:46:55','2020-08-31 03:06:56'),
	(33,2,11,14,1,'Celine','https://maritime-cbd.com/products/detail/11','cbd30ml 3%','イライラが無くなった。\r\n眠りが深くなりました。',5,'2020-08-29 03:10:04','2020-08-31 03:07:23'),
	(34,1,11,1,1,'フレデリック','https://review.rakuten.co.jp/item/1/380585_10001582/7cqu-i044t-8o_5_1203112996/','朝まで熟睡出来ました！','すごくはやくMARITIMEのCBDオイルが届きました。\r\n国内生産という点が魅力で買ってみました。\r\n寝る前に飲んですぐに体がふわっとして、そのまま眠りにつきました。\r\n想像以上に良かったです。\r\nちなみに、バッグにも入れておいて仕事で疲れた時に飲むとか、色々使い勝手も良いです。',5,'2020-08-31 03:05:45','2020-08-31 03:17:40'),
	(35,1,9,1,1,'harutoua','https://review.rakuten.co.jp/item/1/380585_10001575/7cqu-i0443-q1_5_1197229306/','この頃、睡眠が浅く体調管理の為にネットで睡眠解消と検索してCBDオイルのことを知りました。','この頃、睡眠が浅く体調管理の為にネットで睡眠解消と検索してCBDオイルのことを知りました。\r\n日本産ということで安心かなと思い購入。CBDオイルを使用してから数日後にぐっすり眠れたようで朝の目覚めがとてもさわやかでした。毎日寝る前に使用してます。オリーブオイルの味はサラっとしていて舌下で吸収をしていてもまったく違和感はありません。他の効能も楽しみにしております。',5,'2020-08-31 03:11:01','2020-08-31 03:14:00'),
	(36,1,9,1,1,'【ルカリオ】','https://review.rakuten.co.jp/item/1/380585_10001575/7cqu-i0443-8w_5_1196311418/','効果実感出来ました！','海外製は不安で日本製のCBDオイルを探していましたので安心して買うことが出来ました。私は夜に飲んでいますが、リラックス効果は実感しました。妻も良く眠れると言っていました。少しずつ濃度の高いオイルにチャレンジしようと思います。',5,'2020-08-31 03:12:23','2020-08-31 03:14:11'),
	(37,1,9,1,1,'朝霧407','https://review.rakuten.co.jp/item/1/380585_10001575/7cqu-i0443-8j_5_1197328158/','これなんですね。','これなんですね。\r\n友人に聞いて体調が凄く良くなるって、早速購入してみました。丁度今日で1週間ですが、毎日の睡眠、寝起き、体のだるさが改善されてます。継続していきたいと思ってます。\r\n特に夜なかなか寝れない人にはお勧めできそうです。早速友人にも勧めてみます。',4,'2020-08-31 03:13:30','2020-08-31 03:14:18'),
	(38,1,4,1,1,'フレデリック','https://review.rakuten.co.jp/item/1/380585_10001585/7cqu-i044t-8o_5_1203112998/','続ければ続けるほど効果実感！','ネットでCBDの万能っぷりを知って購入してみました。\r\n私はストレス予防として飲んでいるのですが、効果もちゃんと実感できるしパッケージも可愛いので気に入ってます。\r\n毎日持ち歩いてストレスを感じた時に飲むようにしています！おすすめです！',5,'2020-08-31 03:17:11','2020-08-31 03:17:21'),
	(39,1,10,NULL,1,'Really',NULL,'First time','Good sleep \r\nPowerful energy',5,'2020-08-31 11:48:48','2020-09-18 00:16:01'),
	(40,1,6,NULL,1,'購入者',NULL,'朝が気持ちいい！','就寝前に飲んでいます朝が気持ちよくスッキリって感じです。',4,'2020-09-06 16:52:22','2020-09-18 00:15:12'),
	(41,2,12,NULL,1,'フジコ',NULL,'ちょうどいい','ちょうどいいです。オリーブオイルのせいか便秘がないです。夜もグッスリ眠れています。',5,'2020-09-07 02:28:06','2020-09-18 00:15:42'),
	(44,1,13,NULL,2,'test',NULL,'イライラがなくなった','イライラがなくなった',5,'2020-09-18 08:17:23','2020-09-18 08:17:23'),
	(45,1,9,NULL,2,'fれでrfdっfっg',NULL,'test','効果実感できました！dfdfdsfdsfdsfds',5,'2020-09-18 09:24:39','2020-09-18 09:24:39'),
	(46,1,8,NULL,2,'購入者',NULL,'眠りが深くなった','眠りが深くなった\r\nよく眠れますね、飲み過ぎないようにスポイト４割ぐらいが丁度いい感じですね。いろいろ考えることが多くてストレスいっぱいだなと感じたときも少し飲んでいます。',5,'2020-09-19 16:50:29','2020-09-19 16:50:29');

/*!40000 ALTER TABLE `plg_product_review` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table plg_product_review_config
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plg_product_review_config`;

CREATE TABLE `plg_product_review_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `csv_type_id` smallint(5) unsigned DEFAULT NULL,
  `review_max` smallint(5) unsigned DEFAULT 5,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  PRIMARY KEY (`id`),
  KEY `IDX_D27A17EFE8507796` (`csv_type_id`),
  CONSTRAINT `FK_D27A17EFE8507796` FOREIGN KEY (`csv_type_id`) REFERENCES `mtb_csv_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `plg_product_review_config` WRITE;
/*!40000 ALTER TABLE `plg_product_review_config` DISABLE KEYS */;

INSERT INTO `plg_product_review_config` (`id`, `csv_type_id`, `review_max`, `create_date`, `update_date`)
VALUES
	(1,6,5,'2020-05-06 11:18:12','2020-05-06 11:18:12');

/*!40000 ALTER TABLE `plg_product_review_config` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table plg_product_review_status
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plg_product_review_status`;

CREATE TABLE `plg_product_review_status` (
  `id` smallint(5) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `plg_product_review_status` WRITE;
/*!40000 ALTER TABLE `plg_product_review_status` DISABLE KEYS */;

INSERT INTO `plg_product_review_status` (`id`, `name`, `sort_no`)
VALUES
	(1,'公開',1),
	(2,'非公開',2);

/*!40000 ALTER TABLE `plg_product_review_status` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table plg_taba_cms_category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plg_taba_cms_category`;

CREATE TABLE `plg_taba_cms_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) NOT NULL,
  `data_key` varchar(255) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `description` tinytext DEFAULT NULL,
  `tag_attributes` varchar(255) DEFAULT NULL,
  `memo` tinytext DEFAULT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetime)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetime)',
  `order_no` int(11) NOT NULL,
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `plg_taba_cms_category_unique_data_key` (`type_id`,`data_key`),
  KEY `IDX_946A754FC54C8C93` (`type_id`),
  CONSTRAINT `FK_946A754FC54C8C93` FOREIGN KEY (`type_id`) REFERENCES `plg_taba_cms_type` (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `plg_taba_cms_category` WRITE;
/*!40000 ALTER TABLE `plg_taba_cms_category` DISABLE KEYS */;

INSERT INTO `plg_taba_cms_category` (`category_id`, `type_id`, `data_key`, `category_name`, `description`, `tag_attributes`, `memo`, `create_date`, `update_date`, `order_no`)
VALUES
	(1,1,'notice','重要なお知らせ',NULL,'style=\"border:solid 1px #ff0000;background-color:#ffffff;color:#ff0000;\"',NULL,'2020-05-06 09:56:55','2020-05-06 09:56:55',0),
	(2,1,'system','システムメンテナンス',NULL,'style=\"border:solid 1px #cccccc;background-color:#ffffff;color:#cccccc;\"',NULL,'2020-05-06 09:57:33','2020-05-06 09:57:33',0),
	(3,1,'info','お知らせ',NULL,'style=\"border:solid 1px #5cb1b1;background-color:#ffffff;color:#5cb1b1;\"',NULL,'2020-05-06 09:58:08','2020-05-06 09:58:08',0),
	(4,2,'column','コラム',NULL,'style=\"border:solid 1px #ff0000;background-color:#ffffff;color:#ff0000;\"',NULL,'2020-05-06 09:59:48','2020-05-11 09:26:49',0),
	(5,2,'shop_news','ショップニュース',NULL,'style=\"border:solid 1px #5cb1b1;background-color:#ffffff;color:#5cb1b1;\"',NULL,'2020-05-06 10:01:44','2020-05-11 09:26:21',0);

/*!40000 ALTER TABLE `plg_taba_cms_category` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table plg_taba_cms_post
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plg_taba_cms_post`;

CREATE TABLE `plg_taba_cms_post` (
  `post_id` int(11) NOT NULL AUTO_INCREMENT,
  `creator_id` int(10) unsigned NOT NULL,
  `type_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `data_key` varchar(255) NOT NULL,
  `public_div` int(11) NOT NULL,
  `public_date` datetime NOT NULL COMMENT '(DC2Type:datetime)',
  `content_div` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text DEFAULT NULL,
  `link_url` varchar(255) DEFAULT NULL,
  `link_target` varchar(255) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `memo` tinytext DEFAULT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetime)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetime)',
  `meta_author` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keyword` varchar(255) DEFAULT NULL,
  `meta_robots` varchar(255) DEFAULT NULL,
  `meta_tags` text DEFAULT NULL,
  `overwrite_route` varchar(255) DEFAULT NULL,
  `script` text DEFAULT NULL,
  PRIMARY KEY (`post_id`),
  UNIQUE KEY `UNIQ_316AB2EFCE1C4A1C` (`data_key`),
  KEY `IDX_316AB2EF12469DE2` (`category_id`),
  KEY `IDX_316AB2EFC54C8C93` (`type_id`),
  KEY `IDX_316AB2EF61220EA6` (`creator_id`),
  CONSTRAINT `FK_316AB2EF12469DE2` FOREIGN KEY (`category_id`) REFERENCES `plg_taba_cms_category` (`category_id`),
  CONSTRAINT `FK_316AB2EF61220EA6` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`id`),
  CONSTRAINT `FK_316AB2EFC54C8C93` FOREIGN KEY (`type_id`) REFERENCES `plg_taba_cms_type` (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `plg_taba_cms_post` WRITE;
/*!40000 ALTER TABLE `plg_taba_cms_post` DISABLE KEYS */;

INSERT INTO `plg_taba_cms_post` (`post_id`, `creator_id`, `type_id`, `category_id`, `data_key`, `public_div`, `public_date`, `content_div`, `title`, `body`, `link_url`, `link_target`, `thumbnail`, `memo`, `create_date`, `update_date`, `meta_author`, `meta_description`, `meta_keyword`, `meta_robots`, `meta_tags`, `overwrite_route`, `script`)
VALUES
	(1,1,1,1,'golden-week-2020-info',1,'2020-05-06 09:49:00',1,'GW期間の営業・発送に関して','<p style=\"vertical-align: baseline; font-family: &quot;Yu Gothic Medium&quot;, &quot;游ゴシック Medium&quot;, YuGothic, 游ゴシック体, &quot;ヒラギノ角ゴ Pro W3&quot;, メイリオ, sans-serif; outline: 0px; padding: 0px 0px 27px; margin: 0px; border: 0px; -webkit-font-smoothing: subpixel-antialiased; color: rgb(103, 103, 103);\">いつもご愛顧賜り誠にありがとうございます。</p><p style=\"vertical-align: baseline; font-family: &quot;Yu Gothic Medium&quot;, &quot;游ゴシック Medium&quot;, YuGothic, 游ゴシック体, &quot;ヒラギノ角ゴ Pro W3&quot;, メイリオ, sans-serif; outline: 0px; padding: 0px 0px 27px; margin: 0px; border: 0px; -webkit-font-smoothing: subpixel-antialiased; color: rgb(103, 103, 103);\">GW期間についての営業及び発送については下記の通りとなります。</p><p style=\"vertical-align: baseline; font-family: &quot;Yu Gothic Medium&quot;, &quot;游ゴシック Medium&quot;, YuGothic, 游ゴシック体, &quot;ヒラギノ角ゴ Pro W3&quot;, メイリオ, sans-serif; outline: 0px; padding: 0px 0px 27px; margin: 0px; border: 0px; -webkit-font-smoothing: subpixel-antialiased; color: rgb(103, 103, 103);\"><span style=\"vertical-align: baseline; font-weight: 600; font-style: inherit; outline: 0px; padding: 0px; margin: 0px; border: 0px;\">［GW休業期間 ］<br>4月29日（水）、5月2日（土）～5月6日（水）</span><br><br>4月28日（火曜日）午前11時までご注文いただければ当日発送させていただき、11時以降の場合は4月30日（木曜日）の発送とさせていただきます。<br>5月1日（金曜日）についても、午前11時までご注文いただければ当日発送させていただき、11時以降の場合は5月7日（木曜日）の発送となりますことご了承ください。<br><span style=\"vertical-align: baseline; font-weight: 600; font-style: inherit; outline: 0px; padding: 0px; margin: 0px; border: 0px;\">※期間中は配送業務・電話お問い合わせを行っておりません。またメールでの返信も遅れることがございますので予めご了承ください。</span></p><p style=\"vertical-align: baseline; font-family: &quot;Yu Gothic Medium&quot;, &quot;游ゴシック Medium&quot;, YuGothic, 游ゴシック体, &quot;ヒラギノ角ゴ Pro W3&quot;, メイリオ, sans-serif; outline: 0px; padding: 0px 0px 27px; margin: 0px; border: 0px; -webkit-font-smoothing: subpixel-antialiased; color: rgb(103, 103, 103);\">その他の日については、通常通り配送業務・お問い合わせ業務に対応しております。引き続き何卒お願い申し上げます。</p>',NULL,NULL,NULL,NULL,'2020-05-06 09:50:33','2020-05-11 09:35:27','Maritime','今話題のCBDオイルを試したら即効性がすごかった【ストレス緩和、睡眠改善、免疫疾患改善】','Maritime、CBDオイル、ストレス緩和、睡眠改善、免疫疾患改善',NULL,NULL,NULL,NULL),
	(6,3,1,3,'news_20200723',1,'2020-07-23 07:33:00',1,'マリタイムのCBDオイル、CBDウォーターの商品情報がプレスリリースにて紹介されました。','<p>マリタイムのCBDオイルとCBDウォーターの日本国内の正式提供開始がプレスリリースにて発表されました。マリタイムは、メイド・イン・ジャパンの品質、オリーブオイルを使った天然由来CBDオイルと清涼飲料水CBDウォーターを販売しています。高純度99％以上のCBDを使用しています。国内生産にこだわった、「高品質」かつ「安心・信頼」の日本製CBDオイルをお届けしたいと願いを込めて作ったサプリメントです。THCは100％入っていません。当店で扱っているすべての商品は、日本の法律に厳守し、正式な許可を得て販売しています。厚生労働省への許可申請や継続的な検査と厳格な品質保証を受けています。卸売パッケージをご用意しておりますのでお気軽にお問い合わせください<span style=\"font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, &quot;Noto Sans&quot;, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;;\">。<br><br></span></p><p>日本のプレスリリース会社のPR TIMES, VALUE PRESSのほか、下記の27メディアでも紹介されました。<span style=\"font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, &quot;Noto Sans&quot;, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;;\">卸売パッケージのお問い合わせ、</span><span style=\"font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, &quot;Noto Sans&quot;, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;;\">その他の各種メディアのオファーなどは、</span><a href=\"mailto: maritime@maritime-cbd.com\">maritime@maritime-cbd.com</a>から<span style=\"font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, &quot;Noto Sans&quot;, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;;\">ご連絡ください。</span></p><br><p>PR TIMES社のプレスリリース：</p><p><a href=\"https://prtimes.jp/main/html/rd/p/000000002.000061428.html\" target=\"_blank\">https://prtimes.jp/main/html/rd/p/000000002.000061428.html</a></p><br><p>VALUE PRESS社のプレスリリース：</p><p><a href=\"https://www.value-press.com/pressrelease/248994\" target=\"_blank\">https://www.value-press.com/pressrelease/248994</a></p><br><ul class=\"slidemain\" style=\"margin: 0px; padding: 0px 40px 40px; border: 0px; outline: 0px; font-size: 16px; vertical-align: baseline; background: rgb(245, 245, 245); list-style: none; color: rgb(89, 89, 89); font-family: &quot;Helvetica Neue&quot;, Arial, &quot;Hiragino Kaku Gothic ProN&quot;, &quot;ヒラギノ角ゴ ProN W3&quot;, メイリオ, Meiryo, sans-serif; width: 910px;\"><li class=\"artttl\" style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background: transparent; font-weight: bold; position: relative;\"><div class=\"it it1\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 200px;\"><br><br class=\"Apple-interchange-newline\">媒体名</div><div class=\"it it2\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 200px;\">媒体社名</div><div class=\"it it3\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 450px;\">URL</div></li><li class=\"art\" id=\"media_1\" style=\"margin: 0px; padding: 15px 0px; border-width: 1px 0px 0px; border-top-style: solid; border-right-style: initial; border-bottom-style: initial; border-left-style: initial; border-top-color: rgb(223, 223, 223); border-right-color: initial; border-bottom-color: initial; border-left-color: initial; border-image: initial; outline: 0px; vertical-align: baseline; background: transparent; position: relative;\"><div class=\"it it0\" style=\"margin: 0px; padding: 0px 5px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; white-space: nowrap; text-align: right; width: 24px;\">1</div><div class=\"it it1\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 200px;\">ジョルダンニュース！</div><div class=\"it it2\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; font-weight: bold; width: 200px;\">ジョルダン株式会社</div><div class=\"it it3\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 450px;\"><a href=\"http://news.jorudan.co.jp/docs/news/detail.cgi?newsid=PT000002A000061428\" target=\"_blank\" style=\"margin: 0px; padding: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(44, 152, 181);\">http://news.jorudan.co.jp/docs/news/detail.cgi?newsid=PT000002A000061428</a></div></li><li class=\"art\" id=\"media_2\" style=\"margin: 0px; padding: 15px 0px; border-width: 1px 0px 0px; border-top-style: solid; border-right-style: initial; border-bottom-style: initial; border-left-style: initial; border-top-color: rgb(223, 223, 223); border-right-color: initial; border-bottom-color: initial; border-left-color: initial; border-image: initial; outline: 0px; vertical-align: baseline; background: transparent; position: relative;\"><div class=\"it it0\" style=\"margin: 0px; padding: 0px 5px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; white-space: nowrap; text-align: right; width: 24px;\">2</div><div class=\"it it1\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 200px;\">産経ニュース</div><div class=\"it it2\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; font-weight: bold; width: 200px;\">株式会社産経デジタル</div><div class=\"it it3\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 450px;\"><a href=\"https://www.sankei.com/economy/news/200723/prl2007230097-n1.html\" target=\"_blank\" style=\"margin: 0px; padding: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(44, 152, 181);\">https://www.sankei.com/economy/news/200723/prl2007230097-n1.html</a></div></li><li class=\"art\" id=\"media_3\" style=\"margin: 0px; padding: 15px 0px; border-width: 1px 0px 0px; border-top-style: solid; border-right-style: initial; border-bottom-style: initial; border-left-style: initial; border-top-color: rgb(223, 223, 223); border-right-color: initial; border-bottom-color: initial; border-left-color: initial; border-image: initial; outline: 0px; vertical-align: baseline; background: transparent; position: relative;\"><div class=\"it it0\" style=\"margin: 0px; padding: 0px 5px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; white-space: nowrap; text-align: right; width: 24px;\">3</div><div class=\"it it1\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 200px;\">ORICON NEWS</div><div class=\"it it2\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; font-weight: bold; width: 200px;\">オリコン株式会社</div><div class=\"it it3\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 450px;\"><a href=\"https://www.oricon.co.jp/pressrelease/685137/\" target=\"_blank\" style=\"margin: 0px; padding: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(44, 152, 181);\">https://www.oricon.co.jp/pressrelease/685137/</a></div></li><li class=\"art\" id=\"media_4\" style=\"margin: 0px; padding: 15px 0px; border-width: 1px 0px 0px; border-top-style: solid; border-right-style: initial; border-bottom-style: initial; border-left-style: initial; border-top-color: rgb(223, 223, 223); border-right-color: initial; border-bottom-color: initial; border-left-color: initial; border-image: initial; outline: 0px; vertical-align: baseline; background: transparent; position: relative;\"><div class=\"it it0\" style=\"margin: 0px; padding: 0px 5px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; white-space: nowrap; text-align: right; width: 24px;\">4</div><div class=\"it it1\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 200px;\">All About NEWS</div><div class=\"it it2\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; font-weight: bold; width: 200px;\">株式会社オールアバウト</div><div class=\"it it3\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 450px;\"><a href=\"https://news.allabout.co.jp/articles/p/000000002.000061428/\" target=\"_blank\" style=\"margin: 0px; padding: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(44, 152, 181);\">https://news.allabout.co.jp/articles/p/000000002.000061428/</a></div></li><li class=\"art\" id=\"media_5\" style=\"margin: 0px; padding: 15px 0px; border-width: 1px 0px 0px; border-top-style: solid; border-right-style: initial; border-bottom-style: initial; border-left-style: initial; border-top-color: rgb(223, 223, 223); border-right-color: initial; border-bottom-color: initial; border-left-color: initial; border-image: initial; outline: 0px; vertical-align: baseline; background: transparent; position: relative;\"><div class=\"it it0\" style=\"margin: 0px; padding: 0px 5px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; white-space: nowrap; text-align: right; width: 24px;\">5</div><div class=\"it it1\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 200px;\">現代ビジネス</div><div class=\"it it2\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; font-weight: bold; width: 200px;\">株式会社講談社</div><div class=\"it it3\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 450px;\"><a href=\"http://gendai.ismedia.jp/ud/pressrelease/5f1921417765616955070000\" target=\"_blank\" style=\"margin: 0px; padding: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(44, 152, 181);\">http://gendai.ismedia.jp/ud/pressrelease/5f1921417765616955070000</a></div></li><li class=\"art\" id=\"media_6\" style=\"margin: 0px; padding: 15px 0px; border-width: 1px 0px 0px; border-top-style: solid; border-right-style: initial; border-bottom-style: initial; border-left-style: initial; border-top-color: rgb(223, 223, 223); border-right-color: initial; border-bottom-color: initial; border-left-color: initial; border-image: initial; outline: 0px; vertical-align: baseline; background: transparent; position: relative;\"><div class=\"it it0\" style=\"margin: 0px; padding: 0px 5px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; white-space: nowrap; text-align: right; width: 24px;\">6</div><div class=\"it it1\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 200px;\">@DIME（アットダイム）</div><div class=\"it it2\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; font-weight: bold; width: 200px;\">株式会社小学館</div><div class=\"it it3\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 450px;\"><a href=\"https://dime.jp/company_news/detail/?pr=648133\" target=\"_blank\" style=\"margin: 0px; padding: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(44, 152, 181);\">https://dime.jp/company_news/detail/?pr=648133</a></div></li><li class=\"art\" id=\"media_7\" style=\"margin: 0px; padding: 15px 0px; border-width: 1px 0px 0px; border-top-style: solid; border-right-style: initial; border-bottom-style: initial; border-left-style: initial; border-top-color: rgb(223, 223, 223); border-right-color: initial; border-bottom-color: initial; border-left-color: initial; border-image: initial; outline: 0px; vertical-align: baseline; background: transparent; position: relative;\"><div class=\"it it0\" style=\"margin: 0px; padding: 0px 5px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; white-space: nowrap; text-align: right; width: 24px;\">7</div><div class=\"it it1\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 200px;\">尼崎経済新聞</div><div class=\"it it2\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; font-weight: bold; width: 200px;\">株式会社エアグラウンド</div><div class=\"it it3\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 450px;\"><a href=\"https://amagasaki.keizai.biz/release.php?id=41469\" target=\"_blank\" style=\"margin: 0px; padding: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(44, 152, 181);\">https://amagasaki.keizai.biz/release.php?id=41469</a></div></li><li class=\"art\" id=\"media_8\" style=\"margin: 0px; padding: 15px 0px; border-width: 1px 0px 0px; border-top-style: solid; border-right-style: initial; border-bottom-style: initial; border-left-style: initial; border-top-color: rgb(223, 223, 223); border-right-color: initial; border-bottom-color: initial; border-left-color: initial; border-image: initial; outline: 0px; vertical-align: baseline; background: transparent; position: relative;\"><div class=\"it it0\" style=\"margin: 0px; padding: 0px 5px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; white-space: nowrap; text-align: right; width: 24px;\">8</div><div class=\"it it1\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 200px;\">東洋経済オンライン</div><div class=\"it it2\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; font-weight: bold; width: 200px;\">株式会社東洋経済新報社</div><div class=\"it it3\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 450px;\"><a href=\"http://toyokeizai.net/ud/pressrelease/5f1921007765613e19030000\" target=\"_blank\" style=\"margin: 0px; padding: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(44, 152, 181);\">http://toyokeizai.net/ud/pressrelease/5f1921007765613e19030000</a></div></li><li class=\"art\" id=\"media_9\" style=\"margin: 0px; padding: 15px 0px; border-width: 1px 0px 0px; border-top-style: solid; border-right-style: initial; border-bottom-style: initial; border-left-style: initial; border-top-color: rgb(223, 223, 223); border-right-color: initial; border-bottom-color: initial; border-left-color: initial; border-image: initial; outline: 0px; vertical-align: baseline; background: transparent; position: relative;\"><div class=\"it it0\" style=\"margin: 0px; padding: 0px 5px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; white-space: nowrap; text-align: right; width: 24px;\">9</div><div class=\"it it1\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 200px;\">NewsCafe</div><div class=\"it it2\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; font-weight: bold; width: 200px;\">株式会社イード</div><div class=\"it it3\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 450px;\"><a href=\"https://www.newscafe.ne.jp/release/prtimes2/20200723/512675.html\" target=\"_blank\" style=\"margin: 0px; padding: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(44, 152, 181);\">https://www.newscafe.ne.jp/release/prtimes2/20200723/512675.html</a></div></li><li class=\"art\" id=\"media_10\" style=\"margin: 0px; padding: 15px 0px; border-width: 1px 0px 0px; border-top-style: solid; border-right-style: initial; border-bottom-style: initial; border-left-style: initial; border-top-color: rgb(223, 223, 223); border-right-color: initial; border-bottom-color: initial; border-left-color: initial; border-image: initial; outline: 0px; vertical-align: baseline; background: transparent; position: relative;\"><div class=\"it it0\" style=\"margin: 0px; padding: 0px 5px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; white-space: nowrap; text-align: right; width: 24px;\">10</div><div class=\"it it1\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 200px;\">BtoBプラットフォーム</div><div class=\"it it2\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; font-weight: bold; width: 200px;\">株式会社インフォマート</div><div class=\"it it3\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 450px;\"><a href=\"https://b2b-ch.infomart.co.jp/news/detail.page?IMNEWS4=2074126\" target=\"_blank\" style=\"margin: 0px; padding: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(44, 152, 181);\">https://b2b-ch.infomart.co.jp/news/search/../detail.page?IMNEWS4=2074126</a></div></li><li class=\"art\" id=\"media_11\" style=\"margin: 0px; padding: 15px 0px; border-width: 1px 0px 0px; border-top-style: solid; border-right-style: initial; border-bottom-style: initial; border-left-style: initial; border-top-color: rgb(223, 223, 223); border-right-color: initial; border-bottom-color: initial; border-left-color: initial; border-image: initial; outline: 0px; vertical-align: baseline; background: transparent; position: relative;\"><div class=\"it it0\" style=\"margin: 0px; padding: 0px 5px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; white-space: nowrap; text-align: right; width: 24px;\">11</div><div class=\"it it1\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 200px;\">時事メディカル</div><div class=\"it it2\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; font-weight: bold; width: 200px;\">株式会社時事通信社</div><div class=\"it it3\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 450px;\"><a href=\"https://medical.jiji.com/prtimes/16564\" target=\"_blank\" style=\"margin: 0px; padding: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(44, 152, 181);\">https://medical.jiji.com/prtimes/16564</a></div></li><li class=\"art\" id=\"media_12\" style=\"margin: 0px; padding: 15px 0px; border-width: 1px 0px 0px; border-top-style: solid; border-right-style: initial; border-bottom-style: initial; border-left-style: initial; border-top-color: rgb(223, 223, 223); border-right-color: initial; border-bottom-color: initial; border-left-color: initial; border-image: initial; outline: 0px; vertical-align: baseline; background: transparent; position: relative;\"><div class=\"it it0\" style=\"margin: 0px; padding: 0px 5px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; white-space: nowrap; text-align: right; width: 24px;\">12</div><div class=\"it it1\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 200px;\">朝日新聞デジタル＆M</div><div class=\"it it2\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; font-weight: bold; width: 200px;\">株式会社朝日新聞社</div><div class=\"it it3\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 450px;\"><a href=\"https://www.asahi.com/and_M/pressrelease/pre_14330929/\" target=\"_blank\" style=\"margin: 0px; padding: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(44, 152, 181);\">https://www.asahi.com/and_M/pressrelease/pre_14330929/</a></div></li><li class=\"art\" id=\"media_13\" style=\"margin: 0px; padding: 15px 0px; border-width: 1px 0px 0px; border-top-style: solid; border-right-style: initial; border-bottom-style: initial; border-left-style: initial; border-top-color: rgb(223, 223, 223); border-right-color: initial; border-bottom-color: initial; border-left-color: initial; border-image: initial; outline: 0px; vertical-align: baseline; background: transparent; position: relative;\"><div class=\"it it0\" style=\"margin: 0px; padding: 0px 5px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; white-space: nowrap; text-align: right; width: 24px;\">13</div><div class=\"it it1\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 200px;\">財経新聞</div><div class=\"it it2\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; font-weight: bold; width: 200px;\">株式会社財経新聞社</div><div class=\"it it3\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 450px;\"><a href=\"https://www.zaikei.co.jp/releases/1052409/\" target=\"_blank\" style=\"margin: 0px; padding: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(44, 152, 181);\">https://www.zaikei.co.jp/releases/1052409/</a></div></li><li class=\"art\" id=\"media_14\" style=\"margin: 0px; padding: 15px 0px; border-width: 1px 0px 0px; border-top-style: solid; border-right-style: initial; border-bottom-style: initial; border-left-style: initial; border-top-color: rgb(223, 223, 223); border-right-color: initial; border-bottom-color: initial; border-left-color: initial; border-image: initial; outline: 0px; vertical-align: baseline; background: transparent; position: relative;\"><div class=\"it it0\" style=\"margin: 0px; padding: 0px 5px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; white-space: nowrap; text-align: right; width: 24px;\">14</div><div class=\"it it1\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 200px;\">おたくま経済新聞</div><div class=\"it it2\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; font-weight: bold; width: 200px;\">C.S.T Entertainment,Inc.</div><div class=\"it it3\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 450px;\"><a href=\"https://otakei.otakuma.net/archives/prtimes/000000002-000061428\" target=\"_blank\" style=\"margin: 0px; padding: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(44, 152, 181);\">https://otakei.otakuma.net/archives/prtimes/000000002-000061428</a></div></li><li class=\"art\" id=\"media_15\" style=\"margin: 0px; padding: 15px 0px; border-width: 1px 0px 0px; border-top-style: solid; border-right-style: initial; border-bottom-style: initial; border-left-style: initial; border-top-color: rgb(223, 223, 223); border-right-color: initial; border-bottom-color: initial; border-left-color: initial; border-image: initial; outline: 0px; vertical-align: baseline; background: transparent; position: relative;\"><div class=\"it it0\" style=\"margin: 0px; padding: 0px 5px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; white-space: nowrap; text-align: right; width: 24px;\">15</div><div class=\"it it1\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 200px;\">BIGLOBEニュース</div><div class=\"it it2\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; font-weight: bold; width: 200px;\">ビッグローブ株式会社</div><div class=\"it it3\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 450px;\"><a href=\"https://news.biglobe.ne.jp/economy/0723/prt_200723_5927780251.html\" target=\"_blank\" style=\"margin: 0px; padding: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(44, 152, 181);\">https://news.biglobe.ne.jp/economy/0723/prt_200723_5927780251.html</a></div></li><li class=\"art\" id=\"media_16\" style=\"margin: 0px; padding: 15px 0px; border-width: 1px 0px 0px; border-top-style: solid; border-right-style: initial; border-bottom-style: initial; border-left-style: initial; border-top-color: rgb(223, 223, 223); border-right-color: initial; border-bottom-color: initial; border-left-color: initial; border-image: initial; outline: 0px; vertical-align: baseline; background: transparent; position: relative;\"><div class=\"it it0\" style=\"margin: 0px; padding: 0px 5px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; white-space: nowrap; text-align: right; width: 24px;\">16</div><div class=\"it it1\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 200px;\">LINE NEWS（ラインニュース）</div><div class=\"it it2\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; font-weight: bold; width: 200px;\">LINE株式会社</div><div class=\"it it3\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 450px;\"><a href=\"https://news.line.me/articles/oa-rp31535/f5c2e8721d69\" target=\"_blank\" style=\"margin: 0px; padding: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(44, 152, 181);\">https://news.line.me/articles/oa-rp31535/f5c2e8721d69</a></div></li><li class=\"art\" id=\"media_17\" style=\"margin: 0px; padding: 15px 0px; border-width: 1px 0px 0px; border-top-style: solid; border-right-style: initial; border-bottom-style: initial; border-left-style: initial; border-top-color: rgb(223, 223, 223); border-right-color: initial; border-bottom-color: initial; border-left-color: initial; border-image: initial; outline: 0px; vertical-align: baseline; background: transparent; position: relative;\"><div class=\"it it0\" style=\"margin: 0px; padding: 0px 5px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; white-space: nowrap; text-align: right; width: 24px;\">17</div><div class=\"it it1\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 200px;\">PRESIDENT Online</div><div class=\"it it2\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; font-weight: bold; width: 200px;\">株式会社プレジデント社</div><div class=\"it it3\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 450px;\"><a href=\"https://president.jp/ud/pressrelease/5f191d2f7765611998070000\" target=\"_blank\" style=\"margin: 0px; padding: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(44, 152, 181);\">https://president.jp/ud/pressrelease/5f191d2f7765611998070000</a></div></li><li class=\"art\" id=\"media_18\" style=\"margin: 0px; padding: 15px 0px; border-width: 1px 0px 0px; border-top-style: solid; border-right-style: initial; border-bottom-style: initial; border-left-style: initial; border-top-color: rgb(223, 223, 223); border-right-color: initial; border-bottom-color: initial; border-left-color: initial; border-image: initial; outline: 0px; vertical-align: baseline; background: transparent; position: relative;\"><div class=\"it it0\" style=\"margin: 0px; padding: 0px 5px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; white-space: nowrap; text-align: right; width: 24px;\">18</div><div class=\"it it1\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 200px;\">時事ドットコム</div><div class=\"it it2\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; font-weight: bold; width: 200px;\">株式会社時事通信社</div><div class=\"it it3\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 450px;\"><a href=\"https://www.jiji.com/jc/article?k=000000002.000061428&amp;g=prt\" target=\"_blank\" style=\"margin: 0px; padding: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(44, 152, 181);\">https://www.jiji.com/jc/article?k=000000002.000061428&amp;g=prt</a></div></li><li class=\"art\" id=\"media_19\" style=\"margin: 0px; padding: 15px 0px; border-width: 1px 0px 0px; border-top-style: solid; border-right-style: initial; border-bottom-style: initial; border-left-style: initial; border-top-color: rgb(223, 223, 223); border-right-color: initial; border-bottom-color: initial; border-left-color: initial; border-image: initial; outline: 0px; vertical-align: baseline; background: transparent; position: relative;\"><div class=\"it it0\" style=\"margin: 0px; padding: 0px 5px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; white-space: nowrap; text-align: right; width: 24px;\">19</div><div class=\"it it1\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 200px;\">iza（イザ！）</div><div class=\"it it2\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; font-weight: bold; width: 200px;\">株式会社産経デジタル</div><div class=\"it it3\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 450px;\"><a href=\"http://www.iza.ne.jp/kiji/pressrelease/news/200723/prl20072314090097-n1.html\" target=\"_blank\" style=\"margin: 0px; padding: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(44, 152, 181);\">http://www.iza.ne.jp/kiji/pressrelease/news/200723/prl20072314090097-n1.html</a></div></li><li class=\"art\" id=\"media_20\" style=\"margin: 0px; padding: 15px 0px; border-width: 1px 0px 0px; border-top-style: solid; border-right-style: initial; border-bottom-style: initial; border-left-style: initial; border-top-color: rgb(223, 223, 223); border-right-color: initial; border-bottom-color: initial; border-left-color: initial; border-image: initial; outline: 0px; vertical-align: baseline; background: transparent; position: relative;\"><div class=\"it it0\" style=\"margin: 0px; padding: 0px 5px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; white-space: nowrap; text-align: right; width: 24px;\">20</div><div class=\"it it1\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 200px;\">@niftyビジネス</div><div class=\"it it2\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; font-weight: bold; width: 200px;\">ニフティ株式会社</div><div class=\"it it3\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 450px;\"><a href=\"https://business.nifty.com/cs/catalog/business_release/catalog_prt000000002000061428_1.htm\" target=\"_blank\" style=\"margin: 0px; padding: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(44, 152, 181);\">https://business.nifty.com/cs/catalog/business_release/catalog_prt000000002000061428_1.htm</a></div></li><li class=\"art\" id=\"media_21\" style=\"margin: 0px; padding: 15px 0px; border-width: 1px 0px 0px; border-top-style: solid; border-right-style: initial; border-bottom-style: initial; border-left-style: initial; border-top-color: rgb(223, 223, 223); border-right-color: initial; border-bottom-color: initial; border-left-color: initial; border-image: initial; outline: 0px; vertical-align: baseline; background: transparent; position: relative;\"><div class=\"it it0\" style=\"margin: 0px; padding: 0px 5px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; white-space: nowrap; text-align: right; width: 24px;\">21</div><div class=\"it it1\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 200px;\">JBpress（日本ビジネスプレス）</div><div class=\"it it2\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; font-weight: bold; width: 200px;\">株式会社日本ビジネスプレス</div><div class=\"it it3\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 450px;\"><a href=\"https://jbpress.ismedia.jp/ud/pressrelease/5f191d237765619b7a070000\" target=\"_blank\" style=\"margin: 0px; padding: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(44, 152, 181);\">https://jbpress.ismedia.jp/ud/pressrelease/5f191d237765619b7a070000</a></div></li><li class=\"art\" id=\"media_22\" style=\"margin: 0px; padding: 15px 0px; border-width: 1px 0px 0px; border-top-style: solid; border-right-style: initial; border-bottom-style: initial; border-left-style: initial; border-top-color: rgb(223, 223, 223); border-right-color: initial; border-bottom-color: initial; border-left-color: initial; border-image: initial; outline: 0px; vertical-align: baseline; background: transparent; position: relative;\"><div class=\"it it0\" style=\"margin: 0px; padding: 0px 5px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; white-space: nowrap; text-align: right; width: 24px;\">22</div><div class=\"it it1\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 200px;\">マピオンニュース</div><div class=\"it it2\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; font-weight: bold; width: 200px;\">株式会社マピオン</div><div class=\"it it3\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 450px;\"><a href=\"https://www.mapion.co.jp/news/release/000000002.000061428/\" target=\"_blank\" style=\"margin: 0px; padding: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(44, 152, 181);\">https://www.mapion.co.jp/news/release/000000002.000061428/</a></div></li><li class=\"art\" id=\"media_23\" style=\"margin: 0px; padding: 15px 0px; border-width: 1px 0px 0px; border-top-style: solid; border-right-style: initial; border-bottom-style: initial; border-left-style: initial; border-top-color: rgb(223, 223, 223); border-right-color: initial; border-bottom-color: initial; border-left-color: initial; border-image: initial; outline: 0px; vertical-align: baseline; background: transparent; position: relative;\"><div class=\"it it0\" style=\"margin: 0px; padding: 0px 5px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; white-space: nowrap; text-align: right; width: 24px;\">23</div><div class=\"it it1\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 200px;\">とれまがニュース</div><div class=\"it it2\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; font-weight: bold; width: 200px;\">株式会社サイトスコープ</div><div class=\"it it3\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 450px;\"><a href=\"https://news.toremaga.com/release/others/1587715.html\" target=\"_blank\" style=\"margin: 0px; padding: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(44, 152, 181);\">https://news.toremaga.com/release/others/1587715.html</a></div></li><li class=\"art\" id=\"media_24\" style=\"margin: 0px; padding: 15px 0px; border-width: 1px 0px 0px; border-top-style: solid; border-right-style: initial; border-bottom-style: initial; border-left-style: initial; border-top-color: rgb(223, 223, 223); border-right-color: initial; border-bottom-color: initial; border-left-color: initial; border-image: initial; outline: 0px; vertical-align: baseline; background: transparent; position: relative;\"><div class=\"it it0\" style=\"margin: 0px; padding: 0px 5px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; white-space: nowrap; text-align: right; width: 24px;\">24</div><div class=\"it it1\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 200px;\">エキサイトニュース</div><div class=\"it it2\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; font-weight: bold; width: 200px;\">エキサイト株式会社</div><div class=\"it it3\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 450px;\"><a href=\"https://www.excite.co.jp/news/article/Prtimes_2020-07-23-61428-2/\" target=\"_blank\" style=\"margin: 0px; padding: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(44, 152, 181);\">https://www.excite.co.jp/news/article/Prtimes_2020-07-23-61428-2/</a></div></li><li class=\"art\" id=\"media_25\" style=\"margin: 0px; padding: 15px 0px; border-width: 1px 0px 0px; border-top-style: solid; border-right-style: initial; border-bottom-style: initial; border-left-style: initial; border-top-color: rgb(223, 223, 223); border-right-color: initial; border-bottom-color: initial; border-left-color: initial; border-image: initial; outline: 0px; vertical-align: baseline; background: transparent; position: relative;\"><div class=\"it it0\" style=\"margin: 0px; padding: 0px 5px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; white-space: nowrap; text-align: right; width: 24px;\">25</div><div class=\"it it1\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 200px;\">SEOTOOLS</div><div class=\"it it2\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; font-weight: bold; width: 200px;\">ブラストホールディングス株式会社</div><div class=\"it it3\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 450px;\"><a href=\"http://www.seotools.jp/news/id_000000002.000061428.html\" target=\"_blank\" style=\"margin: 0px; padding: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(44, 152, 181);\">http://www.seotools.jp/news/id_000000002.000061428.html</a></div></li><li class=\"art\" id=\"media_26\" style=\"margin: 0px; padding: 15px 0px; border-width: 1px 0px 0px; border-top-style: solid; border-right-style: initial; border-bottom-style: initial; border-left-style: initial; border-top-color: rgb(223, 223, 223); border-right-color: initial; border-bottom-color: initial; border-left-color: initial; border-image: initial; outline: 0px; vertical-align: baseline; background: transparent; position: relative;\"><div class=\"it it0\" style=\"margin: 0px; padding: 0px 5px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; white-space: nowrap; text-align: right; width: 24px;\">26</div><div class=\"it it1\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 200px;\">Infoseekニュース</div><div class=\"it it2\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; font-weight: bold; width: 200px;\">楽天株式会社</div><div class=\"it it3\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 450px;\"><a href=\"https://news.infoseek.co.jp/article/prtimes_000000002_000061428/\" target=\"_blank\" style=\"margin: 0px; padding: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(44, 152, 181);\">https://news.infoseek.co.jp/article/prtimes_000000002_000061428/</a></div></li><li class=\"art\" id=\"media_27\" style=\"margin: 0px; padding: 15px 0px; border-width: 1px 0px 0px; border-top-style: solid; border-right-style: initial; border-bottom-style: initial; border-left-style: initial; border-top-color: rgb(223, 223, 223); border-right-color: initial; border-bottom-color: initial; border-left-color: initial; border-image: initial; outline: 0px; vertical-align: baseline; background: transparent; position: relative;\"><div class=\"it it0\" style=\"margin: 0px; padding: 0px 5px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; white-space: nowrap; text-align: right; width: 24px;\">27</div><div class=\"it it1\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 200px;\">グルメプレス</div><div class=\"it it2\" style=\"margin: 0px; padding: 0px 30px 0px 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; font-weight: bold; width: 200px;\">Labylinkグルメプレス編集部</div><div class=\"it it3\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: middle; background: transparent; display: table-cell; width: 450px;\"><a href=\"https://gourmetpress.net/379477/\" target=\"_blank\" style=\"margin: 0px; padding: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(44, 152, 181);\">https://gourmetpress.net/379477/</a></div></li></ul>',NULL,NULL,'0724004447_5f19b06f28312.jpg','Update by Ko Sugimori (23rd July 2020)','2020-07-23 15:44:52','2020-07-23 15:47:53','MARITIME PRESS','マリタイムのCBDオイル、CBDウォーターの商品情報がプレスリリースにて紹介されました。','プレスリリース, マリタイム, MARITIME, CBDオイル, CBDウォーター, PR TIMES, VALUE PRESS',NULL,NULL,NULL,NULL),
	(7,3,2,4,'blog_20200724',1,'2020-07-23 15:57:00',1,'アメリカで話題！CBDオイルの3つの効果','<p>こんにちは、マリタイムです。\r\n<br><br><span style=\"font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, &quot;Noto Sans&quot;, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;;\">「ストレスが溜まっている」「なかなか眠れない」といった悩みを抱える方に人気の「CBDオイル」。\r\n\r\n \r\n聞き慣れない方も多いかもしれませんが、実はこのCBD、アメリカでは健康効果の高さから注目を浴びている成分なのです。\r\n\r\n\r\n \r\nCBDオイルとは、CBD成分にオイルを加えたもので、舌の裏側に垂らして飲むタイプの商品が人気。\r\n\r\n \r\nまだまだ日本では馴染みがない成分のため、安全性を気にされている方もいらっしゃるでしょう。\r\n\r\n\r\n \r\nそこで今回は、「CBDオイル」の特徴や3つの効果を解説します。また、日本製のCBDオイルやCBDウォーターの商品もご紹介。\r\n\r\n \r\nストレスや睡眠の悩みがある方は、ぜひ参考にしてくださいね。&nbsp;</span><br></p><br><h2>CBDとは</h2><p>\r\n\r\n \r\nCBDとは、大麻草から抽出された「カンナビジオール」と呼ばれる天然成分です。\r\n\r\n \r\n同じく大麻草の有効成分の「テトラヒドロカンナビノール(THC)」は、大麻（マリファナ）に含まれていて、精神作用をもたらします。\r\n\r\n \r\nCBDには、大麻のように「ハイ」になる成分は入っていません。副作用もないので安心して使うことができます。\r\n\r\n \r\n健康志向ブームのアメリカでは、CBD入りのチョコレートやコーヒーなどの食品などが売られているほど注目が高まっているのです。\r\n\r\n\r\n \r\n次は、そんなCBDを使った「CBDオイル」の効果をご説明します。\r\n\r\n\r\n\r\n</p><h2><br>CBDオイルの3つの効果</h2><p>\r\n\r\n \r\nCBDオイルには、主に3つの効果があります。\r\n\r\n \r\n</p><ul>\r\n\r\n<li>1.ストレスを解消する</li>\r\n\r\n<li>2.睡眠を改善する</li>\r\n\r\n<li>3.気持ちが前向きになる<br><br></li>\r\n\r\n</ul>\r\n\r\n \r\n\r\n\r\n<h3>ストレスを解消する</h3><p>\r\n\r\n \r\n「カンナビノイド」と呼ばれる成分は、感情や心のストレスを軽減し、リラックス効果をもたらす働きがあると言われています。\r\n\r\n \r\n実際、うつ病などの精神疾患にも効果的だということが発表されました。\r\n\r\n \r\n慌ただしい生活を送っていると、知らず知らずのうちにストレスを溜めている方も多いでしょう。\r\n\r\n \r\n日々のストレスケアには、気分を穏やかにしてくれるCBDオイルが役立ちます。\r\n\r\n\r\n \r\n<br><br></p><h3>睡眠を改善する</h3><p>\r\n\r\n \r\n睡眠の質を良くすることで、不眠症にも効果的だとされているCBD。\r\n\r\n \r\nCBDの成分がカンナビノイド受容体に届くと、筋肉がゆるみ、体がゆったりと落ち着いた状態となることで、睡眠の質が良くなります。\r\n\r\n \r\n不安やイライラした感情を鎮めるCBDオイルは、朝までぐっすり眠りたい人に最適です。\r\n\r\n\r\n \r\n <br><br></p><h3>気持ちが前向きになる</h3><p>\r\n\r\n \r\nCBDは不安な気持ちを和らげ、気持ちを明るく前向きにさせてくれる効果が期待できます。\r\n\r\n \r\n幸福のホルモンと呼ばれる「セロトニン」や、やる気に関わる「ドーパミン」などの神経伝達物質を刺激するためです。\r\n\r\n \r\nまた、植物本来の成分であるカンナビジオール成分は、人間が本来持っている機能を正常に戻す働きがあります。\r\n<br><br>&nbsp;次回は【日本製】「MARITIME」のCBDオイルのメリットをご紹介します。</p><br><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">■マリタイムのLINE公式ページのお友達登録はこちらから。今なら登録で10%割引クーポンをプレゼント中！</span></p><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">URL：&nbsp;</span><a href=\"https://line.me/R/ti/p/%40838kfjfn\" target=\"_blank\" rel=\"nofollow ugc\" style=\"color: rgb(23, 68, 135); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 16.8px; padding: 0px; margin: 0px; vertical-align: baseline; outline: none; transition: color 0.1s linear 0s; font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana;\">https://line.me/R/ti/p/%40838kfjfn</a><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\"><br></span></p><br><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">■その他のマリタイム公式アカウントはこちらから：</span></p><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">ウェブサイト：</span><a href=\"https://maritime-cbd.com/\" target=\"_blank\" rel=\"nofollow ugc\" style=\"color: rgb(23, 68, 135); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 16.8px; padding: 0px; margin: 0px; vertical-align: baseline; outline: none; transition: color 0.1s linear 0s; font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana;\">https://maritime-cbd.com/</a></p><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">Twitter:&nbsp;</span><a href=\"https://twitter.com/cbdmaritime\" target=\"_blank\" rel=\"nofollow ugc\" style=\"color: rgb(23, 68, 135); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 16.8px; padding: 0px; margin: 0px; vertical-align: baseline; outline: none; transition: color 0.1s linear 0s; font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana;\">https://twitter.com/cbdmaritime</a></p><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">Instagram:&nbsp;</span><a href=\"https://www.instagram.com/cbdmaritime/\" target=\"_blank\" rel=\"nofollow ugc\" style=\"color: rgb(23, 68, 135); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 16.8px; padding: 0px; margin: 0px; vertical-align: baseline; outline: none; transition: color 0.1s linear 0s; font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana;\">https://www.instagram.com/cbdmaritime/</a></p>',NULL,NULL,'0724005941_5f19b3ed953b4.png',NULL,'2020-07-23 15:57:08','2020-08-27 20:29:08','MARITIME PRESS','CBDオイルの3つの効果と日本製のおすすめ商品をご紹介','CBDオイル, CBDとは,  カンナビジオール, ストレスを解消, .睡眠を改善, 気持ちが前向き',NULL,NULL,NULL,NULL),
	(8,3,2,4,'blog_20200730',1,'2020-07-30 09:47:00',1,'【日本製】「MARITIME」のCBDオイルのメリット','<h2><br></h2><p>\r\n\r\n日本でもさまざまなCBDオイルが売られていますが、「どれを買ったらいいの？」と悩んでいる方も多いのではないでしょうか。\r\n\r\nとくに初めて使う方にとって、不安はつきもの。\r\n\r\nここからは、安心して使える「MARITIME」のCBDオイルのメリットをご紹介します。\r\n\r\n</p><br><h3>メリット1.国の許可を得ているから安全</h3><p>\r\n\r\nCBDオイルをはじめとする「MARITIME」の商品はすべて、日本の法律基準を満たし、厚生労働省から正式な許可が得られているので安全です。\r\n\r\nさらに、定期的に厳しいチェックを受けているため、品質もしっかり保証されています。\r\n\r\n</p><br><h3>メリット2.日本製だから安心</h3><p>\r\n\r\nMARITIMEのCBDオイルの魅力は、なんと言っても純粋な日本製の商品であること。\r\n\r\n海外の製品を使うのに抵抗がある方にとって、安心できるポイントではないでしょうか？\r\n\r\n2年以上じっくりかけて開発された「MARITIME」には、製造者の想いやこだわりが詰まっています。\r\n\r\n</p><br><h3>メリット3.最高級スペイン産オリーブオイルで飲みやすい</h3><p>\r\n\r\n最高級のスペイン産のオリーブオイルを使用している「MARITIME」のCBDオイルは、フレッシュな味わいが特徴です。\r\n\r\nそのため、CBDオイルの独特な味が苦手な方でも、気軽に飲み続けられます。\r\n\r\nさらにポリフェノールをたっぷり含み、悪玉コレステロールを減らす効果があるオリーブオイルは、美容を意識したい女性にもオススメ。\r\n\r\n心も体も整えられる「MARITIME」のCBDオイルは、安心・安全・良品質だから、誰でも気軽に使えます。\r\n</p><br><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">■マリタイムのLINE公式ページのお友達登録はこちらから。今なら登録で10%割引クーポンをプレゼント中！</span></p><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">URL：&nbsp;</span><a href=\"https://line.me/R/ti/p/%40838kfjfn\" target=\"_blank\" rel=\"nofollow ugc\" style=\"color: rgb(23, 68, 135); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 16.8px; padding: 0px; margin: 0px; vertical-align: baseline; outline: none; transition: color 0.1s linear 0s; font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana;\">https://line.me/R/ti/p/%40838kfjfn</a><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\"><br></span></p><br><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">■その他のマリタイム公式アカウントはこちらから：</span></p><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">ウェブサイト：</span><a href=\"https://maritime-cbd.com/\" target=\"_blank\" rel=\"nofollow ugc\" style=\"color: rgb(23, 68, 135); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 16.8px; padding: 0px; margin: 0px; vertical-align: baseline; outline: none; transition: color 0.1s linear 0s; font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana;\">https://maritime-cbd.com/</a></p><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">Twitter:&nbsp;</span><a href=\"https://twitter.com/cbdmaritime\" target=\"_blank\" rel=\"nofollow ugc\" style=\"color: rgb(23, 68, 135); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 16.8px; padding: 0px; margin: 0px; vertical-align: baseline; outline: none; transition: color 0.1s linear 0s; font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana;\">https://twitter.com/cbdmaritime</a></p><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">Instagram:&nbsp;</span><a href=\"https://www.instagram.com/cbdmaritime/\" target=\"_blank\" rel=\"nofollow ugc\" style=\"color: rgb(23, 68, 135); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 16.8px; padding: 0px; margin: 0px; vertical-align: baseline; outline: none; transition: color 0.1s linear 0s; font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana;\">https://www.instagram.com/cbdmaritime/</a></p>',NULL,NULL,'0730185238_5f229866ba73c.jpg',NULL,'2020-07-30 09:47:58','2020-08-27 20:29:16','MARITIME','安心して使える「MARITIME」のCBDオイルのメリットをご紹介します。','日本製, マリタイム, CBDオイルのメリット, CBDオイルの品質保証, 最高級スペイン産オリーブオイル, ポリフェノール,',NULL,NULL,NULL,NULL),
	(9,3,2,4,'blog_20200730_1',1,'2020-08-07 09:56:00',1,'CBDウォーターも手軽で人気','<h3><span style=\"font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, &quot;Noto Sans&quot;, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 14px;\">いつでも手軽に摂取できる清涼飲料水「CBDウォーター」も人気です。\r\n\r\nもちろんマリファナの成分THCフリーは一切入っていません。\r\n\r\n健康を意識する人に嬉しいのは、3つの”ゼロ”。</span><br></h3><br><ul>\r\n<li>香料ゼロ</li>\r\n<li>甘味料ゼロ</li>\r\n<li>カロリーゼロ</li>\r\n</ul><p>\r\n\r\n「まずはCBDの効果を試してみたい」という方にもイチオシです。\r\n\r\n</p><br><h2>CBDオイルで心も体もリラックス！</h2><br><p>海外で注目を集めている成分を使った「CBDオイル」についてご紹介しました。\r\n\r\nCBDは不眠やストレスに対する効果が高いだけではなく、大麻（マリファナ）のような副作用がないので安心です。\r\n\r\n海外の製品を使うのに抵抗がある場合は、純国産の「MARITIME」のCBDオイルやCBDウォーターがぴったり。\r\n\r\nストレスが減って毎日ぐっすり眠れるようになれば、仕事もよりいっそう頑張れるはず。\r\n\r\n心身のリラックスに、セルフケアに、一度試してみてはいかがでしょうか？\r\n</p><br><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">■マリタイムのLINE公式ページのお友達登録はこちらから。今なら登録で10%割引クーポンをプレゼント中！</span></p><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">URL：&nbsp;</span><a href=\"https://line.me/R/ti/p/%40838kfjfn\" target=\"_blank\" rel=\"nofollow ugc\" style=\"color: rgb(23, 68, 135); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 16.8px; padding: 0px; margin: 0px; vertical-align: baseline; outline: none; transition: color 0.1s linear 0s; font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana;\">https://line.me/R/ti/p/%40838kfjfn</a><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\"><br></span></p><br><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">■その他のマリタイム公式アカウントはこちらから：</span></p><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">ウェブサイト：</span><a href=\"https://maritime-cbd.com/\" target=\"_blank\" rel=\"nofollow ugc\" style=\"color: rgb(23, 68, 135); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 16.8px; padding: 0px; margin: 0px; vertical-align: baseline; outline: none; transition: color 0.1s linear 0s; font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana;\">https://maritime-cbd.com/</a></p><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">Twitter:&nbsp;</span><a href=\"https://twitter.com/cbdmaritime\" target=\"_blank\" rel=\"nofollow ugc\" style=\"color: rgb(23, 68, 135); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 16.8px; padding: 0px; margin: 0px; vertical-align: baseline; outline: none; transition: color 0.1s linear 0s; font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana;\">https://twitter.com/cbdmaritime</a></p><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">Instagram:&nbsp;</span><a href=\"https://www.instagram.com/cbdmaritime/\" target=\"_blank\" rel=\"nofollow ugc\" style=\"color: rgb(23, 68, 135); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 16.8px; padding: 0px; margin: 0px; vertical-align: baseline; outline: none; transition: color 0.1s linear 0s; font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana;\">https://www.instagram.com/cbdmaritime/</a></p>',NULL,NULL,'0730185435_5f2298db46b02.png',NULL,'2020-07-30 09:56:40','2020-08-27 20:28:30','MARITIME','いつでも手軽に摂取できる清涼飲料水「CBDウォーター」も人気です。 もちろんマリファナの成分THCフリーは一切入っていません。  健康を意識する人に嬉しいのは、3つの”ゼロ”。','CBDウォーター, 清涼飲料水, マリファナの成分THC, THCフリー, 健康を意識する人, 香料ゼロ, 甘味料ゼロ, カロリーゼロ, 純国産,心身のリラックス, セルフケア',NULL,NULL,NULL,NULL),
	(10,3,2,5,'blog_20200820',1,'2020-08-20 09:21:00',1,'健康を意識する人に嬉しい3つの”ゼロ”が入ったCBDウォーター。マリタイムなら、CBDオイルを購入の方にもれなくプレゼントのお試しキャンペーン実施中！','<p class=\"MsoNormal\" style=\"margin: 0cm; font-size: 12pt; font-family: Calibri, sans-serif; color: rgb(0, 0, 0);\"><span lang=\"JA\" style=\"font-family: &quot;Yu Mincho&quot;, serif;\"><br>免疫システムを後押し、サポートする</span>CBD<span lang=\"JA\" style=\"font-family: &quot;Yu Mincho&quot;, serif;\">が</span>24<span lang=\"EN-US\">m</span>g<span lang=\"JA\" style=\"font-family: &quot;Yu Mincho&quot;, serif;\">注入された清涼飲料水「</span>CBD<span lang=\"JA\" style=\"font-family: &quot;Yu Mincho&quot;, serif;\">ウォーター」を販売しました。オープンニングキャンペーンとして、</span>2020<span lang=\"JA\" style=\"font-family: &quot;Yu Mincho&quot;, serif;\">年</span>9<span lang=\"JA\" style=\"font-family: &quot;Yu Mincho&quot;, serif;\">月末までにマリタイムの</span>CBD<span lang=\"JA\" style=\"font-family: &quot;Yu Mincho&quot;, serif;\">オイルを購入の方にもれなくプレゼントのお試しキャンペーンを実施中です！</span><i><o:p></o:p></i></p><span lang=\"JA\" style=\"font-family: &quot;Yu Mincho&quot;, serif;\"><span lang=\"JA\" style=\"font-family: &quot;Yu Mincho&quot;, serif;\"><br><img src=\"https://prtimes.jp/i/61428/3/resize/d61428-3-902669-6.jpg\" alt=\"\" width=\"\" style=\"font-family: -apple-system, system-ui, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, &quot;Noto Sans&quot;, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;;\"><br></span></span><p class=\"MsoNormal\" style=\"margin: 0cm; font-size: 12pt; font-family: Calibri, sans-serif; color: rgb(0, 0, 0);\"><span lang=\"JA\" style=\"font-family: &quot;Yu Mincho&quot;, serif;\">【マリタイム</span>CBD<span lang=\"JA\" style=\"font-family: &quot;Yu Mincho&quot;, serif;\">ウォーターの紹介】</span><o:p></o:p></p><p class=\"MsoNormal\" style=\"margin: 0cm; font-size: 12pt; font-family: Calibri, sans-serif; color: rgb(0, 0, 0);\"><o:p>&nbsp;</o:p></p><p class=\"MsoNormal\" style=\"margin: 0cm; font-size: 12pt; font-family: Calibri, sans-serif; color: rgb(0, 0, 0);\"><span lang=\"JA\" style=\"font-family: &quot;Yu Mincho&quot;, serif;\">天然麻由来成分</span>CBD<span lang=\"JA\" style=\"font-family: &quot;Yu Mincho&quot;, serif;\">（カンナビジオール）入りの清涼飲料水で、</span>THC<span lang=\"JA\" style=\"font-family: &quot;Yu Mincho&quot;, serif;\">（テトラヒドロカンナビノール）フリーの濃度</span>99<span lang=\"JA\" style=\"font-family: &quot;Yu Mincho&quot;, serif;\">％以上の</span>CBD<span lang=\"JA\" style=\"font-family: &quot;Yu Mincho&quot;, serif;\">を</span>24mg<span lang=\"JA\" style=\"font-family: &quot;Yu Mincho&quot;, serif;\">配合。いつでも手軽に摂取できる清涼飲料水がこの</span>CBD<span lang=\"JA\" style=\"font-family: &quot;Yu Mincho&quot;, serif;\">ウォーターです。マリタイム</span>CBD <span lang=\"JA\" style=\"font-family: &quot;Yu Mincho&quot;, serif;\">ウォーター</span> 500ml<span lang=\"JA\" style=\"font-family: &quot;Yu Mincho&quot;, serif;\">が</span>1,253<span lang=\"JA\" style=\"font-family: &quot;Yu Mincho&quot;, serif;\">円、</span><span lang=\"EN-US\">6</span><span lang=\"JA\" style=\"font-family: &quot;Yu Mincho&quot;, serif;\">本セットで</span>7,517<span lang=\"JA\" style=\"font-family: &quot;Yu Mincho&quot;, serif;\">円。毎月の定期購読（サブスク）での申し込みで最大</span>20<span lang=\"JA\" style=\"font-family: &quot;Yu Mincho&quot;, serif;\">％の割引となります。</span><o:p></o:p></p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span lang=\"EN-US\"><o:p></o:p></span><p class=\"MsoNormal\" style=\"margin: 0cm; font-size: 12pt; font-family: Calibri, sans-serif; color: rgb(0, 0, 0);\"><span lang=\"JA\" style=\"font-family: &quot;Yu Mincho&quot;, serif;\">健康を意識する人に嬉しいのは、</span>3<span lang=\"JA\" style=\"font-family: &quot;Yu Mincho&quot;, serif;\">つの”ゼロ”。</span><o:p></o:p></p><p class=\"MsoNormal\" style=\"margin: 0cm; font-size: 12pt; font-family: Calibri, sans-serif; color: rgb(0, 0, 0);\"><o:p>&nbsp;</o:p></p><p class=\"MsoNormal\" style=\"margin: 0cm; font-size: 12pt; font-family: Calibri, sans-serif; color: rgb(0, 0, 0);\"><span lang=\"JA\" style=\"font-family: &quot;Yu Mincho&quot;, serif;\">香料ゼロ｜甘味料ゼロ｜カロリーゼロ</span><o:p></o:p></p><p class=\"MsoNormal\" style=\"margin: 0cm; font-size: 12pt; font-family: Calibri, sans-serif; color: rgb(0, 0, 0);\"><o:p>&nbsp;</o:p></p><p class=\"MsoNormal\" style=\"margin: 0cm; font-size: 12pt; font-family: Calibri, sans-serif; color: rgb(0, 0, 0);\"><span lang=\"JA\" style=\"font-family: &quot;Yu Mincho&quot;, serif;\">「まずは</span>CBD<span lang=\"JA\" style=\"font-family: &quot;Yu Mincho&quot;, serif;\">の効果を試してみたい」という方にもイチオシです。</span>CBD<span lang=\"JA\" style=\"font-family: &quot;Yu Mincho&quot;, serif;\">が</span>24mg<span lang=\"JA\" style=\"font-family: &quot;Yu Mincho&quot;, serif;\">入っており、さまざまな効果を発揮します。ランニング中、ヨガ中、エクササイズ中の水分補給に最適なこの清涼飲料水。ぜひお試しください。</span><o:p></o:p></p><p class=\"MsoNormal\" style=\"margin: 0cm; font-size: 12pt; font-family: Calibri, sans-serif; color: rgb(0, 0, 0);\"><o:p>&nbsp;</o:p></p><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">■マリタイムのLINE公式ページのお友達登録はこちらから。今なら登録で10%割引クーポンをプレゼント中！</span></p><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">URL：&nbsp;</span><a href=\"https://line.me/R/ti/p/%40838kfjfn\" target=\"_blank\" rel=\"nofollow ugc\" style=\"color: rgb(23, 68, 135); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 16.8px; padding: 0px; margin: 0px; vertical-align: baseline; outline: none; transition: color 0.1s linear 0s; font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana;\">https://line.me/R/ti/p/%40838kfjfn</a><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\"><br></span></p><br><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">■その他のマリタイム公式アカウントはこちらから：</span></p><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">ウェブサイト：</span><a href=\"https://maritime-cbd.com/\" target=\"_blank\" rel=\"nofollow ugc\" style=\"color: rgb(23, 68, 135); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 16.8px; padding: 0px; margin: 0px; vertical-align: baseline; outline: none; transition: color 0.1s linear 0s; font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana;\">https://maritime-cbd.com/</a></p><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">Twitter:&nbsp;</span><a href=\"https://twitter.com/cbdmaritime\" target=\"_blank\" rel=\"nofollow ugc\" style=\"color: rgb(23, 68, 135); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 16.8px; padding: 0px; margin: 0px; vertical-align: baseline; outline: none; transition: color 0.1s linear 0s; font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana;\">https://twitter.com/cbdmaritime</a></p><p class=\"MsoNormal\" style=\"margin: 0cm; font-size: 12pt; font-family: Calibri, sans-serif; color: rgb(0, 0, 0);\"><o:p></o:p></p><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">Instagram:&nbsp;</span><a href=\"https://www.instagram.com/cbdmaritime/\" target=\"_blank\" rel=\"nofollow ugc\" style=\"color: rgb(23, 68, 135); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 16.8px; padding: 0px; margin: 0px; vertical-align: baseline; outline: none; transition: color 0.1s linear 0s; font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana;\">https://www.instagram.com/cbdmaritime/</a></p><br>',NULL,NULL,'0820182125_5f3e4095f09db.jpg',NULL,'2020-08-20 09:21:06','2020-08-27 20:28:19',NULL,'海外セレブにも人気、健康を意識する人に嬉しい3つの”ゼロ”が入ったCBDウォーター。マリタイムなら、CBDオイルを購入の方にもれなくプレゼントのお試しキャンペーン実施中！','CBDウォーター, 清涼飲料水, マリファナの成分THC, THCフリー, 健康を意識する人, 香料ゼロ, 甘味料ゼロ, カロリーゼロ, 純国産,心身のリラックス, セルフケア',NULL,NULL,NULL,NULL),
	(11,3,2,4,'blog_20200828',1,'2020-08-27 20:25:00',1,'【海外セレブにも人気のCBD】CBD商品は、アメリカやヨーロッパでも大人気です。','<p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">【海外セレブにも人気のCBD】</span></p><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">CBD商品は、アメリカやヨーロッパでも大人気です。CBDオイルとCBDウォーターの他にも、CBDチョコレート、CBDコーヒー、CBDココナッツウォーター、CBD美容クリーム、CBDマッサージオイルなど、ソーシャルメディアでも多くの支持を得ており、CBD使用を公言している海外セレブも多数います。俳優のジェニファー・アニストンや元ボクサーのマイク・タイソンなど、多くのセレブリティを顧客に持ち、ストレスや不安と常にとなり合わせのセレブやスーパーモデルや、心の痛みや不安、ストレスに毎日苛まれているやり手の政治家、ビジネスパーソンの手助けとなり、人生を成功に導くお手伝いを後押ししています。</span></p><br style=\"font-size: 16.8px; padding: 0px; margin: 0px; color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana;\"><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">【CBDオイルで心も体もリラックス！】</span></p><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">CBDは不眠やストレスに対する効果が高いだけではなく、大麻（マリファナ）のような副作用がないので安心です。 海外の製品を使うのに抵抗がある場合は、純国産のマリタイムのCBDオイルやCBDウォーターがぴったり。 ストレスが減って毎日ぐっすり眠れるようになれば、仕事もよりいっそう頑張れるはず。 心身のリラックスに、セルフケアに、一度試してみてはいかがでしょうか？</span></p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\"><br></span><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">■マリタイムのLINE公式ページのお友達登録はこちらから。今なら登録で10%割引クーポンをプレゼント中！</span></p><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">URL：&nbsp;</span><a href=\"https://line.me/R/ti/p/%40838kfjfn\" target=\"_blank\" rel=\"nofollow ugc\" style=\"color: rgb(23, 68, 135); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 16.8px; padding: 0px; margin: 0px; vertical-align: baseline; outline: none; transition: color 0.1s linear 0s; font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana;\">https://line.me/R/ti/p/%40838kfjfn</a><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\"><br></span></p><br><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">■その他のマリタイム公式アカウントはこちらから：</span></p><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">ウェブサイト：</span><a href=\"https://maritime-cbd.com/\" target=\"_blank\" rel=\"nofollow ugc\" style=\"color: rgb(23, 68, 135); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 16.8px; padding: 0px; margin: 0px; vertical-align: baseline; outline: none; transition: color 0.1s linear 0s; font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana;\">https://maritime-cbd.com/</a></p><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">Twitter:&nbsp;</span><a href=\"https://twitter.com/cbdmaritime\" target=\"_blank\" rel=\"nofollow ugc\" style=\"color: rgb(23, 68, 135); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 16.8px; padding: 0px; margin: 0px; vertical-align: baseline; outline: none; transition: color 0.1s linear 0s; font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana;\">https://twitter.com/cbdmaritime</a></p><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">Instagram:&nbsp;</span><a href=\"https://www.instagram.com/cbdmaritime/\" target=\"_blank\" rel=\"nofollow ugc\" style=\"color: rgb(23, 68, 135); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 16.8px; padding: 0px; margin: 0px; vertical-align: baseline; outline: none; transition: color 0.1s linear 0s; font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana;\">https://www.instagram.com/cbdmaritime/</a></p>',NULL,NULL,'0828052524_5f4816b43eb6b.jpg',NULL,'2020-08-27 20:25:28','2020-08-27 20:28:09','MARITIME','【海外セレブにも人気のCBD】CBD商品は、アメリカやヨーロッパでも大人気です。','CBDウォーター, 清涼飲料水, マリファナの成分THC, THCフリー, 健康を意識する人, 香料ゼロ, 甘味料ゼロ, カロリーゼロ, 純国産,心身のリラックス, セルフケア',NULL,NULL,NULL,NULL),
	(12,3,2,4,'blog_20200903',1,'2020-09-03 12:25:00',1,'CBDどれを買ったらいいの？','<p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px; background-color: transparent;\">【CBDどれを買ったらいいの？】</span></p><p style=\"background-color: transparent;\"><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px; background-color: transparent;\">日本でもさまざまなCBD商品が売られていますが、「どれを買ったらいいの？」と悩んでいる方も多いのではないでしょうか。 とくにはじめて使う方にとって、不安はつきもの。 安心して使えるマリタイムCBDのメリットをご紹介します。</span></p><br style=\"font-size: 16.8px; padding: 0px; margin: 0px; color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana;\"><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">メリット1.　国の許可を得ているから安全</span></p><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">CBDオイルやCBDウォーターをはじめとするマリタイムの原材料は日本の法律基準を満たし厚生労働省から正式な許可を得て輸入した安全なCBDアイソレートを使用しているため安全です。さらに、定期的に厳しいチェックを受けているため、品質もしっかり保証されています。</span></p><br style=\"font-size: 16.8px; padding: 0px; margin: 0px; color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana;\"><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">メリット2.　日本製だから安心</span></p><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">マリタイムCBDの魅力は、なんと言っても日本製の商品であること。 2年以上じっくりかけて開発されたマリタイムには、製造者の想いやこだわりが詰まっています。</span></p><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\"><br></span><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">メリット3.　最高級スペイン産オリーブオイルで飲みやすい</span></p><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">最高級のスペイン産のオリーブオイルを使用しているマリタイムのCBDオイルは、フレッシュな味わいが特徴です。 そのため、CBDオイルの独特な味が苦手な方でも、気軽に飲み続けられます。 さらにポリフェノールをたっぷり含み、悪玉コレステロールを減らす効果があるオリーブオイルは、美容を意識したい女性にもオススメ。 心も体も整えられるマリタイムのCBDオイルは、安心・安全・良品質だから、誰でも気軽に使えます。</span></p><br style=\"font-size: 16.8px; padding: 0px; margin: 0px; color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana;\"><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">マリタイムは公式ウェブサイト（</span><a href=\"https://maritime-cbd.com/\" target=\"_blank\" rel=\"nofollow ugc\" style=\"font-size: 16.8px; padding: 0px; margin: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(23, 68, 135); outline: none; transition: color 0.1s linear 0s; font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana;\">https://maritime-cbd.com</a><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">）のほか、楽天市場、ヤフーショッピングでも購入可能です。近日中に複数のオンラインストアや日本国内店舗でも販売開始予定です。現在販売代理店も随時募集しています。世界規模で広がるヘンプビジネスは2025年には市場規模2兆3000億円を超えるといわれています。収益率が高く小資金でCBDオイル10種類とCBDウォーターの販売からのスタートできる、卸売パッケージをご用意しています。お気軽にお問い合わせください。</span><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\"><br></span></p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\"><br></span><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">■マリタイムのLINE公式ページのお友達登録はこちらから。今なら登録で10%割引クーポンをプレゼント中！</span></p><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">URL：&nbsp;</span><a href=\"https://line.me/R/ti/p/%40838kfjfn\" target=\"_blank\" rel=\"nofollow ugc\" style=\"font-size: 16.8px; padding: 0px; margin: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(23, 68, 135); outline: none; transition: color 0.1s linear 0s; font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana;\">https://line.me/R/ti/p/%40838kfjfn</a><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\"><br></span></p><p><br></p><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">■その他のマリタイム公式アカウントはこちらから：</span></p><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">ウェブサイト：</span><a href=\"https://maritime-cbd.com/\" target=\"_blank\" rel=\"nofollow ugc\" style=\"font-size: 16.8px; padding: 0px; margin: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(23, 68, 135); outline: none; transition: color 0.1s linear 0s; font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana;\">https://maritime-cbd.com/</a></p><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">Twitter:&nbsp;</span><a href=\"https://twitter.com/cbdmaritime\" target=\"_blank\" rel=\"nofollow ugc\" style=\"font-size: 16.8px; padding: 0px; margin: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(23, 68, 135); outline: none; transition: color 0.1s linear 0s; font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana;\">https://twitter.com/cbdmaritime</a></p><p><span style=\"color: rgb(30, 30, 23); font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana; font-size: 16.8px;\">Instagram:&nbsp;</span><a href=\"https://www.instagram.com/cbdmaritime/\" target=\"_blank\" rel=\"nofollow ugc\" style=\"font-size: 16.8px; padding: 0px; margin: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(23, 68, 135); outline: none; transition: color 0.1s linear 0s; font-family: HiraKakuProN-W3, &quot;ヒラギノ角ゴ ProN W3&quot;, HiraKakuPro-W3, &quot;ヒラギノ角ゴ Pro W3&quot;, Meiryo, メイリオ, &quot;MS Pgothic&quot;, &quot;ＭＳ Ｐゴシック&quot;, Osaka, sans-serif, Helvetica, &quot;Helvetica Neue&quot;, Arial, Verdana;\">https://www.instagram.com/cbdmaritime/</a></p><br><br><br>',NULL,NULL,'0904041629_5f51410d603f7.png',NULL,'2020-08-27 20:27:49','2020-09-03 19:16:33','MARITIME','日本でもさまざまなCBD商品が売られていますが、「どれを買ったらいいの？」と悩んでいる方も多いのではないでしょうか。','CBDオイル, CBDとは,  カンナビジオール, ストレスを解消, .睡眠を改善, 気持ちが前向き',NULL,NULL,NULL,NULL),
	(13,3,2,4,'blog_20200907',1,'2020-09-07 11:17:00',1,'「安心して毎日飲み続けられるCBDオイルをつくりたい。」そんな想いから生まれた、フレッシュな味わいのMagic of CBD「MARITIME」。','<br><p style=\"background-color: transparent;\">こちらは<a href=\"https://prtimes.jp/story/detail/ZrNXG6U1qBo\">PR TIMESストーリーにて紹介された、MARITIME社オーナーのストーリー</a>を転載して紹介いたします。</p><p><a href=\"https://prtimes.jp/story/detail/ZrNXG6U1qBo\">https://prtimes.jp/story/detail/ZrNXG6U1qBo</a><br></p><br><p style=\"margin: 0px; padding: 0px; counter-reset: list-1 0 list-2 0 list-3 0 list-4 0 list-5 0 list-6 0 list-7 0 list-8 0 list-9 0; overflow-wrap: break-word; font-size: 1.8rem; line-height: 36px; color: rgb(51, 51, 51);\">CBD（カンナビジオール）をご存知でない方も多いと思います。CBDとは、大麻草やヘンプに含まれる有効成分の一つです。睡眠の質やストレスが改善されるなどの効果の高さから、世界中でますます注目が集まっています。</p><br><p style=\"margin: 0px; padding: 0px; counter-reset: list-1 0 list-2 0 list-3 0 list-4 0 list-5 0 list-6 0 list-7 0 list-8 0 list-9 0; overflow-wrap: break-word; font-size: 1.8rem; line-height: 36px; color: rgb(51, 51, 51); background-color: transparent;\">数年前、代表の私はCBDに出会い、睡眠の質が改善されたことに感動し、高品質なCBDオイルを開発することにしました。</p><br><p style=\"margin: 0px; padding: 0px; counter-reset: list-1 0 list-2 0 list-3 0 list-4 0 list-5 0 list-6 0 list-7 0 list-8 0 list-9 0; overflow-wrap: break-word; font-size: 1.8rem; line-height: 36px; color: rgb(51, 51, 51);\">弊社の第一弾CBDオイル「MARITIME」が生まれたきっかけから、開発にあたり苦労したこと、商品へのこだわりまで、お話をシェアできればと思います。</p><br><p style=\"margin: 0px; padding: 0px; counter-reset: list-1 0 list-2 0 list-3 0 list-4 0 list-5 0 list-6 0 list-7 0 list-8 0 list-9 0; overflow-wrap: break-word; font-size: 1.8rem; line-height: 36px; color: rgb(51, 51, 51);\"><span style=\"font-size: 2.4rem; font-weight: 700; font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, &quot;Noto Sans&quot;, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;;\"><br>CBDとの出会い、開発を決めるまで。</span><br></p><p style=\"margin: 0px; padding: 0px; counter-reset: list-1 0 list-2 0 list-3 0 list-4 0 list-5 0 list-6 0 list-7 0 list-8 0 list-9 0; overflow-wrap: break-word; font-size: 1.8rem; line-height: 36px; color: rgb(51, 51, 51);\">年に10回ほど日本と海外を往復し、一年の半分をアメリカで過ごす…。そんな生活を15年以上続けてきた私や弊社のメンバーは、ずっと眠りに悩まされていました。積み重なったストレスや疲れ、時差ボケなどが原因です。</p><br><p style=\"margin: 0px; padding: 0px; counter-reset: list-1 0 list-2 0 list-3 0 list-4 0 list-5 0 list-6 0 list-7 0 list-8 0 list-9 0; overflow-wrap: break-word; font-size: 1.8rem; line-height: 36px; color: rgb(51, 51, 51);\">病院で処方された睡眠誘導剤を服用していましたが、睡眠の質はいっこうに良くならず、体のだるさを感じる日々が続いていました。そんな時、アメリカでCBDにめぐりあい、CBDが持つ健康効果や可能性にとても驚きました。</p><br><p style=\"margin: 0px; padding: 0px; counter-reset: list-1 0 list-2 0 list-3 0 list-4 0 list-5 0 list-6 0 list-7 0 list-8 0 list-9 0; overflow-wrap: break-word; font-size: 1.8rem; line-height: 36px; color: rgb(51, 51, 51);\">アメリカのニュースでCBDが特集されているのを見て、まっ先にメンバーに連絡。幸運なことに、メンバーの友人で大麻農家を所有する方がいたので、貴重な情報をたくさん頂けました。その後、自分に合ったCBDオイルを見つけ出そうと、ありとあらゆる商品を使い比べてみることにしたのです。</p><br><p style=\"margin: 0px; padding: 0px; counter-reset: list-1 0 list-2 0 list-3 0 list-4 0 list-5 0 list-6 0 list-7 0 list-8 0 list-9 0; overflow-wrap: break-word; font-size: 1.8rem; line-height: 36px; color: rgb(51, 51, 51);\">CBDオイルを飲みはじめて、睡眠の質ががらりと変わり、朝起きた時にエネルギーを感じることが多くなりました。</p><br><p style=\"margin: 0px; padding: 0px; counter-reset: list-1 0 list-2 0 list-3 0 list-4 0 list-5 0 list-6 0 list-7 0 list-8 0 list-9 0; overflow-wrap: break-word; font-size: 1.8rem; line-height: 36px; color: rgb(51, 51, 51);\">しかしさまざまなCBDオイルの中で、最後まで飲みきった商品はほんのひと握り。継続して使用できるオイルに出会えなかったことが、毎日飲み続けられるCBDオイルの開発に至ったきっかけです。</p><br><p style=\"margin: 0px; padding: 0px; counter-reset: list-1 0 list-2 0 list-3 0 list-4 0 list-5 0 list-6 0 list-7 0 list-8 0 list-9 0; overflow-wrap: break-word; font-size: 1.8rem; line-height: 36px; color: rgb(51, 51, 51);\"><img class=\"ql-image\" src=\"https://public.admin-story.prtimes.jp/uploads/61428/1312/thumbnail_c502ef10-e253-11ea-8650-0bcb259afce6.jpg\" width=\"null\" data-original=\"https://public.admin-story.prtimes.jp/uploads/61428/1312/original_c502ef10-e253-11ea-8650-0bcb259afce6.jpg\" data-name=\"5706\" style=\"vertical-align: bottom; display: block; margin: 0px auto;\"></p><h2 style=\"font-size: 2.4rem; line-height: 38.4px; font-weight: 700; margin: 2em 0px 0.5em; padding: 0px; counter-reset: list-1 0 list-2 0 list-3 0 list-4 0 list-5 0 list-6 0 list-7 0 list-8 0 list-9 0; overflow-wrap: break-word; color: rgb(51, 51, 51);\">2年の時を経て誕生した、理想のCBDオイル</h2><p style=\"margin: 0px; padding: 0px; counter-reset: list-1 0 list-2 0 list-3 0 list-4 0 list-5 0 list-6 0 list-7 0 list-8 0 list-9 0; overflow-wrap: break-word; font-size: 1.8rem; line-height: 36px; color: rgb(51, 51, 51);\">開発に取り組むにあたって、まずは日本の法律基準をクリアし、安全かつ信頼できる「CBDアイソレート」と呼ばれる純粋な結晶・粉末を選別する必要があります。</p><br><p style=\"margin: 0px; padding: 0px; counter-reset: list-1 0 list-2 0 list-3 0 list-4 0 list-5 0 list-6 0 list-7 0 list-8 0 list-9 0; overflow-wrap: break-word; font-size: 1.8rem; line-height: 36px; color: rgb(51, 51, 51);\">そのため、国内・海外の業者と幾度となく面談を行ったり、さまざまな情報を収集。さらに自らありとあらゆるサンプルを飲み続けて、ようやく理想のCBDアイソレートにたどり着けました。</p><br><p style=\"margin: 0px; padding: 0px; counter-reset: list-1 0 list-2 0 list-3 0 list-4 0 list-5 0 list-6 0 list-7 0 list-8 0 list-9 0; overflow-wrap: break-word; font-size: 1.8rem; line-height: 36px; color: rgb(51, 51, 51);\">次は、商品開発にあたって、CBDオイルのベースを決めなければなりません。そこで私は100種類以上の商品を飲み比べて、CBDの含有量が同じでも、それぞれの味わいに違いがあることに気づきました。</p><br><p style=\"margin: 0px; padding: 0px; counter-reset: list-1 0 list-2 0 list-3 0 list-4 0 list-5 0 list-6 0 list-7 0 list-8 0 list-9 0; overflow-wrap: break-word; font-size: 1.8rem; line-height: 36px; color: rgb(51, 51, 51);\">試行錯誤した結果「エキストラバージンオリーブオイル」を使うことに決定。ナチュラルで味付けの必要がないこと、そして、オイル自体の栄養価がすばらしかったことが理由です。こうして、約2年の時を経て、Magic of CBD「MARITIME」が誕生しました。</p><br><img src=\"/html/user_data/Bottles-1.png\" alt=\"\" width=\"\"><p><span style=\"color: rgb(51, 51, 51); font-size: 2.4rem; font-weight: 700; font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, &quot;Noto Sans&quot;, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;;\">Magic of CBD「MARITIME」へのこだわりとは</span></p><p style=\"margin: 0px; padding: 0px; counter-reset: list-1 0 list-2 0 list-3 0 list-4 0 list-5 0 list-6 0 list-7 0 list-8 0 list-9 0; overflow-wrap: break-word; font-size: 1.8rem; line-height: 36px; color: rgb(51, 51, 51);\">「MARITIME」の最大の魅力は、その「飲みやすさ」にあります。新鮮なスペイン産オリーブオイルを使用し、CBD特有の風味をできるかぎり抑えて、すっきりとした味わいに仕上げました。口に入れた時に感じる、爽やかな香りとほどよい苦味は、まさにオリーブのフレッシュジュースのようです。</p><br><p style=\"margin: 0px; padding: 0px; counter-reset: list-1 0 list-2 0 list-3 0 list-4 0 list-5 0 list-6 0 list-7 0 list-8 0 list-9 0; overflow-wrap: break-word; font-size: 1.8rem; line-height: 36px; color: rgb(51, 51, 51);\">原材料もとてもシンプル。「エキストラバージンオリーブオイル」と「CBDアイソレート」の2種類のみで、ハイになる成分の「THC」は一切入っていません。美味しく飲める上に、安心してお使いいただける商品です。</p><br><p style=\"margin: 0px; padding: 0px; counter-reset: list-1 0 list-2 0 list-3 0 list-4 0 list-5 0 list-6 0 list-7 0 list-8 0 list-9 0; overflow-wrap: break-word; font-size: 1.8rem; line-height: 36px; color: rgb(51, 51, 51);\">また、CBDオイルは10種類のラインナップを展開し、含有量も1%から10%のものまで豊富に取り揃えています。さらに手軽にCBDを体験していただけるように、500mlのCBDウォーターも発売いたしました。</p><br><p style=\"margin: 0px; padding: 0px; counter-reset: list-1 0 list-2 0 list-3 0 list-4 0 list-5 0 list-6 0 list-7 0 list-8 0 list-9 0; overflow-wrap: break-word; font-size: 1.8rem; line-height: 36px; color: rgb(51, 51, 51);\">「MARITIME」は、開発者の私たち自身が納得いくまで追求して作り上げたブランドです。皆様にもぜひ、CBDに秘められた効果を体験していただけたら嬉しく思っております。</p><br><img src=\"/html/user_data/cbd-oil-5358400_1920.jpg\" alt=\"\" width=\"\"><br><span style=\"font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, &quot;Noto Sans&quot;, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 2.4rem; font-weight: 700;\"><br></span><p style=\"margin: 0px; padding: 0px; counter-reset: list-1 0 list-2 0 list-3 0 list-4 0 list-5 0 list-6 0 list-7 0 list-8 0 list-9 0; overflow-wrap: break-word; font-size: 1.8rem; line-height: 36px; color: rgb(51, 51, 51);\"><span style=\"font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, &quot;Noto Sans&quot;, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 2.4rem; font-weight: 700;\">安心・安全のCBDオイル「MARITIME」で健やかな暮らしを</span></p><p style=\"margin: 0px; padding: 0px; counter-reset: list-1 0 list-2 0 list-3 0 list-4 0 list-5 0 list-6 0 list-7 0 list-8 0 list-9 0; overflow-wrap: break-word; font-size: 1.8rem; line-height: 36px; color: rgb(51, 51, 51);\">世界中のCBD業界は年々拡大しており、数年後には2兆円を超える市場規模に成長する、と予想されています。しかし日本では、まったくと言っていいほど認知度がありません。</p><br><p style=\"margin: 0px; padding: 0px; counter-reset: list-1 0 list-2 0 list-3 0 list-4 0 list-5 0 list-6 0 list-7 0 list-8 0 list-9 0; overflow-wrap: break-word; font-size: 1.8rem; line-height: 36px; color: rgb(51, 51, 51);\">安全でクオリティの高いCBDを提供するために、株式会社メディファインでは、国内外の協力企業とともに、CBDを含むカンナビノイドと呼ばれる薬効成分の研究や商品開発を行っています。</p><br><p style=\"margin: 0px; padding: 0px; counter-reset: list-1 0 list-2 0 list-3 0 list-4 0 list-5 0 list-6 0 list-7 0 list-8 0 list-9 0; overflow-wrap: break-word; font-size: 1.8rem; line-height: 36px; color: rgb(51, 51, 51);\">私たちが自信を持ってオススメするCBDブランド「MARITIME」を、少しでも多くの方に喜んでいただけるよう、これからも頑張ってまいります。</p><br><p style=\"margin: 0px; padding: 0px; counter-reset: list-1 0 list-2 0 list-3 0 list-4 0 list-5 0 list-6 0 list-7 0 list-8 0 list-9 0; overflow-wrap: break-word; font-size: 1.8rem; line-height: 36px; color: rgb(51, 51, 51);\"><img class=\"ql-image\" src=\"https://public.admin-story.prtimes.jp/uploads/61428/1312/thumbnail_7b3c9e80-e254-11ea-8dd8-1bfe5331b1ce.jpg\" width=\"null\" data-original=\"https://public.admin-story.prtimes.jp/uploads/61428/1312/original_7b3c9e80-e254-11ea-8dd8-1bfe5331b1ce.jpg\" data-name=\"5711\" style=\"vertical-align: bottom; display: block; margin: 0px auto;\"></p><br style=\"font-family: YuGothic, Meiryo, system-ui, sans-serif; font-size: 18px;\">',NULL,NULL,'0904042109_5f5142253fe34.jpg',NULL,'2020-09-03 19:21:58','2020-09-13 08:14:12','MARITIME','PR TIMESストーリーにて紹介された、MARITIME社オーナーのストーリーを転載して紹介いたします。','MARITIME社オーナーのストーリー, PR TIMES, CBDオイル, CBDとは,  カンナビジオール, ストレスを解消, .睡眠を改善, 気持ちが前向き',NULL,NULL,NULL,NULL),
	(14,3,1,3,'news_20200909',1,'2020-09-08 22:21:00',1,'【抽選で100名様にマリタイムCBDウォーターをプレゼントのインスタグラムキャンペーン】','<p style=\"background-color: transparent;\">【抽選で100名様にマリタイムCBDウォーターをプレゼントのインスタグラムキャンペーン】</p><p>.</p><p>株式会社メディファインは、2020年の9月9日（水）から11日（金）までの３日間、インテックス大阪にて行われる　第１回国際化粧品展大阪COSME OSAKA 2020にマリタイムCBDとして出展をいたします。今回の第１回国際化粧品展大阪　COSME OSAKA 2020への出展を記念、そしてマリタイムCBD<span style=\"background-color: transparent; font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, &quot;Noto Sans&quot;, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;;\">オープン記念キャンペーン第二弾として、100名様にマリタイムCBDウォーターをプレゼントするインスタグラムキャンペーンを開催いたします。@cosme_osaka</span></p><p>.</p><p>キャンペーン期間：2020年9月9日（水）～2020年9月30日（水）</p><p>ご対象者様：マリタイム CBDのインスタグラムアカウント（@cbdmaritime）をフォローし、投稿にいいね！をつければ応募完了です。ストーリー共有や再投稿（リポスト）、#マリタイムCBDウォーター（タグ）をつけてご投稿してくださった方には当選確率が倍増します。当選者には@cbdmaritimeアカウントよりDM（ダイレクメッセージ）にてご連絡をいたします。</p><br><p>【マリタイム CBDウォーターキャンペーン応募規約】</p><p>・多くのお客様にプレゼントさせて頂きたい為、お一人様につき一回のご応募をお願いいたします。</p><p>・キャンペーン期間前、期間後のプレゼントはお断りさせて頂いております。</p><p>・本プレゼントのご返品、現金との交換はお断りいたします。&nbsp;</p><p>・非公開アカウント、抽選時にフォローを外した方は対象外です。</p><p>・DMにて当選通知後に弊社指定の期限までにお届け先の情報を記載いただきます。</p><p>・当選商品の発送は日本国内に限らせていただきます。</p><p>・記載いただいた住所や名前に誤りがあった場合、不在によりお受け取りが不可能な場合、当選無効とさせていただく場合もございます。</p>&nbsp;',NULL,NULL,'0909072035_5f5803b34af77.jpg',NULL,'2020-09-08 22:21:35','2020-09-10 12:23:52','MARITIME','株式会社メディファインは、2020年の9月9日（水）から11日（金）までの３日間、インテックス大阪にて行われる　第１回国際化粧品展大阪COSME OSAKA 2020にマリタイムCBDとして出展をいたします。今回の第１回国際化粧品展大阪　COSME OSAKA 2020への出展を記念、そしてマリタイムCBDオープン記念キャンペーン第二弾として、100名様にマリタイムCBDウォーターをプレゼントするインスタグラムキャンペーンを開催いたします。','マリタイムCBDウォーター, インスタグラムキャンペーン',NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `plg_taba_cms_post` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table plg_taba_cms_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plg_taba_cms_type`;

CREATE TABLE `plg_taba_cms_type` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT,
  `data_key` varchar(255) NOT NULL,
  `public_div` int(11) NOT NULL,
  `type_name` varchar(255) NOT NULL,
  `edit_div` int(11) NOT NULL,
  `memo` longtext DEFAULT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetime)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetime)',
  `public_id` int(11) NOT NULL,
  PRIMARY KEY (`type_id`),
  UNIQUE KEY `UNIQ_E73E894BCE1C4A1C` (`data_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `plg_taba_cms_type` WRITE;
/*!40000 ALTER TABLE `plg_taba_cms_type` DISABLE KEYS */;

INSERT INTO `plg_taba_cms_type` (`type_id`, `data_key`, `public_div`, `type_name`, `edit_div`, `memo`, `create_date`, `update_date`, `public_id`)
VALUES
	(1,'news',1,'新着情報',0,NULL,'2020-05-06 09:38:57','2020-05-06 09:38:57',2),
	(2,'blog',1,'ブログ',0,NULL,'2020-05-06 09:38:57','2020-05-06 09:38:57',2);

/*!40000 ALTER TABLE `plg_taba_cms_type` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
