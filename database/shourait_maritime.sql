-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 25, 2020 at 04:10 AM
-- Server version: 5.6.36-82.1-log
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shourait_maritime`
--

-- --------------------------------------------------------

--
-- Table structure for table `dtb_authority_role`
--

CREATE TABLE `dtb_authority_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `authority_id` smallint(5) UNSIGNED DEFAULT NULL,
  `creator_id` int(10) UNSIGNED DEFAULT NULL,
  `deny_url` varchar(4000) NOT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtb_authority_role`
--

INSERT INTO `dtb_authority_role` (`id`, `authority_id`, `creator_id`, `deny_url`, `create_date`, `update_date`, `discriminator_type`) VALUES
(1, 1, 1, '/admin_maritime/setting/', '2020-07-20 06:55:19', '2020-07-20 06:55:19', 'authorityrole');

-- --------------------------------------------------------

--
-- Table structure for table `dtb_base_info`
--

CREATE TABLE `dtb_base_info` (
  `id` int(10) UNSIGNED NOT NULL,
  `country_id` smallint(5) UNSIGNED DEFAULT NULL,
  `pref_id` smallint(5) UNSIGNED DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `company_kana` varchar(255) DEFAULT NULL,
  `postal_code` varchar(8) DEFAULT NULL,
  `addr01` varchar(255) DEFAULT NULL,
  `addr02` varchar(255) DEFAULT NULL,
  `phone_number` varchar(14) DEFAULT NULL,
  `business_hour` varchar(255) DEFAULT NULL,
  `email01` varchar(255) DEFAULT NULL,
  `email02` varchar(255) DEFAULT NULL,
  `email03` varchar(255) DEFAULT NULL,
  `email04` varchar(255) DEFAULT NULL,
  `shop_name` varchar(255) DEFAULT NULL,
  `shop_kana` varchar(255) DEFAULT NULL,
  `shop_name_eng` varchar(255) DEFAULT NULL,
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `good_traded` varchar(4000) DEFAULT NULL,
  `message` varchar(4000) DEFAULT NULL,
  `delivery_free_amount` decimal(12,2) UNSIGNED DEFAULT NULL,
  `delivery_free_quantity` int(10) UNSIGNED DEFAULT NULL,
  `option_mypage_order_status_display` tinyint(1) NOT NULL DEFAULT '1',
  `option_nostock_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `option_favorite_product` tinyint(1) NOT NULL DEFAULT '1',
  `option_product_delivery_fee` tinyint(1) NOT NULL DEFAULT '0',
  `option_product_tax_rule` tinyint(1) NOT NULL DEFAULT '0',
  `option_customer_activate` tinyint(1) NOT NULL DEFAULT '1',
  `option_remember_me` tinyint(1) NOT NULL DEFAULT '1',
  `authentication_key` varchar(255) DEFAULT NULL,
  `php_path` varchar(255) DEFAULT NULL,
  `option_point` tinyint(1) NOT NULL DEFAULT '1',
  `basic_point_rate` decimal(10,0) UNSIGNED DEFAULT '1',
  `point_conversion_rate` decimal(10,0) UNSIGNED DEFAULT '1',
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtb_base_info`
--

INSERT INTO `dtb_base_info` (`id`, `country_id`, `pref_id`, `company_name`, `company_kana`, `postal_code`, `addr01`, `addr02`, `phone_number`, `business_hour`, `email01`, `email02`, `email03`, `email04`, `shop_name`, `shop_kana`, `shop_name_eng`, `update_date`, `good_traded`, `message`, `delivery_free_amount`, `delivery_free_quantity`, `option_mypage_order_status_display`, `option_nostock_hidden`, `option_favorite_product`, `option_product_delivery_fee`, `option_product_tax_rule`, `option_customer_activate`, `option_remember_me`, `authentication_key`, `php_path`, `option_point`, `basic_point_rate`, `point_conversion_rate`, `discriminator_type`) VALUES
(1, NULL, 28, '株式会社メディファイン', 'メディファイン', '6500013', '神戸市中央区花隈町', '5-21-1110号', '0787777000', '10:00～19:00（水曜定休）', 'maritime@shourai.io', 'maritime@shourai.io', 'maritime@shourai.io', 'maritime@shourai.io', 'MARITIME', 'マリタイム', 'MARITIME', '2020-08-05 13:23:08', 'admin.good_traded_txt', 'admin.shop_message_txt', NULL, NULL, 1, 0, 1, 0, 0, 1, 1, '5b5acb396ec335b1e72cbb94033e406721949a47', NULL, 1, '1', '1', 'baseinfo');

-- --------------------------------------------------------

--
-- Table structure for table `dtb_block`
--

CREATE TABLE `dtb_block` (
  `id` int(10) UNSIGNED NOT NULL,
  `device_type_id` smallint(5) UNSIGNED DEFAULT NULL,
  `block_name` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) NOT NULL,
  `use_controller` tinyint(1) NOT NULL DEFAULT '0',
  `deletable` tinyint(1) NOT NULL DEFAULT '1',
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtb_block`
--

INSERT INTO `dtb_block` (`id`, `device_type_id`, `block_name`, `file_name`, `use_controller`, `deletable`, `create_date`, `update_date`, `discriminator_type`) VALUES
(1, 10, 'カート', 'cart', 0, 0, '2017-03-07 10:14:52', '2017-03-07 10:14:52', 'block'),
(2, 10, 'カテゴリ', 'category', 0, 0, '2017-03-07 10:14:52', '2017-03-07 10:14:52', 'block'),
(3, 10, 'カテゴリナビ(PC)', 'category_nav_pc', 0, 0, '2017-03-07 10:14:52', '2017-03-07 10:14:52', 'block'),
(4, 10, 'カテゴリナビ(SP)', 'category_nav_sp', 0, 0, '2017-03-07 10:14:52', '2017-03-07 10:14:52', 'block'),
(5, 10, '新入荷商品特集', 'eyecatch', 0, 0, '2017-03-07 10:14:52', '2017-03-07 10:14:52', 'block'),
(6, 10, 'フッター', 'footer', 0, 0, '2017-03-07 10:14:52', '2017-03-07 10:14:52', 'block'),
(7, 10, 'ヘッダー(トップページ）', 'header', 0, 0, '2017-03-07 10:14:52', '2020-04-29 00:54:14', 'block'),
(8, 10, 'ログインナビ(共通)', 'login', 0, 0, '2017-03-07 10:14:52', '2017-03-07 10:14:52', 'block'),
(9, 10, 'ログインナビ(SP)', 'login_sp', 0, 0, '2017-03-07 10:14:52', '2017-03-07 10:14:52', 'block'),
(10, 10, 'ロゴ', 'logo', 0, 0, '2017-03-07 10:14:52', '2017-03-07 10:14:52', 'block'),
(11, 10, '新着商品', 'new_item', 0, 0, '2017-03-07 10:14:52', '2017-03-07 10:14:52', 'block'),
(12, 10, '新着情報(original)', 'news', 0, 0, '2017-03-07 10:14:52', '2020-05-11 09:46:27', 'block'),
(13, 10, '商品検索', 'search_product', 1, 0, '2017-03-07 10:14:52', '2017-03-07 10:14:52', 'block'),
(14, 10, 'トピック', 'topic', 0, 0, '2017-03-07 10:14:52', '2017-03-07 10:14:52', 'block'),
(15, 10, 'CBD Oils', 'product_slide', 0, 1, '2020-04-26 04:57:04', '2020-04-29 00:31:11', 'block'),
(16, 10, 'CBD Water', 'water_product', 0, 1, '2020-04-26 04:58:38', '2020-04-26 04:58:38', 'block'),
(17, 10, 'How it works', 'how_it_works', 0, 1, '2020-04-26 05:00:39', '2020-04-26 05:00:39', 'block'),
(18, 10, 'Join newsletter', 'join_newsletter', 0, 1, '2020-04-26 05:01:45', '2020-04-29 00:27:57', 'block'),
(19, 10, 'Maritime goal', 'owner_message', 0, 1, '2020-04-26 06:32:38', '2020-04-26 06:32:38', 'block'),
(20, 10, 'Usage recommendation', 'usage_recommendation', 0, 1, '2020-04-26 06:34:03', '2020-04-26 06:35:48', 'block'),
(21, 10, 'Introduction', 'introduction', 0, 1, '2020-04-26 06:36:52', '2020-04-26 06:36:52', 'block'),
(23, 10, '新着情報一覧', 'news_list', 0, 1, '2020-05-11 09:47:22', '2020-05-11 10:09:02', 'block'),
(24, 10, 'ブログ一覧', 'blog_list', 0, 1, '2020-05-11 10:09:54', '2020-05-11 10:09:54', 'block'),
(25, 10, 'カテゴリー商品一覧', 'category_products', 0, 1, '2020-05-13 09:04:25', '2020-05-13 09:04:25', 'block'),
(28, 10, 'メールマガジン(Popup)', 'newsletter', 0, 1, '2020-08-19 10:03:49', '2020-08-21 00:28:45', 'block'),
(29, 10, 'PayPalロゴ', 'paypal_logo', 0, 1, '2020-09-02 03:35:42', '2020-09-02 03:35:42', 'block'),
(30, 10, 'パンくずプラグイン', 'BreadcrumbList4', 1, 1, '2020-09-02 09:35:37', '2020-09-02 09:35:37', 'block'),
(31, 10, 'Video Introduction', 'video_intro', 0, 1, '2020-09-10 02:39:11', '2020-09-10 02:39:11', 'block'),
(32, 10, 'Reviews slide', 'review_slide', 0, 1, '2020-09-29 10:25:32', '2020-09-29 10:25:32', 'block'),
(33, 10, '記事一覧', 'articles_overview', 0, 1, '2020-10-11 05:20:42', '2020-10-11 05:45:33', 'block');

-- --------------------------------------------------------

--
-- Table structure for table `dtb_block_position`
--

CREATE TABLE `dtb_block_position` (
  `section` int(10) UNSIGNED NOT NULL,
  `block_id` int(10) UNSIGNED NOT NULL,
  `layout_id` int(10) UNSIGNED NOT NULL,
  `block_row` int(10) UNSIGNED DEFAULT NULL,
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtb_block_position`
--

INSERT INTO `dtb_block_position` (`section`, `block_id`, `layout_id`, `block_row`, `discriminator_type`) VALUES
(3, 3, 0, 2, 'blockposition'),
(3, 7, 0, 0, 'blockposition'),
(3, 7, 1, 0, 'blockposition'),
(3, 7, 2, 0, 'blockposition'),
(3, 7, 3, 0, 'blockposition'),
(3, 7, 4, 0, 'blockposition'),
(3, 7, 5, 0, 'blockposition'),
(3, 7, 7, 0, 'blockposition'),
(3, 10, 0, 1, 'blockposition'),
(4, 2, 3, 1, 'blockposition'),
(4, 30, 3, 0, 'blockposition'),
(6, 30, 2, 0, 'blockposition'),
(6, 30, 4, 0, 'blockposition'),
(6, 30, 5, 0, 'blockposition'),
(6, 30, 7, 0, 'blockposition'),
(7, 15, 0, 2, 'blockposition'),
(7, 15, 1, 4, 'blockposition'),
(7, 16, 0, 3, 'blockposition'),
(7, 16, 1, 5, 'blockposition'),
(7, 17, 0, 4, 'blockposition'),
(7, 17, 1, 6, 'blockposition'),
(7, 17, 4, 0, 'blockposition'),
(7, 18, 0, 6, 'blockposition'),
(7, 19, 0, 5, 'blockposition'),
(7, 19, 1, 7, 'blockposition'),
(7, 20, 0, 1, 'blockposition'),
(7, 20, 1, 3, 'blockposition'),
(7, 21, 0, 0, 'blockposition'),
(7, 21, 1, 0, 'blockposition'),
(7, 23, 1, 9, 'blockposition'),
(7, 24, 1, 8, 'blockposition'),
(7, 25, 4, 1, 'blockposition'),
(7, 31, 1, 2, 'blockposition'),
(7, 32, 1, 1, 'blockposition'),
(7, 33, 7, 0, 'blockposition'),
(9, 18, 1, 0, 'blockposition'),
(9, 18, 2, 0, 'blockposition'),
(9, 18, 3, 0, 'blockposition'),
(9, 18, 4, 0, 'blockposition'),
(9, 18, 5, 0, 'blockposition'),
(9, 18, 7, 0, 'blockposition'),
(10, 6, 0, 0, 'blockposition'),
(10, 6, 1, 0, 'blockposition'),
(10, 6, 2, 0, 'blockposition'),
(10, 6, 3, 0, 'blockposition'),
(10, 6, 4, 0, 'blockposition'),
(10, 6, 5, 0, 'blockposition'),
(10, 6, 7, 0, 'blockposition'),
(10, 29, 1, 1, 'blockposition'),
(10, 29, 2, 1, 'blockposition'),
(11, 4, 0, 1, 'blockposition'),
(11, 4, 1, 1, 'blockposition'),
(11, 4, 2, 1, 'blockposition'),
(11, 4, 3, 1, 'blockposition'),
(11, 4, 4, 1, 'blockposition'),
(11, 4, 5, 1, 'blockposition'),
(11, 4, 7, 1, 'blockposition'),
(11, 9, 0, 2, 'blockposition'),
(11, 9, 1, 2, 'blockposition'),
(11, 9, 2, 2, 'blockposition'),
(11, 9, 3, 2, 'blockposition'),
(11, 9, 4, 2, 'blockposition'),
(11, 9, 5, 2, 'blockposition'),
(11, 9, 7, 2, 'blockposition'),
(11, 13, 0, 0, 'blockposition'),
(11, 13, 1, 0, 'blockposition'),
(11, 13, 2, 0, 'blockposition'),
(11, 13, 3, 0, 'blockposition'),
(11, 13, 4, 0, 'blockposition'),
(11, 13, 5, 0, 'blockposition'),
(11, 13, 7, 0, 'blockposition'),
(11, 28, 1, 3, 'blockposition');

-- --------------------------------------------------------

--
-- Table structure for table `dtb_cart`
--

CREATE TABLE `dtb_cart` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED DEFAULT NULL,
  `cart_key` varchar(255) DEFAULT NULL,
  `pre_order_id` varchar(255) DEFAULT NULL,
  `total_price` decimal(12,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `delivery_fee_total` decimal(12,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `sort_no` smallint(5) UNSIGNED DEFAULT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `add_point` decimal(12,0) UNSIGNED NOT NULL DEFAULT '0',
  `use_point` decimal(12,0) UNSIGNED NOT NULL DEFAULT '0',
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtb_cart`
--

INSERT INTO `dtb_cart` (`id`, `customer_id`, `cart_key`, `pre_order_id`, `total_price`, `delivery_fee_total`, `sort_no`, `create_date`, `update_date`, `add_point`, `use_point`, `discriminator_type`) VALUES
(249, NULL, 'Hk4hibxyFPmg5BiktstgXSQrNqvPaer8_1', NULL, '5280.00', '0.00', NULL, '2020-08-18 14:30:05', '2020-08-18 14:30:05', '0', '0', 'cart'),
(250, NULL, 'bwxApYwr9lMXE4LnI1DSuG0epMrJbsLN_1', NULL, '5280.00', '0.00', NULL, '2020-08-18 14:33:56', '2020-08-18 14:33:56', '0', '0', 'cart'),
(251, NULL, '0uqhpSfi14sb7RjOj0nx0GRHHWcYJ1zk_1', 'd6a4296a18606ba92e6c4e409941ee09863ab35e', '5280.00', '0.00', NULL, '2020-08-18 14:36:48', '2020-08-18 14:38:23', '0', '0', 'cart'),
(252, NULL, 'FJ75wI259sh6Z6j5vaYzyLzydyxJ6lsW_1', '2686b1a4dcfa9af5c0298afa4fe9ad7b038dd849', '5280.00', '0.00', NULL, '2020-08-18 14:44:16', '2020-08-18 14:44:50', '0', '0', 'cart'),
(258, 4, '4_1', NULL, '5280.00', '0.00', NULL, '2020-08-19 10:46:10', '2020-08-19 10:46:10', '0', '0', 'cart'),
(262, NULL, 'fAsMiE0Su8tKroM10TMOLUx7EV92qjzi_1', NULL, '25300.00', '0.00', NULL, '2020-08-20 08:32:29', '2020-08-20 08:47:48', '0', '0', 'cart'),
(264, NULL, 'Ma310XDoiarvH1IucQBhCOJaYHyHhmbC_1', NULL, '5280.00', '0.00', NULL, '2020-08-20 10:14:46', '2020-08-20 10:17:17', '0', '0', 'cart'),
(268, NULL, 'NaResCZVw1ZERgtH6LxkKvf0C5lFqhw5_1', NULL, '10560.00', '0.00', NULL, '2020-08-25 00:26:17', '2020-08-25 00:26:23', '0', '0', 'cart'),
(276, NULL, 'UigWP340vxB5y4itEelgjJ2Dsc1eA5n7_1', 'a8cf3cabe148302bd249cacd71bcbcd792177da9', '1276.00', '0.00', NULL, '2020-09-01 11:23:26', '2020-09-01 11:23:54', '0', '0', 'cart'),
(284, NULL, 'iut5OYDSGJtms9FYLge2FNMhRGzqZYv8_1', NULL, '5280.00', '0.00', NULL, '2020-09-02 05:08:45', '2020-09-02 05:08:48', '0', '0', 'cart'),
(293, NULL, 'uxCfGnkbumXSioYgBY4FUL0MoA5Ky8BH_1', NULL, '5280.00', '0.00', NULL, '2020-09-09 23:19:00', '2020-09-09 23:19:03', '0', '0', 'cart'),
(307, 1, '1_1', NULL, '30888.00', '0.00', NULL, '2020-09-18 06:21:09', '2020-09-18 06:21:09', '0', '0', 'cart'),
(308, NULL, 'KcmUGQaLJZlnra8f7MwwfmXAUJp40jlz_1', '152c5584dcc11d4f41a32c915105948e83784d57', '5280.00', '0.00', NULL, '2020-09-28 02:05:09', '2020-10-01 09:17:15', '0', '0', 'cart'),
(309, NULL, 'tNtxY0Uo1zz25LpAUghn9sa5jMSN61yn_1', NULL, '5280.00', '0.00', NULL, '2020-09-30 10:39:46', '2020-09-30 10:39:49', '0', '0', 'cart'),
(310, NULL, 'PtdhzaU40RfVPCokMYY8y3EnRyOSRQsB_1', NULL, '35750.00', '0.00', NULL, '2020-09-30 10:47:30', '2020-09-30 10:47:33', '0', '0', 'cart'),
(311, NULL, 'N4yibWNPiTQZ91pBnQiSgN4IWHQwqx7i_1', '45c971fe6da159cb844e467a8bad282d48f08548', '5280.00', '0.00', NULL, '2020-10-01 09:22:07', '2020-10-01 09:37:05', '0', '0', 'cart'),
(312, NULL, 'wgj2gq74Y12fqaL2yTVJpNie0E21srFZ_1', NULL, '5280.00', '0.00', NULL, '2020-10-23 06:31:58', '2020-10-23 06:32:02', '0', '0', 'cart');

-- --------------------------------------------------------

--
-- Table structure for table `dtb_cart_item`
--

CREATE TABLE `dtb_cart_item` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_class_id` int(10) UNSIGNED DEFAULT NULL,
  `cart_id` int(10) UNSIGNED DEFAULT NULL,
  `price` decimal(12,2) NOT NULL DEFAULT '0.00',
  `quantity` decimal(10,0) NOT NULL DEFAULT '0',
  `point_rate` decimal(10,0) UNSIGNED DEFAULT NULL,
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtb_cart_item`
--

INSERT INTO `dtb_cart_item` (`id`, `product_class_id`, `cart_id`, `price`, `quantity`, `point_rate`, `discriminator_type`) VALUES
(300, 44, 258, '5280.00', '1', NULL, 'cartitem'),
(304, 44, 262, '5280.00', '1', NULL, 'cartitem'),
(307, 44, 264, '5280.00', '1', NULL, 'cartitem'),
(326, 38, 276, '1276.00', '1', NULL, 'cartitem'),
(334, 44, 284, '5280.00', '1', NULL, 'cartitem'),
(357, 40, 307, '26136.00', '1', NULL, 'cartitem'),
(358, 44, 307, '4752.00', '1', NULL, 'cartitem'),
(359, 44, 308, '5280.00', '1', NULL, 'cartitem'),
(360, 44, 309, '5280.00', '1', NULL, 'cartitem'),
(361, 48, 310, '35750.00', '1', NULL, 'cartitem'),
(362, 44, 311, '5280.00', '1', NULL, 'cartitem'),
(363, 44, 312, '5280.00', '1', NULL, 'cartitem');

-- --------------------------------------------------------

--
-- Table structure for table `dtb_category`
--

CREATE TABLE `dtb_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_category_id` int(10) UNSIGNED DEFAULT NULL,
  `creator_id` int(10) UNSIGNED DEFAULT NULL,
  `category_name` varchar(255) NOT NULL,
  `hierarchy` int(10) UNSIGNED NOT NULL,
  `sort_no` int(11) NOT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtb_category`
--

INSERT INTO `dtb_category` (`id`, `parent_category_id`, `creator_id`, `category_name`, `hierarchy`, `sort_no`, `create_date`, `update_date`, `discriminator_type`) VALUES
(1, NULL, 1, 'CBD商品', 1, 13, '2017-03-07 10:14:52', '2020-06-23 08:27:22', 'category'),
(3, 1, 1, 'CBDオイル', 2, 12, '2017-03-07 10:14:52', '2020-05-09 10:31:10', 'category'),
(7, 1, 1, 'CBDウォーター', 2, 2, '2020-04-28 23:49:55', '2020-05-15 06:55:57', 'category'),
(15, 1, 1, 'その他', 2, 1, '2020-05-09 10:31:04', '2020-05-09 10:31:10', 'category'),
(19, 31, 1, '10ml', 4, 9, '2020-05-10 03:48:48', '2020-06-17 04:53:28', 'category'),
(20, 31, 1, '30ml', 4, 3, '2020-05-10 03:48:54', '2020-06-17 04:53:28', 'category'),
(21, 32, 1, '10%（高濃度をお求めの方）', 4, 4, '2020-05-10 03:50:11', '2020-06-17 10:19:40', 'category'),
(22, 32, 1, '5%（実感がなく少し物足りない方）', 4, 5, '2020-05-10 03:50:19', '2020-10-17 03:20:54', 'category'),
(23, 32, 1, '3%（一番人気がありMARITIMEお勧め商品です）', 4, 6, '2020-05-10 03:50:24', '2020-06-17 05:22:03', 'category'),
(24, 32, 1, '2%（はじめての方と薄めがいい方）', 4, 7, '2020-05-10 03:50:31', '2020-06-17 05:21:42', 'category'),
(25, 32, 1, '1%（はじめての方に）', 4, 8, '2020-05-10 03:50:37', '2020-06-17 05:21:13', 'category'),
(31, 3, 1, '内容量', 3, 10, '2020-06-17 04:47:49', '2020-06-17 04:47:49', 'category'),
(32, 3, 1, 'CBD％', 3, 11, '2020-06-17 04:48:35', '2020-06-17 04:48:35', 'category');

-- --------------------------------------------------------

--
-- Table structure for table `dtb_class_category`
--

CREATE TABLE `dtb_class_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `class_name_id` int(10) UNSIGNED DEFAULT NULL,
  `creator_id` int(10) UNSIGNED DEFAULT NULL,
  `backend_name` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` int(10) UNSIGNED NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtb_class_category`
--

INSERT INTO `dtb_class_category` (`id`, `class_name_id`, `creator_id`, `backend_name`, `name`, `sort_no`, `visible`, `create_date`, `update_date`, `discriminator_type`) VALUES
(1, 1, 1, '小', '小', 2, 0, '2020-05-10 04:38:27', '2020-05-15 01:44:59', 'classcategory'),
(2, 1, 1, '大', '大', 1, 0, '2020-05-10 04:38:41', '2020-05-15 01:44:55', 'classcategory'),
(3, 2, 1, '通常購入', '通常購入', 2, 1, '2020-05-14 22:22:16', '2020-05-14 22:28:19', 'classcategory'),
(4, 2, 1, '定期購買', '定期購買', 1, 1, '2020-05-14 22:22:42', '2020-05-14 22:28:35', 'classcategory');

-- --------------------------------------------------------

--
-- Table structure for table `dtb_class_name`
--

CREATE TABLE `dtb_class_name` (
  `id` int(10) UNSIGNED NOT NULL,
  `creator_id` int(10) UNSIGNED DEFAULT NULL,
  `backend_name` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` int(10) UNSIGNED NOT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtb_class_name`
--

INSERT INTO `dtb_class_name` (`id`, `creator_id`, `backend_name`, `name`, `sort_no`, `create_date`, `update_date`, `discriminator_type`) VALUES
(1, 1, 'サイズ', 'サイズ', 1, '2020-05-10 04:36:55', '2020-05-10 04:37:46', 'classname'),
(2, 1, '販売種別', '販売種別', 2, '2020-05-14 22:21:43', '2020-05-14 22:21:43', 'classname');

-- --------------------------------------------------------

--
-- Table structure for table `dtb_csv`
--

CREATE TABLE `dtb_csv` (
  `id` int(10) UNSIGNED NOT NULL,
  `csv_type_id` smallint(5) UNSIGNED DEFAULT NULL,
  `creator_id` int(10) UNSIGNED DEFAULT NULL,
  `entity_name` varchar(255) NOT NULL,
  `field_name` varchar(255) NOT NULL,
  `reference_field_name` varchar(255) DEFAULT NULL,
  `disp_name` varchar(255) NOT NULL,
  `sort_no` smallint(5) UNSIGNED NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtb_csv`
--

INSERT INTO `dtb_csv` (`id`, `csv_type_id`, `creator_id`, `entity_name`, `field_name`, `reference_field_name`, `disp_name`, `sort_no`, `enabled`, `create_date`, `update_date`, `discriminator_type`) VALUES
(1, 1, NULL, 'Eccube\\\\Entity\\\\Product', 'id', NULL, '商品ID', 1, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(2, 1, NULL, 'Eccube\\\\Entity\\\\Product', 'Status', 'id', '公開ステータス(ID)', 2, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(3, 1, NULL, 'Eccube\\\\Entity\\\\Product', 'Status', 'name', '公開ステータス(名称)', 3, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(4, 1, NULL, 'Eccube\\\\Entity\\\\Product', 'name', NULL, '商品名', 4, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(5, 1, NULL, 'Eccube\\\\Entity\\\\Product', 'note', NULL, 'ショップ用メモ欄', 5, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(6, 1, NULL, 'Eccube\\\\Entity\\\\Product', 'description_list', NULL, '商品説明(一覧)', 6, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(7, 1, NULL, 'Eccube\\\\Entity\\\\Product', 'description_detail', NULL, '商品説明(詳細)', 7, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(8, 1, NULL, 'Eccube\\\\Entity\\\\Product', 'search_word', NULL, '検索ワード', 8, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(9, 1, NULL, 'Eccube\\\\Entity\\\\Product', 'free_area', NULL, 'フリーエリア', 9, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(10, 1, NULL, 'Eccube\\\\Entity\\\\ProductClass', 'id', NULL, '商品規格ID', 10, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(11, 1, NULL, 'Eccube\\\\Entity\\\\ProductClass', 'SaleType', 'id', '販売種別(ID)', 11, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(12, 1, NULL, 'Eccube\\\\Entity\\\\ProductClass', 'SaleType', 'name', '販売種別(名称)', 12, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(13, 1, NULL, 'Eccube\\\\Entity\\\\ProductClass', 'ClassCategory1', 'id', '規格分類1(ID)', 13, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(14, 1, NULL, 'Eccube\\\\Entity\\\\ProductClass', 'ClassCategory1', 'name', '規格分類1(名称)', 14, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(15, 1, NULL, 'Eccube\\\\Entity\\\\ProductClass', 'ClassCategory2', 'id', '規格分類2(ID)', 15, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(16, 1, NULL, 'Eccube\\\\Entity\\\\ProductClass', 'ClassCategory2', 'name', '規格分類2(名称)', 16, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(17, 1, NULL, 'Eccube\\\\Entity\\\\ProductClass', 'DeliveryDuration', 'id', '発送日目安(ID)', 17, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(18, 1, NULL, 'Eccube\\\\Entity\\\\ProductClass', 'DeliveryDuration', 'name', '発送日目安(名称)', 18, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(19, 1, NULL, 'Eccube\\\\Entity\\\\ProductClass', 'code', NULL, '商品コード', 19, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(20, 1, NULL, 'Eccube\\\\Entity\\\\ProductClass', 'stock', NULL, '在庫数', 20, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(21, 1, NULL, 'Eccube\\\\Entity\\\\ProductClass', 'stock_unlimited', NULL, '在庫数無制限フラグ', 21, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(22, 1, NULL, 'Eccube\\\\Entity\\\\ProductClass', 'sale_limit', NULL, '販売制限数', 22, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(23, 1, NULL, 'Eccube\\\\Entity\\\\ProductClass', 'price01', NULL, '通常価格', 23, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(24, 1, NULL, 'Eccube\\\\Entity\\\\ProductClass', 'price02', NULL, '販売価格', 24, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(25, 1, 1, 'Eccube\\\\Entity\\\\ProductClass', 'delivery_fee', NULL, '送料', 30, 1, '2017-03-07 10:14:00', '2020-06-17 02:50:59', 'csv'),
(26, 1, 1, 'Eccube\\\\Entity\\\\Product', 'ProductImage', 'file_name', '商品画像', 25, 1, '2017-03-07 10:14:00', '2020-06-17 02:50:59', 'csv'),
(27, 1, 1, 'Eccube\\\\Entity\\\\Product', 'ProductCategories', 'category_id', '商品カテゴリ(ID)', 26, 1, '2017-03-07 10:14:00', '2020-06-17 02:50:59', 'csv'),
(28, 1, 1, 'Eccube\\\\Entity\\\\Product', 'ProductCategories', 'Category', '商品カテゴリ(名称)', 27, 1, '2017-03-07 10:14:00', '2020-06-17 02:50:59', 'csv'),
(29, 1, 1, 'Eccube\\\\Entity\\\\Product', 'ProductTag', 'tag_id', 'タグ(ID)', 28, 1, '2017-03-07 10:14:00', '2020-06-17 02:50:59', 'csv'),
(30, 1, 1, 'Eccube\\\\Entity\\\\Product', 'ProductTag', 'Tag', 'タグ(名称)', 29, 1, '2017-03-07 10:14:00', '2020-06-17 02:50:59', 'csv'),
(31, 2, NULL, 'Eccube\\\\Entity\\\\Customer', 'id', NULL, '会員ID', 1, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(32, 2, NULL, 'Eccube\\\\Entity\\\\Customer', 'name01', NULL, 'お名前(姓)', 2, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(33, 2, NULL, 'Eccube\\\\Entity\\\\Customer', 'name02', NULL, 'お名前(名)', 3, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(34, 2, NULL, 'Eccube\\\\Entity\\\\Customer', 'kana01', NULL, 'お名前(セイ)', 4, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(35, 2, NULL, 'Eccube\\\\Entity\\\\Customer', 'kana02', NULL, 'お名前(メイ)', 5, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(36, 2, NULL, 'Eccube\\\\Entity\\\\Customer', 'company_name', NULL, '会社名', 6, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(37, 2, NULL, 'Eccube\\\\Entity\\\\Customer', 'postal_code', NULL, '郵便番号', 7, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(38, 2, NULL, 'Eccube\\\\Entity\\\\Customer', 'Pref', 'id', '都道府県(ID)', 9, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(39, 2, NULL, 'Eccube\\\\Entity\\\\Customer', 'Pref', 'name', '都道府県(名称)', 10, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(40, 2, NULL, 'Eccube\\\\Entity\\\\Customer', 'addr01', NULL, '住所1', 11, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(41, 2, NULL, 'Eccube\\\\Entity\\\\Customer', 'addr02', NULL, '住所2', 12, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(42, 2, NULL, 'Eccube\\\\Entity\\\\Customer', 'email', NULL, 'メールアドレス', 13, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(43, 2, NULL, 'Eccube\\\\Entity\\\\Customer', 'phone_number', NULL, 'TEL', 14, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(44, 2, NULL, 'Eccube\\\\Entity\\\\Customer', 'Sex', 'id', '性別(ID)', 20, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(45, 2, NULL, 'Eccube\\\\Entity\\\\Customer', 'Sex', 'name', '性別(名称)', 21, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(46, 2, NULL, 'Eccube\\\\Entity\\\\Customer', 'Job', 'id', '職業(ID)', 22, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(47, 2, NULL, 'Eccube\\\\Entity\\\\Customer', 'Job', 'name', '職業(名称)', 23, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(48, 2, NULL, 'Eccube\\\\Entity\\\\Customer', 'birth', NULL, '誕生日', 24, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(49, 2, NULL, 'Eccube\\\\Entity\\\\Customer', 'first_buy_date', NULL, '初回購入日', 25, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(50, 2, NULL, 'Eccube\\\\Entity\\\\Customer', 'last_buy_date', NULL, '最終購入日', 26, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(51, 2, NULL, 'Eccube\\\\Entity\\\\Customer', 'buy_times', NULL, '購入回数', 27, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(52, 2, NULL, 'Eccube\\\\Entity\\\\Customer', 'note', NULL, 'ショップ用メモ欄', 28, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(53, 2, NULL, 'Eccube\\\\Entity\\\\Customer', 'Status', 'id', '会員ステータス(ID)', 29, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(54, 2, NULL, 'Eccube\\\\Entity\\\\Customer', 'Status', 'name', '会員ステータス(名称)', 30, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(55, 2, NULL, 'Eccube\\\\Entity\\\\Customer', 'create_date', NULL, '登録日', 31, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(56, 2, NULL, 'Eccube\\\\Entity\\\\Customer', 'update_date', NULL, '更新日', 32, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(57, 3, NULL, 'Eccube\\\\Entity\\\\Order', 'id', NULL, '注文ID', 1, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(58, 3, NULL, 'Eccube\\\\Entity\\\\Order', 'order_no', NULL, '注文番号', 2, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(59, 3, NULL, 'Eccube\\\\Entity\\\\Order', 'Customer', 'id', '会員ID', 3, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(60, 3, NULL, 'Eccube\\\\Entity\\\\Order', 'name01', NULL, 'お名前(姓)', 4, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(61, 3, NULL, 'Eccube\\\\Entity\\\\Order', 'name02', NULL, 'お名前(名)', 5, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(62, 3, NULL, 'Eccube\\\\Entity\\\\Order', 'kana01', NULL, 'お名前(セイ)', 6, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(63, 3, NULL, 'Eccube\\\\Entity\\\\Order', 'kana02', NULL, 'お名前(メイ)', 7, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(64, 3, NULL, 'Eccube\\\\Entity\\\\Order', 'company_name', NULL, '会社名', 8, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(65, 3, NULL, 'Eccube\\\\Entity\\\\Order', 'postal_code', NULL, '郵便番号', 9, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(66, 3, NULL, 'Eccube\\\\Entity\\\\Order', 'Pref', 'id', '都道府県(ID)', 10, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(67, 3, NULL, 'Eccube\\\\Entity\\\\Order', 'Pref', 'name', '都道府県(名称)', 11, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(68, 3, NULL, 'Eccube\\\\Entity\\\\Order', 'addr01', NULL, '住所1', 12, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(69, 3, NULL, 'Eccube\\\\Entity\\\\Order', 'addr02', NULL, '住所2', 13, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(70, 3, NULL, 'Eccube\\\\Entity\\\\Order', 'email', NULL, 'メールアドレス', 14, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(71, 3, NULL, 'Eccube\\\\Entity\\\\Order', 'phone_number', NULL, 'TEL', 15, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(72, 3, NULL, 'Eccube\\\\Entity\\\\Order', 'Sex', 'id', '性別(ID)', 16, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(73, 3, NULL, 'Eccube\\\\Entity\\\\Order', 'Sex', 'name', '性別(名称)', 17, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(74, 3, NULL, 'Eccube\\\\Entity\\\\Order', 'Job', 'id', '職業(ID)', 18, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(75, 3, NULL, 'Eccube\\\\Entity\\\\Order', 'Job', 'name', '職業(名称)', 19, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(76, 3, NULL, 'Eccube\\\\Entity\\\\Order', 'birth', NULL, '誕生日', 20, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(77, 3, NULL, 'Eccube\\\\Entity\\\\Order', 'note', NULL, 'ショップ用メモ欄', 21, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(78, 3, NULL, 'Eccube\\\\Entity\\\\Order', 'subtotal', NULL, '小計', 22, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(79, 3, NULL, 'Eccube\\\\Entity\\\\Order', 'discount', NULL, '値引き', 23, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(80, 3, NULL, 'Eccube\\\\Entity\\\\Order', 'delivery_fee_total', NULL, '送料', 24, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(81, 3, NULL, 'Eccube\\\\Entity\\\\Order', 'tax', NULL, '税金', 25, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(82, 3, NULL, 'Eccube\\\\Entity\\\\Order', 'total', NULL, '合計', 26, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(83, 3, NULL, 'Eccube\\\\Entity\\\\Order', 'payment_total', NULL, '支払合計', 27, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(84, 3, NULL, 'Eccube\\\\Entity\\\\Order', 'OrderStatus', 'id', '対応状況(ID)', 28, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(85, 3, NULL, 'Eccube\\\\Entity\\\\Order', 'OrderStatus', 'name', '対応状況(名称)', 29, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(86, 3, NULL, 'Eccube\\\\Entity\\\\Order', 'Payment', 'id', '支払方法(ID)', 30, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(87, 3, NULL, 'Eccube\\\\Entity\\\\Order', 'payment_method', NULL, '支払方法(名称)', 31, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(88, 3, NULL, 'Eccube\\\\Entity\\\\Order', 'order_date', NULL, '受注日', 32, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(89, 3, NULL, 'Eccube\\\\Entity\\\\Order', 'payment_date', NULL, '入金日', 33, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(90, 3, NULL, 'Eccube\\\\Entity\\\\OrderItem', 'id', NULL, '注文詳細ID', 34, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(91, 3, NULL, 'Eccube\\\\Entity\\\\OrderItem', 'Product', 'id', '商品ID', 35, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(92, 3, NULL, 'Eccube\\\\Entity\\\\OrderItem', 'ProductClass', 'id', '商品規格ID', 36, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(93, 3, NULL, 'Eccube\\\\Entity\\\\OrderItem', 'product_name', NULL, '商品名', 37, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(94, 3, NULL, 'Eccube\\\\Entity\\\\OrderItem', 'product_code', NULL, '商品コード', 38, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(95, 3, NULL, 'Eccube\\\\Entity\\\\OrderItem', 'class_name1', NULL, '規格名1', 39, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(96, 3, NULL, 'Eccube\\\\Entity\\\\OrderItem', 'class_name2', NULL, '規格名2', 40, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(97, 3, NULL, 'Eccube\\\\Entity\\\\OrderItem', 'class_category_name1', NULL, '規格分類名1', 41, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(98, 3, NULL, 'Eccube\\\\Entity\\\\OrderItem', 'class_category_name2', NULL, '規格分類名2', 42, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(99, 3, NULL, 'Eccube\\\\Entity\\\\OrderItem', 'price', NULL, '価格', 43, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(100, 3, NULL, 'Eccube\\\\Entity\\\\OrderItem', 'quantity', NULL, '個数', 44, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(101, 3, NULL, 'Eccube\\\\Entity\\\\OrderItem', 'tax_rate', NULL, '税率', 45, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(102, 3, NULL, 'Eccube\\\\Entity\\\\OrderItem', 'tax_rule', NULL, '税率ルール(ID)', 46, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(103, 3, NULL, 'Eccube\\\\Entity\\\\OrderItem', 'OrderItemType', 'id', '明細区分(ID)', 47, 1, '2018-07-23 09:00:00', '2018-07-23 09:00:00', 'csv'),
(104, 3, NULL, 'Eccube\\\\Entity\\\\OrderItem', 'OrderItemType', 'name', '明細区分(名称)', 48, 1, '2018-07-23 09:00:00', '2018-07-23 09:00:00', 'csv'),
(105, 3, NULL, 'Eccube\\\\Entity\\\\Shipping', 'id', NULL, '配送ID', 49, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(106, 3, NULL, 'Eccube\\\\Entity\\\\Shipping', 'name01', NULL, '配送先_お名前(姓)', 50, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(107, 3, NULL, 'Eccube\\\\Entity\\\\Shipping', 'name02', NULL, '配送先_お名前(名)', 51, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(108, 3, NULL, 'Eccube\\\\Entity\\\\Shipping', 'kana01', NULL, '配送先_お名前(セイ)', 52, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(109, 3, NULL, 'Eccube\\\\Entity\\\\Shipping', 'kana02', NULL, '配送先_お名前(メイ)', 53, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(110, 3, NULL, 'Eccube\\\\Entity\\\\Shipping', 'company_name', NULL, '配送先_会社名', 54, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(111, 3, NULL, 'Eccube\\\\Entity\\\\Shipping', 'postal_code', NULL, '配送先_郵便番号', 55, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(112, 3, NULL, 'Eccube\\\\Entity\\\\Shipping', 'Pref', 'id', '配送先_都道府県(ID)', 56, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(113, 3, NULL, 'Eccube\\\\Entity\\\\Shipping', 'Pref', 'name', '配送先_都道府県(名称)', 57, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(114, 3, NULL, 'Eccube\\\\Entity\\\\Shipping', 'addr01', NULL, '配送先_住所1', 58, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(115, 3, NULL, 'Eccube\\\\Entity\\\\Shipping', 'addr02', NULL, '配送先_住所2', 59, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(116, 3, NULL, 'Eccube\\\\Entity\\\\Shipping', 'phone_number', NULL, '配送先_TEL', 60, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(117, 3, NULL, 'Eccube\\\\Entity\\\\Shipping', 'Delivery', 'id', '配送業者(ID)', 61, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(118, 3, NULL, 'Eccube\\\\Entity\\\\Shipping', 'shipping_delivery_name', NULL, '配送業者(名称)', 62, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(119, 3, NULL, 'Eccube\\\\Entity\\\\Shipping', 'DeliveryTime', 'id', 'お届け時間ID', 63, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(120, 3, NULL, 'Eccube\\\\Entity\\\\Shipping', 'shipping_delivery_time', NULL, 'お届け時間(名称)', 64, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(121, 3, NULL, 'Eccube\\\\Entity\\\\Shipping', 'shipping_delivery_date', NULL, 'お届け希望日', 65, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(122, 3, NULL, 'Eccube\\\\Entity\\\\Shipping', 'DeliveryFee', 'id', '送料ID', 66, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(123, 3, NULL, 'Eccube\\\\Entity\\\\Shipping', 'shipping_delivery_fee', NULL, '送料', 67, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(124, 3, NULL, 'Eccube\\\\Entity\\\\Shipping', 'shipping_date', NULL, '発送日', 68, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(125, 3, NULL, 'Eccube\\\\Entity\\\\Shipping', 'tracking_number', NULL, '出荷伝票番号', 69, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(126, 3, NULL, 'Eccube\\\\Entity\\\\Shipping', 'note', NULL, '配達用メモ', 70, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(127, 3, NULL, 'Eccube\\\\Entity\\\\Shipping', 'mail_send_date', NULL, '出荷メール送信日', 71, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(128, 4, NULL, 'Eccube\\\\Entity\\\\Order', 'id', NULL, '注文ID', 1, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(129, 4, NULL, 'Eccube\\\\Entity\\\\Order', 'order_no', NULL, '注文番号', 2, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(130, 4, NULL, 'Eccube\\\\Entity\\\\Order', 'Customer', 'id', '会員ID', 3, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(131, 4, NULL, 'Eccube\\\\Entity\\\\Order', 'name01', NULL, 'お名前(姓)', 4, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(132, 4, NULL, 'Eccube\\\\Entity\\\\Order', 'name02', NULL, 'お名前(名)', 5, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(133, 4, NULL, 'Eccube\\\\Entity\\\\Order', 'kana01', NULL, 'お名前(セイ)', 6, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(134, 4, NULL, 'Eccube\\\\Entity\\\\Order', 'kana02', NULL, 'お名前(メイ)', 7, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(135, 4, NULL, 'Eccube\\\\Entity\\\\Order', 'company_name', NULL, '会社名', 8, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(136, 4, NULL, 'Eccube\\\\Entity\\\\Order', 'postal_code', NULL, '郵便番号', 9, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(137, 4, NULL, 'Eccube\\\\Entity\\\\Order', 'Pref', 'id', '都道府県(ID)', 10, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(138, 4, NULL, 'Eccube\\\\Entity\\\\Order', 'Pref', 'name', '都道府県(名称)', 11, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(139, 4, NULL, 'Eccube\\\\Entity\\\\Order', 'addr01', NULL, '住所1', 12, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(140, 4, NULL, 'Eccube\\\\Entity\\\\Order', 'addr02', NULL, '住所2', 13, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(141, 4, NULL, 'Eccube\\\\Entity\\\\Order', 'email', NULL, 'メールアドレス', 14, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(142, 4, NULL, 'Eccube\\\\Entity\\\\Order', 'phone_number', NULL, 'TEL', 15, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(143, 4, NULL, 'Eccube\\\\Entity\\\\Order', 'Sex', 'id', '性別(ID)', 16, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(144, 4, NULL, 'Eccube\\\\Entity\\\\Order', 'Sex', 'name', '性別(名称)', 17, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(145, 4, NULL, 'Eccube\\\\Entity\\\\Order', 'Job', 'id', '職業(ID)', 18, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(146, 4, NULL, 'Eccube\\\\Entity\\\\Order', 'Job', 'name', '職業(名称)', 19, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(147, 4, NULL, 'Eccube\\\\Entity\\\\Order', 'birth', NULL, '誕生日', 20, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(148, 4, NULL, 'Eccube\\\\Entity\\\\Order', 'note', NULL, 'ショップ用メモ欄', 21, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(149, 4, NULL, 'Eccube\\\\Entity\\\\Order', 'subtotal', NULL, '小計', 22, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(150, 4, NULL, 'Eccube\\\\Entity\\\\Order', 'discount', NULL, '値引き', 23, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(151, 4, NULL, 'Eccube\\\\Entity\\\\Order', 'delivery_fee_total', NULL, '送料', 24, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(152, 4, NULL, 'Eccube\\\\Entity\\\\Order', 'tax', NULL, '税金', 25, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(153, 4, NULL, 'Eccube\\\\Entity\\\\Order', 'total', NULL, '合計', 26, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(154, 4, NULL, 'Eccube\\\\Entity\\\\Order', 'payment_total', NULL, '支払合計', 27, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(155, 4, NULL, 'Eccube\\\\Entity\\\\Order', 'OrderStatus', 'id', '対応状況(ID)', 28, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(156, 4, NULL, 'Eccube\\\\Entity\\\\Order', 'OrderStatus', 'name', '対応状況(名称)', 29, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(157, 4, NULL, 'Eccube\\\\Entity\\\\Order', 'Payment', 'id', '支払方法(ID)', 30, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(158, 4, NULL, 'Eccube\\\\Entity\\\\Order', 'payment_method', NULL, '支払方法(名称)', 31, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(159, 4, NULL, 'Eccube\\\\Entity\\\\Order', 'order_date', NULL, '受注日', 32, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(160, 4, NULL, 'Eccube\\\\Entity\\\\Order', 'payment_date', NULL, '入金日', 33, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(161, 4, NULL, 'Eccube\\\\Entity\\\\OrderItem', 'id', NULL, '注文詳細ID', 34, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(162, 4, NULL, 'Eccube\\\\Entity\\\\OrderItem', 'Product', 'id', '商品ID', 35, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(163, 4, NULL, 'Eccube\\\\Entity\\\\OrderItem', 'ProductClass', 'id', '商品規格ID', 36, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(164, 4, NULL, 'Eccube\\\\Entity\\\\OrderItem', 'product_name', NULL, '商品名', 37, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(165, 4, NULL, 'Eccube\\\\Entity\\\\OrderItem', 'product_code', NULL, '商品コード', 38, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(166, 4, NULL, 'Eccube\\\\Entity\\\\OrderItem', 'class_name1', NULL, '規格名1', 39, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(167, 4, NULL, 'Eccube\\\\Entity\\\\OrderItem', 'class_name2', NULL, '規格名2', 40, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(168, 4, NULL, 'Eccube\\\\Entity\\\\OrderItem', 'class_category_name1', NULL, '規格分類名1', 41, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(169, 4, NULL, 'Eccube\\\\Entity\\\\OrderItem', 'class_category_name2', NULL, '規格分類名2', 42, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(170, 4, NULL, 'Eccube\\\\Entity\\\\OrderItem', 'price', NULL, '価格', 43, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(171, 4, NULL, 'Eccube\\\\Entity\\\\OrderItem', 'quantity', NULL, '個数', 44, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(172, 4, NULL, 'Eccube\\\\Entity\\\\OrderItem', 'tax_rate', NULL, '税率', 45, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(173, 4, NULL, 'Eccube\\\\Entity\\\\OrderItem', 'tax_rule', NULL, '税率ルール(ID)', 46, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(174, 4, NULL, 'Eccube\\\\Entity\\\\OrderItem', 'OrderItemType', 'id', '明細区分(ID)', 47, 1, '2018-07-23 09:00:00', '2018-07-23 09:00:00', 'csv'),
(175, 4, NULL, 'Eccube\\\\Entity\\\\OrderItem', 'OrderItemType', 'name', '明細区分(名称)', 48, 1, '2018-07-23 09:00:00', '2018-07-23 09:00:00', 'csv'),
(176, 4, NULL, 'Eccube\\\\Entity\\\\Shipping', 'id', NULL, '配送ID', 49, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(177, 4, NULL, 'Eccube\\\\Entity\\\\Shipping', 'name01', NULL, '配送先_お名前(姓)', 50, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(178, 4, NULL, 'Eccube\\\\Entity\\\\Shipping', 'name02', NULL, '配送先_お名前(名)', 51, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(179, 4, NULL, 'Eccube\\\\Entity\\\\Shipping', 'kana01', NULL, '配送先_お名前(セイ)', 52, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(180, 4, NULL, 'Eccube\\\\Entity\\\\Shipping', 'kana02', NULL, '配送先_お名前(メイ)', 53, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(181, 4, NULL, 'Eccube\\\\Entity\\\\Shipping', 'company_name', NULL, '配送先_会社名', 54, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(182, 4, NULL, 'Eccube\\\\Entity\\\\Shipping', 'postal_code', NULL, '配送先_郵便番号', 55, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(183, 4, NULL, 'Eccube\\\\Entity\\\\Shipping', 'Pref', 'id', '配送先_都道府県(ID)', 56, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(184, 4, NULL, 'Eccube\\\\Entity\\\\Shipping', 'Pref', 'name', '配送先_都道府県(名称)', 57, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(185, 4, NULL, 'Eccube\\\\Entity\\\\Shipping', 'addr01', NULL, '配送先_住所1', 58, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(186, 4, NULL, 'Eccube\\\\Entity\\\\Shipping', 'addr02', NULL, '配送先_住所2', 59, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(187, 4, NULL, 'Eccube\\\\Entity\\\\Shipping', 'phone_number', NULL, '配送先_TEL', 60, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(188, 4, NULL, 'Eccube\\\\Entity\\\\Shipping', 'Delivery', 'id', '配送業者(ID)', 61, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(189, 4, NULL, 'Eccube\\\\Entity\\\\Shipping', 'shipping_delivery_name', NULL, '配送業者(名称)', 62, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(190, 4, NULL, 'Eccube\\\\Entity\\\\Shipping', 'DeliveryTime', 'id', 'お届け時間ID', 63, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(191, 4, NULL, 'Eccube\\\\Entity\\\\Shipping', 'shipping_delivery_time', NULL, 'お届け時間(名称)', 64, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(192, 4, NULL, 'Eccube\\\\Entity\\\\Shipping', 'shipping_delivery_date', NULL, 'お届け希望日', 65, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(193, 4, NULL, 'Eccube\\\\Entity\\\\Shipping', 'DeliveryFee', 'id', '送料ID', 66, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(194, 4, NULL, 'Eccube\\\\Entity\\\\Shipping', 'shipping_delivery_fee', NULL, '送料', 67, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(195, 4, NULL, 'Eccube\\\\Entity\\\\Shipping', 'shipping_date', NULL, '発送日', 68, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(196, 4, NULL, 'Eccube\\\\Entity\\\\Shipping', 'tracking_number', NULL, '出荷伝票番号', 69, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(197, 4, NULL, 'Eccube\\\\Entity\\\\Shipping', 'note', NULL, '配達用メモ', 70, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(198, 4, NULL, 'Eccube\\\\Entity\\\\Shipping', 'mail_send_date', NULL, '出荷メール送信日', 71, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(199, 5, NULL, 'Eccube\\\\Entity\\\\Category', 'id', NULL, 'カテゴリID', 1, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(200, 5, NULL, 'Eccube\\\\Entity\\\\Category', 'sort_no', NULL, '表示ランク', 2, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(201, 5, NULL, 'Eccube\\\\Entity\\\\Category', 'name', NULL, 'カテゴリ名', 3, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(202, 5, NULL, 'Eccube\\\\Entity\\\\Category', 'Parent', 'id', '親カテゴリID', 4, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(203, 5, NULL, 'Eccube\\\\Entity\\\\Category', 'level', NULL, '階層', 5, 1, '2017-03-07 10:14:00', '2017-03-07 10:14:00', 'csv'),
(204, 1, 1, 'Eccube\\\\Entity\\\\ProductClass', 'TaxRule', 'tax_rate', '税率', 31, 1, '2017-03-07 10:14:00', '2020-06-17 02:50:59', 'csv'),
(205, 6, 1, 'Plugin\\ProductReview4\\Entity\\ProductReview', 'Product', 'name', '商品名', 1, 1, '2020-05-06 11:18:12', '2020-05-06 11:18:12', 'csv'),
(206, 6, 1, 'Plugin\\ProductReview4\\Entity\\ProductReview', 'Status', 'name', '公開・非公開', 2, 1, '2020-05-06 11:18:12', '2020-05-06 11:18:12', 'csv'),
(207, 6, 1, 'Plugin\\ProductReview4\\Entity\\ProductReview', 'create_date', 'create_date', '投稿日', 3, 1, '2020-05-06 11:18:12', '2020-05-06 11:18:12', 'csv'),
(208, 6, 1, 'Plugin\\ProductReview4\\Entity\\ProductReview', 'reviewer_name', 'reviewer_name', '投稿者名', 4, 1, '2020-05-06 11:18:12', '2020-05-06 11:18:12', 'csv'),
(209, 6, 1, 'Plugin\\ProductReview4\\Entity\\ProductReview', 'reviewer_url', 'reviewer_url', '投稿者URL', 5, 1, '2020-05-06 11:18:12', '2020-05-06 11:18:12', 'csv'),
(210, 6, 1, 'Plugin\\ProductReview4\\Entity\\ProductReview', 'Sex', 'name', '性別', 6, 1, '2020-05-06 11:18:12', '2020-05-06 11:18:12', 'csv'),
(211, 6, 1, 'Plugin\\ProductReview4\\Entity\\ProductReview', 'recommend_level', 'recommend_level', 'おすすめレベル', 7, 1, '2020-05-06 11:18:12', '2020-05-06 11:18:12', 'csv'),
(212, 6, 1, 'Plugin\\ProductReview4\\Entity\\ProductReview', 'title', 'title', 'タイトル', 8, 1, '2020-05-06 11:18:12', '2020-05-06 11:18:12', 'csv'),
(213, 6, 1, 'Plugin\\ProductReview4\\Entity\\ProductReview', 'comment', 'comment', 'コメント', 9, 1, '2020-05-06 11:18:12', '2020-05-06 11:18:12', 'csv'),
(215, 2, 1, 'Eccube\\\\Entity\\\\Customer', 'plgCcpCustomerClass', 'name', '特定会員種別', 9999, 1, '2020-09-02 03:36:32', '2020-09-02 03:36:32', 'csv');

-- --------------------------------------------------------

--
-- Table structure for table `dtb_customer`
--

CREATE TABLE `dtb_customer` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_status_id` smallint(5) UNSIGNED DEFAULT NULL,
  `sex_id` smallint(5) UNSIGNED DEFAULT NULL,
  `job_id` smallint(5) UNSIGNED DEFAULT NULL,
  `country_id` smallint(5) UNSIGNED DEFAULT NULL,
  `pref_id` smallint(5) UNSIGNED DEFAULT NULL,
  `name01` varchar(255) NOT NULL,
  `name02` varchar(255) NOT NULL,
  `kana01` varchar(255) DEFAULT NULL,
  `kana02` varchar(255) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `postal_code` varchar(8) DEFAULT NULL,
  `addr01` varchar(255) DEFAULT NULL,
  `addr02` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `phone_number` varchar(14) DEFAULT NULL,
  `birth` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `secret_key` varchar(255) NOT NULL,
  `first_buy_date` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `last_buy_date` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `buy_times` decimal(10,0) UNSIGNED DEFAULT '0',
  `buy_total` decimal(12,2) UNSIGNED DEFAULT '0.00',
  `note` varchar(4000) DEFAULT NULL,
  `reset_key` varchar(255) DEFAULT NULL,
  `reset_expire` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `point` decimal(12,0) NOT NULL DEFAULT '0',
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL,
  `plg_mailmagazine_flg` smallint(5) UNSIGNED DEFAULT '0',
  `plg_ccp_customer_class_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtb_customer`
--

INSERT INTO `dtb_customer` (`id`, `customer_status_id`, `sex_id`, `job_id`, `country_id`, `pref_id`, `name01`, `name02`, `kana01`, `kana02`, `company_name`, `postal_code`, `addr01`, `addr02`, `email`, `phone_number`, `birth`, `password`, `salt`, `secret_key`, `first_buy_date`, `last_buy_date`, `buy_times`, `buy_total`, `note`, `reset_key`, `reset_expire`, `point`, `create_date`, `update_date`, `discriminator_type`, `plg_mailmagazine_flg`, `plg_ccp_customer_class_id`) VALUES
(1, 2, 1, 2, NULL, 13, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '1570077', '世田谷区鎌田', '123建物', 'djfre2000@hotmail.com', '0123456789', '1985-04-05 15:00:00', '7c9ea39fd3474276c69536871e28c6728e9db7f1a035a7184e90f8c43e311165', '7c1dd9a845', '65aEW8rhYIMr8z7yjLLp5kzvM6W6jITM', '2020-07-16 12:22:23', '2020-09-17 06:20:57', '5', '20062.00', NULL, NULL, '2020-08-06 11:40:56', '533', '2020-05-01 08:27:19', '2020-09-17 06:20:57', 'customer', 1, 1),
(2, 1, NULL, NULL, NULL, 13, 'Test', 'Test', 'カナ', 'カナ', NULL, '1000001', '千代田区千代田', '1-1-1', '05049pyj@gmail.com', '11111111111', NULL, '03e21171469d774c9c42c7fca44d0d0af889868a31a257b136884e5de1bd7874', 'd2827277e5', 'GcNh0olbKL14ABg4J5ckxWE5hfBCsLDw', NULL, NULL, '0', '0.00', NULL, NULL, NULL, '0', '2020-05-15 10:05:22', '2020-05-27 02:47:36', 'customer', 0, 1),
(4, 2, NULL, NULL, NULL, 13, 'Test2', 'Test', 'カナ', 'カナ', NULL, '1000001', '千代田区千代田', '1-1-1', '05049pyj+1@gmail.com', '11122223333', NULL, '3c2794b8a78cd97e315703e1dc85298dd1478393a9ef5b8f6273354cfe0999f8', '5b2c76e586', '4D1HRfhtYtDLhgPpsvDY7JuaZxm9H3vf', NULL, NULL, '0', '0.00', NULL, '0FncX79vzpEQZuXRcViPKbSZaQ9ush7V', '2020-09-07 10:56:31', '0', '2020-05-28 14:50:38', '2020-09-07 10:46:31', 'customer', 0, NULL),
(5, 2, NULL, NULL, NULL, 13, 'Test3', 'Test', 'カナ', 'カナ', NULL, '1000001', '千代田区千代田', '1-1-1', '05049pyj+2@gmail.com', '11122223333', NULL, '7adee46e0dfb87864adebb94b3ae1d722f2dd648e1acd5edf25ba8028e44b565', 'c4a96a1ee0', '7yzVwgLYL7yJiUxFOOiWWfhHZJKCeOMS', NULL, NULL, '0', '0.00', NULL, NULL, NULL, '0', '2020-05-28 15:13:20', '2020-05-28 15:22:38', 'customer', 0, 1),
(6, 2, 1, NULL, NULL, 13, 'Test', 'Test', 'フレデリック', 'ヴェルヴァーク', 'Shourai Ltd.', '1570077', '世田谷区鎌田', 'テスト900', 'frederic@shourai.io', '0123456789', '1999-04-05 15:00:00', '0977822ec60f2ac8843feb835653e549783f78ca5829943e4d69a8a9c73f784c', '2f0a537e17', '2CMsuAK14sp5A8yLnbEkzVom8ZNaKhgA', NULL, NULL, '0', '0.00', NULL, NULL, NULL, '0', '2020-05-29 00:00:39', '2020-07-20 06:56:39', 'customer', 1, 1),
(7, 1, 1, 1, NULL, 13, 'TEST', 'TEST', 'テスト', 'テスト', 'Test', '1570077', '世田谷区鎌田123', '123', 'f.vervaecke@gmail.com', '0123456789', '2019-02-02 15:00:00', 'd2e6e0ab4aa7157ae7e4e9d2cd0b812ab66bd38c030db973a9c69c77f2547d17', '62c2c09cf1', 'perWdwV08T0oc00eNv6GjuWggQI8pD3U', NULL, NULL, '0', '0.00', NULL, NULL, NULL, '0', '2020-08-03 05:09:10', '2020-08-03 05:09:10', 'customer', 1, 1),
(8, 1, NULL, NULL, NULL, 13, 'Test', 'Test', 'テスト', 'テスト', NULL, '1570077', '世田谷区鎌田', '123', 'test@testt.com', '0123456789', NULL, 'b574ae46df9ee85afd1f43b08b3681cd01e33468f12e613799c1b21b742973bd', '3da10ee542', 'SrScdeXNZ3uDpMp5tXkTQCjV2n5uCdJ4', NULL, NULL, '0', '0.00', NULL, NULL, NULL, '0', '2020-09-18 01:24:29', '2020-09-18 01:24:29', 'customer', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `dtb_customer_address`
--

CREATE TABLE `dtb_customer_address` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED DEFAULT NULL,
  `country_id` smallint(5) UNSIGNED DEFAULT NULL,
  `pref_id` smallint(5) UNSIGNED DEFAULT NULL,
  `name01` varchar(255) NOT NULL,
  `name02` varchar(255) NOT NULL,
  `kana01` varchar(255) DEFAULT NULL,
  `kana02` varchar(255) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `postal_code` varchar(8) DEFAULT NULL,
  `addr01` varchar(255) DEFAULT NULL,
  `addr02` varchar(255) DEFAULT NULL,
  `phone_number` varchar(14) DEFAULT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtb_customer_address`
--

INSERT INTO `dtb_customer_address` (`id`, `customer_id`, `country_id`, `pref_id`, `name01`, `name02`, `kana01`, `kana02`, `company_name`, `postal_code`, `addr01`, `addr02`, `phone_number`, `create_date`, `update_date`, `discriminator_type`) VALUES
(1, 1, NULL, 28, 'TEst', 'TEst', 'フ', 'フ', '株式会社メディファイン', '6500013', '神戸市中央区花隈町', '123', '0787777000', '2020-08-03 06:17:33', '2020-08-03 06:17:33', 'customeraddress');

-- --------------------------------------------------------

--
-- Table structure for table `dtb_customer_favorite_product`
--

CREATE TABLE `dtb_customer_favorite_product` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED DEFAULT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtb_customer_favorite_product`
--

INSERT INTO `dtb_customer_favorite_product` (`id`, `customer_id`, `product_id`, `create_date`, `update_date`, `discriminator_type`) VALUES
(1, 1, 2, '2020-05-01 08:32:09', '2020-05-01 08:32:09', 'customerfavoriteproduct'),
(2, 1, 9, '2020-08-03 06:20:41', '2020-08-03 06:20:41', 'customerfavoriteproduct'),
(3, 1, 14, '2020-08-13 01:45:46', '2020-08-13 01:45:46', 'customerfavoriteproduct');

-- --------------------------------------------------------

--
-- Table structure for table `dtb_delivery`
--

CREATE TABLE `dtb_delivery` (
  `id` int(10) UNSIGNED NOT NULL,
  `creator_id` int(10) UNSIGNED DEFAULT NULL,
  `sale_type_id` smallint(5) UNSIGNED DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `service_name` varchar(255) DEFAULT NULL,
  `description` varchar(4000) DEFAULT NULL,
  `confirm_url` varchar(4000) DEFAULT NULL,
  `sort_no` int(10) UNSIGNED DEFAULT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtb_delivery`
--

INSERT INTO `dtb_delivery` (`id`, `creator_id`, `sale_type_id`, `name`, `service_name`, `description`, `confirm_url`, `sort_no`, `visible`, `create_date`, `update_date`, `discriminator_type`) VALUES
(1, 1, 1, '佐川急便', '佐川急便（通常購入）', NULL, 'https://k2k.sagawa-exp.co.jp/p/sagawa/web/okurijoinput.jsp', 2, 1, '2017-03-07 10:14:52', '2020-07-24 06:39:24', 'delivery'),
(2, 1, 2, '佐川急便', '佐川急便 (定期購買)', NULL, 'https://k2k.sagawa-exp.co.jp/p/sagawa/web/okurijoinput.jsp', 1, 0, '2017-03-07 10:14:52', '2020-07-24 06:43:23', 'delivery');

-- --------------------------------------------------------

--
-- Table structure for table `dtb_delivery_duration`
--

CREATE TABLE `dtb_delivery_duration` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `duration` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `sort_no` int(10) UNSIGNED NOT NULL,
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtb_delivery_duration`
--

INSERT INTO `dtb_delivery_duration` (`id`, `name`, `duration`, `sort_no`, `discriminator_type`) VALUES
(1, '即日', 0, 0, 'deliveryduration'),
(2, '1～2日後', 1, 1, 'deliveryduration'),
(3, '3～4日後', 3, 2, 'deliveryduration'),
(4, '1週間以降', 7, 3, 'deliveryduration'),
(5, '2週間以降', 14, 4, 'deliveryduration'),
(6, '3週間以降', 21, 5, 'deliveryduration'),
(7, '1ヶ月以降', 30, 6, 'deliveryduration'),
(8, '2ヶ月以降', 60, 7, 'deliveryduration'),
(9, 'お取り寄せ(商品入荷後)', 0, 8, 'deliveryduration');

-- --------------------------------------------------------

--
-- Table structure for table `dtb_delivery_fee`
--

CREATE TABLE `dtb_delivery_fee` (
  `id` int(10) UNSIGNED NOT NULL,
  `delivery_id` int(10) UNSIGNED DEFAULT NULL,
  `pref_id` smallint(5) UNSIGNED DEFAULT NULL,
  `fee` decimal(12,2) UNSIGNED NOT NULL,
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtb_delivery_fee`
--

INSERT INTO `dtb_delivery_fee` (`id`, `delivery_id`, `pref_id`, `fee`, `discriminator_type`) VALUES
(1, 1, 1, '600.00', 'deliveryfee'),
(2, 1, 2, '600.00', 'deliveryfee'),
(3, 1, 3, '600.00', 'deliveryfee'),
(4, 1, 4, '600.00', 'deliveryfee'),
(5, 1, 5, '600.00', 'deliveryfee'),
(6, 1, 6, '600.00', 'deliveryfee'),
(7, 1, 7, '600.00', 'deliveryfee'),
(8, 1, 8, '600.00', 'deliveryfee'),
(9, 1, 9, '600.00', 'deliveryfee'),
(10, 1, 10, '600.00', 'deliveryfee'),
(11, 1, 11, '600.00', 'deliveryfee'),
(12, 1, 12, '600.00', 'deliveryfee'),
(13, 1, 13, '600.00', 'deliveryfee'),
(14, 1, 14, '600.00', 'deliveryfee'),
(15, 1, 15, '600.00', 'deliveryfee'),
(16, 1, 16, '600.00', 'deliveryfee'),
(17, 1, 17, '600.00', 'deliveryfee'),
(18, 1, 18, '600.00', 'deliveryfee'),
(19, 1, 19, '600.00', 'deliveryfee'),
(20, 1, 20, '600.00', 'deliveryfee'),
(21, 1, 21, '600.00', 'deliveryfee'),
(22, 1, 22, '600.00', 'deliveryfee'),
(23, 1, 23, '600.00', 'deliveryfee'),
(24, 1, 24, '600.00', 'deliveryfee'),
(25, 1, 25, '600.00', 'deliveryfee'),
(26, 1, 26, '600.00', 'deliveryfee'),
(27, 1, 27, '600.00', 'deliveryfee'),
(28, 1, 28, '600.00', 'deliveryfee'),
(29, 1, 29, '600.00', 'deliveryfee'),
(30, 1, 30, '600.00', 'deliveryfee'),
(31, 1, 31, '600.00', 'deliveryfee'),
(32, 1, 32, '600.00', 'deliveryfee'),
(33, 1, 33, '600.00', 'deliveryfee'),
(34, 1, 34, '600.00', 'deliveryfee'),
(35, 1, 35, '600.00', 'deliveryfee'),
(36, 1, 36, '600.00', 'deliveryfee'),
(37, 1, 37, '600.00', 'deliveryfee'),
(38, 1, 38, '600.00', 'deliveryfee'),
(39, 1, 39, '600.00', 'deliveryfee'),
(40, 1, 40, '600.00', 'deliveryfee'),
(41, 1, 41, '600.00', 'deliveryfee'),
(42, 1, 42, '600.00', 'deliveryfee'),
(43, 1, 43, '600.00', 'deliveryfee'),
(44, 1, 44, '600.00', 'deliveryfee'),
(45, 1, 45, '600.00', 'deliveryfee'),
(46, 1, 46, '600.00', 'deliveryfee'),
(47, 1, 47, '600.00', 'deliveryfee'),
(48, 2, 1, '600.00', 'deliveryfee'),
(49, 2, 2, '600.00', 'deliveryfee'),
(50, 2, 3, '600.00', 'deliveryfee'),
(51, 2, 4, '600.00', 'deliveryfee'),
(52, 2, 5, '600.00', 'deliveryfee'),
(53, 2, 6, '600.00', 'deliveryfee'),
(54, 2, 7, '600.00', 'deliveryfee'),
(55, 2, 8, '600.00', 'deliveryfee'),
(56, 2, 9, '600.00', 'deliveryfee'),
(57, 2, 10, '600.00', 'deliveryfee'),
(58, 2, 11, '600.00', 'deliveryfee'),
(59, 2, 12, '600.00', 'deliveryfee'),
(60, 2, 13, '600.00', 'deliveryfee'),
(61, 2, 14, '600.00', 'deliveryfee'),
(62, 2, 15, '600.00', 'deliveryfee'),
(63, 2, 16, '600.00', 'deliveryfee'),
(64, 2, 17, '600.00', 'deliveryfee'),
(65, 2, 18, '600.00', 'deliveryfee'),
(66, 2, 19, '600.00', 'deliveryfee'),
(67, 2, 20, '600.00', 'deliveryfee'),
(68, 2, 21, '600.00', 'deliveryfee'),
(69, 2, 22, '600.00', 'deliveryfee'),
(70, 2, 23, '600.00', 'deliveryfee'),
(71, 2, 24, '600.00', 'deliveryfee'),
(72, 2, 25, '600.00', 'deliveryfee'),
(73, 2, 26, '600.00', 'deliveryfee'),
(74, 2, 27, '600.00', 'deliveryfee'),
(75, 2, 28, '600.00', 'deliveryfee'),
(76, 2, 29, '600.00', 'deliveryfee'),
(77, 2, 30, '600.00', 'deliveryfee'),
(78, 2, 31, '600.00', 'deliveryfee'),
(79, 2, 32, '600.00', 'deliveryfee'),
(80, 2, 33, '600.00', 'deliveryfee'),
(81, 2, 34, '600.00', 'deliveryfee'),
(82, 2, 35, '600.00', 'deliveryfee'),
(83, 2, 36, '600.00', 'deliveryfee'),
(84, 2, 37, '600.00', 'deliveryfee'),
(85, 2, 38, '600.00', 'deliveryfee'),
(86, 2, 39, '600.00', 'deliveryfee'),
(87, 2, 40, '600.00', 'deliveryfee'),
(88, 2, 41, '600.00', 'deliveryfee'),
(89, 2, 42, '600.00', 'deliveryfee'),
(90, 2, 43, '600.00', 'deliveryfee'),
(91, 2, 44, '600.00', 'deliveryfee'),
(92, 2, 45, '600.00', 'deliveryfee'),
(93, 2, 46, '600.00', 'deliveryfee'),
(94, 2, 47, '600.00', 'deliveryfee');

-- --------------------------------------------------------

--
-- Table structure for table `dtb_delivery_time`
--

CREATE TABLE `dtb_delivery_time` (
  `id` int(10) UNSIGNED NOT NULL,
  `delivery_id` int(10) UNSIGNED DEFAULT NULL,
  `delivery_time` varchar(255) NOT NULL,
  `sort_no` smallint(5) UNSIGNED NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtb_delivery_time`
--

INSERT INTO `dtb_delivery_time` (`id`, `delivery_id`, `delivery_time`, `sort_no`, `visible`, `create_date`, `update_date`, `discriminator_type`) VALUES
(3, 2, '午前', 1, 0, '2020-05-15 06:19:49', '2020-07-24 06:43:23', 'deliverytime'),
(4, 2, '午後', 2, 0, '2020-05-15 06:19:49', '2020-07-24 06:43:23', 'deliverytime'),
(6, 1, '午前中（8時～12時）', 1, 1, '2020-07-22 03:59:42', '2020-08-30 07:20:02', 'deliverytime'),
(7, 1, '12時～14時', 2, 1, '2020-07-22 03:59:42', '2020-08-30 07:20:02', 'deliverytime'),
(8, 1, '14時～16時', 3, 1, '2020-07-22 03:59:42', '2020-08-30 07:20:02', 'deliverytime'),
(9, 1, '16時～18時', 4, 1, '2020-07-22 03:59:42', '2020-08-30 07:20:02', 'deliverytime'),
(10, 1, '18時～21時', 5, 1, '2020-07-22 03:59:42', '2020-08-30 07:20:02', 'deliverytime'),
(11, 1, '19時～21時', 6, 1, '2020-07-22 03:59:42', '2020-08-30 07:20:02', 'deliverytime');

-- --------------------------------------------------------

--
-- Table structure for table `dtb_layout`
--

CREATE TABLE `dtb_layout` (
  `id` int(10) UNSIGNED NOT NULL,
  `device_type_id` smallint(5) UNSIGNED DEFAULT NULL,
  `layout_name` varchar(255) DEFAULT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtb_layout`
--

INSERT INTO `dtb_layout` (`id`, `device_type_id`, `layout_name`, `create_date`, `update_date`, `discriminator_type`) VALUES
(0, 10, 'トップページ用レイアウト', '2017-03-07 10:14:52', '2020-04-26 06:48:24', 'layout'),
(1, 10, 'トップページ用レイアウト', '2017-03-07 10:14:52', '2017-03-07 10:14:52', 'layout'),
(2, 10, '下層ページ用レイアウト', '2017-03-07 10:14:52', '2017-03-07 10:14:52', 'layout'),
(3, 10, '商品一覧レイアウト', '2020-04-29 00:44:53', '2020-04-29 00:44:53', 'layout'),
(4, 10, '商品詳細レイアウト', '2020-05-13 09:30:15', '2020-05-13 09:30:15', 'layout'),
(5, 10, '代理店レイアウト', '2020-05-16 11:50:24', '2020-05-23 09:14:24', 'layout'),
(6, 10, 'Japanese LP', '2020-08-19 10:36:07', '2020-08-19 10:36:07', 'layout'),
(7, 10, '記事レイアウト', '2020-10-11 05:13:12', '2020-10-11 05:13:12', 'layout');

-- --------------------------------------------------------

--
-- Table structure for table `dtb_mail_history`
--

CREATE TABLE `dtb_mail_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED DEFAULT NULL,
  `creator_id` int(10) UNSIGNED DEFAULT NULL,
  `send_date` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `mail_subject` varchar(255) DEFAULT NULL,
  `mail_body` longtext,
  `mail_html_body` longtext,
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtb_mail_history`
--

INSERT INTO `dtb_mail_history` (`id`, `order_id`, `creator_id`, `send_date`, `mail_subject`, `mail_body`, `mail_html_body`, `discriminator_type`) VALUES
(1, 61, NULL, '2020-06-14 05:09:44', '[Maritime] ご注文ありがとうございます', 'Frederic Vervaecke 様\n\nこの度はご注文いただき誠にありがとうございます。下記ご注文内容にお間違えがないかご確認下さい。\n\n************************************************\n　ご請求金額\n************************************************\n\nご注文日時：2020/06/13 12:29:28\nご注文番号：61\nお支払い合計：￥4,384\nお支払い方法：PayPal決済\nご利用ポイント：0 pt\nお問い合わせ：\n\n\n************************************************\n　ご注文商品明細\n************************************************\n\n商品コード：cbd-water-500\n商品名：MARITIME CBD ウォーター 500ml  通常購入  \n単価：￥4,384\n数量：4\n\n\n-------------------------------------------------\n小　計：￥4,384\n\n手数料：￥0\n送　料：￥0\n============================================\n合　計：￥4,384\n\n************************************************\n　ご注文者情報\n************************************************\nお名前：Frederic Vervaecke 様\nお名前(カナ)：フレデリック ヴェルヴァーク 様\n郵便番号：〒1570077\n住所：東京都世田谷区鎌田123建物\n電話番号：0123456789\nメールアドレス：djfre2000@hotmail.com\n\n************************************************\n　配送情報\n************************************************\n\n◎お届け先\nお名前：Frederic Vervaecke 様\nお名前(カナ)：フレデリック ヴェルヴァーク 様\n郵便番号：〒1570077\n住所：東京都世田谷区鎌田123建物\n電話番号：0123456789\n\n配送方法：スタンダード宅配\nお届け日：指定なし\nお届け時間：指定なし\n\n商品コード：cbd-water-500\n商品名：MARITIME CBD ウォーター 500ml  通常購入  \n数量：4\n\n\n\n============================================\n\nこのメッセージはお客様へのお知らせ専用ですので、\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。\nご了承ください。\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">Maritime</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"title\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">この度はご注文いただき誠にありがとうございます。</div>\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            下記ご注文内容にお間違えがないかご確認下さい。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご請求金額<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご注文日時：2020/06/13 12:29:28<br/>\n                            ご注文番号：61<br/>\n                            お支払い合計：￥4,384<br/>\n                            お支払い方法：PayPal決済<br/>\n                                                        ご利用ポイント：0 pt<br/>\n                                                        お問い合わせ：<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご注文商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                            商品コード：cbd-water-500<br/>\n                                商品名：MARITIME CBD ウォーター 500ml  通常購入  <br/>\n                                単価：￥4,384<br/>\n                                数量：4<br/>\n                                <br/>\n                                                        <hr style=\"border-top: 2px dashed #8c8b8b;\">\n                            小　計：￥4,384<br/>\n                            <br/>\n                            手数料：￥0<br/>\n                            送　料：￥0<br/>\n                                                        <hr style=\"border-top: 1px dotted #8c8b8b;\">\n                            合　計：￥4,384<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご注文者情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            お名前：Frederic Vervaecke 様<br/>\n                            お名前(カナ)：フレデリック ヴェルヴァーク 様<br/>\n                                                        郵便番号：〒1570077<br/>\n                            住所：東京都世田谷区鎌田123建物<br/>\n                            電話番号：0123456789<br/>\n                            メールアドレス：djfre2000@hotmail.com<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　配送情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n\n                                                            ◎お届け先<br/>\n                                <br/>\n                                お名前：Frederic Vervaecke 様<br/>\n                                お名前(カナ)：フレデリック ヴェルヴァーク 様<br/>\n                                                                郵便番号：〒1570077<br/>\n                                住所：東京都世田谷区鎌田123建物<br/>\n                                電話番号：0123456789<br/>\n                                <br/>\n                                配送方法：スタンダード宅配<br/>\n                                お届け日：指定なし<br/>\n                                お届け時間：指定なし<br/>\n                                <br/>\n                                                                    商品コード：cbd-water-500<br/>\n                                    商品名：MARITIME CBD ウォーター 500ml  通常購入  <br/>\n                                    数量：4<br/>\n                                    <br/>\n                                                                                                                    <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br/>\n                            このメッセージへの返信としてご質問をお送りいただいても回答できません。<br/>\n                            ご了承ください。<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、Maritimeより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/\" style=\"color:#aaaaaa;text-decoration:none;\">Maritime</a></div>\n                        <div>copyright &copy; Maritime all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', 'mailhistory'),
(2, 61, 1, '2020-06-14 05:22:52', '[Maritime] ご注文ありがとうございます', 'Frederic Vervaecke 様\r\n\r\nこの度はご注文いただき誠にありがとうございます。下記ご注文内容にお間違えがないかご確認下さい。\r\n\r\n************************************************\r\n　ご請求金額\r\n************************************************\r\n\r\nご注文日時：2020/06/13 12:29:28\r\nご注文番号：61\r\nお支払い合計：￥4,384\r\nお支払い方法：PayPal決済\r\nご利用ポイント：0 pt\r\nお問い合わせ：\r\n\r\n\r\n************************************************\r\n　ご注文商品明細\r\n************************************************\r\n\r\n商品コード：cbd-water-500\r\n商品名：MARITIME CBD ウォーター 500ml  通常購入  \r\n単価：￥4,384\r\n数量：4\r\n\r\n\r\n-------------------------------------------------\r\n小　計：￥4,384\r\n\r\n手数料：￥0\r\n送　料：￥0\r\n============================================\r\n合　計：￥4,384\r\n\r\n************************************************\r\n　ご注文者情報\r\n************************************************\r\nお名前：Frederic Vervaecke 様\r\nお名前(カナ)：フレデリック ヴェルヴァーク 様\r\n郵便番号：〒1570077\r\n住所：東京都世田谷区鎌田123建物\r\n電話番号：0123456789\r\nメールアドレス：djfre2000@hotmail.com\r\n\r\n************************************************\r\n　配送情報\r\n************************************************\r\n\r\n◎お届け先\r\nお名前：Frederic Vervaecke 様\r\nお名前(カナ)：フレデリック ヴェルヴァーク 様\r\n郵便番号：〒1570077\r\n住所：東京都世田谷区鎌田123建物\r\n電話番号：0123456789\r\n\r\n配送方法：スタンダード宅配\r\nお届け日：指定なし\r\nお届け時間：指定なし\r\n\r\n商品コード：cbd-water-500\r\n商品名：MARITIME CBD ウォーター 500ml  通常購入  \r\n数量：4\r\n\r\nTEST FREDERIC!\r\n\r\n============================================\r\n\r\nこのメッセージはお客様へのお知らせ専用ですので、\r\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。\r\nご了承ください。', NULL, 'mailhistory'),
(3, 61, 1, '2020-06-15 04:11:38', '[Maritime] ご注文ありがとうございます', 'Frederic Vervaecke 様\r\n\r\nこの度はご注文いただき誠にありがとうございます。下記ご注文内容にお間違えがないかご確認下さい。\r\n\r\n************************************************\r\n　ご請求金額\r\n************************************************\r\n\r\nご注文日時：2020/06/13 12:29:28\r\nご注文番号：61\r\nお支払い合計：￥4,384\r\nお支払い方法：PayPal決済\r\nご利用ポイント：0 pt\r\nお問い合わせ：\r\n\r\n\r\n************************************************\r\n　ご注文商品明細\r\n************************************************\r\n\r\n商品コード：cbd-water-500\r\n商品名：MARITIME CBD ウォーター 500ml  通常購入  \r\n単価：￥4,384\r\n数量：4\r\n\r\n\r\n-------------------------------------------------\r\n小　計：￥4,384\r\n\r\n手数料：￥0\r\n送　料：￥0\r\n============================================\r\n合　計：￥4,384\r\n\r\n************************************************\r\n　ご注文者情報\r\n************************************************\r\nお名前：Frederic Vervaecke 様\r\nお名前(カナ)：フレデリック ヴェルヴァーク 様\r\n郵便番号：〒1570077\r\n住所：東京都世田谷区鎌田123建物\r\n電話番号：0123456789\r\nメールアドレス：djfre2000@hotmail.com\r\n\r\n************************************************\r\n　配送情報\r\n************************************************\r\n\r\n◎お届け先\r\nお名前：Frederic Vervaecke 様\r\nお名前(カナ)：フレデリック ヴェルヴァーク 様\r\n郵便番号：〒1570077\r\n住所：東京都世田谷区鎌田123建物\r\n電話番号：0123456789\r\n\r\n配送方法：スタンダード宅配\r\nお届け日：指定なし\r\nお届け時間：指定なし\r\n\r\n商品コード：cbd-water-500\r\n商品名：MARITIME CBD ウォーター 500ml  通常購入  \r\n数量：4\r\n\r\n\r\n\r\n============================================\r\n\r\nこのメッセージはお客様へのお知らせ専用ですので、\r\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。\r\nご了承ください。', NULL, 'mailhistory'),
(4, 64, NULL, '2020-06-17 11:28:27', '[MARITIME] ご注文ありがとうございます', 'Frederic Vervaecke 様\n\nこの度はご注文いただき誠にありがとうございます。下記ご注文内容にお間違えがないかご確認下さい。\n\n************************************************\n　ご請求金額\n************************************************\n\nご注文日時：2020/06/17 20:27:16\nご注文番号：64\nお支払い合計：￥13,153\nお支払い方法：PayPal決済\nご利用ポイント：0 pt\nお問い合わせ：\n\n\n************************************************\n　ご注文商品明細\n************************************************\n\n商品コード：cbd-water-500-set\n商品名：MARITIME CBD ウォーター 500ml ６本セット  通常購入  \n単価：￥6,577\n数量：1\n\n商品コード：cbd-water-500\n商品名：MARITIME CBD ウォーター 500ml  通常購入  \n単価：￥6,576\n数量：6\n\n\n-------------------------------------------------\n小　計：￥13,153\n\n手数料：￥0\n送　料：￥0\n============================================\n合　計：￥13,153\n\n************************************************\n　ご注文者情報\n************************************************\nお名前：Frederic Vervaecke 様\nお名前(カナ)：フレデリック ヴェルヴァーク 様\n郵便番号：〒1570077\n住所：東京都世田谷区鎌田123建物\n電話番号：0123456789\nメールアドレス：djfre2000@hotmail.com\n\n************************************************\n　配送情報\n************************************************\n\n◎お届け先\nお名前：Frederic Vervaecke 様\nお名前(カナ)：フレデリック ヴェルヴァーク 様\n郵便番号：〒1570077\n住所：東京都世田谷区鎌田123建物\n電話番号：0123456789\n\n配送方法：スタンダード宅配\nお届け日：指定なし\nお届け時間：指定なし\n\n商品コード：cbd-water-500-set\n商品名：MARITIME CBD ウォーター 500ml ６本セット  通常購入  \n数量：1\n\n商品コード：cbd-water-500\n商品名：MARITIME CBD ウォーター 500ml  通常購入  \n数量：6\n\n\n\n============================================\n\nこのメッセージはお客様へのお知らせ専用ですので、\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。\nご了承ください。\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"title\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">この度はご注文いただき誠にありがとうございます。</div>\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            下記ご注文内容にお間違えがないかご確認下さい。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご請求金額<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご注文日時：2020/06/17 20:27:16<br/>\n                            ご注文番号：64<br/>\n                            お支払い合計：￥13,153<br/>\n                            お支払い方法：PayPal決済<br/>\n                                                        ご利用ポイント：0 pt<br/>\n                                                        お問い合わせ：<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご注文商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                            商品コード：cbd-water-500-set<br/>\n                                商品名：MARITIME CBD ウォーター 500ml ６本セット  通常購入  <br/>\n                                単価：￥6,577<br/>\n                                数量：1<br/>\n                                <br/>\n                                                            商品コード：cbd-water-500<br/>\n                                商品名：MARITIME CBD ウォーター 500ml  通常購入  <br/>\n                                単価：￥6,576<br/>\n                                数量：6<br/>\n                                <br/>\n                                                        <hr style=\"border-top: 2px dashed #8c8b8b;\">\n                            小　計：￥13,153<br/>\n                            <br/>\n                            手数料：￥0<br/>\n                            送　料：￥0<br/>\n                                                        <hr style=\"border-top: 1px dotted #8c8b8b;\">\n                            合　計：￥13,153<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご注文者情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            お名前：Frederic Vervaecke 様<br/>\n                            お名前(カナ)：フレデリック ヴェルヴァーク 様<br/>\n                                                        郵便番号：〒1570077<br/>\n                            住所：東京都世田谷区鎌田123建物<br/>\n                            電話番号：0123456789<br/>\n                            メールアドレス：djfre2000@hotmail.com<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　配送情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n\n                                                            ◎お届け先<br/>\n                                <br/>\n                                お名前：Frederic Vervaecke 様<br/>\n                                お名前(カナ)：フレデリック ヴェルヴァーク 様<br/>\n                                                                郵便番号：〒1570077<br/>\n                                住所：東京都世田谷区鎌田123建物<br/>\n                                電話番号：0123456789<br/>\n                                <br/>\n                                配送方法：スタンダード宅配<br/>\n                                お届け日：指定なし<br/>\n                                お届け時間：指定なし<br/>\n                                <br/>\n                                                                    商品コード：cbd-water-500-set<br/>\n                                    商品名：MARITIME CBD ウォーター 500ml ６本セット  通常購入  <br/>\n                                    数量：1<br/>\n                                    <br/>\n                                                                    商品コード：cbd-water-500<br/>\n                                    商品名：MARITIME CBD ウォーター 500ml  通常購入  <br/>\n                                    数量：6<br/>\n                                    <br/>\n                                                                                                                    <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br/>\n                            このメッセージへの返信としてご質問をお送りいただいても回答できません。<br/>\n                            ご了承ください。<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', 'mailhistory'),
(5, 67, NULL, '2020-06-26 04:21:12', '[MARITIME] ご注文ありがとうございます', 'Frederic Vervaecke 様\n\nこの度はご注文いただき誠にありがとうございます。下記ご注文内容にお間違えがないかご確認下さい。\n\n************************************************\n　ご請求金額\n************************************************\n\nご注文日時：2020/06/26 13:20:30\nご注文番号：67\nお支払い合計：￥1,928\nお支払い方法：PayPal決済\nご利用ポイント：0 pt\nお問い合わせ：\n\n\n************************************************\n　ご注文商品明細\n************************************************\n\n商品コード：cbd-water-500\n商品名：MARITIME CBD ウォーター 500ml  通常購入  \n単価：￥1,128\n数量：1\n\n\n-------------------------------------------------\n小　計：￥1,128\n\n手数料：￥0\n送　料：￥800\n============================================\n合　計：￥1,928\n\n************************************************\n　ご注文者情報\n************************************************\nお名前：Frederic Vervaecke 様\nお名前(カナ)：フレデリック ヴェルヴァーク 様\n郵便番号：〒1570077\n住所：東京都世田谷区鎌田123建物\n電話番号：0123456789\nメールアドレス：djfre2000@hotmail.com\n\n************************************************\n　配送情報\n************************************************\n\n◎お届け先\nお名前：Frederic Vervaecke 様\nお名前(カナ)：フレデリック ヴェルヴァーク 様\n郵便番号：〒1570077\n住所：東京都世田谷区鎌田123建物\n電話番号：0123456789\n\n配送方法：スタンダード宅配\nお届け日：指定なし\nお届け時間：指定なし\n\n商品コード：cbd-water-500\n商品名：MARITIME CBD ウォーター 500ml  通常購入  \n数量：1\n\n\n\n============================================\n\nこのメッセージはお客様へのお知らせ専用ですので、\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。\nご了承ください。\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"title\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">この度はご注文いただき誠にありがとうございます。</div>\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            下記ご注文内容にお間違えがないかご確認下さい。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご請求金額<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご注文日時：2020/06/26 13:20:30<br/>\n                            ご注文番号：67<br/>\n                            お支払い合計：￥1,928<br/>\n                            お支払い方法：PayPal決済<br/>\n                                                        ご利用ポイント：0 pt<br/>\n                                                        お問い合わせ：<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご注文商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                            商品コード：cbd-water-500<br/>\n                                商品名：MARITIME CBD ウォーター 500ml  通常購入  <br/>\n                                単価：￥1,128<br/>\n                                数量：1<br/>\n                                <br/>\n                                                        <hr style=\"border-top: 2px dashed #8c8b8b;\">\n                            小　計：￥1,128<br/>\n                            <br/>\n                            手数料：￥0<br/>\n                            送　料：￥800<br/>\n                                                        <hr style=\"border-top: 1px dotted #8c8b8b;\">\n                            合　計：￥1,928<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご注文者情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            お名前：Frederic Vervaecke 様<br/>\n                            お名前(カナ)：フレデリック ヴェルヴァーク 様<br/>\n                                                        郵便番号：〒1570077<br/>\n                            住所：東京都世田谷区鎌田123建物<br/>\n                            電話番号：0123456789<br/>\n                            メールアドレス：djfre2000@hotmail.com<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　配送情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n\n                                                            ◎お届け先<br/>\n                                <br/>\n                                お名前：Frederic Vervaecke 様<br/>\n                                お名前(カナ)：フレデリック ヴェルヴァーク 様<br/>\n                                                                郵便番号：〒1570077<br/>\n                                住所：東京都世田谷区鎌田123建物<br/>\n                                電話番号：0123456789<br/>\n                                <br/>\n                                配送方法：スタンダード宅配<br/>\n                                お届け日：指定なし<br/>\n                                お届け時間：指定なし<br/>\n                                <br/>\n                                                                    商品コード：cbd-water-500<br/>\n                                    商品名：MARITIME CBD ウォーター 500ml  通常購入  <br/>\n                                    数量：1<br/>\n                                    <br/>\n                                                                                                                    <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br/>\n                            このメッセージへの返信としてご質問をお送りいただいても回答できません。<br/>\n                            ご了承ください。<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', 'mailhistory'),
(6, 67, 1, '2020-06-26 04:29:46', '[MARITIME] Test', 'test', NULL, 'mailhistory'),
(7, 71, NULL, '2020-06-26 09:02:36', '[MARITIME] ご注文ありがとうございます', 'Frederic Vervaecke 様\n\nこの度はご注文いただき誠にありがとうございます。下記ご注文内容にお間違えがないかご確認下さい。\n\n************************************************\n　ご請求金額\n************************************************\n\nご注文日時：2020/06/26 18:01:33\nご注文番号：71\nお支払い合計：￥4,666\nお支払い方法：PayPal決済\nご利用ポイント：0 pt\nお問い合わせ：\n\n\n************************************************\n　ご注文商品明細\n************************************************\n\n商品コード：cbd-100\n商品名：MARITIME CBDオイル 10ml (1%)  通常購入  \n単価：￥4,666\n数量：1\n\n\n-------------------------------------------------\n小　計：￥4,666\n\n手数料：￥0\n送　料：￥0\n============================================\n合　計：￥4,666\n\n************************************************\n　ご注文者情報\n************************************************\nお名前：Frederic Vervaecke 様\nお名前(カナ)：フレデリック ヴェルヴァーク 様\n郵便番号：〒1570077\n住所：東京都世田谷区鎌田123建物\n電話番号：0123456789\nメールアドレス：djfre2000@hotmail.com\n\n************************************************\n　配送情報\n************************************************\n\n◎お届け先\nお名前：Frederic Vervaecke 様\nお名前(カナ)：フレデリック ヴェルヴァーク 様\n郵便番号：〒1570077\n住所：東京都世田谷区鎌田123建物\n電話番号：0123456789\n\n配送方法：スタンダード宅配\nお届け日：指定なし\nお届け時間：指定なし\n\n商品コード：cbd-100\n商品名：MARITIME CBDオイル 10ml (1%)  通常購入  \n数量：1\n\n\n\n============================================\n\nこのメッセージはお客様へのお知らせ専用ですので、\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。\nご了承ください。\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"title\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">この度はご注文いただき誠にありがとうございます。</div>\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            下記ご注文内容にお間違えがないかご確認下さい。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご請求金額<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご注文日時：2020/06/26 18:01:33<br/>\n                            ご注文番号：71<br/>\n                            お支払い合計：￥4,666<br/>\n                            お支払い方法：PayPal決済<br/>\n                                                        ご利用ポイント：0 pt<br/>\n                                                        お問い合わせ：<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご注文商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                            商品コード：cbd-100<br/>\n                                商品名：MARITIME CBDオイル 10ml (1%)  通常購入  <br/>\n                                単価：￥4,666<br/>\n                                数量：1<br/>\n                                <br/>\n                                                        <hr style=\"border-top: 2px dashed #8c8b8b;\">\n                            小　計：￥4,666<br/>\n                            <br/>\n                            手数料：￥0<br/>\n                            送　料：￥0<br/>\n                                                        <hr style=\"border-top: 1px dotted #8c8b8b;\">\n                            合　計：￥4,666<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご注文者情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            お名前：Frederic Vervaecke 様<br/>\n                            お名前(カナ)：フレデリック ヴェルヴァーク 様<br/>\n                                                        郵便番号：〒1570077<br/>\n                            住所：東京都世田谷区鎌田123建物<br/>\n                            電話番号：0123456789<br/>\n                            メールアドレス：djfre2000@hotmail.com<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　配送情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n\n                                                            ◎お届け先<br/>\n                                <br/>\n                                お名前：Frederic Vervaecke 様<br/>\n                                お名前(カナ)：フレデリック ヴェルヴァーク 様<br/>\n                                                                郵便番号：〒1570077<br/>\n                                住所：東京都世田谷区鎌田123建物<br/>\n                                電話番号：0123456789<br/>\n                                <br/>\n                                配送方法：スタンダード宅配<br/>\n                                お届け日：指定なし<br/>\n                                お届け時間：指定なし<br/>\n                                <br/>\n                                                                    商品コード：cbd-100<br/>\n                                    商品名：MARITIME CBDオイル 10ml (1%)  通常購入  <br/>\n                                    数量：1<br/>\n                                    <br/>\n                                                                                                                    <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br/>\n                            このメッセージへの返信としてご質問をお送りいただいても回答できません。<br/>\n                            ご了承ください。<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', 'mailhistory'),
(8, 72, NULL, '2020-06-30 11:04:50', '[MARITIME] ご注文ありがとうございます', 'Frederic Vervaecke 様\n\nこの度はご注文いただき誠にありがとうございます。下記ご注文内容にお間違いがないかご確認下さい。\n\n************************************************\n　ご請求金額\n************************************************\n\nご注文日時：2020/06/30 20:01:54\nご注文番号：72\nお支払い合計：￥31,590\nお支払い方法：PayPal決済\nご利用ポイント：0 pt\nお問い合わせ：\n\n\n************************************************\n　ご注文商品明細\n************************************************\n\n商品コード：cbd-1500\n商品名：MARITIME CBDオイル 30ml (5%)  通常購入  \n単価：￥31,590\n数量：1\n\n\n-------------------------------------------------\n小　計：￥31,590\n\n手数料：￥0\n送　料：￥0\n============================================\n合　計：￥31,590\n\n************************************************\n　ご注文者情報\n************************************************\nお名前：Frederic Vervaecke 様\nお名前(カナ)：フレデリック ヴェルヴァーク 様\n郵便番号：〒1570077\n住所：東京都世田谷区鎌田123建物\n電話番号：0123456789\nメールアドレス：djfre2000@hotmail.com\n\n************************************************\n　配送情報\n************************************************\n\n◎お届け先\nお名前：Frederic Vervaecke 様\nお名前(カナ)：フレデリック ヴェルヴァーク 様\n郵便番号：〒1570077\n住所：東京都世田谷区鎌田123建物\n電話番号：0123456789\n\n配送方法：スタンダード宅配\nお届け日：指定なし\nお届け時間：指定なし\n\n商品コード：cbd-1500\n商品名：MARITIME CBDオイル 30ml (5%)  通常購入  \n数量：1\n\n\n\n============================================\n\nこのメッセージはお客様へのお知らせ専用ですので、\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。\nご了承ください。\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"title\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">この度はご注文いただき誠にありがとうございます。</div>\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            下記ご注文内容にお間違いがないかご確認下さい。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご請求金額<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご注文日時：2020/06/30 20:01:54<br/>\n                            ご注文番号：72<br/>\n                            お支払い合計：￥31,590<br/>\n                            お支払い方法：PayPal決済<br/>\n                                                        ご利用ポイント：0 pt<br/>\n                                                        お問い合わせ：<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご注文商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                            商品コード：cbd-1500<br/>\n                                商品名：MARITIME CBDオイル 30ml (5%)  通常購入  <br/>\n                                単価：￥31,590<br/>\n                                数量：1<br/>\n                                <br/>\n                                                        <hr style=\"border-top: 2px dashed #8c8b8b;\">\n                            小　計：￥31,590<br/>\n                            <br/>\n                            手数料：￥0<br/>\n                            送　料：￥0<br/>\n                                                        <hr style=\"border-top: 1px dotted #8c8b8b;\">\n                            合　計：￥31,590<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご注文者情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            お名前：Frederic Vervaecke 様<br/>\n                            お名前(カナ)：フレデリック ヴェルヴァーク 様<br/>\n                                                        郵便番号：〒1570077<br/>\n                            住所：東京都世田谷区鎌田123建物<br/>\n                            電話番号：0123456789<br/>\n                            メールアドレス：djfre2000@hotmail.com<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　配送情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n\n                                                            ◎お届け先<br/>\n                                <br/>\n                                お名前：Frederic Vervaecke 様<br/>\n                                お名前(カナ)：フレデリック ヴェルヴァーク 様<br/>\n                                                                郵便番号：〒1570077<br/>\n                                住所：東京都世田谷区鎌田123建物<br/>\n                                電話番号：0123456789<br/>\n                                <br/>\n                                配送方法：スタンダード宅配<br/>\n                                お届け日：指定なし<br/>\n                                お届け時間：指定なし<br/>\n                                <br/>\n                                                                    商品コード：cbd-1500<br/>\n                                    商品名：MARITIME CBDオイル 30ml (5%)  通常購入  <br/>\n                                    数量：1<br/>\n                                    <br/>\n                                                                                                                    <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br/>\n                            このメッセージへの返信としてご質問をお送りいただいても回答できません。<br/>\n                            ご了承ください。<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', 'mailhistory');
INSERT INTO `dtb_mail_history` (`id`, `order_id`, `creator_id`, `send_date`, `mail_subject`, `mail_body`, `mail_html_body`, `discriminator_type`) VALUES
(9, 80, NULL, '2020-07-16 10:13:40', '[MARITIME] ご予約注文ありがとうございます', 'Frederic Vervaecke 様\n\nこの度はご予約注文いただき誠にありがとうございます。下記ご予約注文内容にお間違えがないかご確認下さい。\n\n************************************************\n　ご請求金額\n************************************************\n\nご予約注文日時：2020/07/16 19:13:35\nご予約注文番号：80\nお支払い合計：￥1,748\nお支払い方法：PayPal決済\nご利用ポイント：0 pt\n加算ポイント：10 pt\nお問い合わせ：\n\n\n************************************************\n　ご予約注文商品明細\n************************************************\n\n商品コード：cbd-water-500\n商品名：MARITIME CBD ウォーター 500ml  通常購入   \n単価：￥1,148\n数量：1\n\n※は軽減税率対象商品です。\n-------------------------------------------------\n小　計：￥1,148\n手数料：￥0\n送　料：￥600\n-------------------------------------------------\n合　計：￥1,748\n        (10 %対象：￥1,748)\n    ============================================\nお支払い合計：￥1,748\n\n************************************************\n　ご予約注文者情報\n************************************************\nお名前：Frederic Vervaecke 様\nお名前(カナ)：フレデリック ヴェルヴァーク 様\n郵便番号：〒1570077\n住所：東京都世田谷区鎌田123建物\n電話番号：0123456789\nメールアドレス：djfre2000@hotmail.com\n\n************************************************\n　配送情報\n************************************************\n\n◎お届け先\nお名前：Frederic Vervaecke 様\nお名前(カナ)：フレデリック ヴェルヴァーク 様\n郵便番号：〒1570077\n住所：東京都世田谷区鎌田123建物\n電話番号：0123456789\n\n配送方法：スタンダード宅配\nお届け日：指定なし\nお届け時間：指定なし\n\n商品コード：cbd-water-500\n商品名：MARITIME CBD ウォーター 500ml  通常購入  \n数量：1\n発送予定日：2020年7月19日\n\n\n============================================\n\nこのメッセージはお客様へのお知らせ専用ですので、\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。\nご了承ください。\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"title\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">この度はご予約注文いただき誠にありがとうございます。</div>\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            下記ご予約注文内容にお間違えがないかご確認下さい。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご請求金額<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご予約注文日時：2020/07/16 19:13:35<br/>\n                            ご予約注文番号：80<br/>\n                            お支払い合計：￥1,748<br/>\n                            お支払い方法：PayPal決済<br/>\n                                                        ご利用ポイント：0 pt<br/>\n                            加算ポイント：10 pt<br/>\n                                                        お問い合わせ：<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご予約注文商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                            商品コード：cbd-water-500<br/>\n                                商品名：MARITIME CBD ウォーター 500ml  通常購入   <br/>\n                                単価：￥1,148<br/>\n                                数量：1<br/>\n                                <br/>\n                                                        ※は軽減税率対象商品です。\n                            <hr style=\"border-top: 2px dashed #8c8b8b;\">\n                            小　計：￥1,148<br/>\n                            手数料：￥0<br/>\n                            送　料：￥600<br/>\n                                                        <hr style=\"border-top: 1px dotted #8c8b8b;\">\n                            合　計：￥1,748<br/>\n                                                            (10 %対象：￥1,748)<br/>\n                                                                                    <hr style=\"border-top: 1px dotted #8c8b8b;\">\n                            お支払い合計：￥1,748\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご予約注文者情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            お名前：Frederic Vervaecke 様<br/>\n                            お名前(カナ)：フレデリック ヴェルヴァーク 様<br/>\n                                                        郵便番号：〒1570077<br/>\n                            住所：東京都世田谷区鎌田123建物<br/>\n                            電話番号：0123456789<br/>\n                            メールアドレス：djfre2000@hotmail.com<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　配送情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n\n                                                            ◎お届け先<br/>\n                                <br/>\n                                お名前：Frederic Vervaecke 様<br/>\n                                お名前(カナ)：フレデリック ヴェルヴァーク 様<br/>\n                                                                郵便番号：〒1570077<br/>\n                                住所：東京都世田谷区鎌田123建物<br/>\n                                電話番号：0123456789<br/>\n                                <br/>\n                                配送方法：スタンダード宅配<br/>\n                                お届け日：指定なし<br/>\n                                お届け時間：指定なし<br/>\n                                <br/>\n                                                                    商品コード：cbd-water-500<br/>\n                                    商品名：MARITIME CBD ウォーター 500ml  通常購入  <br/>\n                                    数量：1<br/>\n                                                                        発送予定日：2020年7月19日<br/>\n                                                                        <br/>\n                                                                                                                    <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br/>\n                            このメッセージへの返信としてご質問をお送りいただいても回答できません。<br/>\n                            ご了承ください。<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', 'mailhistory'),
(10, 80, 1, '2020-07-16 12:17:34', '[MARITIME] 商品出荷のお知らせ', 'Frederic Vervaecke 様\n\nお客さまがご注文された以下の商品を発送いたしました。商品の到着まで、今しばらくお待ちください。\n\n\n************************************************\n　ご注文商品明細\n************************************************\n\n商品コード：cbd-water-500\n商品名：MARITIME CBD ウォーター 500ml  通常購入  \n数量：1\n\n\n============================================\n\n************************************************\n　ご注文者情報\n************************************************\nお名前：Frederic Vervaecke 様\nお名前(カナ)：フレデリック ヴェルヴァーク 様\n郵便番号：〒1570077\n住所：東京都世田谷区鎌田123建物\n電話番号：0123456789\n\n************************************************\n　配送情報\n************************************************\n\nお名前：Frederic Vervaecke 様\nお名前(カナ)：フレデリック ヴェルヴァーク 様\n郵便番号：〒1570077\n住所：東京都世田谷区鎌田123建物\n電話番号：0123456789\n\nお届け日：指定なし\nお届け時間：指定なし\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"title\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:600;color:#374550;\">商品を発送いたしました。</div>\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br>\n                            <br>\n                            MARITIMEでございます。<br/>\n                            お客さまがご注文された以下の商品を発送いたしました。商品の到着まで、今しばらくお待ちください。<br/>\n                            <br/>\n                                                        <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご注文商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                            商品コード：cbd-water-500<br/>\n                                商品名：MARITIME CBD ウォーター 500ml  通常購入  <br/>\n                                数量：1<br/>\n                                <br/>\n                                                        <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご注文者情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            お名前：Frederic Vervaecke 様<br/>\n                            お名前(カナ)：フレデリック ヴェルヴァーク 様<br/>\n                                                        郵便番号：〒1570077<br/>\n                            住所：東京都世田谷区鎌田123建物<br/>\n                            電話番号：0123456789<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　配送情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            お名前：Frederic Vervaecke 様<br/>\n                            お名前(カナ)：フレデリック ヴェルヴァーク 様<br/>\n                                                        郵便番号：〒1570077<br/>\n                            住所：東京都世田谷区鎌田123建物<br/>\n                            電話番号：0123456789<br/>\n                            <br/>\n                            お届け日：指定なし<br/>\n                            お届け時間：指定なし<br/>\n                            <br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n\n', 'mailhistory'),
(11, 80, 1, '2020-07-16 12:18:51', '[MARITIME] 商品出荷のお知らせ', 'Frederic Vervaecke 様\n\nお客さまがご注文された以下の商品を発送いたしました。商品の到着まで、今しばらくお待ちください。\n\n\n************************************************\n　ご注文商品明細\n************************************************\n\n商品コード：cbd-water-500\n商品名：MARITIME CBD ウォーター 500ml  通常購入  \n数量：1\n\n\n============================================\n\n************************************************\n　ご注文者情報\n************************************************\nお名前：Frederic Vervaecke 様\nお名前(カナ)：フレデリック ヴェルヴァーク 様\n郵便番号：〒1570077\n住所：東京都世田谷区鎌田123建物\n電話番号：0123456789\n\n************************************************\n　配送情報\n************************************************\n\nお名前：Frederic Vervaecke 様\nお名前(カナ)：フレデリック ヴェルヴァーク 様\n郵便番号：〒1570077\n住所：東京都世田谷区鎌田123建物\n電話番号：0123456789\n\nお届け日：指定なし\nお届け時間：指定なし\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"title\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:600;color:#374550;\">商品を発送いたしました。</div>\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br>\n                            <br>\n                            MARITIMEでございます。<br/>\n                            お客さまがご注文された以下の商品を発送いたしました。商品の到着まで、今しばらくお待ちください。<br/>\n                            <br/>\n                                                        <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご注文商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                            商品コード：cbd-water-500<br/>\n                                商品名：MARITIME CBD ウォーター 500ml  通常購入  <br/>\n                                数量：1<br/>\n                                <br/>\n                                                        <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご注文者情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            お名前：Frederic Vervaecke 様<br/>\n                            お名前(カナ)：フレデリック ヴェルヴァーク 様<br/>\n                                                        郵便番号：〒1570077<br/>\n                            住所：東京都世田谷区鎌田123建物<br/>\n                            電話番号：0123456789<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　配送情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            お名前：Frederic Vervaecke 様<br/>\n                            お名前(カナ)：フレデリック ヴェルヴァーク 様<br/>\n                                                        郵便番号：〒1570077<br/>\n                            住所：東京都世田谷区鎌田123建物<br/>\n                            電話番号：0123456789<br/>\n                            <br/>\n                            お届け日：指定なし<br/>\n                            お届け時間：指定なし<br/>\n                            <br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n\n', 'mailhistory'),
(12, 82, NULL, '2020-07-16 12:22:27', '[MARITIME] ご予約注文ありがとうございます', 'Frederic Vervaecke 様\n\nこの度はご予約注文いただき誠にありがとうございます。下記ご予約注文内容にお間違えがないかご確認下さい。\n\n************************************************\n　ご請求金額\n************************************************\n\nご予約注文日時：2020/07/16 21:22:23\nご予約注文番号：82\nお支払い合計：￥2,896\nお支払い方法：PayPal決済\nご利用ポイント：0 pt\n加算ポイント：20 pt\nお問い合わせ：\n\n\n************************************************\n　ご予約注文商品明細\n************************************************\n\n商品コード：cbd-water-500\n商品名：MARITIME CBD ウォーター 500ml  通常購入   \n単価：￥1,148\n数量：2\n\n※は軽減税率対象商品です。\n-------------------------------------------------\n小　計：￥2,296\n手数料：￥0\n送　料：￥600\n-------------------------------------------------\n合　計：￥2,896\n        (10 %対象：￥2,896)\n    ============================================\nお支払い合計：￥2,896\n\n************************************************\n　ご予約注文者情報\n************************************************\nお名前：Frederic Vervaecke 様\nお名前(カナ)：フレデリック ヴェルヴァーク 様\n郵便番号：〒1570077\n住所：東京都世田谷区鎌田123建物\n電話番号：0123456789\nメールアドレス：djfre2000@hotmail.com\n\n************************************************\n　配送情報\n************************************************\n\n◎お届け先\nお名前：Frederic Vervaecke 様\nお名前(カナ)：フレデリック ヴェルヴァーク 様\n郵便番号：〒1570077\n住所：東京都世田谷区鎌田123建物\n電話番号：0123456789\n\n配送方法：スタンダード宅配\nお届け日：指定なし\nお届け時間：指定なし\n\n商品コード：cbd-water-500\n商品名：MARITIME CBD ウォーター 500ml  通常購入  \n数量：2\n発送予定日：2020年7月19日\n\n\n============================================\n\nこのメッセージはお客様へのお知らせ専用ですので、\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。\nご了承ください。\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"title\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">この度はご予約注文いただき誠にありがとうございます。</div>\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            下記ご予約注文内容にお間違えがないかご確認下さい。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご請求金額<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご予約注文日時：2020/07/16 21:22:23<br/>\n                            ご予約注文番号：82<br/>\n                            お支払い合計：￥2,896<br/>\n                            お支払い方法：PayPal決済<br/>\n                                                        ご利用ポイント：0 pt<br/>\n                            加算ポイント：20 pt<br/>\n                                                        お問い合わせ：<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご予約注文商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                            商品コード：cbd-water-500<br/>\n                                商品名：MARITIME CBD ウォーター 500ml  通常購入   <br/>\n                                単価：￥1,148<br/>\n                                数量：2<br/>\n                                <br/>\n                                                        ※は軽減税率対象商品です。\n                            <hr style=\"border-top: 2px dashed #8c8b8b;\">\n                            小　計：￥2,296<br/>\n                            手数料：￥0<br/>\n                            送　料：￥600<br/>\n                                                        <hr style=\"border-top: 1px dotted #8c8b8b;\">\n                            合　計：￥2,896<br/>\n                                                            (10 %対象：￥2,896)<br/>\n                                                                                    <hr style=\"border-top: 1px dotted #8c8b8b;\">\n                            お支払い合計：￥2,896\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご予約注文者情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            お名前：Frederic Vervaecke 様<br/>\n                            お名前(カナ)：フレデリック ヴェルヴァーク 様<br/>\n                                                        郵便番号：〒1570077<br/>\n                            住所：東京都世田谷区鎌田123建物<br/>\n                            電話番号：0123456789<br/>\n                            メールアドレス：djfre2000@hotmail.com<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　配送情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n\n                                                            ◎お届け先<br/>\n                                <br/>\n                                お名前：Frederic Vervaecke 様<br/>\n                                お名前(カナ)：フレデリック ヴェルヴァーク 様<br/>\n                                                                郵便番号：〒1570077<br/>\n                                住所：東京都世田谷区鎌田123建物<br/>\n                                電話番号：0123456789<br/>\n                                <br/>\n                                配送方法：スタンダード宅配<br/>\n                                お届け日：指定なし<br/>\n                                お届け時間：指定なし<br/>\n                                <br/>\n                                                                    商品コード：cbd-water-500<br/>\n                                    商品名：MARITIME CBD ウォーター 500ml  通常購入  <br/>\n                                    数量：2<br/>\n                                                                        発送予定日：2020年7月19日<br/>\n                                                                        <br/>\n                                                                                                                    <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br/>\n                            このメッセージへの返信としてご質問をお送りいただいても回答できません。<br/>\n                            ご了承ください。<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', 'mailhistory'),
(13, 83, NULL, '2020-07-16 13:13:39', '[MARITIME] ご予約注文ありがとうございます', 'Frederic Vervaecke 様\n\nこの度はご予約注文いただき誠にありがとうございます。下記ご予約注文内容にお間違えがないかご確認下さい。\n\n************************************************\n　ご請求金額\n************************************************\n\nご予約注文日時：2020/07/16 22:13:35\nご予約注文番号：83\nお支払い合計：￥1,748\nお支払い方法：PayPal決済\nご利用ポイント：0 pt\n加算ポイント：10 pt\nお問い合わせ：\n\n\n************************************************\n　ご予約注文商品明細\n************************************************\n\n商品コード：cbd-water-500\n商品名：MARITIME CBD ウォーター 500ml  通常購入   \n単価：￥1,148\n数量：1\n\n※は軽減税率対象商品です。\n-------------------------------------------------\n小　計：￥1,148\n手数料：￥0\n送　料：￥600\n-------------------------------------------------\n合　計：￥1,748\n        (10 %対象：￥1,748)\n    ============================================\nお支払い合計：￥1,748\n\n************************************************\n　ご予約注文者情報\n************************************************\nお名前：Frederic Vervaecke 様\nお名前(カナ)：フレデリック ヴェルヴァーク 様\n郵便番号：〒1570077\n住所：東京都世田谷区鎌田123建物\n電話番号：0123456789\nメールアドレス：djfre2000@hotmail.com\n\n************************************************\n　配送情報\n************************************************\n\n◎お届け先\nお名前：Frederic Vervaecke 様\nお名前(カナ)：フレデリック ヴェルヴァーク 様\n郵便番号：〒1570077\n住所：東京都世田谷区鎌田123建物\n電話番号：0123456789\n\n配送方法：スタンダード宅配\nお届け日：指定なし\nお届け時間：指定なし\n\n商品コード：cbd-water-500\n商品名：MARITIME CBD ウォーター 500ml  通常購入  \n数量：1\n発送予定日：2020年7月19日\n\n\n============================================\n\nこのメッセージはお客様へのお知らせ専用ですので、\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。\nご了承ください。\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"title\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">この度はご予約注文いただき誠にありがとうございます。</div>\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            下記ご予約注文内容にお間違えがないかご確認下さい。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご請求金額<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご予約注文日時：2020/07/16 22:13:35<br/>\n                            ご予約注文番号：83<br/>\n                            お支払い合計：￥1,748<br/>\n                            お支払い方法：PayPal決済<br/>\n                                                        ご利用ポイント：0 pt<br/>\n                            加算ポイント：10 pt<br/>\n                                                        お問い合わせ：<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご予約注文商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                            商品コード：cbd-water-500<br/>\n                                商品名：MARITIME CBD ウォーター 500ml  通常購入   <br/>\n                                単価：￥1,148<br/>\n                                数量：1<br/>\n                                <br/>\n                                                        ※は軽減税率対象商品です。\n                            <hr style=\"border-top: 2px dashed #8c8b8b;\">\n                            小　計：￥1,148<br/>\n                            手数料：￥0<br/>\n                            送　料：￥600<br/>\n                                                        <hr style=\"border-top: 1px dotted #8c8b8b;\">\n                            合　計：￥1,748<br/>\n                                                            (10 %対象：￥1,748)<br/>\n                                                                                    <hr style=\"border-top: 1px dotted #8c8b8b;\">\n                            お支払い合計：￥1,748\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご予約注文者情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            お名前：Frederic Vervaecke 様<br/>\n                            お名前(カナ)：フレデリック ヴェルヴァーク 様<br/>\n                                                        郵便番号：〒1570077<br/>\n                            住所：東京都世田谷区鎌田123建物<br/>\n                            電話番号：0123456789<br/>\n                            メールアドレス：djfre2000@hotmail.com<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　配送情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n\n                                                            ◎お届け先<br/>\n                                <br/>\n                                お名前：Frederic Vervaecke 様<br/>\n                                お名前(カナ)：フレデリック ヴェルヴァーク 様<br/>\n                                                                郵便番号：〒1570077<br/>\n                                住所：東京都世田谷区鎌田123建物<br/>\n                                電話番号：0123456789<br/>\n                                <br/>\n                                配送方法：スタンダード宅配<br/>\n                                お届け日：指定なし<br/>\n                                お届け時間：指定なし<br/>\n                                <br/>\n                                                                    商品コード：cbd-water-500<br/>\n                                    商品名：MARITIME CBD ウォーター 500ml  通常購入  <br/>\n                                    数量：1<br/>\n                                                                        発送予定日：2020年7月19日<br/>\n                                                                        <br/>\n                                                                                                                    <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br/>\n                            このメッセージへの返信としてご質問をお送りいただいても回答できません。<br/>\n                            ご了承ください。<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', 'mailhistory'),
(14, 84, NULL, '2020-07-17 08:14:16', '[MARITIME] ご予約注文ありがとうございます', 'Frederic Vervaecke 様\n\nこの度はご予約注文いただき誠にありがとうございます。下記ご予約注文内容にお間違えがないかご確認下さい。\n\n************************************************\n　ご請求金額\n************************************************\n\nご予約注文日時：2020/07/17 17:14:11\nご予約注文番号：84\nお支払い合計：￥1,748\nお支払い方法：PayPal決済\nご利用ポイント：0 pt\n加算ポイント：10 pt\nお問い合わせ：\n\n\n************************************************\n　ご予約注文商品明細\n************************************************\n\n商品コード：cbd-water-500\n商品名：MARITIME CBD ウォーター 500ml  通常購入   \n単価：￥1,148\n数量：1\n\n※は軽減税率対象商品です。\n-------------------------------------------------\n小　計：￥1,148\n手数料：￥0\n送　料：￥600\n-------------------------------------------------\n合　計：￥1,748\n        (10 %対象：￥1,748)\n    ============================================\nお支払い合計：￥1,748\n\n************************************************\n　ご予約注文者情報\n************************************************\nお名前：Frederic Vervaecke 様\nお名前(カナ)：フレデリック ヴェルヴァーク 様\n郵便番号：〒1570077\n住所：東京都世田谷区鎌田123建物\n電話番号：0123456789\nメールアドレス：djfre2000@hotmail.com\n\n************************************************\n　配送情報\n************************************************\n\n◎お届け先\nお名前：Frederic Vervaecke 様\nお名前(カナ)：フレデリック ヴェルヴァーク 様\n郵便番号：〒1570077\n住所：東京都世田谷区鎌田123建物\n電話番号：0123456789\n\n配送方法：スタンダード宅配\nお届け日：指定なし\nお届け時間：指定なし\n\n商品コード：cbd-water-500\n商品名：MARITIME CBD ウォーター 500ml  通常購入  \n数量：1\n発送予定日：2020年7月19日\n\n\n============================================\n\nこのメッセージはお客様へのお知らせ専用ですので、\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。\nご了承ください。\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"title\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">この度はご予約注文いただき誠にありがとうございます。</div>\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            下記ご予約注文内容にお間違えがないかご確認下さい。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご請求金額<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご予約注文日時：2020/07/17 17:14:11<br/>\n                            ご予約注文番号：84<br/>\n                            お支払い合計：￥1,748<br/>\n                            お支払い方法：PayPal決済<br/>\n                                                        ご利用ポイント：0 pt<br/>\n                            加算ポイント：10 pt<br/>\n                                                        お問い合わせ：<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご予約注文商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                            商品コード：cbd-water-500<br/>\n                                商品名：MARITIME CBD ウォーター 500ml  通常購入   <br/>\n                                単価：￥1,148<br/>\n                                数量：1<br/>\n                                <br/>\n                                                        ※は軽減税率対象商品です。\n                            <hr style=\"border-top: 2px dashed #8c8b8b;\">\n                            小　計：￥1,148<br/>\n                            手数料：￥0<br/>\n                            送　料：￥600<br/>\n                                                        <hr style=\"border-top: 1px dotted #8c8b8b;\">\n                            合　計：￥1,748<br/>\n                                                            (10 %対象：￥1,748)<br/>\n                                                                                    <hr style=\"border-top: 1px dotted #8c8b8b;\">\n                            お支払い合計：￥1,748\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご予約注文者情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            お名前：Frederic Vervaecke 様<br/>\n                            お名前(カナ)：フレデリック ヴェルヴァーク 様<br/>\n                                                        郵便番号：〒1570077<br/>\n                            住所：東京都世田谷区鎌田123建物<br/>\n                            電話番号：0123456789<br/>\n                            メールアドレス：djfre2000@hotmail.com<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　配送情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n\n                                                            ◎お届け先<br/>\n                                <br/>\n                                お名前：Frederic Vervaecke 様<br/>\n                                お名前(カナ)：フレデリック ヴェルヴァーク 様<br/>\n                                                                郵便番号：〒1570077<br/>\n                                住所：東京都世田谷区鎌田123建物<br/>\n                                電話番号：0123456789<br/>\n                                <br/>\n                                配送方法：スタンダード宅配<br/>\n                                お届け日：指定なし<br/>\n                                お届け時間：指定なし<br/>\n                                <br/>\n                                                                    商品コード：cbd-water-500<br/>\n                                    商品名：MARITIME CBD ウォーター 500ml  通常購入  <br/>\n                                    数量：1<br/>\n                                                                        発送予定日：2020年7月19日<br/>\n                                                                        <br/>\n                                                                                                                    <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br/>\n                            このメッセージへの返信としてご質問をお送りいただいても回答できません。<br/>\n                            ご了承ください。<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', 'mailhistory');
INSERT INTO `dtb_mail_history` (`id`, `order_id`, `creator_id`, `send_date`, `mail_subject`, `mail_body`, `mail_html_body`, `discriminator_type`) VALUES
(15, 89, NULL, '2020-08-05 11:47:11', '[MARITIME] ご注文ありがとうございます', 'Frederic Vervaecke 様\n\nこの度はご注文いただき誠にありがとうございます。下記ご注文内容にお間違いがないかご確認下さい。\n\n************************************************\n　ご請求金額\n************************************************\n\nご注文日時：Aug 5, 2020 6:06:03 PM\nご注文番号：89\nお支払い合計：¥1,748\nお支払い方法：PayPal決済\nご利用ポイント：0 pt\nお問い合わせ：\n\n\n************************************************\n　ご注文商品明細\n************************************************\n\n商品コード：cbd-water-500\n商品名：MARITIME CBD Water 500ml  通常購入  \n単価：¥1,148\n数量：1\n\n\n-------------------------------------------------\n小　計：¥1,148\n\n手数料：¥0\n送　料：¥600\n============================================\n合　計：¥1,748\n\n************************************************\n　ご注文者情報\n************************************************\nお名前：Frederic Vervaecke 様\nお名前(カナ)：フレデリック ヴェルヴァーク 様\n郵便番号：〒1570077\n住所：東京都世田谷区鎌田123建物\n電話番号：0123456789\nメールアドレス：djfre2000@hotmail.com\n\n************************************************\n　配送情報\n************************************************\n\n◎お届け先\nお名前：Frederic Vervaecke 様\nお名前(カナ)：フレデリック ヴェルヴァーク 様\n郵便番号：〒1570077\n住所：東京都世田谷区鎌田123建物\n電話番号：0123456789\n\n配送方法：佐川急便\nお届け日：指定なし\nお届け時間：指定なし\n\n商品コード：cbd-water-500\n商品名：MARITIME CBD Water 500ml  通常購入  \n数量：1\n\n\n\n============================================\n\nこのメッセージはお客様へのお知らせ専用ですので、\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。\nご了承ください。\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/en/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"title\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">この度はご注文いただき誠にありがとうございます。</div>\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            下記ご注文内容にお間違いがないかご確認下さい。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご請求金額<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご注文日時：Aug 5, 2020 6:06:03 PM<br/>\n                            ご注文番号：89<br/>\n                            お支払い合計：¥1,748<br/>\n                            お支払い方法：PayPal決済<br/>\n                                                        ご利用ポイント：0 pt<br/>\n                                                        お問い合わせ：<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご注文商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                            商品コード：cbd-water-500<br/>\n                                商品名：MARITIME CBD Water 500ml  通常購入  <br/>\n                                単価：¥1,148<br/>\n                                数量：1<br/>\n                                <br/>\n                                                        <hr style=\"border-top: 2px dashed #8c8b8b;\">\n                            小　計：¥1,148<br/>\n                            <br/>\n                            手数料：¥0<br/>\n                            送　料：¥600<br/>\n                                                        <hr style=\"border-top: 1px dotted #8c8b8b;\">\n                            合　計：¥1,748<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご注文者情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            お名前：Frederic Vervaecke 様<br/>\n                            お名前(カナ)：フレデリック ヴェルヴァーク 様<br/>\n                                                        郵便番号：〒1570077<br/>\n                            住所：東京都世田谷区鎌田123建物<br/>\n                            電話番号：0123456789<br/>\n                            メールアドレス：djfre2000@hotmail.com<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　配送情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n\n                                                            ◎お届け先<br/>\n                                <br/>\n                                お名前：Frederic Vervaecke 様<br/>\n                                お名前(カナ)：フレデリック ヴェルヴァーク 様<br/>\n                                                                郵便番号：〒1570077<br/>\n                                住所：東京都世田谷区鎌田123建物<br/>\n                                電話番号：0123456789<br/>\n                                <br/>\n                                配送方法：佐川急便<br/>\n                                お届け日：指定なし<br/>\n                                お届け時間：指定なし<br/>\n                                <br/>\n                                                                    商品コード：cbd-water-500<br/>\n                                    商品名：MARITIME CBD Water 500ml  通常購入  <br/>\n                                    数量：1<br/>\n                                    <br/>\n                                                                                                                    <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br/>\n                            このメッセージへの返信としてご質問をお送りいただいても回答できません。<br/>\n                            ご了承ください。<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:info@maritime-cbd.com\" style=\"text-decoration:none;\">info@maritime-cbd.com</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/en/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', 'mailhistory'),
(16, 91, NULL, '2020-08-06 10:27:59', '[MARITIME] Thank you for your order', 'Frederic Vervaecke \nThank you very much for your order.Please check to make sure that there is no mistake in the following order.\n\n************************************************\n　Billed amount\n************************************************\n\nOrder date：Aug 5, 2020 8:48:04 PM\nOrder number：91\nPayment total：¥4,742\nPayment method：PayPal payment\nUsed points：10 pt\nContact Us：test\n\n\n************************************************\n　Detailed information on ordered items\n************************************************\n\nProduct code：cbd-100\nProduct name：MARITIME CBD oil 10ml (1%)  Regular purchase  \nUnit price：¥4,752\nQuantity：1\n\n\n-------------------------------------------------\nSub Total：¥4,752\n\nHandling fee：¥0\nShipping fee：¥0\nDiscount：(¥10)\n============================================\nTotal：¥4,742\n\n************************************************\n　Customer\'s information\n************************************************\nName：Frederic Vervaecke Name (Kana)：フレデリック ヴェルヴァーク ZipCode：ZipCode 1570077\nAddress：Tokyo, 世田谷区鎌田123建物\nTelephone Number：0123456789\nE-mail Address：djfre2000@hotmail.com\n\n************************************************\n　Shipping Information\n************************************************\n\n◎Recipient\nName：Frederic Vervaecke Name (Kana)：フレデリック ヴェルヴァーク ZipCode：ZipCode 1570077\nAddress：Tokyo, 世田谷区鎌田123建物\nTelephone Number：0123456789\n\nShipping method：Sagawa Express\nDelivery date：Aug 27, 2020\nDelivery time：Morning (8h-12h)\n\nProduct code：cbd-100\nProduct name：MARITIME CBD oil 10ml (1%)  Regular purchase  \nQuantity：1\n\n\n\n============================================\n\nThis message is dedicated to sending customers notifications;<br />\nso you will not get a reply by replying to this message.<br />\n\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/en/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"title\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">Thank you very much for your order.</div>\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke <br/>\n                            <br/>\n                            Please check to make sure that there is no mistake in the following order.<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　Billed amount<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            Order date：Aug 5, 2020 8:48:04 PM<br/>\n                            Order number：91<br/>\n                            Payment total：¥4,742<br/>\n                            Payment method：PayPal payment<br/>\n                                                        Used points：10 pt<br/>\n                                                        Contact Us：test<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　Detailed information on ordered items<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                            Product code：cbd-100<br/>\n                                Product name：MARITIME CBD oil 10ml (1%)  Regular purchase  <br/>\n                                Unit price：¥4,752<br/>\n                                Quantity：1<br/>\n                                <br/>\n                                                        <hr style=\"border-top: 2px dashed #8c8b8b;\">\n                            Sub Total：¥4,752<br/>\n                            <br/>\n                            Handling fee：¥0<br/>\n                            Shipping fee：¥0<br/>\n                                                            Discount：(¥10)<br/>\n                                                        <hr style=\"border-top: 1px dotted #8c8b8b;\">\n                            Total：¥4,742<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            Customer&#039;s information<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            Name：Frederic Vervaecke <br/>\n                            Name (Kana)：フレデリック ヴェルヴァーク <br/>\n                                                        ZipCode：ZipCode 1570077<br/>\n                            Address：Tokyo, 世田谷区鎌田123建物<br/>\n                            Telephone Number：0123456789<br/>\n                            E-mail Address：djfre2000@hotmail.com<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　Shipping Information<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n\n                                                            ◎Recipient<br/>\n                                <br/>\n                                Name：Frederic Vervaecke <br/>\n                                Name (Kana)：フレデリック ヴェルヴァーク <br/>\n                                                                ZipCode：ZipCode 1570077<br/>\n                                Address：Tokyo, 世田谷区鎌田123建物<br/>\n                                Telephone Number：0123456789<br/>\n                                <br/>\n                                Shipping method：Sagawa Express<br/>\n                                Delivery date：Aug 27, 2020<br/>\n                                Delivery time：Morning (8h-12h)<br/>\n                                <br/>\n                                                                    Product code：cbd-100<br/>\n                                    Product name：MARITIME CBD oil 10ml (1%)  Regular purchase  <br/>\n                                    Quantity：1<br/>\n                                    <br/>\n                                                                                                                    <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            This message is dedicated to sending customers notifications;<br />\nso you will not get a reply by replying to this message.<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        This email is sent by MARITIME<br/>\n                        If you think you received this email by mistake, please contact <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> .<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/en/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', 'mailhistory'),
(17, 106, NULL, '2020-08-30 07:23:44', '[MARITIME] ご注文ありがとうございます', 'Frederic Vervaecke 様\nこの度はご注文いただき誠にありがとうございます。下記ご注文内容にお間違いがないかご確認下さい。\n\n************************************************\n　ご請求金額\n************************************************\n\nご注文日時：2020/08/30 16:21:49\nご注文番号：106\nお支払い合計：￥9,504\nお支払い方法：銀行振込\nご利用ポイント：0 pt\nお問い合わせ内容：テスト\n\n************************************************\n　銀行口座情報\n************************************************\n\n銀行名：神戸信用金庫\n支店名：中央支店\n口座番号：0500384\n預金種別：普通\n口座名義：株式会社メディファイン\n\n************************************************\n　ご注文商品明細\n************************************************\n\n商品コード：cbd-300-10\n商品名：MARITIME CBDオイル 10ml (3%)  通常購入  \n単価：￥9,504\n数量：1\n\n\n-------------------------------------------------\n小　計：￥9,504\n\n手数料：￥0\n送　料：￥0\n============================================\n合　計：￥9,504\n\n************************************************\n　ご注文者情報\n************************************************\nお名前：Frederic Vervaecke 様お名前(カナ)：フレデリック ヴェルヴァーク 様郵便番号：〒1570077\n住所：東京都世田谷区鎌田123建物\n電話番号：0123456789\nメールアドレス：djfre2000@hotmail.com\n\n************************************************\n　配送情報\n************************************************\n\n◎お届け先\nお名前：Frederic Vervaecke 様お名前(カナ)：フレデリック ヴェルヴァーク 様郵便番号：〒1570077\n住所：東京都世田谷区鎌田123建物\n電話番号：0123456789\n\n配送方法：佐川急便\nお届け日：指定なし\nお届け時間：指定なし\n\n商品コード：cbd-300-10\n商品名：MARITIME CBDオイル 10ml (3%)  通常購入  \n数量：1\n\n\n\n============================================\n\nこのメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"title\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">この度はご注文いただき誠にありがとうございます。</div>\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            下記ご注文内容にお間違いがないかご確認下さい。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご請求金額<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご注文日時：2020/08/30 16:21:49<br/>\n                            ご注文番号：106<br/>\n                            お支払い合計：￥9,504<br/>\n                            お支払い方法：銀行振込<br/>\n                                                        ご利用ポイント：0 pt<br/>\n                                                        お問い合わせ内容：テスト<br/>\n                            <br/>\n                                                                                    <hr style=\"border-top: 3px double #8c8b8b;\">\n                            銀行口座情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            <br/>\n                            銀行名：神戸信用金庫<br/>　\n                            支店名：中央支店<br/>\n                            口座番号：0500384<br/>\n                            預金種別：普通<br/>\n                            口座名義：株式会社メディファイン<br/>\n                            <br/>\n                                                        <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご注文商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                            商品コード：cbd-300-10<br/>\n                                商品名：MARITIME CBDオイル 10ml (3%)  通常購入  <br/>\n                                単価：￥9,504<br/>\n                                数量：1<br/>\n                                <br/>\n                                                        <hr style=\"border-top: 2px dashed #8c8b8b;\">\n                            小　計：￥9,504<br/>\n                            <br/>\n                            手数料：￥0<br/>\n                            送　料：￥0<br/>\n                                                        <hr style=\"border-top: 1px dotted #8c8b8b;\">\n                            合　計：￥9,504<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご注文者情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            お名前：Frederic Vervaecke 様<br/>\n                            お名前(カナ)：フレデリック ヴェルヴァーク 様<br/>\n                                                        郵便番号：〒1570077<br/>\n                            住所：東京都世田谷区鎌田123建物<br/>\n                            電話番号：0123456789<br/>\n                            メールアドレス：djfre2000@hotmail.com<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　配送情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n\n                                                            ◎お届け先<br/>\n                                <br/>\n                                お名前：Frederic Vervaecke 様<br/>\n                                お名前(カナ)：フレデリック ヴェルヴァーク 様<br/>\n                                                                郵便番号：〒1570077<br/>\n                                住所：東京都世田谷区鎌田123建物<br/>\n                                電話番号：0123456789<br/>\n                                <br/>\n                                配送方法：佐川急便<br/>\n                                お届け日：指定なし<br/>\n                                お届け時間：指定なし<br/>\n                                <br/>\n                                                                    商品コード：cbd-300-10<br/>\n                                    商品名：MARITIME CBDオイル 10ml (3%)  通常購入  <br/>\n                                    数量：1<br/>\n                                    <br/>\n                                                                                                                    <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', 'mailhistory'),
(18, 106, 1, '2020-08-30 07:26:14', '[MARITIME] ご注文ありがとうございます', 'Frederic Vervaecke 様\r\nこの度はご注文いただき誠にありがとうございます。下記ご注文内容にお間違いがないかご確認下さい。\r\n\r\n************************************************\r\n　ご請求金額\r\n************************************************\r\n\r\nご注文日時：2020/08/30 16:21:49\r\nご注文番号：106\r\nお支払い合計：￥9,504\r\nお支払い方法：銀行振込\r\nご利用ポイント：0 pt\r\nお問い合わせ内容：テスト\r\n\r\n************************************************\r\n　銀行口座情報\r\n************************************************\r\n\r\n銀行名：神戸信用金庫\r\n支店名：中央支店\r\n口座番号：0500384\r\n預金種別：普通\r\n口座名義：株式会社メディファイン\r\n\r\n************************************************\r\n　ご注文商品明細\r\n************************************************\r\n\r\n商品コード：cbd-300-10\r\n商品名：MARITIME CBDオイル 10ml (3%)  通常購入  \r\n単価：￥9,504\r\n数量：1\r\n\r\n\r\n-------------------------------------------------\r\n小　計：￥9,504\r\n\r\n手数料：￥0\r\n送　料：￥0\r\n============================================\r\n合　計：￥9,504\r\n\r\n************************************************\r\n　ご注文者情報\r\n************************************************\r\nお名前：Frederic Vervaecke 様お名前(カナ)：フレデリック ヴェルヴァーク 様郵便番号：〒1570077\r\n住所：東京都世田谷区鎌田123建物\r\n電話番号：0123456789\r\nメールアドレス：djfre2000@hotmail.com\r\n\r\n************************************************\r\n　配送情報\r\n************************************************\r\n\r\n◎お届け先\r\nお名前：Frederic Vervaecke 様お名前(カナ)：フレデリック ヴェルヴァーク 様郵便番号：〒1570077\r\n住所：東京都世田谷区鎌田123建物\r\n電話番号：0123456789\r\n\r\n配送方法：佐川急便\r\nお届け日：指定なし\r\nお届け時間：指定なし\r\n\r\n商品コード：cbd-300-10\r\n商品名：MARITIME CBDオイル 10ml (3%)  通常購入  \r\n数量：1\r\n\r\n\r\n\r\n============================================\r\n\r\nこのメッセージはお客様へのお知らせ専用ですので、<br />\r\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\r\nご了承ください。<br />', NULL, 'mailhistory'),
(19, 107, NULL, '2020-08-30 07:34:56', '[MARITIME] Thank you for your order', 'Frederic Vervaecke \nThank you very much for your order.Please check to make sure that there is no mistake in the following order.\n\n************************************************\n　Billed amount\n************************************************\n\nOrder date：Aug 30, 2020 4:34:28 PM\nOrder number：107\nPayment total：¥4,752\nPayment method：Bank transfer\nUsed points：0 pt\nDetails of Inquiry：\n\n************************************************\n　Bank account information\n************************************************\n\nBank name：神戸信用金庫\nBranch name：中央支店\nAccount number：0500384\nDeposit type：普通\nAccount holder：株式会社メディファイン\n\n************************************************\n　Detailed information on ordered items\n************************************************\n\nProduct code：cbd-100\nProduct name：MARITIME CBD oil 10ml (1%)  Regular purchase  \nUnit price：¥4,752\nQuantity：1\n\n\n-------------------------------------------------\nSub Total：¥4,752\n\nHandling fee：¥0\nShipping fee：¥0\n============================================\nTotal：¥4,752\n\n************************************************\n　Customer\'s information\n************************************************\nName：Frederic Vervaecke Name (Kana)：フレデリック ヴェルヴァーク Zip Code：Zip Code 1570077\nAddress：Tokyo, 世田谷区鎌田123建物\nTelephone Number：0123456789\nE-mail Address：djfre2000@hotmail.com\n\n************************************************\n　Shipping Information\n************************************************\n\n◎Recipient\nName：Frederic Vervaecke Name (Kana)：フレデリック ヴェルヴァーク Zip Code：Zip Code 1570077\nAddress：Tokyo, 世田谷区鎌田123建物\nTelephone Number：0123456789\n\nShipping method：Sagawa Express\nDelivery date：Aug 31, 2020\nDelivery time：12h-14h\n\nProduct code：cbd-100\nProduct name：MARITIME CBD oil 10ml (1%)  Regular purchase  \nQuantity：1\n\n\n\n============================================\n\nThis email is dedicated to sending customers notifications;<br />\nso you will not get a reply by replying to this message.<br />\n\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/en/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"title\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">Thank you very much for your order.</div>\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke <br/>\n                            <br/>\n                            Please check to make sure that there is no mistake in the following order.<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　Billed amount<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            Order date：Aug 30, 2020 4:34:28 PM<br/>\n                            Order number：107<br/>\n                            Payment total：¥4,752<br/>\n                            Payment method：Bank transfer<br/>\n                                                        Used points：0 pt<br/>\n                                                        Details of Inquiry：<br/>\n                            <br/>\n                                                                                    <hr style=\"border-top: 3px double #8c8b8b;\">\n                            Bank account information<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            <br/>\n                            Bank name：神戸信用金庫<br/>\n                            Branch name：中央支店<br/>\n                            Account number：0500384<br/>\n                            Deposit type：普通<br/>\n                            Account holder：株式会社メディファイン<br/>\n                            <br/>\n                                                        <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　Detailed information on ordered items<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                            Product code：cbd-100<br/>\n                                Product name：MARITIME CBD oil 10ml (1%)  Regular purchase  <br/>\n                                Unit price：¥4,752<br/>\n                                Quantity：1<br/>\n                                <br/>\n                                                        <hr style=\"border-top: 2px dashed #8c8b8b;\">\n                            Sub Total：¥4,752<br/>\n                            <br/>\n                            Handling fee：¥0<br/>\n                            Shipping fee：¥0<br/>\n                                                        <hr style=\"border-top: 1px dotted #8c8b8b;\">\n                            Total：¥4,752<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            Customer&#039;s information<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            Name：Frederic Vervaecke <br/>\n                            Name (Kana)：フレデリック ヴェルヴァーク <br/>\n                                                        Zip Code：Zip Code 1570077<br/>\n                            Address：Tokyo, 世田谷区鎌田123建物<br/>\n                            Telephone Number：0123456789<br/>\n                            E-mail Address：djfre2000@hotmail.com<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　Shipping Information<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n\n                                                            ◎Recipient<br/>\n                                <br/>\n                                Name：Frederic Vervaecke <br/>\n                                Name (Kana)：フレデリック ヴェルヴァーク <br/>\n                                                                Zip Code：Zip Code 1570077<br/>\n                                Address：Tokyo, 世田谷区鎌田123建物<br/>\n                                Telephone Number：0123456789<br/>\n                                <br/>\n                                Shipping method：Sagawa Express<br/>\n                                Delivery date：Aug 31, 2020<br/>\n                                Delivery time：12h-14h<br/>\n                                <br/>\n                                                                    Product code：cbd-100<br/>\n                                    Product name：MARITIME CBD oil 10ml (1%)  Regular purchase  <br/>\n                                    Quantity：1<br/>\n                                    <br/>\n                                                                                                                    <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            This email is dedicated to sending customers notifications;<br />\nso you will not get a reply by replying to this message.<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        This email is sent by MARITIME<br/>\n                        If you think you received this email by mistake, please contact <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> .<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/en/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', 'mailhistory'),
(20, 111, 1, '2020-09-02 05:12:59', '[MARITIME] カートの中の商品があります', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            ご確認をお願いいたします。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 10ml (1%)  通常購入  <br/>\n                            単価：￥4,752<br/>\n                            数量：2<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            ご確認をお願いいたします。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 10ml (1%)  通常購入  <br/>\n                            単価：￥4,752<br/>\n                            数量：2<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', 'mailhistory');
INSERT INTO `dtb_mail_history` (`id`, `order_id`, `creator_id`, `send_date`, `mail_subject`, `mail_body`, `mail_html_body`, `discriminator_type`) VALUES
(21, 113, NULL, '2020-09-03 07:48:28', '[MARITIME] Thank you for your order', 'Frederic Vervaecke \nThank you very much for your order.Please check to make sure that there is no mistake in the following order.\n\n************************************************\n　Billed amount\n************************************************\n\nOrder date：Sep 3, 2020 4:46:56 PM\nOrder number：113\nPayment total：¥4,752\nPayment method：Bank transfer\nUsed points：0 pt\nDetails of Inquiry：\n\n************************************************\n　Bank account information\n************************************************\n\nBank name：神戸信用金庫\nBranch name：中央支店\nAccount number：0500384\nDeposit type：普通\nAccount holder：株式会社メディファイン\n\n************************************************\n　Detailed information on ordered items\n************************************************\n\nProduct code：cbd-100\nProduct name：MARITIME CBD oil 10ml (1%)  Regular purchase  \nUnit price：¥4,752\nQuantity：1\n\n\n-------------------------------------------------\nSub Total：¥4,752\n\nHandling fee：¥0\nShipping fee：¥0\n============================================\nTotal：¥4,752\n\n************************************************\n　Customer\'s information\n************************************************\nName：Frederic Vervaecke Name (Kana)：フレデリック ヴェルヴァーク Zip Code：Zip Code 1570077\nAddress：Tokyo, 世田谷区鎌田123建物\nTelephone Number：0123456789\nE-mail Address：djfre2000@hotmail.com\n\n************************************************\n　Shipping Information\n************************************************\n\n◎Recipient\nName：Frederic Vervaecke Name (Kana)：フレデリック ヴェルヴァーク Zip Code：Zip Code 1570077\nAddress：Tokyo, 世田谷区鎌田123建物\nTelephone Number：0123456789\n\nShipping method：Sagawa Express\nDelivery date：Unspecified\nDelivery time：Unspecified\n\nProduct code：cbd-100\nProduct name：MARITIME CBD oil 10ml (1%)  Regular purchase  \nQuantity：1\n\n\n\n============================================\n\nThis email is dedicated to sending customers notifications;<br />\nso you will not get a reply by replying to this message.<br />\n\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/en/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"title\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">Thank you very much for your order.</div>\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke <br/>\n                            <br/>\n                            Please check to make sure that there is no mistake in the following order.<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　Billed amount<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            Order date：Sep 3, 2020 4:46:56 PM<br/>\n                            Order number：113<br/>\n                            Payment total：¥4,752<br/>\n                            Payment method：Bank transfer<br/>\n                                                        Used points：0 pt<br/>\n                                                        Details of Inquiry：<br/>\n                            <br/>\n                                                                                    <hr style=\"border-top: 3px double #8c8b8b;\">\n                            Bank account information<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            <br/>\n                            Bank name：神戸信用金庫<br/>\n                            Branch name：中央支店<br/>\n                            Account number：0500384<br/>\n                            Deposit type：普通<br/>\n                            Account holder：株式会社メディファイン<br/>\n                            <br/>\n                                                        <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　Detailed information on ordered items<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                            Product code：cbd-100<br/>\n                                Product name：MARITIME CBD oil 10ml (1%)  Regular purchase  <br/>\n                                Unit price：¥4,752<br/>\n                                Quantity：1<br/>\n                                <br/>\n                                                        <hr style=\"border-top: 2px dashed #8c8b8b;\">\n                            Sub Total：¥4,752<br/>\n                            <br/>\n                            Handling fee：¥0<br/>\n                            Shipping fee：¥0<br/>\n                                                        <hr style=\"border-top: 1px dotted #8c8b8b;\">\n                            Total：¥4,752<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            Customer&#039;s information<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            Name：Frederic Vervaecke <br/>\n                            Name (Kana)：フレデリック ヴェルヴァーク <br/>\n                                                        Zip Code：Zip Code 1570077<br/>\n                            Address：Tokyo, 世田谷区鎌田123建物<br/>\n                            Telephone Number：0123456789<br/>\n                            E-mail Address：djfre2000@hotmail.com<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　Shipping Information<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n\n                                                            ◎Recipient<br/>\n                                <br/>\n                                Name：Frederic Vervaecke <br/>\n                                Name (Kana)：フレデリック ヴェルヴァーク <br/>\n                                                                Zip Code：Zip Code 1570077<br/>\n                                Address：Tokyo, 世田谷区鎌田123建物<br/>\n                                Telephone Number：0123456789<br/>\n                                <br/>\n                                Shipping method：Sagawa Express<br/>\n                                Delivery date：Unspecified<br/>\n                                Delivery time：Unspecified<br/>\n                                <br/>\n                                                                    Product code：cbd-100<br/>\n                                    Product name：MARITIME CBD oil 10ml (1%)  Regular purchase  <br/>\n                                    Quantity：1<br/>\n                                    <br/>\n                                                                                                                    <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            This email is dedicated to sending customers notifications;<br />\nso you will not get a reply by replying to this message.<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        This email is sent by MARITIME<br/>\n                        If you think you received this email by mistake, please contact <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> .<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/en/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', 'mailhistory'),
(22, 109, 1, '2020-09-04 10:02:54', '[MARITIME] カートの中の商品があります', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Test Test 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            ご確認をお願いいたします。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBD ウォーター 500ml  通常購入  <br/>\n                            単価：￥1,276<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Test Test 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            ご確認をお願いいたします。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBD ウォーター 500ml  通常購入  <br/>\n                            単価：￥1,276<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', 'mailhistory'),
(23, 114, NULL, '2020-09-04 10:32:25', '[MARITIME] カートの中の商品があります', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://localhost/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            ご確認をお願いいたします。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 30ml (10%)  通常購入  <br/>\n                            単価：￥47,025<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://localhost/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://localhost/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            ご確認をお願いいたします。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 30ml (10%)  通常購入  <br/>\n                            単価：￥47,025<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://localhost/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', 'mailhistory'),
(24, 101, NULL, '2020-09-04 10:32:25', '[MARITIME] カートの中の商品があります', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://localhost/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Test Test 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            ご確認をお願いいたします。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 10ml (1%)  通常購入  <br/>\n                            単価：￥5,280<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://localhost/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://localhost/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Test Test 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            ご確認をお願いいたします。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 10ml (1%)  通常購入  <br/>\n                            単価：￥5,280<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://localhost/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', 'mailhistory'),
(25, 100, NULL, '2020-09-04 10:32:25', '[MARITIME] カートの中の商品があります', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://localhost/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Test Test 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            ご確認をお願いいたします。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 10ml (1%)  通常購入  <br/>\n                            単価：￥5,280<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://localhost/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://localhost/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Test Test 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            ご確認をお願いいたします。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 10ml (1%)  通常購入  <br/>\n                            単価：￥5,280<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://localhost/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', 'mailhistory');
INSERT INTO `dtb_mail_history` (`id`, `order_id`, `creator_id`, `send_date`, `mail_subject`, `mail_body`, `mail_html_body`, `discriminator_type`) VALUES
(26, NULL, 1, '2020-09-04 10:33:02', '[MARITIME] カートの中の商品があります', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Test2 Test 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            ご確認をお願いいたします。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                                                                                        商品名：MARITIME CBDオイル 10ml (1%)\n                                                                    販売種別：通常購入\n                                                                <br/>\n                            単価：￥5,280<br/>\n                            数量：1<br/>\n                                <br/>\n                                \n                                                        <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Test2 Test 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            ご確認をお願いいたします。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                                                                                        商品名：MARITIME CBDオイル 10ml (1%)\n                                                                    販売種別：通常購入\n                                                                <br/>\n                            単価：￥5,280<br/>\n                            数量：1<br/>\n                                <br/>\n                                \n                                                        <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', 'mailhistory'),
(27, 114, NULL, '2020-09-04 10:43:35', '[MARITIME] カートの中の商品があります', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://localhost/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            ご確認をお願いいたします。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 30ml (10%)  通常購入  <br/>\n                            単価：￥47,025<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://localhost/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://localhost/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            ご確認をお願いいたします。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 30ml (10%)  通常購入  <br/>\n                            単価：￥47,025<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://localhost/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', 'mailhistory'),
(28, 109, NULL, '2020-09-04 10:43:35', '[MARITIME] カートの中の商品があります', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://localhost/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Test Test 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            ご確認をお願いいたします。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBD ウォーター 500ml  通常購入  <br/>\n                            単価：￥1,276<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://localhost/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://localhost/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Test Test 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            ご確認をお願いいたします。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBD ウォーター 500ml  通常購入  <br/>\n                            単価：￥1,276<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://localhost/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', 'mailhistory'),
(29, NULL, NULL, '2020-09-04 10:43:35', '[MARITIME] カートの中の商品があります', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://localhost/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Test2 Test 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            ご確認をお願いいたします。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                                                                                        商品名：MARITIME CBDオイル 10ml (1%)\n                                                                    販売種別：通常購入\n                                                                <br/>\n                            単価：￥5,280<br/>\n                            数量：1<br/>\n                                <br/>\n                                \n                                                        <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://localhost/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://localhost/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Test2 Test 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            ご確認をお願いいたします。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                                                                                        商品名：MARITIME CBDオイル 10ml (1%)\n                                                                    販売種別：通常購入\n                                                                <br/>\n                            単価：￥5,280<br/>\n                            数量：1<br/>\n                                <br/>\n                                \n                                                        <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://localhost/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', 'mailhistory'),
(30, 101, NULL, '2020-09-04 10:43:35', '[MARITIME] カートの中の商品があります', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://localhost/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Test Test 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            ご確認をお願いいたします。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 10ml (1%)  通常購入  <br/>\n                            単価：￥5,280<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://localhost/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://localhost/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Test Test 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            ご確認をお願いいたします。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 10ml (1%)  通常購入  <br/>\n                            単価：￥5,280<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://localhost/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', 'mailhistory');
INSERT INTO `dtb_mail_history` (`id`, `order_id`, `creator_id`, `send_date`, `mail_subject`, `mail_body`, `mail_html_body`, `discriminator_type`) VALUES
(31, 100, NULL, '2020-09-04 10:43:35', '[MARITIME] カートの中の商品があります', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://localhost/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Test Test 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            ご確認をお願いいたします。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 10ml (1%)  通常購入  <br/>\n                            単価：￥5,280<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://localhost/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://localhost/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Test Test 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            ご確認をお願いいたします。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 10ml (1%)  通常購入  <br/>\n                            単価：￥5,280<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://localhost/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', 'mailhistory'),
(32, 114, 1, '2020-09-07 06:56:12', '[MARITIME] カートの中の商品があります', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            ご確認をお願いいたします。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 30ml (10%)  通常購入  <br/>\n                            単価：￥47,025<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            ご確認をお願いいたします。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 30ml (10%)  通常購入  <br/>\n                            単価：￥47,025<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', 'mailhistory'),
(33, 109, 1, '2020-09-07 10:09:04', '[MARITIME] カートの中の商品があります', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Test Test 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            下記URLにご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime.shourai.io/cart\" target=\"_blank\">https://maritime.shourai.io/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBD ウォーター 500ml  通常購入  <br/>\n                            単価：￥1,276<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Test Test 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            下記URLにご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime.shourai.io/cart\" target=\"_blank\">https://maritime.shourai.io/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBD ウォーター 500ml  通常購入  <br/>\n                            単価：￥1,276<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', 'mailhistory'),
(34, NULL, NULL, '2020-09-07 10:21:24', '[MARITIME] カートの中の商品があります', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            下記URLにご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime.shourai.io/cart\" target=\"_blank\">https://maritime.shourai.io/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                                                                                        商品名：MARITIME CBDオイル 30ml (10%)\n                                                                    販売種別：通常購入\n                                                                <br/>\n                            単価：￥47,025<br/>\n                            数量：1<br/>\n                                <br/>\n                                \n                                                        <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            下記URLにご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime.shourai.io/cart\" target=\"_blank\">https://maritime.shourai.io/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                                                                                        商品名：MARITIME CBDオイル 30ml (10%)\n                                                                    販売種別：通常購入\n                                                                <br/>\n                            単価：￥47,025<br/>\n                            数量：1<br/>\n                                <br/>\n                                \n                                                        <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', 'mailhistory'),
(35, 109, NULL, '2020-09-07 10:21:24', '[MARITIME] カートの中の商品があります', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Test Test 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            下記URLにご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime.shourai.io/cart\" target=\"_blank\">https://maritime.shourai.io/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBD ウォーター 500ml  通常購入  <br/>\n                            単価：￥1,276<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Test Test 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            下記URLにご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime.shourai.io/cart\" target=\"_blank\">https://maritime.shourai.io/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBD ウォーター 500ml  通常購入  <br/>\n                            単価：￥1,276<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', 'mailhistory');
INSERT INTO `dtb_mail_history` (`id`, `order_id`, `creator_id`, `send_date`, `mail_subject`, `mail_body`, `mail_html_body`, `discriminator_type`) VALUES
(36, NULL, NULL, '2020-09-07 10:21:24', '[MARITIME] カートの中の商品があります', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Test2 Test 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            下記URLにご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime.shourai.io/cart\" target=\"_blank\">https://maritime.shourai.io/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                                                                                        商品名：MARITIME CBDオイル 10ml (1%)\n                                                                    販売種別：通常購入\n                                                                <br/>\n                            単価：￥5,280<br/>\n                            数量：1<br/>\n                                <br/>\n                                \n                                                        <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Test2 Test 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            下記URLにご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime.shourai.io/cart\" target=\"_blank\">https://maritime.shourai.io/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                                                                                        商品名：MARITIME CBDオイル 10ml (1%)\n                                                                    販売種別：通常購入\n                                                                <br/>\n                            単価：￥5,280<br/>\n                            数量：1<br/>\n                                <br/>\n                                \n                                                        <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', 'mailhistory'),
(37, 101, NULL, '2020-09-07 10:21:24', '[MARITIME] カートの中の商品があります', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Test Test 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            下記URLにご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime.shourai.io/cart\" target=\"_blank\">https://maritime.shourai.io/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 10ml (1%)  通常購入  <br/>\n                            単価：￥5,280<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Test Test 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            下記URLにご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime.shourai.io/cart\" target=\"_blank\">https://maritime.shourai.io/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 10ml (1%)  通常購入  <br/>\n                            単価：￥5,280<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', 'mailhistory'),
(38, 100, NULL, '2020-09-07 10:21:24', '[MARITIME] カートの中の商品があります', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Test Test 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            下記URLにご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime.shourai.io/cart\" target=\"_blank\">https://maritime.shourai.io/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 10ml (1%)  通常購入  <br/>\n                            単価：￥5,280<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Test Test 様<br/>\n                            <br/>\n                            カートに商品が入っています。<br/>\n                            下記URLにご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime.shourai.io/cart\" target=\"_blank\">https://maritime.shourai.io/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                        商品名：MARITIME CBDオイル 10ml (1%)  通常購入  <br/>\n                            単価：￥5,280<br/>\n                            数量：1<br/>\n                                    <br/>\n                                                                                       <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', 'mailhistory'),
(39, NULL, 1, '2020-09-09 08:18:27', '[MARITIME] カートの中の商品があります', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            マリタイムCBDショップをご利用いただき誠にありがとうございます。お買い忘れはありませんか？ショッピングカートに入ったままの商品があるようです。<br/>\n                            <br/>\n                            下記URLからご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime.shourai.io/cart\" target=\"_blank\">https://maritime.shourai.io/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                                                                                        商品名：MARITIME CBDオイル 30ml (10%)\n                                                                    販売種別：通常購入\n                                                                <br/>\n                            単価：￥47,025<br/>\n                            数量：1<br/>\n                                <br/>\n                                \n                                                        <hr style=\"border-top: 2px double #8c8b8b;\">\n                            当店のCBD商品はメイド・イン・ジャパンの品質、オリーブオイルを使った天然由来CBDオイルです。「安心して毎日飲み続けられるCBDオイルをつくりたい。」そんな想いから生まれました。<br/>\n                            <br/>\n                            ショッピングカートの内容は2日間保存しています。今ならCBDオイルをご購入のお客さまにもれなく、マリタイムCBDウォーターをプレゼントのキャンペーン中です。この機会にぜひお買い求めください。<br/>\n                            <br/>\n                            何かお困りのことはありませんか？お問い合わせは <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> まで。CBDオイルのよくある質問はこちらからご覧ください。（<a href=\"https://maritime-cbd.com/user_data/faq\" style=\"color:#aaaaaa;text-decoration:none;\">CBDオイルのよくある質問</a>）<br/>\n                            <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            マリタイムCBDショップをご利用いただき誠にありがとうございます。お買い忘れはありませんか？ショッピングカートに入ったままの商品があるようです。<br/>\n                            <br/>\n                            下記URLからご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime.shourai.io/cart\" target=\"_blank\">https://maritime.shourai.io/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                                                                                        商品名：MARITIME CBDオイル 30ml (10%)\n                                                                    販売種別：通常購入\n                                                                <br/>\n                            単価：￥47,025<br/>\n                            数量：1<br/>\n                                <br/>\n                                \n                                                        <hr style=\"border-top: 2px double #8c8b8b;\">\n                            当店のCBD商品はメイド・イン・ジャパンの品質、オリーブオイルを使った天然由来CBDオイルです。「安心して毎日飲み続けられるCBDオイルをつくりたい。」そんな想いから生まれました。<br/>\n                            <br/>\n                            ショッピングカートの内容は2日間保存しています。今ならCBDオイルをご購入のお客さまにもれなく、マリタイムCBDウォーターをプレゼントのキャンペーン中です。この機会にぜひお買い求めください。<br/>\n                            <br/>\n                            何かお困りのことはありませんか？お問い合わせは <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> まで。CBDオイルのよくある質問はこちらからご覧ください。（<a href=\"https://maritime-cbd.com/user_data/faq\" style=\"color:#aaaaaa;text-decoration:none;\">CBDオイルのよくある質問</a>）<br/>\n                            <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', 'mailhistory'),
(40, NULL, 1, '2020-09-09 08:21:22', '[MARITIME] カートの中の商品があります', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            マリタイムCBDショップをご利用いただき誠にありがとうございます。お買い忘れはありませんか？ショッピングカートに入ったままの商品があるようです。<br/>\n                            <br/>\n                            下記URLからご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime.shourai.io/cart\" target=\"_blank\">https://maritime.shourai.io/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                                                                                        商品名：MARITIME CBDオイル 30ml (10%)\n                                                                    販売種別：通常購入\n                                                                <br/>\n                            単価：￥47,025<br/>\n                            数量：1<br/>\n                                <br/>\n                                \n                                                        <hr style=\"border-top: 2px double #8c8b8b;\">\n                            当店のCBD商品はメイド・イン・ジャパンの品質、オリーブオイルを使った天然由来CBDオイルです。「安心して毎日飲み続けられるCBDオイルをつくりたい。」そんな想いから生まれました。<br/>\n                            <br/>\n                            ショッピングカートの内容は2日間保存しています。今ならCBDオイルをご購入のお客さまにもれなく、マリタイムCBDウォーターをプレゼントのキャンペーン中です。この機会にぜひお買い求めください。<br/>\n                            <br/>\n                            何かお困りのことはありませんか？お問い合わせは <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> まで。CBDオイルのよくある質問はこちらからご覧ください。（<a href=\"https://maritime-cbd.com/user_data/faq\" style=\"color:#aaaaaa;text-decoration:none;\">CBDオイルのよくある質問</a>）<br/>\n                            <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            マリタイムCBDショップをご利用いただき誠にありがとうございます。お買い忘れはありませんか？ショッピングカートに入ったままの商品があるようです。<br/>\n                            <br/>\n                            下記URLからご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime.shourai.io/cart\" target=\"_blank\">https://maritime.shourai.io/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                                                                                        商品名：MARITIME CBDオイル 30ml (10%)\n                                                                    販売種別：通常購入\n                                                                <br/>\n                            単価：￥47,025<br/>\n                            数量：1<br/>\n                                <br/>\n                                \n                                                        <hr style=\"border-top: 2px double #8c8b8b;\">\n                            当店のCBD商品はメイド・イン・ジャパンの品質、オリーブオイルを使った天然由来CBDオイルです。「安心して毎日飲み続けられるCBDオイルをつくりたい。」そんな想いから生まれました。<br/>\n                            <br/>\n                            ショッピングカートの内容は2日間保存しています。今ならCBDオイルをご購入のお客さまにもれなく、マリタイムCBDウォーターをプレゼントのキャンペーン中です。この機会にぜひお買い求めください。<br/>\n                            <br/>\n                            何かお困りのことはありませんか？お問い合わせは <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> まで。CBDオイルのよくある質問はこちらからご覧ください。（<a href=\"https://maritime-cbd.com/user_data/faq\" style=\"color:#aaaaaa;text-decoration:none;\">CBDオイルのよくある質問</a>）<br/>\n                            <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', 'mailhistory');
INSERT INTO `dtb_mail_history` (`id`, `order_id`, `creator_id`, `send_date`, `mail_subject`, `mail_body`, `mail_html_body`, `discriminator_type`) VALUES
(41, NULL, 1, '2020-09-09 09:26:09', '[MARITIME] カートの中の商品があります', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            マリタイムCBDショップをご利用いただき誠にありがとうございます。お買い忘れはありませんか？ショッピングカートに入ったままの商品があるようです。<br/>\n                            <br/>\n                            下記URLからご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime.shourai.io/cart\" target=\"_blank\">https://maritime.shourai.io/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                                                                                        商品名：MARITIME CBDオイル 30ml (10%)\n                                                                    販売種別：通常購入\n                                                                <br/>\n                            単価：￥47,025<br/>\n                            数量：1<br/>\n                                <br/>\n                                \n                                                        <hr style=\"border-top: 2px double #8c8b8b;\">\n                            当店のCBD商品はメイド・イン・ジャパンの品質、オリーブオイルを使った天然由来CBDオイルです。「安心して毎日飲み続けられるCBDオイルをつくりたい。」そんな想いから生まれました。<br/>\n                            <br/>\n                            ショッピングカートの内容は2日間保存しています。今ならCBDオイルをご購入のお客さまにもれなく、マリタイムCBDウォーターをプレゼントのキャンペーン中です。この機会にぜひお買い求めください。<br/>\n                            <br/>\n                            何かお困りのことはありませんか？お問い合わせは <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> まで。CBDオイルのよくある質問はこちらからご覧ください。（<a href=\"https://maritime-cbd.com/user_data/faq\" style=\"color:#aaaaaa;text-decoration:none;\">CBDオイルのよくある質問</a>）<br/>\n                            <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            マリタイムCBDショップをご利用いただき誠にありがとうございます。お買い忘れはありませんか？ショッピングカートに入ったままの商品があるようです。<br/>\n                            <br/>\n                            下記URLからご確認をお願いいたします。<br/>\n                            => <a href=\"https://maritime.shourai.io/cart\" target=\"_blank\">https://maritime.shourai.io/cart</a><br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                                                                                                                        商品名：MARITIME CBDオイル 30ml (10%)\n                                                                    販売種別：通常購入\n                                                                <br/>\n                            単価：￥47,025<br/>\n                            数量：1<br/>\n                                <br/>\n                                \n                                                        <hr style=\"border-top: 2px double #8c8b8b;\">\n                            当店のCBD商品はメイド・イン・ジャパンの品質、オリーブオイルを使った天然由来CBDオイルです。「安心して毎日飲み続けられるCBDオイルをつくりたい。」そんな想いから生まれました。<br/>\n                            <br/>\n                            ショッピングカートの内容は2日間保存しています。今ならCBDオイルをご購入のお客さまにもれなく、マリタイムCBDウォーターをプレゼントのキャンペーン中です。この機会にぜひお買い求めください。<br/>\n                            <br/>\n                            何かお困りのことはありませんか？お問い合わせは <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> まで。CBDオイルのよくある質問はこちらからご覧ください。（<a href=\"https://maritime-cbd.com/user_data/faq\" style=\"color:#aaaaaa;text-decoration:none;\">CBDオイルのよくある質問</a>）<br/>\n                            <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', 'mailhistory'),
(42, 115, NULL, '2020-09-10 00:23:27', '[MARITIME] ご注文ありがとうございます', 'Frederic Vervaecke 様\nこの度はご注文いただき誠にありがとうございます。下記ご注文内容にお間違いがないかご確認下さい。\n\n************************************************\n　ご請求金額\n************************************************\n\nご注文日時：2020/09/10 9:22:18\nご注文番号：115\nお支払い合計：￥41,779\nお支払い方法：銀行振込\nご利用ポイント：543 pt\nお問い合わせ内容：test frederic\n\n************************************************\n　銀行口座情報\n************************************************\n\n銀行名：神戸信用金庫\n支店名：中央支店\n口座番号：0500384\n預金種別：普通\n口座名義：株式会社メディファイン\n\n************************************************\n　ご注文商品明細\n************************************************\n\n商品コード：cbd-3000\n商品名：MARITIME CBDオイル 30ml (10%)  通常購入  \n単価：￥47,025\n数量：1\n\n\n-------------------------------------------------\n小　計：￥47,025\n\n手数料：￥0\n送　料：￥0\n値引き：-￥5,246\n============================================\n合　計：￥41,779\n\n************************************************\n　ご注文者情報\n************************************************\nお名前：Frederic Vervaecke 様お名前(カナ)：フレデリック ヴェルヴァーク 様郵便番号：〒1570077\n住所：東京都世田谷区鎌田123建物\n電話番号：0123456789\nメールアドレス：djfre2000@hotmail.com\n\n************************************************\n　配送情報\n************************************************\n\n◎お届け先\nお名前：Frederic Vervaecke 様お名前(カナ)：フレデリック ヴェルヴァーク 様郵便番号：〒1570077\n住所：東京都世田谷区鎌田123建物\n電話番号：0123456789\n\n配送方法：佐川急便\nお届け日：2020/09/13\nお届け時間：14時～16時\n\n商品コード：cbd-3000\n商品名：MARITIME CBDオイル 30ml (10%)  通常購入  \n数量：1\n\n\n***********************************************\n　クーポン情報                                 \n***********************************************\n\nクーポンコード: MARITIME10 初回10％割引オファー\n\n\n============================================\n\nこのメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"title\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">この度はご注文いただき誠にありがとうございます。</div>\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            下記ご注文内容にお間違いがないかご確認下さい。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご請求金額<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご注文日時：2020/09/10 9:22:18<br/>\n                            ご注文番号：115<br/>\n                            お支払い合計：￥41,779<br/>\n                            お支払い方法：銀行振込<br/>\n                                                        ご利用ポイント：543 pt<br/>\n                                                        お問い合わせ内容：test frederic<br/>\n                            <br/>\n                                                                                    <hr style=\"border-top: 3px double #8c8b8b;\">\n                            銀行口座情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            <br/>\n                            銀行名：神戸信用金庫<br/>\n                            支店名：中央支店<br/>\n                            口座番号：0500384<br/>\n                            預金種別：普通<br/>\n                            口座名義：株式会社メディファイン<br/>\n                            <br/>\n                                                        <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご注文商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                            商品コード：cbd-3000<br/>\n                                商品名：MARITIME CBDオイル 30ml (10%)  通常購入  <br/>\n                                単価：￥47,025<br/>\n                                数量：1<br/>\n                                <br/>\n                                                        <hr style=\"border-top: 2px dashed #8c8b8b;\">\n                            小　計：￥47,025<br/>\n                            <br/>\n                            手数料：￥0<br/>\n                            送　料：￥0<br/>\n                                                            値引き：-￥5,246<br/>\n                                                        <hr style=\"border-top: 1px dotted #8c8b8b;\">\n                            合　計：￥41,779<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご注文者情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            お名前：Frederic Vervaecke 様<br/>\n                            お名前(カナ)：フレデリック ヴェルヴァーク 様<br/>\n                                                        郵便番号：〒1570077<br/>\n                            住所：東京都世田谷区鎌田123建物<br/>\n                            電話番号：0123456789<br/>\n                            メールアドレス：djfre2000@hotmail.com<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　配送情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n\n                                                            ◎お届け先<br/>\n                                <br/>\n                                お名前：Frederic Vervaecke 様<br/>\n                                お名前(カナ)：フレデリック ヴェルヴァーク 様<br/>\n                                                                郵便番号：〒1570077<br/>\n                                住所：東京都世田谷区鎌田123建物<br/>\n                                電話番号：0123456789<br/>\n                                <br/>\n                                配送方法：佐川急便<br/>\n                                お届け日：2020/09/13<br/>\n                                お届け時間：14時～16時<br/>\n                                <br/>\n                                                                    商品コード：cbd-3000<br/>\n                                    商品名：MARITIME CBDオイル 30ml (10%)  通常購入  <br/>\n                                    数量：1<br/>\n                                    <br/>\n                                                                                                                        ***********************************************\n　クーポン情報                                 \n***********************************************\n\nクーポンコード: MARITIME10 初回10％割引オファー\n<br/>\n                                                        <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', 'mailhistory'),
(43, 117, NULL, '2020-09-17 04:04:08', '[MARITIME] ご注文ありがとうございます', 'Frederic Vervaecke 様\nこの度はご注文いただき誠にありがとうございます。下記ご注文内容にお間違いがないかご確認下さい。\n\n************************************************\n　ご請求金額\n************************************************\n\nご注文日時：2020/09/17 12:48:51\nご注文番号：117\nお支払い合計：￥4,265\nお支払い方法：銀行振込\nご利用ポイント：12 pt\nお問い合わせ内容：test\n\n************************************************\n　銀行口座情報\n************************************************\n\n銀行名：神戸信用金庫\n支店名：中央支店\n口座番号：0500384\n預金種別：普通\n口座名義：株式会社メディファイン\n\n************************************************\n　ご注文商品明細\n************************************************\n\n商品コード：cbd-100\n商品名：MARITIME CBDオイル 10ml (1%)  通常購入  \n単価：￥4,752\n数量：1\n\n\n-------------------------------------------------\n小　計：￥4,752\n\n手数料：￥0\n送　料：￥0\n値引き：-￥487\n============================================\n合　計：￥4,265\n\n************************************************\n　ご注文者情報\n************************************************\nお名前：Frederic Vervaecke 様お名前(カナ)：フレデリック ヴェルヴァーク 様郵便番号：〒1570077\n住所：東京都世田谷区鎌田123建物\n電話番号：0123456789\nメールアドレス：djfre2000@hotmail.com\n\n************************************************\n　配送情報\n************************************************\n\n◎お届け先\nお名前：Frederic Vervaecke 様お名前(カナ)：フレデリック ヴェルヴァーク 様郵便番号：〒1570077\n住所：東京都世田谷区鎌田123建物\n電話番号：0123456789\n\n配送方法：佐川急便\nお届け日：2020/09/21\nお届け時間：19時～21時\n\n商品コード：cbd-100\n商品名：MARITIME CBDオイル 10ml (1%)  通常購入  \n数量：1\n\n\n***********************************************\n　クーポン情報                                 \n***********************************************\n\nクーポンコード: MARITIME10 初回10％割引オファー\n\n\n============================================\n\nこのメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"title\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">この度はご注文いただき誠にありがとうございます。</div>\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke 様<br/>\n                            <br/>\n                            下記ご注文内容にお間違いがないかご確認下さい。<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご請求金額<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご注文日時：2020/09/17 12:48:51<br/>\n                            ご注文番号：117<br/>\n                            お支払い合計：￥4,265<br/>\n                            お支払い方法：銀行振込<br/>\n                                                        ご利用ポイント：12 pt<br/>\n                                                        お問い合わせ内容：test<br/>\n                            <br/>\n                                                                                    <hr style=\"border-top: 3px double #8c8b8b;\">\n                            銀行口座情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            <br/>\n                            銀行名：神戸信用金庫<br/>\n                            支店名：中央支店<br/>\n                            口座番号：0500384<br/>\n                            預金種別：普通<br/>\n                            口座名義：株式会社メディファイン<br/>\n                            <br/>\n                                                        <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　ご注文商品明細<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                            商品コード：cbd-100<br/>\n                                商品名：MARITIME CBDオイル 10ml (1%)  通常購入  <br/>\n                                単価：￥4,752<br/>\n                                数量：1<br/>\n                                <br/>\n                                                        <hr style=\"border-top: 2px dashed #8c8b8b;\">\n                            小　計：￥4,752<br/>\n                            <br/>\n                            手数料：￥0<br/>\n                            送　料：￥0<br/>\n                                                            値引き：-￥487<br/>\n                                                        <hr style=\"border-top: 1px dotted #8c8b8b;\">\n                            合　計：￥4,265<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            ご注文者情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            お名前：Frederic Vervaecke 様<br/>\n                            お名前(カナ)：フレデリック ヴェルヴァーク 様<br/>\n                                                        郵便番号：〒1570077<br/>\n                            住所：東京都世田谷区鎌田123建物<br/>\n                            電話番号：0123456789<br/>\n                            メールアドレス：djfre2000@hotmail.com<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　配送情報<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n\n                                                            ◎お届け先<br/>\n                                <br/>\n                                お名前：Frederic Vervaecke 様<br/>\n                                お名前(カナ)：フレデリック ヴェルヴァーク 様<br/>\n                                                                郵便番号：〒1570077<br/>\n                                住所：東京都世田谷区鎌田123建物<br/>\n                                電話番号：0123456789<br/>\n                                <br/>\n                                配送方法：佐川急便<br/>\n                                お届け日：2020/09/21<br/>\n                                お届け時間：19時～21時<br/>\n                                <br/>\n                                                                    商品コード：cbd-100<br/>\n                                    商品名：MARITIME CBDオイル 10ml (1%)  通常購入  <br/>\n                                    数量：1<br/>\n                                    <br/>\n                                                                                                                        ***********************************************\n　クーポン情報                                 \n***********************************************\n\nクーポンコード: MARITIME10 初回10％割引オファー\n<br/>\n                                                        <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            このメッセージはお客様へのお知らせ専用ですので、<br />\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\nご了承ください。<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        本メールは、MARITIMEより送信しております。<br/>\n                        もしお心当たりが無い場合は、その旨 <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> までご連絡いただければ幸いです。<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', 'mailhistory'),
(44, 117, 1, '2020-09-17 04:06:16', '[MARITIME] ご注文ありがとうございます', 'Frederic Vervaecke 様\r\nこの度はご注文いただき誠にありがとうございます。下記ご注文内容にお間違いがないかご確認下さい。\r\n\r\n************************************************\r\n　ご請求金額\r\n************************************************\r\n\r\nご注文日時：2020/09/17 12:48:51\r\nご注文番号：117\r\nお支払い合計：￥4,265\r\nお支払い方法：銀行振込\r\nご利用ポイント：12 pt\r\nお問い合わせ内容：test\r\n\r\n************************************************\r\n　銀行口座情報\r\n************************************************\r\n\r\n銀行名：神戸信用金庫\r\n支店名：中央支店\r\n口座番号：0500384\r\n預金種別：普通\r\n口座名義：株式会社メディファイン\r\n\r\n************************************************\r\n　ご注文商品明細\r\n************************************************\r\n\r\n商品コード：cbd-100\r\n商品名：MARITIME CBDオイル 10ml (1%)  通常購入  \r\n単価：￥4,752\r\n数量：1\r\n\r\n\r\n-------------------------------------------------\r\n小　計：￥4,752\r\n\r\n手数料：￥0\r\n送　料：￥0\r\n値引き：-￥487\r\n============================================\r\n合　計：￥4,265\r\n\r\n************************************************\r\n　ご注文者情報\r\n************************************************\r\nお名前：Frederic Vervaecke 様お名前(カナ)：フレデリック ヴェルヴァーク 様郵便番号：〒1570077\r\n住所：東京都世田谷区鎌田123建物\r\n電話番号：0123456789\r\nメールアドレス：djfre2000@hotmail.com\r\n\r\n************************************************\r\n　配送情報\r\n************************************************\r\n\r\n◎お届け先\r\nお名前：Frederic Vervaecke 様お名前(カナ)：フレデリック ヴェルヴァーク 様郵便番号：〒1570077\r\n住所：東京都世田谷区鎌田123建物\r\n電話番号：0123456789\r\n\r\n配送方法：佐川急便\r\nお届け日：2020/09/21\r\nお届け時間：19時～21時\r\n\r\n商品コード：cbd-100\r\n商品名：MARITIME CBDオイル 10ml (1%)  通常購入  \r\n数量：1\r\n\r\n\r\n***********************************************\r\n　クーポン情報                                 \r\n***********************************************\r\n\r\nクーポンコード: MARITIME10 初回10％割引オファー\r\n\r\n\r\n============================================\r\n\r\nこのメッセージはお客様へのお知らせ専用ですので、<br />\r\nこのメッセージへの返信としてご質問をお送りいただいても回答できません。<br />\r\nご了承ください。<br />', NULL, 'mailhistory'),
(45, 118, NULL, '2020-09-17 05:22:31', '[MARITIME] Thank you for your order', 'Frederic Vervaecke \nThank you very much for your order.Please check to make sure that there is no mistake in the following order.\n\n************************************************\n　Billed amount\n************************************************\n\nOrder date：Sep 17, 2020 2:21:01 PM\nOrder number：118\nPayment total：¥4,267\nPayment method：Bank transfer\nUsed points：10 pt\nDetails of Inquiry：test\n\n************************************************\n　Bank account information\n************************************************\n\nBank name：神戸信用金庫\nBranch name：中央支店\nAccount number：0500384\nDeposit type：普通\nAccount holder：株式会社メディファイン\n\n************************************************\n　Detailed information on ordered items\n************************************************\n\nProduct code：cbd-100\nProduct name：MARITIME CBD oil 10ml (1%)  Regular purchase  \nUnit price：¥4,752\nQuantity：1\n\n\n-------------------------------------------------\nSub Total：¥4,752\n\nHandling fee：¥0\nShipping fee：¥0\nDiscount：(¥485)\n============================================\nTotal：¥4,267\n\n************************************************\n　Customer\'s information\n************************************************\nName：Frederic Vervaecke Name (Kana)：フレデリック ヴェルヴァーク Zip Code：Zip Code 1570077\nAddress：Tokyo, 世田谷区鎌田123建物\nTelephone Number：0123456789\nE-mail Address：djfre2000@hotmail.com\n\n************************************************\n　Shipping Information\n************************************************\n\n◎Recipient\nName：Frederic Vervaecke Name (Kana)：フレデリック ヴェルヴァーク Zip Code：Zip Code 1570077\nAddress：Tokyo, 世田谷区鎌田123建物\nTelephone Number：0123456789\n\nShipping method：Sagawa Express\nDelivery date：Sep 21, 2020\nDelivery time：16h-18h\n\nProduct code：cbd-100\nProduct name：MARITIME CBD oil 10ml (1%)  Regular purchase  \nQuantity：1\n\n\n***********************************************\n　{{ \'クーポン情報\'|trans }}                                 \n***********************************************\n\n{{ \'クーポンコード\'|trans }}: MARITIME10 初回10％割引オファー\n\n\n============================================\n\nThis email is dedicated to sending customers notifications;<br />\nso you will not get a reply by replying to this message.<br />\n\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/en/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"title\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">Thank you very much for your order.</div>\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke <br/>\n                            <br/>\n                            Please check to make sure that there is no mistake in the following order.<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　Billed amount<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            Order date：Sep 17, 2020 2:21:01 PM<br/>\n                            Order number：118<br/>\n                            Payment total：¥4,267<br/>\n                            Payment method：Bank transfer<br/>\n                                                        Used points：10 pt<br/>\n                                                        Details of Inquiry：test<br/>\n                            <br/>\n                                                                                    <hr style=\"border-top: 3px double #8c8b8b;\">\n                            Bank account information<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            <br/>\n                            Bank name：神戸信用金庫<br/>\n                            Branch name：中央支店<br/>\n                            Account number：0500384<br/>\n                            Deposit type：普通<br/>\n                            Account holder：株式会社メディファイン<br/>\n                            <br/>\n                                                        <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　Detailed information on ordered items<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                            Product code：cbd-100<br/>\n                                Product name：MARITIME CBD oil 10ml (1%)  Regular purchase  <br/>\n                                Unit price：¥4,752<br/>\n                                Quantity：1<br/>\n                                <br/>\n                                                        <hr style=\"border-top: 2px dashed #8c8b8b;\">\n                            Sub Total：¥4,752<br/>\n                            <br/>\n                            Handling fee：¥0<br/>\n                            Shipping fee：¥0<br/>\n                                                            Discount：(¥485)<br/>\n                                                        <hr style=\"border-top: 1px dotted #8c8b8b;\">\n                            Total：¥4,267<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            Customer&#039;s information<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            Name：Frederic Vervaecke <br/>\n                            Name (Kana)：フレデリック ヴェルヴァーク <br/>\n                                                        Zip Code：Zip Code 1570077<br/>\n                            Address：Tokyo, 世田谷区鎌田123建物<br/>\n                            Telephone Number：0123456789<br/>\n                            E-mail Address：djfre2000@hotmail.com<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　Shipping Information<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n\n                                                            ◎Recipient<br/>\n                                <br/>\n                                Name：Frederic Vervaecke <br/>\n                                Name (Kana)：フレデリック ヴェルヴァーク <br/>\n                                                                Zip Code：Zip Code 1570077<br/>\n                                Address：Tokyo, 世田谷区鎌田123建物<br/>\n                                Telephone Number：0123456789<br/>\n                                <br/>\n                                Shipping method：Sagawa Express<br/>\n                                Delivery date：Sep 21, 2020<br/>\n                                Delivery time：16h-18h<br/>\n                                <br/>\n                                                                    Product code：cbd-100<br/>\n                                    Product name：MARITIME CBD oil 10ml (1%)  Regular purchase  <br/>\n                                    Quantity：1<br/>\n                                    <br/>\n                                                                                                                        ***********************************************\n　{{ &#039;クーポン情報&#039;|trans }}                                 \n***********************************************\n\n{{ &#039;クーポンコード&#039;|trans }}: MARITIME10 初回10％割引オファー\n<br/>\n                                                        <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            This email is dedicated to sending customers notifications;<br />\nso you will not get a reply by replying to this message.<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        This email is sent by MARITIME<br/>\n                        If you think you received this email by mistake, please contact <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> .<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/en/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', 'mailhistory');
INSERT INTO `dtb_mail_history` (`id`, `order_id`, `creator_id`, `send_date`, `mail_subject`, `mail_body`, `mail_html_body`, `discriminator_type`) VALUES
(46, 119, NULL, '2020-09-17 06:20:57', '[MARITIME] Thank you for your order', 'Frederic Vervaecke \nThank you very much for your order.Please check to make sure that there is no mistake in the following order.\n\n************************************************\n　Billed amount\n************************************************\n\nOrder date：Sep 17, 2020 3:19:50 PM\nOrder number：119\nPayment total：¥8,544\nPayment method：Bank transfer\nUsed points：10 pt\nDetails of Inquiry：fggddg\n\n************************************************\n　Bank account information\n************************************************\n\nBank name：神戸信用金庫\nBranch name：中央支店\nAccount number：0500384\nDeposit type：普通\nAccount holder：株式会社メディファイン\n\n************************************************\n　Detailed information on ordered items\n************************************************\n\nProduct code：cbd-100\nProduct name：MARITIME CBD oil 10ml (1%)  Regular purchase  \nUnit price：¥9,504\nQuantity：2\n\n\n-------------------------------------------------\nSub Total：¥9,504\n\nHandling fee：¥0\nShipping fee：¥0\nDiscount：(¥960)\n============================================\nTotal：¥8,544\n\n************************************************\n　Customer\'s information\n************************************************\nName：Frederic Vervaecke Name (Kana)：フレデリック ヴェルヴァーク Zip Code：Zip Code 1570077\nAddress：Tokyo, 世田谷区鎌田123建物\nTelephone Number：0123456789\nE-mail Address：djfre2000@hotmail.com\n\n************************************************\n　Shipping Information\n************************************************\n\n◎Recipient\nName：Frederic Vervaecke Name (Kana)：フレデリック ヴェルヴァーク Zip Code：Zip Code 1570077\nAddress：Tokyo, 世田谷区鎌田123建物\nTelephone Number：0123456789\n\nShipping method：Sagawa Express\nDelivery date：Unspecified\nDelivery time：Unspecified\n\nProduct code：cbd-100\nProduct name：MARITIME CBD oil 10ml (1%)  Regular purchase  \nQuantity：2\n\n\n***********************************************\n　Coupon information                                 \n***********************************************\n\nCoupon Code: MARITIME10 10% discount on your first order\n\n\n============================================\n\nThis email is dedicated to sending customers notifications;<br />\nso you will not get a reply by replying to this message.<br />\n\n', '<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<body bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;\">\n<br>\n<br>\n<div align=\"center\"><a href=\"https://maritime.shourai.io/en/\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:30px;color:#333333;text-decoration:none;\">MARITIME</a></div>\n<!-- 100% background wrapper (grey background) -->\n<table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F0F0F0\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n    <tr>\n        <td align=\"center\" valign=\"top\" bgcolor=\"#F0F0F0\" style=\"background-color:#F0F0F0;border-collapse:collapse;\">\n            <br>\n            <!-- 600px container (white background) -->\n            <table id=\"html-mail-table1\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content\" align=\"left\" style=\"border-collapse:collapse;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        <br>\n                        <div class=\"title\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">Thank you very much for your order.</div>\n                        <br>\n                        <div class=\"body-text\" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;\">\n                            Frederic Vervaecke <br/>\n                            <br/>\n                            Please check to make sure that there is no mistake in the following order.<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　Billed amount<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            Order date：Sep 17, 2020 3:19:50 PM<br/>\n                            Order number：119<br/>\n                            Payment total：¥8,544<br/>\n                            Payment method：Bank transfer<br/>\n                                                        Used points：10 pt<br/>\n                                                        Details of Inquiry：fggddg<br/>\n                            <br/>\n                                                                                    <hr style=\"border-top: 3px double #8c8b8b;\">\n                            Bank account information<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            <br/>\n                            Bank name：神戸信用金庫<br/>\n                            Branch name：中央支店<br/>\n                            Account number：0500384<br/>\n                            Deposit type：普通<br/>\n                            Account holder：株式会社メディファイン<br/>\n                            <br/>\n                                                        <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　Detailed information on ordered items<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                                                            Product code：cbd-100<br/>\n                                Product name：MARITIME CBD oil 10ml (1%)  Regular purchase  <br/>\n                                Unit price：¥9,504<br/>\n                                Quantity：2<br/>\n                                <br/>\n                                                        <hr style=\"border-top: 2px dashed #8c8b8b;\">\n                            Sub Total：¥9,504<br/>\n                            <br/>\n                            Handling fee：¥0<br/>\n                            Shipping fee：¥0<br/>\n                                                            Discount：(¥960)<br/>\n                                                        <hr style=\"border-top: 1px dotted #8c8b8b;\">\n                            Total：¥8,544<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            Customer&#039;s information<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            Name：Frederic Vervaecke <br/>\n                            Name (Kana)：フレデリック ヴェルヴァーク <br/>\n                                                        Zip Code：Zip Code 1570077<br/>\n                            Address：Tokyo, 世田谷区鎌田123建物<br/>\n                            Telephone Number：0123456789<br/>\n                            E-mail Address：djfre2000@hotmail.com<br/>\n                            <br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n                            　Shipping Information<br/>\n                            <hr style=\"border-top: 3px double #8c8b8b;\">\n\n                                                            ◎Recipient<br/>\n                                <br/>\n                                Name：Frederic Vervaecke <br/>\n                                Name (Kana)：フレデリック ヴェルヴァーク <br/>\n                                                                Zip Code：Zip Code 1570077<br/>\n                                Address：Tokyo, 世田谷区鎌田123建物<br/>\n                                Telephone Number：0123456789<br/>\n                                <br/>\n                                Shipping method：Sagawa Express<br/>\n                                Delivery date：Unspecified<br/>\n                                Delivery time：Unspecified<br/>\n                                <br/>\n                                                                    Product code：cbd-100<br/>\n                                    Product name：MARITIME CBD oil 10ml (1%)  Regular purchase  <br/>\n                                    Quantity：2<br/>\n                                    <br/>\n                                                                                                                        ***********************************************\n　Coupon information                                 \n***********************************************\n\nCoupon Code: MARITIME10 10% discount on your first order\n<br/>\n                                                        <hr style=\"border-top: 2px dotted #8c8b8b;\">\n                            This email is dedicated to sending customers notifications;<br />\nso you will not get a reply by replying to this message.<br />\n<br/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n            <!--/600px container -->\n            <br>\n            <br>\n            <table id=\"html-mail-table2\" border=\"0\" width=\"600px\" cellpadding=\"10\" cellspacing=\"0\" class=\"container\" style=\"border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n                <tr>\n                    <td class=\"container-padding content footer-text\" align=\"left\" style=\"border-collapse:collapse;font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;\">\n                        This email is sent by MARITIME<br/>\n                        If you think you received this email by mistake, please contact <a href=\"mailto:maritime@shourai.io\" style=\"text-decoration:none;\">maritime@shourai.io</a> .<br/>\n                        <br/>\n                        <div class=\"title\" style=\"font-size:14px;font-family:Helvetica, Arial, sans-serif;font-weight:600;color:#374550;\"><a href=\"https://maritime.shourai.io/en/\" style=\"color:#aaaaaa;text-decoration:none;\">MARITIME</a></div>\n                        <div>copyright &copy; MARITIME all rights reserved.</div>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>\n<!--/100% background wrapper-->\n<br>\n<br>\n</body>\n\n</html>\n', 'mailhistory');

-- --------------------------------------------------------

--
-- Table structure for table `dtb_mail_template`
--

CREATE TABLE `dtb_mail_template` (
  `id` int(10) UNSIGNED NOT NULL,
  `creator_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `mail_subject` varchar(255) DEFAULT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtb_mail_template`
--

INSERT INTO `dtb_mail_template` (`id`, `creator_id`, `name`, `file_name`, `mail_subject`, `create_date`, `update_date`, `discriminator_type`) VALUES
(1, NULL, '注文受付メール', 'Mail/order.twig', 'ご注文ありがとうございます', '2017-03-07 10:14:52', '2017-03-07 10:14:52', 'mailtemplate'),
(2, NULL, '会員仮登録メール', 'Mail/entry_confirm.twig', '会員登録のご確認', '2017-03-07 10:14:52', '2017-03-07 10:14:52', 'mailtemplate'),
(3, NULL, '会員本登録メール', 'Mail/entry_complete.twig', '会員登録が完了しました。', '2017-03-07 10:14:52', '2017-03-07 10:14:52', 'mailtemplate'),
(4, NULL, '会員退会メール', 'Mail/customer_withdraw_mail.twig', '退会手続きのご完了', '2017-03-07 10:14:52', '2017-03-07 10:14:52', 'mailtemplate'),
(5, NULL, '問合受付メール', 'Mail/contact_mail.twig', 'お問い合わせを受け付けました。', '2017-03-07 10:14:52', '2017-03-07 10:14:52', 'mailtemplate'),
(6, NULL, 'パスワードリセット', 'Mail/forgot_mail.twig', 'パスワード変更のご確認', '2017-03-07 10:14:52', '2017-03-07 10:14:52', 'mailtemplate'),
(7, NULL, 'パスワードリマインダー', 'Mail/reset_complete_mail.twig', 'パスワード変更のお知らせ', '2017-03-07 10:14:52', '2017-03-07 10:14:52', 'mailtemplate'),
(8, NULL, '出荷通知メール', 'Mail/shipping_notify.twig', '商品出荷のお知らせ', '2017-03-07 10:14:52', '2017-03-07 10:14:52', 'mailtemplate'),
(13, 1, 'カゴ落ちメール', 'CheckKagoochi/Resource/template/default/Mail/kagoochi_notify.twig', 'ショッピングカートに入ったままのアイテムがあります', '2020-09-02 03:34:50', '2020-09-09 09:29:16', 'mailtemplate'),
(14, 1, '商品予約メール', 'Reserve4/order_reservation.twig', 'ご予約注文ありがとうございます', '2020-09-02 03:41:29', '2020-09-02 03:41:29', 'mailtemplate'),
(15, 1, '発売予定日変更通知メール', 'Reserve4/reservation_shipping_change.twig', '予約商品の発送予定日変更のお知らせ', '2020-09-02 03:41:29', '2020-09-02 03:41:29', 'mailtemplate'),
(16, NULL, '代理店問合受付メール', 'Mail/partner_mail.twig', '代理店お問い合わせを受け付けました。', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'mailtemplate'),
(17, 1, 'レビューメール', 'Mail/review_reminder.twig', 'いつもご利用ありがとうございます', '2020-09-02 03:34:50', '2020-09-09 09:29:16', 'mailtemplate');

-- --------------------------------------------------------

--
-- Table structure for table `dtb_member`
--

CREATE TABLE `dtb_member` (
  `id` int(10) UNSIGNED NOT NULL,
  `work_id` smallint(5) UNSIGNED DEFAULT NULL,
  `authority_id` smallint(5) UNSIGNED DEFAULT NULL,
  `creator_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  `login_id` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `sort_no` smallint(5) UNSIGNED NOT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `login_date` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtb_member`
--

INSERT INTO `dtb_member` (`id`, `work_id`, `authority_id`, `creator_id`, `name`, `department`, `login_id`, `password`, `salt`, `sort_no`, `create_date`, `update_date`, `login_date`, `discriminator_type`) VALUES
(1, 1, 0, 1, '管理者', 'EC-CUBE SHOP', 'admin', '187f7f6fa5fac0460e6689040bd88150debe4cf48ad1b7522210c552873fa4b4', 'rXj52vFKQ8q1gJPLSWt65N9kqi1sAbkB', 1, '2020-04-22 10:19:03', '2020-10-21 05:42:49', '2020-10-21 05:42:49', 'member'),
(2, 1, 1, 1, 'test', 'translation', 'test', 'e4ace8779b45dacaa410fe00bc8f4762e16eb60d82391cd7ff9fadea7336811f', '8ba6f0439f', 2, '2020-07-20 06:53:40', '2020-07-20 06:53:40', NULL, 'member');

-- --------------------------------------------------------

--
-- Table structure for table `dtb_news`
--

CREATE TABLE `dtb_news` (
  `id` int(10) UNSIGNED NOT NULL,
  `creator_id` int(10) UNSIGNED DEFAULT NULL,
  `publish_date` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `title` varchar(255) NOT NULL,
  `description` longtext,
  `url` varchar(4000) DEFAULT NULL,
  `link_method` tinyint(1) NOT NULL DEFAULT '0',
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `dtb_order`
--

CREATE TABLE `dtb_order` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED DEFAULT NULL,
  `country_id` smallint(5) UNSIGNED DEFAULT NULL,
  `pref_id` smallint(5) UNSIGNED DEFAULT NULL,
  `sex_id` smallint(5) UNSIGNED DEFAULT NULL,
  `job_id` smallint(5) UNSIGNED DEFAULT NULL,
  `payment_id` int(10) UNSIGNED DEFAULT NULL,
  `device_type_id` smallint(5) UNSIGNED DEFAULT NULL,
  `pre_order_id` varchar(255) DEFAULT NULL,
  `order_no` varchar(255) DEFAULT NULL,
  `message` varchar(4000) DEFAULT NULL,
  `name01` varchar(255) NOT NULL,
  `name02` varchar(255) NOT NULL,
  `kana01` varchar(255) DEFAULT NULL,
  `kana02` varchar(255) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone_number` varchar(14) DEFAULT NULL,
  `postal_code` varchar(8) DEFAULT NULL,
  `addr01` varchar(255) DEFAULT NULL,
  `addr02` varchar(255) DEFAULT NULL,
  `birth` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `subtotal` decimal(12,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `discount` decimal(12,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `delivery_fee_total` decimal(12,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `charge` decimal(12,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `tax` decimal(12,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `total` decimal(12,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `payment_total` decimal(12,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `payment_method` varchar(255) DEFAULT NULL,
  `note` varchar(4000) DEFAULT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `order_date` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `payment_date` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `currency_code` varchar(255) DEFAULT NULL,
  `complete_message` longtext,
  `complete_mail_message` longtext,
  `add_point` decimal(12,0) UNSIGNED NOT NULL DEFAULT '0',
  `use_point` decimal(12,0) UNSIGNED NOT NULL DEFAULT '0',
  `order_status_id` smallint(5) UNSIGNED DEFAULT NULL,
  `discriminator_type` varchar(255) NOT NULL,
  `gmo_payment_gateway_payment_status` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtb_order`
--

INSERT INTO `dtb_order` (`id`, `customer_id`, `country_id`, `pref_id`, `sex_id`, `job_id`, `payment_id`, `device_type_id`, `pre_order_id`, `order_no`, `message`, `name01`, `name02`, `kana01`, `kana02`, `company_name`, `email`, `phone_number`, `postal_code`, `addr01`, `addr02`, `birth`, `subtotal`, `discount`, `delivery_fee_total`, `charge`, `tax`, `total`, `payment_total`, `payment_method`, `note`, `create_date`, `update_date`, `order_date`, `payment_date`, `currency_code`, `complete_message`, `complete_mail_message`, `add_point`, `use_point`, `order_status_id`, `discriminator_type`, `gmo_payment_gateway_payment_status`) VALUES
(1, 1, NULL, 13, 1, 2, 2, 10, '3405d10759c7e3ee9457fa85c02887c2dab9924e', '1', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '5400.00', '0.00', '1000.00', '0.00', '474.00', '6400.00', '6400.00', '現金書留', NULL, '2020-05-01 08:34:15', '2020-05-01 08:35:02', NULL, NULL, 'JPY', NULL, NULL, '50', '0', 8, 'order', NULL),
(2, 1, NULL, 13, 1, 2, 1, 10, '3f31a6a5b4d84e18496f8a9673ade4b2c029b58e', NULL, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '16199.00', '0.00', '1000.00', '0.00', '1274.00', '17199.00', '17199.00', '郵便振替', NULL, '2020-05-01 10:21:41', '2020-05-01 10:21:41', NULL, NULL, 'JPY', NULL, NULL, '150', '0', 8, 'order', NULL),
(3, 1, NULL, 13, 1, 2, 5, 10, '10049bd9a0d867fe8648860e5e6dcfd84c21683a', '3', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '8800.00', '0.00', '0.00', '0.00', '800.00', '8800.00', '8800.00', 'クレジット決済', NULL, '2020-05-15 00:56:37', '2020-05-15 01:00:32', NULL, NULL, 'JPY', NULL, NULL, '80', '0', 8, 'order', NULL),
(11, 1, NULL, 13, 1, 2, 5, 10, '550567c8624190c11b209c8204181fcd5a870140', NULL, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '17600.00', '0.00', '0.00', '0.00', '1600.00', '17600.00', '17600.00', 'クレジット決済', NULL, '2020-05-15 05:56:00', '2020-05-15 05:56:00', NULL, NULL, 'JPY', NULL, NULL, '160', '0', 8, 'order', NULL),
(12, 1, NULL, 13, 1, 2, 5, 10, '4756e56604e04c982ef97cedcab34c7fbe00c3f1', '12', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '8800.00', '0.00', '0.00', '0.00', '800.00', '8800.00', '8800.00', 'クレジット決済', NULL, '2020-05-15 05:58:24', '2020-05-15 05:59:22', NULL, NULL, 'JPY', NULL, NULL, '80', '0', 8, 'order', NULL),
(13, 1, NULL, 13, 1, 2, 5, 10, '6d86fbfcfd4922520daeab7e96266f82c7c013ed', '13', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '8800.00', '0.00', '1000.00', '0.00', '891.00', '9800.00', '9800.00', 'クレジット決済', NULL, '2020-05-15 06:17:31', '2020-05-15 09:14:03', NULL, NULL, 'JPY', NULL, NULL, '80', '0', 8, 'order', NULL),
(14, 1, NULL, 13, 1, 2, 12, 10, '480aadd3e52b07d34bb96f7222ce783ef09c5dca', '14', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '2200.00', '0.00', '1000.00', '0.00', '291.00', '3200.00', '3200.00', '楽天ペイ', NULL, '2020-05-15 06:21:21', '2020-05-15 09:14:24', NULL, NULL, 'JPY', NULL, NULL, '20', '0', 8, 'order', NULL),
(15, 1, NULL, 13, 1, 2, 5, 10, 'c53920c7faf8b9a68a80c0580a621d9c185f7c57', '15', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '8800.00', '0.00', '1000.00', '0.00', '891.00', '9800.00', '9800.00', 'クレジット決済', NULL, '2020-05-16 10:34:59', '2020-05-16 10:37:36', NULL, NULL, 'JPY', NULL, NULL, '80', '0', 8, 'order', NULL),
(16, 1, NULL, 13, 1, 2, 12, 10, 'daafa2f8f6e88400a43802d6e07fbf85a4198289', NULL, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '2200.00', '0.00', '1000.00', '0.00', '291.00', '3200.00', '3200.00', '楽天ペイ', NULL, '2020-05-16 10:38:03', '2020-05-16 10:38:03', NULL, NULL, 'JPY', NULL, NULL, '20', '0', 8, 'order', NULL),
(17, NULL, NULL, 13, NULL, NULL, 5, 10, 'fc6dd8474325aaf74d4910b9cb7a96bf019be48e', NULL, NULL, 'Gues', 'User', 'グエスト', 'ユーザ', NULL, 'freddy@shourai.io', '0123456789', '1570077', '世田谷区鎌田', 'Test, 99', NULL, '8800.00', '0.00', '1000.00', '0.00', '891.00', '9800.00', '9800.00', 'クレジット決済', NULL, '2020-05-17 02:08:21', '2020-05-17 02:08:21', NULL, NULL, 'JPY', NULL, NULL, '0', '0', 8, 'order', NULL),
(18, NULL, NULL, 13, NULL, NULL, 5, 10, '3370ac78203036923bc6c36f05993a65e6df5812', '18', NULL, 'Gues', 'User', 'グエスト', 'ユーザ', NULL, 'freddy@shourai.io', '0123456789', '1570077', '世田谷区鎌田', 'Test, 99', NULL, '13200.00', '0.00', '1000.00', '0.00', '1291.00', '14200.00', '14200.00', 'クレジット決済', NULL, '2020-05-17 02:09:43', '2020-05-17 02:13:33', NULL, NULL, 'JPY', NULL, NULL, '0', '0', 8, 'order', NULL),
(19, 1, NULL, 13, 1, 2, 5, 10, '0989047a9e7cc5adc9c6399cff60ff1243f83705', '19', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '8800.00', '880.00', '1000.00', '0.00', '891.00', '8920.00', '8920.00', 'クレジット決済', NULL, '2020-05-22 03:44:49', '2020-05-23 07:20:45', NULL, NULL, 'JPY', NULL, '***********************************************\n　クーポン情報                                 \n***********************************************\n\nクーポンコード: maritime10 初回10％割引オファー\n', '71', '0', 8, 'order', NULL),
(20, 1, NULL, 13, 1, 2, 5, 10, 'c649f5cd3b96fb09e1ea93442980dfaa4eb4516d', NULL, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '2200.00', '0.00', '1000.00', '0.00', '291.00', '3200.00', '3200.00', 'クレジット決済', NULL, '2020-05-23 07:21:25', '2020-05-23 07:21:25', NULL, NULL, 'JPY', NULL, NULL, '20', '0', 8, 'order', NULL),
(21, 1, NULL, 13, 1, 2, 14, 10, 'fe227529f9ab22a80cc545330ab1b50cf883e99d', '21', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '1117.00', '0.00', '1000.00', '0.00', '193.00', '2117.00', '2117.00', 'PayPal決済', NULL, '2020-05-24 08:47:10', '2020-05-24 08:47:51', NULL, NULL, 'JPY', NULL, NULL, '10', '0', 8, 'order', NULL),
(22, 1, NULL, 13, 1, 2, 13, 10, 'd7210bcfb2f6606e439575d5c378bfc5b6430a3e', '22', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '8800.00', '0.00', '1000.00', '0.00', '891.00', '9800.00', '9800.00', 'クレジット決済「定期購入」', NULL, '2020-05-26 12:05:15', '2020-05-27 00:49:54', NULL, NULL, 'JPY', NULL, NULL, '80', '0', 8, 'order', NULL),
(23, 1, NULL, 13, 1, 2, 13, 10, '74539e45951df6064bdef5809dbc29425045b889', '23', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '31350.00', '3135.00', '1000.00', '0.00', '2941.00', '29215.00', '29215.00', 'クレジット決済「定期購入」', NULL, '2020-05-27 11:46:11', '2020-05-27 12:19:05', NULL, NULL, 'JPY', NULL, '***********************************************\n　クーポン情報                                 \n***********************************************\n\nクーポンコード: MARITIME10 初回10％割引オファー\n', '254', '0', 8, 'order', NULL),
(24, 1, NULL, 13, 1, 2, 13, 10, '6e13fd81af63c888cfe1d0f8a2e0e8b83709be16', '24', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '30780.00', '0.00', '800.00', '0.00', '2339.00', '31580.00', '31580.00', 'クレジット決済「定期購入」', NULL, '2020-06-03 07:17:59', '2020-06-03 07:22:39', NULL, NULL, 'JPY', NULL, NULL, '285', '0', 8, 'order', NULL),
(25, 1, NULL, 13, 1, 2, 13, 10, '1bcf6c4ff255168f80ce3bb029a91695f68e0114', '25', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '61560.00', '0.00', '1600.00', '0.00', '4678.00', '63160.00', '63160.00', 'クレジット決済「定期購入」', NULL, '2020-06-03 07:24:32', '2020-06-04 04:25:36', NULL, NULL, 'JPY', NULL, NULL, '570', '0', 8, 'order', NULL),
(26, 4, NULL, 13, NULL, NULL, 5, 10, '4edcf3ce6b3fa55036eae826e76db1aa8cf55068', NULL, NULL, 'Test2', 'Test', 'カナ', 'カナ', NULL, '05049pyj+1@gmail.com', '11122223333', '1000001', '千代田区千代田', '1-1-1', NULL, '12960.00', '0.00', '800.00', '0.00', '1019.00', '13760.00', '13760.00', 'クレジット決済', NULL, '2020-06-03 10:21:34', '2020-06-03 10:21:34', NULL, NULL, 'JPY', NULL, NULL, '120', '0', 8, 'order', NULL),
(27, 4, NULL, 13, NULL, NULL, 5, 10, '073d1b69375f57705cd91d5a2a6ca349fa20e8c8', NULL, NULL, 'Test2', 'Test', 'カナ', 'カナ', NULL, '05049pyj+1@gmail.com', '11122223333', '1000001', '千代田区千代田', '1-1-1', NULL, '15552.00', '0.00', '800.00', '0.00', '1211.00', '16352.00', '16352.00', 'クレジット決済', NULL, '2020-06-03 14:27:51', '2020-06-03 14:27:51', NULL, NULL, 'JPY', NULL, NULL, '144', '0', 8, 'order', NULL),
(28, 4, NULL, 13, NULL, NULL, 5, 10, '1909097e66d89d39bb29e5bd310dad8d23773403', NULL, NULL, 'Test2', 'Test', 'カナ', 'カナ', NULL, '05049pyj+1@gmail.com', '11122223333', '1000001', '千代田区千代田', '1-1-1', NULL, '10368.00', '0.00', '800.00', '0.00', '827.00', '11168.00', '11168.00', 'クレジット決済', NULL, '2020-06-03 14:52:55', '2020-06-03 14:52:55', NULL, NULL, 'JPY', NULL, NULL, '96', '0', 8, 'order', NULL),
(29, 4, NULL, 13, NULL, NULL, 5, 10, '32df25ce8635602c52c6449fd942996169ff2d88', NULL, NULL, 'Test2', 'Test', 'カナ', 'カナ', NULL, '05049pyj+1@gmail.com', '11122223333', '1000001', '千代田区千代田', '1-1-1', NULL, '15552.00', '0.00', '800.00', '0.00', '1211.00', '16352.00', '16352.00', 'クレジット決済', NULL, '2020-06-03 14:53:38', '2020-06-03 14:53:38', NULL, NULL, 'JPY', NULL, NULL, '144', '0', 8, 'order', NULL),
(30, 4, NULL, 13, NULL, NULL, 5, 10, 'b58fee34a2a7091f1bafcf35b9aea0f1e8b96e69', NULL, NULL, 'Test2', 'Test', 'カナ', 'カナ', NULL, '05049pyj+1@gmail.com', '11122223333', '1000001', '千代田区千代田', '1-1-1', NULL, '10368.00', '0.00', '800.00', '0.00', '827.00', '11168.00', '11168.00', 'クレジット決済', NULL, '2020-06-03 14:55:48', '2020-06-03 14:55:48', NULL, NULL, 'JPY', NULL, NULL, '96', '0', 8, 'order', NULL),
(31, 4, NULL, 13, NULL, NULL, 5, 10, '473e86b7fdd62a87ef4fdbd1d85a5f45622b7793', NULL, NULL, 'Test2', 'Test', 'カナ', 'カナ', NULL, '05049pyj+1@gmail.com', '11122223333', '1000001', '千代田区千代田', '1-1-1', NULL, '15552.00', '0.00', '800.00', '0.00', '1211.00', '16352.00', '16352.00', 'クレジット決済', NULL, '2020-06-03 15:04:08', '2020-06-03 15:04:08', NULL, NULL, 'JPY', NULL, NULL, '144', '0', 8, 'order', NULL),
(32, 4, NULL, 13, NULL, NULL, 5, 10, '0e434821127e4e9eacbd1cae13673becf2b760a5', '32', NULL, 'Test2', 'Test', 'カナ', 'カナ', NULL, '05049pyj+1@gmail.com', '11122223333', '1000001', '千代田区千代田', '1-1-1', NULL, '20736.00', '0.00', '0.00', '0.00', '1536.00', '20736.00', '20736.00', 'クレジット決済', NULL, '2020-06-03 15:05:19', '2020-06-03 15:06:01', NULL, NULL, 'JPY', NULL, NULL, '192', '0', 8, 'order', NULL),
(33, 4, NULL, 13, NULL, NULL, 5, 10, '47b98c2d77fef10df637dce0b1db99be4ac64d37', NULL, NULL, 'Test2', 'Test', 'カナ', 'カナ', NULL, '05049pyj+1@gmail.com', '11122223333', '1000001', '千代田区千代田', '1-1-1', NULL, '10368.00', '0.00', '1600.00', '0.00', '886.00', '11968.00', '11968.00', 'クレジット決済', NULL, '2020-06-03 15:06:18', '2020-06-03 15:06:18', NULL, NULL, 'JPY', NULL, NULL, '96', '0', 8, 'order', NULL),
(34, 4, NULL, 13, NULL, NULL, 5, 10, '97650a8d5a7563484a2cc0f7552c3dacfcf17579', NULL, NULL, 'Test2', 'Test', 'カナ', 'カナ', NULL, '05049pyj+1@gmail.com', '11122223333', '1000001', '千代田区千代田', '1-1-1', NULL, '12874.00', '0.00', '0.00', '0.00', '954.00', '12874.00', '12874.00', 'クレジット決済', NULL, '2020-06-03 15:07:46', '2020-06-03 15:07:46', NULL, NULL, 'JPY', NULL, NULL, '120', '0', 8, 'order', NULL),
(35, 4, NULL, 13, NULL, NULL, 13, 10, '6ac5a3bafb2d37882d7c10c17dfd8ef518bb6e5a', NULL, NULL, 'Test2', 'Test', 'カナ', 'カナ', NULL, '05049pyj+1@gmail.com', '11122223333', '1000001', '千代田区千代田', '1-1-1', NULL, '1880.00', '0.00', '1600.00', '0.00', '258.00', '3480.00', '3480.00', 'クレジット決済「定期購入」', NULL, '2020-06-03 15:08:05', '2020-06-03 15:08:05', NULL, NULL, 'JPY', NULL, NULL, '18', '0', 8, 'order', NULL),
(36, 4, NULL, 13, NULL, NULL, 5, 10, '197db9774c89e3eab46733e49a395ff1fee5a871', NULL, NULL, 'Test2', 'Test', 'カナ', 'カナ', NULL, '05049pyj+1@gmail.com', '11122223333', '1000001', '千代田区千代田', '1-1-1', NULL, '5012.00', '0.00', '0.00', '0.00', '372.00', '5012.00', '5012.00', 'クレジット決済', NULL, '2020-06-03 15:55:19', '2020-06-03 15:55:19', NULL, NULL, 'JPY', NULL, NULL, '48', '0', 8, 'order', NULL),
(37, 1, NULL, 13, 1, 2, 5, 10, 'dd9f417b8728fa7e1b175fbc09cdb4c1232b80a9', NULL, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '4536.00', '0.00', '800.00', '0.00', '395.00', '5336.00', '5336.00', 'クレジット決済', NULL, '2020-06-04 06:42:16', '2020-06-04 06:42:16', NULL, NULL, 'JPY', NULL, NULL, '42', '0', 8, 'order', NULL),
(38, 1, NULL, 13, 1, 2, 5, 10, '01a197b164dac61880fc4e775e1ba14753225413', NULL, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '9072.00', '0.00', '1600.00', '0.00', '790.00', '10672.00', '10672.00', 'クレジット決済', NULL, '2020-06-04 06:42:56', '2020-06-04 06:42:56', NULL, NULL, 'JPY', NULL, NULL, '84', '0', 8, 'order', NULL),
(39, 1, NULL, 13, 1, 2, 5, 10, '3218fee57e5b4cf18f5e69190d2a999bb28fd0d8', NULL, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '27216.00', '0.00', '0.00', '0.00', '2016.00', '27216.00', '27216.00', 'クレジット決済', NULL, '2020-06-04 06:43:15', '2020-06-04 06:43:15', NULL, NULL, 'JPY', NULL, NULL, '252', '0', 8, 'order', NULL),
(40, 1, NULL, 13, 1, 2, 5, 2, '65218a9de6bcc8763b6c4adb493a4bdaa6e37b4d', '40', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '4536.00', '0.00', '800.00', '0.00', '395.00', '5336.00', '5336.00', 'クレジット決済', NULL, '2020-06-05 08:12:29', '2020-06-05 08:12:58', NULL, NULL, 'JPY', NULL, NULL, '42', '0', 8, 'order', NULL),
(41, 1, NULL, 13, 1, 2, 5, 10, '344e80fdfcbe4796379b988856c2669fe1c04855', NULL, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '5632.00', '0.00', '1600.00', '0.00', '535.00', '7232.00', '7232.00', 'クレジット決済', NULL, '2020-06-05 09:06:16', '2020-06-05 09:06:16', NULL, NULL, 'JPY', NULL, NULL, '52', '0', 8, 'order', NULL),
(42, 1, NULL, 13, 1, 2, 5, 10, '6190095275e11343f58e229a928bc6910323702c', NULL, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '10168.00', '0.00', '0.00', '0.00', '753.00', '10168.00', '10168.00', 'クレジット決済', NULL, '2020-06-05 09:06:42', '2020-06-05 09:06:42', NULL, NULL, 'JPY', NULL, NULL, '94', '0', 8, 'order', NULL),
(43, 1, NULL, 13, 1, 2, 5, 10, 'a37fbd87f4a0ff8b2e220aedb07fb8ff9a8f69b3', '43', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '9072.00', '0.00', '1600.00', '0.00', '790.00', '10672.00', '10672.00', 'クレジット決済', NULL, '2020-06-05 09:10:56', '2020-06-05 09:16:54', NULL, NULL, 'JPY', NULL, NULL, '84', '0', 8, 'order', NULL),
(44, 1, NULL, 13, 1, 2, 13, 10, 'b5194dcaed88ff2ed937ce4897cd8251a34fd2ba', NULL, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '940.00', '0.00', '800.00', '0.00', '129.00', '1740.00', '1740.00', 'クレジット決済「定期購入」', NULL, '2020-06-05 09:19:05', '2020-06-05 09:19:05', NULL, NULL, 'JPY', NULL, NULL, '9', '0', 8, 'order', NULL),
(45, 1, NULL, 13, 1, 2, 5, 2, 'b2a7b573eac6251775f68b71a36e18cebe25f69f', NULL, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '10168.00', '0.00', '0.00', '0.00', '753.00', '10168.00', '10168.00', 'クレジット決済', NULL, '2020-06-05 09:25:32', '2020-06-05 09:25:32', NULL, NULL, 'JPY', NULL, NULL, '94', '0', 8, 'order', NULL),
(46, 1, NULL, 13, 1, 2, 5, 10, '783954b790ed5e9d448e34cfe9981b022a643378', NULL, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '2192.00', '0.00', '1753600.00', '0.00', '129490.00', '1755792.00', '1755792.00', 'クレジット決済', NULL, '2020-06-11 12:10:56', '2020-06-11 12:10:56', NULL, NULL, 'JPY', NULL, NULL, '20', '0', 8, 'order', NULL),
(47, 1, NULL, 13, 1, 2, 5, 10, 'd65252f0ca2606f12c40551b73b2a7ddce8cb2ad', NULL, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '3288.00', '0.00', '0.00', '0.00', '243.00', '3288.00', '3288.00', 'クレジット決済', NULL, '2020-06-11 12:12:30', '2020-06-11 12:12:30', NULL, NULL, 'JPY', NULL, NULL, '30', '0', 8, 'order', NULL),
(48, 1, NULL, 13, 1, 2, 5, 10, '28fe9c96910bff3c213f4d0db7a650c0a336f791', '48', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '2192.00', '0.00', '1600.00', '0.00', '280.00', '3792.00', '3792.00', 'クレジット決済', NULL, '2020-06-11 12:12:58', '2020-06-11 13:52:06', NULL, NULL, 'JPY', NULL, NULL, '20', '0', 8, 'order', NULL),
(49, 5, NULL, 13, NULL, NULL, 5, 10, '1f5d96dde6f18638232f7e760d228b6fba652356', NULL, NULL, 'Test3', 'Test', 'カナ', 'カナ', NULL, '05049pyj+2@gmail.com', '11122223333', '1000001', '千代田区千代田', '1-1-1', NULL, '2192.00', '0.00', '1600.00', '0.00', '280.00', '3792.00', '3792.00', 'クレジット決済', NULL, '2020-06-11 13:50:56', '2020-06-11 13:50:56', NULL, NULL, 'JPY', NULL, NULL, '20', '0', 8, 'order', NULL),
(50, 5, NULL, 13, NULL, NULL, 5, 10, '7ff9a65e5bda31c02a40d76f09594018cd2d9462', NULL, NULL, 'Test3', 'Test', 'カナ', 'カナ', NULL, '05049pyj+2@gmail.com', '11122223333', '1000001', '千代田区千代田', '1-1-1', NULL, '6577.00', '0.00', '0.00', '0.00', '487.00', '6577.00', '6577.00', 'クレジット決済', NULL, '2020-06-11 13:51:45', '2020-06-11 13:51:45', NULL, NULL, 'JPY', NULL, NULL, '61', '0', 8, 'order', NULL),
(51, 1, NULL, 13, 1, 2, 5, 10, 'c2058a2285961b9300c1f6740df0d5ad8ee30d76', NULL, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '3288.00', '0.00', '0.00', '0.00', '243.00', '3288.00', '3288.00', 'クレジット決済', NULL, '2020-06-11 13:52:37', '2020-06-11 13:52:37', NULL, NULL, 'JPY', NULL, NULL, '30', '0', 8, 'order', NULL),
(52, 1, NULL, 13, 1, 2, 5, 10, '5b80d65eb23fccbfd6a13960689799665e6d8958', '52', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '2192.00', '0.00', '1600.00', '0.00', '280.00', '3792.00', '3792.00', 'クレジット決済', NULL, '2020-06-12 02:25:38', '2020-06-12 09:15:50', NULL, NULL, 'JPY', NULL, NULL, '20', '0', 8, 'order', NULL),
(53, 1, NULL, 13, 1, 2, 5, 10, 'f08916af4c4995511be84438b61669567c9356ea', NULL, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '3288.00', '0.00', '0.00', '0.00', '243.00', '3288.00', '3288.00', 'クレジット決済', NULL, '2020-06-12 09:19:42', '2020-06-12 09:19:42', NULL, NULL, 'JPY', NULL, NULL, '30', '0', 8, 'order', NULL),
(54, 1, NULL, 13, 1, 2, 5, 10, '8a345cc6ec461500680358155f638f90e2686286', NULL, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '13153.00', '0.00', '0.00', '0.00', '973.00', '13153.00', '13153.00', 'クレジット決済', NULL, '2020-06-12 09:25:25', '2020-06-12 09:25:25', NULL, NULL, 'JPY', NULL, NULL, '121', '0', 8, 'order', NULL),
(55, 1, NULL, 13, 1, 2, 5, 10, '6f31e16faefd9ba04520194b06d61aa8bc20c140', '55', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '3288.00', '0.00', '800.00', '0.00', '302.00', '4088.00', '4088.00', 'クレジット決済', NULL, '2020-06-13 03:20:44', '2020-06-13 03:23:45', NULL, NULL, 'JPY', NULL, NULL, '30', '0', 8, 'order', NULL),
(56, 1, NULL, 13, 1, 2, 5, 10, 'abff9bbcff710eef63fe56bc6325344ded1ccd32', NULL, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '4384.00', '0.00', '0.00', '0.00', '324.00', '4384.00', '4384.00', 'クレジット決済', NULL, '2020-06-13 03:24:07', '2020-06-13 03:24:07', NULL, NULL, 'JPY', NULL, NULL, '40', '0', 8, 'order', NULL),
(57, 1, NULL, 13, 1, 2, 5, 10, '9883d88164e0b7a1641db7d0d06c114e47d4f288', NULL, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '9865.00', '0.00', '0.00', '0.00', '730.00', '9865.00', '9865.00', 'クレジット決済', NULL, '2020-06-13 03:24:44', '2020-06-13 03:24:44', NULL, NULL, 'JPY', NULL, NULL, '91', '0', 8, 'order', NULL),
(58, 1, NULL, 13, 1, 2, 13, 10, 'f647b5350e38445008078e0b363343b2892b4f8d', NULL, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '3888.00', '0.00', '800.00', '0.00', '347.00', '4688.00', '4688.00', 'クレジット決済「定期購入」', NULL, '2020-06-13 03:25:47', '2020-06-13 03:25:47', NULL, NULL, 'JPY', NULL, NULL, '36', '0', 8, 'order', NULL),
(59, 1, NULL, 13, 1, 2, 13, 10, 'eb62789db50dd2b830c7157eaecb314c58185a9c', NULL, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '5832.00', '0.00', '0.00', '0.00', '432.00', '5832.00', '5832.00', 'クレジット決済「定期購入」', NULL, '2020-06-13 03:26:37', '2020-06-13 03:26:37', NULL, NULL, 'JPY', NULL, NULL, '54', '0', 8, 'order', NULL),
(60, 1, NULL, 13, 1, 2, 5, 10, '87a580022ba6670d51c35207f3afc9642491732c', NULL, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '3288.00', '0.00', '800.00', '0.00', '302.00', '4088.00', '4088.00', 'クレジット決済', NULL, '2020-06-13 03:29:04', '2020-06-13 03:29:04', NULL, NULL, 'JPY', NULL, NULL, '30', '0', 8, 'order', NULL),
(61, 1, NULL, 13, 1, 2, 14, 10, 'f109b34bc47246dc59ee68cfc30eb37f33e5e9f9', '61', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '4384.00', '0.00', '0.00', '0.00', '324.00', '4384.00', '4384.00', 'PayPal決済', NULL, '2020-06-13 03:29:28', '2020-06-27 02:53:37', '2020-06-14 05:09:39', '2020-06-14 05:09:39', 'JPY', NULL, NULL, '40', '0', 3, 'order', NULL),
(62, 1, NULL, 13, 1, 2, 5, 10, 'fb41bad8fc372ca369092da7ca3b968ba86e1adc', NULL, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '1096.00', '0.00', '800.00', '0.00', '140.00', '1896.00', '1896.00', 'クレジット決済', NULL, '2020-06-14 05:46:34', '2020-06-14 05:46:34', NULL, NULL, 'JPY', NULL, NULL, '10', '0', 8, 'order', NULL),
(63, NULL, NULL, 13, NULL, NULL, 14, 10, '6ef009d03978b6961410a575dccce902150c0656', '63', NULL, 'Sugimori', 'Koichiro', 'スギモリ', 'コウイチロウ', 'Personal Media International', 'ko@thepmi.net', '+447803924335', '1520004', '目黒区鷹番', '1-8-17', NULL, '5012.00', '0.00', '0.00', '0.00', '372.00', '5012.00', '5012.00', 'PayPal決済', NULL, '2020-06-17 08:34:08', '2020-06-17 08:34:29', NULL, NULL, 'JPY', NULL, NULL, '0', '0', 8, 'order', NULL),
(64, 1, NULL, 13, 1, 2, 14, 10, '6cb04827ea97c95ee510be24e7887e3ee2a18100', '64', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '13153.00', '0.00', '0.00', '0.00', '973.00', '13153.00', '13153.00', 'PayPal決済', NULL, '2020-06-17 11:27:16', '2020-06-27 02:54:35', '2020-06-17 11:28:22', '2020-06-17 11:28:22', 'JPY', NULL, NULL, '121', '0', 3, 'order', NULL),
(65, 1, NULL, 13, 1, 2, 5, 10, '205a584d036aac0cef8fd3a76e61274a7a5bce23', '65', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '1096.00', '0.00', '800.00', '0.00', '140.00', '1896.00', '1896.00', 'クレジット決済', NULL, '2020-06-17 12:52:17', '2020-06-17 13:11:34', NULL, NULL, 'JPY', NULL, NULL, '10', '0', 8, 'order', NULL),
(66, 1, NULL, 13, 1, 2, 14, 2, 'afda321cca21b72f6a8352fa3bd7f922a22d6da1', '66', 'よろしく', 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '1128.00', '0.00', '800.00', '0.00', '143.00', '1928.00', '1928.00', 'PayPal決済', NULL, '2020-06-26 01:51:01', '2020-06-26 04:10:38', NULL, NULL, 'JPY', NULL, NULL, '10', '0', 8, 'order', NULL),
(67, 1, NULL, 13, 1, 2, 14, 10, '1721f9a64d6faa438100d6bb69b42a9f1e492768', '67', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '1128.00', '0.00', '800.00', '0.00', '143.00', '1928.00', '1928.00', 'PayPal決済', NULL, '2020-06-26 04:20:30', '2020-06-27 02:54:35', '2020-06-26 04:21:07', '2020-06-26 04:21:07', 'JPY', NULL, NULL, '10', '0', 3, 'order', NULL),
(68, 1, NULL, 13, 1, 2, 14, 10, '66389f48e16e5e731e1ed5bf673a85ad417ea571', '68', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '4666.00', '0.00', '0.00', '0.00', '346.00', '4666.00', '4666.00', 'PayPal決済', NULL, '2020-06-26 08:19:22', '2020-06-26 08:19:35', NULL, NULL, 'JPY', NULL, NULL, '43', '0', 8, 'order', NULL),
(69, 1, NULL, 13, 1, 2, 14, 10, 'c53c827c837c605260038465ceaf6a48639d984b', '69', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '4666.00', '0.00', '0.00', '0.00', '346.00', '4666.00', '4666.00', 'PayPal決済', NULL, '2020-06-26 08:20:44', '2020-06-26 08:20:56', NULL, NULL, 'JPY', NULL, NULL, '43', '0', 8, 'order', NULL),
(70, 1, NULL, 13, 1, 2, 14, 10, 'd98240bb9e7be347c02bc4cc5a47457039d90197', '70', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '4666.00', '0.00', '0.00', '0.00', '346.00', '4666.00', '4666.00', 'PayPal決済', NULL, '2020-06-26 08:38:41', '2020-06-26 08:39:47', NULL, NULL, 'JPY', NULL, NULL, '43', '0', 8, 'order', NULL),
(71, 1, NULL, 13, 1, 2, 14, 10, 'cc4581dd05691076ca941e32c4310d7d7fa41169', '71', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '4666.00', '0.00', '0.00', '0.00', '346.00', '4666.00', '4666.00', 'PayPal決済', NULL, '2020-06-26 09:01:33', '2020-06-27 02:54:35', '2020-06-26 09:02:31', '2020-06-26 09:02:31', 'JPY', NULL, NULL, '43', '0', 3, 'order', NULL),
(72, 1, NULL, 13, 1, 2, 14, 10, 'c40e316922cebb6e643a58bc5aee8c4c29546225', '72', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '31590.00', '0.00', '0.00', '0.00', '2340.00', '31590.00', '31590.00', 'PayPal決済', NULL, '2020-06-30 11:01:54', '2020-06-30 11:07:15', '2020-06-30 11:04:46', '2020-06-30 11:04:46', 'JPY', NULL, NULL, '293', '0', 3, 'order', NULL),
(73, 1, NULL, 13, 1, 2, 5, 10, 'e646b0fc14438d38711576ac1c5bb285f2dd7b89', '73', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '4666.00', '0.00', '0.00', '0.00', '346.00', '4666.00', '4666.00', 'クレジット決済', NULL, '2020-06-30 11:44:45', '2020-07-02 02:45:05', NULL, NULL, 'JPY', NULL, NULL, '43', '0', 8, 'order', NULL),
(74, 1, NULL, 13, 1, 2, 5, 2, 'a8ef2b04812bc9cc5ff98cf1be13167e1b316d08', '74', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '4666.00', '0.00', '0.00', '0.00', '346.00', '4666.00', '4666.00', 'クレジット決済', NULL, '2020-07-02 02:48:28', '2020-07-02 02:49:05', NULL, NULL, 'JPY', NULL, NULL, '43', '0', 8, 'order', NULL),
(75, 1, NULL, 13, 1, 2, 5, 2, '920ded563019450ab27a7b1533ae1a3a03b0a612', NULL, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '4666.00', '0.00', '0.00', '0.00', '346.00', '4666.00', '4666.00', 'クレジット決済', NULL, '2020-07-02 02:51:51', '2020-07-02 02:51:51', NULL, NULL, 'JPY', NULL, NULL, '43', '0', 8, 'order', NULL),
(76, 1, NULL, 13, 1, 2, 14, 10, '6dd5fd320492ef44aa7123782964523373162d12', '76', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '1128.00', '0.00', '600.00', '0.00', '128.00', '1728.00', '1728.00', 'PayPal決済', NULL, '2020-07-16 08:56:03', '2020-07-16 08:56:28', NULL, NULL, 'JPY', NULL, NULL, '10', '0', 8, 'order', NULL),
(77, 1, NULL, 13, 1, 2, 14, 10, '347c741803588c474aa7a5fd29616c00e1ed19b4', '77', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '7893.00', '0.00', '0.00', '0.00', '585.00', '7893.00', '7893.00', 'PayPal決済', NULL, '2020-07-16 09:06:59', '2020-07-16 09:29:59', NULL, NULL, 'JPY', NULL, NULL, '73', '0', 8, 'order', NULL),
(78, 1, NULL, 13, 1, 2, 14, 10, '46927e622f792dc71a9f7708bed9ed78c2e8374a', '78', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '8038.00', '0.00', '0.00', '0.00', '730.00', '8038.00', '8038.00', 'PayPal決済', NULL, '2020-07-16 09:37:36', '2020-07-16 09:39:10', NULL, NULL, 'JPY', NULL, NULL, '73', '0', 8, 'order', NULL),
(79, 1, NULL, 13, 1, 2, 14, 10, 'efd7a9987312a512352503ab45cce561f344fa4e', '79', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '1148.00', '0.00', '600.00', '0.00', '159.00', '1748.00', '1748.00', 'PayPal決済', NULL, '2020-07-16 09:39:53', '2020-07-16 09:57:06', NULL, NULL, 'JPY', NULL, NULL, '10', '0', 8, 'order', NULL),
(80, 1, NULL, 13, 1, 2, 14, 10, '8ff6429cd27f03c55dd0c43343a1c79eebe82d3b', '80', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '1276.00', '0.00', '600.00', '0.00', '171.00', '1876.00', '1876.00', 'PayPal決済', NULL, '2020-07-16 09:57:45', '2020-07-16 12:19:45', '2020-07-16 10:13:35', '2020-07-16 10:13:35', 'JPY', NULL, NULL, '12', '0', 9, 'order', NULL),
(81, 1, NULL, 13, 1, 2, 14, 10, '3f63e5eba754f53e138736b865c200eeedbecf2e', '81', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '1148.00', '0.00', '600.00', '0.00', '159.00', '1748.00', '1748.00', 'PayPal決済', NULL, '2020-07-16 10:16:15', '2020-07-16 10:28:49', NULL, NULL, 'JPY', NULL, NULL, '10', '0', 8, 'order', NULL),
(82, 1, NULL, 13, 1, 2, 14, 10, '2970bd3088299977027407db1a67f6855147ec5a', '82', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '2552.00', '0.00', '600.00', '0.00', '287.00', '3152.00', '3152.00', 'PayPal決済', NULL, '2020-07-16 12:21:13', '2020-07-16 12:56:35', '2020-07-16 12:22:23', '2020-07-16 12:22:23', 'JPY', NULL, NULL, '24', '0', 6, 'order', NULL),
(83, 1, NULL, 13, 1, 2, 14, 10, '47e07c346b03d88c550df180157b9229c27e481a', '83', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '1148.00', '0.00', '600.00', '0.00', '159.00', '1748.00', '1748.00', 'PayPal決済', NULL, '2020-07-16 13:12:52', '2020-07-16 13:13:40', '2020-07-16 13:13:35', '2020-07-16 13:13:35', 'JPY', NULL, NULL, '10', '0', 10, 'order', NULL),
(84, 1, NULL, 13, 1, 2, 14, 10, 'c62a9203b830fd211af71c6a867833fce1cfd17d', '84', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '1276.00', '0.00', '600.00', '0.00', '171.00', '1876.00', '1876.00', 'PayPal決済', NULL, '2020-07-17 08:12:59', '2020-07-21 07:43:29', '2020-07-17 08:14:11', '2020-07-17 08:14:54', 'JPY', NULL, NULL, '12', '0', 5, 'order', NULL),
(85, 1, NULL, 13, 1, 2, 14, 10, 'd22a7443d83d87718afc8cd65c13a07a95a7f3d1', NULL, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '20592.00', '0.00', '0.00', '0.00', '1872.00', '20592.00', '20592.00', 'PayPal決済', NULL, '2020-07-17 08:18:37', '2020-07-17 08:18:37', NULL, NULL, 'JPY', NULL, NULL, '187', '0', 8, 'order', NULL),
(86, 1, NULL, 13, 1, 2, 14, 10, '96ebbc81b7fa7c65c837a531f8f5c7c2504b9b4c', '86', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '4752.00', '0.00', '0.00', '0.00', '432.00', '4752.00', '4752.00', 'PayPal決済', NULL, '2020-08-03 03:09:06', '2020-08-03 03:23:49', NULL, NULL, 'JPY', NULL, NULL, '43', '0', 8, 'order', NULL),
(87, 1, NULL, 13, 1, 2, 12, 10, '753d1377ce02204e738eded8f103182f8b8ba7a1', '87', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '5900.00', '0.00', '0.00', '0.00', '536.00', '5900.00', '5900.00', '楽天ペイ', NULL, '2020-08-03 13:20:51', '2020-08-03 13:21:24', NULL, NULL, 'JPY', NULL, NULL, '53', '0', 8, 'order', NULL),
(88, 1, NULL, 13, 1, 2, 15, 10, '87c106e308dfb7db54daec3341344a50920fc0e4', '88', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '1148.00', '0.00', '600.00', '0.00', '159.00', '1748.00', '1748.00', 'かんたん銀行決済(PayPal)', NULL, '2020-08-04 03:32:45', '2020-08-04 09:48:42', NULL, NULL, 'JPY', NULL, '', '10', '0', 8, 'order', NULL),
(89, 1, NULL, 13, 1, 2, 14, 10, 'e64ae91dd60c24c32ddd3edf7ba8dcd1ee408569', '89', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '1148.00', '0.00', '600.00', '0.00', '159.00', '1748.00', '1748.00', 'PayPal決済', NULL, '2020-08-05 09:06:03', '2020-08-06 03:52:59', '2020-08-05 11:47:07', '2020-08-05 11:47:07', 'JPY', NULL, '', '10', '0', 6, 'order', NULL),
(90, NULL, NULL, 13, NULL, NULL, 14, 10, '23e02e008f3434bf8027f170b9854e4ca896c24a', '90', NULL, 'Test', 'Test', 'テスト', 'テスト', NULL, '05049pyj+test@gmail.com', '11122223333', '1000001', '千代田区千代田', '1-1-1', NULL, '5280.00', '0.00', '0.00', '0.00', '480.00', '5280.00', '5280.00', 'PayPal決済', NULL, '2020-08-05 10:20:48', '2020-08-05 10:47:04', NULL, NULL, 'JPY', NULL, NULL, '0', '0', 8, 'order', NULL),
(91, 1, NULL, 13, 1, 2, 14, 10, '1a1797d0ac2d83cc46b7bcad634b583a055a3833', '91', 'test', 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '4752.00', '10.00', '0.00', '0.00', '432.00', '4742.00', '4742.00', 'PayPal決済', NULL, '2020-08-05 11:48:04', '2020-08-06 10:41:39', '2020-08-06 10:27:55', '2020-08-06 10:27:55', 'JPY', NULL, '', '43', '10', 5, 'order', NULL),
(92, 4, NULL, 13, NULL, NULL, 5, 10, '866f2672deaee287cf6bd4ca45d5c1bffcd58827', NULL, NULL, 'Test2', 'Test', 'カナ', 'カナ', NULL, '05049pyj+1@gmail.com', '11122223333', '1000001', '千代田区千代田', '1-1-1', NULL, '7656.00', '0.00', '0.00', '0.00', '696.00', '7656.00', '7656.00', 'クレジット決済', NULL, '2020-08-05 14:34:22', '2020-08-05 14:34:22', NULL, NULL, 'JPY', NULL, NULL, '72', '0', 8, 'order', NULL),
(93, 4, NULL, 13, NULL, NULL, 5, 10, 'e623bef20812404c0d7b35b98580ec1ba98a3019', '93', NULL, 'Test2', 'Test', 'カナ', 'カナ', NULL, '05049pyj+1@gmail.com', '11122223333', '1000001', '千代田区千代田', '1-1-1', NULL, '6380.00', '638.00', '0.00', '0.00', '580.00', '5742.00', '5742.00', 'クレジット決済', NULL, '2020-08-05 14:34:47', '2020-08-05 14:35:06', NULL, NULL, 'JPY', NULL, '***********************************************\n　クーポン情報                                 \n***********************************************\n\nクーポンコード: MARITIME10 初回10％割引オファー\n', '54', '0', 8, 'order', NULL),
(94, 1, NULL, 13, 1, 2, 14, 10, '74f9c78d68352de7a154f0899948837e1283e0e3', '94', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '4752.00', '15.00', '0.00', '0.00', '432.00', '4737.00', '4737.00', 'PayPal決済', NULL, '2020-08-06 11:22:49', '2020-08-06 11:28:10', NULL, NULL, 'JPY', NULL, '', '43', '15', 8, 'order', NULL),
(95, 1, NULL, 13, 1, 2, 5, 10, '2ec5ebbaac717fbecbc6240f6b0b734ee23eaa9f', '95', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '9504.00', '0.00', '0.00', '0.00', '864.00', '9504.00', '9504.00', 'クレジット決済', NULL, '2020-08-06 12:04:55', '2020-08-06 12:06:17', NULL, NULL, 'JPY', NULL, '', '86', '0', 8, 'order', NULL),
(96, 1, NULL, 13, 1, 2, 14, 10, '4f9f0e02335dbb9697613cadc0064844fbd89409', '96', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '4752.00', '475.00', '0.00', '0.00', '432.00', '4277.00', '4277.00', 'PayPal決済', NULL, '2020-08-06 12:06:37', '2020-08-10 04:04:40', NULL, NULL, 'JPY', NULL, '***********************************************\n　クーポン情報                                 \n***********************************************\n\nクーポンコード: MARITIME10 初回10％割引オファー\n', '38', '0', 8, 'order', NULL),
(97, 4, NULL, 13, NULL, NULL, 5, 10, 'c9cacd6fd6ff43a0bb7cef84c5d997e8b165c058', '97', NULL, 'Test2', 'Test', 'カナ', 'カナ', NULL, '05049pyj+1@gmail.com', '11122223333', '1000001', '千代田区千代田', '1-1-1', NULL, '5280.00', '528.00', '0.00', '0.00', '480.00', '4752.00', '4752.00', 'クレジット決済', NULL, '2020-08-06 12:32:08', '2020-08-06 12:33:37', NULL, NULL, 'JPY', NULL, '***********************************************\n　クーポン情報                                 \n***********************************************\n\nクーポンコード: MARITIME10 初回10％割引オファー\n', '43', '0', 8, 'order', NULL),
(98, 1, NULL, 13, 1, 2, 5, 10, 'bb2cbd3823ba87daaf92708bb23b683071ecffb0', NULL, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '3444.00', '0.00', '600.00', '0.00', '367.00', '4044.00', '4044.00', 'クレジット決済', NULL, '2020-08-13 07:55:09', '2020-08-13 07:55:09', NULL, NULL, 'JPY', NULL, NULL, '30', '0', 8, 'order', NULL),
(99, 1, NULL, 13, 1, 2, 5, 10, '9b56ccbe678e7d2f4dac4d0c80625a4f2be9cbe3', NULL, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '8196.00', '0.00', '0.00', '0.00', '744.00', '8196.00', '8196.00', 'クレジット決済', NULL, '2020-08-17 10:27:49', '2020-08-17 10:27:49', NULL, NULL, 'JPY', NULL, NULL, '73', '0', 8, 'order', NULL),
(100, NULL, NULL, 13, NULL, NULL, 5, 10, 'd6a4296a18606ba92e6c4e409941ee09863ab35e', '100', NULL, 'Test', 'Test', 'テスト', 'テスト', NULL, 'test@test.com', '11122223333', '1000000', '千代田区', '1-1-1', NULL, '5280.00', '0.00', '0.00', '0.00', '480.00', '5280.00', '5280.00', 'クレジット決済', NULL, '2020-08-18 14:38:23', '2020-08-18 14:43:45', NULL, NULL, 'JPY', NULL, NULL, '0', '0', 8, 'order', NULL),
(101, NULL, NULL, 13, NULL, NULL, 5, 10, '2686b1a4dcfa9af5c0298afa4fe9ad7b038dd849', '101', NULL, 'Test', 'Test', 'テスト', 'テスト', NULL, 'test@test.com', '11122223333', '1000000', '千代田区', '1-1-1', NULL, '5280.00', '0.00', '0.00', '0.00', '480.00', '5280.00', '5280.00', 'クレジット決済', NULL, '2020-08-18 14:44:50', '2020-08-18 14:45:15', NULL, NULL, 'JPY', NULL, NULL, '0', '0', 8, 'order', NULL),
(102, 1, NULL, 13, 1, 2, 14, 10, '444207a7c0bef913ebf350b0b9dc0bb0e0c3c133', '102', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '37026.00', '0.00', '0.00', '0.00', '3366.00', '37026.00', '37026.00', 'PayPal決済', NULL, '2020-08-20 08:48:38', '2020-08-20 08:56:02', NULL, NULL, 'JPY', NULL, NULL, '336', '0', 8, 'order', NULL),
(103, 1, NULL, 13, 1, 2, 5, 10, '883f91495174b6aa300ab1e8581286e8a1edc542', '103', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '18513.00', '0.00', '0.00', '0.00', '1683.00', '18513.00', '18513.00', 'クレジット決済', NULL, '2020-08-20 23:14:54', '2020-08-20 23:19:22', NULL, NULL, 'JPY', NULL, NULL, '168', '0', 8, 'order', NULL),
(104, 1, NULL, 13, 1, 2, 5, 10, '457d70eaeb9f0a098296035c18a40fd900e8b4a4', NULL, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '28017.00', '0.00', '0.00', '0.00', '2547.00', '28017.00', '28017.00', 'クレジット決済', NULL, '2020-08-25 00:26:39', '2020-08-25 00:26:39', NULL, NULL, 'JPY', NULL, NULL, '254', '0', 8, 'order', NULL),
(105, 1, NULL, 13, 1, 2, 15, 10, '2f5430cbd02a90546c32afedd491258910b68603', '105', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '9504.00', '0.00', '0.00', '0.00', '864.00', '9504.00', '9504.00', 'かんたん銀行決済(PayPal)', NULL, '2020-08-28 01:56:54', '2020-08-28 02:46:55', NULL, NULL, 'JPY', NULL, NULL, '86', '0', 8, 'order', NULL),
(106, 1, NULL, 13, 1, 2, 3, 10, '37735f420ee0935ac95e48902342ad994b71f77a', '106', 'テスト', 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '9504.00', '0.00', '0.00', '0.00', '864.00', '9504.00', '9504.00', '銀行振込', NULL, '2020-08-30 07:21:49', '2020-09-17 03:52:57', '2020-08-30 07:23:43', NULL, 'JPY', NULL, NULL, '86', '0', 3, 'order', NULL),
(107, 1, NULL, 13, 1, 2, 3, 10, '55f1e3e94a2f3881ce7e5c1b3266c1b0d85a88f8', '107', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '4752.00', '0.00', '0.00', '0.00', '432.00', '4752.00', '4752.00', '銀行振込', NULL, '2020-08-30 07:34:28', '2020-09-17 03:52:56', '2020-08-30 07:34:56', NULL, 'JPY', NULL, NULL, '43', '0', 3, 'order', NULL),
(108, 1, NULL, 13, 1, 2, 3, 10, '4609f308f924321ff5d6344929bf4f519df6ddf4', '108', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '4752.00', '0.00', '0.00', '0.00', '432.00', '4752.00', '4752.00', '銀行振込', NULL, '2020-08-30 07:48:34', '2020-09-02 04:12:24', NULL, NULL, 'JPY', NULL, NULL, '43', '0', 8, 'order', NULL),
(109, NULL, NULL, 13, NULL, NULL, 3, 10, 'a8cf3cabe148302bd249cacd71bcbcd792177da9', '109', NULL, 'Test', 'Test', 'テスト', 'テスト', NULL, '05049pyj+test@gmail.com', '11122223333', '1000001', '千代田区千代田', '1-1-1', NULL, '1276.00', '0.00', '600.00', '0.00', '171.00', '1876.00', '1876.00', '銀行振込', NULL, '2020-09-01 11:23:54', '2020-09-01 11:25:40', NULL, NULL, 'JPY', NULL, NULL, '0', '0', 8, 'order', NULL),
(110, 1, NULL, 13, 1, 2, 5, 10, '8f664ac2be8a268dd1eee3706867a97f1e7661ae', NULL, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '9504.00', '0.00', '0.00', '0.00', '864.00', '9504.00', '9504.00', 'クレジット決済', NULL, '2020-09-02 04:14:31', '2020-09-02 04:14:31', NULL, NULL, 'JPY', NULL, NULL, '86', '0', 8, 'order', NULL),
(111, 1, NULL, 13, 1, 2, 3, 10, '0e5c0ec5fa8e59f07d085fdd4f4a2b0822d98e2c', '111', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '9504.00', '0.00', '0.00', '0.00', '864.00', '9504.00', '9504.00', '銀行振込', NULL, '2020-09-02 05:09:55', '2020-09-02 05:11:08', NULL, NULL, 'JPY', NULL, NULL, '86', '0', 8, 'order', NULL),
(112, 1, NULL, 13, 1, 2, 5, 10, 'df3a9d1061e0ec505a18040c7254bb8eaeec407f', '112', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '4752.00', '0.00', '0.00', '0.00', '432.00', '4752.00', '4752.00', 'クレジット決済', NULL, '2020-09-02 06:34:05', '2020-09-02 07:50:26', NULL, NULL, 'JPY', NULL, NULL, '43', '0', 8, 'order', NULL),
(113, 1, NULL, 13, 1, 2, 3, 10, 'fa81f842061ce3e16287825845b3b4b8e94d1a5a', '113', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '4752.00', '0.00', '0.00', '0.00', '432.00', '4752.00', '4752.00', '銀行振込', NULL, '2020-09-03 07:46:56', '2020-09-17 03:52:57', '2020-09-03 07:48:28', NULL, 'JPY', NULL, NULL, '43', '0', 3, 'order', NULL),
(114, 1, NULL, 13, 1, 2, 5, 10, '021c7a68c626464460e4d821bd31bf5b31a8fd68', NULL, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '47025.00', '0.00', '0.00', '0.00', '4275.00', '47025.00', '47025.00', 'クレジット決済', NULL, '2020-09-04 01:00:53', '2020-09-04 01:00:53', NULL, NULL, 'JPY', NULL, NULL, '428', '0', 8, 'order', NULL),
(115, 1, NULL, 13, 1, 2, 3, 10, '379324704e6716a5e216f030d9253e47c8f6e694', '115', 'test frederic', 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '47025.00', '543.00', '0.00', '0.00', '4275.00', '46482.00', '46482.00', '銀行振込', NULL, '2020-09-10 00:22:18', '2020-09-17 03:54:23', '2020-09-10 00:23:26', NULL, 'JPY', NULL, '***********************************************\n　クーポン情報                                 \n***********************************************\n\nクーポンコード: MARITIME10 初回10％割引オファー\n', '423', '543', 3, 'order', NULL),
(116, 1, NULL, 13, 1, 2, 5, 10, '198854f46e6d349dc6a5158f709cec099b9573f2', '116', NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '4752.00', '0.00', '0.00', '0.00', '432.00', '4752.00', '4752.00', 'クレジット決済', NULL, '2020-09-10 23:51:39', '2020-09-10 23:51:52', NULL, NULL, 'JPY', NULL, NULL, '43', '0', 8, 'order', NULL),
(117, 1, NULL, 13, 1, 2, 3, 10, '0af817dacbbbfac5f0bdbaf147fe0a1070efb405', '117', 'test', 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '4752.00', '12.00', '0.00', '0.00', '432.00', '4740.00', '4740.00', '銀行振込', NULL, '2020-09-17 03:48:51', '2020-09-17 05:22:04', '2020-09-17 04:04:07', NULL, 'JPY', NULL, '***********************************************\n　クーポン情報                                 \n***********************************************\n\nクーポンコード: MARITIME10 初回10％割引オファー\n', '43', '12', 3, 'order', NULL),
(118, 1, NULL, 13, 1, 2, 3, 10, 'd97c8c6fc22364946c65b8e2a78b8a489d4bd6ff', '118', 'test', 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '4752.00', '485.00', '0.00', '0.00', '432.00', '4267.00', '4267.00', '銀行振込', NULL, '2020-09-17 05:21:01', '2020-09-17 05:25:20', '2020-09-17 05:22:31', NULL, 'JPY', NULL, '***********************************************\n　{{ \'クーポン情報\'|trans }}                                 \n***********************************************\n\n{{ \'クーポンコード\'|trans }}: MARITIME10 初回10％割引オファー\n', '38', '10', 3, 'order', NULL),
(119, 1, NULL, 13, 1, 2, 3, 10, 'aa86120289f30314eb47e2ea14b7dff3dcd23ec1', '119', 'fggddg', 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123建物', '1985-04-05 15:00:00', '9504.00', '960.00', '0.00', '0.00', '864.00', '8544.00', '8544.00', '銀行振込', NULL, '2020-09-17 06:19:50', '2020-09-17 06:20:57', '2020-09-17 06:20:57', NULL, 'JPY', NULL, '***********************************************\n　Coupon information                                 \n***********************************************\n\nCoupon Code: MARITIME10 10% discount on your first order\n', '76', '10', 1, 'order', NULL);
INSERT INTO `dtb_order` (`id`, `customer_id`, `country_id`, `pref_id`, `sex_id`, `job_id`, `payment_id`, `device_type_id`, `pre_order_id`, `order_no`, `message`, `name01`, `name02`, `kana01`, `kana02`, `company_name`, `email`, `phone_number`, `postal_code`, `addr01`, `addr02`, `birth`, `subtotal`, `discount`, `delivery_fee_total`, `charge`, `tax`, `total`, `payment_total`, `payment_method`, `note`, `create_date`, `update_date`, `order_date`, `payment_date`, `currency_code`, `complete_message`, `complete_mail_message`, `add_point`, `use_point`, `order_status_id`, `discriminator_type`, `gmo_payment_gateway_payment_status`) VALUES
(120, NULL, NULL, 1, NULL, NULL, 5, 10, '152c5584dcc11d4f41a32c915105948e83784d57', '120', NULL, 'Test!', 'te', 'テス', 'テス', NULL, 'test@test.com', '+32123456789', '1000', 'City', 'fdgdf', NULL, '5280.00', '0.00', '0.00', '0.00', '480.00', '5280.00', '5280.00', 'クレジット決済', NULL, '2020-09-28 02:14:20', '2020-10-01 09:20:10', NULL, NULL, 'JPY', NULL, NULL, '0', '0', 8, 'order', NULL),
(121, NULL, NULL, 13, NULL, NULL, 5, 10, '45c971fe6da159cb844e467a8bad282d48f08548', NULL, NULL, 'Frederic', 'Vervaecke', 'テスト', 'テスト', NULL, 'djfre2000@hotmail.com', '0123456789', '1570077', '世田谷区鎌田', '123', NULL, '5280.00', '0.00', '0.00', '0.00', '480.00', '5280.00', '5280.00', 'クレジット決済', NULL, '2020-10-01 09:37:05', '2020-10-01 09:37:05', NULL, NULL, 'JPY', NULL, NULL, '0', '0', 8, 'order', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `dtb_order_item`
--

CREATE TABLE `dtb_order_item` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED DEFAULT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `product_class_id` int(10) UNSIGNED DEFAULT NULL,
  `shipping_id` int(10) UNSIGNED DEFAULT NULL,
  `rounding_type_id` smallint(5) UNSIGNED DEFAULT NULL,
  `tax_type_id` smallint(5) UNSIGNED DEFAULT NULL,
  `tax_display_type_id` smallint(5) UNSIGNED DEFAULT NULL,
  `order_item_type_id` smallint(5) UNSIGNED DEFAULT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_code` varchar(255) DEFAULT NULL,
  `class_name1` varchar(255) DEFAULT NULL,
  `class_name2` varchar(255) DEFAULT NULL,
  `class_category_name1` varchar(255) DEFAULT NULL,
  `class_category_name2` varchar(255) DEFAULT NULL,
  `price` decimal(12,2) NOT NULL DEFAULT '0.00',
  `quantity` decimal(10,0) NOT NULL DEFAULT '0',
  `tax` decimal(10,0) NOT NULL DEFAULT '0',
  `tax_rate` decimal(10,0) UNSIGNED NOT NULL DEFAULT '0',
  `tax_adjust` decimal(10,0) UNSIGNED NOT NULL DEFAULT '0',
  `tax_rule_id` smallint(5) UNSIGNED DEFAULT NULL,
  `currency_code` varchar(255) DEFAULT NULL,
  `processor_name` varchar(255) DEFAULT NULL,
  `point_rate` decimal(10,0) UNSIGNED DEFAULT NULL,
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtb_order_item`
--

INSERT INTO `dtb_order_item` (`id`, `order_id`, `product_id`, `product_class_id`, `shipping_id`, `rounding_type_id`, `tax_type_id`, `tax_display_type_id`, `order_item_type_id`, `product_name`, `product_code`, `class_name1`, `class_name2`, `class_category_name1`, `class_category_name2`, `price`, `quantity`, `tax`, `tax_rate`, `tax_adjust`, `tax_rule_id`, `currency_code`, `processor_name`, `point_rate`, `discriminator_type`) VALUES
(1, 1, 4, 23, 1, 1, 1, 1, 1, 'Maritime CBD Water', 'cbd-water-500', NULL, NULL, NULL, NULL, '5000.00', '1', '400', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(3, 1, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(5, 1, NULL, NULL, 1, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '1000.00', '1', '74', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(6, 2, 2, 11, 2, 1, 1, 1, 1, 'MARITIME CBD 3000', 'cbd-3000', NULL, NULL, NULL, NULL, '9999.00', '1', '800', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(7, 2, 4, 23, 2, 1, 1, 1, 1, 'Maritime CBD Water', 'cbd-water-500', NULL, NULL, NULL, NULL, '5000.00', '1', '400', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(8, 2, NULL, NULL, 2, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '1000.00', '1', '74', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(9, 2, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(10, 3, 14, 37, 3, 1, 1, 1, 1, 'MARITIME CBD 3000', 'cbd-3000-s', '販売種別', NULL, '定期購買', NULL, '8000.00', '1', '800', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(12, 3, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(14, 3, NULL, NULL, 3, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(39, 11, 14, 37, 11, 1, 1, 1, 1, 'MARITIME CBDオイル 30ml (10%)', 'cbd-3000-s', '販売種別', NULL, '定期購買', NULL, '8000.00', '2', '800', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(40, 11, NULL, NULL, 11, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(41, 11, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(42, 12, 14, 37, 12, 1, 1, 1, 1, 'MARITIME CBDオイル 30ml (10%)', 'cbd-3000-s', '販売種別', NULL, '定期購買', NULL, '8000.00', '1', '800', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(44, 12, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(45, 12, NULL, NULL, 12, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(46, 13, 14, 37, 13, 1, 1, 1, 1, 'MARITIME CBDオイル 30ml (10%)', 'cbd-3000-s', '販売種別', NULL, '定期購買', NULL, '8000.00', '1', '800', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(48, 13, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(51, 14, 4, 38, 14, 1, 1, 1, 1, 'MARITIME CBD Water 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '2000.00', '1', '200', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(53, 14, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(55, 13, NULL, NULL, 13, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '1000.00', '1', '91', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(56, 14, NULL, NULL, 14, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '1000.00', '1', '91', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(57, 15, 14, 37, 15, 1, 1, 1, 1, 'MARITIME CBDオイル 30ml (10%)', 'cbd-3000-s', '販売種別', NULL, '定期購買', NULL, '8000.00', '1', '800', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(59, 15, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(60, 15, NULL, NULL, 15, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '1000.00', '1', '91', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(61, 16, 4, 38, 16, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '2000.00', '1', '200', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(62, 16, NULL, NULL, 16, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '1000.00', '1', '91', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(63, 16, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(64, 17, 14, 37, 17, 1, 1, 1, 1, 'MARITIME CBDオイル 30ml (10%)', 'cbd-3000-s', '販売種別', NULL, '定期購買', NULL, '8000.00', '1', '800', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(65, 17, NULL, NULL, 17, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '1000.00', '1', '91', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(66, 17, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(67, 18, 14, 36, 18, 1, 1, 1, 1, 'MARITIME CBDオイル 30ml (10%)', 'cbd-3000', '販売種別', NULL, '通常購入', NULL, '12000.00', '1', '1200', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(69, 18, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(75, 18, NULL, NULL, 18, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '1000.00', '1', '91', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(76, 19, 14, 37, 19, 1, 1, 1, 1, 'MARITIME CBDオイル 30ml (10%)', 'cbd-3000-s', '販売種別', NULL, '定期購買', NULL, '8000.00', '1', '800', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(78, 19, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(97, 19, NULL, NULL, 19, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '1000.00', '1', '91', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(98, 19, NULL, NULL, NULL, NULL, 2, 2, 4, '初回10％割引オファー', NULL, NULL, NULL, NULL, NULL, '-880.00', '1', '0', '0', '0', NULL, 'JPY', 'Plugin\\Coupon4\\Service\\PurchaseFlow\\Processor\\CouponProcessor', NULL, 'orderitem'),
(99, 20, 4, 38, 20, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '2000.00', '1', '200', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(100, 20, NULL, NULL, 20, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '1000.00', '1', '91', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(101, 20, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(102, 21, 4, 38, 21, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1015.00', '1', '102', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(104, 21, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(107, 21, NULL, NULL, 21, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '1000.00', '1', '91', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(108, 22, 14, 37, 22, 1, 1, 1, 1, 'MARITIME CBDオイル 30ml (10%)', 'cbd-3000-s', '販売種別', NULL, '定期購買', NULL, '8000.00', '1', '800', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(110, 22, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(113, 22, NULL, NULL, 22, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '1000.00', '1', '91', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(114, 23, 14, 37, 23, 1, 1, 1, 1, 'MARITIME CBDオイル 30ml (10%)', 'cbd-3000-s', '販売種別', NULL, '定期購買', NULL, '28500.00', '1', '2850', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(116, 23, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(126, 23, NULL, NULL, 23, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '1000.00', '1', '91', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(127, 23, NULL, NULL, NULL, NULL, 2, 2, 4, '初回10％割引オファー', NULL, NULL, NULL, NULL, NULL, '-3135.00', '1', '0', '0', '0', NULL, 'JPY', 'Plugin\\Coupon4\\Service\\PurchaseFlow\\Processor\\CouponProcessor', NULL, 'orderitem'),
(128, 24, 14, 37, 24, 1, 1, 1, 1, 'MARITIME CBDオイル 30ml (10%)', 'cbd-3000-s', '販売種別', NULL, '定期購買', NULL, '28500.00', '1', '2280', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(130, 24, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(135, 24, NULL, NULL, 24, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '1', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(136, 25, 14, 37, 25, 1, 1, 1, 1, 'MARITIME CBDオイル 30ml (10%)', 'cbd-3000-s', '販売種別', NULL, '定期購買', NULL, '28500.00', '2', '2280', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(138, 25, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(139, 26, 9, 44, 26, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4800.00', '1', '384', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(140, 26, 8, 54, 26, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (2%)', 'cbd-200', '販売種別', NULL, '通常購入', NULL, '7200.00', '1', '576', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(141, 26, NULL, NULL, 26, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '1', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(142, 26, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(143, 27, 9, 44, 27, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4800.00', '3', '384', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(144, 27, NULL, NULL, 27, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '1', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(145, 27, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(146, 28, 9, 44, 28, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4800.00', '2', '384', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(147, 28, NULL, NULL, 28, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '1', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(148, 28, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(149, 29, 9, 44, 29, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4800.00', '3', '384', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(150, 29, NULL, NULL, 29, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '1', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(151, 29, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(152, 30, 9, 44, 30, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4800.00', '2', '384', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(153, 30, NULL, NULL, 30, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '1', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(154, 30, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(155, 31, 9, 44, 31, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4800.00', '3', '384', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(156, 31, NULL, NULL, 31, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '1', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(157, 31, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(158, 32, 9, 44, 32, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4800.00', '4', '384', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(160, 32, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(161, 32, NULL, NULL, 32, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '0', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(162, 33, 9, 44, 33, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4800.00', '2', '384', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(163, 33, NULL, NULL, 33, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '2', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(164, 33, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(165, 34, 9, 44, 34, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4800.00', '2', '384', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(166, 34, 4, 38, 34, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1160.00', '2', '93', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(167, 34, NULL, NULL, 34, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '0', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(168, 34, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(169, 35, 4, 39, 35, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500-s', '販売種別', NULL, '定期購買', NULL, '870.00', '2', '70', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(170, 35, NULL, NULL, 35, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '2', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(171, 35, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(172, 36, 4, 38, 36, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1160.00', '4', '93', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(173, 36, NULL, NULL, 36, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '0', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(174, 36, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(175, 25, NULL, NULL, 25, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '2', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(176, 37, 9, 44, 37, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4200.00', '1', '336', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(177, 37, NULL, NULL, 37, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '1', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(178, 37, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(179, 38, 9, 44, 38, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4200.00', '2', '336', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(180, 38, NULL, NULL, 38, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '2', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(181, 38, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(182, 39, 9, 44, 39, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4200.00', '6', '336', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(183, 39, NULL, NULL, 39, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '0', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(184, 39, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(185, 40, 9, 44, 40, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4200.00', '1', '336', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(187, 40, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(190, 40, NULL, NULL, 40, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '1', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(191, 41, 9, 44, 41, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4200.00', '1', '336', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(192, 41, 4, 38, 41, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1015.00', '1', '81', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(193, 41, NULL, NULL, 41, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '2', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(194, 41, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(195, 42, 9, 44, 42, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4200.00', '2', '336', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(196, 42, 4, 38, 42, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1015.00', '1', '81', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(197, 42, NULL, NULL, 42, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '0', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(198, 42, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(199, 43, 9, 44, 43, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4200.00', '2', '336', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(201, 43, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(202, 43, NULL, NULL, 43, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '2', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(203, 44, 4, 39, 44, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500-s', '販売種別', NULL, '定期購買', NULL, '870.00', '1', '70', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(204, 44, NULL, NULL, 44, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '1', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(205, 44, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(206, 45, 4, 38, 45, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1015.00', '1', '81', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(207, 45, 9, 44, 45, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4200.00', '2', '336', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(208, 45, NULL, NULL, 45, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '0', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(209, 45, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(210, 46, 4, 38, 46, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1015.00', '2', '81', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(211, 46, NULL, NULL, 46, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '2192', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(212, 46, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(213, 47, 4, 38, 47, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1015.00', '3', '81', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(214, 47, NULL, NULL, 47, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '0', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(215, 47, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(216, 48, 4, 38, 48, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1015.00', '2', '81', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(218, 48, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(219, 49, 4, 38, 49, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1015.00', '2', '81', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(220, 49, NULL, NULL, 49, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '2', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(221, 49, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(222, 50, 15, 59, 50, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml ６本セット', 'cbd-water-500-set', '販売種別', NULL, '通常購入', NULL, '6090.00', '1', '487', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(223, 50, NULL, NULL, 50, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '0', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(224, 50, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(225, 48, NULL, NULL, 48, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '2', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(226, 51, 4, 38, 51, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1015.00', '3', '81', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(227, 51, NULL, NULL, 51, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '0', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(228, 51, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(229, 52, 4, 38, 52, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1015.00', '2', '81', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(231, 52, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(232, 52, NULL, NULL, 52, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '2', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(233, 53, 4, 38, 53, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1015.00', '3', '81', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(234, 53, NULL, NULL, 53, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '0', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(235, 53, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(236, 54, 4, 38, 54, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1015.00', '6', '81', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(237, 54, 15, 59, 54, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml ６本セット', 'cbd-water-500-set', '販売種別', NULL, '通常購入', NULL, '6090.00', '1', '487', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(238, 54, NULL, NULL, 54, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '0', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(239, 54, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(240, 55, 4, 38, 55, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1015.00', '3', '81', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(242, 55, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(243, 55, NULL, NULL, 55, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '1', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(244, 56, 4, 38, 56, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1015.00', '4', '81', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(245, 56, NULL, NULL, 56, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '0', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(246, 56, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(247, 57, 4, 38, 57, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1015.00', '3', '81', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(248, 57, 15, 59, 57, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml ６本セット', 'cbd-water-500-set', '販売種別', NULL, '通常購入', NULL, '6090.00', '1', '487', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(249, 57, NULL, NULL, 57, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '0', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(250, 57, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(251, 58, 9, 45, 58, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100-s', '販売種別', NULL, '定期購買', NULL, '3600.00', '1', '288', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(252, 58, NULL, NULL, 58, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '1', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(253, 58, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(254, 59, 8, 55, 59, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (2%)', 'cbd-200-s', '販売種別', NULL, '定期購買', NULL, '5400.00', '1', '432', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(255, 59, NULL, NULL, 59, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '0', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(256, 59, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(257, 60, 4, 38, 60, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1015.00', '3', '81', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(258, 60, NULL, NULL, 60, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '1', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(259, 60, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(260, 61, 4, 38, 61, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1015.00', '4', '81', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(262, 61, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(307, 61, NULL, NULL, 61, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '0', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(308, 62, 4, 38, 62, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1015.00', '1', '81', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(309, 62, NULL, NULL, 62, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '1', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(310, 62, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(311, 63, 4, 38, 63, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1160.00', '4', '93', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(313, 63, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(316, 63, NULL, NULL, 63, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '0', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(317, 64, 15, 59, 64, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml ６本セット', 'cbd-water-500-set', '販売種別', NULL, '通常購入', NULL, '6090.00', '1', '487', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(318, 64, 4, 38, 64, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1015.00', '6', '81', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(320, 64, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(328, 64, NULL, NULL, 64, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '0', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(329, 65, 4, 38, 65, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1015.00', '1', '81', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(331, 65, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(338, 65, NULL, NULL, 65, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '1', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(339, 66, 4, 38, 66, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1044.00', '1', '84', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(341, 66, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(346, 66, NULL, NULL, 66, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '1', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(348, 67, 4, 38, 67, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1044.00', '1', '84', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(350, 67, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(352, 67, NULL, NULL, 67, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '1', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(353, 68, 9, 44, 68, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4320.00', '1', '346', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(355, 68, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(356, 68, NULL, NULL, 68, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '0', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(358, 69, 9, 44, 69, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4320.00', '1', '346', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(360, 69, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(361, 69, NULL, NULL, 69, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '800.00', '0', '59', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(363, 70, 9, 44, 70, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4320.00', '1', '346', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(365, 70, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(367, 70, NULL, NULL, 70, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '44', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(369, 71, 9, 44, 71, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4320.00', '1', '346', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(371, 71, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(373, 71, NULL, NULL, 71, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '44', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(374, 72, 10, 48, 72, 1, 1, 1, 1, 'MARITIME CBDオイル 30ml (5%)', 'cbd-1500', '販売種別', NULL, '通常購入', NULL, '29250.00', '1', '2340', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(376, 72, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(378, 72, NULL, NULL, 72, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '44', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(379, 73, 9, 44, 73, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4320.00', '1', '346', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(381, 73, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(394, 73, NULL, NULL, 73, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '44', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(395, 74, 9, 44, 74, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4320.00', '1', '346', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(397, 74, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(398, 74, NULL, NULL, 74, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '44', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(399, 75, 9, 44, 75, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4320.00', '1', '346', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(400, 75, NULL, NULL, 75, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '44', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(401, 75, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(402, 76, 4, 38, 76, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1044.00', '1', '84', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(404, 76, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(406, 76, NULL, NULL, 76, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '1', '44', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(407, 77, 4, 38, 77, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1044.00', '1', '84', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(408, 77, 15, 59, 77, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml ６本セット', 'cbd-water-500-set', '販売種別', NULL, '通常購入', NULL, '6264.00', '1', '501', '8', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(410, 77, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '8', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(420, 77, NULL, NULL, 77, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(421, 78, 4, 38, 78, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1044.00', '1', '104', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(422, 78, 15, 59, 78, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml ６本セット', 'cbd-water-500-set', '販売種別', NULL, '通常購入', NULL, '6264.00', '1', '626', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(424, 78, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(427, 78, NULL, NULL, 78, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(428, 79, 4, 38, 79, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1044.00', '1', '104', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(430, 79, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(438, 79, NULL, NULL, 79, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '1', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(439, 80, 4, 38, 80, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1160.00', '1', '116', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(441, 80, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(447, 80, NULL, NULL, 80, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '1', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(448, 81, 4, 38, 81, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1044.00', '1', '104', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(450, 81, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(451, 81, NULL, NULL, 81, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '1', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(452, 82, 4, 38, 82, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1160.00', '2', '116', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(454, 82, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(456, 82, NULL, NULL, 82, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '1', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(457, 83, 4, 38, 83, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1160.00', '1', '104', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(459, 83, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(461, 83, NULL, NULL, 83, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '1', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(462, 84, 4, 38, 84, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1160.00', '1', '116', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(464, 84, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(466, 84, NULL, NULL, 84, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '1', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(467, 85, 11, 52, 85, 1, 1, 1, 1, 'MARITIME CBDオイル 30ml (3%)', 'cbd-900', '販売種別', NULL, '通常購入', NULL, '18720.00', '1', '1872', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(468, 85, NULL, NULL, 85, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(469, 85, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(470, 86, 9, 44, 86, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4320.00', '1', '432', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(472, 86, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(478, 86, NULL, NULL, 86, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(479, 87, 4, 38, 87, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1044.00', '1', '104', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(480, 87, 9, 44, 87, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4320.00', '1', '432', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(482, 87, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(485, 87, NULL, NULL, 87, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem');
INSERT INTO `dtb_order_item` (`id`, `order_id`, `product_id`, `product_class_id`, `shipping_id`, `rounding_type_id`, `tax_type_id`, `tax_display_type_id`, `order_item_type_id`, `product_name`, `product_code`, `class_name1`, `class_name2`, `class_category_name1`, `class_category_name2`, `price`, `quantity`, `tax`, `tax_rate`, `tax_adjust`, `tax_rule_id`, `currency_code`, `processor_name`, `point_rate`, `discriminator_type`) VALUES
(487, 88, 4, 38, 88, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1044.00', '1', '104', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(489, 88, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(529, 88, NULL, NULL, 88, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '1', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(530, 89, 4, 38, 89, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1044.00', '1', '104', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(532, 89, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(536, 90, 9, 44, 90, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4800.00', '1', '480', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(538, 90, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(547, 90, NULL, NULL, 90, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(557, 89, NULL, NULL, 89, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '1', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(558, 91, 9, 44, 91, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4320.00', '1', '432', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(560, 91, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(564, 92, 4, 38, 92, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1160.00', '6', '116', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(565, 92, NULL, NULL, 92, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(566, 92, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(567, 93, 4, 38, 93, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1160.00', '5', '116', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(569, 93, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(572, 93, NULL, NULL, 93, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(573, 93, NULL, NULL, NULL, NULL, 2, 2, 4, '初回10％割引オファー', NULL, NULL, NULL, NULL, NULL, '-638.00', '1', '0', '0', '0', NULL, 'JPY', 'Plugin\\Coupon4\\Service\\PurchaseFlow\\Processor\\CouponProcessor', NULL, 'orderitem'),
(589, 91, NULL, NULL, 91, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(591, 91, NULL, NULL, NULL, NULL, 2, 2, 6, 'ポイント', NULL, NULL, NULL, NULL, NULL, '-10.00', '1', '0', '0', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PointProcessor', NULL, 'orderitem'),
(592, 94, 9, 44, 94, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4320.00', '1', '432', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(594, 94, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(607, 94, NULL, NULL, 94, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(608, 94, NULL, NULL, NULL, NULL, 2, 2, 6, 'ポイント', NULL, NULL, NULL, NULL, NULL, '-15.00', '1', '0', '0', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PointProcessor', NULL, 'orderitem'),
(609, 95, 9, 44, 95, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4320.00', '2', '432', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(611, 95, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(617, 95, NULL, NULL, 95, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(618, 96, 9, 44, 96, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4320.00', '1', '432', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(620, 96, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(637, 97, 9, 44, 97, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4800.00', '1', '480', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(639, 97, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(642, 97, NULL, NULL, 97, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(643, 97, NULL, NULL, NULL, NULL, 2, 2, 4, '初回10％割引オファー', NULL, NULL, NULL, NULL, NULL, '-528.00', '1', '0', '0', '0', NULL, 'JPY', 'Plugin\\Coupon4\\Service\\PurchaseFlow\\Processor\\CouponProcessor', NULL, 'orderitem'),
(683, 96, NULL, NULL, 96, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(684, 96, NULL, NULL, NULL, NULL, 2, 2, 4, '初回10％割引オファー', NULL, NULL, NULL, NULL, NULL, '-475.00', '1', '0', '0', '0', NULL, 'JPY', 'Plugin\\Coupon4\\Service\\PurchaseFlow\\Processor\\CouponProcessor', NULL, 'orderitem'),
(685, 98, 4, 38, 98, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1044.00', '3', '104', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(686, 98, NULL, NULL, 98, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '1', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(687, 98, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(688, 99, 4, 38, 99, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1044.00', '3', '104', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(689, 99, 9, 44, 99, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4320.00', '1', '432', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(690, 99, NULL, NULL, 99, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(691, 99, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(692, 100, 9, 44, 100, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4800.00', '1', '480', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(694, 100, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(699, 100, NULL, NULL, 100, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(700, 101, 9, 44, 101, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4800.00', '1', '480', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(702, 101, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(703, 101, NULL, NULL, 101, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(704, 102, 13, 46, 102, 1, 1, 1, 1, 'MARITIME CBDオイル 30ml (1%)', 'cbd-300-30', '販売種別', NULL, '通常購入', NULL, '8190.00', '2', '819', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(705, 102, 9, 44, 102, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4320.00', '4', '432', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(707, 102, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(714, 102, NULL, NULL, 102, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(715, 103, 13, 46, 103, 1, 1, 1, 1, 'MARITIME CBDオイル 30ml (1%)', 'cbd-300-30', '販売種別', NULL, '通常購入', NULL, '8190.00', '1', '819', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(716, 103, 9, 44, 103, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4320.00', '2', '432', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(718, 103, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(719, 103, NULL, NULL, 103, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(720, 104, 7, 56, 104, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (3%)', 'cbd-300-10', '販売種別', NULL, '通常購入', NULL, '8640.00', '1', '864', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(721, 104, 13, 46, 104, 1, 1, 1, 1, 'MARITIME CBDオイル 30ml (1%)', 'cbd-300-30', '販売種別', NULL, '通常購入', NULL, '8190.00', '1', '819', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(722, 104, 9, 44, 104, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4320.00', '2', '432', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(723, 104, NULL, NULL, 104, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(724, 104, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(725, 105, 7, 56, 105, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (3%)', 'cbd-300-10', '販売種別', NULL, '通常購入', NULL, '8640.00', '1', '864', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(727, 105, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(732, 105, NULL, NULL, 105, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(733, 106, 7, 56, 106, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (3%)', 'cbd-300-10', '販売種別', NULL, '通常購入', NULL, '8640.00', '1', '864', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(735, 106, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(739, 106, NULL, NULL, 106, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(740, 107, 9, 44, 107, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4320.00', '1', '432', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(742, 107, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(746, 107, NULL, NULL, 107, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(747, 108, 9, 44, 108, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4320.00', '1', '432', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(749, 108, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(753, 109, 4, 38, 109, 1, 1, 1, 1, 'MARITIME CBD ウォーター 500ml', 'cbd-water-500', '販売種別', NULL, '通常購入', NULL, '1160.00', '1', '116', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(755, 109, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(759, 109, NULL, NULL, 109, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '1', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(764, 108, NULL, NULL, 108, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(765, 110, 9, 44, 110, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4320.00', '2', '432', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(766, 110, NULL, NULL, 110, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(767, 110, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(768, 111, 9, 44, 111, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4320.00', '2', '432', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(770, 111, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(774, 111, NULL, NULL, 111, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(775, 112, 9, 44, 112, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4320.00', '1', '432', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(777, 112, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(780, 112, NULL, NULL, 112, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(781, 113, 9, 44, 113, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4320.00', '1', '432', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(783, 113, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(787, 113, NULL, NULL, 113, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(788, 114, 14, 36, 114, 1, 1, 1, 1, 'MARITIME CBDオイル 30ml (10%)', 'cbd-3000', '販売種別', NULL, '通常購入', NULL, '42750.00', '1', '4275', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(789, 114, NULL, NULL, 114, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(790, 114, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(791, 115, 14, 36, 115, 1, 1, 1, 1, 'MARITIME CBDオイル 30ml (10%)', 'cbd-3000', '販売種別', NULL, '通常購入', NULL, '42750.00', '1', '4275', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(793, 115, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(809, 115, NULL, NULL, 115, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(812, 116, 9, 44, 116, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4320.00', '1', '432', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(814, 116, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(816, 116, NULL, NULL, 116, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(817, 117, 9, 44, 117, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4320.00', '1', '432', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(819, 117, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(824, 115, NULL, NULL, NULL, NULL, 2, 2, 6, 'ポイント', NULL, NULL, NULL, NULL, NULL, '-543.00', '1', '0', '0', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PointProcessor', NULL, 'orderitem'),
(840, 117, NULL, NULL, 117, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(844, 118, 9, 44, 118, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4320.00', '1', '432', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(846, 118, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(857, 117, NULL, NULL, NULL, NULL, 2, 2, 6, 'ポイント', NULL, NULL, NULL, NULL, NULL, '-12.00', '1', '0', '0', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PointProcessor', NULL, 'orderitem'),
(864, 118, NULL, NULL, 118, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(865, 118, NULL, NULL, NULL, NULL, 2, 2, 4, '初回10％割引オファー', NULL, NULL, NULL, NULL, NULL, '-475.00', '1', '0', '0', '0', NULL, 'JPY', 'Plugin\\Coupon4\\Service\\PurchaseFlow\\Processor\\CouponProcessor', NULL, 'orderitem'),
(867, 118, NULL, NULL, NULL, NULL, 2, 2, 6, 'ポイント', NULL, NULL, NULL, NULL, NULL, '-10.00', '1', '0', '0', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PointProcessor', NULL, 'orderitem'),
(868, 119, 9, 44, 119, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4320.00', '2', '432', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(870, 119, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(887, 119, NULL, NULL, 119, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(888, 119, NULL, NULL, NULL, NULL, 2, 2, 4, '初回10％割引オファー', NULL, NULL, NULL, NULL, NULL, '-950.00', '1', '0', '0', '0', NULL, 'JPY', 'Plugin\\Coupon4\\Service\\PurchaseFlow\\Processor\\CouponProcessor', NULL, 'orderitem'),
(889, 119, NULL, NULL, NULL, NULL, 2, 2, 6, 'ポイント', NULL, NULL, NULL, NULL, NULL, '-10.00', '1', '0', '0', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PointProcessor', NULL, 'orderitem'),
(890, 120, 9, 44, 120, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4800.00', '1', '480', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(892, 120, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem'),
(895, 120, NULL, NULL, 120, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(896, 121, 9, 44, 121, 1, 1, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'cbd-100', '販売種別', NULL, '通常購入', NULL, '4800.00', '1', '480', '10', '0', NULL, 'JPY', NULL, NULL, 'orderitem'),
(897, 121, NULL, NULL, 121, 1, 1, 2, 2, '送料', NULL, NULL, NULL, NULL, NULL, '600.00', '0', '55', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\DeliveryFeePreprocessor', NULL, 'orderitem'),
(898, 121, NULL, NULL, NULL, 1, 1, 2, 3, '手数料', NULL, NULL, NULL, NULL, NULL, '0.00', '1', '0', '10', '0', NULL, 'JPY', 'Eccube\\Service\\PurchaseFlow\\Processor\\PaymentChargePreprocessor', NULL, 'orderitem');

-- --------------------------------------------------------

--
-- Table structure for table `dtb_order_pdf`
--

CREATE TABLE `dtb_order_pdf` (
  `member_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `message1` varchar(255) DEFAULT NULL,
  `message2` varchar(255) DEFAULT NULL,
  `message3` varchar(255) DEFAULT NULL,
  `note1` varchar(255) DEFAULT NULL,
  `note2` varchar(255) DEFAULT NULL,
  `note3` varchar(255) DEFAULT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `dtb_page`
--

CREATE TABLE `dtb_page` (
  `id` int(10) UNSIGNED NOT NULL,
  `master_page_id` int(10) UNSIGNED DEFAULT NULL,
  `page_name` varchar(255) DEFAULT NULL,
  `url` varchar(255) NOT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `edit_type` smallint(5) UNSIGNED NOT NULL DEFAULT '1',
  `author` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `keyword` varchar(255) DEFAULT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `meta_robots` varchar(255) DEFAULT NULL,
  `meta_tags` varchar(4000) DEFAULT NULL,
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtb_page`
--

INSERT INTO `dtb_page` (`id`, `master_page_id`, `page_name`, `url`, `file_name`, `edit_type`, `author`, `description`, `keyword`, `create_date`, `update_date`, `meta_robots`, `meta_tags`, `discriminator_type`) VALUES
(0, NULL, 'プレビューデータ', 'preview', NULL, 1, 'MARITIME', NULL, NULL, '2017-03-07 10:14:52', '2017-03-07 10:14:52', NULL, NULL, 'page'),
(1, NULL, 'ホーム', 'homepage', 'index', 2, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。CBDオイルは1%から10%までの高濃度をご用意。コスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターもおすすめです。', 'MARITIME,マリタイム,マリタイムおすすめのCBDウォーター,カンナビノイド効果を今すぐ試そう,cbdオイル,cbdオイル 効果,cbdオイル おすすめ,cbdオイル 日本,cbdオイル 使い方,cbdオイル 使い方,cbdオイルとは,cbdオイル 口コミ,cbdオイル 通販', '2017-03-07 10:14:52', '2020-09-04 05:17:11', NULL, NULL, 'page'),
(2, NULL, '商品一覧', 'product_list', 'Product/list', 2, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの商品一覧です。', 'MARITIME,マリタイム,マリタイム商品一覧ページ,おすすめのCBDウォーター,カンナビノイド効果を今すぐ試そう,cbdオイル,cbdオイル 効果,cbdオイル おすすめ,cbdオイル 日本,cbdオイル 使い方,cbdオイル 使い方,cbdオイルとは,cbdオイル 口コミ,cbdオイル 通販', '2017-03-07 10:14:52', '2020-09-03 06:34:22', NULL, NULL, 'page'),
(3, NULL, '商品詳細', 'product_detail', 'Product/detail', 2, 'MARITIME', NULL, NULL, '2017-03-07 10:14:52', '2020-09-30 01:18:03', NULL, '<meta property=\"description\" content=\"{{ Product.name }}。日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの商品詳細です。\" />\r\n<meta property=\"keywords\" content=\"{{ Product.name }},MARITIME,マリタイム,マリタイム cbdオイル,カンナビノイド効果を今すぐ試そう,cbdオイル,cbdオイル 効果,cbd 効果,cbdオイル おすすめ,cbdオイル 日本,cbdオイル 使い方,cbdオイルとは,cbdオイル 口コミ,cbdオイル 通販\" />\r\n<meta property=\"og:type\" content=\"og:product\" />\r\n<meta property=\"og:title\" content=\"{{ Product.name }}\" />\r\n<meta property=\"og:image\" content=\"{{ url(\'homepage\') }}{{ asset(Product.main_list_image|no_image_product, \'save_image\') }}\" />\r\n<meta property=\"og:description\" content=\"{{ Product.description_detail|trans|striptags|replace({\"。\":\"\",\"\\n\\n\":\"。\",\"\\n\": \"。\", \"\\r\": \"\", \"\\t\": \"\"})}}\" />\r\n<meta property=\"og:url\" content=\"{{ url(\'product_detail\', {\'id\': Product.id}) }}\" />\r\n<meta property=\"product:price:amount\" content=\"{{ Product.getPrice02IncTaxMax }}\"/>\r\n<meta property=\"product:price:currency\" content=\"{{ eccube_config.currency }}\"/>\r\n<meta property=\"product:product_link\" content=\"{{ url(\'product_detail\', {\'id\': Product.id}) }}\"/>\r\n<meta property=\"product:retailer_title\" content=\"{{ Product.name }}\"/>', 'page'),
(4, NULL, 'マイページ', 'mypage', 'Mypage/index', 2, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターのMYページです。', 'MARITIME,マリタイム,cbdオイル,cbd効果,cbd飲料,cbdとは,cbdオイル 使い方,cbdオイル 効果,cbd ショップ,cbd hemp おすすめ,cbdウオーター,cbdオイル高濃度,cbd オイル %,cbd おすすめ,cbdオイル 購入,cbd 日本 合法,cbdカンナビジオール,cbdオイル おすすめ,cbd コスパ,カンナビジオール,カンナビジオール 効果,カンナビジオール 厚生労働省', '2017-03-07 10:14:52', '2020-09-03 06:38:46', 'noindex', NULL, 'page'),
(5, NULL, '会員登録内容変更(入力ページ)', 'mypage_change', 'Mypage/change', 2, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの会員登録内容変更(入力ページ)です。', 'MARITIME,マリタイム,cbdオイル,cbd効果,cbd飲料,cbdとは,cbdオイル 使い方,cbdオイル 効果,cbd ショップ,cbd hemp おすすめ,cbdウオーター,cbdオイル高濃度,cbd オイル %,cbd おすすめ,cbdオイル 購入,cbd 日本 合法,cbdカンナビジオール,cbdオイル おすすめ,cbd コスパ,カンナビジオール,カンナビジオール 効果,カンナビジオール 厚生労働省', '2017-03-07 10:14:52', '2020-08-13 03:39:15', 'noindex', NULL, 'page'),
(6, NULL, '会員登録内容変更(完了ページ)', 'mypage_change_complete', 'Mypage/change_complete', 2, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの会員登録内容変更(完了ページ)です。', 'MARITIME,マリタイム,cbdオイル,cbd効果,cbd飲料,cbdとは,cbdオイル 使い方,cbdオイル 効果,cbd ショップ,cbd hemp おすすめ,cbdウオーター,cbdオイル高濃度,cbd オイル %,cbd おすすめ,cbdオイル 購入,cbd 日本 合法,cbdカンナビジオール,cbdオイル おすすめ,cbd コスパ,カンナビジオール,カンナビジオール 効果,カンナビジオール 厚生労働省', '2017-03-07 10:14:52', '2020-08-13 03:40:00', 'noindex', NULL, 'page'),
(7, NULL, 'お届け先一覧', 'mypage_delivery', 'Mypage/delivery', 2, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターのお届け先一覧ページです。', 'MARITIME,マリタイム,cbdオイル,cbd効果,cbd飲料,cbdとは,cbdオイル 使い方,cbdオイル 効果,cbd ショップ,cbd hemp おすすめ,cbdウオーター,cbdオイル高濃度,cbd オイル %,cbd おすすめ,cbdオイル 購入,cbd 日本 合法,cbdカンナビジオール,cbdオイル おすすめ,cbd コスパ,カンナビジオール,カンナビジオール 効果,カンナビジオール 厚生労働省', '2017-03-07 10:14:52', '2020-08-13 03:40:41', 'noindex', NULL, 'page'),
(8, NULL, 'お届け先追加', 'mypage_delivery_new', 'Mypage/delivery_edit', 2, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターのお届け先追加ページです。', 'MARITIME,マリタイム,cbdオイル,cbd効果,cbd飲料,cbdとは,cbdオイル 使い方,cbdオイル 効果,cbd ショップ,cbd hemp おすすめ,cbdウオーター,cbdオイル高濃度,cbd オイル %,cbd おすすめ,cbdオイル 購入,cbd 日本 合法,cbdカンナビジオール,cbdオイル おすすめ,cbd コスパ,カンナビジオール,カンナビジオール 効果,カンナビジオール 厚生労働省', '2017-03-07 10:14:52', '2020-08-13 03:41:20', 'noindex', NULL, 'page'),
(9, NULL, 'お気に入り一覧', 'mypage_favorite', 'Mypage/favorite', 2, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターのお気に入り一覧ページです。', 'MARITIME,マリタイム,cbdオイル,cbd効果,cbd飲料,cbdとは,cbdオイル 使い方,cbdオイル 効果,cbd ショップ,cbd hemp おすすめ,cbdウオーター,cbdオイル高濃度,cbd オイル %,cbd おすすめ,cbdオイル 購入,cbd 日本 合法,cbdカンナビジオール,cbdオイル おすすめ,cbd コスパ,カンナビジオール,カンナビジオール 効果,カンナビジオール 厚生労働省', '2017-03-07 10:14:52', '2020-08-13 03:41:54', 'noindex', NULL, 'page'),
(10, NULL, '購入履歴詳細', 'mypage_history', 'Mypage/history', 2, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの購入履歴詳細ページです。', 'MARITIME,マリタイム,cbdオイル,cbd効果,cbd飲料,cbdとは,cbdオイル 使い方,cbdオイル 効果,cbd ショップ,cbd hemp おすすめ,cbdウオーター,cbdオイル高濃度,cbd オイル %,cbd おすすめ,cbdオイル 購入,cbd 日本 合法,cbdカンナビジオール,cbdオイル おすすめ,cbd コスパ,カンナビジオール,カンナビジオール 効果,カンナビジオール 厚生労働省', '2017-03-07 10:14:52', '2020-08-13 03:42:28', 'noindex', NULL, 'page'),
(11, NULL, 'ログイン', 'mypage_login', 'Mypage/login', 2, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターのログインページです。', 'MARITIME,マリタイム,cbdオイル,cbd効果,cbd飲料,cbdとは,cbdオイル 使い方,cbdオイル 効果,cbd ショップ,cbd hemp おすすめ,cbdウオーター,cbdオイル高濃度,cbd オイル %,cbd おすすめ,cbdオイル 購入,cbd 日本 合法,cbdカンナビジオール,cbdオイル おすすめ,cbd コスパ,カンナビジオール,カンナビジオール 効果,カンナビジオール 厚生労働省', '2017-03-07 10:14:52', '2020-08-13 03:43:01', 'noindex', NULL, 'page'),
(12, NULL, '退会手続き(入力ページ)', 'mypage_withdraw', 'Mypage/withdraw', 2, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの退会手続き(入力ページ)ページです。', 'MARITIME,マリタイム,cbdオイル,cbd効果,cbd飲料,cbdとは,cbdオイル 使い方,cbdオイル 効果,cbd ショップ,cbd hemp おすすめ,cbdウオーター,cbdオイル高濃度,cbd オイル %,cbd おすすめ,cbdオイル 購入,cbd 日本 合法,cbdカンナビジオール,cbdオイル おすすめ,cbd コスパ,カンナビジオール,カンナビジオール 効果,カンナビジオール 厚生労働省', '2017-03-07 10:14:52', '2020-08-13 03:43:40', 'noindex', NULL, 'page'),
(13, NULL, '退会手続き(完了ページ)', 'mypage_withdraw_complete', 'Mypage/withdraw_complete', 2, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの退会手続き(完了ページ)ページです。', 'MARITIME,マリタイム,cbdオイル,cbd効果,cbd飲料,cbdとは,cbdオイル 使い方,cbdオイル 効果,cbd ショップ,cbd hemp おすすめ,cbdウオーター,cbdオイル高濃度,cbd オイル %,cbd おすすめ,cbdオイル 購入,cbd 日本 合法,cbdカンナビジオール,cbdオイル おすすめ,cbd コスパ,カンナビジオール,カンナビジオール 効果,カンナビジオール 厚生労働省', '2017-03-07 10:14:52', '2020-08-13 03:44:15', 'noindex', NULL, 'page'),
(14, NULL, '当サイトについて', 'help_about', 'Help/about', 2, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの会社紹介ページです。', 'MARITIME,マリタイム,マリタイムサイトについて,おすすめのCBDウォーター,カンナビノイド効果を今すぐ試そう,cbdオイル,cbdオイル 効果,cbdオイル おすすめ,cbdオイル 日本,cbdオイル 使い方,cbdオイル 使い方,cbdオイルとは,cbdオイル 口コミ,cbdオイル 通販', '2017-03-07 10:14:52', '2020-09-03 05:20:29', NULL, NULL, 'page'),
(15, NULL, '現在のカゴの中', 'cart', 'Cart/index', 2, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの現在のショッピングカートの中のページです。', 'MARITIME,マリタイム,cbdオイル,cbd効果,cbd飲料,cbdとは,cbdオイル 使い方,cbdオイル 効果,cbd ショップ,cbd hemp おすすめ,cbdウオーター,cbdオイル高濃度,cbd オイル %,cbd おすすめ,cbdオイル 購入,cbd 日本 合法,cbdカンナビジオール,cbdオイル おすすめ,cbd コスパ,カンナビジオール,カンナビジオール 効果,カンナビジオール 厚生労働省', '2017-03-07 10:14:52', '2020-08-13 03:46:07', 'noindex', NULL, 'page'),
(16, NULL, 'お問い合わせ(入力ページ)', 'contact', 'Contact/index', 2, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターのお問い合わせ(入力ページ)です。', 'MARITIME,マリタイム,cbdオイル,cbd効果,cbd飲料,cbdとは,cbdオイル 使い方,cbdオイル 効果,cbd ショップ,cbd hemp おすすめ,cbdウオーター,cbdオイル高濃度,cbd オイル %,cbd おすすめ,cbdオイル 購入,cbd 日本 合法,cbdカンナビジオール,cbdオイル おすすめ,cbd コスパ,カンナビジオール,カンナビジオール 効果,カンナビジオール 厚生労働省', '2017-03-07 10:14:52', '2020-08-13 03:46:55', NULL, NULL, 'page'),
(17, NULL, 'お問い合わせ(完了ページ)', 'contact_complete', 'Contact/complete', 2, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターのお問い合わせ(完了ページ)です。', 'MARITIME,マリタイム,cbdオイル,cbd効果,cbd飲料,cbdとは,cbdオイル 使い方,cbdオイル 効果,cbd ショップ,cbd hemp おすすめ,cbdウオーター,cbdオイル高濃度,cbd オイル %,cbd おすすめ,cbdオイル 購入,cbd 日本 合法,cbdカンナビジオール,cbdオイル おすすめ,cbd コスパ,カンナビジオール,カンナビジオール 効果,カンナビジオール 厚生労働省', '2017-03-07 10:14:52', '2020-08-13 03:47:34', NULL, NULL, 'page'),
(18, NULL, '会員登録(入力ページ)', 'entry', 'Entry/index', 2, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの会員登録(入力ページ)です。', 'MARITIME,マリタイム,cbdオイル,cbd効果,cbd飲料,cbdとは,cbdオイル 使い方,cbdオイル 効果,cbd ショップ,cbd hemp おすすめ,cbdウオーター,cbdオイル高濃度,cbd オイル %,cbd おすすめ,cbdオイル 購入,cbd 日本 合法,cbdカンナビジオール,cbdオイル おすすめ,cbd コスパ,カンナビジオール,カンナビジオール 効果,カンナビジオール 厚生労働省', '2017-03-07 10:14:52', '2020-08-13 03:48:13', NULL, NULL, 'page'),
(19, NULL, 'ご利用規約', 'help_agreement', 'Help/agreement', 2, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターのご利用規約ページです。', 'MARITIME,マリタイム,マリタイムご利用規約,おすすめのCBDウォーター,カンナビノイド効果を今すぐ試そう,cbdオイル,cbdオイル 効果,cbdオイル おすすめ,cbdオイル 日本,cbdオイル 使い方,cbdオイル 使い方,cbdオイルとは,cbdオイル 口コミ,cbdオイル 通販', '2017-03-07 10:14:52', '2020-08-13 03:49:28', NULL, NULL, 'page'),
(20, NULL, '会員登録(完了ページ)', 'entry_complete', 'Entry/complete', 2, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの会員登録(完了ページ)です。', 'MARITIME,マリタイム,cbdオイル,cbd効果,cbd飲料,cbdとは,cbdオイル 使い方,cbdオイル 効果,cbd ショップ,cbd hemp おすすめ,cbdウオーター,cbdオイル高濃度,cbd オイル %,cbd おすすめ,cbdオイル 購入,cbd 日本 合法,cbdカンナビジオール,cbdオイル おすすめ,cbd コスパ,カンナビジオール,カンナビジオール 効果,カンナビジオール 厚生労働省', '2017-03-07 10:14:52', '2020-08-13 03:50:08', NULL, NULL, 'page'),
(21, NULL, '特定商取引法に基づく表記', 'help_tradelaw', 'Help/tradelaw', 2, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの特定商取引に関する法律に基づく表記ページです。', 'MARITIME,マリタイム,マリタイム　特定商取引に関する法律に基づく表記,マリタイムご利用規約,おすすめのCBDウォーター,カンナビノイド効果を今すぐ試そう,cbdオイル,cbdオイル 効果,cbdオイル おすすめ,cbdオイル 日本,cbdオイル 使い方,cbdオイル 使い方,cbdオイルとは,cbdオイル 口コミ,cbdオイル 通販', '2017-03-07 10:14:52', '2020-08-13 03:51:00', NULL, NULL, 'page'),
(22, NULL, '本会員登録(完了ページ)', 'entry_activate', 'Entry/activate', 2, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの本会員登録(完了ページ)です。', 'MARITIME,マリタイム,cbdオイル,cbd効果,cbd飲料,cbdとは,cbdオイル 使い方,cbdオイル 効果,cbd ショップ,cbd hemp おすすめ,cbdウオーター,cbdオイル高濃度,cbd オイル %,cbd おすすめ,cbdオイル 購入,cbd 日本 合法,cbdカンナビジオール,cbdオイル おすすめ,cbd コスパ,カンナビジオール,カンナビジオール 効果,カンナビジオール 厚生労働省', '2017-03-07 10:14:52', '2020-08-13 03:51:46', NULL, NULL, 'page'),
(23, NULL, '商品購入', 'shopping', 'Shopping/index', 2, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの商品購入ページです。', 'MARITIME,マリタイム,cbdオイル,cbd効果,cbd飲料,cbdとは,cbdオイル 使い方,cbdオイル 効果,cbd ショップ,cbd hemp おすすめ,cbdウオーター,cbdオイル高濃度,cbd オイル %,cbd おすすめ,cbdオイル 購入,cbd 日本 合法,cbdカンナビジオール,cbdオイル おすすめ,cbd コスパ,カンナビジオール,カンナビジオール 効果,カンナビジオール 厚生労働省', '2017-03-07 10:14:52', '2020-08-13 03:52:22', 'noindex', NULL, 'page'),
(24, NULL, '商品購入/お届け先の指定', 'shopping_shipping', 'Shopping/shipping', 2, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの商品購入/お届け先の指定ページです。', 'MARITIME,マリタイム,cbdオイル,cbd効果,cbd飲料,cbdとは,cbdオイル 使い方,cbdオイル 効果,cbd ショップ,cbd hemp おすすめ,cbdウオーター,cbdオイル高濃度,cbd オイル %,cbd おすすめ,cbdオイル 購入,cbd 日本 合法,cbdカンナビジオール,cbdオイル おすすめ,cbd コスパ,カンナビジオール,カンナビジオール 効果,カンナビジオール 厚生労働省', '2017-03-07 10:14:52', '2020-08-13 03:52:56', 'noindex', NULL, 'page'),
(25, NULL, '商品購入/お届け先の複数指定', 'shopping_shipping_multiple', 'Shopping/shipping_multiple', 2, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの商品購入/お届け先の複数指定ページです。', 'MARITIME,マリタイム,cbdオイル,cbd効果,cbd飲料,cbdとは,cbdオイル 使い方,cbdオイル 効果,cbd ショップ,cbd hemp おすすめ,cbdウオーター,cbdオイル高濃度,cbd オイル %,cbd おすすめ,cbdオイル 購入,cbd 日本 合法,cbdカンナビジオール,cbdオイル おすすめ,cbd コスパ,カンナビジオール,カンナビジオール 効果,カンナビジオール 厚生労働省', '2017-03-07 10:14:52', '2020-08-13 03:53:39', 'noindex', NULL, 'page'),
(28, NULL, '商品購入/ご注文完了', 'shopping_complete', 'Shopping/complete', 2, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの商品購入/ご注文完了ページです。', 'MARITIME,マリタイム,cbdオイル,cbd効果,cbd飲料,cbdとは,cbdオイル 使い方,cbdオイル 効果,cbd ショップ,cbd hemp おすすめ,cbdウオーター,cbdオイル高濃度,cbd オイル %,cbd おすすめ,cbdオイル 購入,cbd 日本 合法,cbdカンナビジオール,cbdオイル おすすめ,cbd コスパ,カンナビジオール,カンナビジオール 効果,カンナビジオール 厚生労働省', '2017-03-07 10:14:52', '2020-08-13 03:54:20', 'noindex', NULL, 'page'),
(29, NULL, 'プライバシーポリシー', 'help_privacy', 'Help/privacy', 2, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの商品購入/購入エラーページです。', 'MARITIME,マリタイム,cbdオイル,cbd効果,cbd飲料,cbdとは,cbdオイル 使い方,cbdオイル 効果,cbd ショップ,cbd hemp おすすめ,cbdウオーター,cbdオイル高濃度,cbd オイル %,cbd おすすめ,cbdオイル 購入,cbd 日本 合法,cbdカンナビジオール,cbdオイル おすすめ,cbd コスパ,カンナビジオール,カンナビジオール 効果,カンナビジオール 厚生労働省', '2017-03-07 10:14:52', '2020-09-04 05:25:30', 'noindex', NULL, 'page'),
(30, NULL, '商品購入ログイン', 'shopping_login', 'Shopping/login', 2, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの商品購入ログインページです。', 'MARITIME,マリタイム,cbdオイル,cbd効果,cbd飲料,cbdとは,cbdオイル 使い方,cbdオイル 効果,cbd ショップ,cbd hemp おすすめ,cbdウオーター,cbdオイル高濃度,cbd オイル %,cbd おすすめ,cbdオイル 購入,cbd 日本 合法,cbdカンナビジオール,cbdオイル おすすめ,cbd コスパ,カンナビジオール,カンナビジオール 効果,カンナビジオール 厚生労働省', '2017-03-07 10:14:52', '2020-08-13 03:55:56', NULL, NULL, 'page'),
(31, NULL, 'ゲスト購入情報入力', 'shopping_nonmember', 'Shopping/nonmember', 2, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターのゲスト購入情報入力ページです。', 'MARITIME,マリタイム,マリタイムゲスト購入,cbdオイル,cbd効果,cbd飲料,cbdとは,cbdオイル 使い方,cbdオイル 効果,cbd ショップ,cbd hemp おすすめ,cbdウオーター,cbdオイル高濃度,cbd オイル %,cbd おすすめ,cbdオイル 購入,cbd 日本 合法,cbdカンナビジオール,cbdオイル おすすめ,cbd コスパ,カンナビジオール,カンナビジオール 効果,カンナビジオール 厚生労働省', '2017-03-07 10:14:52', '2020-08-13 03:56:26', NULL, NULL, 'page'),
(32, NULL, '商品購入/お届け先の追加', 'shopping_shipping_edit', 'Shopping/shipping_edit', 2, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの商品購入/お届け先の追加ページです。', 'MARITIME,マリタイム,cbdオイル,cbd効果,cbd飲料,cbdとは,cbdオイル 使い方,cbdオイル 効果,cbd ショップ,cbd hemp おすすめ,cbdウオーター,cbdオイル高濃度,cbd オイル %,cbd おすすめ,cbdオイル 購入,cbd 日本 合法,cbdカンナビジオール,cbdオイル おすすめ,cbd コスパ,カンナビジオール,カンナビジオール 効果,カンナビジオール 厚生労働省', '2017-03-07 01:15:02', '2020-08-13 03:57:07', 'noindex', NULL, 'page'),
(33, NULL, '商品購入/お届け先の複数指定(お届け先の追加)', 'shopping_shipping_multiple_edit', 'Shopping/shipping_multiple_edit', 2, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの商品購入/お届け先の複数指定(お届け先の追加)ページです。', 'MARITIME,マリタイム,cbdオイル,cbd効果,cbd飲料,cbdとは,cbdオイル 使い方,cbdオイル 効果,cbd ショップ,cbd hemp おすすめ,cbdウオーター,cbdオイル高濃度,cbd オイル %,cbd おすすめ,cbdオイル 購入,cbd 日本 合法,cbdカンナビジオール,cbdオイル おすすめ,cbd コスパ,カンナビジオール,カンナビジオール 効果,カンナビジオール 厚生労働省', '2017-03-07 01:15:02', '2020-08-13 03:59:12', 'noindex', NULL, 'page'),
(34, NULL, '商品購入/購入エラー', 'shopping_error', 'Shopping/shopping_error', 2, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの商品購入/購入エラーページです。', 'MARITIME,マリタイム,cbdオイル,cbd効果,cbd飲料,cbdとは,cbdオイル 使い方,cbdオイル 効果,cbd ショップ,cbd hemp おすすめ,cbdウオーター,cbdオイル高濃度,cbd オイル %,cbd おすすめ,cbdオイル 購入,cbd 日本 合法,cbdカンナビジオール,cbdオイル おすすめ,cbd コスパ,カンナビジオール,カンナビジオール 効果,カンナビジオール 厚生労働省', '2017-03-07 01:15:02', '2020-08-13 03:59:56', 'noindex', NULL, 'page'),
(35, NULL, 'ご利用ガイド', 'help_guide', 'Help/guide', 2, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターのご利用ガイドページです。', 'MARITIME,マリタイム,マリタイムご利用ガイド,おすすめのCBDウォーター,カンナビノイド効果を今すぐ試そう,cbdオイル,cbdオイル 効果,cbdオイル おすすめ,cbdオイル 日本,cbdオイル 使い方,cbdオイル 使い方,cbdオイルとは,cbdオイル 口コミ,cbdオイル 通販', '2017-03-07 01:15:02', '2020-08-13 04:02:19', NULL, NULL, 'page'),
(36, NULL, 'パスワード再発行(入力ページ)', 'forgot', 'Forgot/index', 2, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターのパスワード再発行(入力ページ)です。', 'MARITIME,マリタイム,cbdオイル,cbd効果,cbd飲料,cbdとは,cbdオイル 使い方,cbdオイル 効果,cbd ショップ,cbd hemp おすすめ,cbdウオーター,cbdオイル高濃度,cbd オイル %,cbd おすすめ,cbdオイル 購入,cbd 日本 合法,cbdカンナビジオール,cbdオイル おすすめ,cbd コスパ,カンナビジオール,カンナビジオール 効果,カンナビジオール 厚生労働省', '2017-03-07 01:15:02', '2020-08-13 04:03:18', NULL, NULL, 'page'),
(37, NULL, 'パスワード再発行(完了ページ)', 'forgot_complete', 'Forgot/complete', 2, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターのパスワード再発行(完了ページ)です。', 'MARITIME,マリタイム,cbdオイル,cbd効果,cbd飲料,cbdとは,cbdオイル 使い方,cbdオイル 効果,cbd ショップ,cbd hemp おすすめ,cbdウオーター,cbdオイル高濃度,cbd オイル %,cbd おすすめ,cbdオイル 購入,cbd 日本 合法,cbdカンナビジオール,cbdオイル おすすめ,cbd コスパ,カンナビジオール,カンナビジオール 効果,カンナビジオール 厚生労働省', '2017-03-07 01:15:02', '2020-08-13 04:04:03', 'noindex', NULL, 'page'),
(38, NULL, 'パスワード再発行(再設定ページ)', 'forgot_reset', 'Forgot/reset', 2, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターのパスワード再発行(再設定ページ)です。', 'MARITIME,マリタイム,cbdオイル,cbd効果,cbd飲料,cbdとは,cbdオイル 使い方,cbdオイル 効果,cbd ショップ,cbd hemp おすすめ,cbdウオーター,cbdオイル高濃度,cbd オイル %,cbd おすすめ,cbdオイル 購入,cbd 日本 合法,cbdカンナビジオール,cbdオイル おすすめ,cbd コスパ,カンナビジオール,カンナビジオール 効果,カンナビジオール 厚生労働省', '2017-03-07 01:15:02', '2020-08-13 04:04:40', 'noindex', NULL, 'page'),
(42, NULL, '商品購入/遷移', 'shopping_redirect_to', 'Shopping/index', 2, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターのパスワード商品購入/遷移ページです。', 'MARITIME,マリタイム,cbdオイル,cbd効果,cbd飲料,cbdとは,cbdオイル 使い方,cbdオイル 効果,cbd ショップ,cbd hemp おすすめ,cbdウオーター,cbdオイル高濃度,cbd オイル %,cbd おすすめ,cbdオイル 購入,cbd 日本 合法,cbdカンナビジオール,cbdオイル おすすめ,cbd コスパ,カンナビジオール,カンナビジオール 効果,カンナビジオール 厚生労働省', '2017-03-07 01:15:03', '2020-08-13 04:05:50', 'noindex', NULL, 'page'),
(44, 8, 'MYページ/お届け先編集', 'mypage_delivery_edit', 'Mypage/delivery_edit', 2, 'MARITIME', NULL, NULL, '2017-03-07 01:15:05', '2017-03-07 01:15:05', 'noindex', NULL, 'page'),
(45, NULL, '商品購入/ご注文確認', 'shopping_confirm', 'Shopping/confirm', 2, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの商品購入/ご注文確認ページです。', 'MARITIME,マリタイム,cbdオイル,cbd効果,cbd飲料,cbdとは,cbdオイル 使い方,cbdオイル 効果,cbd ショップ,cbd hemp おすすめ,cbdウオーター,cbdオイル高濃度,cbd オイル %,cbd おすすめ,cbdオイル 購入,cbd 日本 合法,cbdカンナビジオール,cbdオイル おすすめ,cbd コスパ,カンナビジオール,カンナビジオール 効果,カンナビジオール 厚生労働省', '2017-03-07 01:15:03', '2020-08-13 04:06:39', 'noindex', NULL, 'page'),
(46, NULL, 'レビューを投稿', 'product_review_index', '@ProductReview4/default/index', 2, 'MARITIME', NULL, NULL, '2020-05-06 11:18:12', '2020-05-06 11:18:12', NULL, NULL, 'page'),
(47, NULL, 'レビューを投稿(完了)', 'product_review_complete', '@ProductReview4/default/index', 2, 'MARITIME', NULL, NULL, '2020-05-06 11:18:12', '2020-05-06 11:18:12', NULL, NULL, 'page'),
(48, NULL, 'MYページ/カード情報編集', 'gmo_mypage_card_edit', '@GmoPaymentGateway4/mypage_card', 2, 'MARITIME', NULL, NULL, '2020-05-14 22:31:28', '2020-05-14 22:31:28', 'noindex', NULL, 'page'),
(50, NULL, '代理店ページ', 'partner', 'partner', 0, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの代理店ページです。', 'MARITIME,マリタイム,マリタイム代理店ページ,おすすめのCBDウォーター,カンナビノイド効果を今すぐ試そう,cbdオイル,cbdオイル 効果,cbdオイル おすすめ,cbdオイル 日本,cbdオイル 使い方,cbdオイル 使い方,cbdオイルとは,cbdオイル 口コミ,cbdオイル 通販', '2020-05-16 11:47:45', '2020-08-13 04:08:14', NULL, NULL, 'page'),
(53, NULL, 'FAQページ', 'faq', 'faq', 0, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターのFAQ、よくある質問ページです。', 'MARITIME,マリタイム,マリタイムFAQ,マリタイムよくある質問,おすすめのCBDウォーター,カンナビノイド効果を今すぐ試そう,cbdオイル,cbdオイル 効果,cbdオイル おすすめ,cbdオイル 日本,cbdオイル 使い方,cbdオイル 使い方,cbdオイルとは,cbdオイル 口コミ,cbdオイル 通販', '2020-06-11 10:53:08', '2020-08-13 04:10:23', NULL, NULL, 'page'),
(55, NULL, 'CBD成分分析表', 'analysis', 'analysis', 0, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターのCBD成分分析表ページです。', 'MARITIME,マリタイム,マリタイムCBD成分分析表,おすすめのCBDウォーター,カンナビノイド効果を今すぐ試そう,cbdオイル,cbdオイル 効果,cbdオイル おすすめ,cbdオイル 日本,cbdオイル 使い方,cbdオイル 使い方,cbdオイルとは,cbdオイル 口コミ,cbdオイル 通販', '2020-06-19 05:17:36', '2020-08-13 04:15:38', NULL, NULL, 'page'),
(57, NULL, 'CBD摂取量ページ', 'cbd_intake', 'cbd_intake', 0, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターのCBD摂取量ページです。', 'MARITIME,マリタイム,マリタイムCBD摂取量ページ,マリタイムCBD摂取量ガガイド,マリタイムCBD摂取量目安,CBDおすすめのCBDウォーター,カンナビノイド効果を今すぐ試そう,cbdオイル,cbdオイル 効果,cbdオイル おすすめ,cbdオイル 日本,cbdオイル 使い方,cbdオイル 使い方,cbdオイルとは,cbdオイル 口コミ,cbdオイル 通販', '2020-07-06 02:39:23', '2020-08-13 04:12:27', NULL, NULL, 'page'),
(58, NULL, '100種類のCBDオイルを飲み比べ、辿り着いたこの味。  メイド・イン・ジャパンの品質、オリーブオイルを使った天然由来CBDオイル', 'maritime_lp_jp', 'maritime_lp_jp', 0, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。CBDオイルは1%から10%までの高濃度をご用意。コスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターもおすすめです。', 'MARITIME,マリタイム,マリタイムおすすめのCBDウォーター,カンナビノイド効果を今すぐ試そう,cbdオイル,cbdオイル 効果,cbdオイル おすすめ,cbdオイル 日本,cbdオイル 使い方,cbdオイル 使い方,cbdオイルとは,cbdオイル 口コミ,cbdオイル 通販', '2020-08-18 10:05:23', '2020-09-30 01:41:36', NULL, NULL, 'page'),
(59, NULL, '商品購入/ご注文確認', 'paypal_confirm', 'Shopping/confirm', 2, 'MARITIME', NULL, NULL, '2020-09-02 03:35:42', '2020-09-02 03:35:42', 'noindex', NULL, 'page'),
(60, NULL, '商品購入/クーポン利用', 'plugin_coupon_shopping', 'Coupon4/Resource/template/default/shopping_coupon', 2, 'MARITIME', NULL, NULL, '2020-09-02 03:36:02', '2020-09-02 03:36:02', 'noindex', NULL, 'page'),
(62, NULL, '代理店お問い合わせ(入力ページ)', 'partner_entry', 'Partner/index', 2, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターのお問い合わせ(入力ページ)です。', 'MARITIME,マリタイム,cbdオイル,cbd効果,cbd飲料,cbdとは,cbdオイル 使い方,cbdオイル 効果,cbd ショップ,cbd hemp おすすめ,cbdウオーター,cbdオイル高濃度,cbd オイル %,cbd おすすめ,cbdオイル 購入,cbd 日本 合法,cbdカンナビジオール,cbdオイル おすすめ,cbd コスパ,カンナビジオール,カンナビジオール 効果,カンナビジオール 厚生労働省', '2017-03-07 10:14:52', '2020-08-13 03:46:55', NULL, NULL, 'page'),
(63, NULL, '代理店お問い合わせ(完了ページ)', 'partner_complete', 'Partner/complete', 2, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターのお問い合わせ(完了ページ)です。', 'MARITIME,マリタイム,cbdオイル,cbd効果,cbd飲料,cbdとは,cbdオイル 使い方,cbdオイル 効果,cbd ショップ,cbd hemp おすすめ,cbdウオーター,cbdオイル高濃度,cbd オイル %,cbd おすすめ,cbdオイル 購入,cbd 日本 合法,cbdカンナビジオール,cbdオイル おすすめ,cbd コスパ,カンナビジオール,カンナビジオール 効果,カンナビジオール 厚生労働省', '2017-03-07 10:14:52', '2020-08-13 03:47:34', NULL, NULL, 'page'),
(64, NULL, 'お客様の声', 'product_review_list', 'review_list', 2, 'MARITIME', '日本製のCBDオイル、THCフリーの厚生労働省認定、合法のカンナビジオール効果をお求めならマリタイムへ。1%から10%までの高濃度のCBDオイルとコスパも良い天然麻由来成分CBD(カンナビジオール)入りの清涼飲料水CBDウォーターの商品一覧です。', 'MARITIME,マリタイム,マリタイム商品一覧ページ,おすすめのCBDウォーター,カンナビノイド効果を今すぐ試そう,cbdオイル,cbdオイル 効果,cbdオイル おすすめ,cbdオイル 日本,cbdオイル 使い方,cbdオイル 使い方,cbdオイルとは,cbdオイル 口コミ,cbdオイル 通販', '2017-03-07 10:14:52', '2020-10-02 00:24:23', NULL, NULL, 'page'),
(65, NULL, 'CBDとは?', 'what-is-cbd', 'what_is_cbd', 0, 'MARITIME', 'CBDとは何ですか？ CBDとはカンナビジオール（Cannabidiol）の略で、大麻から抽出される化合物（カンナビノイド）の一種です。  カンナビノイドとは、体の中の生きていくための機能や活動を調整したり、活性化したりする生理活性物質です。', 'cbdとは, cbd 効果, cbd 日本製,CBDは日本で合法,CBDに期待される効果効能とは,CBDの副作用や依存性,CBDオイル・ウォーター製品,CBDを日本で購入する注意点,CBDオイル販売店MARITIMEの特徴,海外での大麻・CBDの取り扱い', '2020-10-07 09:34:51', '2020-10-09 04:29:10', NULL, NULL, 'page'),
(66, NULL, 'CBDオイルとは?', 'what-is-cbd-oil', 'what_is_cbd_oil', 0, 'MARITIME', 'CBDオイルは、CBDを手軽に摂取できることから世界中で人気を集めているCBD製品の一つです。アメリカを中心に海外セレブたちの間で愛用されていることでも知られています。', NULL, '2020-10-09 04:23:47', '2020-10-11 07:55:46', NULL, NULL, 'page'),
(67, NULL, 'CBDウォーターとは?', 'what-is-cbd-water', 'what_is_cbd_water', 0, 'MARITIME', 'CBDウォーターについて詳しく知りたい、日本で購入を考えている方に、CBDの効果や摂取量の目安を解説します。CBDウォーターの評判や口コミも紹介します。', 'CBDウォーター, CBDウォーターとは, cbdウォーター 効果, cbdウォーターローション, cbdウォーター 口コミ, cbdウォーター ハワイ, cbdウォーター 神戸, cbdウォーター 日本', '2020-10-21 05:47:18', '2020-10-23 02:24:56', NULL, NULL, 'page'),
(68, NULL, 'CBDオイル＆CBDウォーターの使い方', 'how-to-use-cbd', 'how_to_use_cbd', 0, 'MARITIME', 'ここでは、CBDオイルとCBDウォーターの使い方と摂取方法、そして摂取タイミングなどについて紹介します。', 'CBD使い方, CBD使い方法, CBDオイル　使い方, CBDドロップス　使い方, CBDドロップス　摂取方法, cbdウォーター 使い方', '2020-10-23 00:55:19', '2020-10-23 02:27:24', NULL, NULL, 'page');

-- --------------------------------------------------------

--
-- Table structure for table `dtb_page_layout`
--

CREATE TABLE `dtb_page_layout` (
  `page_id` int(10) UNSIGNED NOT NULL,
  `layout_id` int(10) UNSIGNED NOT NULL,
  `sort_no` smallint(5) UNSIGNED NOT NULL,
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtb_page_layout`
--

INSERT INTO `dtb_page_layout` (`page_id`, `layout_id`, `sort_no`, `discriminator_type`) VALUES
(0, 0, 2, 'pagelayout'),
(1, 1, 45, 'pagelayout'),
(2, 3, 45, 'pagelayout'),
(3, 4, 45, 'pagelayout'),
(4, 2, 45, 'pagelayout'),
(5, 2, 44, 'pagelayout'),
(6, 2, 44, 'pagelayout'),
(7, 2, 44, 'pagelayout'),
(8, 2, 44, 'pagelayout'),
(9, 2, 44, 'pagelayout'),
(10, 2, 44, 'pagelayout'),
(11, 2, 44, 'pagelayout'),
(12, 2, 44, 'pagelayout'),
(13, 2, 44, 'pagelayout'),
(14, 2, 45, 'pagelayout'),
(15, 2, 44, 'pagelayout'),
(16, 2, 44, 'pagelayout'),
(17, 2, 44, 'pagelayout'),
(18, 2, 44, 'pagelayout'),
(19, 2, 44, 'pagelayout'),
(20, 2, 44, 'pagelayout'),
(21, 2, 44, 'pagelayout'),
(22, 2, 44, 'pagelayout'),
(23, 2, 44, 'pagelayout'),
(24, 2, 44, 'pagelayout'),
(25, 2, 44, 'pagelayout'),
(28, 2, 44, 'pagelayout'),
(29, 2, 45, 'pagelayout'),
(30, 2, 44, 'pagelayout'),
(31, 2, 44, 'pagelayout'),
(32, 2, 44, 'pagelayout'),
(33, 2, 44, 'pagelayout'),
(34, 2, 44, 'pagelayout'),
(35, 2, 44, 'pagelayout'),
(36, 2, 44, 'pagelayout'),
(37, 2, 44, 'pagelayout'),
(38, 2, 44, 'pagelayout'),
(42, 2, 44, 'pagelayout'),
(44, 2, 40, 'pagelayout'),
(45, 2, 44, 'pagelayout'),
(46, 2, 0, 'pagelayout'),
(47, 2, 0, 'pagelayout'),
(48, 2, 42, 'pagelayout'),
(50, 7, 45, 'pagelayout'),
(53, 7, 45, 'pagelayout'),
(55, 7, 45, 'pagelayout'),
(57, 7, 45, 'pagelayout'),
(58, 6, 45, 'pagelayout'),
(59, 2, 45, 'pagelayout'),
(60, 2, 0, 'pagelayout'),
(62, 2, 45, 'pagelayout'),
(63, 2, 45, 'pagelayout'),
(64, 2, 45, 'pagelayout'),
(65, 7, 45, 'pagelayout'),
(66, 7, 45, 'pagelayout'),
(67, 7, 45, 'pagelayout'),
(68, 7, 45, 'pagelayout');

-- --------------------------------------------------------

--
-- Table structure for table `dtb_payment`
--

CREATE TABLE `dtb_payment` (
  `id` int(10) UNSIGNED NOT NULL,
  `creator_id` int(10) UNSIGNED DEFAULT NULL,
  `payment_method` varchar(255) DEFAULT NULL,
  `charge` decimal(12,2) UNSIGNED DEFAULT '0.00',
  `rule_max` decimal(12,2) UNSIGNED DEFAULT NULL,
  `sort_no` smallint(5) UNSIGNED DEFAULT NULL,
  `fixed` tinyint(1) NOT NULL DEFAULT '1',
  `payment_image` varchar(255) DEFAULT NULL,
  `rule_min` decimal(12,2) UNSIGNED DEFAULT NULL,
  `method_class` varchar(255) DEFAULT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtb_payment`
--

INSERT INTO `dtb_payment` (`id`, `creator_id`, `payment_method`, `charge`, `rule_max`, `sort_no`, `fixed`, `payment_image`, `rule_min`, `method_class`, `visible`, `create_date`, `update_date`, `discriminator_type`) VALUES
(1, 1, '郵便振替', '0.00', NULL, 1, 1, NULL, '0.00', 'Eccube\\Service\\Payment\\Method\\Cash', 0, '2017-03-07 10:14:52', '2020-08-30 07:20:17', 'payment'),
(2, 1, '現金書留', '0.00', NULL, 1, 1, NULL, '0.00', 'Eccube\\Service\\Payment\\Method\\Cash', 0, '2017-03-07 10:14:52', '2020-08-30 07:20:17', 'payment'),
(3, 1, '銀行振込', '0.00', NULL, 10, 1, NULL, '0.00', 'Eccube\\Service\\Payment\\Method\\Cash', 1, '2017-03-07 10:14:52', '2020-08-30 07:21:07', 'payment'),
(4, 1, '代金引換', '0.00', NULL, 1, 1, NULL, '0.00', 'Eccube\\Service\\Payment\\Method\\Cash', 0, '2017-03-07 10:14:52', '2020-08-30 07:20:17', 'payment'),
(5, 1, 'クレジット決済', '0.00', NULL, 13, 1, NULL, '1.00', 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', 1, '2020-05-14 22:31:28', '2020-08-30 07:20:17', 'payment'),
(6, 1, 'コンビニ決済', '0.00', '299999.00', 7, 1, NULL, '1.00', 'Plugin\\GmoPaymentGateway4\\Service\\Method\\Cvs', 0, '2020-05-14 22:31:28', '2020-08-30 07:20:17', 'payment'),
(7, 1, 'Pay-easy決済(銀行ATM)', '0.00', '999999.00', 2, 1, NULL, '1.00', 'Plugin\\GmoPaymentGateway4\\Service\\Method\\PayEasyAtm', 0, '2020-05-14 22:31:28', '2020-08-30 07:20:17', 'payment'),
(8, 1, 'Pay-easy決済(ネットバンク)', '0.00', '999999.00', 3, 1, NULL, '1.00', 'Plugin\\GmoPaymentGateway4\\Service\\Method\\PayEasyNet', 0, '2020-05-14 22:31:28', '2020-08-30 07:20:17', 'payment'),
(9, 1, 'auかんたん決済', '0.00', '9999999.00', 4, 1, NULL, '1.00', 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CarAu', 0, '2020-05-14 22:31:28', '2020-08-30 07:20:17', 'payment'),
(10, 1, 'ドコモケータイ払い', '0.00', '30000.00', 5, 1, NULL, '1.00', 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CarDocomo', 0, '2020-05-14 22:31:28', '2020-08-30 07:20:17', 'payment'),
(11, 1, 'ソフトバンクまとめて支払い', '0.00', '100000.00', 6, 1, NULL, '1.00', 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CarSoftbank', 0, '2020-05-14 22:31:28', '2020-08-30 07:20:17', 'payment'),
(12, 1, '楽天ペイ', '0.00', '99999999.00', 9, 1, NULL, '100.00', 'Plugin\\GmoPaymentGateway4\\Service\\Method\\RakutenPay', 0, '2020-05-14 22:31:28', '2020-08-30 07:20:17', 'payment'),
(13, 1, 'クレジット決済「定期購入」', '0.00', NULL, 8, 1, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', 0, '2020-05-23 07:17:19', '2020-08-30 07:20:17', 'payment'),
(14, 1, 'PayPal決済', '0.00', '1000000.00', 12, 1, NULL, '1.00', 'Plugin\\PayPalCheckout\\Service\\Method\\CreditCard', 1, '2020-05-24 08:43:48', '2020-09-02 03:35:42', 'payment'),
(15, 1, 'かんたん銀行決済(PayPal)', '0.00', '1000000.00', 11, 1, NULL, '1.00', 'Plugin\\PayPalCheckout\\Service\\Method\\BankTransfer', 1, '2020-05-24 08:43:48', '2020-09-02 03:35:42', 'payment');

-- --------------------------------------------------------

--
-- Table structure for table `dtb_payment_option`
--

CREATE TABLE `dtb_payment_option` (
  `delivery_id` int(10) UNSIGNED NOT NULL,
  `payment_id` int(10) UNSIGNED NOT NULL,
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtb_payment_option`
--

INSERT INTO `dtb_payment_option` (`delivery_id`, `payment_id`, `discriminator_type`) VALUES
(1, 3, 'paymentoption'),
(1, 5, 'paymentoption'),
(1, 14, 'paymentoption'),
(1, 15, 'paymentoption'),
(2, 13, 'paymentoption'),
(2, 14, 'paymentoption'),
(2, 15, 'paymentoption');

-- --------------------------------------------------------

--
-- Table structure for table `dtb_plugin`
--

CREATE TABLE `dtb_plugin` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `version` varchar(255) NOT NULL,
  `source` varchar(255) NOT NULL,
  `initialized` tinyint(1) NOT NULL DEFAULT '0',
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtb_plugin`
--

INSERT INTO `dtb_plugin` (`id`, `name`, `code`, `enabled`, `version`, `source`, `initialized`, `create_date`, `update_date`, `discriminator_type`) VALUES
(1, 'taba app CMSプラグイン', 'TabaCMS', 1, '1.0.8', '1719', 1, '2020-05-06 09:37:40', '2020-09-01 11:22:04', 'plugin'),
(2, '商品レビュー管理プラグイン', 'ProductReview4', 1, '4.0.1', '1756', 1, '2020-05-06 11:16:46', '2020-09-02 03:42:06', 'plugin'),
(3, '商品問い合わせ for EC-CUBE4', 'ProductContact4', 1, '1.0.3', '2005', 1, '2020-05-09 00:00:10', '2020-09-02 03:39:41', 'plugin'),
(4, 'ソーシャルボタンを追加', 'MGSocialButton', 1, '1.0.2', '1776', 1, '2020-05-09 00:02:09', '2020-09-02 03:39:09', 'plugin'),
(5, 'お問い合わせ管理 for EC-CUBE4', 'ContactManagement4', 1, '1.0.3', '2004', 1, '2020-05-09 00:07:13', '2020-09-02 03:35:08', 'plugin'),
(6, 'PGマルチペイメントサービス決済プラグイン', 'GmoPaymentGateway4', 1, '1.0.4', '1797', 1, '2020-05-14 22:12:27', '2020-09-02 03:36:52', 'plugin'),
(7, 'Coupon Plugin for EC-CUBE4', 'Coupon4', 1, '4.0.6', '1923', 1, '2020-05-16 10:15:58', '2020-09-02 03:36:03', 'plugin'),
(8, 'メールマガジンプラグイン', 'MailMagazine4', 1, '4.0.2', '1760', 1, '2020-05-16 10:30:32', '2020-09-02 03:37:27', 'plugin'),
(9, 'ペイパルチェックアウト決済プラグイン(4.0系)', 'PayPalCheckout', 1, '1.0.5', '1930', 1, '2020-05-24 08:13:13', '2020-09-02 03:35:42', 'plugin'),
(10, '特定会員価格プラグイン for EC-CUBE4', 'CustomerClassPrice4', 1, '1.0.5', '1929', 1, '2020-05-27 01:36:39', '2020-09-02 03:36:33', 'plugin'),
(12, '商品おすすめ順並び替えプラグイン for EC-CUBE4', 'ProductDisplayRank4', 1, '1.0.1', '2020', 1, '2020-06-23 09:51:33', '2020-09-02 03:40:05', 'plugin'),
(14, '予約商品プラグイン', 'ProductReserve4', 1, '1.0.10', '2011', 1, '2020-07-15 06:18:22', '2020-09-02 03:41:29', 'plugin'),
(15, 'カゴ落ちチェックプラグイン', 'CheckKagoochi', 1, '1.0.1', '2087', 1, '2020-08-30 06:10:58', '2020-09-02 03:34:50', 'plugin'),
(16, '全ページ対応パンくずリスト表示プラグイン for EC-CUBE4', 'BreadcrumbList4', 1, '2.0.0', '1762', 1, '2020-09-02 09:34:42', '2020-09-02 09:35:38', 'plugin');

-- --------------------------------------------------------

--
-- Table structure for table `dtb_product`
--

CREATE TABLE `dtb_product` (
  `id` int(10) UNSIGNED NOT NULL,
  `creator_id` int(10) UNSIGNED DEFAULT NULL,
  `product_status_id` smallint(5) UNSIGNED DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `note` varchar(4000) DEFAULT NULL,
  `description_list` varchar(4000) DEFAULT NULL,
  `description_detail` varchar(4000) DEFAULT NULL,
  `search_word` varchar(4000) DEFAULT NULL,
  `free_area` longtext,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL,
  `plg_ccp_enabled_discount` tinyint(1) DEFAULT '1',
  `display_rank` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtb_product`
--

INSERT INTO `dtb_product` (`id`, `creator_id`, `product_status_id`, `name`, `note`, `description_list`, `description_detail`, `search_word`, `free_area`, `create_date`, `update_date`, `discriminator_type`, `plg_ccp_enabled_discount`, `display_rank`) VALUES
(2, 1, 3, 'MARITIME CBDオイル 30ml (10%)', NULL, NULL, '10%\r\n30ml/3000mg\r\nトライアル用\r\n\r\nまずはMaritimeの確かな品質をCBD1%のトライアルオイルでお試しください。\r\nその他用途に合わせて５タイプの濃度と２タイプのサイズからお選びいただけます。\r\n常に健康に気を配りたい方へのサブスク（定期購買）システムにも対応しています。', 'Maritime CBD 3000 3000mg 30ml 10% CBDオイル', NULL, '2018-09-28 10:14:52', '2020-07-15 10:33:14', 'product', 1, 0),
(4, 1, 1, 'MARITIME CBD ウォーター 500ml', 'スライダー順番：1', NULL, 'front.water_description', 'MARITIME CBD Infused mineral water ウォーター 500ml CBD 24mg', NULL, '2020-04-29 00:06:19', '2020-08-04 05:38:35', 'product', 1, 110),
(5, 1, 1, 'MARITIME CBDオイル 10ml (10%)', 'スライダー順番：8', NULL, 'front.cbd_1000_description', 'MARITIME CBD1000 1000mg 10ml 10% CBDオイル', NULL, '2020-05-10 03:52:28', '2020-08-04 05:35:49', 'product', 1, 80),
(6, 1, 1, 'MARITIME CBDオイル 10ml (5%)', 'スライダー順番：7', NULL, 'front.cbd_500_description', 'MARITIME CBD500 500mg 10ml 5% CBDオイル', NULL, '2020-05-10 04:01:39', '2020-08-04 05:33:05', 'product', 1, 60),
(7, 1, 1, 'MARITIME CBDオイル 10ml (3%)', 'スライダー順番：6', NULL, 'front.cbd_300_10_description', 'MARITIME CBD300 300mg 10ml 3% CBDオイル', NULL, '2020-05-10 04:13:02', '2020-08-04 05:31:00', 'product', 1, 50),
(8, 1, 1, 'MARITIME CBDオイル 10ml (2%)', 'スライダー順番：5', NULL, 'front.cbd_200_description', 'MARITIME CBD200 200mg 10ml 2% CBDオイル', NULL, '2020-05-10 04:14:21', '2020-08-02 12:14:26', 'product', 1, 30),
(9, 1, 1, 'MARITIME CBDオイル 10ml (1%)', 'スライダー順番：4', NULL, 'front.cbd_100_description', 'MARITIME CBD100 100mg 10ml 1% CBDオイル', NULL, '2020-05-10 04:21:25', '2020-08-02 12:05:04', 'product', 1, 10),
(10, 1, 1, 'MARITIME CBDオイル 30ml (5%)', 'スライダー順番：2', NULL, 'front.cbd_1500_description', 'MARITIME CBD 1500 1500mg 30ml 5% CBDオイル', NULL, '2020-05-10 04:23:31', '2020-08-04 05:36:40', 'product', 1, 90),
(11, 1, 1, 'MARITIME CBDオイル 30ml (3%)', 'スライダー順番：1', NULL, 'front.cbd_900_description', 'MARITIME CBD900 900mg 30ml 3% CBDオイル', NULL, '2020-05-10 04:24:53', '2020-08-04 05:35:11', 'product', 1, 70),
(12, 1, 1, 'MARITIME CBDオイル 30ml (2%)', 'スライダー順番：最後', NULL, 'front.cbd_600_description', 'MARITIME CBD 600 3000mg 30ml 2% CBDオイル', NULL, '2020-05-10 04:28:10', '2020-08-04 05:34:29', 'product', 1, 40),
(13, 1, 1, 'MARITIME CBDオイル 30ml (1%)', 'スライダー順番：9', NULL, 'front.cbd_300_30_description', 'MARITIME CBD300 300mg 30ml 1% CBDオイル', NULL, '2020-05-10 04:31:16', '2020-08-04 05:32:14', 'product', 1, 20),
(14, 1, 1, 'MARITIME CBDオイル 30ml (10%)', 'スライダー順番：3', NULL, 'front.cbd_3000_description', 'MARITIME CBD 3000 3000mg 30ml 10% CBDオイル', NULL, '2020-05-10 04:36:03', '2020-08-04 05:37:13', 'product', 1, 100),
(15, 1, 1, 'MARITIME CBD ウォーター 500ml ６本セット', 'スライダー順番：2', NULL, 'front.water_set_description', 'MARITIME CBD Infused mineral water ウォーター 500ml ６本セット CBD 24mg', NULL, '2020-06-10 09:33:12', '2020-08-04 05:38:03', 'product', 1, 120);

-- --------------------------------------------------------

--
-- Table structure for table `dtb_product_category`
--

CREATE TABLE `dtb_product_category` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtb_product_category`
--

INSERT INTO `dtb_product_category` (`product_id`, `category_id`, `discriminator_type`) VALUES
(4, 1, 'productcategory'),
(4, 7, 'productcategory'),
(5, 1, 'productcategory'),
(5, 3, 'productcategory'),
(5, 19, 'productcategory'),
(5, 21, 'productcategory'),
(5, 31, 'productcategory'),
(5, 32, 'productcategory'),
(6, 1, 'productcategory'),
(6, 3, 'productcategory'),
(6, 19, 'productcategory'),
(6, 22, 'productcategory'),
(6, 31, 'productcategory'),
(6, 32, 'productcategory'),
(7, 1, 'productcategory'),
(7, 3, 'productcategory'),
(7, 19, 'productcategory'),
(7, 23, 'productcategory'),
(7, 31, 'productcategory'),
(7, 32, 'productcategory'),
(8, 1, 'productcategory'),
(8, 3, 'productcategory'),
(8, 19, 'productcategory'),
(8, 24, 'productcategory'),
(8, 31, 'productcategory'),
(8, 32, 'productcategory'),
(9, 1, 'productcategory'),
(9, 3, 'productcategory'),
(9, 19, 'productcategory'),
(9, 25, 'productcategory'),
(9, 31, 'productcategory'),
(9, 32, 'productcategory'),
(10, 1, 'productcategory'),
(10, 3, 'productcategory'),
(10, 20, 'productcategory'),
(10, 22, 'productcategory'),
(10, 31, 'productcategory'),
(10, 32, 'productcategory'),
(11, 1, 'productcategory'),
(11, 3, 'productcategory'),
(11, 20, 'productcategory'),
(11, 23, 'productcategory'),
(11, 31, 'productcategory'),
(11, 32, 'productcategory'),
(12, 1, 'productcategory'),
(12, 3, 'productcategory'),
(12, 20, 'productcategory'),
(12, 24, 'productcategory'),
(12, 31, 'productcategory'),
(12, 32, 'productcategory'),
(13, 1, 'productcategory'),
(13, 3, 'productcategory'),
(13, 20, 'productcategory'),
(13, 25, 'productcategory'),
(13, 31, 'productcategory'),
(13, 32, 'productcategory'),
(14, 1, 'productcategory'),
(14, 3, 'productcategory'),
(14, 20, 'productcategory'),
(14, 21, 'productcategory'),
(14, 31, 'productcategory'),
(14, 32, 'productcategory'),
(15, 1, 'productcategory'),
(15, 7, 'productcategory');

-- --------------------------------------------------------

--
-- Table structure for table `dtb_product_class`
--

CREATE TABLE `dtb_product_class` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `sale_type_id` smallint(5) UNSIGNED DEFAULT NULL,
  `class_category_id1` int(10) UNSIGNED DEFAULT NULL,
  `class_category_id2` int(10) UNSIGNED DEFAULT NULL,
  `delivery_duration_id` int(10) UNSIGNED DEFAULT NULL,
  `creator_id` int(10) UNSIGNED DEFAULT NULL,
  `product_code` varchar(255) DEFAULT NULL,
  `stock` decimal(10,0) DEFAULT NULL,
  `stock_unlimited` tinyint(1) NOT NULL DEFAULT '0',
  `sale_limit` decimal(10,0) UNSIGNED DEFAULT NULL,
  `price01` decimal(12,2) DEFAULT NULL,
  `price02` decimal(12,2) NOT NULL,
  `delivery_fee` decimal(12,2) UNSIGNED DEFAULT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `currency_code` varchar(255) DEFAULT NULL,
  `point_rate` decimal(10,0) UNSIGNED DEFAULT NULL,
  `discriminator_type` varchar(255) NOT NULL,
  `subscription_pricing` int(10) UNSIGNED DEFAULT NULL,
  `use_subscription` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtb_product_class`
--

INSERT INTO `dtb_product_class` (`id`, `product_id`, `sale_type_id`, `class_category_id1`, `class_category_id2`, `delivery_duration_id`, `creator_id`, `product_code`, `stock`, `stock_unlimited`, `sale_limit`, `price01`, `price02`, `delivery_fee`, `visible`, `create_date`, `update_date`, `currency_code`, `point_rate`, `discriminator_type`, `subscription_pricing`, `use_subscription`) VALUES
(11, 2, 1, NULL, NULL, 2, 1, 'cbd-3000', '1', 0, '5', NULL, '9999.00', NULL, 1, '2017-03-07 10:14:52', '2020-07-15 10:33:14', 'JPY', NULL, 'productclass', NULL, 0),
(23, 4, 1, NULL, NULL, 2, 1, 'cbd-water-500', '100', 0, NULL, NULL, '5000.00', NULL, 0, '2020-04-29 00:06:19', '2020-05-15 01:41:10', 'JPY', NULL, 'productclass', NULL, 0),
(24, 5, 1, NULL, NULL, 2, 1, 'cbd-1000', '100', 0, '5', NULL, '9999.00', NULL, 0, '2020-05-10 03:52:28', '2020-05-15 01:46:01', 'JPY', NULL, 'productclass', NULL, 0),
(25, 6, 1, NULL, NULL, 2, 1, 'cbd-500', '100', 0, '5', NULL, '9999.00', NULL, 0, '2020-05-10 04:01:39', '2020-05-15 01:47:02', 'JPY', NULL, 'productclass', NULL, 0),
(26, 7, 1, NULL, NULL, 2, 1, 'cbd-300', '100', 0, '5', NULL, '9999.00', NULL, 0, '2020-05-10 04:13:02', '2020-05-15 01:56:25', 'JPY', NULL, 'productclass', NULL, 0),
(27, 8, 1, NULL, NULL, 2, 1, 'cbd-200', '100', 0, '5', NULL, '9999.00', NULL, 0, '2020-05-10 04:14:21', '2020-05-15 01:55:04', 'JPY', NULL, 'productclass', NULL, 0),
(28, 9, 1, NULL, NULL, 2, 1, 'cbd-100', '100', 0, '5', NULL, '9999.00', NULL, 0, '2020-05-10 04:21:25', '2020-05-15 01:47:58', 'JPY', NULL, 'productclass', NULL, 0),
(29, 10, 1, NULL, NULL, 2, 1, 'cbd-1500', '100', 0, '5', NULL, '9999.00', NULL, 0, '2020-05-10 04:23:31', '2020-05-15 01:51:51', 'JPY', NULL, 'productclass', NULL, 0),
(30, 11, 1, NULL, NULL, 2, 1, 'cbd-900', '100', 0, '5', NULL, '9999.00', NULL, 0, '2020-05-10 04:24:53', '2020-05-15 01:53:59', 'JPY', NULL, 'productclass', NULL, 0),
(31, 12, 1, NULL, NULL, 2, 1, 'cbd-600', '100', 0, '5', NULL, '9999.00', NULL, 0, '2020-05-10 04:28:10', '2020-05-15 01:52:49', 'JPY', NULL, 'productclass', NULL, 0),
(32, 13, 1, NULL, NULL, 2, 1, 'cbd-300', '100', 0, '5', NULL, '9999.00', NULL, 0, '2020-05-10 04:31:16', '2020-05-15 01:50:03', 'JPY', NULL, 'productclass', NULL, 0),
(33, 14, 1, NULL, NULL, 2, 1, 'cbd-3000', '100', 0, '5', NULL, '9999.00', NULL, 0, '2020-05-10 04:36:03', '2020-05-14 22:30:04', 'JPY', NULL, 'productclass', NULL, 0),
(34, 14, 1, 1, NULL, 2, 1, 'cbd-1000', '5', 0, NULL, NULL, '4000.00', NULL, 0, '2020-05-10 04:42:02', '2020-05-14 22:23:53', 'JPY', NULL, 'productclass', NULL, 0),
(35, 14, 1, 2, NULL, 2, 1, 'cbd-3000', '5', 0, NULL, NULL, '6999.00', NULL, 0, '2020-05-10 04:42:02', '2020-05-14 22:23:53', 'JPY', NULL, 'productclass', NULL, 0),
(36, 14, 1, 3, NULL, 2, 1, 'cbd-3000', NULL, 1, '10', '47500.00', '47500.00', NULL, 1, '2020-05-14 22:26:10', '2020-07-17 08:22:32', NULL, NULL, 'productclass', NULL, 0),
(37, 14, 2, 4, NULL, 2, 1, 'cbd-3000-s', NULL, 1, '10', NULL, '38000.00', NULL, 1, '2020-05-14 22:26:10', '2020-07-17 08:22:32', NULL, NULL, 'productclass', NULL, 0),
(38, 4, 1, 3, NULL, 2, 1, 'cbd-water-500', '1', 0, '60', '1160.00', '1160.00', NULL, 1, '2020-05-15 01:41:10', '2020-08-20 06:16:46', 'JPY', NULL, 'productclass', NULL, 0),
(39, 4, 2, 4, NULL, 2, 1, 'cbd-water-500-s', '1', 0, '60', NULL, '928.00', NULL, 1, '2020-05-15 01:41:10', '2020-08-20 06:16:46', 'JPY', NULL, 'productclass', NULL, 0),
(40, 5, 1, 3, NULL, 2, 1, 'cbd-1000', NULL, 1, '10', '26400.00', '26400.00', NULL, 1, '2020-05-15 01:46:01', '2020-06-19 03:31:51', 'JPY', NULL, 'productclass', NULL, 0),
(41, 5, 2, 4, NULL, 2, 1, 'cbd-1000-s', NULL, 1, '10', NULL, '21120.00', NULL, 1, '2020-05-15 01:46:01', '2020-06-19 03:31:51', 'JPY', NULL, 'productclass', NULL, 0),
(42, 6, 1, 3, NULL, 2, 1, 'cbd-500', NULL, 1, '10', '14400.00', '14400.00', NULL, 1, '2020-05-15 01:47:02', '2020-06-19 03:38:23', 'JPY', NULL, 'productclass', NULL, 0),
(43, 6, 2, 4, NULL, 2, 1, 'cbd-500-s', NULL, 1, '10', NULL, '11520.00', NULL, 1, '2020-05-15 01:47:02', '2020-06-19 03:38:23', 'JPY', NULL, 'productclass', NULL, 0),
(44, 9, 1, 3, NULL, 2, 1, 'cbd-100', '3', 0, '10', '4800.00', '4800.00', NULL, 1, '2020-05-15 01:47:58', '2020-09-17 06:20:57', 'JPY', NULL, 'productclass', NULL, 0),
(45, 9, 2, 4, NULL, 2, 1, 'cbd-100-s', NULL, 1, '10', NULL, '3840.00', NULL, 1, '2020-05-15 01:47:58', '2020-08-18 15:48:00', 'JPY', NULL, 'productclass', NULL, 0),
(46, 13, 1, 3, NULL, 2, 1, 'cbd-300-30', NULL, 1, '10', '9100.00', '9100.00', NULL, 1, '2020-05-15 01:50:03', '2020-06-19 03:34:17', 'JPY', NULL, 'productclass', NULL, 0),
(47, 13, 2, 4, NULL, NULL, 1, 'cbd-300-30-s', NULL, 1, '10', NULL, '7280.00', NULL, 1, '2020-05-15 01:50:03', '2020-06-19 03:34:17', 'JPY', NULL, 'productclass', NULL, 0),
(48, 10, 1, 3, NULL, NULL, 1, 'cbd-1500', NULL, 1, '10', '32500.00', '32500.00', NULL, 1, '2020-05-15 01:51:51', '2020-06-19 03:36:30', 'JPY', NULL, 'productclass', NULL, 0),
(49, 10, 2, 4, NULL, NULL, 1, 'cbd-1500-s', NULL, 1, '10', NULL, '26000.00', NULL, 1, '2020-05-15 01:51:51', '2020-06-19 03:36:30', 'JPY', NULL, 'productclass', NULL, 0),
(50, 12, 1, 3, NULL, NULL, 1, 'cbd-600', NULL, 1, '10', '14950.00', '14950.00', NULL, 1, '2020-05-15 01:52:49', '2020-06-19 03:35:21', 'JPY', NULL, 'productclass', NULL, 0),
(51, 12, 2, 4, NULL, NULL, 1, 'cbd-600-s', NULL, 1, '10', NULL, '11960.00', NULL, 1, '2020-05-15 01:52:49', '2020-06-19 03:35:21', 'JPY', NULL, 'productclass', NULL, 0),
(52, 11, 1, 3, NULL, NULL, 1, 'cbd-900', NULL, 1, '10', '20800.00', '20800.00', NULL, 1, '2020-05-15 01:53:59', '2020-06-19 03:30:41', 'JPY', NULL, 'productclass', NULL, 0),
(53, 11, 2, 4, NULL, NULL, 1, 'cbd-900-s', NULL, 1, '10', NULL, '16640.00', NULL, 1, '2020-05-15 01:53:59', '2020-06-19 03:30:41', 'JPY', NULL, 'productclass', NULL, 0),
(54, 8, 1, 3, NULL, NULL, 1, 'cbd-200', NULL, 1, '10', '7200.00', '7200.00', NULL, 1, '2020-05-15 01:55:04', '2020-06-19 03:57:30', 'JPY', NULL, 'productclass', NULL, 0),
(55, 8, 2, 4, NULL, NULL, 1, 'cbd-200-s', NULL, 1, '10', NULL, '5760.00', NULL, 1, '2020-05-15 01:55:04', '2020-06-19 03:57:30', 'JPY', NULL, 'productclass', NULL, 0),
(56, 7, 1, 3, NULL, NULL, 1, 'cbd-300-10', NULL, 1, '10', '9600.00', '9600.00', NULL, 1, '2020-05-15 01:56:25', '2020-06-19 03:28:28', 'JPY', NULL, 'productclass', NULL, 0),
(57, 7, 2, 4, NULL, NULL, 1, 'cbd-300-10-s', NULL, 1, '10', NULL, '7680.00', NULL, 1, '2020-05-15 01:56:25', '2020-06-19 03:28:28', 'JPY', NULL, 'productclass', NULL, 0),
(58, 15, 1, NULL, NULL, 4, 1, 'cbd-water-500', '100', 0, NULL, NULL, '5000.00', NULL, 0, '2020-06-10 09:33:12', '2020-06-10 09:33:12', 'JPY', NULL, 'productclass', NULL, 0),
(59, 15, 1, 3, NULL, 2, 1, 'cbd-water-500-set', '1', 0, '10', '6960.00', '6960.00', NULL, 1, '2020-06-10 09:33:12', '2020-08-20 06:17:39', 'JPY', NULL, 'productclass', NULL, 0),
(60, 15, 2, 4, NULL, 2, 1, 'cbd-water-500-set-s', '1', 0, '10', NULL, '5568.00', NULL, 1, '2020-06-10 09:33:12', '2020-08-20 06:17:39', 'JPY', NULL, 'productclass', NULL, 0),
(61, 15, 1, NULL, NULL, 4, 1, 'cbd-water-500', '100', 0, NULL, NULL, '5000.00', NULL, 0, '2020-06-10 09:33:12', '2020-06-10 09:33:12', 'JPY', NULL, 'productclass', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `dtb_product_image`
--

CREATE TABLE `dtb_product_image` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `creator_id` int(10) UNSIGNED DEFAULT NULL,
  `file_name` varchar(255) NOT NULL,
  `sort_no` smallint(5) UNSIGNED NOT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtb_product_image`
--

INSERT INTO `dtb_product_image` (`id`, `product_id`, `creator_id`, `file_name`, `sort_no`, `create_date`, `discriminator_type`) VALUES
(31, 2, 1, '0429091111_5ea8c61f1a69b.jpg', 2, '2020-04-29 00:11:53', 'productimage'),
(83, 2, 1, '0515084828_5ebdd8cc00c6c.png', 1, '2020-05-14 23:48:35', 'productimage'),
(235, 7, 1, '0611180459_5ee1f3bb85579.png', 1, '2020-06-19 03:54:39', 'productimage'),
(236, 7, 1, '0611180454_5ee1f3b64454c.png', 2, '2020-06-19 03:54:39', 'productimage'),
(237, 7, 1, '0611180501_5ee1f3bd20f76.png', 3, '2020-06-19 03:54:39', 'productimage'),
(238, 7, 1, '0611180428_5ee1f39c0c05d.png', 4, '2020-06-19 03:54:39', 'productimage'),
(249, 11, 1, '0611180734_5ee1f456d2c22.png', 1, '2020-06-19 03:54:39', 'productimage'),
(250, 11, 1, '0611180733_5ee1f4557f227.png', 2, '2020-06-19 03:54:39', 'productimage'),
(251, 11, 1, '0611180736_5ee1f4581ec4f.png', 3, '2020-06-19 03:54:39', 'productimage'),
(252, 11, 1, '0611180737_5ee1f459ede13.png', 4, '2020-06-19 03:54:39', 'productimage'),
(253, 11, 1, '0611180739_5ee1f45b8e4d3.png', 5, '2020-06-19 03:54:39', 'productimage'),
(262, 5, 1, '0611180346_5ee1f37281399.png', 1, '2020-06-19 03:54:39', 'productimage'),
(263, 5, 1, '0611180342_5ee1f36e3c868.png', 2, '2020-06-19 03:54:39', 'productimage'),
(264, 5, 1, '0611180347_5ee1f373a9912.png', 3, '2020-06-19 03:54:39', 'productimage'),
(265, 5, 1, '0611180335_5ee1f3677ba69.png', 4, '2020-06-19 03:54:39', 'productimage'),
(282, 14, 1, '0611181141_5ee1f54e001e1.png', 1, '2020-06-19 03:54:39', 'productimage'),
(283, 14, 1, '0611181140_5ee1f54c952a3.png', 2, '2020-06-19 03:54:39', 'productimage'),
(284, 14, 1, '0611181143_5ee1f54f4da6f.png', 3, '2020-06-19 03:54:39', 'productimage'),
(285, 14, 1, '0611181145_5ee1f551f385d.png', 4, '2020-06-19 03:54:39', 'productimage'),
(294, 13, 1, '0611180951_5ee1f4dfccea4.png', 1, '2020-06-19 03:54:39', 'productimage'),
(295, 13, 1, '0611180950_5ee1f4dea01b8.png', 2, '2020-06-19 03:54:39', 'productimage'),
(296, 13, 1, '0611180953_5ee1f4e1415f4.png', 3, '2020-06-19 03:54:39', 'productimage'),
(297, 13, 1, '0611180959_5ee1f4e790f1c.png', 4, '2020-06-19 03:54:39', 'productimage'),
(306, 12, 1, '0611171916_5ee1e90471919.png', 1, '2020-06-19 03:54:39', 'productimage'),
(307, 12, 1, '0611175955_5ee1f28b6b8a6.png', 2, '2020-06-19 03:54:39', 'productimage'),
(308, 12, 1, '0611171921_5ee1e9096eefe.png', 3, '2020-06-19 03:54:39', 'productimage'),
(309, 12, 1, '0611172003_5ee1e933d6541.png', 4, '2020-06-19 03:54:39', 'productimage'),
(318, 10, 1, '0611181057_5ee1f521901aa.png', 1, '2020-06-19 03:54:39', 'productimage'),
(319, 10, 1, '0611181054_5ee1f51e8c2a7.png', 2, '2020-06-19 03:54:39', 'productimage'),
(320, 10, 1, '0611181059_5ee1f523647b8.png', 3, '2020-06-19 03:54:39', 'productimage'),
(321, 10, 1, '0611181036_5ee1f50c9867e.png', 4, '2020-06-19 03:54:39', 'productimage'),
(330, 9, 1, '0611180052_5ee1f2c495385.png', 1, '2020-06-19 03:54:39', 'productimage'),
(331, 9, 1, '0611180047_5ee1f2bfa3699.png', 2, '2020-06-19 03:54:39', 'productimage'),
(332, 9, 1, '0611180053_5ee1f2c5c043f.png', 3, '2020-06-19 03:54:39', 'productimage'),
(333, 9, 1, '0611180125_5ee1f2e5e008e.png', 4, '2020-06-19 03:54:39', 'productimage'),
(342, 6, 1, '0611180238_5ee1f32e300ab.png', 1, '2020-06-19 03:54:39', 'productimage'),
(343, 6, 1, '0611180234_5ee1f32a5d8e3.png', 2, '2020-06-19 03:54:39', 'productimage'),
(344, 6, 1, '0611180240_5ee1f3301aada.png', 3, '2020-06-19 03:54:39', 'productimage'),
(345, 6, 1, '0611180225_5ee1f321ed961.png', 4, '2020-06-19 03:54:39', 'productimage'),
(354, 8, 1, '0611180639_5ee1f41f3cb19.png', 1, '2020-06-19 03:54:40', 'productimage'),
(355, 8, 1, '0611180637_5ee1f41dc3fb1.png', 2, '2020-06-19 03:54:40', 'productimage'),
(356, 8, 1, '0611180640_5ee1f420cf168.png', 3, '2020-06-19 03:54:40', 'productimage'),
(357, 8, 1, '0611180645_5ee1f425003a1.png', 4, '2020-06-19 03:54:40', 'productimage'),
(367, 15, 1, '0611161225_5ee1d9591a4c7.png', 1, '2020-06-19 03:54:40', 'productimage'),
(368, 15, 1, '0610183312_5ee0a8d8ca2bf.png', 2, '2020-06-19 03:54:40', 'productimage'),
(369, 15, 1, '0610183312_5ee0a8d8cd318.png', 3, '2020-06-19 03:54:40', 'productimage'),
(376, 4, 1, '0606154607_5edb3bafce37b.png', 1, '2020-06-19 03:54:40', 'productimage'),
(377, 4, 1, '0613120457_5ee4425941906.png', 2, '2020-06-19 03:54:40', 'productimage'),
(378, 4, 1, '0613120613_5ee442a55c319.png', 3, '2020-06-19 03:54:40', 'productimage');

-- --------------------------------------------------------

--
-- Table structure for table `dtb_product_stock`
--

CREATE TABLE `dtb_product_stock` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_class_id` int(10) UNSIGNED DEFAULT NULL,
  `creator_id` int(10) UNSIGNED DEFAULT NULL,
  `stock` decimal(10,0) DEFAULT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtb_product_stock`
--

INSERT INTO `dtb_product_stock` (`id`, `product_class_id`, `creator_id`, `stock`, `create_date`, `update_date`, `discriminator_type`) VALUES
(11, 11, 1, '1', '2017-03-07 10:14:52', '2020-07-15 10:33:14', 'productstock'),
(23, 23, 1, '100', '2020-04-29 00:06:19', '2020-05-15 01:39:40', 'productstock'),
(24, 24, 1, '100', '2020-05-10 03:52:28', '2020-05-15 01:42:53', 'productstock'),
(25, 25, 1, '100', '2020-05-10 04:01:39', '2020-05-15 01:46:30', 'productstock'),
(26, 26, 1, '100', '2020-05-10 04:13:02', '2020-05-15 01:55:37', 'productstock'),
(27, 27, 1, '100', '2020-05-10 04:14:21', '2020-05-15 01:54:36', 'productstock'),
(28, 28, 1, '100', '2020-05-10 04:21:25', '2020-05-15 01:47:25', 'productstock'),
(29, 29, 1, '100', '2020-05-10 04:23:31', '2020-05-15 01:51:12', 'productstock'),
(30, 30, 1, '100', '2020-05-10 04:24:53', '2020-05-15 01:53:16', 'productstock'),
(31, 31, 1, '100', '2020-05-10 04:28:10', '2020-05-15 01:52:16', 'productstock'),
(32, 32, 1, '100', '2020-05-10 04:31:16', '2020-05-15 01:48:51', 'productstock'),
(33, 33, 1, '100', '2020-05-10 04:36:03', '2020-05-14 22:29:13', 'productstock'),
(34, 34, 1, '5', '2020-05-10 04:42:02', '2020-05-10 04:42:11', 'productstock'),
(35, 35, 1, '5', '2020-05-10 04:42:02', '2020-05-10 04:42:11', 'productstock'),
(36, 36, 1, NULL, '2020-05-14 22:26:10', '2020-05-27 02:20:30', 'productstock'),
(37, 37, 1, NULL, '2020-05-14 22:26:10', '2020-05-14 22:26:10', 'productstock'),
(38, 36, 1, '5', '2020-05-14 22:30:04', '2020-05-14 22:30:04', 'productstock'),
(39, 37, 1, NULL, '2020-05-14 22:30:04', '2020-05-14 22:30:04', 'productstock'),
(40, 38, 1, '1', '2020-05-15 01:41:10', '2020-08-20 06:16:46', 'productstock'),
(41, 39, 1, '1', '2020-05-15 01:41:10', '2020-08-20 06:16:46', 'productstock'),
(42, 40, 1, NULL, '2020-05-15 01:46:01', '2020-05-15 01:46:01', 'productstock'),
(43, 41, 1, NULL, '2020-05-15 01:46:01', '2020-05-15 01:46:01', 'productstock'),
(44, 42, 1, NULL, '2020-05-15 01:47:02', '2020-05-15 01:47:02', 'productstock'),
(45, 43, 1, NULL, '2020-05-15 01:47:02', '2020-05-15 01:47:02', 'productstock'),
(46, 44, 1, '3', '2020-05-15 01:47:58', '2020-09-17 06:20:57', 'productstock'),
(47, 45, 1, NULL, '2020-05-15 01:47:58', '2020-05-15 01:47:58', 'productstock'),
(48, 46, 1, NULL, '2020-05-15 01:50:03', '2020-05-15 01:50:03', 'productstock'),
(49, 47, 1, NULL, '2020-05-15 01:50:03', '2020-05-15 01:50:03', 'productstock'),
(50, 48, 1, NULL, '2020-05-15 01:51:51', '2020-05-15 01:51:51', 'productstock'),
(51, 49, 1, NULL, '2020-05-15 01:51:51', '2020-05-15 01:51:51', 'productstock'),
(52, 50, 1, NULL, '2020-05-15 01:52:49', '2020-05-15 01:52:49', 'productstock'),
(53, 51, 1, NULL, '2020-05-15 01:52:49', '2020-05-15 01:52:49', 'productstock'),
(54, 52, 1, NULL, '2020-05-15 01:53:59', '2020-05-15 01:53:59', 'productstock'),
(55, 53, 1, NULL, '2020-05-15 01:53:59', '2020-05-15 01:53:59', 'productstock'),
(56, 54, 1, NULL, '2020-05-15 01:55:04', '2020-05-15 01:55:04', 'productstock'),
(57, 55, 1, NULL, '2020-05-15 01:55:04', '2020-05-15 01:55:04', 'productstock'),
(58, 56, 1, NULL, '2020-05-15 01:56:25', '2020-05-15 01:56:25', 'productstock'),
(59, 57, 1, NULL, '2020-05-15 01:56:25', '2020-05-15 01:56:25', 'productstock'),
(60, 58, 1, '100', '2020-06-10 09:33:12', '2020-06-10 09:33:12', 'productstock'),
(61, 59, 1, '1', '2020-06-10 09:33:12', '2020-08-20 06:17:39', 'productstock'),
(62, 60, 1, '1', '2020-06-10 09:33:12', '2020-08-20 06:17:39', 'productstock'),
(63, 61, 1, '100', '2020-06-10 09:33:12', '2020-06-10 09:33:12', 'productstock');

-- --------------------------------------------------------

--
-- Table structure for table `dtb_product_tag`
--

CREATE TABLE `dtb_product_tag` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `tag_id` int(10) UNSIGNED DEFAULT NULL,
  `creator_id` int(10) UNSIGNED DEFAULT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtb_product_tag`
--

INSERT INTO `dtb_product_tag` (`id`, `product_id`, `tag_id`, `creator_id`, `create_date`, `discriminator_type`) VALUES
(405, 2, 2, 1, '2020-07-15 10:33:14', 'producttag'),
(450, 9, 2, 1, '2020-08-02 12:05:04', 'producttag'),
(451, 8, 2, 1, '2020-08-02 12:14:26', 'producttag'),
(452, 13, 2, 1, '2020-08-04 05:32:14', 'producttag'),
(453, 6, 2, 1, '2020-08-04 05:33:05', 'producttag'),
(454, 12, 2, 1, '2020-08-04 05:34:29', 'producttag'),
(455, 11, 2, 1, '2020-08-04 05:35:11', 'producttag'),
(456, 5, 2, 1, '2020-08-04 05:35:49', 'producttag'),
(457, 10, 2, 1, '2020-08-04 05:36:40', 'producttag'),
(458, 14, 2, 1, '2020-08-04 05:37:13', 'producttag'),
(459, 15, 6, 1, '2020-08-04 05:38:03', 'producttag'),
(460, 15, 5, 1, '2020-08-04 05:38:03', 'producttag'),
(461, 15, 4, 1, '2020-08-04 05:38:03', 'producttag'),
(462, 15, 3, 1, '2020-08-04 05:38:03', 'producttag'),
(463, 4, 6, 1, '2020-08-04 05:38:35', 'producttag'),
(464, 4, 5, 1, '2020-08-04 05:38:35', 'producttag'),
(465, 4, 4, 1, '2020-08-04 05:38:35', 'producttag'),
(466, 4, 3, 1, '2020-08-04 05:38:35', 'producttag');

-- --------------------------------------------------------

--
-- Table structure for table `dtb_shipping`
--

CREATE TABLE `dtb_shipping` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED DEFAULT NULL,
  `country_id` smallint(5) UNSIGNED DEFAULT NULL,
  `pref_id` smallint(5) UNSIGNED DEFAULT NULL,
  `delivery_id` int(10) UNSIGNED DEFAULT NULL,
  `creator_id` int(10) UNSIGNED DEFAULT NULL,
  `name01` varchar(255) NOT NULL,
  `name02` varchar(255) NOT NULL,
  `kana01` varchar(255) DEFAULT NULL,
  `kana02` varchar(255) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `phone_number` varchar(14) DEFAULT NULL,
  `postal_code` varchar(8) DEFAULT NULL,
  `addr01` varchar(255) DEFAULT NULL,
  `addr02` varchar(255) DEFAULT NULL,
  `delivery_name` varchar(255) DEFAULT NULL,
  `time_id` int(10) UNSIGNED DEFAULT NULL,
  `delivery_time` varchar(255) DEFAULT NULL,
  `delivery_date` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `shipping_date` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `tracking_number` varchar(255) DEFAULT NULL,
  `note` varchar(4000) DEFAULT NULL,
  `sort_no` smallint(5) UNSIGNED DEFAULT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `mail_send_date` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtb_shipping`
--

INSERT INTO `dtb_shipping` (`id`, `order_id`, `country_id`, `pref_id`, `delivery_id`, `creator_id`, `name01`, `name02`, `kana01`, `kana02`, `company_name`, `phone_number`, `postal_code`, `addr01`, `addr02`, `delivery_name`, `time_id`, `delivery_time`, `delivery_date`, `shipping_date`, `tracking_number`, `note`, `sort_no`, `create_date`, `update_date`, `mail_send_date`, `discriminator_type`) VALUES
(1, 1, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'サンプル業者', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-01 08:34:15', '2020-05-01 08:34:15', NULL, 'shipping'),
(2, 2, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'サンプル業者', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-01 10:21:41', '2020-05-01 10:21:41', NULL, 'shipping'),
(3, 3, NULL, 13, 2, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'サンプル宅配 (定期購買)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-15 00:56:37', '2020-05-15 01:00:32', NULL, 'shipping'),
(11, 11, NULL, 13, 2, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'サンプル宅配 (定期購買)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-15 05:56:00', '2020-05-15 05:56:00', NULL, 'shipping'),
(12, 12, NULL, 13, 2, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'サンプル宅配 (定期購買)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-15 05:58:24', '2020-05-15 05:58:24', NULL, 'shipping'),
(13, 13, NULL, 13, 2, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'サンプル宅配 (定期購買)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-15 06:17:31', '2020-05-15 06:17:31', NULL, 'shipping'),
(14, 14, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'サンプル宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-15 06:21:21', '2020-05-15 06:21:21', NULL, 'shipping'),
(15, 15, NULL, 13, 2, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'サンプル宅配 (定期購買)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-16 10:34:59', '2020-05-16 10:34:59', NULL, 'shipping'),
(16, 16, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'サンプル宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-16 10:38:03', '2020-05-16 10:38:03', NULL, 'shipping'),
(17, 17, NULL, 13, 2, NULL, 'Gues', 'User', 'グエスト', 'ユーザ', NULL, '0123456789', '1570077', '世田谷区鎌田', 'Test, 99', 'サンプル宅配 (定期購買)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-17 02:08:21', '2020-05-17 02:08:21', NULL, 'shipping'),
(18, 18, NULL, 13, 1, NULL, 'Gues', 'User', 'グエスト', 'ユーザ', NULL, '0123456789', '1570077', '世田谷区鎌田', 'Test, 99', 'サンプル宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-17 02:09:43', '2020-05-17 02:09:43', NULL, 'shipping'),
(19, 19, NULL, 13, 2, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'サンプル宅配 (定期購買)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-22 03:44:49', '2020-05-22 03:44:49', NULL, 'shipping'),
(20, 20, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'サンプル宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-23 07:21:25', '2020-05-23 07:21:25', NULL, 'shipping'),
(21, 21, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'サンプル宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-24 08:47:10', '2020-05-24 08:47:10', NULL, 'shipping'),
(22, 22, NULL, 13, 2, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'サンプル宅配 (定期購買)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-26 12:05:15', '2020-05-26 12:05:15', NULL, 'shipping'),
(23, 23, NULL, 13, 2, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'サンプル宅配 (定期購買)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-27 11:46:11', '2020-05-27 11:46:11', NULL, 'shipping'),
(24, 24, NULL, 13, 2, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'サンプル宅配 (定期購買)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-03 07:17:59', '2020-06-03 07:17:59', NULL, 'shipping'),
(25, 25, NULL, 13, 2, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-03 07:24:32', '2020-06-03 07:24:32', NULL, 'shipping'),
(26, 26, NULL, 13, 1, NULL, 'Test2', 'Test', 'カナ', 'カナ', NULL, '11122223333', '1000001', '千代田区千代田', '1-1-1', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-03 10:21:34', '2020-06-03 10:21:34', NULL, 'shipping'),
(27, 27, NULL, 13, 1, NULL, 'Test2', 'Test', 'カナ', 'カナ', NULL, '11122223333', '1000001', '千代田区千代田', '1-1-1', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-03 14:27:51', '2020-06-03 14:27:51', NULL, 'shipping'),
(28, 28, NULL, 13, 1, NULL, 'Test2', 'Test', 'カナ', 'カナ', NULL, '11122223333', '1000001', '千代田区千代田', '1-1-1', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-03 14:52:55', '2020-06-03 14:52:55', NULL, 'shipping'),
(29, 29, NULL, 13, 1, NULL, 'Test2', 'Test', 'カナ', 'カナ', NULL, '11122223333', '1000001', '千代田区千代田', '1-1-1', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-03 14:53:38', '2020-06-03 14:53:38', NULL, 'shipping'),
(30, 30, NULL, 13, 1, NULL, 'Test2', 'Test', 'カナ', 'カナ', NULL, '11122223333', '1000001', '千代田区千代田', '1-1-1', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-03 14:55:48', '2020-06-03 14:55:48', NULL, 'shipping'),
(31, 31, NULL, 13, 1, NULL, 'Test2', 'Test', 'カナ', 'カナ', NULL, '11122223333', '1000001', '千代田区千代田', '1-1-1', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-03 15:04:08', '2020-06-03 15:04:08', NULL, 'shipping'),
(32, 32, NULL, 13, 1, NULL, 'Test2', 'Test', 'カナ', 'カナ', NULL, '11122223333', '1000001', '千代田区千代田', '1-1-1', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-03 15:05:19', '2020-06-03 15:05:19', NULL, 'shipping'),
(33, 33, NULL, 13, 1, NULL, 'Test2', 'Test', 'カナ', 'カナ', NULL, '11122223333', '1000001', '千代田区千代田', '1-1-1', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-03 15:06:18', '2020-06-03 15:06:18', NULL, 'shipping'),
(34, 34, NULL, 13, 1, NULL, 'Test2', 'Test', 'カナ', 'カナ', NULL, '11122223333', '1000001', '千代田区千代田', '1-1-1', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-03 15:07:46', '2020-06-03 15:07:46', NULL, 'shipping'),
(35, 35, NULL, 13, 2, NULL, 'Test2', 'Test', 'カナ', 'カナ', NULL, '11122223333', '1000001', '千代田区千代田', '1-1-1', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-03 15:08:05', '2020-06-03 15:08:05', NULL, 'shipping'),
(36, 36, NULL, 13, 1, NULL, 'Test2', 'Test', 'カナ', 'カナ', NULL, '11122223333', '1000001', '千代田区千代田', '1-1-1', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-03 15:55:19', '2020-06-03 15:55:19', NULL, 'shipping'),
(37, 37, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-04 06:42:16', '2020-06-04 06:42:16', NULL, 'shipping'),
(38, 38, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-04 06:42:56', '2020-06-04 06:42:56', NULL, 'shipping'),
(39, 39, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-04 06:43:15', '2020-06-04 06:43:15', NULL, 'shipping'),
(40, 40, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-05 08:12:29', '2020-06-05 08:12:29', NULL, 'shipping'),
(41, 41, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-05 09:06:16', '2020-06-05 09:06:16', NULL, 'shipping'),
(42, 42, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-05 09:06:42', '2020-06-05 09:06:42', NULL, 'shipping'),
(43, 43, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-05 09:10:56', '2020-06-05 09:10:56', NULL, 'shipping'),
(44, 44, NULL, 13, 2, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-05 09:19:05', '2020-06-05 09:19:05', NULL, 'shipping'),
(45, 45, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-05 09:25:32', '2020-06-05 09:25:32', NULL, 'shipping'),
(46, 46, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 12:10:56', '2020-06-11 12:10:56', NULL, 'shipping'),
(47, 47, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 12:12:30', '2020-06-11 12:12:30', NULL, 'shipping'),
(48, 48, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 12:12:58', '2020-06-11 12:12:58', NULL, 'shipping'),
(49, 49, NULL, 13, 1, NULL, 'Test3', 'Test', 'カナ', 'カナ', NULL, '11122223333', '1000001', '千代田区千代田', '1-1-1', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 13:50:56', '2020-06-11 13:50:56', NULL, 'shipping'),
(50, 50, NULL, 13, 1, NULL, 'Test3', 'Test', 'カナ', 'カナ', NULL, '11122223333', '1000001', '千代田区千代田', '1-1-1', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 13:51:45', '2020-06-11 13:51:45', NULL, 'shipping'),
(51, 51, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 13:52:37', '2020-06-11 13:52:37', NULL, 'shipping'),
(52, 52, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-12 02:25:38', '2020-06-12 02:25:38', NULL, 'shipping'),
(53, 53, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-12 09:19:42', '2020-06-12 09:19:42', NULL, 'shipping'),
(54, 54, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-12 09:25:25', '2020-06-12 09:25:25', NULL, 'shipping'),
(55, 55, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-13 03:20:44', '2020-06-13 03:20:44', NULL, 'shipping'),
(56, 56, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-13 03:24:07', '2020-06-13 03:24:07', NULL, 'shipping'),
(57, 57, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-13 03:24:44', '2020-06-13 03:24:44', NULL, 'shipping'),
(58, 58, NULL, 13, 2, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-13 03:25:47', '2020-06-13 03:25:47', NULL, 'shipping'),
(59, 59, NULL, 13, 2, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-13 03:26:37', '2020-06-13 03:26:37', NULL, 'shipping'),
(60, 60, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-13 03:29:04', '2020-06-13 03:29:04', NULL, 'shipping'),
(61, 61, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-13 03:29:28', '2020-06-13 03:29:28', NULL, 'shipping'),
(62, 62, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-14 05:46:34', '2020-06-14 05:46:34', NULL, 'shipping'),
(63, 63, NULL, 13, 1, NULL, 'Sugimori', 'Koichiro', 'スギモリ', 'コウイチロウ', 'Personal Media International', '+447803924335', '1520004', '目黒区鷹番', '1-8-17', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-17 08:34:08', '2020-06-17 08:34:08', NULL, 'shipping'),
(64, 64, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-17 11:27:16', '2020-06-17 11:27:16', NULL, 'shipping'),
(65, 65, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-17 12:52:17', '2020-06-17 12:52:17', NULL, 'shipping'),
(66, 66, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-26 01:51:01', '2020-06-26 01:51:01', NULL, 'shipping'),
(67, 67, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-26 04:20:30', '2020-06-26 04:20:30', NULL, 'shipping'),
(68, 68, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-26 08:19:22', '2020-06-26 08:19:22', NULL, 'shipping'),
(69, 69, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-26 08:20:44', '2020-06-26 08:20:44', NULL, 'shipping'),
(70, 70, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-26 08:38:41', '2020-06-26 08:38:41', NULL, 'shipping'),
(71, 71, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-26 09:01:33', '2020-06-26 09:01:33', NULL, 'shipping'),
(72, 72, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-30 11:01:54', '2020-06-30 11:01:54', NULL, 'shipping'),
(73, 73, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-30 11:44:45', '2020-06-30 11:44:45', NULL, 'shipping'),
(74, 74, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-02 02:48:28', '2020-07-02 02:48:28', NULL, 'shipping'),
(75, 75, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-02 02:51:51', '2020-07-02 02:51:51', NULL, 'shipping'),
(76, 76, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-16 08:56:03', '2020-07-16 08:56:03', NULL, 'shipping'),
(77, 77, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-16 09:06:59', '2020-07-16 09:06:59', NULL, 'shipping'),
(78, 78, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-16 09:37:36', '2020-07-16 09:37:36', NULL, 'shipping'),
(79, 79, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-16 09:39:53', '2020-07-16 09:39:53', NULL, 'shipping'),
(80, 80, NULL, 13, 1, 1, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, '2020-07-16 12:18:51', NULL, NULL, NULL, '2020-07-16 09:57:45', '2020-07-16 12:18:51', '2020-07-16 12:18:51', 'shipping'),
(81, 81, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-16 10:16:15', '2020-07-16 10:16:15', NULL, 'shipping'),
(82, 82, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-16 12:21:13', '2020-07-16 12:21:13', NULL, 'shipping'),
(83, 83, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-16 13:12:52', '2020-07-16 13:12:52', NULL, 'shipping'),
(84, 84, NULL, 13, 1, 1, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', 2, '午後', '2020-07-30 15:00:00', '2020-07-21 07:40:48', 'SAGAWA-123456789', 'EMS parcel', NULL, '2020-07-17 08:12:59', '2020-07-24 07:30:48', NULL, 'shipping'),
(85, 85, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', 'スタンダード宅配', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-17 08:18:37', '2020-07-17 08:18:37', NULL, 'shipping'),
(86, 86, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', '佐川急便', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-03 03:09:06', '2020-08-03 03:09:06', NULL, 'shipping'),
(87, 87, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', '佐川急便', 11, '19時～21時', '2020-08-23 15:00:00', NULL, NULL, NULL, NULL, '2020-08-03 13:20:51', '2020-08-03 13:21:24', NULL, 'shipping'),
(88, 88, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', '佐川急便', 6, '午前中（8時～12時）', '2020-08-04 15:00:00', NULL, NULL, NULL, NULL, '2020-08-04 03:32:45', '2020-08-04 09:48:42', NULL, 'shipping'),
(89, 89, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', '佐川急便', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-05 09:06:03', '2020-08-05 09:06:03', NULL, 'shipping'),
(90, 90, NULL, 13, 1, NULL, 'Test', 'Test', 'テスト', 'テスト', NULL, '11122223333', '1000001', '千代田区千代田', '1-1-1', '佐川急便', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-05 10:20:48', '2020-08-05 10:20:48', NULL, 'shipping'),
(91, 91, NULL, 13, 1, 1, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', '佐川急便', 6, '午前中（8時～12時）', '2020-08-26 15:00:00', '2020-08-06 10:41:39', '123456', NULL, NULL, '2020-08-05 11:48:04', '2020-08-06 10:41:39', NULL, 'shipping'),
(92, 92, NULL, 13, 1, NULL, 'Test2', 'Test', 'カナ', 'カナ', NULL, '11122223333', '1000001', '千代田区千代田', '1-1-1', '佐川急便', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-05 14:34:22', '2020-08-05 14:34:22', NULL, 'shipping'),
(93, 93, NULL, 13, 1, NULL, 'Test2', 'Test', 'カナ', 'カナ', NULL, '11122223333', '1000001', '千代田区千代田', '1-1-1', '佐川急便', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-05 14:34:47', '2020-08-05 14:34:47', NULL, 'shipping'),
(94, 94, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', '佐川急便', 11, '19時～21時', '2020-08-26 15:00:00', NULL, NULL, NULL, NULL, '2020-08-06 11:22:49', '2020-08-06 11:27:59', NULL, 'shipping'),
(95, 95, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', '佐川急便', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-06 12:04:55', '2020-08-06 12:04:55', NULL, 'shipping'),
(96, 96, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', '佐川急便', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-06 12:06:37', '2020-08-06 12:06:37', NULL, 'shipping'),
(97, 97, NULL, 13, 1, NULL, 'Test2', 'Test', 'カナ', 'カナ', NULL, '11122223333', '1000001', '千代田区千代田', '1-1-1', '佐川急便', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-06 12:32:08', '2020-08-06 12:32:08', NULL, 'shipping'),
(98, 98, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', '佐川急便', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-13 07:55:09', '2020-08-13 07:55:09', NULL, 'shipping'),
(99, 99, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', '佐川急便', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-17 10:27:49', '2020-08-17 10:27:49', NULL, 'shipping'),
(100, 100, NULL, 13, 1, NULL, 'Test', 'Test', 'テスト', 'テスト', NULL, '11122223333', '1000000', '千代田区', '1-1-1', '佐川急便', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-18 14:38:23', '2020-08-18 14:38:23', NULL, 'shipping'),
(101, 101, NULL, 13, 1, NULL, 'Test', 'Test', 'テスト', 'テスト', NULL, '11122223333', '1000000', '千代田区', '1-1-1', '佐川急便', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-18 14:44:50', '2020-08-18 14:44:50', NULL, 'shipping'),
(102, 102, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', '佐川急便', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-20 08:48:38', '2020-08-20 08:48:38', NULL, 'shipping'),
(103, 103, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', '佐川急便', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-20 23:14:54', '2020-08-20 23:14:54', NULL, 'shipping'),
(104, 104, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', '佐川急便', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-25 00:26:39', '2020-08-25 00:26:39', NULL, 'shipping'),
(105, 105, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', '佐川急便', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-28 01:56:54', '2020-08-28 01:56:54', NULL, 'shipping'),
(106, 106, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', '佐川急便', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-30 07:21:49', '2020-08-30 07:21:49', NULL, 'shipping'),
(107, 107, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', '佐川急便', 7, '12時～14時', '2020-08-30 15:00:00', NULL, NULL, NULL, NULL, '2020-08-30 07:34:28', '2020-08-30 07:34:47', NULL, 'shipping'),
(108, 108, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', '佐川急便', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-30 07:48:34', '2020-08-30 07:48:34', NULL, 'shipping'),
(109, 109, NULL, 13, 1, NULL, 'Test', 'Test', 'テスト', 'テスト', NULL, '11122223333', '1000001', '千代田区千代田', '1-1-1', '佐川急便', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-01 11:23:54', '2020-09-01 11:23:54', NULL, 'shipping'),
(110, 110, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', '佐川急便', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-02 04:14:31', '2020-09-02 04:14:31', NULL, 'shipping'),
(111, 111, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', '佐川急便', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-02 05:09:55', '2020-09-02 05:09:55', NULL, 'shipping'),
(112, 112, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', '佐川急便', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-02 06:34:05', '2020-09-02 06:34:05', NULL, 'shipping'),
(113, 113, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', '佐川急便', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-03 07:46:56', '2020-09-03 07:46:56', NULL, 'shipping'),
(114, 114, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', '佐川急便', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-04 01:00:53', '2020-09-04 01:00:53', NULL, 'shipping'),
(115, 115, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', '佐川急便', 8, '14時～16時', '2020-09-12 15:00:00', NULL, NULL, NULL, NULL, '2020-09-10 00:22:18', '2020-09-10 00:23:18', NULL, 'shipping'),
(116, 116, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', '佐川急便', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-10 23:51:39', '2020-09-10 23:51:39', NULL, 'shipping'),
(117, 117, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', '佐川急便', 11, '19時～21時', '2020-09-20 15:00:00', NULL, NULL, NULL, NULL, '2020-09-17 03:48:51', '2020-09-17 04:04:00', NULL, 'shipping'),
(118, 118, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', '佐川急便', 9, '16時～18時', '2020-09-20 15:00:00', NULL, NULL, NULL, NULL, '2020-09-17 05:21:01', '2020-09-17 05:22:24', NULL, 'shipping'),
(119, 119, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', NULL, '0123456789', '1570077', '世田谷区鎌田', '123建物', '佐川急便', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-17 06:19:50', '2020-09-17 06:19:50', NULL, 'shipping'),
(120, 120, NULL, 1, 1, NULL, 'Test!', 'te', 'テス', 'テス', NULL, '+32123456789', '1000', 'City', 'fdgdf', '佐川急便', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-28 02:14:20', '2020-09-28 02:14:20', NULL, 'shipping'),
(121, 121, NULL, 13, 1, NULL, 'Frederic', 'Vervaecke', 'テスト', 'テスト', NULL, '0123456789', '1570077', '世田谷区鎌田', '123', '佐川急便', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-10-01 09:37:05', '2020-10-01 09:37:05', NULL, 'shipping');

-- --------------------------------------------------------

--
-- Table structure for table `dtb_tag`
--

CREATE TABLE `dtb_tag` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) UNSIGNED NOT NULL,
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtb_tag`
--

INSERT INTO `dtb_tag` (`id`, `name`, `sort_no`, `discriminator_type`) VALUES
(1, '新商品', 1, 'tag'),
(2, 'おすすめ商品', 2, 'tag'),
(3, '限定品', 3, 'tag'),
(4, '甘味料ゼロ', 4, 'tag'),
(5, '香料ゼロ', 5, 'tag'),
(6, 'カロリーゼロ', 6, 'tag'),
(7, '10ml', 7, 'tag'),
(8, '30ml', 8, 'tag'),
(9, '1%', 9, 'tag'),
(10, '2%', 10, 'tag'),
(11, '3%', 11, 'tag'),
(12, '5%', 12, 'tag'),
(13, '10%', 13, 'tag');

-- --------------------------------------------------------

--
-- Table structure for table `dtb_tax_rule`
--

CREATE TABLE `dtb_tax_rule` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_class_id` int(10) UNSIGNED DEFAULT NULL,
  `creator_id` int(10) UNSIGNED DEFAULT NULL,
  `country_id` smallint(5) UNSIGNED DEFAULT NULL,
  `pref_id` smallint(5) UNSIGNED DEFAULT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `rounding_type_id` smallint(5) UNSIGNED DEFAULT NULL,
  `tax_rate` decimal(10,0) UNSIGNED NOT NULL DEFAULT '8',
  `tax_adjust` decimal(10,0) UNSIGNED NOT NULL DEFAULT '0',
  `apply_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtb_tax_rule`
--

INSERT INTO `dtb_tax_rule` (`id`, `product_class_id`, `creator_id`, `country_id`, `pref_id`, `product_id`, `rounding_type_id`, `tax_rate`, `tax_adjust`, `apply_date`, `create_date`, `update_date`, `discriminator_type`) VALUES
(1, NULL, 1, NULL, NULL, NULL, 1, '10', '0', '2017-03-07 10:14:52', '2017-03-07 10:14:52', '2020-07-16 09:29:54', 'taxrule');

-- --------------------------------------------------------

--
-- Table structure for table `dtb_template`
--

CREATE TABLE `dtb_template` (
  `id` int(10) UNSIGNED NOT NULL,
  `device_type_id` smallint(5) UNSIGNED DEFAULT NULL,
  `template_code` varchar(255) NOT NULL,
  `template_name` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtb_template`
--

INSERT INTO `dtb_template` (`id`, `device_type_id`, `template_code`, `template_name`, `create_date`, `update_date`, `discriminator_type`) VALUES
(1, 10, 'default', 'デフォルト', '2017-03-07 10:14:52', '2017-03-07 10:14:52', 'template'),
(2, 10, 'maritime', 'Maritime Template', '2020-04-26 04:12:10', '2020-04-26 04:12:10', 'template');

-- --------------------------------------------------------

--
-- Table structure for table `migration_ContactManagement4`
--

CREATE TABLE `migration_ContactManagement4` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migration_ContactManagement4`
--

INSERT INTO `migration_ContactManagement4` (`version`) VALUES
('20190703085414');

-- --------------------------------------------------------

--
-- Table structure for table `mtb_authority`
--

CREATE TABLE `mtb_authority` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) UNSIGNED NOT NULL,
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mtb_authority`
--

INSERT INTO `mtb_authority` (`id`, `name`, `sort_no`, `discriminator_type`) VALUES
(0, 'システム管理者', 0, 'authority'),
(1, '店舗オーナー', 1, 'authority');

-- --------------------------------------------------------

--
-- Table structure for table `mtb_country`
--

CREATE TABLE `mtb_country` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) UNSIGNED NOT NULL,
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mtb_country`
--

INSERT INTO `mtb_country` (`id`, `name`, `sort_no`, `discriminator_type`) VALUES
(4, 'アフガニスタン', 4, 'country'),
(8, 'アルバニア', 12, 'country'),
(10, '南極', 149, 'country'),
(12, 'アルジェリア', 9, 'country'),
(16, 'アメリカ領サモア', 7, 'country'),
(20, 'アンドラ', 17, 'country'),
(24, 'アンゴラ', 15, 'country'),
(28, 'アンティグア・バーブーダ', 16, 'country'),
(31, 'アゼルバイジャン', 3, 'country'),
(32, 'アルゼンチン', 10, 'country'),
(36, 'オーストラリア', 39, 'country'),
(40, 'オーストリア', 40, 'country'),
(44, 'バハマ', 167, 'country'),
(48, 'バーレーン', 161, 'country'),
(50, 'バングラデシュ', 175, 'country'),
(51, 'アルメニア', 13, 'country'),
(52, 'バルバドス', 172, 'country'),
(56, 'ベルギー', 201, 'country'),
(60, 'バミューダ諸島|バミューダ', 169, 'country'),
(64, 'ブータン', 181, 'country'),
(68, 'ボリビア|ボリビア多民族国', 206, 'country'),
(70, 'ボスニア・ヘルツェゴビナ', 203, 'country'),
(72, 'ボツワナ', 204, 'country'),
(74, 'ブーベ島', 182, 'country'),
(76, 'ブラジル', 186, 'country'),
(84, 'ベリーズ', 199, 'country'),
(86, 'イギリス領インド洋地域', 20, 'country'),
(90, 'ソロモン諸島', 121, 'country'),
(92, 'イギリス領ヴァージン諸島', 21, 'country'),
(96, 'ブルネイ|ブルネイ・ダルサラーム', 193, 'country'),
(100, 'ブルガリア', 191, 'country'),
(104, 'ミャンマー', 224, 'country'),
(108, 'ブルンジ', 194, 'country'),
(112, 'ベラルーシ', 198, 'country'),
(116, 'カンボジア', 55, 'country'),
(120, 'カメルーン', 53, 'country'),
(124, 'カナダ', 51, 'country'),
(132, 'カーボベルデ', 45, 'country'),
(136, 'ケイマン諸島', 75, 'country'),
(140, '中央アフリカ共和国', 130, 'country'),
(144, 'スリランカ', 108, 'country'),
(148, 'チャド', 129, 'country'),
(152, 'チリ', 134, 'country'),
(156, '中華人民共和国|中国', 131, 'country'),
(158, '台湾', 125, 'country'),
(162, 'クリスマス島 (オーストラリア)|クリスマス島', 71, 'country'),
(166, 'ココス諸島|ココス（キーリング）諸島', 78, 'country'),
(170, 'コロンビア', 81, 'country'),
(174, 'コモロ', 80, 'country'),
(175, 'マヨット', 214, 'country'),
(178, 'コンゴ共和国', 82, 'country'),
(180, 'コンゴ民主共和国', 83, 'country'),
(184, 'クック諸島', 69, 'country'),
(188, 'コスタリカ', 79, 'country'),
(191, 'クロアチア', 74, 'country'),
(192, 'キューバ', 60, 'country'),
(196, 'キプロス', 59, 'country'),
(203, 'チェコ', 128, 'country'),
(204, 'ベナン', 196, 'country'),
(208, 'デンマーク', 136, 'country'),
(212, 'ドミニカ国', 141, 'country'),
(214, 'ドミニカ共和国', 140, 'country'),
(218, 'エクアドル', 33, 'country'),
(222, 'エルサルバドル', 38, 'country'),
(226, '赤道ギニア', 113, 'country'),
(231, 'エチオピア', 36, 'country'),
(232, 'エリトリア', 37, 'country'),
(233, 'エストニア', 35, 'country'),
(234, 'フェロー諸島', 184, 'country'),
(238, 'フォークランド諸島|フォークランド（マルビナス）諸島', 185, 'country'),
(239, 'サウスジョージア・サウスサンドウィッチ諸島', 85, 'country'),
(242, 'フィジー', 178, 'country'),
(246, 'フィンランド', 180, 'country'),
(248, 'オーランド諸島', 41, 'country'),
(250, 'フランス', 187, 'country'),
(254, 'フランス領ギアナ', 188, 'country'),
(258, 'フランス領ポリネシア', 189, 'country'),
(260, 'フランス領南方・南極地域', 190, 'country'),
(262, 'ジブチ', 94, 'country'),
(266, 'ガボン', 52, 'country'),
(268, 'グルジア', 72, 'country'),
(270, 'ガンビア', 54, 'country'),
(275, 'パレスチナ', 173, 'country'),
(276, 'ドイツ', 137, 'country'),
(288, 'ガーナ', 44, 'country'),
(292, 'ジブラルタル', 95, 'country'),
(296, 'キリバス', 63, 'country'),
(300, 'ギリシャ', 62, 'country'),
(304, 'グリーンランド', 70, 'country'),
(308, 'グレナダ', 73, 'country'),
(312, 'グアドループ', 66, 'country'),
(316, 'グアム', 67, 'country'),
(320, 'グアテマラ', 65, 'country'),
(324, 'ギニア', 57, 'country'),
(328, 'ガイアナ', 47, 'country'),
(332, 'ハイチ', 162, 'country'),
(334, 'ハード島とマクドナルド諸島', 160, 'country'),
(336, 'バチカン|バチカン市国', 164, 'country'),
(340, 'ホンジュラス', 209, 'country'),
(344, '香港', 208, 'country'),
(348, 'ハンガリー', 174, 'country'),
(352, 'アイスランド', 1, 'country'),
(356, 'インド', 26, 'country'),
(360, 'インドネシア', 27, 'country'),
(364, 'イラン|イラン・イスラム共和国', 25, 'country'),
(368, 'イラク', 24, 'country'),
(372, 'アイルランド', 2, 'country'),
(376, 'イスラエル', 22, 'country'),
(380, 'イタリア', 23, 'country'),
(384, 'コートジボワール', 77, 'country'),
(388, 'ジャマイカ', 97, 'country'),
(392, '日本', 153, 'country'),
(398, 'カザフスタン', 48, 'country'),
(400, 'ヨルダン', 236, 'country'),
(404, 'ケニア', 76, 'country'),
(408, '朝鮮民主主義人民共和国', 133, 'country'),
(410, '大韓民国', 124, 'country'),
(414, 'クウェート', 68, 'country'),
(417, 'キルギス', 64, 'country'),
(418, 'ラオス|ラオス人民民主共和国', 237, 'country'),
(422, 'レバノン', 247, 'country'),
(426, 'レソト', 246, 'country'),
(428, 'ラトビア', 238, 'country'),
(430, 'リベリア', 242, 'country'),
(434, 'リビア', 240, 'country'),
(438, 'リヒテンシュタイン', 241, 'country'),
(440, 'リトアニア', 239, 'country'),
(442, 'ルクセンブルク', 244, 'country'),
(446, 'マカオ', 211, 'country'),
(450, 'マダガスカル', 213, 'country'),
(454, 'マラウイ', 215, 'country'),
(458, 'マレーシア', 219, 'country'),
(462, 'モルディブ', 230, 'country'),
(466, 'マリ共和国|マリ', 216, 'country'),
(470, 'マルタ', 217, 'country'),
(474, 'マルティニーク', 218, 'country'),
(478, 'モーリタニア', 227, 'country'),
(480, 'モーリシャス', 226, 'country'),
(484, 'メキシコ', 225, 'country'),
(492, 'モナコ', 229, 'country'),
(496, 'モンゴル国|モンゴル', 233, 'country'),
(498, 'モルドバ|モルドバ共和国', 231, 'country'),
(499, 'モンテネグロ', 234, 'country'),
(500, 'モントセラト', 235, 'country'),
(504, 'モロッコ', 232, 'country'),
(508, 'モザンビーク', 228, 'country'),
(512, 'オマーン', 42, 'country'),
(516, 'ナミビア', 148, 'country'),
(520, 'ナウル', 147, 'country'),
(524, 'ネパール', 157, 'country'),
(528, 'オランダ', 43, 'country'),
(531, 'キュラソー島|キュラソー', 61, 'country'),
(533, 'アルバ', 11, 'country'),
(534, 'シント・マールテン|シント・マールテン（オランダ領）', 100, 'country'),
(535, 'BES諸島|ボネール、シント・ユースタティウスおよびサバ', 205, 'country'),
(540, 'ニューカレドニア', 155, 'country'),
(548, 'バヌアツ', 166, 'country'),
(554, 'ニュージーランド', 156, 'country'),
(558, 'ニカラグア', 151, 'country'),
(562, 'ニジェール', 152, 'country'),
(566, 'ナイジェリア', 146, 'country'),
(570, 'ニウエ', 150, 'country'),
(574, 'ノーフォーク島', 158, 'country'),
(578, 'ノルウェー', 159, 'country'),
(580, '北マリアナ諸島', 56, 'country'),
(581, '合衆国領有小離島', 50, 'country'),
(583, 'ミクロネシア連邦', 221, 'country'),
(584, 'マーシャル諸島', 210, 'country'),
(585, 'パラオ', 170, 'country'),
(586, 'パキスタン', 163, 'country'),
(591, 'パナマ', 165, 'country'),
(598, 'パプアニューギニア', 168, 'country'),
(600, 'パラグアイ', 171, 'country'),
(604, 'ペルー', 200, 'country'),
(608, 'フィリピン', 179, 'country'),
(612, 'ピトケアン諸島|ピトケアン', 177, 'country'),
(616, 'ポーランド', 202, 'country'),
(620, 'ポルトガル', 207, 'country'),
(624, 'ギニアビサウ', 58, 'country'),
(626, '東ティモール', 176, 'country'),
(630, 'プエルトリコ', 183, 'country'),
(634, 'カタール', 49, 'country'),
(638, 'レユニオン', 248, 'country'),
(642, 'ルーマニア', 243, 'country'),
(643, 'ロシア|ロシア連邦', 249, 'country'),
(646, 'ルワンダ', 245, 'country'),
(652, 'サン・バルテルミー島|サン・バルテルミー', 88, 'country'),
(654, 'セントヘレナ・アセンションおよびトリスタンダクーニャ', 118, 'country'),
(659, 'セントクリストファー・ネイビス', 116, 'country'),
(660, 'アンギラ', 14, 'country'),
(662, 'セントルシア', 119, 'country'),
(663, 'サン・マルタン (西インド諸島)|サン・マルタン（フランス領）', 92, 'country'),
(666, 'サンピエール島・ミクロン島', 90, 'country'),
(670, 'セントビンセント・グレナディーン|セントビンセントおよびグレナディーン諸島', 117, 'country'),
(674, 'サンマリノ', 91, 'country'),
(678, 'サントメ・プリンシペ', 87, 'country'),
(682, 'サウジアラビア', 84, 'country'),
(686, 'セネガル', 114, 'country'),
(688, 'セルビア', 115, 'country'),
(690, 'セーシェル', 112, 'country'),
(694, 'シエラレオネ', 93, 'country'),
(702, 'シンガポール', 99, 'country'),
(703, 'スロバキア', 109, 'country'),
(704, 'ベトナム', 195, 'country'),
(705, 'スロベニア', 110, 'country'),
(706, 'ソマリア', 120, 'country'),
(710, '南アフリカ共和国|南アフリカ', 222, 'country'),
(716, 'ジンバブエ', 101, 'country'),
(724, 'スペイン', 106, 'country'),
(728, '南スーダン', 223, 'country'),
(729, 'スーダン', 104, 'country'),
(732, '西サハラ', 154, 'country'),
(740, 'スリナム', 107, 'country'),
(744, 'スヴァールバル諸島およびヤンマイエン島', 105, 'country'),
(748, 'スワジランド', 111, 'country'),
(752, 'スウェーデン', 103, 'country'),
(756, 'スイス', 102, 'country'),
(760, 'シリア|シリア・アラブ共和国', 98, 'country'),
(762, 'タジキスタン', 126, 'country'),
(764, 'タイ王国|タイ', 123, 'country'),
(768, 'トーゴ', 138, 'country'),
(772, 'トケラウ', 139, 'country'),
(776, 'トンガ', 145, 'country'),
(780, 'トリニダード・トバゴ', 142, 'country'),
(784, 'アラブ首長国連邦', 8, 'country'),
(788, 'チュニジア', 132, 'country'),
(792, 'トルコ', 144, 'country'),
(795, 'トルクメニスタン', 143, 'country'),
(796, 'タークス・カイコス諸島', 122, 'country'),
(798, 'ツバル', 135, 'country'),
(800, 'ウガンダ', 29, 'country'),
(804, 'ウクライナ', 30, 'country'),
(807, 'マケドニア共和国|マケドニア旧ユーゴスラビア共和国', 212, 'country'),
(818, 'エジプト', 34, 'country'),
(826, 'イギリス', 19, 'country'),
(831, 'ガーンジー', 46, 'country'),
(832, 'ジャージー', 96, 'country'),
(833, 'マン島', 220, 'country'),
(834, 'タンザニア', 127, 'country'),
(840, 'アメリカ合衆国', 5, 'country'),
(850, 'アメリカ領ヴァージン諸島', 6, 'country'),
(854, 'ブルキナファソ', 192, 'country'),
(858, 'ウルグアイ', 32, 'country'),
(860, 'ウズベキスタン', 31, 'country'),
(862, 'ベネズエラ|ベネズエラ・ボリバル共和国', 197, 'country'),
(876, 'ウォリス・フツナ', 28, 'country'),
(882, 'サモア', 86, 'country'),
(887, 'イエメン', 18, 'country'),
(894, 'ザンビア', 89, 'country');

-- --------------------------------------------------------

--
-- Table structure for table `mtb_csv_type`
--

CREATE TABLE `mtb_csv_type` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) UNSIGNED NOT NULL,
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mtb_csv_type`
--

INSERT INTO `mtb_csv_type` (`id`, `name`, `sort_no`, `discriminator_type`) VALUES
(1, '商品CSV', 3, 'csvtype'),
(2, '会員CSV', 4, 'csvtype'),
(3, '受注CSV', 1, 'csvtype'),
(4, '配送CSV', 1, 'csvtype'),
(5, 'カテゴリCSV', 5, 'csvtype'),
(6, '商品レビューCSV', 6, 'csvtype');

-- --------------------------------------------------------

--
-- Table structure for table `mtb_customer_order_status`
--

CREATE TABLE `mtb_customer_order_status` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) UNSIGNED NOT NULL,
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mtb_customer_order_status`
--

INSERT INTO `mtb_customer_order_status` (`id`, `name`, `sort_no`, `discriminator_type`) VALUES
(1, '注文受付', 0, 'customerorderstatus'),
(3, '注文取消し', 4, 'customerorderstatus'),
(4, '注文受付', 3, 'customerorderstatus'),
(5, '発送済み', 6, 'customerorderstatus'),
(6, '注文受付', 2, 'customerorderstatus'),
(7, '注文受付', 1, 'customerorderstatus'),
(8, '注文未完了', 5, 'customerorderstatus'),
(9, '返品', 7, 'customerorderstatus'),
(10, '注文受付（予約注文）', 8, 'customerorderstatus');

-- --------------------------------------------------------

--
-- Table structure for table `mtb_customer_status`
--

CREATE TABLE `mtb_customer_status` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) UNSIGNED NOT NULL,
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mtb_customer_status`
--

INSERT INTO `mtb_customer_status` (`id`, `name`, `sort_no`, `discriminator_type`) VALUES
(1, '仮会員', 0, 'customerstatus'),
(2, '本会員', 1, 'customerstatus'),
(3, '退会', 2, 'customerstatus');

-- --------------------------------------------------------

--
-- Table structure for table `mtb_device_type`
--

CREATE TABLE `mtb_device_type` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) UNSIGNED NOT NULL,
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mtb_device_type`
--

INSERT INTO `mtb_device_type` (`id`, `name`, `sort_no`, `discriminator_type`) VALUES
(2, 'モバイル', 0, 'devicetype'),
(10, 'PC', 1, 'devicetype');

-- --------------------------------------------------------

--
-- Table structure for table `mtb_job`
--

CREATE TABLE `mtb_job` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) UNSIGNED NOT NULL,
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mtb_job`
--

INSERT INTO `mtb_job` (`id`, `name`, `sort_no`, `discriminator_type`) VALUES
(1, '公務員', 0, 'job'),
(2, 'コンサルタント', 1, 'job'),
(3, 'コンピューター関連技術職', 2, 'job'),
(4, 'コンピューター関連以外の技術職', 3, 'job'),
(5, '金融関係', 4, 'job'),
(6, '医師', 5, 'job'),
(7, '弁護士', 6, 'job'),
(8, '総務・人事・事務', 7, 'job'),
(9, '営業・販売', 8, 'job'),
(10, '研究・開発', 9, 'job'),
(11, '広報・宣伝', 10, 'job'),
(12, '企画・マーケティング', 11, 'job'),
(13, 'デザイン関係', 12, 'job'),
(14, '会社経営・役員', 13, 'job'),
(15, '出版・マスコミ関係', 14, 'job'),
(16, '学生・フリーター', 15, 'job'),
(17, '主婦', 16, 'job'),
(18, 'その他', 17, 'job');

-- --------------------------------------------------------

--
-- Table structure for table `mtb_order_item_type`
--

CREATE TABLE `mtb_order_item_type` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) UNSIGNED NOT NULL,
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mtb_order_item_type`
--

INSERT INTO `mtb_order_item_type` (`id`, `name`, `sort_no`, `discriminator_type`) VALUES
(1, '商品', 0, 'orderitemtype'),
(2, '送料', 1, 'orderitemtype'),
(3, '手数料', 2, 'orderitemtype'),
(4, '割引', 3, 'orderitemtype'),
(5, '税', 4, 'orderitemtype'),
(6, 'ポイント', 5, 'orderitemtype');

-- --------------------------------------------------------

--
-- Table structure for table `mtb_order_status`
--

CREATE TABLE `mtb_order_status` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `display_order_count` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) UNSIGNED NOT NULL,
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mtb_order_status`
--

INSERT INTO `mtb_order_status` (`id`, `display_order_count`, `name`, `sort_no`, `discriminator_type`) VALUES
(1, 1, '新規受付', 0, 'orderstatus'),
(3, 0, '注文取消し', 3, 'orderstatus'),
(4, 1, '対応中', 2, 'orderstatus'),
(5, 0, '発送済み', 4, 'orderstatus'),
(6, 1, '入金済み', 1, 'orderstatus'),
(7, 0, '決済処理中', 6, 'orderstatus'),
(8, 0, '購入処理中', 5, 'orderstatus'),
(9, 0, '返品', 7, 'orderstatus'),
(10, 0, '新規受付（予約注文）', 8, 'orderstatus');

-- --------------------------------------------------------

--
-- Table structure for table `mtb_order_status_color`
--

CREATE TABLE `mtb_order_status_color` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) UNSIGNED NOT NULL,
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mtb_order_status_color`
--

INSERT INTO `mtb_order_status_color` (`id`, `name`, `sort_no`, `discriminator_type`) VALUES
(1, '#437ec4', 0, 'orderstatuscolor'),
(3, '#C04949', 3, 'orderstatuscolor'),
(4, '#EEB128', 2, 'orderstatuscolor'),
(5, '#25B877', 4, 'orderstatuscolor'),
(6, '#25B877', 1, 'orderstatuscolor'),
(7, '#A3A3A3', 6, 'orderstatuscolor'),
(8, '#A3A3A3', 5, 'orderstatuscolor'),
(9, '#C04949', 7, 'orderstatuscolor'),
(10, '#437ec4', 8, 'orderstatuscolor');

-- --------------------------------------------------------

--
-- Table structure for table `mtb_page_max`
--

CREATE TABLE `mtb_page_max` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) UNSIGNED NOT NULL,
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mtb_page_max`
--

INSERT INTO `mtb_page_max` (`id`, `name`, `sort_no`, `discriminator_type`) VALUES
(10, '10', 0, 'pagemax'),
(20, '20', 1, 'pagemax'),
(30, '30', 2, 'pagemax'),
(40, '40', 3, 'pagemax'),
(50, '50', 4, 'pagemax'),
(60, '60', 5, 'pagemax'),
(70, '70', 6, 'pagemax'),
(80, '80', 7, 'pagemax'),
(90, '90', 8, 'pagemax'),
(100, '100', 9, 'pagemax');

-- --------------------------------------------------------

--
-- Table structure for table `mtb_pref`
--

CREATE TABLE `mtb_pref` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) UNSIGNED NOT NULL,
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mtb_pref`
--

INSERT INTO `mtb_pref` (`id`, `name`, `sort_no`, `discriminator_type`) VALUES
(1, '北海道', 1, 'pref'),
(2, '青森県', 2, 'pref'),
(3, '岩手県', 3, 'pref'),
(4, '宮城県', 4, 'pref'),
(5, '秋田県', 5, 'pref'),
(6, '山形県', 6, 'pref'),
(7, '福島県', 7, 'pref'),
(8, '茨城県', 8, 'pref'),
(9, '栃木県', 9, 'pref'),
(10, '群馬県', 10, 'pref'),
(11, '埼玉県', 11, 'pref'),
(12, '千葉県', 12, 'pref'),
(13, '東京都', 13, 'pref'),
(14, '神奈川県', 14, 'pref'),
(15, '新潟県', 15, 'pref'),
(16, '富山県', 16, 'pref'),
(17, '石川県', 17, 'pref'),
(18, '福井県', 18, 'pref'),
(19, '山梨県', 19, 'pref'),
(20, '長野県', 20, 'pref'),
(21, '岐阜県', 21, 'pref'),
(22, '静岡県', 22, 'pref'),
(23, '愛知県', 23, 'pref'),
(24, '三重県', 24, 'pref'),
(25, '滋賀県', 25, 'pref'),
(26, '京都府', 26, 'pref'),
(27, '大阪府', 27, 'pref'),
(28, '兵庫県', 28, 'pref'),
(29, '奈良県', 29, 'pref'),
(30, '和歌山県', 30, 'pref'),
(31, '鳥取県', 31, 'pref'),
(32, '島根県', 32, 'pref'),
(33, '岡山県', 33, 'pref'),
(34, '広島県', 34, 'pref'),
(35, '山口県', 35, 'pref'),
(36, '徳島県', 36, 'pref'),
(37, '香川県', 37, 'pref'),
(38, '愛媛県', 38, 'pref'),
(39, '高知県', 39, 'pref'),
(40, '福岡県', 40, 'pref'),
(41, '佐賀県', 41, 'pref'),
(42, '長崎県', 42, 'pref'),
(43, '熊本県', 43, 'pref'),
(44, '大分県', 44, 'pref'),
(45, '宮崎県', 45, 'pref'),
(46, '鹿児島県', 46, 'pref'),
(47, '沖縄県', 47, 'pref');

-- --------------------------------------------------------

--
-- Table structure for table `mtb_product_list_max`
--

CREATE TABLE `mtb_product_list_max` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) UNSIGNED NOT NULL,
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mtb_product_list_max`
--

INSERT INTO `mtb_product_list_max` (`id`, `name`, `sort_no`, `discriminator_type`) VALUES
(20, '20件', 0, 'productlistmax'),
(40, '40件', 1, 'productlistmax'),
(60, '60件', 2, 'productlistmax');

-- --------------------------------------------------------

--
-- Table structure for table `mtb_product_list_order_by`
--

CREATE TABLE `mtb_product_list_order_by` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) UNSIGNED NOT NULL,
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mtb_product_list_order_by`
--

INSERT INTO `mtb_product_list_order_by` (`id`, `name`, `sort_no`, `discriminator_type`) VALUES
(1, '価格が低い順', 3, 'productlistorderby'),
(2, '新着順', 5, 'productlistorderby'),
(3, '価格が高い順', 4, 'productlistorderby'),
(4, 'おすすめ順', 2, 'productlistorderby');

-- --------------------------------------------------------

--
-- Table structure for table `mtb_product_status`
--

CREATE TABLE `mtb_product_status` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) UNSIGNED NOT NULL,
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mtb_product_status`
--

INSERT INTO `mtb_product_status` (`id`, `name`, `sort_no`, `discriminator_type`) VALUES
(1, '公開', 0, 'productstatus'),
(2, '非公開', 1, 'productstatus'),
(3, '廃止', 2, 'productstatus');

-- --------------------------------------------------------

--
-- Table structure for table `mtb_rounding_type`
--

CREATE TABLE `mtb_rounding_type` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) UNSIGNED NOT NULL,
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mtb_rounding_type`
--

INSERT INTO `mtb_rounding_type` (`id`, `name`, `sort_no`, `discriminator_type`) VALUES
(1, '四捨五入', 0, 'roundingtype'),
(2, '切り捨て', 1, 'roundingtype'),
(3, '切り上げ', 2, 'roundingtype');

-- --------------------------------------------------------

--
-- Table structure for table `mtb_sale_type`
--

CREATE TABLE `mtb_sale_type` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) UNSIGNED NOT NULL,
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mtb_sale_type`
--

INSERT INTO `mtb_sale_type` (`id`, `name`, `sort_no`, `discriminator_type`) VALUES
(1, '通常購入', 0, 'saletype'),
(2, '定期購買', 1, 'saletype');

-- --------------------------------------------------------

--
-- Table structure for table `mtb_sex`
--

CREATE TABLE `mtb_sex` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) UNSIGNED NOT NULL,
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mtb_sex`
--

INSERT INTO `mtb_sex` (`id`, `name`, `sort_no`, `discriminator_type`) VALUES
(1, '男性', 0, 'sex'),
(2, '女性', 1, 'sex');

-- --------------------------------------------------------

--
-- Table structure for table `mtb_tax_display_type`
--

CREATE TABLE `mtb_tax_display_type` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) UNSIGNED NOT NULL,
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mtb_tax_display_type`
--

INSERT INTO `mtb_tax_display_type` (`id`, `name`, `sort_no`, `discriminator_type`) VALUES
(1, '税抜', 0, 'taxdisplaytype'),
(2, '税込', 1, 'taxdisplaytype');

-- --------------------------------------------------------

--
-- Table structure for table `mtb_tax_type`
--

CREATE TABLE `mtb_tax_type` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) UNSIGNED NOT NULL,
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mtb_tax_type`
--

INSERT INTO `mtb_tax_type` (`id`, `name`, `sort_no`, `discriminator_type`) VALUES
(1, '課税', 0, 'taxtype'),
(2, '不課税', 1, 'taxtype'),
(3, '非課税', 2, 'taxtype');

-- --------------------------------------------------------

--
-- Table structure for table `mtb_work`
--

CREATE TABLE `mtb_work` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) UNSIGNED NOT NULL,
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mtb_work`
--

INSERT INTO `mtb_work` (`id`, `name`, `sort_no`, `discriminator_type`) VALUES
(0, '非稼働', 0, 'work'),
(1, '稼働', 1, 'work');

-- --------------------------------------------------------

--
-- Table structure for table `plg_ccp_config`
--

CREATE TABLE `plg_ccp_config` (
  `id` int(10) UNSIGNED NOT NULL,
  `rounding_type_id` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `plg_ccp_config`
--

INSERT INTO `plg_ccp_config` (`id`, `rounding_type_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `plg_ccp_customer_class`
--

CREATE TABLE `plg_ccp_customer_class` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `discount_rate` decimal(10,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `plg_ccp_customer_class`
--

INSERT INTO `plg_ccp_customer_class` (`id`, `name`, `discount_rate`) VALUES
(1, '会員価格', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `plg_ccp_customer_class_price`
--

CREATE TABLE `plg_ccp_customer_class_price` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_class_id` int(10) UNSIGNED DEFAULT NULL,
  `product_class_id` int(10) UNSIGNED DEFAULT NULL,
  `price` decimal(12,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `plg_ccp_customer_class_price`
--

INSERT INTO `plg_ccp_customer_class_price` (`id`, `customer_class_id`, `product_class_id`, `price`) VALUES
(1, 1, 38, '1044.00'),
(2, 1, 39, '928.00'),
(3, 1, 56, '8640.00'),
(4, 1, 57, '7680.00'),
(5, 1, 54, '6480.00'),
(6, 1, 55, '5760.00'),
(7, 1, 52, '18720.00'),
(8, 1, 53, '16640.00'),
(9, 1, 50, '13455.00'),
(10, 1, 51, '11960.00'),
(11, 1, 48, '29250.00'),
(12, 1, 49, '26000.00'),
(13, 1, 46, '8190.00'),
(14, 1, 47, '7280.00'),
(15, 1, 44, '4320.00'),
(16, 1, 45, '3840.00'),
(17, 1, 42, '12960.00'),
(18, 1, 43, '11520.00'),
(19, 1, 40, '23760.00'),
(20, 1, 41, '21120.00'),
(21, 1, 36, '42750.00'),
(22, 1, 37, '38000.00'),
(23, 1, 59, '6264.00'),
(24, 1, 60, '5568.00'),
(25, 1, 11, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `plg_check_kagoochi_sendedmail`
--

CREATE TABLE `plg_check_kagoochi_sendedmail` (
  `id` int(10) UNSIGNED NOT NULL,
  `target_id` int(11) NOT NULL,
  `target_kbn` varchar(1) NOT NULL DEFAULT '1',
  `customer_id` int(11) NOT NULL,
  `send_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `plg_check_kagoochi_sendedmail`
--

INSERT INTO `plg_check_kagoochi_sendedmail` (`id`, `target_id`, `target_kbn`, `customer_id`, `send_date`) VALUES
(14, 292, 'c', 1, '2020-09-07 10:21:24'),
(15, 109, 'o', 0, '2020-09-07 10:21:24'),
(16, 258, 'c', 4, '2020-09-07 10:21:24'),
(17, 101, 'o', 0, '2020-09-07 10:21:24'),
(18, 100, 'o', 0, '2020-09-07 10:21:24'),
(19, 292, 'c', 1, '2020-09-09 08:18:27'),
(20, 292, 'c', 1, '2020-09-09 08:21:22'),
(21, 292, 'c', 1, '2020-09-09 09:26:09');

-- --------------------------------------------------------

--
-- Table structure for table `plg_contact_management4_config`
--

CREATE TABLE `plg_contact_management4_config` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `plg_contact_management4_contact`
--

CREATE TABLE `plg_contact_management4_contact` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `pref_id` smallint(5) UNSIGNED DEFAULT NULL,
  `contact_status_id` smallint(5) UNSIGNED DEFAULT NULL,
  `name01` varchar(255) NOT NULL,
  `name02` varchar(255) NOT NULL,
  `kana01` varchar(255) DEFAULT NULL,
  `kana02` varchar(255) DEFAULT NULL,
  `postal_code` varchar(8) DEFAULT NULL,
  `addr01` varchar(255) DEFAULT NULL,
  `addr02` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `phone_number` varchar(14) DEFAULT NULL,
  `contents` longtext,
  `note` longtext,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `plg_contact_management4_contact`
--

INSERT INTO `plg_contact_management4_contact` (`id`, `customer_id`, `staff_id`, `product_id`, `pref_id`, `contact_status_id`, `name01`, `name02`, `kana01`, `kana02`, `postal_code`, `addr01`, `addr02`, `email`, `phone_number`, `contents`, `note`, `create_date`, `update_date`) VALUES
(1, 1, NULL, NULL, 13, 1, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', '1570077', '世田谷区鎌田', '123建物', 'djfre2000@hotmail.com', '0123456789', 'Test Contact form', NULL, '2020-05-11 12:10:48', '2020-05-11 12:10:48'),
(2, NULL, 1, NULL, 13, 2, '多炉', 'ゆう', 'タロ', 'ユウ', '1570077', '世田谷区鎌田', NULL, 'djfre2000@hotmail.com', NULL, 'Test Frederic contact form', NULL, '2020-05-15 01:15:21', '2020-05-15 02:14:52'),
(3, 1, NULL, NULL, 13, 1, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', '1570077', '世田谷区鎌田', '123建物', 'djfre2000@hotmail.com', '0123456789', 'テストメール', NULL, '2020-05-16 00:38:52', '2020-05-16 00:38:52'),
(4, 1, NULL, NULL, 13, 1, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', '1570077', '世田谷区鎌田', '123建物', 'djfre2000@hotmail.com', '0123456789', 'テスト2', NULL, '2020-05-16 00:45:45', '2020-06-25 07:10:17'),
(5, 1, NULL, NULL, 13, 1, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', '1570077', '世田谷区鎌田', '123建物', 'djfre2000@hotmail.com', '0123456789', 'test', NULL, '2020-08-04 09:23:20', '2020-08-04 09:23:20'),
(6, 1, NULL, NULL, 13, 1, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', '1570077', '世田谷区鎌田', '123建物', 'djfre2000@hotmail.com', '0123456789', 'test', NULL, '2020-08-05 11:52:19', '2020-08-05 11:52:19'),
(7, 1, NULL, NULL, 13, 1, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', '1570077', '世田谷区鎌田', '123建物', 'djfre2000@hotmail.com', '0123456789', 'tsste', NULL, '2020-08-05 12:00:08', '2020-08-05 12:00:08'),
(8, 1, NULL, NULL, 13, 1, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', '1570077', '世田谷区鎌田', '123建物', 'djfre2000@hotmail.com', '0123456789', 'tast', NULL, '2020-08-05 12:01:26', '2020-08-05 12:01:26'),
(11, 1, NULL, NULL, 13, 1, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', '1570077', '世田谷区鎌田', '123建物', 'djfre2000@hotmail.com', '0123456789', 'test', NULL, '2020-08-05 12:20:27', '2020-08-05 12:20:27'),
(12, 1, NULL, NULL, 13, 1, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', '1570077', '世田谷区鎌田', '123建物', 'djfre2000@hotmail.com', '0123456789', 'eej', NULL, '2020-08-05 12:59:21', '2020-08-05 12:59:21'),
(13, 1, NULL, NULL, 13, 1, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', '1570077', '世田谷区鎌田', '123建物', 'djfre2000@hotmail.com', '0123456789', 'ehehe', NULL, '2020-08-05 13:02:54', '2020-08-05 13:02:54'),
(14, 1, NULL, NULL, 13, 1, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', '1570077', '世田谷区鎌田', '123建物', 'djfre2000@hotmail.com', '0123456789', 'eng', NULL, '2020-08-05 13:04:29', '2020-08-05 13:04:29'),
(15, 1, NULL, NULL, 13, 1, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', '1570077', '世田谷区鎌田', '123建物', 'djfre2000@hotmail.com', '0123456789', 'ja', NULL, '2020-08-05 13:05:03', '2020-08-05 13:05:03'),
(16, 1, NULL, NULL, 13, 1, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', '1570077', '世田谷区鎌田', '123建物', 'djfre2000@hotmail.com', '0123456789', 'ja', NULL, '2020-08-05 13:07:17', '2020-08-05 13:07:17'),
(17, 1, NULL, NULL, 13, 1, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', '1570077', '世田谷区鎌田', '123建物', 'djfre2000@hotmail.com', '0123456789', 'eng', NULL, '2020-08-05 13:07:58', '2020-08-05 13:07:58'),
(18, 1, NULL, 10, 13, 1, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', '1570077', '世田谷区鎌田', '123建物', 'djfre2000@hotmail.com', '0123456789', 'I contact you regarding the following MARITIME product.\r\nTEst', NULL, '2020-08-06 10:59:02', '2020-08-06 10:59:02'),
(19, 1, NULL, NULL, 13, 1, 'Frederic', 'Vervaecke', 'フレデリック', 'ヴェルヴァーク', '1570077', '世田谷区鎌田', '123建物', 'djfre2000@hotmail.com', '0123456789', 'test', NULL, '2020-08-06 11:40:44', '2020-08-06 11:40:44');

-- --------------------------------------------------------

--
-- Table structure for table `plg_contact_management4_contact_status`
--

CREATE TABLE `plg_contact_management4_contact_status` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) UNSIGNED NOT NULL,
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `plg_contact_management4_contact_status`
--

INSERT INTO `plg_contact_management4_contact_status` (`id`, `name`, `sort_no`, `discriminator_type`) VALUES
(1, '未対応', 1, 'contactstatus'),
(2, '対応中', 2, 'contactstatus'),
(99, '対応完了', 99, 'contactstatus');

-- --------------------------------------------------------

--
-- Table structure for table `plg_coupon`
--

CREATE TABLE `plg_coupon` (
  `coupon_id` int(10) UNSIGNED NOT NULL,
  `coupon_cd` varchar(20) DEFAULT NULL,
  `coupon_type` smallint(6) DEFAULT NULL,
  `coupon_name` varchar(50) DEFAULT NULL,
  `discount_type` smallint(6) DEFAULT NULL,
  `coupon_use_time` int(11) DEFAULT NULL,
  `discount_price` decimal(12,2) UNSIGNED DEFAULT '0.00',
  `discount_rate` decimal(10,0) UNSIGNED DEFAULT '0',
  `enable_flag` tinyint(1) NOT NULL DEFAULT '1',
  `available_from_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `available_to_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `coupon_member` tinyint(1) NOT NULL DEFAULT '0',
  `coupon_lower_limit` decimal(12,2) UNSIGNED DEFAULT '0.00',
  `coupon_release` int(11) NOT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `plg_coupon`
--

INSERT INTO `plg_coupon` (`coupon_id`, `coupon_cd`, `coupon_type`, `coupon_name`, `discount_type`, `coupon_use_time`, `discount_price`, `discount_rate`, `enable_flag`, `available_from_date`, `available_to_date`, `visible`, `coupon_member`, `coupon_lower_limit`, `coupon_release`, `create_date`, `update_date`, `discriminator_type`) VALUES
(1, 'MARITIME10', 3, '初回10％割引オファー', 2, 9998, NULL, '10', 1, '2020-05-15 15:00:00', '2024-12-30 15:00:00', 1, 0, NULL, 9999, '2020-05-16 10:34:02', '2020-09-17 06:20:57', 'coupon'),
(2, 'REVIEW15', 3, 'レビュー15％割引クーポン', 2, 9999, NULL, '15', 1, '2020-09-17 15:00:00', '2024-12-30 15:00:00', 1, 0, NULL, 9999, '2020-09-18 06:19:42', '2020-09-18 06:19:42', 'coupon');

-- --------------------------------------------------------

--
-- Table structure for table `plg_coupon_detail`
--

CREATE TABLE `plg_coupon_detail` (
  `coupon_detail_id` int(10) UNSIGNED NOT NULL,
  `coupon_id` int(10) UNSIGNED DEFAULT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `coupon_type` smallint(6) DEFAULT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `plg_coupon_order`
--

CREATE TABLE `plg_coupon_order` (
  `coupon_order_id` int(10) UNSIGNED NOT NULL,
  `coupon_id` int(10) UNSIGNED NOT NULL,
  `coupon_cd` varchar(20) DEFAULT NULL,
  `coupon_name` varchar(50) DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `pre_order_id` varchar(255) DEFAULT NULL,
  `order_date` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `order_item_id` int(10) UNSIGNED DEFAULT NULL,
  `discount` decimal(12,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `order_change_status` tinyint(1) NOT NULL DEFAULT '1',
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `discriminator_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `plg_coupon_order`
--

INSERT INTO `plg_coupon_order` (`coupon_order_id`, `coupon_id`, `coupon_cd`, `coupon_name`, `user_id`, `email`, `order_id`, `pre_order_id`, `order_date`, `order_item_id`, `discount`, `visible`, `order_change_status`, `create_date`, `update_date`, `discriminator_type`) VALUES
(1, 1, 'maritime10', '初回10％割引オファー', 1, NULL, 19, '0989047a9e7cc5adc9c6399cff60ff1243f83705', NULL, NULL, '880.00', 1, 0, '2020-05-22 03:52:27', '2020-05-22 03:52:27', 'couponorder'),
(2, 1, 'MARITIME10', '初回10％割引オファー', 1, NULL, 23, '74539e45951df6064bdef5809dbc29425045b889', NULL, NULL, '3135.00', 1, 0, '2020-05-27 12:19:04', '2020-05-27 12:19:04', 'couponorder'),
(8, 1, 'MARITIME10', '初回10％割引オファー', 4, NULL, 93, 'e623bef20812404c0d7b35b98580ec1ba98a3019', NULL, NULL, '638.00', 1, 0, '2020-08-05 14:35:05', '2020-08-05 14:35:05', 'couponorder'),
(17, 1, 'MARITIME10', '初回10％割引オファー', 4, NULL, 97, 'c9cacd6fd6ff43a0bb7cef84c5d997e8b165c058', NULL, NULL, '528.00', 1, 0, '2020-08-06 12:33:25', '2020-08-06 12:33:25', 'couponorder'),
(18, 1, 'MARITIME10', '初回10％割引オファー', 1, NULL, 96, '4f9f0e02335dbb9697613cadc0064844fbd89409', NULL, NULL, '475.00', 1, 0, '2020-08-06 13:27:30', '2020-08-06 13:27:30', 'couponorder'),
(19, 1, 'MARITIME10', '初回10％割引オファー', 1, NULL, 115, '379324704e6716a5e216f030d9253e47c8f6e694', NULL, NULL, '4703.00', 1, 1, '2020-09-10 00:22:45', '2020-09-17 03:54:00', 'couponorder'),
(20, 1, 'MARITIME10', '初回10％割引オファー', 1, NULL, 117, '0af817dacbbbfac5f0bdbaf147fe0a1070efb405', NULL, NULL, '475.00', 1, 1, '2020-09-17 03:54:27', '2020-09-17 05:22:04', 'couponorder'),
(21, 1, 'MARITIME10', '初回10％割引オファー', 1, NULL, 118, 'd97c8c6fc22364946c65b8e2a78b8a489d4bd6ff', NULL, NULL, '475.00', 1, 1, '2020-09-17 05:22:11', '2020-09-17 05:25:20', 'couponorder'),
(22, 1, 'MARITIME10', '初回10％割引オファー', 1, NULL, 119, 'aa86120289f30314eb47e2ea14b7dff3dcd23ec1', '2020-09-17 06:20:57', NULL, '950.00', 1, 0, '2020-09-17 06:20:35', '2020-09-17 06:20:57', 'couponorder');

-- --------------------------------------------------------

--
-- Table structure for table `plg_gmo_payment_gateway_config`
--

CREATE TABLE `plg_gmo_payment_gateway_config` (
  `id` int(10) UNSIGNED NOT NULL,
  `connect_server_type` int(10) UNSIGNED NOT NULL,
  `server_url` varchar(255) NOT NULL,
  `kanri_server_url` varchar(255) NOT NULL,
  `site_id` varchar(16) NOT NULL,
  `site_pass` varchar(16) NOT NULL,
  `shop_id` varchar(16) NOT NULL,
  `shop_pass` varchar(16) NOT NULL,
  `card_regist_flg` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `plg_gmo_payment_gateway_config`
--

INSERT INTO `plg_gmo_payment_gateway_config` (`id`, `connect_server_type`, `server_url`, `kanri_server_url`, `site_id`, `site_pass`, `shop_id`, `shop_pass`, `card_regist_flg`) VALUES
(1, 1, 'https://pt01.mul-pay.jp/payment/', 'https://kt01.mul-pay.jp/kanri/', 'mst2000023988', 'czsmw5mk', '9200002712272', 'br59be55', 1);

-- --------------------------------------------------------

--
-- Table structure for table `plg_gmo_payment_gateway_member`
--

CREATE TABLE `plg_gmo_payment_gateway_member` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `member_id` longtext,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `plg_gmo_payment_gateway_member`
--

INSERT INTO `plg_gmo_payment_gateway_member` (`id`, `customer_id`, `member_id`, `create_date`, `update_date`) VALUES
(1, 1, '764baf3c05e5101d9dec64811588b9e06a35ee5e', '2020-08-03 05:58:15', '2020-08-03 05:58:15');

-- --------------------------------------------------------

--
-- Table structure for table `plg_gmo_payment_gateway_order_payment`
--

CREATE TABLE `plg_gmo_payment_gateway_order_payment` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `memo01` longtext,
  `memo02` longtext,
  `memo03` longtext,
  `memo04` longtext,
  `memo05` longtext,
  `memo06` longtext,
  `memo07` longtext,
  `memo08` longtext,
  `memo09` longtext,
  `memo10` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `plg_gmo_payment_gateway_order_payment`
--

INSERT INTO `plg_gmo_payment_gateway_order_payment` (`id`, `order_id`, `memo01`, `memo02`, `memo03`, `memo04`, `memo05`, `memo06`, `memo07`, `memo08`, `memo09`, `memo10`) VALUES
(11, 11, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 12, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 13, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 14, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\RakutenPay', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 17, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 18, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, 19, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, 20, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, 21, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, 22, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, 23, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, 24, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, 25, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, 26, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, 27, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, 28, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, 29, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, 30, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(29, 31, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(30, 32, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(31, 33, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(32, 34, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(33, 35, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(34, 36, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(35, 37, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(36, 38, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(37, 39, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(38, 40, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(39, 41, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(40, 42, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(41, 43, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(42, 44, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(43, 45, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(44, 46, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(45, 47, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(46, 48, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(47, 49, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(48, 50, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(49, 51, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(50, 52, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(51, 53, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(52, 54, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(53, 55, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(54, 56, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(55, 57, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(56, 58, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(57, 59, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(58, 60, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(59, 61, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(60, 62, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(61, 63, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(62, 64, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(63, 65, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(64, 66, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(65, 73, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(66, 74, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(67, 75, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(68, 76, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(69, 77, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(70, 78, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(71, 79, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(72, 87, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\RakutenPay', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(73, 88, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(74, 89, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(75, 90, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(76, 91, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(77, 92, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(78, 93, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(79, 94, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(80, 95, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(81, 96, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(82, 97, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(83, 98, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(84, 99, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(85, 100, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(86, 101, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(87, 102, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(88, 103, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(89, 104, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(90, 105, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(91, 106, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(92, 107, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(93, 108, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(94, 110, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(95, 111, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(96, 112, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(97, 113, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(98, 114, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(99, 115, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(100, 116, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(101, 117, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(102, 118, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(103, 119, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(104, 120, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(105, 121, NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `plg_gmo_payment_gateway_payment_method`
--

CREATE TABLE `plg_gmo_payment_gateway_payment_method` (
  `id` int(10) UNSIGNED NOT NULL,
  `payment_id` int(10) UNSIGNED NOT NULL,
  `payment_method` longtext NOT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `memo01` longtext,
  `memo02` longtext,
  `memo03` longtext,
  `memo04` longtext,
  `memo05` longtext,
  `memo06` longtext,
  `memo07` longtext,
  `memo08` longtext,
  `memo09` longtext,
  `memo10` longtext,
  `plugin_code` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `plg_gmo_payment_gateway_payment_method`
--

INSERT INTO `plg_gmo_payment_gateway_payment_method` (`id`, `payment_id`, `payment_method`, `create_date`, `update_date`, `memo01`, `memo02`, `memo03`, `memo04`, `memo05`, `memo06`, `memo07`, `memo08`, `memo09`, `memo10`, `plugin_code`) VALUES
(1, 5, 'クレジット決済', '2020-05-14 22:31:28', '2020-06-14 05:17:58', NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CreditCard', NULL, '{\"JobCd\":\"CAPTURE\",\"credit_pay_methods\":[\"1-0\"],\"use_securitycd\":1,\"use_securitycd_option\":0,\"TdFlag\":0,\"TdTenantName\":null,\"order_mail_title1\":\"\\u304a\\u652f\\u6255\\u3044\\u306b\\u3064\\u3044\\u3066\",\"order_mail_body1\":null,\"ClientField1\":null,\"ClientField2\":null}', NULL, NULL, NULL, NULL, NULL, 'GmoPaymentGateway4'),
(2, 6, 'コンビニ決済', '2020-05-14 22:31:28', '2020-06-13 04:49:25', NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\Cvs', NULL, '{\"conveni\":[10001,10002,10005,\"00006\",\"00007\",10008],\"PaymentTermDay\":\"7\",\"enable_mail\":0,\"enable_cvs_mails\":[],\"RegisterDisp1\":\"\",\"RegisterDisp2\":\"\",\"RegisterDisp3\":\"\",\"RegisterDisp4\":\"\",\"RegisterDisp5\":\"\",\"RegisterDisp6\":\"\",\"RegisterDisp7\":\"\",\"RegisterDisp8\":\"\",\"ReceiptsDisp1\":\"\",\"ReceiptsDisp2\":\"\",\"ReceiptsDisp3\":\"\",\"ReceiptsDisp4\":\"\",\"ReceiptsDisp5\":\"\",\"ReceiptsDisp6\":\"\",\"ReceiptsDisp7\":\"\",\"ReceiptsDisp8\":\"\",\"ReceiptsDisp9\":\"\",\"ReceiptsDisp10\":\"\",\"ReceiptsDisp11\":\"\\u30c6\\u30b9\\u30c8\",\"ReceiptsDisp12_1\":\"078\",\"ReceiptsDisp12_2\":\"777\",\"ReceiptsDisp12_3\":\"7000\",\"ReceiptsDisp13_1\":\"10\",\"ReceiptsDisp13_2\":\"0\",\"ReceiptsDisp13_3\":\"19\",\"ReceiptsDisp13_4\":\"0\",\"order_mail_title1\":\"\\u304a\\u652f\\u6255\\u3044\\u306b\\u3064\\u3044\\u3066\",\"order_mail_body1\":null,\"ClientField1\":null,\"ClientField2\":null}', NULL, NULL, NULL, NULL, NULL, 'GmoPaymentGateway4'),
(3, 7, 'Pay-easy決済(銀行ATM)', '2020-05-14 22:31:28', '2020-05-14 22:31:28', NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\PayEasyAtm', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'GmoPaymentGateway4'),
(4, 8, 'Pay-easy決済(ネットバンク)', '2020-05-14 22:31:28', '2020-05-14 22:31:28', NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\PayEasyNet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'GmoPaymentGateway4'),
(5, 9, 'auかんたん決済', '2020-05-14 22:31:28', '2020-05-14 22:31:28', NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CarAu', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'GmoPaymentGateway4'),
(6, 10, 'ドコモケータイ払い', '2020-05-14 22:31:28', '2020-05-14 22:31:28', NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CarDocomo', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'GmoPaymentGateway4'),
(7, 11, 'ソフトバンクまとめて支払い', '2020-05-14 22:31:28', '2020-05-14 22:31:28', NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\CarSoftbank', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'GmoPaymentGateway4'),
(8, 12, '楽天ペイ', '2020-05-14 22:31:28', '2020-05-14 22:31:28', NULL, NULL, 'Plugin\\GmoPaymentGateway4\\Service\\Method\\RakutenPay', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'GmoPaymentGateway4');

-- --------------------------------------------------------

--
-- Table structure for table `plg_mailmaga_send_history`
--

CREATE TABLE `plg_mailmaga_send_history` (
  `send_id` int(10) UNSIGNED NOT NULL,
  `creator_id` int(10) UNSIGNED DEFAULT NULL,
  `mail_method` smallint(6) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `body` longtext,
  `html_body` longtext,
  `send_count` int(10) UNSIGNED DEFAULT NULL,
  `complete_count` int(10) UNSIGNED DEFAULT '0',
  `error_count` int(10) UNSIGNED DEFAULT '0',
  `start_date` datetime DEFAULT NULL COMMENT '(DC2Type:datetime)',
  `end_date` datetime DEFAULT NULL COMMENT '(DC2Type:datetime)',
  `search_data` longtext,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetime)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetime)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `plg_mailmaga_template`
--

CREATE TABLE `plg_mailmaga_template` (
  `template_id` int(10) UNSIGNED NOT NULL,
  `subject` varchar(255) NOT NULL,
  `body` longtext NOT NULL,
  `html_body` longtext NOT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetime)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetime)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `plg_paypal_config`
--

CREATE TABLE `plg_paypal_config` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` varchar(255) NOT NULL,
  `client_secret` varchar(255) NOT NULL,
  `use_sandbox` tinyint(1) NOT NULL DEFAULT '1',
  `paypal_logo` varchar(255) NOT NULL,
  `payment_paypal_logo` varchar(255) NOT NULL,
  `use_express_btn` tinyint(1) NOT NULL DEFAULT '0',
  `reference_day` int(11) DEFAULT NULL,
  `cut_off_day` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `plg_paypal_config`
--

INSERT INTO `plg_paypal_config` (`id`, `client_id`, `client_secret`, `use_sandbox`, `paypal_logo`, `payment_paypal_logo`, `use_express_btn`, `reference_day`, `cut_off_day`) VALUES
(1, 'AXUia_krl3hyrNnCbMsF01aZd5kuYkTT8GPYRlVqswQZdAqwmd1w1t4Rrq3mqICTMq55Bds6Pb88uXn9', 'EKYrhXiTxiMYrDYdBqzbvG3lgZCJNR6RQ21cgQUdH4DLp9e553n6QqnvyZ9sIrhKe3KKJ3d1htTrZIM-', 1, '1', '1', 0, 2, 15);

-- --------------------------------------------------------

--
-- Table structure for table `plg_paypal_subscribing_customer`
--

CREATE TABLE `plg_paypal_subscribing_customer` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `product_class_id` int(10) UNSIGNED NOT NULL,
  `reference_transaction_id` int(10) UNSIGNED DEFAULT NULL,
  `primary_shipping_address` text COMMENT '(DC2Type:json_array)',
  `error_message` varchar(255) DEFAULT NULL,
  `next_payment_date` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `contracted_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `withdrawal_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `plg_paypal_transaction`
--

CREATE TABLE `plg_paypal_transaction` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED DEFAULT NULL,
  `status_code` varchar(255) DEFAULT NULL,
  `paypal_debug_id` varchar(255) DEFAULT NULL,
  `billing_agreement_id` varchar(255) DEFAULT NULL,
  `capture_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `plg_paypal_transaction`
--

INSERT INTO `plg_paypal_transaction` (`id`, `order_id`, `status_code`, `paypal_debug_id`, `billing_agreement_id`, `capture_id`) VALUES
(1, 61, '201', 'be4b2a007d992', NULL, '4T208563KN926181V'),
(2, 64, '201', '841e280319c59', NULL, '2V390668T14921428'),
(3, 67, '201', '34d73d8c8288d', NULL, '5C526557U45622401'),
(4, 71, '201', 'ce8b416663797', NULL, '9D116294W6380240L'),
(5, 72, '201', '930ca3097749b', NULL, '9VV12023L95753710'),
(6, 80, '201', 'a346b23ac764a', NULL, '3A248903UH3952601'),
(7, 82, '201', '6b2e908d251a7', NULL, '6SK020932G3545709'),
(8, 83, '201', '70ffce40a1448', NULL, '23C30582D4978951F'),
(9, 84, '201', 'f36a4192ccbc5', NULL, '56V156663W2048814'),
(10, 89, '201', 'd2a19cb84e5e3', NULL, '95U61340VP480510B'),
(11, 91, '201', '31f005ad68b5b', NULL, '3PX06801AL855174M');

-- --------------------------------------------------------

--
-- Table structure for table `plg_product_class_reserve4_extra`
--

CREATE TABLE `plg_product_class_reserve4_extra` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_class_id` int(10) UNSIGNED DEFAULT NULL,
  `class_category_id1` int(10) UNSIGNED DEFAULT NULL,
  `class_category_id2` int(10) UNSIGNED DEFAULT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `is_allowed` int(11) NOT NULL DEFAULT '0',
  `start_date` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `end_date` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `shipping_date` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `shipping_date_changed` int(11) NOT NULL DEFAULT '0',
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `plg_product_contact4_config`
--

CREATE TABLE `plg_product_contact4_config` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `plg_product_display_rank4_config`
--

CREATE TABLE `plg_product_display_rank4_config` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_list_order_by_id` smallint(5) UNSIGNED NOT NULL,
  `type` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `plg_product_display_rank4_config`
--

INSERT INTO `plg_product_display_rank4_config` (`id`, `product_list_order_by_id`, `type`, `name`) VALUES
(1, 4, 2, 'おすすめ順'),
(2, 4, 1, 'おすすめ順');

-- --------------------------------------------------------

--
-- Table structure for table `plg_product_reserve4_config`
--

CREATE TABLE `plg_product_reserve4_config` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `plg_product_reserve4_config`
--

INSERT INTO `plg_product_reserve4_config` (`id`, `name`, `value`) VALUES
(1, 'mail_order_reservation', '14'),
(2, 'mail_reservation_shipping_change', '15'),
(3, 'order_status', '10'),
(4, 'payments', '14,15');

-- --------------------------------------------------------

--
-- Table structure for table `plg_product_reserve4_extra`
--

CREATE TABLE `plg_product_reserve4_extra` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `is_allowed` int(11) NOT NULL DEFAULT '0',
  `start_date` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `end_date` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `shipping_date` datetime DEFAULT NULL COMMENT '(DC2Type:datetimetz)',
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `plg_product_reserve4_extra`
--

INSERT INTO `plg_product_reserve4_extra` (`product_id`, `is_allowed`, `start_date`, `end_date`, `shipping_date`, `create_date`, `update_date`) VALUES
(2, 0, NULL, NULL, NULL, '2020-07-15 10:32:56', '2020-07-15 10:33:14'),
(4, 1, '2020-06-30 15:00:00', '2020-07-17 15:00:00', '2020-07-23 15:00:00', '2020-07-16 07:54:19', '2020-08-04 05:38:35'),
(5, 0, NULL, NULL, NULL, '2020-08-04 05:35:49', '2020-08-04 05:35:49'),
(6, 0, NULL, NULL, NULL, '2020-08-04 05:33:05', '2020-08-04 05:33:05'),
(7, 0, NULL, NULL, NULL, '2020-08-04 05:31:00', '2020-08-04 05:31:00'),
(8, 0, NULL, NULL, NULL, '2020-08-02 12:14:26', '2020-08-02 12:14:26'),
(9, 0, NULL, NULL, NULL, '2020-08-02 12:05:04', '2020-08-02 12:05:04'),
(10, 0, NULL, NULL, NULL, '2020-08-04 05:36:40', '2020-08-04 05:36:40'),
(11, 0, NULL, NULL, NULL, '2020-08-04 05:35:11', '2020-08-04 05:35:11'),
(12, 0, NULL, NULL, NULL, '2020-08-04 05:34:29', '2020-08-04 05:34:29'),
(13, 0, NULL, NULL, NULL, '2020-08-04 05:32:14', '2020-08-04 05:32:14'),
(14, 0, NULL, NULL, NULL, '2020-08-04 05:37:13', '2020-08-04 05:37:13'),
(15, 1, '2020-07-03 15:00:00', NULL, NULL, '2020-07-16 09:06:27', '2020-08-04 05:38:03');

-- --------------------------------------------------------

--
-- Table structure for table `plg_product_reserve4_order`
--

CREATE TABLE `plg_product_reserve4_order` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED DEFAULT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `plg_product_reserve4_order`
--

INSERT INTO `plg_product_reserve4_order` (`id`, `order_id`, `product_id`, `created_at`) VALUES
(1, 80, 4, '2020-07-16 10:13:41'),
(2, 82, 4, '2020-07-16 12:22:28'),
(3, 83, 4, '2020-07-16 13:13:40'),
(4, 84, 4, '2020-07-17 08:14:17');

-- --------------------------------------------------------

--
-- Table structure for table `plg_product_review`
--

CREATE TABLE `plg_product_review` (
  `id` int(10) UNSIGNED NOT NULL,
  `sex_id` smallint(5) UNSIGNED DEFAULT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `customer_id` int(10) UNSIGNED DEFAULT NULL,
  `status_id` smallint(5) UNSIGNED DEFAULT NULL,
  `reviewer_name` varchar(255) NOT NULL,
  `reviewer_url` longtext,
  `title` varchar(50) NOT NULL,
  `comment` longtext NOT NULL,
  `recommend_level` smallint(6) NOT NULL,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `plg_product_review`
--

INSERT INTO `plg_product_review` (`id`, `sex_id`, `product_id`, `customer_id`, `status_id`, `reviewer_name`, `reviewer_url`, `title`, `comment`, `recommend_level`, `create_date`, `update_date`) VALUES
(1, 1, 11, 1, 1, 'くましん', NULL, '目に見えての効果は感じられない', '友人からこちらをプレゼントしてもらい、最近使っています。\r\n\r\n正直目に見えての効果は感じられません。\r\n寝る前に使用して朝起きると確かに寝起きがいいと言われればいい。\r\nけど、体調がただ良いかもしれない。。。。\r\n\r\nまだ使い始めたばかりなのでその内効果が出てくるかもしれません。\r\n\r\nただ、お酒と一緒に使うと効果がはっきりと感じられますが、少し危険です。\r\n私の場合は最初気持ち悪くなったのですが、2時間ぐらい経つとすごいリラックス状態になりました。\r\n例えるとサウナのととのう状態です。\r\nしかしお酒との使用はダメな場合もあるので気を付ける必要があります。\r\n\r\n継続して使ってみようと思います。', 4, '2020-08-02 07:55:48', '2020-08-02 08:37:29'),
(2, 2, 9, 1, 1, 'test', 'https://shourai.io', 'test', 'test', 5, '2020-08-04 10:33:14', '2020-08-13 02:31:46'),
(3, 2, 4, 1, 1, 'ふみ', NULL, 'もったいない', '美味しい水なのですが、値段のこともあって、少し遠慮して飲んでしまいます。犬の散歩のお共になってます。カルピスみたいな濃縮タイプで、水道水に薄めて飲めるタイプがあれば良いと思いました。', 4, '2020-08-13 10:42:41', '2020-08-13 10:44:22'),
(4, 1, 4, 1, 1, 'マサ', NULL, '飲みやすい、お得なオプションが欲しいです！', 'とても飲みやすいですね。たくさん買うともっと安くなるお得なシステムがあるといい。', 4, '2020-08-13 10:43:41', '2020-08-13 10:44:08'),
(5, NULL, 14, NULL, 1, 'Test', NULL, 'イライラがなくなった', 'イライラがなくなった', 5, '2020-09-11 10:09:40', '2020-09-11 10:11:08'),
(6, NULL, 14, NULL, 1, 'Test 2', NULL, '眠りが深くなった', '眠りが深くなった', 4, '2020-09-11 10:10:18', '2020-09-11 10:11:19'),
(7, 1, 9, 1, 2, 'Test Frederic', NULL, 'イライラがなくなった', 'イライラがなくなった', 5, '2020-09-14 03:22:34', '2020-09-14 03:22:34'),
(8, 1, 9, 1, 1, 'Frederic', NULL, 'Great stuff', '効果実感できました！', 5, '2020-09-16 02:36:52', '2020-09-16 02:37:23'),
(9, 1, 5, 1, 2, 'test', NULL, 'Improved sleep', 'Improved sleep and I feel great', 5, '2020-09-18 00:07:00', '2020-09-18 00:07:00'),
(10, 1, 5, 1, 2, 'Frederic', NULL, 'Improved sleep', 'Improved sleep and feeling better yeey', 5, '2020-09-18 00:10:23', '2020-09-18 00:10:23'),
(11, 1, 9, 1, 2, 'test', NULL, 'イライラがなくなった', 'イライラがなくなった', 5, '2020-09-18 06:21:41', '2020-09-18 06:21:41');

-- --------------------------------------------------------

--
-- Table structure for table `plg_product_review_config`
--

CREATE TABLE `plg_product_review_config` (
  `id` int(10) UNSIGNED NOT NULL,
  `csv_type_id` smallint(5) UNSIGNED DEFAULT NULL,
  `review_max` smallint(5) UNSIGNED DEFAULT '5',
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetimetz)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `plg_product_review_config`
--

INSERT INTO `plg_product_review_config` (`id`, `csv_type_id`, `review_max`, `create_date`, `update_date`) VALUES
(1, 6, 5, '2020-05-06 11:18:12', '2020-05-06 11:18:12');

-- --------------------------------------------------------

--
-- Table structure for table `plg_product_review_status`
--

CREATE TABLE `plg_product_review_status` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_no` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `plg_product_review_status`
--

INSERT INTO `plg_product_review_status` (`id`, `name`, `sort_no`) VALUES
(1, '公開', 1),
(2, '非公開', 2);

-- --------------------------------------------------------

--
-- Table structure for table `plg_taba_cms_category`
--

CREATE TABLE `plg_taba_cms_category` (
  `category_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `data_key` varchar(255) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `description` tinytext,
  `tag_attributes` varchar(255) DEFAULT NULL,
  `memo` tinytext,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetime)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetime)',
  `order_no` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `plg_taba_cms_category`
--

INSERT INTO `plg_taba_cms_category` (`category_id`, `type_id`, `data_key`, `category_name`, `description`, `tag_attributes`, `memo`, `create_date`, `update_date`, `order_no`) VALUES
(1, 1, 'notice', '重要なお知らせ', NULL, 'style=\"border:solid 1px #ff0000;background-color:#ffffff;color:#ff0000;\"', NULL, '2020-05-06 09:56:55', '2020-05-06 09:56:55', 0),
(2, 1, 'system', 'システムメンテナンス', NULL, 'style=\"border:solid 1px #cccccc;background-color:#ffffff;color:#cccccc;\"', NULL, '2020-05-06 09:57:33', '2020-05-06 09:57:33', 0),
(3, 1, 'info', 'お知らせ', NULL, 'style=\"border:solid 1px #5cb1b1;background-color:#ffffff;color:#5cb1b1;\"', NULL, '2020-05-06 09:58:08', '2020-05-06 09:58:08', 0),
(4, 2, 'column', 'コラム', NULL, 'style=\"border:solid 1px #ff0000;background-color:#ffffff;color:#ff0000;\"', NULL, '2020-05-06 09:59:48', '2020-05-11 09:26:49', 0),
(5, 2, 'shop_news', 'ショップニュース', NULL, 'style=\"border:solid 1px #5cb1b1;background-color:#ffffff;color:#5cb1b1;\"', NULL, '2020-05-06 10:01:44', '2020-05-11 09:26:21', 0);

-- --------------------------------------------------------

--
-- Table structure for table `plg_taba_cms_post`
--

CREATE TABLE `plg_taba_cms_post` (
  `post_id` int(11) NOT NULL,
  `creator_id` int(10) UNSIGNED NOT NULL,
  `type_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `data_key` varchar(255) NOT NULL,
  `public_div` int(11) NOT NULL,
  `public_date` datetime NOT NULL COMMENT '(DC2Type:datetime)',
  `content_div` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text,
  `link_url` varchar(255) DEFAULT NULL,
  `link_target` varchar(255) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `memo` tinytext,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetime)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetime)',
  `meta_author` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keyword` varchar(255) DEFAULT NULL,
  `meta_robots` varchar(255) DEFAULT NULL,
  `meta_tags` text,
  `overwrite_route` varchar(255) DEFAULT NULL,
  `script` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `plg_taba_cms_post`
--

INSERT INTO `plg_taba_cms_post` (`post_id`, `creator_id`, `type_id`, `category_id`, `data_key`, `public_div`, `public_date`, `content_div`, `title`, `body`, `link_url`, `link_target`, `thumbnail`, `memo`, `create_date`, `update_date`, `meta_author`, `meta_description`, `meta_keyword`, `meta_robots`, `meta_tags`, `overwrite_route`, `script`) VALUES
(1, 1, 1, 1, 'golden-week-2020-info', 1, '2020-05-06 09:49:00', 1, 'GW期間の営業・発送に関して', '<p style=\"vertical-align: baseline; font-family: &quot;Yu Gothic Medium&quot;, &quot;游ゴシック Medium&quot;, YuGothic, 游ゴシック体, &quot;ヒラギノ角ゴ Pro W3&quot;, メイリオ, sans-serif; outline: 0px; padding: 0px 0px 27px; margin: 0px; border: 0px; -webkit-font-smoothing: subpixel-antialiased; color: rgb(103, 103, 103);\">いつもご愛顧賜り誠にありがとうございます。</p><p style=\"vertical-align: baseline; font-family: &quot;Yu Gothic Medium&quot;, &quot;游ゴシック Medium&quot;, YuGothic, 游ゴシック体, &quot;ヒラギノ角ゴ Pro W3&quot;, メイリオ, sans-serif; outline: 0px; padding: 0px 0px 27px; margin: 0px; border: 0px; -webkit-font-smoothing: subpixel-antialiased; color: rgb(103, 103, 103);\">GW期間についての営業及び発送については下記の通りとなります。</p><p style=\"vertical-align: baseline; font-family: &quot;Yu Gothic Medium&quot;, &quot;游ゴシック Medium&quot;, YuGothic, 游ゴシック体, &quot;ヒラギノ角ゴ Pro W3&quot;, メイリオ, sans-serif; outline: 0px; padding: 0px 0px 27px; margin: 0px; border: 0px; -webkit-font-smoothing: subpixel-antialiased; color: rgb(103, 103, 103);\"><span style=\"vertical-align: baseline; font-weight: 600; font-style: inherit; outline: 0px; padding: 0px; margin: 0px; border: 0px;\">［GW休業期間 ］<br>4月29日（水）、5月2日（土）～5月6日（水）</span><br><br>4月28日（火曜日）午前11時までご注文いただければ当日発送させていただき、11時以降の場合は4月30日（木曜日）の発送とさせていただきます。<br>5月1日（金曜日）についても、午前11時までご注文いただければ当日発送させていただき、11時以降の場合は5月7日（木曜日）の発送となりますことご了承ください。<br><span style=\"vertical-align: baseline; font-weight: 600; font-style: inherit; outline: 0px; padding: 0px; margin: 0px; border: 0px;\">※期間中は配送業務・電話お問い合わせを行っておりません。またメールでの返信も遅れることがございますので予めご了承ください。</span></p><p style=\"vertical-align: baseline; font-family: &quot;Yu Gothic Medium&quot;, &quot;游ゴシック Medium&quot;, YuGothic, 游ゴシック体, &quot;ヒラギノ角ゴ Pro W3&quot;, メイリオ, sans-serif; outline: 0px; padding: 0px 0px 27px; margin: 0px; border: 0px; -webkit-font-smoothing: subpixel-antialiased; color: rgb(103, 103, 103);\">その他の日については、通常通り配送業務・お問い合わせ業務に対応しております。引き続き何卒お願い申し上げます。</p>', NULL, NULL, NULL, NULL, '2020-05-06 09:50:33', '2020-05-11 09:35:27', 'Maritime', '今話題のCBDオイルを試したら即効性がすごかった【ストレス緩和、睡眠改善、免疫疾患改善】', 'Maritime、CBDオイル、ストレス緩和、睡眠改善、免疫疾患改善', NULL, NULL, NULL, NULL),
(2, 1, 2, 5, 'my-first-time-testing-cbd-oil', 0, '2020-05-06 10:07:00', 1, '今話題のCBDオイルを試したら即効性がすごかった【ストレス緩和、睡眠改善、免疫疾患改善】', '<p style=\"padding: 0px; margin: 1.4em 0px; overflow-wrap: break-word; line-height: 1.8; color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif; font-size: 16px;\">こんにちは。<br style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">Maritimeです。</p><p style=\"padding: 0px; margin: 1.4em 0px; overflow-wrap: break-word; line-height: 1.8; color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif; font-size: 16px;\">先日の記事で大麻ビジネスの注目度を解説しました。<br style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">その中で紹介したCBDという成分を含むオイルを購入してみたので、今回はそのレビューをしたいと思います。</p><div id=\"toc\" class=\"toc tnt-number toc-center border-element\" style=\"padding: 0px; margin: 1.4em auto; overflow-wrap: break-word; border: 2px solid rgba(135, 135, 135, 0.5); font-size: 0.9em; line-height: 1.8; display: table; color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif;\"><label class=\"toc-title\" for=\"toc-checkbox-1\" style=\"padding: 2px 16px; margin: 0px; overflow-wrap: break-word; cursor: pointer; font-size: 1.1em; text-align: center; display: block; letter-spacing: 0.5em;\">目次</label><div class=\"toc-content\" style=\"padding: 0.6em 0px 0px; margin: 0px; overflow-wrap: break-word; border-top: 1px solid rgb(204, 204, 204); visibility: visible; height: 214.625px; opacity: 1; transition: all 0.5s ease-out 0s;\"><ol class=\"toc-list open\" style=\"padding: 0px 0px 0px 20px; margin: 0px 20px 20px; overflow-wrap: break-word;\"><li style=\"padding: 0px; margin: 0px 0px 6px; overflow-wrap: break-word;\"><a href=\"https://asanopapa.com/379/#toc1\" tabindex=\"0\" style=\"padding: 0px; margin: 0px; overflow-wrap: break-word; color: rgb(51, 51, 51);\">CBDの効果</a></li><li style=\"padding: 0px; margin: 0px 0px 6px; overflow-wrap: break-word;\"><a href=\"https://asanopapa.com/379/#toc2\" tabindex=\"0\" style=\"padding: 0px; margin: 0px; overflow-wrap: break-word; color: rgb(51, 51, 51);\">日本におけるCBDの流通</a></li><li style=\"padding: 0px; margin: 0px 0px 6px; overflow-wrap: break-word;\"><a href=\"https://asanopapa.com/379/#toc3\" tabindex=\"0\" style=\"padding: 0px; margin: 0px; overflow-wrap: break-word; color: rgb(51, 51, 51);\">CBDオイルを実際に試した結果</a><ol style=\"padding: 0px 0px 0px 20px; margin: 0px; overflow-wrap: break-word;\"><li style=\"padding: 0px; margin: 0px 0px 6px; overflow-wrap: break-word;\"><a href=\"https://asanopapa.com/379/#toc4\" tabindex=\"0\" style=\"padding: 0px; margin: 0px; overflow-wrap: break-word; color: rgb(51, 51, 51);\">CBDオイルを選ぶする基準</a></li><li style=\"padding: 0px; margin: 0px 0px 6px; overflow-wrap: break-word;\"><a href=\"https://asanopapa.com/379/#toc5\" tabindex=\"0\" style=\"padding: 0px; margin: 0px; overflow-wrap: break-word; color: rgb(51, 51, 51);\">購入したCBDオイル</a></li><li style=\"padding: 0px; margin: 0px 0px 6px; overflow-wrap: break-word;\"><a href=\"https://asanopapa.com/379/#toc6\" tabindex=\"0\" style=\"padding: 0px; margin: 0px; overflow-wrap: break-word; color: rgb(51, 51, 51);\">CBDオイルを使った感想</a></li></ol></li><li style=\"padding: 0px; margin: 0px 0px 6px; overflow-wrap: break-word;\"><a href=\"https://asanopapa.com/379/#toc7\" tabindex=\"0\" style=\"padding: 0px; margin: 0px; overflow-wrap: break-word; color: rgb(51, 51, 51);\">最後に</a></li></ol></div></div><h2 style=\"padding: 15px; margin: 50px 0px 28px; overflow-wrap: break-word; line-height: 1.25; font-weight: bold; font-size: 24px; background-color: rgb(135, 135, 135); border-radius: 2px; position: relative; border-top: 2px solid rgb(32, 32, 32); border-bottom: 2px solid rgb(32, 32, 32); color: rgb(255, 255, 255); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif;\"><span id=\"toc1\" style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">CBDの効果</span></h2><p style=\"padding: 0px; margin: 1.4em 0px; overflow-wrap: break-word; line-height: 1.8; color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif; font-size: 16px;\"><a target=\"_self\" href=\"https://asanopapa.com/wp-content/uploads/2020/01/HEMP-oil.jpg\" style=\"padding: 0px; margin: 0px; overflow-wrap: break-word; color: rgb(36, 55, 229);\"><img class=\"aligncenter size-large wp-image-382\" src=\"https://maritime.shourai.io/html/user_data/assets/img/category/Maritime_CDB_oils_all_products.jpg\" alt=\"大麻オイル\" width=\"600\" height=\"400\" srcset=\"https://maritime.shourai.io/html/user_data/assets/img/category/Maritime_CDB_oils_all_products.jpg 600w, https://maritime.shourai.io/html/user_data/assets/img/category/Maritime_CDB_oils_all_products.jpg 300w, https://maritime.shourai.io/html/user_data/assets/img/category/Maritime_CDB_oils_all_products.jpg 768w, https://maritime.shourai.io/html/user_data/assets/img/category/Maritime_CDB_oils_all_products.jpg 1350w\" sizes=\"(max-width: 600px) 100vw, 600px\" style=\"padding: 0px; margin: 0px auto; overflow-wrap: break-word; border: 0px; display: block;\"></a></p><p style=\"padding: 0px; margin: 1.4em 0px; overflow-wrap: break-word; line-height: 1.8; color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif; font-size: 16px;\">CBDオイルとは大麻草から得られる「カンナビジオール（CBD）」を含む成分です。<br style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">CBDには以下の効果があります。</p><div class=\"blank-box bb-tab bb-check\" style=\"padding: 1.2em 1em; margin: 2em 18px 1.4em; overflow-wrap: break-word; border: 3px solid rgb(148, 148, 149); border-radius: 0px 4px 4px; line-height: 1.8; position: relative; color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif; font-size: 16px;\"><div class=\"bb-label\" style=\"padding: 0px 1em 0px 0.8em; margin: 0px; overflow-wrap: break-word; background-color: rgb(148, 148, 149); font-family: Aharoni, &quot;Arial Black&quot;, Impact, Arial, sans-serif; position: absolute; font-size: 13px; top: -1.8em; line-height: 1.8; color: rgb(255, 255, 255); left: -3px; border-radius: 6px 6px 0px 0px;\"><span class=\"fa\" style=\"padding: 0px; margin: 0px 6px 0px 0px; overflow-wrap: break-word; font-weight: normal; font-family: FontAwesome;\"></span></div>・精神安定化<br style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">・炎症抑制<br style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">・発作抑制<br style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">・免疫系のバランスを整える</div><p style=\"padding: 0px; margin: 1.4em 0px; overflow-wrap: break-word; line-height: 1.8; color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif; font-size: 16px;\">上記のように様々な人体への良い効果があるため、健康食品や医療への応用が期待されている成分です。</p><p style=\"padding: 0px; margin: 1.4em 0px; overflow-wrap: break-word; line-height: 1.8; color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif; font-size: 16px;\">例えば、仕事で納期に追われ、常に顧客と接し緊張しっぱなしでストレスが溜まっている人。<br style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">このような人がCBDを摂取することで、夜の睡眠が深くなり脳の緊張をほぐす効果が期待でき、また仕事上においてもリラックスした状態で頑張ることができてストレスが溜まりにくくなります。</p><p style=\"padding: 0px; margin: 1.4em 0px; overflow-wrap: break-word; line-height: 1.8; color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif; font-size: 16px;\">また、アレルギーやアトピーを患い、免疫系の疾患を抱えている人。<br style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">このような人に対しても、免疫系の暴走を抑え、アレルギー発作の抑制やアトピーのかゆみの抑制に期待できます。</p><p style=\"padding: 0px; margin: 1.4em 0px; overflow-wrap: break-word; line-height: 1.8; color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif; font-size: 16px;\">この成分のすごいところは即効性があるということ。<br style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">ストレス系に効く漢方薬や健康に良いとされる食品のほとんどは即効性は無く、数か月継続してやっと効果が現れるといったものばかりです。<br style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">その点、このCBDは違います。<br style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">効果の対象にもよりますが、リラックス効果であれば、早ければ10分、標準でも30分くらいで効果は実感できます。</p><p style=\"padding: 0px; margin: 1.4em 0px; overflow-wrap: break-word; line-height: 1.8; color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif; font-size: 16px;\">注意点として、CBDは対症療法に属するため、定期的に摂取しないと元の状態に戻ってしまいます。<br style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">例えばアトピー患者がCBDを摂取するとして、摂取した一定時間はかゆみは軽減しますが、CBDの摂取をやめたら徐々に元のかゆみが辛い状態に戻ってしまいます。</p><h2 style=\"padding: 15px; margin: 50px 0px 28px; overflow-wrap: break-word; line-height: 1.25; font-weight: bold; font-size: 24px; background-color: rgb(135, 135, 135); border-radius: 2px; position: relative; border-top: 2px solid rgb(32, 32, 32); border-bottom: 2px solid rgb(32, 32, 32); color: rgb(255, 255, 255); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif;\"><span id=\"toc2\" style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">日本におけるCBDの流通</span></h2><p style=\"padding: 0px; margin: 1.4em 0px; overflow-wrap: break-word; line-height: 1.8; color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif; font-size: 16px;\"><a target=\"_self\" href=\"https://asanopapa.com/wp-content/uploads/2020/01/Cannabis-grass.jpg\" style=\"padding: 0px; margin: 0px; overflow-wrap: break-word; color: rgb(36, 55, 229);\"><img class=\"aligncenter size-large wp-image-384\" src=\"https://asanopapa.com/wp-content/uploads/2020/01/Cannabis-grass-600x400.jpg\" alt=\"大麻草\" width=\"600\" height=\"400\" srcset=\"https://asanopapa.com/wp-content/uploads/2020/01/Cannabis-grass-600x400.jpg 600w, https://asanopapa.com/wp-content/uploads/2020/01/Cannabis-grass-300x200.jpg 300w, https://asanopapa.com/wp-content/uploads/2020/01/Cannabis-grass-768x512.jpg 768w, https://asanopapa.com/wp-content/uploads/2020/01/Cannabis-grass.jpg 1351w\" sizes=\"(max-width: 600px) 100vw, 600px\" style=\"padding: 0px; margin: 0px auto; overflow-wrap: break-word; border: 0px; display: block;\"></a></p><p style=\"padding: 0px; margin: 1.4em 0px; overflow-wrap: break-word; line-height: 1.8; color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif; font-size: 16px;\">日本では大麻は法律で禁止されています。<br style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">大麻というのは大麻草の茎と種以外の部分になります。<br style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">裏を返せば大麻草でも茎と種に限れば条件付きで合法となるのです。</p><p style=\"padding: 0px; margin: 1.4em 0px; overflow-wrap: break-word; line-height: 1.8; color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif; font-size: 16px;\">最近になりCBDを含んだ製品が徐々に流通し始めていますが、これらの製品は全て「大麻草の茎と種から摂取されたTHCを含まない成分」という条件付きです。<br style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">（THCとはマリファナ等で有名な陶酔成分）<br style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">そのため日本におけるCBD製品は流通が限定されており、まだ世間に浸透して間もない製品であるため、Amazonや楽天で探しても製品数は未だ少ないです。<br style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">ちなみに外国の法律は大麻に対してもっと寛容であるため、CBD製品も日本より広く普及しています。</p><h2 style=\"padding: 15px; margin: 50px 0px 28px; overflow-wrap: break-word; line-height: 1.25; font-weight: bold; font-size: 24px; background-color: rgb(135, 135, 135); border-radius: 2px; position: relative; border-top: 2px solid rgb(32, 32, 32); border-bottom: 2px solid rgb(32, 32, 32); color: rgb(255, 255, 255); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif;\"><span id=\"toc3\" style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">CBDオイルを実際に試した結果</span></h2><p style=\"padding: 0px; margin: 1.4em 0px; overflow-wrap: break-word; line-height: 1.8; color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif; font-size: 16px;\"><a target=\"_self\" href=\"https://asanopapa.com/wp-content/uploads/2020/01/CBD-oil-1-scaled.jpg\" style=\"padding: 0px; margin: 0px; overflow-wrap: break-word; color: rgb(36, 55, 229);\"><img class=\"aligncenter size-large wp-image-381\" src=\"https://maritime.shourai.io/products/detail/2\" alt=\"CBDオイル\" width=\"400\" height=\"450\" srcset=\"https://maritime.shourai.io/html/upload/save_image/0429081633_5ea8b9513aa62.jpg 600w, https://maritime.shourai.io/html/upload/save_image/0429081633_5ea8b9513aa62.jpg 300w, https://maritime.shourai.io/html/upload/save_image/0429081633_5ea8b9513aa62.jpg 768w, https://maritime.shourai.io/html/upload/save_image/0429081633_5ea8b9513aa62.jpg 1536w, https://maritime.shourai.io/html/upload/save_image/0429081633_5ea8b9513aa62.jpg 2048w\" sizes=\"(max-width: 600px) 100vw, 600px\" style=\"padding: 0px; margin: 0px auto; overflow-wrap: break-word; border: 0px; display: block;\"></a></p><h3 style=\"padding: 6px 15px; margin: 45px 0px 1em; overflow-wrap: break-word; line-height: 1.25; font-weight: bold; border-left: none rgb(135, 135, 135); border-right: none rgb(135, 135, 135); border-top: none rgb(135, 135, 135); border-bottom: 3px solid rgb(135, 135, 135); font-size: 22px; border-image: initial; color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif;\"><span id=\"toc4\" style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">CBDオイルを選ぶする基準</span></h3><p style=\"padding: 0px; margin: 1.4em 0px; overflow-wrap: break-word; line-height: 1.8; color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif; font-size: 16px;\">まずCBDオイルを選ぶ基準を説明させてください。<br style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">選ぶ基準としては主に以下の2つです。</p><div class=\"blank-box\" style=\"padding: 1.2em 1em; margin: 1.4em 18px; overflow-wrap: break-word; border: 3px solid rgb(148, 148, 149); border-radius: 4px; line-height: 1.8; color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif; font-size: 16px;\">・CBDの含有量<br style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">・CBDの摂取形態</div><p style=\"padding: 0px; margin: 1.4em 0px; overflow-wrap: break-word; line-height: 1.8; color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif; font-size: 16px;\">CBDの含有量として200mg/50mlや500ml/10mlと製品によって含有率に差があります。<br style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">そのため同じオイル容量でもCBDの含有量が違えば価格も大きく違ってきます。</p><p style=\"padding: 0px; margin: 1.4em 0px; overflow-wrap: break-word; line-height: 1.8; color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif; font-size: 16px;\">また、CBDの摂取形態としてはオイルを舌裏に垂らし飲む形式と液体をスプレーで口内に噴霧する方式があります。<br style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">どちらでも所望の効果は得られますので、自身が摂取しやすそうな方を選ぶと良いと思います。</p><p style=\"padding: 0px; margin: 1.4em 0px; overflow-wrap: break-word; line-height: 1.8; color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif; font-size: 16px;\">なお、日本に流通しているCBD製品はどれもマリファナ等に含まれる陶酔成分（THC）を含んでいないので安心です。</p><h3 style=\"padding: 6px 15px; margin: 45px 0px 1em; overflow-wrap: break-word; line-height: 1.25; font-weight: bold; border-left: none rgb(135, 135, 135); border-right: none rgb(135, 135, 135); border-top: none rgb(135, 135, 135); border-bottom: 3px solid rgb(135, 135, 135); font-size: 22px; border-image: initial; color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif;\"><span id=\"toc5\" style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">購入したCBDオイル</span></h3><p style=\"padding: 0px; margin: 1.4em 0px; overflow-wrap: break-word; line-height: 1.8; color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif; font-size: 16px;\">購入したCBDオイルは楽天で見つけた値段も手ごろであり、またレビューも420件で4.42と一番レビュー数が多く評価も高い製品。<br style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\"></p><div class=\"rakuten-item-box product-item-box no-icon pis-m suppleplus:10000019 cf\" style=\"padding: 22px 25px; margin: 1.4em auto; overflow-wrap: break-word; width: 864px; border: 3px solid rgb(223, 223, 223); position: relative; display: flex; font-size: 16px; line-height: 1.8; color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif;\"><figure class=\"rakuten-item-thumb product-item-thumb\" style=\"padding: 0px; overflow-wrap: break-word; width: 160px; text-align: center; vertical-align: top; margin-right: auto !important; margin-bottom: 1em !important; margin-left: auto !important; min-width: auto !important; float: none !important;\"><a rel=\"nofollow noopener\" target=\"_blank\" href=\"https://hb.afl.rakuten.co.jp/hgc/g00srwb7.lyyzs40d.g00srwb7.lyyzt936/?pc=https%3A%2F%2Fitem.rakuten.co.jp%2Fsuppleplus%2F1725q%2F&amp;m=http%3A%2F%2Fm.rakuten.co.jp%2Fsuppleplus%2Fi%2F10000019%2F\" class=\"rakuten-item-thumb-link product-item-thumb-link\" title=\"CBDオイル200 【アウトレット：ラベル不備】CBD200mg 50ml カンナビジオール カンナビノイド ヘンプ 農薬不使用 vape 不眠 ストレス 過食　更年期 フルスペクトラム 花粉 リラックス THCゼロ PMS HSP 生理痛 IBS オリーブオイル【39ショップ】\" style=\"padding: 0px; margin: 0px; overflow-wrap: break-word; color: rgb(36, 55, 229); display: block;\"><img src=\"https://thumbnail.image.rakuten.co.jp/@0_mall/suppleplus/cabinet/thumb/cbd_ol_01_sleep.jpg?_ex=128x128\" alt=\"CBDオイル200 【アウトレット：ラベル不備】CBD200mg 50ml カンナビジオール カンナビノイド ヘンプ 農薬不使用 vape 不眠 ストレス 過食　更年期 フルスペクトラム 花粉 リラックス THCゼロ PMS HSP 生理痛 IBS オリーブオイル【39ショップ】\" width=\"128\" height=\"128\" class=\"rakuten-item-thumb-image product-item-thumb-image\" style=\"padding: 0px; margin: 0px auto; overflow-wrap: break-word; border: 0px; display: block;\"></a></figure><div class=\"rakuten-item-content product-item-content cf\" style=\"padding: 0px 0px 0px 25px; margin: 0px; overflow-wrap: break-word; line-height: 20px; width: 675.141px; vertical-align: top;\"><div class=\"rakuten-item-title product-item-title\" style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\"><a rel=\"nofollow noopener\" target=\"_blank\" href=\"https://hb.afl.rakuten.co.jp/hgc/g00srwb7.lyyzs40d.g00srwb7.lyyzt936/?pc=https%3A%2F%2Fitem.rakuten.co.jp%2Fsuppleplus%2F1725q%2F&amp;m=http%3A%2F%2Fm.rakuten.co.jp%2Fsuppleplus%2Fi%2F10000019%2F\" class=\"rakuten-item-title-link product-item-title-link\" title=\"CBDオイル200 【アウトレット：ラベル不備】CBD200mg 50ml カンナビジオール カンナビノイド ヘンプ 農薬不使用 vape 不眠 ストレス 過食　更年期 フルスペクトラム 花粉 リラックス THCゼロ PMS HSP 生理痛 IBS オリーブオイル【39ショップ】\" style=\"padding: 0px; margin: 0px; overflow-wrap: break-word; color: rgb(36, 55, 229);\">CBDオイル200 【アウトレット：ラベル不備】CBD200mg 50ml カンナビジオール カンナビノイド ヘンプ 農薬不使用 vape 不眠 ストレス 過食　更年期 フルスペクトラム 花粉 リラックス THCゼロ PMS HSP 生理痛 IBS オリーブオイル【39ショップ】</a></div><div class=\"rakuten-item-snippet product-item-snippet\" style=\"padding: 0px; margin: 6px 0px 0px; overflow-wrap: break-word; font-size: 0.8em;\"><div class=\"rakuten-item-maker product-item-maker\" style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">サプリプラス</div></div><div class=\"amazon-item-buttons product-item-buttons\" style=\"padding: 0px; margin: 1em 0px 0px; overflow-wrap: break-word; display: flex; flex-wrap: wrap;\"><div class=\"shoplinkrakuten\" style=\"margin: 3.25px; overflow-wrap: break-word; padding: 0px !important; background: none !important;\"><a rel=\"nofollow noopener\" target=\"_blank\" href=\"https://hb.afl.rakuten.co.jp/hgc/1b6d96a8.58185991.1b6d96a9.856f5dd9/?pc=https%3A%2F%2Fsearch.rakuten.co.jp%2Fsearch%2Fmall%2FCBD%E3%82%AA%E3%82%A4%E3%83%AB200+%E3%82%A2%E3%82%A6%E3%83%88%E3%83%AC%E3%83%83%E3%83%88+CBD200mg+50ml+%E3%82%B5%E3%83%97%E3%83%AA%E3%83%97%E3%83%A9%E3%82%B9%2F&amp;m=https%3A%2F%2Fsearch.rakuten.co.jp%2Fsearch%2Fmall%2FCBD%E3%82%AA%E3%82%A4%E3%83%AB200+%E3%82%A2%E3%82%A6%E3%83%88%E3%83%AC%E3%83%83%E3%83%88+CBD200mg+50ml+%E3%82%B5%E3%83%97%E3%83%AA%E3%83%97%E3%83%A9%E3%82%B9%2F\" style=\"padding: 6px 12px; margin: 0px auto 8px; overflow-wrap: break-word; color: rgb(255, 255, 255); width: auto; display: block; font-size: 13px; font-weight: bold; text-align: center; border-radius: 3px; background: rgb(191, 0, 0);\">楽天</a></div><div class=\"shoplinkyahoo\" style=\"margin: 3.25px; overflow-wrap: break-word; padding: 0px !important; background: none !important;\"><a rel=\"nofollow noopener\" target=\"_blank\" href=\"https://ck.jp.ap.valuecommerce.com/servlet/referral?sid=3498290&amp;pid=886515609&amp;vc_url=http%3A%2F%2Fsearch.shopping.yahoo.co.jp%2Fsearch%3Fp%3DCBD%E3%82%AA%E3%82%A4%E3%83%AB200+%E3%82%A2%E3%82%A6%E3%83%88%E3%83%AC%E3%83%83%E3%83%88+CBD200mg+50ml+%E3%82%B5%E3%83%97%E3%83%AA%E3%83%97%E3%83%A9%E3%82%B9\" style=\"padding: 6px 12px; margin: 0px auto 8px; overflow-wrap: break-word; color: rgb(255, 255, 255); width: auto; display: block; font-size: 13px; font-weight: bold; text-align: center; border-radius: 3px; background: rgb(230, 0, 51); position: relative;\">Yahoo!ショッピング</a></div></div></div></div><br style=\"padding: 0px; margin: 1.4em 0px; overflow-wrap: break-word; line-height: 1.8; color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif; font-size: 16px;\"><p><span style=\"color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif; font-size: 16px;\">私が購入したオイルは飲む形式で含有量としては少なめの200mg/50mlを購入しました。</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif; font-size: 16px;\">価格としては、アウトレット価格で3564円（税込み）であり、CBDオイルとしてはかなり安価な部類になります。</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif; font-size: 16px;\">アウトレットの理由としては製品のラベルに不備があるためです。</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif; font-size: 16px;\">オイル自体に不具合は無いため、アウトレット品でも効果は全く問題ありません。</span></p><p style=\"padding: 0px; margin: 1.4em 0px; overflow-wrap: break-word; line-height: 1.8; color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif; font-size: 16px;\">ちなみにCBDオイル1本の使用可能期間は、推奨使用方法である2回/1日の使用で約35日もちます。</p><h3 style=\"padding: 6px 15px; margin: 45px 0px 1em; overflow-wrap: break-word; line-height: 1.25; font-weight: bold; border-left: none rgb(135, 135, 135); border-right: none rgb(135, 135, 135); border-top: none rgb(135, 135, 135); border-bottom: 3px solid rgb(135, 135, 135); font-size: 22px; border-image: initial; color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif;\"><span id=\"toc6\" style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">CBDオイルを使った感想</span></h3><p style=\"padding: 0px; margin: 1.4em 0px; overflow-wrap: break-word; line-height: 1.8; color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif; font-size: 16px;\">使い方としては推奨の使用方法である以下に従って使用していました。</p><div class=\"blank-box\" style=\"padding: 1.2em 1em; margin: 1.4em 18px; overflow-wrap: break-word; border: 3px solid rgb(148, 148, 149); border-radius: 4px; line-height: 1.8; color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif; font-size: 16px;\">・1日2回摂取（朝、寝る前）<br style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">・付属のスポイトで1回舌裏にCBDオイルを垂らす<br style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">・垂らした後は1～2分間口の中で含み続け、その後飲み込む</div><p style=\"padding: 0px; margin: 1.4em 0px; overflow-wrap: break-word; line-height: 1.8; color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif; font-size: 16px;\">使った感想ですが、まず一言で言うと、最高でした。<br style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">これまでに色々な健康製品や疲れを癒す製品を試してきましたが、これらと比較してCBDオイルは群を抜いて一番良かったです。</p><p style=\"padding: 0px; margin: 1.4em 0px; overflow-wrap: break-word; line-height: 1.8; color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif; font-size: 16px;\">素晴らしいのが、即効性。<br style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">CBDオイルを始めて飲んだ数十分後から不思議とイライラを感じなくなりました。<br style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">普段だと仕事疲れで帰った後、ブログを記事ストックを書かなければと頭で思いながら家事を手伝い、子供のわがままに付き添ってイライラを必死に抑えることも多かったですが、CBDオイルを摂取した直後は自然と焦る気持ちは抑えられ、まるで第三者から物事を観ているかのように落ち着いて子供に接することができました。<br style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">これが良く言われるリラックス効果であると納得しました。</p><p style=\"padding: 0px; margin: 1.4em 0px; overflow-wrap: break-word; line-height: 1.8; color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif; font-size: 16px;\">睡眠にも変化がありました。<br style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">私の場合は朝起きるのが早くなりました。<br style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">ですが、不思議とスッキリしていて睡眠不足は全く感じません。<br style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">睡眠自体の質が良くなったんだと思います。</p><p style=\"padding: 0px; margin: 1.4em 0px; overflow-wrap: break-word; line-height: 1.8; color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif; font-size: 16px;\">CBDオイルは他にも、アトピーやアレルギー等の免疫が関係する病気にも効果があると言われています。<br style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">私はアトピーですが、今は症状は落ち着いていて完治といっても良い状態ですので、アトピーに対する効果は実感できていません。<br style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">またいつかアトピーが悪化した際に試すことができたら、ここに追記したいと思います。</p><p style=\"padding: 0px; margin: 1.4em 0px; overflow-wrap: break-word; line-height: 1.8; color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif; font-size: 16px;\">CBDオイルに興味持たれた方は一度試してみては如何でしょうか。<br style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">効果は個人差はあると思いますが、仕事で脳疲労やストレスが溜まっている方は試す価値はあると思います。</p><div class=\"rakuten-item-box product-item-box no-icon pis-m suppleplus:10000019 cf\" style=\"padding: 22px 25px; margin: 1.4em auto; overflow-wrap: break-word; width: 864px; border: 3px solid rgb(223, 223, 223); position: relative; display: flex; font-size: 16px; line-height: 1.8; color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif;\"><figure class=\"rakuten-item-thumb product-item-thumb\" style=\"padding: 0px; overflow-wrap: break-word; width: 160px; text-align: center; vertical-align: top; margin-right: auto !important; margin-bottom: 1em !important; margin-left: auto !important; min-width: auto !important; float: none !important;\"><a rel=\"nofollow noopener\" target=\"_blank\" href=\"https://hb.afl.rakuten.co.jp/hgc/g00srwb7.lyyzs40d.g00srwb7.lyyzt936/?pc=https%3A%2F%2Fitem.rakuten.co.jp%2Fsuppleplus%2F1725q%2F&amp;m=http%3A%2F%2Fm.rakuten.co.jp%2Fsuppleplus%2Fi%2F10000019%2F\" class=\"rakuten-item-thumb-link product-item-thumb-link\" title=\"CBDオイル200 【アウトレット：ラベル不備】CBD200mg 50ml カンナビジオール カンナビノイド ヘンプ 農薬不使用 vape 不眠 ストレス 過食　更年期 フルスペクトラム 花粉 リラックス THCゼロ PMS HSP 生理痛 IBS オリーブオイル【39ショップ】\" style=\"padding: 0px; margin: 0px; overflow-wrap: break-word; color: rgb(36, 55, 229); display: block;\"><img src=\"https://thumbnail.image.rakuten.co.jp/@0_mall/suppleplus/cabinet/thumb/cbd_ol_01_sleep.jpg?_ex=128x128\" alt=\"CBDオイル200 【アウトレット：ラベル不備】CBD200mg 50ml カンナビジオール カンナビノイド ヘンプ 農薬不使用 vape 不眠 ストレス 過食　更年期 フルスペクトラム 花粉 リラックス THCゼロ PMS HSP 生理痛 IBS オリーブオイル【39ショップ】\" width=\"128\" height=\"128\" class=\"rakuten-item-thumb-image product-item-thumb-image\" style=\"padding: 0px; margin: 0px auto; overflow-wrap: break-word; border: 0px; display: block;\"></a></figure><div class=\"rakuten-item-content product-item-content cf\" style=\"padding: 0px 0px 0px 25px; margin: 0px; overflow-wrap: break-word; line-height: 20px; width: 675.141px; vertical-align: top;\"><div class=\"rakuten-item-title product-item-title\" style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\"><a rel=\"nofollow noopener\" target=\"_blank\" href=\"https://hb.afl.rakuten.co.jp/hgc/g00srwb7.lyyzs40d.g00srwb7.lyyzt936/?pc=https%3A%2F%2Fitem.rakuten.co.jp%2Fsuppleplus%2F1725q%2F&amp;m=http%3A%2F%2Fm.rakuten.co.jp%2Fsuppleplus%2Fi%2F10000019%2F\" class=\"rakuten-item-title-link product-item-title-link\" title=\"CBDオイル200 【アウトレット：ラベル不備】CBD200mg 50ml カンナビジオール カンナビノイド ヘンプ 農薬不使用 vape 不眠 ストレス 過食　更年期 フルスペクトラム 花粉 リラックス THCゼロ PMS HSP 生理痛 IBS オリーブオイル【39ショップ】\" style=\"padding: 0px; margin: 0px; overflow-wrap: break-word; color: rgb(36, 55, 229);\">CBDオイル200 【アウトレット：ラベル不備】CBD200mg 50ml カンナビジオール カンナビノイド ヘンプ 農薬不使用 vape 不眠 ストレス 過食　更年期 フルスペクトラム 花粉 リラックス THCゼロ PMS HSP 生理痛 IBS オリーブオイル【39ショップ】</a></div><div class=\"rakuten-item-snippet product-item-snippet\" style=\"padding: 0px; margin: 6px 0px 0px; overflow-wrap: break-word; font-size: 0.8em;\"><div class=\"rakuten-item-maker product-item-maker\" style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">サプリプラス</div></div><div class=\"amazon-item-buttons product-item-buttons\" style=\"padding: 0px; margin: 1em 0px 0px; overflow-wrap: break-word; display: flex; flex-wrap: wrap;\"><div class=\"shoplinkrakuten\" style=\"margin: 3.25px; overflow-wrap: break-word; padding: 0px !important; background: none !important;\"><a rel=\"nofollow noopener\" target=\"_blank\" href=\"https://hb.afl.rakuten.co.jp/hgc/1b6d96a8.58185991.1b6d96a9.856f5dd9/?pc=https%3A%2F%2Fsearch.rakuten.co.jp%2Fsearch%2Fmall%2FCBD%E3%82%AA%E3%82%A4%E3%83%AB200+%E3%82%A2%E3%82%A6%E3%83%88%E3%83%AC%E3%83%83%E3%83%88+CBD200mg+50ml+%E3%82%B5%E3%83%97%E3%83%AA%E3%83%97%E3%83%A9%E3%82%B9%2F&amp;m=https%3A%2F%2Fsearch.rakuten.co.jp%2Fsearch%2Fmall%2FCBD%E3%82%AA%E3%82%A4%E3%83%AB200+%E3%82%A2%E3%82%A6%E3%83%88%E3%83%AC%E3%83%83%E3%83%88+CBD200mg+50ml+%E3%82%B5%E3%83%97%E3%83%AA%E3%83%97%E3%83%A9%E3%82%B9%2F\" style=\"padding: 6px 12px; margin: 0px auto 8px; overflow-wrap: break-word; color: rgb(255, 255, 255); width: auto; display: block; font-size: 13px; font-weight: bold; text-align: center; border-radius: 3px; background: rgb(191, 0, 0);\">楽天</a></div><div class=\"shoplinkyahoo\" style=\"margin: 3.25px; overflow-wrap: break-word; padding: 0px !important; background: none !important;\"><a rel=\"nofollow noopener\" target=\"_blank\" href=\"https://ck.jp.ap.valuecommerce.com/servlet/referral?sid=3498290&amp;pid=886515609&amp;vc_url=http%3A%2F%2Fsearch.shopping.yahoo.co.jp%2Fsearch%3Fp%3DCBD%E3%82%AA%E3%82%A4%E3%83%AB200+%E3%82%A2%E3%82%A6%E3%83%88%E3%83%AC%E3%83%83%E3%83%88+CBD200mg+50ml+%E3%82%B5%E3%83%97%E3%83%AA%E3%83%97%E3%83%A9%E3%82%B9\" style=\"padding: 6px 12px; margin: 0px auto 8px; overflow-wrap: break-word; color: rgb(255, 255, 255); width: auto; display: block; font-size: 13px; font-weight: bold; text-align: center; border-radius: 3px; background: rgb(230, 0, 51); position: relative;\">Yahoo!ショッピング</a></div></div></div></div><div class=\"blank-box sticky\" style=\"padding: 1.2em 1em; margin: 1.4em 18px; overflow-wrap: break-word; border-width: 0px 0px 0px 6px; border-style: solid; border-color: rgb(119, 119, 119); border-image: initial; border-radius: 4px; line-height: 1.8; background-color: rgb(243, 244, 245); color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif; font-size: 16px;\"><span class=\"bold blue\" style=\"padding: 0px; margin: 0px; overflow-wrap: break-word; font-weight: bold; color: rgb(0, 149, 217);\"><strong style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">追記：</strong></span><br style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">後日、CBD製品のオイルタイプ、食べるタイプ、吸うタイプを実際に使用し比較してみました。<br style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">それぞれのタイプの長所、短所がわかり購入時の参考になると思います。<br style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\"><a target=\"_self\" href=\"https://asanopapa.com/785/\" style=\"padding: 0px; margin: 0px; overflow-wrap: break-word; color: rgb(36, 55, 229);\">⇒様々な不調に効果が期待できるCBD製品はどのタイプが一番良いのか比べてみた【オイル、カプセル、リキッド】</a></div><h2 style=\"padding: 15px; margin: 50px 0px 28px; overflow-wrap: break-word; line-height: 1.25; font-weight: bold; font-size: 24px; background-color: rgb(135, 135, 135); border-radius: 2px; position: relative; border-top: 2px solid rgb(32, 32, 32); border-bottom: 2px solid rgb(32, 32, 32); color: rgb(255, 255, 255); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif;\"><span id=\"toc7\" style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">最後に</span></h2><p style=\"padding: 0px; margin: 1.4em 0px; overflow-wrap: break-word; line-height: 1.8; color: rgb(51, 51, 51); font-family: 游ゴシック体, &quot;Yu Gothic&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, sans-serif; font-size: 16px;\">最後まで読んでいただきありがとうございました。<br style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">今回は実際にCBDオイルを試してその良さを書かせていただきました。<br style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">まだに日本のCBD市場は開けたばかりです。<br style=\"padding: 0px; margin: 0px; overflow-wrap: break-word;\">今後CBDの様々な応用製品が登場し、予防医療として活躍することを願っています。</p>', NULL, NULL, '0506190707_5eb28c4bb0a11.png', NULL, '2020-05-06 10:07:19', '2020-06-18 06:49:18', 'Maritime', '今話題のCBDオイルを試したら即効性がすごかった【ストレス緩和、睡眠改善、免疫疾患改善】', 'Maritime、CBDオイル、ストレス緩和、睡眠改善、免疫疾患改善', NULL, NULL, NULL, NULL),
(3, 1, 2, 4, 'test-lefa', 0, '2020-05-11 08:36:00', 1, 'Test Leaflet', '<p style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sed sollicitudin purus. Cras at blandit turpis. Integer dapibus quam non porta porttitor. Proin placerat maximus mi, in pellentesque nunc. Etiam eu arcu at sem pharetra aliquet eleifend ut dolor. Nulla facilisi. Etiam condimentum gravida justo. Morbi a suscipit elit. Pellentesque eget magna at nisi interdum finibus.</p><p style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Nullam rutrum, justo nec varius sagittis, metus felis feugiat nisi, id consequat massa quam ut mi. Cras interdum vehicula lectus nec imperdiet. Nulla vehicula sodales turpis, vitae gravida nisi accumsan et. Sed interdum tristique facilisis. Pellentesque pretium a nunc non maximus. Sed semper ante eget tincidunt pretium. Phasellus eget fringilla dolor. Morbi varius lacus leo, non gravida tellus euismod a. Vivamus aliquet orci dui, non tristique ante molestie eu.</p>', NULL, NULL, '0511173405_5eb90dfd56331.jpg', NULL, '2020-05-11 08:36:42', '2020-06-18 06:37:00', 'Maritime', NULL, NULL, NULL, NULL, NULL, NULL),
(4, 1, 2, 4, 'cbd_oil_merit_test', 0, '2020-05-11 08:46:00', 1, 'CBDオイルのメリット 『動画』', '<p><iframe width=\"100%\" height=\"415\" src=\"https://www.youtube.com/embed/CwN-LByRLcw\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen=\"\"></iframe></p><br><p><span style=\"font-size: large;\">Benefits of CBD</span></p>\r\n<span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\"><br></span><p><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sed sollicitudin purus. Cras at blandit turpis. Integer dapibus quam non porta porttitor. Proin placerat maximus mi, in pellentesque nunc. Etiam eu arcu at sem pharetra aliquet eleifend ut dolor. Nulla facilisi. Etiam condimentum gravida justo. Morbi a suscipit elit. Pellentesque eget magna at nisi interdum finibus.</span></p><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\"></span><p style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Nullam rutrum, justo nec varius sagittis, metus felis feugiat nisi, id consequat massa quam ut mi. Cras interdum vehicula lectus nec imperdiet. Nulla vehicula sodales turpis, vitae gravida nisi accumsan et. Sed interdum tristique facilisis. Pellentesque pretium a nunc non maximus. Sed semper ante eget tincidunt pretium. Phasellus eget fringilla dolor. Morbi varius lacus leo, non gravida tellus euismod a. Vivamus aliquet orci dui, non tristique ante molestie eu.</p>', NULL, NULL, '0511174523_5eb910a3814c3.jpg', NULL, '2020-05-11 08:46:31', '2020-06-18 06:36:46', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 1, 2, 5, 'free-test-campaign-test', 0, '2020-05-11 10:21:00', 1, '☆4月限定☆CBDオイル無料お試しキャンペーン', '<p><span style=\"color: rgb(51, 51, 51); font-family: メイリオ, Meiryo, &quot;ＭＳ Ｐゴシック&quot;, &quot;MS P Gothic&quot;, &quot;ヒラギノ角ゴ Pro W3&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Osaka, arial, sans-serif, verdana, Helvetica; font-size: medium;\">こんばんは！Maritimeの◯◯です(*^-^*)</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: メイリオ, Meiryo, &quot;ＭＳ Ｐゴシック&quot;, &quot;MS P Gothic&quot;, &quot;ヒラギノ角ゴ Pro W3&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Osaka, arial, sans-serif, verdana, Helvetica; font-size: medium;\">新年度が始まり、みなさまいかがお過ごしでしょうか？</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: メイリオ, Meiryo, &quot;ＭＳ Ｐゴシック&quot;, &quot;MS P Gothic&quot;, &quot;ヒラギノ角ゴ Pro W3&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Osaka, arial, sans-serif, verdana, Helvetica; font-size: medium;\">新型コロナの影響が色んなところに出てきているのを、私も肌で感じております。</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: メイリオ, Meiryo, &quot;ＭＳ Ｐゴシック&quot;, &quot;MS P Gothic&quot;, &quot;ヒラギノ角ゴ Pro W3&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Osaka, arial, sans-serif, verdana, Helvetica; font-size: medium;\">こんな状況なので、今日も変わらない一日を過ごせたこと、明日もお仕事があることがすごくありがたく感じます。</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: メイリオ, Meiryo, &quot;ＭＳ Ｐゴシック&quot;, &quot;MS P Gothic&quot;, &quot;ヒラギノ角ゴ Pro W3&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Osaka, arial, sans-serif, verdana, Helvetica; font-size: medium;\">リピーターのお客様が1か月ぶりのご来店で変わらず元気な姿を見せて下さると、本当に嬉しく思います。</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: メイリオ, Meiryo, &quot;ＭＳ Ｐゴシック&quot;, &quot;MS P Gothic&quot;, &quot;ヒラギノ角ゴ Pro W3&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Osaka, arial, sans-serif, verdana, Helvetica; font-size: medium;\">私たちの身体には、somatic resonanceが備わっているそうです。</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: メイリオ, Meiryo, &quot;ＭＳ Ｐゴシック&quot;, &quot;MS P Gothic&quot;, &quot;ヒラギノ角ゴ Pro W3&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Osaka, arial, sans-serif, verdana, Helvetica; font-size: medium;\">日本語にすると、身体的共鳴というものだそうです。</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: メイリオ, Meiryo, &quot;ＭＳ Ｐゴシック&quot;, &quot;MS P Gothic&quot;, &quot;ヒラギノ角ゴ Pro W3&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Osaka, arial, sans-serif, verdana, Helvetica; font-size: medium;\">何も言葉にしなくても、近くにいる人の不安や恐れはなんとなく感じ取れてしまったり、</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: メイリオ, Meiryo, &quot;ＭＳ Ｐゴシック&quot;, &quot;MS P Gothic&quot;, &quot;ヒラギノ角ゴ Pro W3&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Osaka, arial, sans-serif, verdana, Helvetica; font-size: medium;\">それに共鳴して自分もブルーになってしまうこともあったり。そんな能力のことです。</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: メイリオ, Meiryo, &quot;ＭＳ Ｐゴシック&quot;, &quot;MS P Gothic&quot;, &quot;ヒラギノ角ゴ Pro W3&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Osaka, arial, sans-serif, verdana, Helvetica; font-size: medium;\">知識としては知らない人も、なんとなく経験から知っているのではないでしょうか。</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: メイリオ, Meiryo, &quot;ＭＳ Ｐゴシック&quot;, &quot;MS P Gothic&quot;, &quot;ヒラギノ角ゴ Pro W3&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Osaka, arial, sans-serif, verdana, Helvetica; font-size: medium;\">人間の持つ身体機能は本当にすごいですね。</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: メイリオ, Meiryo, &quot;ＭＳ Ｐゴシック&quot;, &quot;MS P Gothic&quot;, &quot;ヒラギノ角ゴ Pro W3&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Osaka, arial, sans-serif, verdana, Helvetica; font-size: medium;\">ホッと安心できる感覚や、happy なエネルギーを手のひらから伝えていけるように、</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: メイリオ, Meiryo, &quot;ＭＳ Ｐゴシック&quot;, &quot;MS P Gothic&quot;, &quot;ヒラギノ角ゴ Pro W3&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Osaka, arial, sans-serif, verdana, Helvetica; font-size: medium;\">私も日々の施術で心がけていきたいなと改めて思いました。</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: メイリオ, Meiryo, &quot;ＭＳ Ｐゴシック&quot;, &quot;MS P Gothic&quot;, &quot;ヒラギノ角ゴ Pro W3&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Osaka, arial, sans-serif, verdana, Helvetica; font-size: medium;\">さて、前置きが長くなってしまいましたが、、</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: メイリオ, Meiryo, &quot;ＭＳ Ｐゴシック&quot;, &quot;MS P Gothic&quot;, &quot;ヒラギノ角ゴ Pro W3&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Osaka, arial, sans-serif, verdana, Helvetica; font-size: medium;\">４月のスペシャルなキャンペーンのお知らせです♪(^o^)/</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: メイリオ, Meiryo, &quot;ＭＳ Ｐゴシック&quot;, &quot;MS P Gothic&quot;, &quot;ヒラギノ角ゴ Pro W3&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Osaka, arial, sans-serif, verdana, Helvetica; font-size: medium;\">★☆今月ご来店のお客様全員に、CBDオイル無料サービスします！！☆★</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: メイリオ, Meiryo, &quot;ＭＳ Ｐゴシック&quot;, &quot;MS P Gothic&quot;, &quot;ヒラギノ角ゴ Pro W3&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Osaka, arial, sans-serif, verdana, Helvetica; font-size: medium;\">・１本３万円以上する、最高グレードのホップCBDオイルをお試しいただけます♪</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: メイリオ, Meiryo, &quot;ＭＳ Ｐゴシック&quot;, &quot;MS P Gothic&quot;, &quot;ヒラギノ角ゴ Pro W3&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Osaka, arial, sans-serif, verdana, Helvetica; font-size: medium;\">・CBDオイルは、加齢とともに衰えていく「自己調整機能」をアップさせてくれるといわれています</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: メイリオ, Meiryo, &quot;ＭＳ Ｐゴシック&quot;, &quot;MS P Gothic&quot;, &quot;ヒラギノ角ゴ Pro W3&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Osaka, arial, sans-serif, verdana, Helvetica; font-size: medium;\">・アメリカでは既に大ブームとなっていて、日本でもこれから人気が高まるといわれています</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: メイリオ, Meiryo, &quot;ＭＳ Ｐゴシック&quot;, &quot;MS P Gothic&quot;, &quot;ヒラギノ角ゴ Pro W3&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Osaka, arial, sans-serif, verdana, Helvetica; font-size: medium;\">・多くの論文で疾患への効果が検証されており、アメリカでは医薬品として承認されたものもあります</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: メイリオ, Meiryo, &quot;ＭＳ Ｐゴシック&quot;, &quot;MS P Gothic&quot;, &quot;ヒラギノ角ゴ Pro W3&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Osaka, arial, sans-serif, verdana, Helvetica; font-size: medium;\">・精神的リラックスを高めてくれるといわれており、ボディケアとの相乗効果が期待できます</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: メイリオ, Meiryo, &quot;ＭＳ Ｐゴシック&quot;, &quot;MS P Gothic&quot;, &quot;ヒラギノ角ゴ Pro W3&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Osaka, arial, sans-serif, verdana, Helvetica; font-size: medium;\">CBDオイルは舌下に垂らして吸収させます。</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: メイリオ, Meiryo, &quot;ＭＳ Ｐゴシック&quot;, &quot;MS P Gothic&quot;, &quot;ヒラギノ角ゴ Pro W3&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Osaka, arial, sans-serif, verdana, Helvetica; font-size: medium;\">日本でも雑誌で紹介されたり、医療ドラマで登場したりしたので、気になっていた方も多いのではないでしょうか。</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: メイリオ, Meiryo, &quot;ＭＳ Ｐゴシック&quot;, &quot;MS P Gothic&quot;, &quot;ヒラギノ角ゴ Pro W3&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Osaka, arial, sans-serif, verdana, Helvetica; font-size: medium;\">ぜひこの機会に無料でお試しください♪</span></p><div class=\"taC\" style=\"padding: 0px; margin: 25px 0px; text-size-adjust: none; color: rgb(51, 51, 51); font-family: メイリオ, Meiryo, &quot;ＭＳ Ｐゴシック&quot;, &quot;MS P Gothic&quot;, &quot;ヒラギノ角ゴ Pro W3&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Osaka, arial, sans-serif, verdana, Helvetica; font-size: 12px; text-align: center !important;\"><img src=\"https://imgbp.hotp.jp/CSP/IMG_BLOG_K/88/68/I045418868/I045418868_349-262.jpg\" alt=\"☆4月限定☆CBDオイル無料お試しキャンペーン_20200402_1\" style=\"padding: 0px; margin: 0px; text-size-adjust: none; vertical-align: bottom; border: 0px !important;\"></div>', NULL, NULL, '0511192122_5eb927222d827.jpg', NULL, '2020-05-11 10:21:26', '2020-06-18 06:36:26', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 1, 2, NULL, 'blog-20200930', 1, '2020-09-30 06:45:00', 1, 'Test frederic', 'test', NULL, NULL, NULL, NULL, '2020-09-30 06:45:06', '2020-09-30 06:45:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `plg_taba_cms_post` (`post_id`, `creator_id`, `type_id`, `category_id`, `data_key`, `public_div`, `public_date`, `content_div`, `title`, `body`, `link_url`, `link_target`, `thumbnail`, `memo`, `create_date`, `update_date`, `meta_author`, `meta_description`, `meta_keyword`, `meta_robots`, `meta_tags`, `overwrite_route`, `script`) VALUES
(7, 1, 1, NULL, 'news-20201019', 0, '2020-10-19 10:38:00', 1, 'CBDウォーターとは｜日本で合法？効果や口コミを紹介', '<p><span style=\"font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, &quot;Noto Sans&quot;, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;;\">CBDウォーターについて詳しく知りたい、日本で購入を考えている方に、CBDの効果や摂取量の目安を解説します。CBDウォーターの評判や口コミも紹介します。</span><br></p><br><h2>CBDウォーターとは</h2><br><p>CBDウォーターとは植物由来成分CBDを配合した清涼飲料水です。</p><br><p>2000年以降CBDに関する研究が進み、様々な治療効果をもたらすことが明らかになり注目が集まっています。アメリカをはじめ世界でCBD製品の人気が広がりをみせています。</p><br><p>CBDウォーターを摂取することでCBDの作用による心身への健康効果が期待されます。</p><br><h3>CBDとは</h3><br><p>CBD（カンナビジオール）は大麻に含まれる成分（カンナビノイド）で、神経系に良い作用をすることで知られています。数多くあるカンナビノイドの中でも、医療目的での研究が積極的に進められています。</p><br><p>薬効成分の研究がされており、アメリカでは小児てんかんの治療薬エピディオレックスとして医薬品の認可がされました。</p><br><h3>CBDオイル、ウォーターは合法？</h3><br><p>日本では大麻草の花穂・葉・未熟な茎は大麻取締法によって規制されています。また、大麻由来の成分を含む嗜好品も同様に規制されています。一方で、「ハイになる」と言われるTHCの成分が含まれない成熟した茎から抽出されるCBD製品は、日本の法律に触れることはありません。</p><br><p>CBDは幻覚などの向精神作用を持たず、依存性もなく合法です。</p><br><h2>CBDオイルやCBDウォーターを摂取するとどんな効果がある？</h2><br><p>「CBDオイル」や「CBDウォーター」など、自分の好きなときに好きな量を摂取できる製品が人気です。</p><br><p>以下のような悩みを抱えている人にCBDウォーターの効果が期待されます。</p><br><p>不眠を改善し質の高い睡眠をとりたい</p><p>ストレスを和らげリラックス効果を得たい</p><p>自律神経のバランスを整えたい</p><p>不安感を緩和したい</p><br><p>CBDを摂取することで、人間の身体調節機能であるECS（エンド・カンナビノイド・システム）の機能低下が改善する効果が得られるとされます。</p><br><p>他にも、CBDには以下のような効果があることが研究により明らかになっています。</p><br><p>抗炎症作用&nbsp;</p><p>鎮痛作用</p><p>抗痙攣作用</p><p>嘔吐作用を抑制</p><p>抗不安作用</p><p>細胞障害の抑制と改善作用</p><br><p>など。現在もなおCBDの研究は進められており、今後さらに活用されることが期待されています。</p><br><h2>CBDウォーターの口コミ・評判</h2><br><p>CBDウォーター購入者のレビューや口コミでは、睡眠の質の向上やリラックスする目的での利用が多く見られます。</p><br><p>リラックスしたいときに飲んでみましたが、気持ちが楽になりました。</p><p>イライラの治りを感じられた。</p><p>ゆったりした気分になれて飲みやすい。</p><p>気起床時の背面の気怠さが軽減されていました。</p><p>無味無臭でお水が柔らかく、飲みやすい。</p><br><h2>ハワイでCBDウォーターが人気</h2><br><p>ハワイでは2017年に医療大麻の販売が解禁になったのをきっかけに、大麻関連製品に注目が集まりました。（医療大麻は激しい痛みを緩和するために用いたり、医師の診断のもとに処方されるもので市販のCBD製品とは異なります。）</p><br><p>医薬品ではないCBD製品は、ウォーター、サプリ、グミなどの食品をはじめハワイのスーパーやコンビニなどでよく見かけられるようになりました。</p><br><p>また、抗炎症や鎮静効果もあることから美容分野でも注目が高く、コスメショップにCBDウォーターローション、ジェル、クリームなど並んでいます。エステやマッサージでの利用も増えています。</p><br><p>旅行者でも気軽に購入できるため、旅先のハワイで試したというブログやSNSでの口コミが見られます。</p><br><h2>CBDウォーターの摂取目安量は？</h2><br><p>人それぞれの体調や体重によって適量は異なります。研究でも症状に対し量を変えて使用しており、慢性痛の治療には2.5〜20mg/日、睡眠障害の治療には40〜160mg/日を経口投与など、大幅に差があります。副作用は1日あたり1500mg投与しても問題はないとされます。しかし、症状や効果の感じ方は人により違うため、全ての人に同量の適量が示されるものではありません。</p><br><p>参考に医療用CBDとしてアメリカで承認された小児てんかんの治療薬エピディオレックスの例では、体重1kg当たり2.5mgを1日2回、1日5mg/kgの摂取量とされています。例えば体重50kgの人は、1回125mgを1日2回の摂取目安となります。ただしこれは医師により処方された医薬品です。</p><br><p>一般に市販されているCBD製品では、1日あたり20〜40mgくらいを目安として摂取することが好ましいとされます。効果の様子を見ながら、1週間ごとに5mgずつ摂取量を増やすようにしてください。MARITIMEのCBDウォーターはボトル1本あたり24mgのCBDを配合しています。タイミングは1日2回朝晩、または1日3回食後に摂取するのが良いでしょう。</p><br><p>繰り返しになりますが、健康状態や体質、体重も関係して人により効果の感じ方は異なります。そのため、最初は少量から始めて徐々に量を増やすようにしてください。</p><br><p>摂取時の注意点としては、眠気を引き起こす可能性がある薬を服用している人はCBDの摂取を避けるか量を減らす必要があります。また、車の運転も避けた方が良いでしょう。</p><br><h2>CBDウォーターが日本で購入できるショップ</h2><br><p>合法で安全なCBD製品を購入するなら、日本で生産されている商品がおすすめです。</p><br><p>MARITIMEのCBDウォーターは厚生労働省の正式な許可を得てアメリカより輸入した高品質な原料を使用して日本で製造されています。向精神作用のあるTHCは一切入っておらず、安心できるCBD製品です。</p><br><p>濃度99%以上のCBDを24mg配合CBDウォーター（500ml）￥1,253 （税込）</p><p>毎日継続できるCBDウォーター6本セット（500ml×6）￥7,517 （税込）</p><br><p>毎日の健康のために香料・甘味料・カロリーはゼロです。お申し込み時に毎月の定期購読を選択いただくと最大20％割引で大変お得です。</p><br>', NULL, NULL, NULL, NULL, '2020-10-19 10:38:00', '2020-10-19 10:41:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `plg_taba_cms_type`
--

CREATE TABLE `plg_taba_cms_type` (
  `type_id` int(11) NOT NULL,
  `data_key` varchar(255) NOT NULL,
  `public_div` int(11) NOT NULL,
  `type_name` varchar(255) NOT NULL,
  `edit_div` int(11) NOT NULL,
  `memo` longtext,
  `create_date` datetime NOT NULL COMMENT '(DC2Type:datetime)',
  `update_date` datetime NOT NULL COMMENT '(DC2Type:datetime)',
  `public_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `plg_taba_cms_type`
--

INSERT INTO `plg_taba_cms_type` (`type_id`, `data_key`, `public_div`, `type_name`, `edit_div`, `memo`, `create_date`, `update_date`, `public_id`) VALUES
(1, 'news', 1, '新着情報', 0, NULL, '2020-05-06 09:38:57', '2020-05-06 09:38:57', 2),
(2, 'blog', 1, 'ブログ', 0, NULL, '2020-05-06 09:38:57', '2020-05-06 09:38:57', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dtb_authority_role`
--
ALTER TABLE `dtb_authority_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_4A1F70B181EC865B` (`authority_id`),
  ADD KEY `IDX_4A1F70B161220EA6` (`creator_id`);

--
-- Indexes for table `dtb_base_info`
--
ALTER TABLE `dtb_base_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_1D3655F4F92F3E70` (`country_id`),
  ADD KEY `IDX_1D3655F4E171EF5F` (`pref_id`);

--
-- Indexes for table `dtb_block`
--
ALTER TABLE `dtb_block`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `device_type_id` (`device_type_id`,`file_name`),
  ADD KEY `IDX_6B54DCBD4FFA550E` (`device_type_id`);

--
-- Indexes for table `dtb_block_position`
--
ALTER TABLE `dtb_block_position`
  ADD PRIMARY KEY (`section`,`block_id`,`layout_id`),
  ADD KEY `IDX_35DCD731E9ED820C` (`block_id`),
  ADD KEY `IDX_35DCD7318C22AA1A` (`layout_id`);

--
-- Indexes for table `dtb_cart`
--
ALTER TABLE `dtb_cart`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `dtb_cart_pre_order_id_idx` (`pre_order_id`),
  ADD KEY `IDX_FC3C24F09395C3F3` (`customer_id`),
  ADD KEY `dtb_cart_update_date_idx` (`update_date`);

--
-- Indexes for table `dtb_cart_item`
--
ALTER TABLE `dtb_cart_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_B0228F7421B06187` (`product_class_id`),
  ADD KEY `IDX_B0228F741AD5CDBF` (`cart_id`);

--
-- Indexes for table `dtb_category`
--
ALTER TABLE `dtb_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_5ED2C2B796A8F92` (`parent_category_id`),
  ADD KEY `IDX_5ED2C2B61220EA6` (`creator_id`);

--
-- Indexes for table `dtb_class_category`
--
ALTER TABLE `dtb_class_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_9B0D1DBAB462FB2A` (`class_name_id`),
  ADD KEY `IDX_9B0D1DBA61220EA6` (`creator_id`);

--
-- Indexes for table `dtb_class_name`
--
ALTER TABLE `dtb_class_name`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_187C95AD61220EA6` (`creator_id`);

--
-- Indexes for table `dtb_csv`
--
ALTER TABLE `dtb_csv`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_F55F48C3E8507796` (`csv_type_id`),
  ADD KEY `IDX_F55F48C361220EA6` (`creator_id`);

--
-- Indexes for table `dtb_customer`
--
ALTER TABLE `dtb_customer`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `secret_key` (`secret_key`),
  ADD KEY `IDX_8298BBE3C00AF8A7` (`customer_status_id`),
  ADD KEY `IDX_8298BBE35A2DB2A0` (`sex_id`),
  ADD KEY `IDX_8298BBE3BE04EA9` (`job_id`),
  ADD KEY `IDX_8298BBE3F92F3E70` (`country_id`),
  ADD KEY `IDX_8298BBE3E171EF5F` (`pref_id`),
  ADD KEY `dtb_customer_buy_times_idx` (`buy_times`),
  ADD KEY `dtb_customer_buy_total_idx` (`buy_total`),
  ADD KEY `dtb_customer_create_date_idx` (`create_date`),
  ADD KEY `dtb_customer_update_date_idx` (`update_date`),
  ADD KEY `dtb_customer_last_buy_date_idx` (`last_buy_date`),
  ADD KEY `dtb_customer_email_idx` (`email`),
  ADD KEY `IDX_8298BBE39A406193` (`plg_ccp_customer_class_id`);

--
-- Indexes for table `dtb_customer_address`
--
ALTER TABLE `dtb_customer_address`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_6C38C0F89395C3F3` (`customer_id`),
  ADD KEY `IDX_6C38C0F8F92F3E70` (`country_id`),
  ADD KEY `IDX_6C38C0F8E171EF5F` (`pref_id`);

--
-- Indexes for table `dtb_customer_favorite_product`
--
ALTER TABLE `dtb_customer_favorite_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_ED6313839395C3F3` (`customer_id`),
  ADD KEY `IDX_ED6313834584665A` (`product_id`);

--
-- Indexes for table `dtb_delivery`
--
ALTER TABLE `dtb_delivery`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_3420D9FA61220EA6` (`creator_id`),
  ADD KEY `IDX_3420D9FAB0524E01` (`sale_type_id`);

--
-- Indexes for table `dtb_delivery_duration`
--
ALTER TABLE `dtb_delivery_duration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dtb_delivery_fee`
--
ALTER TABLE `dtb_delivery_fee`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_491552412136921` (`delivery_id`),
  ADD KEY `IDX_4915524E171EF5F` (`pref_id`);

--
-- Indexes for table `dtb_delivery_time`
--
ALTER TABLE `dtb_delivery_time`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_E80EE3A612136921` (`delivery_id`);

--
-- Indexes for table `dtb_layout`
--
ALTER TABLE `dtb_layout`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_5A62AA7C4FFA550E` (`device_type_id`);

--
-- Indexes for table `dtb_mail_history`
--
ALTER TABLE `dtb_mail_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_4870AB118D9F6D38` (`order_id`),
  ADD KEY `IDX_4870AB1161220EA6` (`creator_id`);

--
-- Indexes for table `dtb_mail_template`
--
ALTER TABLE `dtb_mail_template`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_1CB16DB261220EA6` (`creator_id`);

--
-- Indexes for table `dtb_member`
--
ALTER TABLE `dtb_member`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_10BC3BE6BB3453DB` (`work_id`),
  ADD KEY `IDX_10BC3BE681EC865B` (`authority_id`),
  ADD KEY `IDX_10BC3BE661220EA6` (`creator_id`);

--
-- Indexes for table `dtb_news`
--
ALTER TABLE `dtb_news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_EA4C351761220EA6` (`creator_id`);

--
-- Indexes for table `dtb_order`
--
ALTER TABLE `dtb_order`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `dtb_order_pre_order_id_idx` (`pre_order_id`),
  ADD KEY `IDX_1D66D8079395C3F3` (`customer_id`),
  ADD KEY `IDX_1D66D807F92F3E70` (`country_id`),
  ADD KEY `IDX_1D66D807E171EF5F` (`pref_id`),
  ADD KEY `IDX_1D66D8075A2DB2A0` (`sex_id`),
  ADD KEY `IDX_1D66D807BE04EA9` (`job_id`),
  ADD KEY `IDX_1D66D8074C3A3BB` (`payment_id`),
  ADD KEY `IDX_1D66D8074FFA550E` (`device_type_id`),
  ADD KEY `IDX_1D66D807D7707B45` (`order_status_id`),
  ADD KEY `dtb_order_email_idx` (`email`),
  ADD KEY `dtb_order_order_date_idx` (`order_date`),
  ADD KEY `dtb_order_payment_date_idx` (`payment_date`),
  ADD KEY `dtb_order_update_date_idx` (`update_date`),
  ADD KEY `dtb_order_order_no_idx` (`order_no`);

--
-- Indexes for table `dtb_order_item`
--
ALTER TABLE `dtb_order_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_A0C8C3ED8D9F6D38` (`order_id`),
  ADD KEY `IDX_A0C8C3ED4584665A` (`product_id`),
  ADD KEY `IDX_A0C8C3ED21B06187` (`product_class_id`),
  ADD KEY `IDX_A0C8C3ED4887F3F8` (`shipping_id`),
  ADD KEY `IDX_A0C8C3ED1BD5C574` (`rounding_type_id`),
  ADD KEY `IDX_A0C8C3ED84042C99` (`tax_type_id`),
  ADD KEY `IDX_A0C8C3EDA2505856` (`tax_display_type_id`),
  ADD KEY `IDX_A0C8C3EDCAD13EAD` (`order_item_type_id`);

--
-- Indexes for table `dtb_order_pdf`
--
ALTER TABLE `dtb_order_pdf`
  ADD PRIMARY KEY (`member_id`);

--
-- Indexes for table `dtb_page`
--
ALTER TABLE `dtb_page`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_E3951A67D0618E8C` (`master_page_id`),
  ADD KEY `dtb_page_url_idx` (`url`);

--
-- Indexes for table `dtb_page_layout`
--
ALTER TABLE `dtb_page_layout`
  ADD PRIMARY KEY (`page_id`,`layout_id`),
  ADD KEY `IDX_F2799941C4663E4` (`page_id`),
  ADD KEY `IDX_F27999418C22AA1A` (`layout_id`);

--
-- Indexes for table `dtb_payment`
--
ALTER TABLE `dtb_payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_7AFF628F61220EA6` (`creator_id`);

--
-- Indexes for table `dtb_payment_option`
--
ALTER TABLE `dtb_payment_option`
  ADD PRIMARY KEY (`delivery_id`,`payment_id`),
  ADD KEY `IDX_5631540D12136921` (`delivery_id`),
  ADD KEY `IDX_5631540D4C3A3BB` (`payment_id`);

--
-- Indexes for table `dtb_plugin`
--
ALTER TABLE `dtb_plugin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dtb_product`
--
ALTER TABLE `dtb_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_C49DE22F61220EA6` (`creator_id`),
  ADD KEY `IDX_C49DE22F557B630` (`product_status_id`);

--
-- Indexes for table `dtb_product_category`
--
ALTER TABLE `dtb_product_category`
  ADD PRIMARY KEY (`product_id`,`category_id`),
  ADD KEY `IDX_B05778914584665A` (`product_id`),
  ADD KEY `IDX_B057789112469DE2` (`category_id`);

--
-- Indexes for table `dtb_product_class`
--
ALTER TABLE `dtb_product_class`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_1A11D1BA4584665A` (`product_id`),
  ADD KEY `IDX_1A11D1BAB0524E01` (`sale_type_id`),
  ADD KEY `IDX_1A11D1BA248D128` (`class_category_id1`),
  ADD KEY `IDX_1A11D1BA9B418092` (`class_category_id2`),
  ADD KEY `IDX_1A11D1BABA4269E` (`delivery_duration_id`),
  ADD KEY `IDX_1A11D1BA61220EA6` (`creator_id`),
  ADD KEY `dtb_product_class_price02_idx` (`price02`),
  ADD KEY `dtb_product_class_stock_stock_unlimited_idx` (`stock`,`stock_unlimited`);

--
-- Indexes for table `dtb_product_image`
--
ALTER TABLE `dtb_product_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_3267CC7A4584665A` (`product_id`),
  ADD KEY `IDX_3267CC7A61220EA6` (`creator_id`);

--
-- Indexes for table `dtb_product_stock`
--
ALTER TABLE `dtb_product_stock`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_BC6C9E4521B06187` (`product_class_id`),
  ADD KEY `IDX_BC6C9E4561220EA6` (`creator_id`);

--
-- Indexes for table `dtb_product_tag`
--
ALTER TABLE `dtb_product_tag`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_4433E7214584665A` (`product_id`),
  ADD KEY `IDX_4433E721BAD26311` (`tag_id`),
  ADD KEY `IDX_4433E72161220EA6` (`creator_id`);

--
-- Indexes for table `dtb_shipping`
--
ALTER TABLE `dtb_shipping`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_2EBD22CE8D9F6D38` (`order_id`),
  ADD KEY `IDX_2EBD22CEF92F3E70` (`country_id`),
  ADD KEY `IDX_2EBD22CEE171EF5F` (`pref_id`),
  ADD KEY `IDX_2EBD22CE12136921` (`delivery_id`),
  ADD KEY `IDX_2EBD22CE61220EA6` (`creator_id`);

--
-- Indexes for table `dtb_tag`
--
ALTER TABLE `dtb_tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dtb_tax_rule`
--
ALTER TABLE `dtb_tax_rule`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_59F696DE21B06187` (`product_class_id`),
  ADD KEY `IDX_59F696DE61220EA6` (`creator_id`),
  ADD KEY `IDX_59F696DEF92F3E70` (`country_id`),
  ADD KEY `IDX_59F696DEE171EF5F` (`pref_id`),
  ADD KEY `IDX_59F696DE4584665A` (`product_id`),
  ADD KEY `IDX_59F696DE1BD5C574` (`rounding_type_id`);

--
-- Indexes for table `dtb_template`
--
ALTER TABLE `dtb_template`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_94C12A694FFA550E` (`device_type_id`);

--
-- Indexes for table `migration_ContactManagement4`
--
ALTER TABLE `migration_ContactManagement4`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `mtb_authority`
--
ALTER TABLE `mtb_authority`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mtb_country`
--
ALTER TABLE `mtb_country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mtb_csv_type`
--
ALTER TABLE `mtb_csv_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mtb_customer_order_status`
--
ALTER TABLE `mtb_customer_order_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mtb_customer_status`
--
ALTER TABLE `mtb_customer_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mtb_device_type`
--
ALTER TABLE `mtb_device_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mtb_job`
--
ALTER TABLE `mtb_job`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mtb_order_item_type`
--
ALTER TABLE `mtb_order_item_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mtb_order_status`
--
ALTER TABLE `mtb_order_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mtb_order_status_color`
--
ALTER TABLE `mtb_order_status_color`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mtb_page_max`
--
ALTER TABLE `mtb_page_max`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mtb_pref`
--
ALTER TABLE `mtb_pref`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mtb_product_list_max`
--
ALTER TABLE `mtb_product_list_max`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mtb_product_list_order_by`
--
ALTER TABLE `mtb_product_list_order_by`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mtb_product_status`
--
ALTER TABLE `mtb_product_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mtb_rounding_type`
--
ALTER TABLE `mtb_rounding_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mtb_sale_type`
--
ALTER TABLE `mtb_sale_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mtb_sex`
--
ALTER TABLE `mtb_sex`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mtb_tax_display_type`
--
ALTER TABLE `mtb_tax_display_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mtb_tax_type`
--
ALTER TABLE `mtb_tax_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mtb_work`
--
ALTER TABLE `mtb_work`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plg_ccp_config`
--
ALTER TABLE `plg_ccp_config`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_5BAA81411BD5C574` (`rounding_type_id`);

--
-- Indexes for table `plg_ccp_customer_class`
--
ALTER TABLE `plg_ccp_customer_class`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plg_ccp_customer_class_price`
--
ALTER TABLE `plg_ccp_customer_class_price`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_26A264FBC6C5483F` (`customer_class_id`),
  ADD KEY `IDX_26A264FB21B06187` (`product_class_id`);

--
-- Indexes for table `plg_check_kagoochi_sendedmail`
--
ALTER TABLE `plg_check_kagoochi_sendedmail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `target_kbn` (`target_kbn`,`target_id`),
  ADD KEY `send_date` (`send_date`),
  ADD KEY `target_id` (`target_id`);

--
-- Indexes for table `plg_contact_management4_config`
--
ALTER TABLE `plg_contact_management4_config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plg_contact_management4_contact`
--
ALTER TABLE `plg_contact_management4_contact`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_53DDB4B9395C3F3` (`customer_id`),
  ADD KEY `IDX_53DDB4BD4D57CD` (`staff_id`),
  ADD KEY `IDX_53DDB4B4584665A` (`product_id`),
  ADD KEY `IDX_53DDB4BE171EF5F` (`pref_id`),
  ADD KEY `IDX_53DDB4B50623C6` (`contact_status_id`);

--
-- Indexes for table `plg_contact_management4_contact_status`
--
ALTER TABLE `plg_contact_management4_contact_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plg_coupon`
--
ALTER TABLE `plg_coupon`
  ADD PRIMARY KEY (`coupon_id`),
  ADD UNIQUE KEY `UNIQ_755A31039C2A7D91` (`coupon_cd`);

--
-- Indexes for table `plg_coupon_detail`
--
ALTER TABLE `plg_coupon_detail`
  ADD PRIMARY KEY (`coupon_detail_id`),
  ADD KEY `IDX_7B9D14166C5951B` (`coupon_id`),
  ADD KEY `IDX_7B9D1414584665A` (`product_id`),
  ADD KEY `IDX_7B9D14112469DE2` (`category_id`);

--
-- Indexes for table `plg_coupon_order`
--
ALTER TABLE `plg_coupon_order`
  ADD PRIMARY KEY (`coupon_order_id`);

--
-- Indexes for table `plg_gmo_payment_gateway_config`
--
ALTER TABLE `plg_gmo_payment_gateway_config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plg_gmo_payment_gateway_member`
--
ALTER TABLE `plg_gmo_payment_gateway_member`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plg_gmo_payment_gateway_order_payment`
--
ALTER TABLE `plg_gmo_payment_gateway_order_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plg_gmo_payment_gateway_payment_method`
--
ALTER TABLE `plg_gmo_payment_gateway_payment_method`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plg_mailmaga_send_history`
--
ALTER TABLE `plg_mailmaga_send_history`
  ADD PRIMARY KEY (`send_id`),
  ADD KEY `IDX_424AD01261220EA6` (`creator_id`);

--
-- Indexes for table `plg_mailmaga_template`
--
ALTER TABLE `plg_mailmaga_template`
  ADD PRIMARY KEY (`template_id`);

--
-- Indexes for table `plg_paypal_config`
--
ALTER TABLE `plg_paypal_config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plg_paypal_subscribing_customer`
--
ALTER TABLE `plg_paypal_subscribing_customer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_7E353C69395C3F3` (`customer_id`),
  ADD KEY `IDX_7E353C621B06187` (`product_class_id`),
  ADD KEY `IDX_7E353C676500E2D` (`reference_transaction_id`);

--
-- Indexes for table `plg_paypal_transaction`
--
ALTER TABLE `plg_paypal_transaction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_2F162BF78D9F6D38` (`order_id`);

--
-- Indexes for table `plg_product_class_reserve4_extra`
--
ALTER TABLE `plg_product_class_reserve4_extra`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_CF38906921B06187` (`product_class_id`),
  ADD KEY `IDX_CF389069248D128` (`class_category_id1`),
  ADD KEY `IDX_CF3890699B418092` (`class_category_id2`),
  ADD KEY `IDX_CF3890694584665A` (`product_id`);

--
-- Indexes for table `plg_product_contact4_config`
--
ALTER TABLE `plg_product_contact4_config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plg_product_display_rank4_config`
--
ALTER TABLE `plg_product_display_rank4_config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plg_product_reserve4_config`
--
ALTER TABLE `plg_product_reserve4_config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plg_product_reserve4_extra`
--
ALTER TABLE `plg_product_reserve4_extra`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `plg_product_reserve4_order`
--
ALTER TABLE `plg_product_reserve4_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_322E96EE8D9F6D38` (`order_id`),
  ADD KEY `IDX_322E96EE4584665A` (`product_id`);

--
-- Indexes for table `plg_product_review`
--
ALTER TABLE `plg_product_review`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_9CA38FA25A2DB2A0` (`sex_id`),
  ADD KEY `IDX_9CA38FA24584665A` (`product_id`),
  ADD KEY `IDX_9CA38FA29395C3F3` (`customer_id`),
  ADD KEY `IDX_9CA38FA26BF700BD` (`status_id`);

--
-- Indexes for table `plg_product_review_config`
--
ALTER TABLE `plg_product_review_config`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_D27A17EFE8507796` (`csv_type_id`);

--
-- Indexes for table `plg_product_review_status`
--
ALTER TABLE `plg_product_review_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plg_taba_cms_category`
--
ALTER TABLE `plg_taba_cms_category`
  ADD PRIMARY KEY (`category_id`),
  ADD UNIQUE KEY `plg_taba_cms_category_unique_data_key` (`type_id`,`data_key`),
  ADD KEY `IDX_946A754FC54C8C93` (`type_id`);

--
-- Indexes for table `plg_taba_cms_post`
--
ALTER TABLE `plg_taba_cms_post`
  ADD PRIMARY KEY (`post_id`),
  ADD UNIQUE KEY `UNIQ_316AB2EFCE1C4A1C` (`data_key`),
  ADD KEY `IDX_316AB2EF12469DE2` (`category_id`),
  ADD KEY `IDX_316AB2EFC54C8C93` (`type_id`),
  ADD KEY `IDX_316AB2EF61220EA6` (`creator_id`);

--
-- Indexes for table `plg_taba_cms_type`
--
ALTER TABLE `plg_taba_cms_type`
  ADD PRIMARY KEY (`type_id`),
  ADD UNIQUE KEY `UNIQ_E73E894BCE1C4A1C` (`data_key`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dtb_authority_role`
--
ALTER TABLE `dtb_authority_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `dtb_base_info`
--
ALTER TABLE `dtb_base_info`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `dtb_block`
--
ALTER TABLE `dtb_block`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `dtb_cart`
--
ALTER TABLE `dtb_cart`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=313;
--
-- AUTO_INCREMENT for table `dtb_cart_item`
--
ALTER TABLE `dtb_cart_item`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=364;
--
-- AUTO_INCREMENT for table `dtb_category`
--
ALTER TABLE `dtb_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `dtb_class_category`
--
ALTER TABLE `dtb_class_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `dtb_class_name`
--
ALTER TABLE `dtb_class_name`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `dtb_csv`
--
ALTER TABLE `dtb_csv`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=216;
--
-- AUTO_INCREMENT for table `dtb_customer`
--
ALTER TABLE `dtb_customer`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `dtb_customer_address`
--
ALTER TABLE `dtb_customer_address`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `dtb_customer_favorite_product`
--
ALTER TABLE `dtb_customer_favorite_product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `dtb_delivery`
--
ALTER TABLE `dtb_delivery`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `dtb_delivery_duration`
--
ALTER TABLE `dtb_delivery_duration`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `dtb_delivery_fee`
--
ALTER TABLE `dtb_delivery_fee`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;
--
-- AUTO_INCREMENT for table `dtb_delivery_time`
--
ALTER TABLE `dtb_delivery_time`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `dtb_layout`
--
ALTER TABLE `dtb_layout`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `dtb_mail_history`
--
ALTER TABLE `dtb_mail_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `dtb_mail_template`
--
ALTER TABLE `dtb_mail_template`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `dtb_member`
--
ALTER TABLE `dtb_member`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `dtb_news`
--
ALTER TABLE `dtb_news`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dtb_order`
--
ALTER TABLE `dtb_order`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;
--
-- AUTO_INCREMENT for table `dtb_order_item`
--
ALTER TABLE `dtb_order_item`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=899;
--
-- AUTO_INCREMENT for table `dtb_page`
--
ALTER TABLE `dtb_page`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT for table `dtb_payment`
--
ALTER TABLE `dtb_payment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `dtb_plugin`
--
ALTER TABLE `dtb_plugin`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `dtb_product`
--
ALTER TABLE `dtb_product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `dtb_product_class`
--
ALTER TABLE `dtb_product_class`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `dtb_product_image`
--
ALTER TABLE `dtb_product_image`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=379;
--
-- AUTO_INCREMENT for table `dtb_product_stock`
--
ALTER TABLE `dtb_product_stock`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT for table `dtb_product_tag`
--
ALTER TABLE `dtb_product_tag`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=467;
--
-- AUTO_INCREMENT for table `dtb_shipping`
--
ALTER TABLE `dtb_shipping`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;
--
-- AUTO_INCREMENT for table `dtb_tag`
--
ALTER TABLE `dtb_tag`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `dtb_tax_rule`
--
ALTER TABLE `dtb_tax_rule`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `dtb_template`
--
ALTER TABLE `dtb_template`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `plg_ccp_config`
--
ALTER TABLE `plg_ccp_config`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `plg_ccp_customer_class`
--
ALTER TABLE `plg_ccp_customer_class`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `plg_ccp_customer_class_price`
--
ALTER TABLE `plg_ccp_customer_class_price`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `plg_check_kagoochi_sendedmail`
--
ALTER TABLE `plg_check_kagoochi_sendedmail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `plg_contact_management4_config`
--
ALTER TABLE `plg_contact_management4_config`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `plg_contact_management4_contact`
--
ALTER TABLE `plg_contact_management4_contact`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `plg_coupon`
--
ALTER TABLE `plg_coupon`
  MODIFY `coupon_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `plg_coupon_detail`
--
ALTER TABLE `plg_coupon_detail`
  MODIFY `coupon_detail_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `plg_coupon_order`
--
ALTER TABLE `plg_coupon_order`
  MODIFY `coupon_order_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `plg_gmo_payment_gateway_config`
--
ALTER TABLE `plg_gmo_payment_gateway_config`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `plg_gmo_payment_gateway_member`
--
ALTER TABLE `plg_gmo_payment_gateway_member`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `plg_gmo_payment_gateway_order_payment`
--
ALTER TABLE `plg_gmo_payment_gateway_order_payment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;
--
-- AUTO_INCREMENT for table `plg_gmo_payment_gateway_payment_method`
--
ALTER TABLE `plg_gmo_payment_gateway_payment_method`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `plg_mailmaga_send_history`
--
ALTER TABLE `plg_mailmaga_send_history`
  MODIFY `send_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `plg_mailmaga_template`
--
ALTER TABLE `plg_mailmaga_template`
  MODIFY `template_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `plg_paypal_config`
--
ALTER TABLE `plg_paypal_config`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `plg_paypal_subscribing_customer`
--
ALTER TABLE `plg_paypal_subscribing_customer`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `plg_paypal_transaction`
--
ALTER TABLE `plg_paypal_transaction`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `plg_product_class_reserve4_extra`
--
ALTER TABLE `plg_product_class_reserve4_extra`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `plg_product_contact4_config`
--
ALTER TABLE `plg_product_contact4_config`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `plg_product_display_rank4_config`
--
ALTER TABLE `plg_product_display_rank4_config`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `plg_product_reserve4_config`
--
ALTER TABLE `plg_product_reserve4_config`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `plg_product_reserve4_order`
--
ALTER TABLE `plg_product_reserve4_order`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `plg_product_review`
--
ALTER TABLE `plg_product_review`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `plg_product_review_config`
--
ALTER TABLE `plg_product_review_config`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `plg_taba_cms_category`
--
ALTER TABLE `plg_taba_cms_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `plg_taba_cms_post`
--
ALTER TABLE `plg_taba_cms_post`
  MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `plg_taba_cms_type`
--
ALTER TABLE `plg_taba_cms_type`
  MODIFY `type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `dtb_authority_role`
--
ALTER TABLE `dtb_authority_role`
  ADD CONSTRAINT `FK_4A1F70B161220EA6` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`id`),
  ADD CONSTRAINT `FK_4A1F70B181EC865B` FOREIGN KEY (`authority_id`) REFERENCES `mtb_authority` (`id`);

--
-- Constraints for table `dtb_base_info`
--
ALTER TABLE `dtb_base_info`
  ADD CONSTRAINT `FK_1D3655F4E171EF5F` FOREIGN KEY (`pref_id`) REFERENCES `mtb_pref` (`id`),
  ADD CONSTRAINT `FK_1D3655F4F92F3E70` FOREIGN KEY (`country_id`) REFERENCES `mtb_country` (`id`);

--
-- Constraints for table `dtb_block`
--
ALTER TABLE `dtb_block`
  ADD CONSTRAINT `FK_6B54DCBD4FFA550E` FOREIGN KEY (`device_type_id`) REFERENCES `mtb_device_type` (`id`);

--
-- Constraints for table `dtb_block_position`
--
ALTER TABLE `dtb_block_position`
  ADD CONSTRAINT `FK_35DCD7318C22AA1A` FOREIGN KEY (`layout_id`) REFERENCES `dtb_layout` (`id`),
  ADD CONSTRAINT `FK_35DCD731E9ED820C` FOREIGN KEY (`block_id`) REFERENCES `dtb_block` (`id`);

--
-- Constraints for table `dtb_cart`
--
ALTER TABLE `dtb_cart`
  ADD CONSTRAINT `FK_FC3C24F09395C3F3` FOREIGN KEY (`customer_id`) REFERENCES `dtb_customer` (`id`);

--
-- Constraints for table `dtb_cart_item`
--
ALTER TABLE `dtb_cart_item`
  ADD CONSTRAINT `FK_B0228F741AD5CDBF` FOREIGN KEY (`cart_id`) REFERENCES `dtb_cart` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_B0228F7421B06187` FOREIGN KEY (`product_class_id`) REFERENCES `dtb_product_class` (`id`);

--
-- Constraints for table `dtb_category`
--
ALTER TABLE `dtb_category`
  ADD CONSTRAINT `FK_5ED2C2B61220EA6` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`id`),
  ADD CONSTRAINT `FK_5ED2C2B796A8F92` FOREIGN KEY (`parent_category_id`) REFERENCES `dtb_category` (`id`);

--
-- Constraints for table `dtb_class_category`
--
ALTER TABLE `dtb_class_category`
  ADD CONSTRAINT `FK_9B0D1DBA61220EA6` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`id`),
  ADD CONSTRAINT `FK_9B0D1DBAB462FB2A` FOREIGN KEY (`class_name_id`) REFERENCES `dtb_class_name` (`id`);

--
-- Constraints for table `dtb_class_name`
--
ALTER TABLE `dtb_class_name`
  ADD CONSTRAINT `FK_187C95AD61220EA6` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`id`);

--
-- Constraints for table `dtb_csv`
--
ALTER TABLE `dtb_csv`
  ADD CONSTRAINT `FK_F55F48C361220EA6` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`id`),
  ADD CONSTRAINT `FK_F55F48C3E8507796` FOREIGN KEY (`csv_type_id`) REFERENCES `mtb_csv_type` (`id`);

--
-- Constraints for table `dtb_customer`
--
ALTER TABLE `dtb_customer`
  ADD CONSTRAINT `FK_8298BBE35A2DB2A0` FOREIGN KEY (`sex_id`) REFERENCES `mtb_sex` (`id`),
  ADD CONSTRAINT `FK_8298BBE39A406193` FOREIGN KEY (`plg_ccp_customer_class_id`) REFERENCES `plg_ccp_customer_class` (`id`),
  ADD CONSTRAINT `FK_8298BBE3BE04EA9` FOREIGN KEY (`job_id`) REFERENCES `mtb_job` (`id`),
  ADD CONSTRAINT `FK_8298BBE3C00AF8A7` FOREIGN KEY (`customer_status_id`) REFERENCES `mtb_customer_status` (`id`),
  ADD CONSTRAINT `FK_8298BBE3E171EF5F` FOREIGN KEY (`pref_id`) REFERENCES `mtb_pref` (`id`),
  ADD CONSTRAINT `FK_8298BBE3F92F3E70` FOREIGN KEY (`country_id`) REFERENCES `mtb_country` (`id`);

--
-- Constraints for table `dtb_customer_address`
--
ALTER TABLE `dtb_customer_address`
  ADD CONSTRAINT `FK_6C38C0F89395C3F3` FOREIGN KEY (`customer_id`) REFERENCES `dtb_customer` (`id`),
  ADD CONSTRAINT `FK_6C38C0F8E171EF5F` FOREIGN KEY (`pref_id`) REFERENCES `mtb_pref` (`id`),
  ADD CONSTRAINT `FK_6C38C0F8F92F3E70` FOREIGN KEY (`country_id`) REFERENCES `mtb_country` (`id`);

--
-- Constraints for table `dtb_customer_favorite_product`
--
ALTER TABLE `dtb_customer_favorite_product`
  ADD CONSTRAINT `FK_ED6313834584665A` FOREIGN KEY (`product_id`) REFERENCES `dtb_product` (`id`),
  ADD CONSTRAINT `FK_ED6313839395C3F3` FOREIGN KEY (`customer_id`) REFERENCES `dtb_customer` (`id`);

--
-- Constraints for table `dtb_delivery`
--
ALTER TABLE `dtb_delivery`
  ADD CONSTRAINT `FK_3420D9FA61220EA6` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`id`),
  ADD CONSTRAINT `FK_3420D9FAB0524E01` FOREIGN KEY (`sale_type_id`) REFERENCES `mtb_sale_type` (`id`);

--
-- Constraints for table `dtb_delivery_fee`
--
ALTER TABLE `dtb_delivery_fee`
  ADD CONSTRAINT `FK_491552412136921` FOREIGN KEY (`delivery_id`) REFERENCES `dtb_delivery` (`id`),
  ADD CONSTRAINT `FK_4915524E171EF5F` FOREIGN KEY (`pref_id`) REFERENCES `mtb_pref` (`id`);

--
-- Constraints for table `dtb_delivery_time`
--
ALTER TABLE `dtb_delivery_time`
  ADD CONSTRAINT `FK_E80EE3A612136921` FOREIGN KEY (`delivery_id`) REFERENCES `dtb_delivery` (`id`);

--
-- Constraints for table `dtb_layout`
--
ALTER TABLE `dtb_layout`
  ADD CONSTRAINT `FK_5A62AA7C4FFA550E` FOREIGN KEY (`device_type_id`) REFERENCES `mtb_device_type` (`id`);

--
-- Constraints for table `dtb_mail_history`
--
ALTER TABLE `dtb_mail_history`
  ADD CONSTRAINT `FK_4870AB1161220EA6` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`id`),
  ADD CONSTRAINT `FK_4870AB118D9F6D38` FOREIGN KEY (`order_id`) REFERENCES `dtb_order` (`id`);

--
-- Constraints for table `dtb_mail_template`
--
ALTER TABLE `dtb_mail_template`
  ADD CONSTRAINT `FK_1CB16DB261220EA6` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`id`);

--
-- Constraints for table `dtb_member`
--
ALTER TABLE `dtb_member`
  ADD CONSTRAINT `FK_10BC3BE661220EA6` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`id`),
  ADD CONSTRAINT `FK_10BC3BE681EC865B` FOREIGN KEY (`authority_id`) REFERENCES `mtb_authority` (`id`),
  ADD CONSTRAINT `FK_10BC3BE6BB3453DB` FOREIGN KEY (`work_id`) REFERENCES `mtb_work` (`id`);

--
-- Constraints for table `dtb_news`
--
ALTER TABLE `dtb_news`
  ADD CONSTRAINT `FK_EA4C351761220EA6` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`id`);

--
-- Constraints for table `dtb_order`
--
ALTER TABLE `dtb_order`
  ADD CONSTRAINT `FK_1D66D8074C3A3BB` FOREIGN KEY (`payment_id`) REFERENCES `dtb_payment` (`id`),
  ADD CONSTRAINT `FK_1D66D8074FFA550E` FOREIGN KEY (`device_type_id`) REFERENCES `mtb_device_type` (`id`),
  ADD CONSTRAINT `FK_1D66D8075A2DB2A0` FOREIGN KEY (`sex_id`) REFERENCES `mtb_sex` (`id`),
  ADD CONSTRAINT `FK_1D66D8079395C3F3` FOREIGN KEY (`customer_id`) REFERENCES `dtb_customer` (`id`),
  ADD CONSTRAINT `FK_1D66D807BE04EA9` FOREIGN KEY (`job_id`) REFERENCES `mtb_job` (`id`),
  ADD CONSTRAINT `FK_1D66D807E171EF5F` FOREIGN KEY (`pref_id`) REFERENCES `mtb_pref` (`id`),
  ADD CONSTRAINT `FK_1D66D807F92F3E70` FOREIGN KEY (`country_id`) REFERENCES `mtb_country` (`id`);

--
-- Constraints for table `dtb_order_item`
--
ALTER TABLE `dtb_order_item`
  ADD CONSTRAINT `FK_A0C8C3ED1BD5C574` FOREIGN KEY (`rounding_type_id`) REFERENCES `mtb_rounding_type` (`id`),
  ADD CONSTRAINT `FK_A0C8C3ED21B06187` FOREIGN KEY (`product_class_id`) REFERENCES `dtb_product_class` (`id`),
  ADD CONSTRAINT `FK_A0C8C3ED4584665A` FOREIGN KEY (`product_id`) REFERENCES `dtb_product` (`id`),
  ADD CONSTRAINT `FK_A0C8C3ED4887F3F8` FOREIGN KEY (`shipping_id`) REFERENCES `dtb_shipping` (`id`),
  ADD CONSTRAINT `FK_A0C8C3ED84042C99` FOREIGN KEY (`tax_type_id`) REFERENCES `mtb_tax_type` (`id`),
  ADD CONSTRAINT `FK_A0C8C3ED8D9F6D38` FOREIGN KEY (`order_id`) REFERENCES `dtb_order` (`id`),
  ADD CONSTRAINT `FK_A0C8C3EDA2505856` FOREIGN KEY (`tax_display_type_id`) REFERENCES `mtb_tax_display_type` (`id`),
  ADD CONSTRAINT `FK_A0C8C3EDCAD13EAD` FOREIGN KEY (`order_item_type_id`) REFERENCES `mtb_order_item_type` (`id`);

--
-- Constraints for table `dtb_page`
--
ALTER TABLE `dtb_page`
  ADD CONSTRAINT `FK_E3951A67D0618E8C` FOREIGN KEY (`master_page_id`) REFERENCES `dtb_page` (`id`);

--
-- Constraints for table `dtb_page_layout`
--
ALTER TABLE `dtb_page_layout`
  ADD CONSTRAINT `FK_F27999418C22AA1A` FOREIGN KEY (`layout_id`) REFERENCES `dtb_layout` (`id`),
  ADD CONSTRAINT `FK_F2799941C4663E4` FOREIGN KEY (`page_id`) REFERENCES `dtb_page` (`id`);

--
-- Constraints for table `dtb_payment`
--
ALTER TABLE `dtb_payment`
  ADD CONSTRAINT `FK_7AFF628F61220EA6` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`id`);

--
-- Constraints for table `dtb_payment_option`
--
ALTER TABLE `dtb_payment_option`
  ADD CONSTRAINT `FK_5631540D12136921` FOREIGN KEY (`delivery_id`) REFERENCES `dtb_delivery` (`id`),
  ADD CONSTRAINT `FK_5631540D4C3A3BB` FOREIGN KEY (`payment_id`) REFERENCES `dtb_payment` (`id`);

--
-- Constraints for table `dtb_product`
--
ALTER TABLE `dtb_product`
  ADD CONSTRAINT `FK_C49DE22F557B630` FOREIGN KEY (`product_status_id`) REFERENCES `mtb_product_status` (`id`),
  ADD CONSTRAINT `FK_C49DE22F61220EA6` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`id`);

--
-- Constraints for table `dtb_product_category`
--
ALTER TABLE `dtb_product_category`
  ADD CONSTRAINT `FK_B057789112469DE2` FOREIGN KEY (`category_id`) REFERENCES `dtb_category` (`id`),
  ADD CONSTRAINT `FK_B05778914584665A` FOREIGN KEY (`product_id`) REFERENCES `dtb_product` (`id`);

--
-- Constraints for table `dtb_product_class`
--
ALTER TABLE `dtb_product_class`
  ADD CONSTRAINT `FK_1A11D1BA248D128` FOREIGN KEY (`class_category_id1`) REFERENCES `dtb_class_category` (`id`),
  ADD CONSTRAINT `FK_1A11D1BA4584665A` FOREIGN KEY (`product_id`) REFERENCES `dtb_product` (`id`),
  ADD CONSTRAINT `FK_1A11D1BA61220EA6` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`id`),
  ADD CONSTRAINT `FK_1A11D1BA9B418092` FOREIGN KEY (`class_category_id2`) REFERENCES `dtb_class_category` (`id`),
  ADD CONSTRAINT `FK_1A11D1BAB0524E01` FOREIGN KEY (`sale_type_id`) REFERENCES `mtb_sale_type` (`id`),
  ADD CONSTRAINT `FK_1A11D1BABA4269E` FOREIGN KEY (`delivery_duration_id`) REFERENCES `dtb_delivery_duration` (`id`);

--
-- Constraints for table `dtb_product_image`
--
ALTER TABLE `dtb_product_image`
  ADD CONSTRAINT `FK_3267CC7A4584665A` FOREIGN KEY (`product_id`) REFERENCES `dtb_product` (`id`),
  ADD CONSTRAINT `FK_3267CC7A61220EA6` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`id`);

--
-- Constraints for table `dtb_product_stock`
--
ALTER TABLE `dtb_product_stock`
  ADD CONSTRAINT `FK_BC6C9E4521B06187` FOREIGN KEY (`product_class_id`) REFERENCES `dtb_product_class` (`id`),
  ADD CONSTRAINT `FK_BC6C9E4561220EA6` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`id`);

--
-- Constraints for table `dtb_product_tag`
--
ALTER TABLE `dtb_product_tag`
  ADD CONSTRAINT `FK_4433E7214584665A` FOREIGN KEY (`product_id`) REFERENCES `dtb_product` (`id`),
  ADD CONSTRAINT `FK_4433E72161220EA6` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`id`),
  ADD CONSTRAINT `FK_4433E721BAD26311` FOREIGN KEY (`tag_id`) REFERENCES `dtb_tag` (`id`);

--
-- Constraints for table `dtb_shipping`
--
ALTER TABLE `dtb_shipping`
  ADD CONSTRAINT `FK_2EBD22CE12136921` FOREIGN KEY (`delivery_id`) REFERENCES `dtb_delivery` (`id`),
  ADD CONSTRAINT `FK_2EBD22CE61220EA6` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`id`),
  ADD CONSTRAINT `FK_2EBD22CE8D9F6D38` FOREIGN KEY (`order_id`) REFERENCES `dtb_order` (`id`),
  ADD CONSTRAINT `FK_2EBD22CEE171EF5F` FOREIGN KEY (`pref_id`) REFERENCES `mtb_pref` (`id`),
  ADD CONSTRAINT `FK_2EBD22CEF92F3E70` FOREIGN KEY (`country_id`) REFERENCES `mtb_country` (`id`);

--
-- Constraints for table `dtb_tax_rule`
--
ALTER TABLE `dtb_tax_rule`
  ADD CONSTRAINT `FK_59F696DE1BD5C574` FOREIGN KEY (`rounding_type_id`) REFERENCES `mtb_rounding_type` (`id`),
  ADD CONSTRAINT `FK_59F696DE21B06187` FOREIGN KEY (`product_class_id`) REFERENCES `dtb_product_class` (`id`),
  ADD CONSTRAINT `FK_59F696DE4584665A` FOREIGN KEY (`product_id`) REFERENCES `dtb_product` (`id`),
  ADD CONSTRAINT `FK_59F696DE61220EA6` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`id`),
  ADD CONSTRAINT `FK_59F696DEE171EF5F` FOREIGN KEY (`pref_id`) REFERENCES `mtb_pref` (`id`),
  ADD CONSTRAINT `FK_59F696DEF92F3E70` FOREIGN KEY (`country_id`) REFERENCES `mtb_country` (`id`);

--
-- Constraints for table `dtb_template`
--
ALTER TABLE `dtb_template`
  ADD CONSTRAINT `FK_94C12A694FFA550E` FOREIGN KEY (`device_type_id`) REFERENCES `mtb_device_type` (`id`);

--
-- Constraints for table `plg_ccp_config`
--
ALTER TABLE `plg_ccp_config`
  ADD CONSTRAINT `FK_5BAA81411BD5C574` FOREIGN KEY (`rounding_type_id`) REFERENCES `mtb_rounding_type` (`id`);

--
-- Constraints for table `plg_ccp_customer_class_price`
--
ALTER TABLE `plg_ccp_customer_class_price`
  ADD CONSTRAINT `FK_26A264FB21B06187` FOREIGN KEY (`product_class_id`) REFERENCES `dtb_product_class` (`id`),
  ADD CONSTRAINT `FK_26A264FBC6C5483F` FOREIGN KEY (`customer_class_id`) REFERENCES `plg_ccp_customer_class` (`id`);

--
-- Constraints for table `plg_contact_management4_contact`
--
ALTER TABLE `plg_contact_management4_contact`
  ADD CONSTRAINT `FK_53DDB4B4584665A` FOREIGN KEY (`product_id`) REFERENCES `dtb_product` (`id`),
  ADD CONSTRAINT `FK_53DDB4B50623C6` FOREIGN KEY (`contact_status_id`) REFERENCES `plg_contact_management4_contact_status` (`id`),
  ADD CONSTRAINT `FK_53DDB4B9395C3F3` FOREIGN KEY (`customer_id`) REFERENCES `dtb_customer` (`id`),
  ADD CONSTRAINT `FK_53DDB4BD4D57CD` FOREIGN KEY (`staff_id`) REFERENCES `dtb_member` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `FK_53DDB4BE171EF5F` FOREIGN KEY (`pref_id`) REFERENCES `mtb_pref` (`id`);

--
-- Constraints for table `plg_coupon_detail`
--
ALTER TABLE `plg_coupon_detail`
  ADD CONSTRAINT `FK_7B9D14112469DE2` FOREIGN KEY (`category_id`) REFERENCES `dtb_category` (`id`),
  ADD CONSTRAINT `FK_7B9D1414584665A` FOREIGN KEY (`product_id`) REFERENCES `dtb_product` (`id`),
  ADD CONSTRAINT `FK_7B9D14166C5951B` FOREIGN KEY (`coupon_id`) REFERENCES `plg_coupon` (`coupon_id`);

--
-- Constraints for table `plg_mailmaga_send_history`
--
ALTER TABLE `plg_mailmaga_send_history`
  ADD CONSTRAINT `FK_424AD01261220EA6` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`id`);

--
-- Constraints for table `plg_paypal_subscribing_customer`
--
ALTER TABLE `plg_paypal_subscribing_customer`
  ADD CONSTRAINT `FK_7E353C621B06187` FOREIGN KEY (`product_class_id`) REFERENCES `dtb_product_class` (`id`),
  ADD CONSTRAINT `FK_7E353C676500E2D` FOREIGN KEY (`reference_transaction_id`) REFERENCES `plg_paypal_transaction` (`id`),
  ADD CONSTRAINT `FK_7E353C69395C3F3` FOREIGN KEY (`customer_id`) REFERENCES `dtb_customer` (`id`);

--
-- Constraints for table `plg_paypal_transaction`
--
ALTER TABLE `plg_paypal_transaction`
  ADD CONSTRAINT `FK_2F162BF78D9F6D38` FOREIGN KEY (`order_id`) REFERENCES `dtb_order` (`id`);

--
-- Constraints for table `plg_product_class_reserve4_extra`
--
ALTER TABLE `plg_product_class_reserve4_extra`
  ADD CONSTRAINT `FK_CF38906921B06187` FOREIGN KEY (`product_class_id`) REFERENCES `dtb_product_class` (`id`),
  ADD CONSTRAINT `FK_CF389069248D128` FOREIGN KEY (`class_category_id1`) REFERENCES `dtb_class_category` (`id`),
  ADD CONSTRAINT `FK_CF3890694584665A` FOREIGN KEY (`product_id`) REFERENCES `dtb_product` (`id`),
  ADD CONSTRAINT `FK_CF3890699B418092` FOREIGN KEY (`class_category_id2`) REFERENCES `dtb_class_category` (`id`);

--
-- Constraints for table `plg_product_reserve4_order`
--
ALTER TABLE `plg_product_reserve4_order`
  ADD CONSTRAINT `FK_322E96EE4584665A` FOREIGN KEY (`product_id`) REFERENCES `dtb_product` (`id`),
  ADD CONSTRAINT `FK_322E96EE8D9F6D38` FOREIGN KEY (`order_id`) REFERENCES `dtb_order` (`id`);

--
-- Constraints for table `plg_product_review`
--
ALTER TABLE `plg_product_review`
  ADD CONSTRAINT `FK_9CA38FA24584665A` FOREIGN KEY (`product_id`) REFERENCES `dtb_product` (`id`),
  ADD CONSTRAINT `FK_9CA38FA25A2DB2A0` FOREIGN KEY (`sex_id`) REFERENCES `mtb_sex` (`id`),
  ADD CONSTRAINT `FK_9CA38FA26BF700BD` FOREIGN KEY (`status_id`) REFERENCES `plg_product_review_status` (`id`),
  ADD CONSTRAINT `FK_9CA38FA29395C3F3` FOREIGN KEY (`customer_id`) REFERENCES `dtb_customer` (`id`);

--
-- Constraints for table `plg_product_review_config`
--
ALTER TABLE `plg_product_review_config`
  ADD CONSTRAINT `FK_D27A17EFE8507796` FOREIGN KEY (`csv_type_id`) REFERENCES `mtb_csv_type` (`id`);

--
-- Constraints for table `plg_taba_cms_category`
--
ALTER TABLE `plg_taba_cms_category`
  ADD CONSTRAINT `FK_946A754FC54C8C93` FOREIGN KEY (`type_id`) REFERENCES `plg_taba_cms_type` (`type_id`);

--
-- Constraints for table `plg_taba_cms_post`
--
ALTER TABLE `plg_taba_cms_post`
  ADD CONSTRAINT `FK_316AB2EF12469DE2` FOREIGN KEY (`category_id`) REFERENCES `plg_taba_cms_category` (`category_id`),
  ADD CONSTRAINT `FK_316AB2EF61220EA6` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`id`),
  ADD CONSTRAINT `FK_316AB2EFC54C8C93` FOREIGN KEY (`type_id`) REFERENCES `plg_taba_cms_type` (`type_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
