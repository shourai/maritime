<?php

namespace Customize\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Eccube\Common\EccubeConfig;
use Eccube\Form\Type\NameType;
use Eccube\Form\Type\AddressType;
use Eccube\Form\Type\PostalType;
use Eccube\Form\Type\PhoneNumberType;
use Eccube\Form\Validator\Email;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Validator\Constraints as Assert;

class PartnerType extends AbstractType {
  protected $eccubeConfig;

  public function __construct(EccubeConfig $eccubeConfig) {
    $this->eccubeConfig = $eccubeConfig;
  }

  public function buildForm(FormBuilderInterface $builder, array $options) {
    $builder
      ->add('name', NameType::class, [
        'required' => true,
      ])
      ->add('company', TextType::class, [
        'constraints' => [
          new Assert\NotBlank(),
        ],
      ])
      ->add('postal_code', PostalType::class, [
        'required' => true,
      ])
      ->add('address', AddressType::class, [
        'required' => true,
      ])
      ->add('phone_number', PhoneNumberType::class, [
        'required' => true,
      ])
      ->add('email', EmailType::class, [
        'constraints' => [
          new Assert\NotBlank(),
          new Email(['strict' => $this->eccubeConfig['eccube_rfc_email_check']]),
        ],
      ])
      ->add('contents', TextareaType::class, [
        'constraints' => [
          new Assert\NotBlank(),
        ],
      ]);
  }
}
