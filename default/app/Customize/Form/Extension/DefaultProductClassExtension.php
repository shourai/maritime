<?php

namespace Customize\Form\Extension;

use Eccube\Form\Type\AddCartType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Exception\InvalidArgumentException;

class DefaultProductClassExtension extends AbstractTypeExtension {
  public function buildForm(FormBuilderInterface $builder, array $options) {
    try {
      $options = $builder->get('classcategory_id1')->getOptions();

      $options['data'] = '3';

      $builder->add('classcategory_id1', ChoiceType::class, $options);
    } catch (InvalidArgumentException $e) {
      return; // pass
    }
  }

  public function getExtendedType() {
    return AddCartType::class;
  }
}
