<?php

namespace Customize\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManagerInterface;

use Plugin\CheckKagoochi\Entity\CheckKagoochiSendedMail;
use Plugin\CheckKagoochi\Service\CheckKagoochiMailService;
use Plugin\CheckKagoochi\Repository\CheckKagoochiRepository;
use Eccube\Repository\ShippingRepository;
use Eccube\Repository\CartRepository;
use Eccube\Repository\BaseInfoRepository;

class KagoochiCommand extends ContainerAwareCommand {
  protected static $defaultName = 'maritime:kagoochi';

  private $orderRepository; 
  private $entityManager;
  private $shippingRepository;
  private $mailService;
  private $BaseInfo;
  private $twig;
  private $cartRepository;

  public function __construct(CheckKagoochiRepository $orderRepository, EntityManagerInterface $entityManager, ShippingRepository $shippingRepository, CheckKagoochiMailService $mailService, BaseInfoRepository $baseInfoRepository, \Twig_Environment $twig, CartRepository $cartRepository) {
    parent::__construct();

    $this->orderRepository = $orderRepository;
    $this->entityManager = $entityManager;
    $this->shippingRepository = $shippingRepository;
    $this->mailService = $mailService;
    $this->BaseInfo = $baseInfoRepository->get();
    $this->twig = $twig;
    $this->cartRepository = $cartRepository;
  }

  protected function execute(InputInterface $input, OutputInterface $output) {
    $router = $this->getContainer()->get('router');
    $context = $router->getContext();
    $context->setHost(env('ECCUBE_HOST'));
    $context->setScheme('https');
    $this->twig->addGlobal('BaseInfo', $this->BaseInfo);
    $this->twig->addGlobal('app', ['request' => ['locale' => 'ja']]);
    $end = new \DateTime();
    $end->add(\DateInterval::createFromDateString('yesterday'));
    $orders = $this->orderRepository->getKagoochiQuerySearchDataForAdmin([
      'update_date_start' => '',
      'update_date_end' => $end,
      'kagoochi_mail' => ['1'],
    ], $this->entityManager);

    foreach ($orders as $order) {
      $shipping = $this->shippingRepository->find($order['shipping_id']);
      if (!$shipping) {
        $cart = $this->cartRepository->find($order['cart_id']);
        $this->mailService->sendKagoochiNotifyMail2($cart);

        // 「送信済み」
        if (!is_null($cart->getCustomer())) {
            $cust_id = $cart->getCustomer()->getId();
        } else {
            $cust_id = 0;
        }
        $SendedMail = new CheckKagoochiSendedMail();
        $SendedMail->setTargetKbn(CheckKagoochiSendedMail::CHECKKAGOOCHI_TARGET_CART)
            ->setTargetId($cart->getId())
            ->setCustomerId($cust_id)
           ->setSendDate(new \DateTime());
        $this->entityManager->persist($SendedMail);
        $this->entityManager->flush();    
      } else {
        $this->mailService->sendKagoochiNotifyMail($shipping);
        if (!is_null($shipping->getOrder()->getCustomer())) {
            $cust_id = $shipping->getOrder()->getCustomer()->getId();
        } else {
            $cust_id = 0;
        }
        $SendedMail = new CheckKagoochiSendedMail();
        $SendedMail->setTargetKbn(CheckKagoochiSendedMail::CHECKKAGOOCHI_TARGET_ORDER)
            ->setTargetId($shipping->getId())
            ->setCustomerId($cust_id)
            ->setSendDate(new \DateTime());
        $this->entityManager->persist($SendedMail);
        $this->entityManager->flush();
      }
    }
  }
}
