<?php

namespace Customize\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Eccube\Entity\MailTemplate;
use Eccube\Repository\MailTemplateRepository;
use Doctrine\ORM\EntityManagerInterface;
use Eccube\Repository\BaseInfoRepository;
use Eccube\Entity\Order;

use Eccube\Repository\OrderRepository;

class ReviewReminderCommand extends ContainerAwareCommand {
  protected static $defaultName = 'maritime:review:remind';

  const ReviewReminder_mailtemplate_name = 'レビューメール';
  const ReviewReminder_mailtemplate_filename = 'Mail/review_reminder.twig';
  const ReviewReminder_mailtemplate_subject = 'いつもご利用ありがとうございます';    //default at install

  private $orderRepository; 
  private $BaseInfo;
  private $twig;
  private $mailer;
  private $mailTemplateRepository;

  public function __construct(OrderRepository $orderRepository, BaseInfoRepository $baseInfoRepository, \Twig_Environment $twig, \Swift_Mailer $mailer, MailTemplateRepository $mailTemplateRepository) {
    parent::__construct();

    $this->orderRepository = $orderRepository;
    $this->BaseInfo = $baseInfoRepository->get();
    $this->twig = $twig;
    $this->mailer = $mailer;
    $this->mailTemplateRepository = $mailTemplateRepository;
  }

  protected function execute(InputInterface $input, OutputInterface $output) {
    $router = $this->getContainer()->get('router');
    $context = $router->getContext();
    $context->setHost(env('ECCUBE_HOST'));
    $context->setScheme('https');
    $this->twig->addGlobal('BaseInfo', $this->BaseInfo);
    $this->twig->addGlobal('app', ['request' => ['locale' => 'ja']]);
    $end = new \DateTime();
    $end->add(\DateInterval::createFromDateString('last week'));
    $MailTemplate = $this->mailTemplateRepository->findOneBy(['name'=>self::ReviewReminder_mailtemplate_name]);
    $orders = $this->orderRepository->getQueryBuilderBySearchDataForAdmin([
      'order_date_start' => $end,
      'order_date_end' => $end,
      'status' => ['5'], // 発送済み / sent
    ])->getQuery()->getResult();
    foreach ($orders as $Order) {
      $body = $this->getKagoochiNotifyMailBody($Order, $MailTemplate->getFileName(), true);
      $message = (new \Swift_Message())
        ->setSubject('['.$this->BaseInfo->getShopName().'] '.$MailTemplate->getMailSubject())
        ->setFrom([$this->BaseInfo->getEmail01() => $this->BaseInfo->getShopName()])
        ->setTo($Order->getEmail())
        ->setBcc($this->BaseInfo->getEmail01())
        ->setReplyTo($this->BaseInfo->getEmail03())
        ->setReturnPath($this->BaseInfo->getEmail04());

      // HTMLテンプレートが存在する場合
      $htmlFileName = $this->getHtmlTemplate($MailTemplate->getFileName());
      if (!is_null($htmlFileName)) {
        $htmlBody = $this->getKagoochiNotifyMailBody($Order, $htmlFileName, true);

        $message
          ->setContentType('text/plain; charset=UTF-8')
          ->setBody($body, 'text/plain')
          ->addPart($htmlBody, 'text/html');
      } else {
        $message->setBody($body);
      }

      $this->mailer->send($message);
    }
  }

  public function getKagoochiNotifyMailBody(Order $Order, $templateName = null, $is_html = false) {
    if (is_null($templateName)) {
      /** @var MailTemplate $MailTemplate */
      $MailTemplate = $this->mailTemplateRepository->findOneBy(['name'=>self::ReviewReminder_mailtemplate_name]);
      $fileName = $MailTemplate->getFileName();
    } else {
      $fileName = $templateName;
    }

    if ($is_html) {
      $htmlFileName = $this->getHtmlTemplate($fileName);
      $fileName = !is_null($htmlFileName) ? $htmlFileName : $fileName;
    }

    return $this->twig->render($fileName, [
      'Shipping' => ['productOrderItems' => []],
      'Order' => $Order,
    ]);
  }

  public function getHtmlTemplate($templateName) {
    // メールテンプレート名からHTMLメール用テンプレート名を生成
    $fileName = explode('.', $templateName);
    $suffix = '.html';
    $htmlFileName = $fileName[0].$suffix.'.'.$fileName[1];

    // HTMLメール用テンプレートの存在チェック
    if ($this->twig->getLoader()->exists($htmlFileName)) {
      return $htmlFileName;
    } else {
      return null;
    }
  }
}
