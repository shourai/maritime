<?php

namespace Customize\Repository;

use Eccube\Repository\AbstractRepository;
use Eccube\Entity\Product;
use Symfony\Bridge\Doctrine\RegistryInterface;

class CategoryProductRepository extends AbstractRepository {
  public function __construct(RegistryInterface $registry) {
    parent::__construct($registry, Product::class);
  }

  public function getCategoryProductList($category_id = 1) {
    $categoryId = $category_id;

    $qb = $this->createQueryBuilder('p')
               ->andWhere('p.Status = 1')
               ->innerJoin('p.ProductCategories', 'pct')
               ->innerJoin('pct.Category', 'c')
               ->andWhere('pct.Category = :Category')
               ->setParameter('Category', $categoryId)
               ->orderBy('p.note', 'ASC')
               ->addOrderBy('p.id', 'DESC')
               ->setMaxResults(20);


    $products = $qb->getQuery()
                   ->useResultCache(true, $this->eccubeConfig['eccube_result_cache_lifetime_short'])
                   ->getResult();

    return $products;
  }
}
