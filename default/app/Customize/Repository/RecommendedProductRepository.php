<?php

namespace Customize\Repository;

use Eccube\Repository\AbstractRepository;
use Eccube\Entity\Product;
use Eccube\Repository\ProductRepository;
use Eccube\Form\Type\AddCartType;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Form\FormFactory;

class RecommendedProductRepository extends AbstractRepository {
  public function __construct(RegistryInterface $registry, FormFactory $formFactory, ProductRepository $productRepository) {
    parent::__construct($registry, Product::class);
    $this->formFactory = $formFactory;
    $this->productRepository = $productRepository;
  }

  public function getRecommendedProducts(Product $product) {
    // Get highest categorical hierarchy for product
    $highest = null; // And by highest, I mean lowest in number
    $level = 999;

    foreach ($product->getProductCategories() as $category) {
      $category = $category->getCategory();
      if ($level > $category->getHierarchy() && $category->getHierarchy() > 1) {
        // We want to get the category that is lower than the top hierarchy
        $highest = $category;
        $level = $category->getHierarchy();
      }
    }

    if (!$highest) {
      return array();
    }

    $qb = $this->createQueryBuilder('p')
               ->andWhere('p.Status = 1')
               ->innerJoin('p.ProductCategories', 'pct')
               ->innerJoin('pct.Category', 'c')
               ->andWhere('pct.Category = :Category')
               ->andWhere('p <> :Product')
               ->setParameter('Category', $highest)
               ->setParameter('Product', $product)
               ->orderBy('p.create_date', 'DESC')
               ->addOrderBy('p.id', 'DESC')
               ->setMaxResults(20);

    $products = $qb->getQuery()
                   ->useResultCache(true, $this->eccubeConfig['eccube_result_cache_lifetime_short'])
                   ->getResult();

    $ids = [];
    foreach ($products as $Product) {
        $ids[] = $Product->getId();
    }
    $ProductsAndClassCategories = $this->productRepository->findProductsWithSortedClassCategories($ids, 'p.id');

    $forms = [];
    foreach ($products as $Product) {
        /* @var $builder \Symfony\Component\Form\FormBuilderInterface */
        $builder = $this->formFactory->createNamedBuilder(
            '',
            AddCartType::class,
            null,
            [
                'product' => $ProductsAndClassCategories[$Product->getId()],
                'allow_extra_fields' => true,
            ]
        );
        $addCartForm = $builder->getForm();

        $forms[$Product->getId()] = $addCartForm->createView();

    }

    return [
      'products' => $products,
      'forms' => $forms,
    ];
  }
}
