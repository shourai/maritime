<?php

namespace Customize\Controller;

use Eccube\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Plugin\ProductReview4\Repository\ProductReviewRepository;
use Plugin\ProductReview4\Entity\ProductReviewStatus;

use Google\Cloud\Translate\V3\TranslationServiceClient;

class ReviewListController extends AbstractController {
  protected $productReviewRepository;
  protected $translateClient;

  public function __construct(ProductReviewRepository $productReviewRepository) {
    putenv('GOOGLE_APPLICATION_CREDENTIALS='.env('CLOUD_TRANSLATE_KEY'));
    $this->translateClient = new TranslationServiceClient([
      'keyFilePath' => env('CLOUD_TRANSLATE_KEY'),
      'projectId' => env('CLOUD_TRANSLATE_PROJECT_ID'),
    ]);
    $this->productReviewRepository = $productReviewRepository;
  }

  /**
   * @Method("GET")
   * @Route("/reviews", name="product_review_list")
   */
  public function product_review_list(Request $request) {
    $ProductReviews = $this->productReviewRepository->findBy(['Status' => ProductReviewStatus::SHOW], ['Product' => 'DESC']);

    $GroupedProductReviews = [];
    foreach ($ProductReviews as $Review) {
      if (!isset($GroupedProductReviews[$Review->getProduct()->getId()])) {
        $rate = $this->productReviewRepository->getAvgAll($Review->getProduct());
        $avg = round($rate['recommend_avg']);
        $count = intval($rate['review_count']);
        $GroupedProductReviews[$Review->getProduct()->getId()] = [
          'Product' => $Review->getProduct(),
          'Reviews' => [],
          'Avg' => $avg,
          'Count' => $count,
        ];
      }
      $GroupedProductReviews[$Review->getProduct()->getId()]['Reviews'][] = $Review;
    }

    return $this->render('review_list.twig', [
      'ProductReviews' => $GroupedProductReviews,
    ]);
  }

  /**
   * @Method("GET")
   * @Route("/reviews/translate", name="product_review_list_translate")
   */
  public function product_review_list_translate(Request $request) {
    $ProductReviews = $this->productReviewRepository->findBy(['Status' => ProductReviewStatus::SHOW], ['Product' => 'DESC']);

    $GroupedProductReviews = [];
    foreach ($ProductReviews as $Review) {
      if (!isset($GroupedProductReviews[$Review->getProduct()->getId()])) {
        $rate = $this->productReviewRepository->getAvgAll($Review->getProduct());
        $avg = round($rate['recommend_avg']);
        $count = intval($rate['review_count']);
        $GroupedProductReviews[$Review->getProduct()->getId()] = [
          'Product' => $Review->getProduct(),
          'Reviews' => [],
          'Avg' => $avg,
          'Count' => $count,
        ];
      }
      $GroupedProductReviews[$Review->getProduct()->getId()]['Reviews'][] = $Review;
    }

    foreach ($GroupedProductReviews as $ProductId => $ProductReviews) {
      $translateContents = [];
      foreach ($ProductReviews['Reviews'] as $review) {
        $translateContents[] = $review->getTitle();
        $translateContents[] = $review->getComment();
        $translateContents[] = $review->getReviewerName();
      }
      $formattedParent = $this->translateClient->locationName(env('CLOUD_TRANSLATE_PROJECT_ID'), 'global');
      try {
        $response = $this->translateClient->translateText(
          $translateContents,
          env('ECCUBE_LOCALE', 'ja'),
          $formattedParent,
          [
            'mimeType' => 'text/plain'
          ]
        );
        foreach ($response->getTranslations() as $i => $translation) {
          if ($i % 3 == 0) {
            $ProductReviews['Reviews'][floor($i / 3)]->setTitle($translation->getTranslatedText());
          } elseif ($i % 3 == 1) {
            $ProductReviews['Reviews'][floor($i / 3)]->setComment($translation->getTranslatedText());
          } elseif ($i % 3 == 2) {
            $ProductReviews['Reviews'][floor($i / 3)]->setReviewerName($translation->getTranslatedText());
          }
        }
        $GroupedProductReviews[$ProductId] = $ProductReviews;
      } finally {
        // If failed to translate, then skip
      }
    }

    return $this->render('review_list.twig', [
      'ProductReviews' => $GroupedProductReviews,
    ]);
  }
}
