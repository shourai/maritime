<?php

namespace Customize\Controller;

use Eccube\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Eccube\Entity\Product;
use Symfony\Component\HttpFoundation\Request;
use Plugin\ProductReview4\Entity\ProductReviewStatus;
use Plugin\ProductReview4\Repository\ProductReviewRepository;
use Plugin\ProductReview4\Repository\ProductReviewConfigRepository;

use Google\Cloud\Translate\V3\TranslationServiceClient;

class ReviewTranslateController extends AbstractController {
  private $translateClient;
  private $productReviewRepository;
  private $productReviewConfigRepository;

  public function __construct(ProductReviewRepository $productReviewRepository, ProductReviewConfigRepository $productReviewConfigRepository) {
    putenv('GOOGLE_APPLICATION_CREDENTIALS='.env('CLOUD_TRANSLATE_KEY'));
    $this->translateClient = new TranslationServiceClient([
      'keyFilePath' => env('CLOUD_TRANSLATE_KEY'),
      'projectId' => env('CLOUD_TRANSLATE_PROJECT_ID'),
    ]);
    $this->productReviewRepository = $productReviewRepository;
    $this->productReviewConfigRepository = $productReviewConfigRepository;
  }

  /**
   * @Method("GET")
   * @Route("/product_review/{id}/translate", name="product_review_translate", requirements={"id" = "\d+"})
   */
  public function product_review_translate(Request $request, Product $Product) {
    $Config = $this->productReviewConfigRepository->get();
    $ProductReviews = $this->productReviewRepository->findBy(['Status' => ProductReviewStatus::SHOW, 'Product' => $Product], ['id' => 'DESC'], $Config->getReviewMax());

    $rate = $this->productReviewRepository->getAvgAll($Product);
    $avg = round($rate['recommend_avg']);
    $count = intval($rate['review_count']);

    $translateContents = [];
    foreach ($ProductReviews as $i => $review) {
      $translateContents[] = $review->getTitle();
      $translateContents[] = $review->getComment();
      $translateContents[] = $review->getReviewerName();
    }
    $formattedParent = $this->translateClient->locationName(env('CLOUD_TRANSLATE_PROJECT_ID'), 'global');
    try {
      $response = $this->translateClient->translateText(
        $translateContents,
        env('ECCUBE_LOCALE', 'ja'),
        $formattedParent,
        [
          'mimeType' => 'text/plain'
        ]
      );
      foreach ($response->getTranslations() as $i => $translation) {
        if ($i % 3 == 0) {
          $ProductReviews[floor($i / 3)]->setTitle($translation->getTranslatedText());
        } elseif ($i % 3 == 1) {
          $ProductReviews[floor($i / 3)]->setComment($translation->getTranslatedText());
        } elseif ($i % 3 == 2) {
          $ProductReviews[floor($i / 3)]->setReviewerName($translation->getTranslatedText());
        }
      }
    } finally {
      // If failed to translate, then skip
    }

    $parameters = [];
    $parameters['Product'] = $Product;
    $parameters['ProductReviews'] = $ProductReviews;
    $parameters['ProductReviewAvg'] = $avg;
    $parameters['ProductReviewCount'] = $count;
    $parameters['translated'] = true;

    return $this->render('@ProductReview4/default/review.twig', $parameters);
  }
}
