<?php

namespace Customize\Controller;

use Eccube\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Eccube\Event\EccubeEvents;
use Eccube\Event\EventArgs;
use Symfony\Component\HttpFoundation\Request;
use Eccube\Repository\MailTemplateRepository;
use Eccube\Repository\BaseInfoRepository;

use Customize\Form\Type\PartnerType;

class PartnerController extends AbstractController {
  protected $mailTemplateRepository;
  protected $mailer;
  protected $BaseInfo;
  protected $twig;

  public function __construct(MailTemplateRepository $mailTemplateRepository, \Swift_Mailer $mailer, BaseInfoRepository $baseInfoRepository, \Twig_Environment $twig) {
    $this->mailTemplateRepository = $mailTemplateRepository;
    $this->mailer = $mailer;
    $this->BaseInfo = $baseInfoRepository->get();
    $this->twig = $twig;
  }

  /**
   * @Route("/partner_entry", name="partner_entry")
   * @Template("Partner/index.twig")
   */
  public function partner_entry(Request $request) {
    $builder = $this->formFactory->createBuilder(PartnerType::class);

    if ($this->isGranted('ROLE_USER')) {
      /** @var Customer $user */
      $user = $this->getUser();
      $builder->setData(
        [
          'name01' => $user->getName01(),
          'name02' => $user->getName02(),
          'postal_code' => $user->getPostalCode(),
          'pref' => $user->getPref(),
          'addr01' => $user->getAddr01(),
          'addr02' => $user->getAddr02(),
          'phone_number' => $user->getPhoneNumber(),
          'email' => $user->getEmail(),
        ]
      );
    }

    // FRONT_CONTACT_INDEX_INITIALIZE
    $event = new EventArgs([
        'builder' => $builder,
      ],
      $request
    );
    // $this->eventDispatcher->dispatch(EccubeEvents::FRONT_CONTACT_INDEX_INITIALIZE, $event);

    $form = $builder->getForm();
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      switch ($request->get('mode')) {
      case 'confirm':
        $form = $builder->getForm();
        $form->handleRequest($request);

        return $this->render('Partner/confirm.twig', [
          'form' => $form->createView(),
        ]);

      case 'complete':

        $data = $form->getData();

        $event = new EventArgs([
            'form' => $form,
            'data' => $data,
          ],
          $request
        );
        // $this->eventDispatcher->dispatch(EccubeEvents::FRONT_CONTACT_INDEX_COMPLETE, $event);

        $data = $event->getArgument('data');

        // メール送信
        $MailTemplate = $this->mailTemplateRepository->findOneBy(['name' => '代理店問合受付メール']);

        $body = $this->twig->render($MailTemplate->getFileName(), [
            'data' => $data,
            'BaseInfo' => $this->BaseInfo,
        ]);

        // 問い合わせ者にメール送信
        // TODO: Create Separate English Template
        $message = (new \Swift_Message())
            ->setSubject('['.$this->BaseInfo->getShopName().'] '. ( (env('ECCUBE_LOCALE') == 'ja') ? $MailTemplate->getMailSubject() : 'We have received your partnership inquiry.' ) )
            ->setFrom([$this->BaseInfo->getEmail02() => $this->BaseInfo->getShopName()])
            ->setTo([$data['email']])
            ->setBcc('info@medyfine.co.jp') //  $this->BaseInfo->getEmail02() For dev site
            ->setReplyTo('info@medyfine.co.jp')
            ->setReturnPath($this->BaseInfo->getEmail04());

        // HTMLテンプレートが存在する場合
        $htmlFileName = $this->getHtmlTemplate($MailTemplate->getFileName());
        if (!is_null($htmlFileName)) {
            $htmlBody = $this->twig->render($htmlFileName, [
                'data' => $data,
                'BaseInfo' => $this->BaseInfo,
            ]);

            $message
                ->setContentType('text/plain; charset=UTF-8')
                ->setBody($body, 'text/plain')
                ->addPart($htmlBody, 'text/html');
        } else {
            $message->setBody($body);
        }

        $count = $this->mailer->send($message);

        return $this->redirect($this->generateUrl('partner_complete'));
      }
    }

    return [
      'form' => $form->createView(),
    ];
  }

  /**
   * お問い合わせ完了画面.
   *
   * @Route("/partner_complete", name="partner_complete")
   * @Template("Partner/complete.twig")
   */
  public function complete() {
    return [];
  }

  public function getHtmlTemplate($templateName) {
    // メールテンプレート名からHTMLメール用テンプレート名を生成
    $fileName = explode('.', $templateName);
    $suffix = '.html';
    $htmlFileName = $fileName[0].$suffix.'.'.$fileName[1];

    // HTMLメール用テンプレートの存在チェック
    if ($this->twig->getLoader()->exists($htmlFileName)) {
        return $htmlFileName;
    } else {
        return null;
    }
  }
}
