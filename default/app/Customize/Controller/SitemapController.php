<?php

namespace Customize\Controller;

use Eccube\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Eccube\Repository\ProductRepository;
use Eccube\Repository\PageRepository;
use Plugin\TabaCMS\Repository\PostRepository;

class SitemapController extends AbstractController {
  protected $productRepository;
  protected $pageRepository;
  protected $postRepository;

  public function __construct(ProductRepository $productRepository, PageRepository $pageRepository, PostRepository $postRepository) {
    $this->productRepository = $productRepository;
    $this->pageRepository = $pageRepository;
    $this->postRepository = $postRepository;
  }

  /**
   * @Route("/sitemap.xml", name="sitemap", defaults={"_format"="xml"})
   */
  public function sitemapxml(Request $request, RouterInterface $router) {
    $collection = $router->getRouteCollection();
    $allRoutes = $collection->all();
    $hostname = $request->getSchemeAndHttpHost();

    $routes = [];
    $urls_added = [];
    $addedCmsArticles = false;
    foreach ($allRoutes as $route => $params) {
      $path = $params->getPath();
      if (stristr($path, '_profiler') !== false) continue;
      if (stristr($path, '_error') !== false) continue;
      if (stristr($path, '_wdt') !== false) continue;
      if (stristr($path, 'admin_maritime') !== false) continue;
      if (stristr($path, 'block') !== false) continue;
      if (stristr($path, 'install') !== false) continue;
      if (stristr($path, 'complete') !== false) continue;
      if (stristr($path, 'activate') !== false) continue;
      if (stristr($path, 'reset') !== false) continue;
      if (stristr($path, 'cart') !== false) continue;
      if (stristr($path, 'mypage') !== false) continue;
      if (stristr($path, 'logout') !== false) continue;
      if (stristr($path, 'payment') !== false) continue;
      if (stristr($path, 'paypal') !== false) continue;
      if (stristr($path, 'shopping') !== false) continue;
      if (stristr($path, 'tabacms') !== false) continue;
      if (stristr($path, 'add_favorite') !== false) continue;
      if (stristr($path, 'product_review') !== false) continue;
      if (stristr($path, 'translate') !== false) continue;
      if (stristr($path, 'sitemap.xml') !== false) continue;
      if (sizeof($params->getMethods()) == 1 && strtolower($params->getMethods()[0]) == 'post') continue;
      if (sizeof($params->getMethods()) == 1 && strtolower($params->getMethods()[0]) == 'put') continue;
      if (stristr($path, '{') === false && stristr($path, '}') === false) {
        if (!in_array($path, $urls_added)) {
          $routes[] = [
            'url' => $hostname.$path,
            'locales' => [
              'en' => $hostname.'/en'.$path,
              // 'ko' => $hostname.'/ko'.$path,
            ],
          ];
          $urls_added[] = $path;
        }
      } else {
        if (stristr($path, '/products/') !== false) {
          // Expand to all products
          $qb = $this->productRepository->getQueryBuilderBySearchData(['orderby' => []]);
          $products = $qb->getQuery()->useResultCache(true, $this->eccubeConfig['eccube_result_cache_lifetime_short'])->getResult();

          foreach ($products as $product) {
            $url = str_replace('{id}', $product->getId(), $path);
            if (!in_array($url, $urls_added)) {
              $routes[] = [
                'url' => $hostname.$url,
                'locales' => [
                  'en' => $hostname.'/en'.$url,
                  // 'ko' => $hostname.'/ko'.$url,
                ],
              ];
              $urls_added[] = $url;
            }
          }
        } elseif (stristr($path, '/user_data/') !== false) {
          $pages = $this->pageRepository->getPageList();

          foreach ($pages as $page) {
            $url = str_replace('{route}', $page->getUrl(), $path);
            if ($router->getRouteCollection()->get($page->getUrl())) {
              $url = $router->getRouteCollection()->get($page->getUrl())->getPath();
              if (strpos($url, '{') !== false && strpos($url, '}') !== false) {
                continue;
              }
            }
            if (stristr($url, 'cart') !== false) continue;
            if (stristr($url, 'mypage') !== false) continue;
            if (stristr($url, 'complete') !== false) continue;
            if (stristr($url, 'shopping') !== false) continue;
            if (stristr($url, 'paypal') !== false) continue;
            if (stristr($url, 'translate') !== false) continue;
            if (!in_array($url, $urls_added)) {
              $routes[] = [
                'url' => $hostname.$url,
                'locales' => [
                  'en' => $hostname.'/en'.$url,
                  // 'ko' => $hostname.'/ko'.$url,
                ],
              ];
              $urls_added[] = $url;
            }
          }
        } elseif (stristr($path, '{data_key}') !== false) {
          if (!$addedCmsArticles) {
            $addedCmsArticles = true;
            $articles = $this->postRepository->getList();
            foreach ($articles as $article) {
              if (!in_array($article->getUrl(), $urls_added)) {
                $routes[] = [
                  'url' => $article->getUrl(),
                ];
                $urls_added[] = $article->getUrl();
              }
            }
          }
        }
      }
    }

    $response = new Response($this->renderView('sitemap.xml.twig', ['routes' => $routes, 'hostname' => $hostname]), 200);
    $response->headers->set('Content-Type', 'text/xml');
    return $response;
  }
}

