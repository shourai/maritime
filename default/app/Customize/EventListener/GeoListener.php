<?php

namespace Customize\EventListener;

use Eccube\Request\Context;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use GeoIp2\Database\Reader;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\HttpFoundation\Cookie;

class GeoListener implements EventSubscriberInterface {
  protected $requestContext;
  protected $adapter;

  public function __construct(Context $requestContext, AdapterInterface $adapter) {
    $this->requestContext = $requestContext;
    $this->adapter = $adapter;
  }

  public function onKernelRequest(GetResponseEvent $event) {
    $request = $event->getRequest();
    /* allow everyone to see the Japanese site (Temporarily)
    $country = $this->getCountryIp($request->getClientIp());
    if ($country == 'KR') {
      return $event->setResponse(new RedirectResponse('https://shourai.io'));
    } else {
    */
      $request_uri = $_SERVER['REQUEST_URI'];
      $request_uri = explode('?', $request_uri, 2)[0];
      $request_uri = explode('#', $request_uri, 2)[0];
      if (rtrim($request_uri, '/') != '' && !$request->query->has('hl')) {
        // If user is trying to view other pages or localized pages, skip this
        // whole business
        return;
      }

      $current_language = 'ja';
      $language = 'ja'; // set default language
      $parts = explode('/', $_SERVER['REQUEST_URI']);
      if (count($parts) > 2) {
        if ($parts[1] == 'en' || $parts[1] == 'zh' || $parts[1] == 'ko') {
          // Supported languages
          $current_language = $parts[1];
        }
      } else {
        // Default Japanese
        $current_language = 'ja';
      }

      $set_cookie = true;
      if ($request->query->has('hl')) {
        $lang = $request->query->get('hl');
        if ($lang == 'en' || $lang == 'ko' || $lang == 'zh' || $lang == 'ja') {
          $language = $lang;
        }
      } elseif ($request->cookies->has('hl')) {
        $language = $request->cookies->get('hl');
        $set_cookie = false;
      } elseif ($languages = $request->getLanguages()) {
        foreach ($languages as $lang) {
          $lang = explode('_', $lang, 2)[0];
          if ($lang == 'en' || $lang == 'ko' || $lang == 'zh' || $lang == 'ja') {
            $language = $lang;
            break;
          }
        }
      } else {
        // If no language set on browser, detect based on IP
        $country = $this->getCountryIp($request->getClientIp());
        if ($country == 'JP') {
          $language = 'ja';
        } else {
          $language = 'en';
        }
      }

      // Temporary hack
      if ($current_language == 'ko') $current_language = 'ja';
      if ($language == 'ko') $language = 'ja';

      // If we want to set cookie, send redirection anyway.
      if ($current_language != $language || $set_cookie) {
        $response = new RedirectResponse($language == 'ja' || $language == 'ko' ? '/' : '/'.$language.'/');
        if ($set_cookie) {
          $response->headers->setCookie(new Cookie('hl', $language, time() + 31536000, '/', '', false, false));
        }
        $event->setResponse($response);
      }
    /* } Allow Korean IP's access for now */
  }

  protected function getCountryIp($ip) {
    $item = $this->adapter->getItem('ip_country_'.$ip);
    if (!$item->isHit()) {
      $geoIpReader = new Reader(dirname(__FILE__).'/GeoLite2-Country.mmdb');
      try {
        $record = $geoIpReader->country($ip);
        $country = $record->country->isoCode;
      } catch (\Exception $e) {
        $country = 'JP';  // Default all to Japan if information not available
      }

      $item->set($country);
      $this->adapter->save($item);
    }
    $country = $item->get();
    return $country;
  }

  public static function getSubscribedEvents() {
    return [
      'kernel.request' => ['onKernelRequest', 99999],
    ];
  }
}
