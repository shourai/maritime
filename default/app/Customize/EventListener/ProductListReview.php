<?php

namespace Customize\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Eccube\Event\TemplateEvent;
use Plugin\ProductReview4\Repository\ProductReviewRepository;

class ProductListReview implements EventSubscriberInterface {
  protected $productReviewRepository;

  public function __construct(ProductReviewRepository $productReviewRepository) {
    $this->productReviewRepository = $productReviewRepository;
  }

  public static function getSubscribedEvents() {
    return [
      'Product/list.twig' => 'list',
    ];
  }

  public function list(TemplateEvent $event) {
    $pagination = $event->getParameter('pagination');
    $ProductAvgs = [];
    foreach ($pagination as $Product) {
      $rate = $this->productReviewRepository->getAvgAll($Product);
      $avg = round($rate['recommend_avg']);
      $count = intval($rate['review_count']);
      $ProductAvgs[$Product->getId()] = [
        'avg' => $avg,
        'count' => $count,
      ];
    }
    $parameters = $event->getParameters();
    $parameters['ProductReviewAvgs'] = $ProductAvgs;
    $event->setParameters($parameters);
  }
}
