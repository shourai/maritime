<?php

namespace Customize\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Eccube\Event\TemplateEvent;
use Eccube\Service\CartService;

class CartCountDown implements EventSubscriberInterface {
  protected $cartService;

  public function __construct(CartService $cartService) {
    $this->cartService = $cartService;
  }

  public static function getSubscribedEvents() {
    return [
      'Product/detail.twig' => 'detail',
    ];
  }

  public function detail(TemplateEvent $event) {
    $carts = $this->cartService->getCarts();
    $Product = $event->getParameter('Product');
    $productInCart = false;

    $productClasses = $Product->getProductClasses();
    foreach ($carts as $cart) {
      foreach ($cart->getCartItems() as $cartItem) {
        foreach ($productClasses as $productClass) {
          if ($productClass->getId() == $cartItem->getProductClass()->getId()) {
            $productInCart = true;
            break;
          }
        }
        if ($productInCart) break;
      }
      if ($productInCart) break;
    }
    if ($productInCart) {
      $minimumTime = null;
      foreach ($carts as $cart) {
        $createDate = $cart->getCreateDate();
        if ($minimumTime == null || $createDate < $minimumTime) {
          $minimumTime = $createDate;
        }
      }
      $parameters = $event->getParameters();
      $parameters['CartCreatedDate'] = $minimumTime;
      $event->setParameters($parameters);
    }
  }
}
