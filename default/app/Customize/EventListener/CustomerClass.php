<?php

namespace Customize\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Eccube\Event\EccubeEvents;
use Eccube\Event\EventArgs;
use Doctrine\ORM\EntityManagerInterface;

class CustomerClass implements EventSubscriberInterface {
  private $em;

  public function __construct(EntityManagerInterface $em) {
    $this->em = $em;
  }

  public function onEntryComplete(EventArgs $event) {
    // Set customer default class once registration is completed
    $event['Customer']->setPlgCcpCustomerClass($this->em->find("Plugin\CustomerClassPrice4\Entity\CustomerClass", 1));
    $this->em->persist($event['Customer']);
    $this->em->flush($event['Customer']);
  }

  public static function getSubscribedEvents() {
    return [
      EccubeEvents::FRONT_ENTRY_INDEX_COMPLETE => ['onEntryComplete', 100],
    ];
  }
}
