<?php

namespace Customize\Twig\Extension;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class CartCountDownExtension extends AbstractExtension {
  public function __construct() {
  }

  public function getFunctions() {
    return [
      new TwigFunction('get_minimum_create_date', function ($carts) {
        $minimumTime = null;
        foreach ($carts as $cart) {
          $createDate = $cart->getCreateDate();
          if ($minimumTime == null || $createDate < $minimumTime) {
            $minimumTime = $createDate;
          }
        }

        return $minimumTime->getTimeStamp() + 40*60;
      }, ['is_safe' => ['all']]),
    ];
  }
}

