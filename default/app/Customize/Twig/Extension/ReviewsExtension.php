<?php

namespace Customize\Twig\Extension;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

use Eccube\Common\EccubeConfig;
use Plugin\ProductReview4\Repository\ProductReviewRepository;
use Plugin\ProductReview4\Entity\ProductReviewStatus;

class ReviewsExtension extends AbstractExtension {
  protected $productReviewRepository;
  protected $eccubeConfig;

  public function __construct(ProductReviewRepository $productReviewRepository, EccubeConfig $eccubeConfig) {
    $this->productReviewRepository = $productReviewRepository;
    $this->eccubeConfig = $eccubeConfig;
  }

  public function getFunctions() {
    return [
      new TwigFunction('get_reviews', function () {
        $qb = $this->productReviewRepository->getQueryBuilderBySearchData(['status' => [ProductReviewStatus::SHOW]]);

        return $qb->getQuery()->useResultCache(true, $this->eccubeConfig['eccube_result_cache_lifetime_short'])->getResult();
      }, ['is_safe' => ['all']]),
    ];
  }
}
