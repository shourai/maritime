<?php

namespace Customize\Twig\Extension;

use Customize\Repository\CategoryProductRepository;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class CategoryProductExtension extends AbstractExtension {
  protected $repository;

  public function __construct(CategoryProductRepository $repository) {
    $this->repository = $repository;
  }

  public function getFunctions() {
    return [
      new TwigFunction('get_category_products', function ($category_id) {
        $products = $this->repository->getCategoryProductList($category_id);

        return $products;
      }, ['is_safe' => ['all']]),
    ];
  }
}

