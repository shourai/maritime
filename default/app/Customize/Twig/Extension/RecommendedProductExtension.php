<?php

namespace Customize\Twig\Extension;

use Customize\Repository\RecommendedProductRepository;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Plugin\ProductReview4\Repository\ProductReviewRepository;

class RecommendedProductExtension extends AbstractExtension {
  protected $repository;
  protected $productReviewRepository;

  public function __construct(RecommendedProductRepository $repository, ProductReviewRepository $productReviewRepository) {
    $this->repository = $repository;
    $this->productReviewRepository = $productReviewRepository;
  }

  public function getFunctions() {
    return [
      new TwigFunction('get_recommended_products', function ($product) {
        $products = $this->repository->getRecommendedProducts($product);

        $ProductAvgs = [];
        foreach ($products['products'] as $Product) {
          $rate = $this->productReviewRepository->getAvgAll($Product);
          $avg = round($rate['recommend_avg']);
          $count = intval($rate['review_count']);
          $ProductAvgs[$Product->getId()] = [
            'avg' => $avg,
            'count' => $count,
          ];
        }

        return [
          'products' => $products,
          'reviews' => $ProductAvgs,
        ];
      }, ['is_safe' => ['all']]),
    ];
  }
}

