<?php

/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) EC-CUBE CO.,LTD. All Rights Reserved.
 *
 * http://www.ec-cube.co.jp/
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Plugin\CheckKagoochi\Controller\Admin;

use Eccube\Common\Constant;
use Eccube\Controller\AbstractController;
//use Eccube\Entity\ExportCsvRow;
//use Eccube\Entity\Master\CsvType;
use Eccube\Entity\Master\OrderStatus;
use Eccube\Entity\OrderPdf;
use Eccube\Entity\Shipping;
use Eccube\Event\EccubeEvents;
use Eccube\Event\EventArgs;
use Eccube\Form\Type\Admin\OrderPdfType;

use Plugin\CheckKagoochi\Form\Type\Admin\CheckKagoochiSearchOrderType;
use Eccube\Repository\CustomerRepository;
use Eccube\Repository\Master\OrderStatusRepository;
use Eccube\Repository\Master\PageMaxRepository;
use Eccube\Repository\Master\ProductStatusRepository;
use Eccube\Repository\Master\SexRepository;
use Eccube\Repository\OrderPdfRepository;

use Plugin\CheckKagoochi\Repository\CheckKagoochiRepository;
use Eccube\Repository\PaymentRepository;
use Eccube\Repository\ProductStockRepository;

use Plugin\CheckKagoochi\Service\CheckKagoochiMailService;
use Eccube\Service\OrderPdfService;
use Eccube\Service\OrderStateMachine;
use Eccube\Service\PurchaseFlow\PurchaseFlow;
use Eccube\Util\FormUtil;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

use Eccube\Repository\ShippingRepository;
use Eccube\Entity\Cart;
use Plugin\CheckKagoochi\Entity\CheckKagoochiSendedMail;
use DateTime;

/**
 * Class CheckKagoochiController admin.
 */
class CheckKagoochiController extends AbstractController
{
    /**
     * @var PurchaseFlow
     */
    protected $purchaseFlow;

    /**
     * @var CustomerRepository
     */
    protected $customerRepository;

    /**
     * @var PaymentRepository
     */
    protected $paymentRepository;

    /**
     * @var SexRepository
     */
    protected $sexRepository;

    /**
     * @var OrderStatusRepository
     */
    protected $orderStatusRepository;

    /**
     * @var PageMaxRepository
     */
    protected $pageMaxRepository;

    /**
     * @var ProductStatusRepository
     */
    protected $productStatusRepository;

    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /** @var OrderPdfRepository */
    protected $orderPdfRepository;

    /**
     * @var ProductStockRepository
     */
    protected $productStockRepository;

    /** @var OrderPdfService */
    protected $orderPdfService;

    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * @var OrderStateMachine
     */
    protected $orderStateMachine;

    /**
     * @var MailService
     */
    protected $mailService;

    /**
     * @var ShippingRepository
     */
    protected $shippingRepository;

    /**
     * CheckKagoochiController constructor.
     *
     * @param PurchaseFlow $orderPurchaseFlow
     * @param CustomerRepository $customerRepository
     * @param PaymentRepository $paymentRepository
     * @param SexRepository $sexRepository
     * @param OrderStatusRepository $orderStatusRepository
     * @param PageMaxRepository $pageMaxRepository
     * @param ProductStatusRepository $productStatusRepository
     * @param ProductStockRepository $productStockRepository
     * @param CheckKagoochiRepository $orderRepository
     * @param OrderPdfRepository $orderPdfRepository
     * @param ValidatorInterface $validator
     * @param OrderStateMachine $orderStateMachine ;
     * @param ShippingRepository $shippingRepository
     */
    public function __construct(
        PurchaseFlow $orderPurchaseFlow,
        CustomerRepository $customerRepository,
        PaymentRepository $paymentRepository,
        SexRepository $sexRepository,
        OrderStatusRepository $orderStatusRepository,
        PageMaxRepository $pageMaxRepository,
        ProductStatusRepository $productStatusRepository,
        ProductStockRepository $productStockRepository,
        CheckKagoochiRepository $orderRepository,
        OrderPdfRepository $orderPdfRepository,
        ValidatorInterface $validator,
        OrderStateMachine $orderStateMachine,
        CheckKagoochiMailService $mailService,
        ShippingRepository $shippingRepository
    ) {
        $this->purchaseFlow = $orderPurchaseFlow;
        $this->customerRepository = $customerRepository;
        $this->paymentRepository = $paymentRepository;
        $this->sexRepository = $sexRepository;
        $this->orderStatusRepository = $orderStatusRepository;
        $this->pageMaxRepository = $pageMaxRepository;
        $this->productStatusRepository = $productStatusRepository;
        $this->productStockRepository = $productStockRepository;
        $this->orderRepository = $orderRepository;
        $this->orderPdfRepository = $orderPdfRepository;
        $this->validator = $validator;
        $this->orderStateMachine = $orderStateMachine;
        $this->mailService = $mailService;
        $this->shippingRepository = $shippingRepository;
    }

    /**
     * 一覧画面.
     *
     * - 検索条件, ページ番号, 表示件数はセッションに保持されます.
     * - クエリパラメータでresume=1が指定された場合、検索条件, ページ番号, 表示件数をセッションから復旧します.
     * - 各データの, セッションに保持するアクションは以下の通りです.
     *   - 検索ボタン押下時
     *      - 検索条件をセッションに保存します
     *      - ページ番号は1で初期化し、セッションに保存します。
     *   - 表示件数変更時
     *      - クエリパラメータpage_countをセッションに保存します。
     *      - ただし, mtb_page_maxと一致しない場合, eccube_default_page_countが保存されます.
     *   - ページング時
     *      - URLパラメータpage_noをセッションに保存します.
     *   - 初期表示
     *      - 検索条件は空配列, ページ番号は1で初期化し, セッションに保存します.
     *
     * @Route("/%eccube_admin_route%/check_kagoochi", name="check_kagoochi_admin")
     * @Route("/%eccube_admin_route%/check_kagoochi/page/{page_no}", requirements={"page_no" = "\d+"}, name="check_kagoochi_admin_page")
     * @Template("@CheckKagoochi/admin/index.twig")
     */
    public function index(Request $request, $page_no = null, PaginatorInterface $paginator)
    {
        $builder = $this->formFactory
            ->createBuilder(CheckKagoochiSearchOrderType::class);

        $event = new EventArgs(
            [
                'builder' => $builder,
            ],
            $request
        );
        $this->eventDispatcher->dispatch(EccubeEvents::ADMIN_ORDER_INDEX_INITIALIZE, $event);

        $searchForm = $builder->getForm();

        /**
         * ページの表示件数は, 以下の順に優先される.
         * - リクエストパラメータ
         * - セッション
         * - デフォルト値
         * また, セッションに保存する際は mtb_page_maxと照合し, 一致した場合のみ保存する.
         **/
        $page_count = $this->session->get('eccube.admin.check_kagoochi.search.page_count',
            $this->eccubeConfig->get('eccube_default_page_count'));

        $page_count_param = (int) $request->get('page_count');
        $pageMaxis = $this->pageMaxRepository->findAll();

        if ($page_count_param) {
            foreach ($pageMaxis as $pageMax) {
                if ($page_count_param == $pageMax->getName()) {
                    $page_count = $pageMax->getName();
                    $this->session->set('eccube.admin.check_kagoochi.search.page_count', $page_count);
                    break;
                }
            }
        }

        if ('POST' === $request->getMethod()) {
            $searchForm->handleRequest($request);

            if ($searchForm->isValid()) {
                /**
                 * 検索が実行された場合は, セッションに検索条件を保存する.
                 * ページ番号は最初のページ番号に初期化する.
                 */
                $page_no = 1;
                $searchData = $searchForm->getData();

                // 検索条件, ページ番号をセッションに保持.
                $this->session->set('eccube.admin.check_kagoochi.search', FormUtil::getViewData($searchForm));
                $this->session->set('eccube.admin.check_kagoochi.search.page_no', $page_no);
            } else {
                // 検索エラーの際は, 詳細検索枠を開いてエラー表示する.
                return [
                    'searchForm' => $searchForm->createView(),
                    'pagination' => [],
                    'pageMaxis' => $pageMaxis,
                    'page_no' => $page_no,
                    'page_count' => $page_count,
                    'has_errors' => true,
                ];
            }
        } else {

            if (null !== $page_no || $request->get('resume')) {
                /*
                 * ページ送りの場合または、他画面から戻ってきた場合は, セッションから検索条件を復旧する.
                 */
                if ($page_no) {
                    // ページ送りで遷移した場合.
                    $this->session->set('eccube.admin.check_kagoochi.search.page_no', (int) $page_no);
                } else {
                    // 他画面から遷移した場合.
                    $page_no = $this->session->get('eccube.admin.check_kagoochi.search.page_no', 1);
                }
                $viewData = $this->session->get('eccube.admin.check_kagoochi.search', []);
                $searchData = FormUtil::submitAndGetData($searchForm, $viewData);
            } else {
                /**
                 * 初期表示の場合.
                 */
                $page_no = 1;
                $viewData = [];


                //$viewData = ['status' => (int)Status::PROSESSING];
                $date = new DateTime();
                $enddate = $date->modify('-1 days');
                $viewData = ['update_date_end' => $enddate->format('Y-m-d')];

                $searchData = FormUtil::submitAndGetData($searchForm, $viewData);

                // セッション中の検索条件, ページ番号を初期化.
                $this->session->set('eccube.admin.check_kagoochi.search', $viewData);
                $this->session->set('eccube.admin.check_kagoochi.search.page_no', $page_no);
            }
        }


        $qb = $this->orderRepository->getKagoochiQuerySearchDataForAdmin($searchData, $this->entityManager);

        $event = new EventArgs(
            [
                'qb' => $qb,
                'searchData' => $searchData,
            ],
            $request
        );

        //$this->eventDispatcher->dispatch(EccubeEvents::ADMIN_ORDER_INDEX_SEARCH, $event);
        $this->eventDispatcher->dispatch('eccube.admin.check_kagoochi.search', $event);

        $pagination = $paginator->paginate(
            $qb,
            $page_no,
            $page_count
        );

        return [
            'searchForm' => $searchForm->createView(),
            'pagination' => $pagination,
            'pageMaxis' => $pageMaxis,
            'page_no' => $page_no,
            'page_count' => $page_count,
            'has_errors' => false,
                ];
    }

    /**
     * @Route("/%eccube_admin_route%/check_kagoochi/preview_notify_mail/{id}", requirements={"id" = "\d+"}, name="check_kagoochi_admin_preview_notify_mail")
     *
     * @param Shipping $Shipping
     *
     * @return Response
     *
     * @throws \Twig_Error
     */
    public function previewKagoochiNotifyMail(Shipping $Shipping)
    {
        return new Response($this->mailService->getKagoochiNotifyMailBody($Shipping, $Shipping->getOrder(), null, true));
    }
     
    /**
     * @Route("/%eccube_admin_route%/check_kagoochi/preview_notify_mail2/{id}", requirements={"id" = "\d+"}, name="check_kagoochi_admin_preview_notify_mail2")
     *
     * @param Cart $Cart
     *
     * @return Response
     *
     * @throws \Twig_Error
     */
    public function previewKagoochiNotifyMail2(Cart $Cart)
    {
        return new Response($this->mailService->getKagoochiNotifyMailBody2($Cart, null, true));
    }

    /**
     * @Route("/%eccube_admin_route%/check_kagoochi/notify_mail/{id}", requirements={"id" = "\d+"}, name="check_kagoochi_admin_notify_mail", methods={"PUT"})
     *
     * @param Shipping $Shipping
     *
     * @return JsonResponse
     *
     * @throws \Twig_Error
     */
     public function kagoochiNotifyMail(Shipping $Shipping)
    {
        $this->isTokenValid();

        $this->mailService->sendKagoochiNotifyMail($Shipping);

        // 「送信済み」
        if (!is_null($Shipping->getOrder()->getCustomer())) {
            $cust_id = $Shipping->getOrder()->getCustomer()->getId();
        } else {
            $cust_id = 0;
        }
        $SendedMail = new CheckKagoochiSendedMail();
        $SendedMail->setTargetKbn(CheckKagoochiSendedMail::CHECKKAGOOCHI_TARGET_ORDER)
            ->setTargetId($Shipping->getId())
            ->setCustomerId($cust_id)
            ->setSendDate(new \DateTime());
        $this->entityManager->persist($SendedMail);
        $this->entityManager->flush();
        
        //$Shipping->setMailSendDate(new \DateTime());
        //$this->shippingRepository->save($Shipping);
        //$this->entityManager->flush();

        return $this->json([
            'mail' => true,
            'shipped' => false,
        ]);
    }

    /**
     * @Route("/%eccube_admin_route%/check_kagoochi/notify_mail2/{id}", requirements={"id" = "\d+"}, name="check_kagoochi_admin_notify_mail2", methods={"PUT"})
     *
     * @param Cart $Cart
     *
     * @return JsonResponse
     *
     * @throws \Twig_Error
     */
    public function kagoochiNotifyMail2(Cart $Cart)
    {
        $this->isTokenValid();

        $this->mailService->sendKagoochiNotifyMail2($Cart);

        // 「送信済み」
        if (!is_null($Cart->getCustomer())) {
            $cust_id = $Cart->getCustomer()->getId();
        } else {
            $cust_id = 0;
        }
        $SendedMail = new CheckKagoochiSendedMail();
        $SendedMail->setTargetKbn(CheckKagoochiSendedMail::CHECKKAGOOCHI_TARGET_CART)
            ->setTargetId($Cart->getId())
            ->setCustomerId($cust_id)
           ->setSendDate(new \DateTime());
        $this->entityManager->persist($SendedMail);
        $this->entityManager->flush();    

        return $this->json([
            'mail' => true,
            'shipped' => false,
        ]);
    }
}
