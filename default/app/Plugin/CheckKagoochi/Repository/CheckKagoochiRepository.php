<?php

/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) EC-CUBE CO.,LTD. All Rights Reserved.
 *
 * http://www.ec-cube.co.jp/
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Plugin\CheckKagoochi\Repository;

use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use Eccube\Doctrine\Query\Queries;
use Eccube\Entity\Customer;
use Eccube\Entity\Master\OrderStatus;
use Eccube\Entity\Order;
use Eccube\Entity\Shipping;
use Eccube\Entity\Cart;
use Eccube\Util\StringUtil;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Eccube\Repository\QueryKey;

use Eccube\Repository\AbstractRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\EntityManager;
use DateTime;
use DateTimeZone;
use Plugin\CheckKagoochi\Entity\Config;
use Plugin\CheckKagoochi\Entity\CheckKagoochiSendedMail;


/**
 * CheckKagoochiRepository
 */
class CheckKagoochiRepository extends AbstractRepository
{
    /**
     * @var Queries
     */
    protected $queries;

    /**
     * CheckKagoochiRepository constructor.
     *
     * @param RegistryInterface $registry
     * @param Queries $queries
     */
    public function __construct(RegistryInterface $registry, Queries $queries)
    {
        parent::__construct($registry, Order::class);
        $this->queries = $queries;
    }

    /**
     * 該当の条件での検索結果を返す（管理画面用）
     * 
     * @param  array $searchData
     * @param  EntityManager $entityManager
     * 
     * @return array 
     */
    public function getKagoochiQuerySearchDataForAdmin($searchData, $entityManager)
    {
    
        $where = '';
        $where_ary = [];
        
        $send_ids_order = '';
        $send_ids_cart = '';

        // カゴ落ち 販促メールメール送信/未送信.    
        $join_berore  = "  JOIN (SELECT target_id, max(send_date) AS mail_send_date FROM plg_check_kagoochi_sendedmail WHERE target_kbn = '";
        $join_arter  = "' GROUP BY target_id) sendmail ON sendmail.target_id =  ";
        $join_sm_order  = $join_berore . CheckKagoochiSendedMail::CHECKKAGOOCHI_TARGET_ORDER . $join_arter . "s.id";
        $join_sm_cart  = $join_berore . CheckKagoochiSendedMail::CHECKKAGOOCHI_TARGET_CART . $join_arter . "d0_.id";

        $join_order = '';
        $join_cart = '';
        if (isset($searchData['kagoochi_mail']) && $count = count($searchData['kagoochi_mail'])) {
            if ($count == 1) {
                $checked = current($searchData['kagoochi_mail']);
                if ($checked == CheckKagoochiSendedMail::CHECKKAGOOCHI_MAIL_UNSENT) {
                    // 未送信
                    $join_order  = ' LEFT ' . $join_sm_order;
                    $join_cart  = ' LEFT ' . $join_sm_cart;
                    $where_ary[] =  'target_id IS NULL';
                } elseif ($checked == CheckKagoochiSendedMail::CHECKKAGOOCHI_MAIL_SENT) {
                    // 送信
                    $join_order  = ' INNER ' . $join_sm_order;
                    $join_cart  = ' INNER ' . $join_sm_cart;
                }
            }
        }
        if (strlen($join_order) <= 0) {
            $join_order  = ' LEFT ' . $join_sm_order;
            $join_cart  = ' LEFT ' . $join_sm_cart;

        }            
        
        // update_date
        if (!empty($searchData['update_date_start']) && $searchData['update_date_start']) {
            $startdate = $searchData['update_date_start'];
            $where_ary[] = 'd0_.update_date >= :update_date_start';
        }
        if (!empty($searchData['update_date_end']) && $searchData['update_date_end']) {
            $date = clone $searchData['update_date_end'];
            $enddate = $date
                ->modify('+1 days');
            
            $where_ary[] = 'd0_.update_date < :update_date_end';   
        }

        if (count($where_ary) > 0) {
            $where = ' AND ' . implode(' AND ', $where_ary);
        }    

        $platform = $entityManager->getConnection()->getDatabasePlatform()->getName();
        switch ($platform) {
            case 'postgresql':
                $sql = "SELECT d0_.id,d0_.id AS target_id, 0 AS order_id, d0_.id AS shipping_id, d0_.id AS cart_id, d0_.customer_id, '' AS order_no, name01, name02, email, total_price, total_price AS payment_total,
                0 AS order_status_id, d0_.pre_order_id, 0 AS pref_id,
                '2000/1/1 0:0:0' AS order_date, d0_.update_date, d0_.pre_order_id, sendmail.mail_send_date
                FROM dtb_cart as d0_ LEFT JOIN dtb_customer cust ON cust.id = d0_.customer_id"
                        . $join_cart
                    . " WHERE d0_.pre_order_id IS null AND d0_.customer_id IS NOT null"
                    . $where  .
                    " UNION
                SELECT d0_.id,d0_.id AS target_id, d0_.id AS order_id,s.id AS shipping_id, 0 AS cart_id, d1_.customer_id , d0_.order_no, d0_.name01, d0_.name02, d0_.email, d0_.subtotal, d0_.total AS payment_total, order_status_id,
                d1_.pre_order_id, d0_.pref_id, d0_.order_date, d0_.update_date, d0_.pre_order_id, sendmail.mail_send_date
                FROM dtb_order d0_ INNER JOIN dtb_cart d1_ ON (d1_.pre_order_id = d0_.pre_order_id)
                , dtb_shipping s "
                    . $join_order 
                . " WHERE d0_.id = s.order_id " . $where . 
                    " ORDER BY update_date DESC, shipping_id";
                break;
            default:    //'mysql':
                $sql = 'SELECT d0_.id,d0_.id AS target_id, "" AS order_id, d0_.id AS shipping_id, d0_.id AS cart_id, d0_.customer_id, "" AS order_no, name01, name02, email, total_price, total_price AS payment_total, 
                    "" AS order_status_id, d0_.pre_order_id, "" AS pref_id, 
                    "" AS order_date, d0_.update_date, d0_.pre_order_id, sendmail.mail_send_date
                    FROM dtb_cart as d0_ LEFT JOIN dtb_customer cust ON cust.id = d0_.customer_id'
                        . $join_cart
                    . '  WHERE d0_.pre_order_id IS null AND d0_.customer_id IS NOT null 
                    ' . $where  .
                    ' UNION
                    SELECT d0_.id,d0_.id AS target_id, d0_.id AS order_id,s.id AS shipping_id, "" AS cart_id, d1_.customer_id , d0_.order_no, d0_.name01, d0_.name02, d0_.email, d0_.subtotal, d0_.total AS payment_total, order_status_id,
                    d1_.pre_order_id, d0_.pref_id,  d0_.order_date, d0_.update_date, d0_.pre_order_id, sendmail.mail_send_date
                        FROM dtb_order d0_ INNER JOIN dtb_cart d1_ ON (d1_.pre_order_id = d0_.pre_order_id) 
                        , dtb_shipping s'
                    . $join_order
                    . ' WHERE d0_.id = s.order_id ' . $where ;
                    if ($platform == 'sqlite') {
                        $sql .= ' ORDER BY d0_.update_date DESC, shipping_id';
                    }else{
                        $sql .= ' ORDER BY update_date DESC, shipping_id';
                    }
            }

        //echo $sql;   
        $rsm = new ResultSetMapping();

        $rsm->addScalarResult('id', 'id'); // 結果セットのカラムのマッピング必須（無いとselectに書いてても取得されない）
        $rsm->addScalarResult('target_id', 'target_id');
        $rsm->addScalarResult('order_id', 'order_id');
        $rsm->addScalarResult('shipping_id', 'shipping_id');
        $rsm->addScalarResult('cart_id', 'cart_id');
        $rsm->addScalarResult('customer_id', 'customer_id'); 
        $rsm->addScalarResult('order_no', 'order_no');
        $rsm->addScalarResult('name01', 'name01');
        $rsm->addScalarResult('name02', 'name02');
        $rsm->addScalarResult('email', 'email');
        $rsm->addScalarResult('order_date', 'order_date');
        $rsm->addScalarResult('order_status_id', 'order_status_id');
        $rsm->addScalarResult('order_date', 'order_date');
        $rsm->addScalarResult('payment_total', 'payment_total');
        $rsm->addScalarResult('update_date', 'update_date');
        $rsm->addScalarResult('pre_order_id', 'pre_order_id');
        $rsm->addScalarResult('mail_send_date', 'mail_send_date');

        $query = $entityManager->createNativeQuery($sql,$rsm);

        if (!empty($startdate)) {
            $query->setParameter('update_date_start', $startdate);
        } 
        if (!empty($enddate)) {
            $query->setParameter('update_date_end', $enddate);
        }
        
        $data = $query->getResult();

        $send_ids_order_ary = [];
        $send_ids_cart_ary = [];
        if (strlen($send_ids_order) > 0) {
            $send_ids_order_ary = explode(',', $send_ids_order);            
        }
        if (strlen($send_ids_cart) > 0) {
            $send_ids_cart_ary = explode(',', $send_ids_cart);            
        }

        foreach($data as $key=>$rec){
            //TimeZoneを変更する
            $date = new DateTime($rec['update_date'] . 'Z');
            $date->settimezone(new DateTimeZone('Asia/Tokyo'));
            $data[$key]['update_date'] = $date->format('Y-m-d H:i:s');

            if (strlen($rec['mail_send_date']) > 0) {
                $date = new DateTime($rec['mail_send_date'] . 'Z');
                $date->settimezone(new DateTimeZone('Asia/Tokyo'));
                $data[$key]['mail_send_date'] = $date->format('Y-m-d H:i:s');
            }

        }

        return $data;
    }

}
