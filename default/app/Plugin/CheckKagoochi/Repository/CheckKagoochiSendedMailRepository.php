<?php

namespace Plugin\CheckKagoochi\Repository;

use Eccube\Repository\AbstractRepository;
use Plugin\CheckKagoochi\Entity\Config;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * CheckKagoochiSendedMailRepository
 */
class CheckKagoochiSendedMailRepository extends AbstractRepository
{
    /**
     * CheckKagoochiSendedMailRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CheckKagoochiSendedMailRepository::class);
    }



    /**
     * 該当のidのカゴ落ちメールを返す
     * 
     * @param int $id
     *
     * @return null|CheckKagoochiSendedMail
     */
    public function get($id = 1)
    {
        return $this->find($id);
    }

}
