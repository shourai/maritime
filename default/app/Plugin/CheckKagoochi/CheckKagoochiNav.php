<?php

namespace Plugin\CheckKagoochi;

use Eccube\Common\EccubeNav;

class CheckKagoochiNav implements EccubeNav
{
    /**
     * @return array
     */
    public static function getNav()
    {
        return [
            'order' => [
                'children' => [
                    'check_kagoochi' => [
                        'name' => 'check_kagoochi.admin.navtitle',
                        'url' => 'check_kagoochi_admin',
                    ],
                ],
            ],
        ];
        //return [];
    }
}
