<?php

/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) EC-CUBE CO.,LTD. All Rights Reserved.
 *
 * http://www.ec-cube.co.jp/
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Plugin\CheckKagoochi;

use Doctrine\ORM\EntityManagerInterface;
use Eccube\Entity\MailTemplate;
use Eccube\Plugin\AbstractPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Plugin\CheckKagoochi\Service\CheckKagoochiMailService;

class PluginManager extends AbstractPluginManager
{
    public function enable(array $meta, ContainerInterface $container)
    {
        $em = $container->get('doctrine.orm.entity_manager');

        // メールテンプレートレコードを追加
       $MailTemlate = $this->createMailTemlate($em);
    }

    public function uninstall(array $meta, ContainerInterface $container)
    {
        $em = $container->get('doctrine.orm.entity_manager');

        // メールテンプレートレコードを削除
      $this->removeMailTemlate($em);
    }

    protected function createMailTemlate(EntityManagerInterface $em)
    {
        $MailTemplate = new MailTemplate();
        //$MailTemplate->setCreator(null);
        $MailTemplate->setName(CheckKagoochiMailService::CheckKagoochi_mailtemplate_name);
        $MailTemplate->setFilename(CheckKagoochiMailService::CheckKagoochi_mailtemplate_filename);
        $MailTemplate->setMailSubject(CheckKagoochiMailService::CheckKagoochi_mailtemplate_subject);
        //$MailTemplate->setDiscriminatorType('mailtemplate');

        // DB登録
        $em->persist($MailTemplate);
        $em->flush($MailTemplate);
    }

    protected function removeMailTemlate(EntityManagerInterface $em)
    {
        $MailTemplate = $em->getRepository(MailTemplate::class)->findOneBy(['name'=>CheckKagoochiMailService::CheckKagoochi_mailtemplate_name]);

        if (!$MailTemplate) {
            return;
        }

        $em->remove($MailTemplate);
        $em->flush($MailTemplate);
    }

}