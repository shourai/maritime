<?php

/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) EC-CUBE CO.,LTD. All Rights Reserved.
 *
 * http://www.ec-cube.co.jp/
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Plugin\CheckKagoochi\Entity;

use Doctrine\ORM\Mapping as ORM;
use Eccube\Entity\AbstractEntity;

/**
 * CheckKagoochiSendedMail
 *
 * @ORM\Table(name="plg_check_kagoochi_sendedmail", indexes={@ORM\Index(name="target_kbn", columns={"target_kbn", "target_id"}), @ORM\Index(name="send_date", columns={"send_date"}), @ORM\Index(name="target_id", columns={"target_id"})})
 * @ORM\Entity(repositoryClass="Plugin\CheckKagoochi\Repository\CheckKagoochiSendedMailRepository")
 */
class CheckKagoochiSendedMail extends AbstractEntity
{
        /**
         * メール未送信
         */
        const CHECKKAGOOCHI_MAIL_UNSENT = 1;
        /**
         * メール送信済
         */
        const CHECKKAGOOCHI_MAIL_SENT = 2;
         /**
         * 対象：order record
         */
        const CHECKKAGOOCHI_TARGET_ORDER = 'o';
         /**
         * 対象：order cart
         */
        const CHECKKAGOOCHI_TARGET_CART = 'c';    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="target_id", type="integer", nullable=false)
     */
    private $target_id;

    /**
     * @var string
     *
     * @ORM\Column(name="target_kbn", type="string", length=1, nullable=false, options={"default"="1"})
     */
    private $target_kbn;

    /**
     * @var int
     *
     * @ORM\Column(name="customer_id", type="integer", nullable=false)
     */
    private $customer_id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="send_date", type="datetimetz")
     */
    private $send_date;
    //??? @ ORM\Column(name="send_date", type="datetime", nullable=true)


    /**
     * @param string $kbn
     *
     * @return 
     */
    public function setTargetKbn($kbn)
    {
        $this->target_kbn = $kbn;
        return $this;
    }

    /**
     * @param int $target_id
     *
     * @return 
     */
    public function setTargetId($target_id)
    {
        $this->target_id = $target_id;
        return $this;
    }

    /**
     * @param   \DateTime $send_date
     *
     * @return 
     */
    public function setSendDate($send_date)
    {
        $this->send_date = $send_date;
        return $this;
    }

    /**
     * @param int $customer_id
     *
     * @return 
     */
    public function setCustomerId($customer_id)
    {
        $this->customer_id = $customer_id;
        return $this;
    }

}
