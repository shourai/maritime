<?php

namespace Plugin\ProductDisplayRank4;

use Doctrine\ORM\EntityManagerInterface;
use Eccube\Common\EccubeConfig;
use Eccube\Entity\Master\ProductListOrderBy;
use Eccube\Plugin\AbstractPluginManager;
use Plugin\ProductDisplayRank4\Entity\Config;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class PluginManager.
 */
class PluginManager extends AbstractPluginManager
{
    const VERSION = '1.0.0';

    /**
     * Install the plugin.
     *
     * @param array $meta
     * @param ContainerInterface $container
     */
    public function install(array $meta, ContainerInterface $container)
    {
    }

    /**
     * Update the plugin.
     *
     * @param array $meta
     * @param ContainerInterface $container
     */
    public function update(array $meta, ContainerInterface $container)
    {
    }

    /**
     * Enable the plugin.
     *
     * @param array $meta
     * @param ContainerInterface $container
     */
    public function enable(array $meta, ContainerInterface $container)
    {
        $this->createMasterData($container);
    }

    /**
     * Disable the plugin.
     *
     * @param array $meta
     * @param ContainerInterface $container
     */
    public function disable(array $meta, ContainerInterface $container)
    {
        $this->deleteMasterData($container);
    }

    /**
     * Uninstall the plugin.
     *
     * @param array $meta
     * @param ContainerInterface $container
     */
    public function uninstall(array $meta, ContainerInterface $container)
    {
        $this->deleteMasterData($container);
    }

    /**
     * @param ContainerInterface $container
     */
    private function createMasterData(ContainerInterface $container)
    {
        $entityManager = $container->get('doctrine')->getManager();

        $Configs = $entityManager->getRepository('Plugin\ProductDisplayRank4\Entity\Config')->findAll();

        $maxRank = 1;

        if (!count($Configs)) {
            $qb = $entityManager->createQueryBuilder();
            $maxId = $qb->select('MAX(m.id)')
                ->from(ProductListOrderBy::class, 'm')
                ->getQuery()
                ->getSingleScalarResult();
            $data = [
                [
                    'className' => 'Eccube\Entity\Master\ProductListOrderBy',
                    'id' => ++$maxId,
                    'sort_no' => ++$maxRank,
                    'name' => 'おすすめ順',
                ],
            ];
        } else {
            /* @var $Configs Config[] */
            foreach ($Configs as $Config) {
                $data[] =
                    [
                        'className' => 'Eccube\Entity\Master\ProductListOrderBy',
                        'id' => $Config->getProductListOrderById(),
                        'sort_no' => ++$maxRank,
                        'name' => $Config->getName(),
                    ]
                ;
            }
        }

        foreach ($data as $row) {
            $Entity = $entityManager->getRepository($row['className'])
                ->find($row['id']);
            if (!$Entity) {
                // 先頭に持ってくる処理を入れる
                $OtherEntities = $entityManager->getRepository($row['className'])->findBy([], ['sort_no' => 'asc']);

                $Entity = new $row['className']();
                $Entity
                    ->setName($row['name'])
                    ->setId($row['id'])
                    ->setSortNo($row['sort_no'])
                ;

                $entityManager->persist($Entity);
                $entityManager->flush($Entity);

                $i = 0;
                foreach ($OtherEntities as $OtherEntity) {
                    $OtherEntity->setSortNo($row['sort_no'] + (++$i));
                    $entityManager->flush($OtherEntity);
                }

                $Config = new Config();
                $Config
                    ->setName($row['name'])
                    ->setProductListOrderById($Entity->getId())
                    ->setType(1)
                ;

                $entityManager->persist($Config);
                $entityManager->flush($Config);
            }
        }
    }

    /**
     * @param ContainerInterface $container
     */
    private function deleteMasterData(ContainerInterface $container)
    {
        $entityManager = $container->get('doctrine')->getManager();

        $Configs = $entityManager->getRepository('Plugin\ProductDisplayRank4\Entity\Config')->findAll();

        /* @var $Configs Config[] */
        foreach ($Configs as $Config) {
            $ProductListOrderBy = $entityManager->getRepository('Eccube\Entity\Master\ProductListOrderBy')
                ->find($Config->getProductListOrderById());

            if ($ProductListOrderBy) {
                $entityManager->remove($ProductListOrderBy);
            }
        }
    }
}
