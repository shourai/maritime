<?php

namespace Plugin\ProductReserve4\Repository;

use Eccube\Repository\AbstractRepository;
use Plugin\ProductReserve4\Entity\ReserveOrder;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ReserveOrderRepository extends AbstractRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ReserveOrder::class);
    }

    public function getReserveOrderByProduct($Product)
    {
        $ReserveOrders = $this->findby(array('Product' => $Product));
        return $ReserveOrders;
    }
}
