<?php

namespace Plugin\ProductReserve4\Form\Extension;

use Eccube\Common\EccubeConfig;
use Doctrine\Common\Collections\ArrayCollection;
use Eccube\Entity\Order;
use Eccube\Form\Type\Shopping\ShippingType;
use Eccube\Repository\PaymentRepository;
use Eccube\Entity\Payment;
use Eccube\Entity\Delivery;
use Eccube\Entity\DeliveryTime;
use Eccube\Entity\Shipping;
use Eccube\Repository\DeliveryRepository;
use Eccube\Repository\DeliveryFeeRepository;

use Plugin\ProductReserve4\Repository\ProductClassExtraRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;

use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

use Plugin\ProductReserve4\Repository\ProductExtraRepository;
use Plugin\ProductReserve4\Repository\ConfigRepository;
use Plugin\ProductReserve4\Service\ProductReserveService;

class ReservationShippingExtension extends AbstractTypeExtension
{
    /**
     * @var EccubeConfig
     */
    protected $eccubeConfig;

    /**
     * @var PaymentRepository
     */
    protected $paymentRepository;

    /**
     * @var ProductExtraRepository
     */
    protected $productExtraRepository;

    /**
     * @var ProductClassExtraRepository
     */
    protected $productClassExtraRepository;

    /**
     * @var ConfigRepository
     */
    protected $configRepository;

    protected $productReserveService;

    public function __construct(
        PaymentRepository $paymentRepository,
        ProductExtraRepository $productExtraRepository,
        ProductClassExtraRepository $productClassExtraRepository,
        ConfigRepository $configRepository,
        ProductReserveService $productReserveService,
        EccubeConfig $eccubeConfig, DeliveryRepository $deliveryRepository, DeliveryFeeRepository $deliveryFeeRepository
    )
    {
        $this->paymentRepository = $paymentRepository;
        $this->productExtraRepository = $productExtraRepository;
        $this->productClassExtraRepository = $productClassExtraRepository;
        $this->configRepository = $configRepository;
        $this->productReserveService = $productReserveService;
        $this->eccubeConfig = $eccubeConfig;
        $this->deliveryRepository = $deliveryRepository;
        $this->deliveryFeeRepository = $deliveryFeeRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // お届け日のプルダウンを生成
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) {
                $Shipping = $event->getData();
                if (is_null($Shipping) || !$Shipping->getId()) {
                    return;
                }

                // お届け日の設定
                $minDate = 0;
                $deliveryDurationFlag = false;

                // 配送時に最大となる商品日数を取得
                foreach ($Shipping->getOrderItems() as $detail) {
                    $ProductClass = $detail->getProductClass();
                    if (is_null($ProductClass)) {
                        continue;
                    }
                    $deliveryDuration = $ProductClass->getDeliveryDuration();
                    if (is_null($deliveryDuration)) {
                        continue;
                    }
                    if ($deliveryDuration->getDuration() < 0) {
                        // 配送日数がマイナスの場合はお取り寄せなのでスキップする
                        $deliveryDurationFlag = false;
                        break;
                    }
                    $Product = $ProductClass->getProduct();
                    $product_id = $Product->getId();
                    $reserve_product_extra = $this->productExtraRepository->get($product_id);
                    if($this->productReserveService->getProductReserveStatus($reserve_product_extra) !== ProductReserveService::STATUS_RESERVE){
                        $reserve_product_extra = $this->productClassExtraRepository->getByProductClassId($ProductClass->getId());
                    }
                    if( $reserve_product_extra ) {
                        if(!$reserve_product_extra->getShippingDate()) {
                            $deliveryDurationFlag = false;
                            break;
                        }
                        $reserve_shipping_day = $reserve_product_extra->getShippingDate()->diff(new \DateTime("now"))->d + 1;

                        if ($minDate < ($deliveryDuration->getDuration() + $reserve_shipping_day) ) {
                            $minDate = ($deliveryDuration->getDuration() + $reserve_shipping_day);
                        }
                    } else {
                        if ($minDate < $deliveryDuration->getDuration()) {
                            $minDate = $deliveryDuration->getDuration();
                        }
                    }
                    // 配送日数が設定されている
                    $deliveryDurationFlag = true;
                }

                // 配達最大日数期間を設定
                $deliveryDurations = [];

                // 配送日数が設定されている
                if ($deliveryDurationFlag) {
                    $period = new \DatePeriod(
                        new \DateTime($minDate.' day'),
                        new \DateInterval('P1D'),
                        new \DateTime($minDate + $this->eccubeConfig['eccube_deliv_date_end_max'].' day')
                    );

                    // 曜日設定用
                    $dateFormatter = \IntlDateFormatter::create(
                        'ja_JP@calendar=japanese',
                        \IntlDateFormatter::FULL,
                        \IntlDateFormatter::FULL,
                        'Asia/Tokyo',
                        \IntlDateFormatter::TRADITIONAL,
                        'E'
                    );

                    foreach ($period as $day) {
                        $deliveryDurations[$day->format('Y/m/d')] = $day->format('Y/m/d').'('.$dateFormatter->format($day).')';
                    }
                }

                $form = $event->getForm();
                $form->remove('shipping_delivery_date');
                $form->add(
                        'shipping_delivery_date',
                        ChoiceType::class,
                        [
                            'choices' => array_flip($deliveryDurations),
                            'required' => false,
                            'placeholder' => 'common.select__unspecified',
                            'mapped' => false,
                            'data' => $Shipping->getShippingDeliveryDate() ? $Shipping->getShippingDeliveryDate()->format('Y/m/d') : null,
                        ]
                    );
            }
        );

    }

    public function getExtendedType()
    {
        return ShippingType::class;
    }
}
