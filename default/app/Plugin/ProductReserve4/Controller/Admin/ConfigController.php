<?php

namespace Plugin\ProductReserve4\Controller\Admin;

use Eccube\Controller\AbstractController;
use Plugin\ProductReserve4\Form\Type\Admin\ConfigType;
use Plugin\ProductReserve4\Repository\ConfigRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

use Eccube\Repository\Master\OrderStatusRepository;
use Eccube\Repository\PaymentRepository;


class ConfigController extends AbstractController
{
    /**
     * @var ConfigRepository
     */
    protected $configRepository;

    protected $paymentRepository;

    protected $orderStatusRepository;

    /**
     * ConfigController constructor.
     *
     * @param ConfigRepository $configRepository
     */
    public function __construct(
        ConfigRepository $configRepository,
        OrderStatusRepository $orderStatusRepository,
        PaymentRepository $paymentRepository
    )
    {
        $this->configRepository = $configRepository;
        $this->paymentRepository = $paymentRepository;
        $this->orderStatusRepository = $orderStatusRepository;
    }

    /**
     * @Route("/%eccube_admin_route%/product_reserve4/config", name="product_reserve4_admin_config")
     * @Template("@ProductReserve4/admin/config.twig")
     */
    public function index(Request $request)
    {

        $config_order_status = $this->configRepository->getConfig('order_status');
        $config_payments = $this->configRepository->getConfig('payments');
        $payments = [];
        if( $config_payments ) {
            $payment_ids = explode(',', $config_payments);
            $payments = $this->paymentRepository->findBy(['id'=>$payment_ids]);
        }

        $form = $this->createForm(ConfigType::class, [
            'order_status' => $config_order_status ? $this->orderStatusRepository->find($config_order_status) : null,
            'payments' => $payments
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->configRepository->setConfig('order_status', $form->get('order_status')->getData() ? $form->get('order_status')->getData()->getId() : 0);
            $payments =  $form->get('payments')->getData() ? $form->get('payments')->getData():[];
            $payment_ids = [];
            foreach($payments as $payment) {
                $payment_ids[] = $payment->getId();
            }
            $this->configRepository->setConfig('payments', implode(',', $payment_ids));

            $this->addSuccess('登録しました。', 'admin');

            return $this->redirectToRoute('product_reserve4_admin_config');
        }

        return [
            'form' => $form->createView(),
        ];
    }
}
