<?php

/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Plugin\ProductReview4\Form\Type;

use Eccube\Common\EccubeConfig;
use Eccube\Form\Type\Master\SexType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Translation\Translator;

/**
 * Class ProductReviewType
 * [商品レビュー]-[レビューフロント]用Form.
 */
class ProductReviewType extends AbstractType
{
    /**
     * @var EccubeConfig
     */
    protected $eccubeConfig;
    protected $translator;

    /**
     * ProductReviewType constructor.
     *
     * @param EccubeConfig $eccubeConfig
     */
    public function __construct(EccubeConfig $eccubeConfig, Translator $translator)
    {
        $this->eccubeConfig = $eccubeConfig;
        $this->translator = $translator;
    }

    /**
     * build form.
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $config = $this->eccubeConfig;
        $builder
            ->add('reviewer_name', TextType::class, [
                'label' => 'product_review.form.product_review.reviewer_name',
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length(['max' => $config['eccube_stext_len']]),
                ],
                'attr' => [
                    'maxlength' => $config['eccube_stext_len'],
                ],
            ])
            ->add('reviewer_url', TextType::class, [
                'label' => 'product_review.form.product_review.reviewer_url',
                'required' => false,
                'constraints' => [
                    new Assert\Url(),
                    new Assert\Length(['max' => $config['eccube_mltext_len']]),
                ],
                'attr' => [
                    'maxlength' => $config['eccube_mltext_len'],
                ],
            ])
            ->add('sex', SexType::class, [
                'required' => false,
            ])
            ->add('recommend_level', ChoiceType::class, [
                'label' => 'product_review.form.product_review.recommend_level',
                'choices' => array_flip([
                    '5' => '★★★★★',
                    '4' => '★★★★',
                    '3' => '★★★',
                    '2' => '★★',
                    '1' => '★',
                ]),
                'expanded' => true,
                'multiple' => false,
                'placeholder' => false,
                'constraints' => [
                    new Assert\NotBlank(),
                ],
            ])
            ->add('result', ChoiceType::class, [
              'label' => 'product_review.form.product_review.result',
              'choices' => [
                'product_review.form.default_title_1' => 'default_title_1',
                'product_review.form.default_title_2' => 'default_title_2',
                'product_review.form.default_title_3' => 'default_title_3',
                'product_review.form.other_title' => 'others',
              ],
              'expanded' => true,
              'multiple' => false,
              'placeholder' => false,
              'mapped' => false,
              'constraints' => [
                  new Assert\NotBlank(),
              ],
            ])
            ->add('title', TextType::class, [
                'label' => 'product_review.form.product_review.title',
                'constraints' => [
                    new Assert\Length(['max' => $config['eccube_stext_len']]),
                ],
                'required' => false,
                'attr' => [
                    'maxlength' => $config['eccube_stext_len'],
                ],
                'constraints' => [
                  new Assert\NotBlank([
                    'groups' => ['result_others'],
                  ]),
                ],
            ])
            ->add('comment', TextareaType::class, [
                'label' => 'product_review.form.product_review.comment',
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length(['max' => $config['eccube_ltext_len']]),
                ],
                'attr' => [
                    'maxlength' => $config['eccube_ltext_len'],
                ],
            ]);

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function(FormEvent $event) {
          $form = $event->getForm();
          $data = $event->getData();
          if (isset($data['result'])) {
            switch ($data['result']) {
            case 'default_title_1':
              $data['title'] = $this->translator->trans('product_review.form.default_title_1');
              break;
            case 'default_title_2':
              $data['title'] = $this->translator->trans('product_review.form.default_title_2');
              break;
            case 'default_title_3':
              $data['title'] = $this->translator->trans('product_review.form.default_title_3');
              break;
            default:
              break;
            }
          } else {
            switch ($data['title']) {
            case $this->translator->trans('product_review.form.default_title_1'):
              $data['result'] = 'default_title_1';
              break;
            case $this->translator->trans('product_review.form.default_title_2'):
              $data['result'] = 'default_title_2';
              break;
            case $this->translator->trans('product_review.form.default_title_3'):
              $data['result'] = 'default_title_3';
              break;
            default:
              $data['result'] = 'others';
              break;
            }
          }
          $event->setData($data);
        });
    }

    public function configureOptions(OptionsResolver $resolver) {
      $resolver->setDefaults([
        'validation_groups' => function (FormInterface $form) {
          $groups = ['Default'];
          $data = $form['result']->getData();

          if ($data == 'others') {
            $groups[] = 'result_others';
          }

          return $groups;
        },
      ]);
    }
}
