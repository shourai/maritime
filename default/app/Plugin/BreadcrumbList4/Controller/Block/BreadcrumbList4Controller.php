<?php
/*
* This file is part of the BreadcrumbList4-plugin package.
*
* (c) Nobuhiko Kimoto All Rights Reserved.
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
 */

namespace Plugin\BreadcrumbList4\Controller\Block;

use Eccube\Controller\AbstractController;
use Eccube\Repository\Master\DeviceTypeRepository;
use Eccube\Repository\BlockRepository;
use Eccube\Repository\PageRepository;
use Eccube\Repository\ProductRepository;
use Eccube\Entity\Master\DeviceType;
use Eccube\Form\Type\SearchProductType;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;


class BreadcrumbList4Controller extends AbstractController
{

    /**
     * @Route("/block/BreadcrumbList4", name="block_BreadcrumbList4")
     */
    public function index(RouterInterface $router)
    {
        // 初期化
        $Breadcrumb[0][0] = array();
        $arrBreadcrumb = array();
        // サブdirを考慮
        // $request_uri = str_replace($router->getContext()->getBaseUrl(), '', $_SERVER['REQUEST_URI']);
        $request_uri = $_SERVER['REQUEST_URI'];
        $subdir = $router->getContext()->getBaseUrl();
        if (substr($request_uri, 0, strlen($subdir)) == $subdir) {
          $request_uri = substr($request_uri, strlen($subdir), strlen($request_uri));
        }
        $request = Request::create($request_uri);

        // urlからrouteを作成
        //$result = $route->match(strtok($request_uri, '?'));
        $routes = $this->get('router')->getRouteCollection();
        $context = new RequestContext();
        $context->fromRequest(Request::createFromGlobals());
        $matcher = new UrlMatcher($routes, $context);
        $result = $matcher->match(strtok($request_uri, '?'));

        $route = $result['_route'];

        $DeviceType = $this->container->get(DeviceTypeRepository::class)->find(DeviceType::DEVICE_TYPE_PC);


        if ($route == $this->container->getParameter('eccube_user_data_route')) {
            $route = $result['route'];
        }

        $PageLayout = $this->container->get(PageRepository::class)->getPageByRoute($route);

        switch ($route) {
        case 'product_list':
            if (isset($_GET['category_id'])) {
                $id = $_GET['category_id'];
            }else{
                $id = NULL;
            }

            if ($id) {
                $builder = $this->formFactory->createNamedBuilder('', SearchProductType::class);
                $builder->setMethod('GET');
                $searchForm = $builder->getForm();
                $searchForm->handleRequest($request);
                $searchData = $searchForm->getData();

                $Category = $searchForm->get('category_id')->getData();

                $arrBreadcrumb[0]['category_name'] = $PageLayout->getName();
                $arrBreadcrumb[0]['url'] = 'product_list';
                $arrBreadcrumb[0]['q'] = '';

                $key = 1;

                foreach($Category->getPath() as $cat) {

                    if ((count($Category->getPath())) == $key) {

                        $PageLayout->setName($cat->getName());

                    } else {

                        $arrBreadcrumb[$key]['category_name'] = $cat->getName();
                        $arrBreadcrumb[$key]['url'] = 'product_list';
                        $arrBreadcrumb[$key]['q'] = '?category_id='.$cat->getId();
                    }
                    $key++;
                }
                $Breadcrumb[0] = $arrBreadcrumb;
            }

            break;
        case 'product_detail':
            $Breadcrumb = array();

            // 他にいい方法はないのか
            list($url1, $url2, $url3, $id) = explode('/', $request->getPathInfo());
            $Product = $this->container->get(ProductRepository::class)->find($id);

            foreach($Product->getProductCategories() as $ProductCategories) {

                // 同一ルートのカテゴリーは1つにまとめる
                $id = $ProductCategories->getCategory()->getId();
                $parents = $ProductCategories->getCategory()->getParents();
                if (!empty($parents)) {
                    $id = $parents[0]->getId();
                }

                $key = 1;
                $arrBreadcrumb = array();
                $arrBreadcrumb[0]['category_name'] = '商品一覧';
                $arrBreadcrumb[0]['url'] = 'product_list';
                $arrBreadcrumb[0]['q'] = '';

                foreach($ProductCategories->getCategory()->getPath() as $cat) {
                    $arrBreadcrumb[$key]['category_name'] = $cat->getName();
                    $arrBreadcrumb[$key]['url'] = 'product_list';
                    $arrBreadcrumb[$key]['q'] = '?category_id='.$cat->getId();
                    $key++;
                }
                $Breadcrumb[$id] = $arrBreadcrumb;
            }
            $PageLayout->setName($Product->getName());

            break;
        case 'homepage':
            // TOPは表示しない
            $PageLayout = '';
            break;
        default:
            // todo twigからtitleを自動取得したいが無理そう
            if (strpos($PageLayout->getUrl(), 'mypage') !== false) {
                $arrBreadcrumb[0]['category_name'] = 'マイページ';
                $arrBreadcrumb[0]['url'] = 'mypage';
                $arrBreadcrumb[0]['q'] = '';
                $Breadcrumb[0] = $arrBreadcrumb;
                $PageLayout->setName(ltrim(strstr($PageLayout->getName(), '/'), '/'));

            } elseif (strpos($PageLayout->getUrl(), 'shopping') !== false) {
                $PageLayout->setName('ショッピングカート');
            } elseif ($PageLayout->getUrl() == '') {
                if (strpos($request_uri, '/news') !== false) { // /News url
                    $PageLayout->setName('新着情報');
                } elseif (strpos($request_uri, '/blog') !== false) { // /Blog url
                    $PageLayout->setName('ブログ');
                }
            }
            break;
        }

        return $this->render('Block/BreadcrumbList4.twig', array(
            'PageLayout' => $PageLayout,
            'BreadcrumbList' => $Breadcrumb,
        ));
    }
}
