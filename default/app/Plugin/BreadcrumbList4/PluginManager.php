<?php

namespace Plugin\BreadcrumbList4;

use Eccube\Plugin\AbstractPluginManager;
use Eccube\Repository\Master\DeviceTypeRepository;
use Eccube\Repository\BlockRepository;
use Eccube\Entity\Master\DeviceType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;

class PluginManager extends AbstractPluginManager
{
    private $blockFileName = 'BreadcrumbList4';

    public function enable(array $meta, ContainerInterface $container)
    {
        $em = $container->get('doctrine.orm.entity_manager');
        $DeviceType = $container->get(DeviceTypeRepository::class)->find(DeviceType::DEVICE_TYPE_PC);

        // block 作成
        $Block = new \Eccube\Entity\Block();
        $Block
            ->setName('パンくずプラグイン')
            ->setFileName($this->blockFileName)
            ->setDeviceType($DeviceType)
            ->setUseController(true)
            ->setDeletable(true)
            ;

        $em->persist($Block);
        $em->flush($Block);

        $file = new Filesystem();

        $dir = sprintf('%s/app/template/%s/Block',
            $container->getParameter('kernel.project_dir'),
            $container->getParameter('eccube.theme'));

        $file->copy(__DIR__ . '/Resource/template/'.$this->blockFileName.'.twig', $dir . '/' . $this->blockFileName . '.twig');
    }


    public function disable(array $meta, ContainerInterface $container)
    {
        $em = $container->get('doctrine.orm.entity_manager');

        // 削除
        $Block = $container->get(BlockRepository::class)->findOneBy(array('file_name' => $this->blockFileName));
        $em->remove($Block);
        $em->flush($Block);

    }
}
