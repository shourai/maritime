<?php
namespace Plugin\BreadcrumbList4\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Eccube\Repository\PageRepository;

class BreadcrumbList4Extension extends AbstractExtension
{
    /**
     * @var PageRepository
     */
    protected $pageRepository;

    public function __construct(PageRepository $pageRepository)
    {
        $this->pageRepository = $pageRepository;
    }


    public function getFunctions()
    {
        return array(
            new TwigFunction('breadcrumb', array($this, 'createBreadcrumbList')),
        );
    }

    public function createBreadcrumbList($id)
    {
        $Page = $this->pageRepository->find($id);

        dump($Page->getName());

        return null;
    }
}
