<?php
namespace Plugin\MGSocialButton;

use Eccube\Event\TemplateEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class MGSocialButtonEvent implements EventSubscriberInterface
{
    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            'Product/detail.twig' => 'detail',
        ];
    }

    public function detail(TemplateEvent $event)
    {
        $twig = '@MGSocialButton/social_button.twig';
        $event->addSnippet($twig);
    }
}
