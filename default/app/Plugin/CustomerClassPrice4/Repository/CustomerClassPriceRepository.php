<?php


namespace Plugin\CustomerClassPrice4\Repository;


use Doctrine\Common\Persistence\ManagerRegistry;
use Eccube\Repository\AbstractRepository;
use Plugin\CustomerClassPrice4\Entity\CustomerClassPrice;

class CustomerClassPriceRepository extends AbstractRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CustomerClassPrice::class);
    }
}