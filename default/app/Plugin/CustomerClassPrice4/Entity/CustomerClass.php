<?php

namespace Plugin\CustomerClassPrice4\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Eccube\Entity\AbstractEntity;
use Eccube\Entity\Customer;

if (!class_exists('\Plugin\CustomerClassPrice4\Entity\CustomerClass')) {
    /**
     * @ORM\Table(name="plg_ccp_customer_class")
     * @ORM\Entity(repositoryClass="Plugin\CustomerClassPrice4\Repository\CustomerClassRepository")
     */
    class CustomerClass extends AbstractEntity
    {
        /**
         * @return string
         */
        public function __toString()
        {
            return (string)$this->getName();
        }

        /**
         * @ORM\Column(name="id", type="integer", options={"unsigned":true})
         * @ORM\Id()
         * @ORM\GeneratedValue(strategy="IDENTITY")
         */
        private $id;

        /**
         * @ORM\Column(type="string", length=255)
         */
        private $name;

        /**
         * @ORM\Column(type="decimal", precision=10, scale=0, nullable=true)
         */
        private $discountRate;

        public function getId(): ?int
        {
            return $this->id;
        }

        public function getName(): ?string
        {
            return $this->name;
        }

        public function setName(string $name): self
        {
            $this->name = $name;

            return $this;
        }

        public function getDiscountRate()
        {
            return $this->discountRate;
        }

        public function setDiscountRate($discountRate): self
        {
            $this->discountRate = $discountRate;

            return $this;
        }
    }

}
