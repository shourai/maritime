<?php


namespace Plugin\CustomerClassPrice4\Entity;


use Doctrine\ORM\Mapping as ORM;
use Eccube\Entity\AbstractEntity;
use Eccube\Entity\ProductClass;

if (!class_exists('\Plugin\CustomerClassPrice4\Entity\CustomerClassPrice')) {
    /**
     * @ORM\Table(name="plg_ccp_customer_class_price")
     * @ORM\Entity(repositoryClass="Plugin\CustomerClassPrice4\Repository\CustomerClassPriceRepository")
     *
     * Class CustomerClassPrice
     * @package Plugin\CustomerClassPrice4\Entity
     */
    class CustomerClassPrice extends AbstractEntity
    {
        /**
         * @ORM\Column(name="id", type="integer", options={"unsigned":true})
         * @ORM\Id()
         * @ORM\GeneratedValue(strategy="IDENTITY")
         */
        private $id;

        /**
         * @ORM\Column(type="decimal", precision=12, scale=2, nullable=true)
         */
        private $price;

        /**
         * @ORM\ManyToOne(targetEntity="Plugin\CustomerClassPrice4\Entity\CustomerClass", inversedBy="CustomerClassPrice")
         * @ORM\JoinColumn(nullable=true)
         */
        private $customerClass;

        /**
         * @ORM\ManyToOne(targetEntity="Eccube\Entity\ProductClass", inversedBy="CustomerClassPrice")
         * @ORM\JoinColumn(nullable=true)
         */
        private $productClass;

        public function getId(): ?int
        {
            return $this->id;
        }

        public function getPrice()
        {
            return $this->price;
        }

        public function setPrice($price): self
        {
            $this->price = $price;

            return $this;
        }

        public function getCustomerClass(): ?CustomerClass
        {
            return $this->customerClass;
        }

        public function setCustomerClass(?CustomerClass $customerClass): self
        {
            $this->customerClass = $customerClass;

            return $this;
        }

        public function getProductClass(): ?ProductClass
        {
            return $this->productClass;
        }

        public function setProductClass(?ProductClass $productClass): self
        {
            $this->productClass = $productClass;

            return $this;
        }
    }
}