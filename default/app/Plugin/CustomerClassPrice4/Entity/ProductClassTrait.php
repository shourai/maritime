<?php


namespace Plugin\CustomerClassPrice4\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Eccube\Annotation\EntityExtension;

/**
 * @EntityExtension("Eccube\Entity\ProductClass")
 *
 * Trait ProductClassTrait
 * @package Plugin\CustomerClassPrice4\Entity
 */
trait ProductClassTrait
{
    /**
     * @ORM\OneToMany(targetEntity="Plugin\CustomerClassPrice4\Entity\CustomerClassPrice", mappedBy="productClass", cascade={"persist"})
     */
    private $plgCcpCustomerClassPrices;

    public function getPlgCcpCustomerClassPrices(): Collection
    {
        if(!$this->plgCcpCustomerClassPrices) {
            $this->plgCcpCustomerClassPrices = new ArrayCollection();
        }

        return $this->plgCcpCustomerClassPrices;
    }

    public function addPlgCcpCustomerClassPrice(CustomerClassPrice $customerClassPrice): self
    {
        if(!$this->plgCcpCustomerClassPrices) {
            $this->plgCcpCustomerClassPrices = new ArrayCollection();
        }

        if(!$this->plgCcpCustomerClassPrices->contains($customerClassPrice)) {
            $this->plgCcpCustomerClassPrices[] = $customerClassPrice;
            $customerClassPrice->setProductClass($this);
        }

        return $this;
    }

    public function removePlgCcpCutomerClassPrice(CustomerClassPrice $customerClassPrice): self
    {
        if(!$this->plgCcpCustomerClassPrices) {
            $this->plgCcpCustomerClassPrices = new ArrayCollection();
        }

        if($this->plgCcpCustomerClassPrices->contains($customerClassPrice)) {
            $this->plgCcpCustomerClassPrices->removeElement($customerClassPrice);

            if($customerClassPrice->getProductClass() === $this) {
                $customerClassPrice->setProductClass(null);
            }
        }

        return $this;
    }
}