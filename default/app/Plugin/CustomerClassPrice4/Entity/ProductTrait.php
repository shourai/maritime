<?php

/*
 * Copyright (C) 2019 Akira Kurozumi <info@a-zumi.net>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

namespace Plugin\CustomerClassPrice4\Entity;

use Doctrine\ORM\Mapping as ORM;
use Eccube\Annotation as Eccube;

/**
 * Trait ProductTrait
 *
 * @Eccube\EntityExtension("Eccube\Entity\Product")
 */
trait ProductTrait
{
    /**
     * @ORM\Column("plg_ccp_enabled_discount", type="boolean", options={"default":true}, nullable=true)
     */
    private $plgCcpEnabledDiscount;

    public function isPlgCcpEnabledDiscount(): ?bool
    {
        return $this->plgCcpEnabledDiscount;
    }

    public function setPlgCcpEnabledDiscount(?bool $plgCcpEnabledDiscount): self
    {
        $this->plgCcpEnabledDiscount = $plgCcpEnabledDiscount;

        return $this;
    }
}
