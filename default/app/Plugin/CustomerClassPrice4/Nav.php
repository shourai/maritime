<?php

/*
 * Copyright (C) 2019 Akira Kurozumi <info@a-zumi.net>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

namespace Plugin\CustomerClassPrice4;

use Eccube\Common\EccubeNav;

class Nav implements EccubeNav
{
    /**
     * @return array
     */
    public static function getNav()
    {
        return [
            'customer_class_price4' => [
                'name' => '特定会員設定',
                'icon' => 'fa-users',
                'children' => [
                    'customer_class_price4_customer_class' => [
                        'name' => '会員種別管理',
                        'url' => 'plg_ccp_customer_class_admin',
                    ],
                    'customer_class_price4_config' => [
                        'name' => '割引設定',
                        'url' => 'customer_class_price4_admin_config'
                    ]
                ],
            ],
        ];
    }
}
