<?php


namespace Plugin\CustomerClassPrice4\Service\PurchaseFlow\Processor;


use Eccube\Annotation\ShoppingFlow;
use Eccube\Entity\ItemHolderInterface;
use Eccube\Service\PurchaseFlow\Processor\TaxProcessor;
use Eccube\Service\PurchaseFlow\PurchaseContext;

/**
 * 税の再計算
 *
 * @ShoppingFlow()
 *
 * Class Processor2
 * @package Plugin\CustomerClassPrice4\Service\PurchaseFlow\Processor
 */
class Processor3 extends TaxProcessor
{
    public function process(ItemHolderInterface $itemHolder, PurchaseContext $context)
    {
        return parent::process($itemHolder, $context); // TODO: Change the autogenerated stub
    }

}