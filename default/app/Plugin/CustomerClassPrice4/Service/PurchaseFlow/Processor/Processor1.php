<?php

/*
 * Copyright (C) 2019 Akira Kurozumi <info@a-zumi.net>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

namespace Plugin\CustomerClassPrice4\Service\PurchaseFlow\Processor;

use Doctrine\ORM\EntityManagerInterface;
use Eccube\Annotation\ShoppingFlow;
use Eccube\Entity\Customer;
use Eccube\Entity\ItemHolderInterface;
use Eccube\Entity\Order;
use Eccube\Repository\TaxRuleRepository;
use Eccube\Service\PurchaseFlow\ItemHolderPreprocessor;
use Eccube\Service\PurchaseFlow\PurchaseContext;
use Eccube\Service\TaxRuleService;
use Plugin\CustomerClassPrice4\Service\DiscountHelper;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * 割引率が設定されている特定会員の場合は割引計算した価格をセット
 *
 * @ShoppingFlow()
 *
 * Class Processor1
 * @package Plugin\CustomerClassPrice4\Service\PurchaseFlow\Processor
 */
class Processor1 implements ItemHolderPreprocessor
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var TaxRuleRepository
     */
    protected $taxRuleRepository;

    /**
     * @var TaxRuleService
     */
    protected $taxRuleService;
    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    /**
     * @var DiscountHelper
     */
    private $discountHelper;

    /**
     * TaxProcessor constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param TaxRuleRepository $taxRuleRepository
     * @param TaxRuleService $taxRuleService
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param DiscountHelper $discountHelper
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        TaxRuleRepository $taxRuleRepository,
        TaxRuleService $taxRuleService,
        AuthorizationCheckerInterface $authorizationChecker,
        DiscountHelper $discountHelper
    ) {
        $this->em = $entityManager;
        $this->taxRuleRepository = $taxRuleRepository;
        $this->taxRuleService = $taxRuleService;
        $this->authorizationChecker = $authorizationChecker;
        $this->discountHelper = $discountHelper;
    }

    /**
     * 受注データ調整処理。
     *
     * @param ItemHolderInterface $itemHolder
     * @param PurchaseContext $context
     *
     * @throws \Doctrine\ORM\NoResultException
     */
    public function process(ItemHolderInterface $itemHolder, PurchaseContext $context)
    {
        if (!$itemHolder instanceof Order) {
            return;
        }

        // 会員じゃない場合は割引しない
        $Customer = $context->getUser();
        if (!$Customer instanceof Customer) {
            return;
        }

        // 特定会員じゃない場合は割引しない
        if (!$Customer->isPlgCcpCustomerClass()) {
            return;
        }

        // 割引率が設定されていない特定会員の場合は割引しない
        if (!$Customer->getPlgCcpCustomerClass()->getDiscountRate()) {
            return;
        }

        foreach ($itemHolder->getOrderItems() as $item) {
            if (!$item->isProduct()) {
                continue;
            }

            // 割引対象外だったらスルー
            if (!$item->getProduct()->isPlgCcpEnabledDiscount()) {
                continue;
            }

            // 割引価格をセット
            $item->setPrice($this->discountHelper->calculatePrice($item->getProductClass()->getPrice02(), $Customer));
        }
    }
}
