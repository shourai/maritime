<?php


namespace Plugin\CustomerClassPrice4\Service\PurchaseFlow\Processor;


use Doctrine\ORM\EntityManagerInterface;
use Eccube\Annotation\ShoppingFlow;
use Eccube\Entity\Customer;
use Eccube\Entity\ItemHolderInterface;
use Eccube\Entity\Order;
use Eccube\Service\PurchaseFlow\ItemHolderPreprocessor;
use Eccube\Service\PurchaseFlow\PurchaseContext;
use Plugin\CustomerClassPrice4\Entity\CustomerClassPrice;

/**
 * 特定会員価格が設定されている商品の場合、特定会員価格をセット
 *
 * @ShoppingFlow()
 *
 * Class Processor2
 * @package Plugin\CustomerClassPrice4\Service\PurchaseFlow\Processor
 */
class Processor2 implements ItemHolderPreprocessor
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    /**
     * @inheritDoc
     */
    public function process(ItemHolderInterface $itemHolder, PurchaseContext $context)
    {
        // TODO: Implement process() method.
        if(!$itemHolder instanceof Order) {
            return;
        }

        // 会員じゃない場合は割引しない
        $Customer = $context->getUser();
        if(!$Customer instanceof Customer) {
            return;
        }

        // 特定会員じゃない場合は割引しない
        if(!$Customer->isPlgCcpCustomerClass()) {
            return;
        }

        $customerClass = $Customer->getPlgCcpCustomerClass();
        foreach ($itemHolder->getOrderItems() as $item) {
            if(!$item->isProduct()) {
                continue;
            }

            // 特定会員価格対象外だったらスルー
            if(!$item->getProduct()->isPlgCcpEnabledDiscount()) {
                continue;
            }

            $customerClassPrice = $this->entityManager->getRepository(CustomerClassPrice::class)->findOneBy([
                "customerClass" => $customerClass,
                "productClass" => $item->getProductClass()
            ]);

            if($customerClassPrice instanceof CustomerClassPrice) {
                if($customerClassPrice->getPrice()) {
                    $item->setPrice($customerClassPrice->getPrice());
                }
            }
        }
    }
}