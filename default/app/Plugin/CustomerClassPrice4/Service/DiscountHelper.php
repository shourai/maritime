<?php

/*
 * Copyright (C) 2019 Akira Kurozumi <info@a-zumi.net>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

namespace Plugin\CustomerClassPrice4\Service;

use Doctrine\ORM\EntityManagerInterface;
use Eccube\Entity\Customer;
use Eccube\Request\Context;
use Plugin\CustomerClassPrice4\Entity\Config;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DiscountHelper
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var Context
     */
    private $requestContext;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var CustomerClassPriceHelper
     */
    private $customerClassPriceHelper;

    /**
     * DiscountHelper constructor.
     *
     * @param EntityManagerInterface $em
     * @param Context $requestContext
     * @param ContainerInterface $container
     * @param CustomerClassPriceHelper $customerClassPriceHelper
     */
    public function __construct(
        EntityManagerInterface $em,
        Context $requestContext,
        ContainerInterface $container,
        CustomerClassPriceHelper $customerClassPriceHelper
    ) {
        $this->em = $em;
        $this->requestContext = $requestContext;
        $this->container = $container;
        $this->customerClassPriceHelper = $customerClassPriceHelper;
    }

    /**割引が設定されている特定会員の場合、割引価格を計算
     *
     * @param float $price
     * @param Customer $Customer
     * @return float
     */
    public function calculatePrice(float $price, Customer $Customer): float
    {
        $RoundingType = $this->em->getRepository(Config::class)->get()->getRoundingType();

        $DiscountRate = $Customer->getPlgCcpCustomerClass()->getDiscountRate();
        if ($DiscountRate) {
            // 四捨五入。JPY以外の場合下三桁を四捨五入
            return $this->customerClassPriceHelper->roundByRoundingType($price * ((100 - $DiscountRate) / 100), $RoundingType);
        }

        return $price;
    }
}
