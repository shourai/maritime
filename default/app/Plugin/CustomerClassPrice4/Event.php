<?php


namespace Plugin\CustomerClassPrice4;


use Eccube\Event\TemplateEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class Event implements EventSubscriberInterface
{

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            '@admin/Product/product.twig' => 'onRenderAdminProduct'
        ];
    }

    public function onRenderAdminProduct(TemplateEvent $event)
    {
        $event->addSnippet('@CustomerClassPrice4/admin/Product/product.twig');
    }
}