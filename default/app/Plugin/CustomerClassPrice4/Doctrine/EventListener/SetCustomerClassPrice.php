<?php


namespace Plugin\CustomerClassPrice4\Doctrine\EventListener;


use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\EntityManagerInterface;
use Eccube\Entity\Customer;
use Eccube\Entity\OrderItem;
use Eccube\Entity\ProductClass;
use Eccube\Request\Context;
use Eccube\Service\TaxRuleService;
use Plugin\CustomerClassPrice4\Entity\CustomerClassPrice;
use Plugin\CustomerClassPrice4\Service\DiscountHelper;
use Symfony\Component\HttpFoundation\RequestStack;

class SetCustomerClassPrice
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var TaxRuleService
     */
    private $taxRuleService;

    /**
     * @var Context
     */
    private $context;

    /**
     * @var DiscountHelper
     */
    private $discountHelper;

    /**
     * @var RequestStack
     */
    private $requestStack;

    public function __construct(
        EntityManagerInterface $entityManager,
        TaxRuleService $taxRuleService,
        Context $context,
        DiscountHelper $discountHelper,
        RequestStack $requestStack
    )
    {
        $this->entityManager = $entityManager;
        $this->taxRuleService = $taxRuleService;
        $this->context = $context;
        $this->discountHelper = $discountHelper;
        $this->requestStack = $requestStack;
    }

    public function postLoad(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if ($entity instanceof ProductClass) {
            // 割引対象外だったらスルー
            if (!$entity->getProduct()->isPlgCcpEnabledDiscount()) {
                return;
            }

            $Customer = $this->context->getCurrentUser();
            if (!$Customer instanceof Customer) {
                return;
            }

            // 特定会員登録されているか確認
            if (!$Customer->isPlgCcpCustomerClass()) {
                return;
            }

            $customerClass = $Customer->getPlgCcpCustomerClass();

            // 割引率が設定されている特定会員だったら割引価格セット
            if ($customerClass->getDiscountRate()) {
                $entity->setPrice02IncTax(
                    $this->taxRuleService->getPriceIncTax(
                        $this->discountHelper->calculatePrice($entity->getPrice02(), $Customer),
                        $entity->getProduct(),
                        $entity
                    )
                );
            }

            $customerClassPrice = $this->entityManager->getRepository(CustomerClassPrice::class)->findOneBy([
                "customerClass" => $customerClass,
                "productClass" => $entity
            ]);

            // 特定会員価格が設定されていたら価格をセット
            if ($customerClassPrice instanceof CustomerClassPrice) {
                if ($customerClassPrice->getPrice()) {
                    $entity->setPrice02IncTax(
                        $this->taxRuleService->getPriceIncTax(
                            $customerClassPrice->getPrice(),
                            $entity->getProduct(),
                            $entity
                        )
                    );
                }
            }
        }

        // 販売価格の変更検知に引っかかるので
        // フロント処理でOrderItemテーブルから商品価格を読み込むときは毎回割引前の価格をセットする
        if ($entity instanceof OrderItem) {
            if ($this->context->isFront() && $entity->isProduct()) {
                // OrderItemテーブルに受注商品が登録されている注文手続きページの場合
                if ($entity->getId() && $this->isShopping()) {
                    $entity->setPrice($entity->getProductClass()->getPrice02());
                }
            }
        }
    }

    protected function isShopping(): bool
    {
        $request = $this->requestStack->getMasterRequest();

        if (null === $request) {
            return false;
        }

        $pathInfo = \rawurldecode($request->getPathInfo());

        return \strpos($pathInfo, '/shopping') === 0;
    }
}