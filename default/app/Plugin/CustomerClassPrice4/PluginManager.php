<?php

/*
 * Copyright (C) 2019 Akira Kurozumi <info@a-zumi.net>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

namespace Plugin\CustomerClassPrice4;

use Eccube\Plugin\AbstractPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Plugin\CustomerClassPrice4\Entity\Config;
use Eccube\Entity\Customer;
use Eccube\Entity\Csv;
use Eccube\Entity\Master\CsvType;

/**
 * Description of PluginManager
 *
 * @author Akira Kurozumi <info@a-zumi.net>
 */
class PluginManager extends AbstractPluginManager
{
    public function enable(array $meta, ContainerInterface $container)
    {
        $em = $container->get('doctrine.orm.entity_manager');

        $Config = $em->find(Config::class, 1);

        if (!$Config) {
            $RoundingType = $em
                ->getRepository('Eccube\Entity\Master\RoundingType')
                ->find(1);

            $Config = new Config();
            $Config->setRoundingType($RoundingType);
            $em->persist($Config);
            $em->flush();
        }

        $Csv = $em
                ->getRepository('Eccube\Entity\Csv')
                ->findOneBy([
                    'CsvType' => CsvType::CSV_TYPE_CUSTOMER,
                    'entity_name' => str_replace('\\', '\\\\', Customer::class),
                    'field_name' => 'plgCcpCustomerClass',
                    'reference_field_name' => 'name',
                    'disp_name' => '特定会員種別',
                ]);

        if (!$Csv) {
            $CsvType = $em->find(CsvType::class, CsvType::CSV_TYPE_CUSTOMER);

            $Csv = new Csv();
            $Csv->setCsvType($CsvType);
            $Csv->setEntityName(str_replace('\\', '\\\\', Customer::class));
            $Csv->setFieldName('plgCcpCustomerClass');
            $Csv->setReferenceFieldName('name');
            $Csv->setDispName('特定会員種別');
            $Csv->setSortNo(9999);
            $Csv->setEnabled(true);
            $em->persist($Csv);
            $em->flush();
        }
    }

    public function disable(array $meta, ContainerInterface $container)
    {
        $em = $container->get('doctrine.orm.entity_manager');

        $Csv = $em
                ->getRepository('Eccube\Entity\Csv')
                ->findOneBy([
                    'CsvType' => CsvType::CSV_TYPE_CUSTOMER,
                    'entity_name' => str_replace('\\', '\\\\', Customer::class),
                    'field_name' => 'plgCcpCustomerClass',
                    'reference_field_name' => 'name',
                    'disp_name' => '特定会員種別',
                ]);

        if ($Csv) {
            $em->remove($Csv);
            $em->flush();
        }
    }
}
