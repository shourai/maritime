<?php

/*
 * Copyright (C) 2019 Akira Kurozumi <info@a-zumi.net>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

namespace Plugin\CustomerClassPrice4\Controller\Admin;

use Eccube\Controller\AbstractController;
use Plugin\CustomerClassPrice4\Repository\CustomerClassRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Plugin\CustomerClassPrice4\Entity\CustomerClass;
use Plugin\CustomerClassPrice4\Form\Type\Admin\CustomerClassType;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;

class CustomerClassController extends AbstractController
{
    /**
     * @var CustomerClassRepository
     */
    protected $customerClassRepository;

    /**
     * ConfigController constructor.
     *
     * @param ConfigRepository $configRepository
     */
    public function __construct(CustomerClassRepository $customerClassRepository)
    {
        $this->customerClassRepository = $customerClassRepository;
    }

    /**
     * @Route("/%eccube_admin_route%/customer_class_price4/customer/class", name="plg_ccp_customer_class_admin")
     * @Template("@CustomerClassPrice4/admin/customer_class.twig")
     */
    public function index(Request $request)
    {
        $CustomerClasses = $this->customerClassRepository->findAll();

        return [
            'CustomerClasses' => $CustomerClasses,
        ];
    }

    /**
     * @Route("/%eccube_admin_route%/customer_class_price4/customer/class/new", name="plg_ccp_customer_class_new")
     * @Template("@CustomerClassPrice4/admin/customer_class_edit.twig")
     */
    public function create(Request $request)
    {
        $CustomerClass = new CustomerClass();
        $form = $this->createForm(CustomerClassType::class, $CustomerClass);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $Config = $form->getData();
            $this->entityManager->persist($CustomerClass);
            $this->entityManager->flush($CustomerClass);
            $this->addSuccess('保存しました。', 'admin');

            return $this->redirectToRoute('plg_ccp_customer_class_admin');
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/%eccube_admin_route%/customer_class_price4/customer/class/{id}/edit", requirements={"id" = "\d+"}, name="plg_ccp_customer_class_edit")
     * @Template("@CustomerClassPrice4/admin/customer_class_edit.twig")
     */
    public function edit(Request $request, CustomerClass $CustomerClass)
    {
        $form = $this->createForm(CustomerClassType::class, $CustomerClass);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $Config = $form->getData();
            $this->entityManager->persist($CustomerClass);
            $this->entityManager->flush($CustomerClass);
            $this->addSuccess('保存しました。', 'admin');

            return $this->redirectToRoute('plg_ccp_customer_class_admin');
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/%eccube_admin_route%/customer_class_price4/customer/class/{id}/delete", requirements={"id" = "\d+"}, name="plg_ccp_customer_class_delete", methods={"DELETE"})
     */
    public function delete(Request $request, CustomerClass $CustomerClass)
    {
        $this->isTokenValid();

        try {
            $this->entityManager->remove($CustomerClass);
            $this->entityManager->flush($CustomerClass);

            $this->addSuccess('会員種別を削除しました。', 'admin');
        } catch (ForeignKeyConstraintViolationException $e) {
            $message = trans('admin.common.delete_error_foreign_key', ['%name%' => $CustomerClass->getName()]);
            $this->addError($message, 'admin');
        } catch (\Exception $e) {
            $message = trans('admin.common.delete_error');
            $this->addError($message, 'admin');
        }

        return $this->redirectToRoute('plg_ccp_customer_class_admin');
    }
}
