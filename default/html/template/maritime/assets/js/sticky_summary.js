$(document).ready(function () {
    let position = 225;
    let summary = document.querySelector(".summary");
    let summaryHeight = summary.clientHeight;
    let mainContainer = document.querySelector(".shopping-main");


    $(window).scroll(function () {
        if ($(this).scrollTop() > position) {
            let pageWidth = document.querySelector(".l-main").clientWidth;

            if (pageWidth > 500) {
                let sectionShoppingHeight = document.querySelector('.l-main__middle').clientHeight;
                let contentWidth = document.querySelector(".p-shopping__layout").clientWidth;
                let margin = (pageWidth - contentWidth) / 2;


                let headerHeight = document.querySelector(".b-header").clientHeight;

                summary.classList.add('sticky')
                summary.style.right = margin + "px";
                summary.style.top = headerHeight + "px";

                mainContainer.style.maxWidth = (contentWidth - summary.clientWidth) + "px";

                let positionBottom = (headerHeight + sectionShoppingHeight) - summaryHeight;

                if ($(this).scrollTop() > (positionBottom - 60)) {
                    summary.classList.remove("sticky");
                }
            }

        } else {
            summary.classList.remove("sticky");
        }
    });
});