
import 'modaal';
import 'intersection-observer';
import { throttle } from "lodash";
const FPS = 1000 / 60;

document.addEventListener('DOMContentLoaded', function(){

    $('#js-scroll-to-top').on('click', function () {
        $('html,body').animate({
            'scrollTop': 0
        }, 500);
    });

    $('#js-nav-sp__button--open').on('click', function () {
        $('#js-drawer__overlay').toggleClass('is-active');
        $('#js-drawer__button--close').toggleClass('is-active');
        $('#js-drawer__body').toggleClass('is-active');
    });

    $('#js-drawer__overlay, #js-drawer__button--close').on('click', function () {
        $('#js-drawer__overlay').removeClass('is-active');
        $('#js-drawer__button--close').removeClass('is-active');
        $('#js-drawer__body').removeClass('is-active');
    });

    // TODO: カート展開時のアイコン変更処理
    $('#js-header__utility-cart').on('click', '#js-cart-trigger, #js-cart-cancel, #js-cart__overlay', function () {
        $('.js-cart-target').toggleClass('is-active');
    });

    // スマホのドロワーメニュー内の下層カテゴリ表示
    // TODO FIXME スマホのカテゴリ表示方法
    $('.js-category-nav-sp__heading').click(function () {
        $(this).siblings('.js-category-nav-sp__list').slideToggle(200, 'linear');
        $(this).toggleClass('is-active');
    });


    // イベント実行時のオーバーレイ処理
    // classに「load-overlay」が記述されていると画面がオーバーレイされる
    $('.js-load-overlay').on({
        click: function () {
            loadingOverlay();
        },
        change: function () {
            loadingOverlay();
        }
    });

    // submit処理についてはオーバーレイ処理を行う
    $(document).on('click', 'input[type="submit"], button[type="submit"]', function () {

        // html5 validate対応
        let valid = true;
        let form = getAncestorOfTagType(this, 'FORM');

        if (typeof form !== 'undefined' && !form.hasAttribute('novalidate')) {
            // form validation
            if (typeof form.checkValidity === 'function') {
                valid = form.checkValidity();
            }
        }

        if (valid) {
            loadingOverlay();
        }
    });

    // anchorをクリックした時にformを裏で作って指定のメソッドでリクエストを飛ばす
    // Twigには以下のように埋め込む
    // <a href="PATH" {{ csrf_token_for_anchor() }} data-method="(put/delete/postのうちいずれか)" data-confirm="xxxx" data-message="xxxx">
    //
    // オプション要素
    // data-confirm : falseを定義すると確認ダイアログを出さない。デフォルトはダイアログを出す
    // data-message : 確認ダイアログを出す際のメッセージをデフォルトから変更する
    //
    var createForm = function (action, data) {
        let $form = $('<form action="' + action + '" method="post"></form>');
        for (var input in data) {
            if (data.hasOwnProperty(input)) {
                $form.append('<input name="' + input + '" value="' + data[input] + '">');
            }
        }
        return $form;
    };

    $('a[token-for-anchor]').click(function (e) {
        e.preventDefault();
        let $this = $(this);
        let data = $this.data();
        if (data.confirm != false) {
            if (!confirm(data.message ? data.message : eccube_lang['common.delete_confirm'])) {
                return false;
            }
        }

        // 削除時はオーバーレイ処理を入れる
        loadingOverlay();

        let $form = createForm($this.attr('href'), {
            _token: $this.attr('token-for-anchor'),
            _method: data.method
        }).hide();

        $('body').append($form); // Firefox requires form to be on the page to allow submission
        $form.submit();
    });

    //ヘッダーメニュー
    $('.js-header__menu-list-item').on('click', function () {
        var target = $(this).children('.b-header__menu-icon');
        if(target.hasClass('is-open')) {
            target.removeClass('is-open');
        } else {
            $('.b-header__menu-icon').removeClass('is-open');
            target.addClass('is-open');
        }
    });

    //スマホメニュー
    $('.js-category-nav-sp__product-heading-icon').on('click', function () {
        $(this).toggleClass('is-close');
        $(this).parent().next().slideToggle();
    });

    $('#js-header__utility-search-link').modaal({
        custom_class: 'c-modaal',
        overlay_opacity: 0.5
    });

    /**
     *  アニメーション表示する画像の監視
     */
    const observerTargets = Array.prototype.slice.call(document.querySelectorAll('#js-top-app-bar, #js-top__main-visual, .js-animation-image, #js-new-item__contents'), 0);
    const observerOptions = [{
        root: null,
        rootMargin: "0px",
        threshold: 1.0
    },{
        root: null,
        rootMargin: "0px",
        threshold: [0, 1.0]
    },{
        root: null,
        rootMargin: "0px",
        threshold: 0.25
    }, {
        root: null,
        rootMargin: "0px",
        threshold: 0.5
    }];
    const topAppBarObserver = new IntersectionObserver(intersectState, observerOptions[0]);
    const mainVisualObserver = new IntersectionObserver(intersectTransparent, observerOptions[1]);
    const imageObserver = new IntersectionObserver(intersectShow, observerOptions[2]);
    const newItemObserver = new IntersectionObserver(intersectShow, observerOptions[3]);
    observerTargets.forEach(function(elem) {
        if(elem.getAttribute('id') == 'js-top-app-bar'){
            topAppBarObserver.observe(elem);
        } else if(elem.getAttribute('id') == 'js-top__main-visual'){
            mainVisualObserver.observe(elem);
        } else if(elem.classList.contains('js-animation-image') == true) {
            imageObserver.observe(elem);
        } else {
            newItemObserver.observe(elem);
        }
    });

    setTimeout(stopLoading, 3000);

},false);

window.addEventListener('load', function(){
	//$('#js-loading, #js-animation-screen, #js-animation-cross').addClass('is-loaded');
    $('#js-loading, #js-animation-cross').addClass('is-loaded');
    cartFloating();
    scrollToTopFloating();
},false);

var timer = false;
window.addEventListener("scroll", function(){
    if(timer){
        clearTimeout(timer);
    }
    timer = setTimeout(function(){
        cartFloating();
        timer = false;
    }, 175);
}, isPassiveSupport() ? {
    passive: true
}: false);

window.addEventListener('scroll', throttle(function(){
    scrollToTopFloating();
}, FPS), isPassiveSupport() ? {
    passive: true
}: false);

window.addEventListener( 'resize', function() {
}, false);

window.addEventListener("pageshow", function(){
    loadingOverlay('hide');
}, false);

/**
 *  オーバーレイ処理
 */
let loadingOverlay = function(action) {
    let $overlay;
    if (action == 'hide') {
        $('#js-overlay').remove();
    } else {
        $overlay = $('<div class="c-load-overlay" id="js-overlay"></div>');
        $('body').append($overlay);
    }
}
window.loadingOverlay = loadingOverlay;

/**
 *  要素FORMチェック
 */
function getAncestorOfTagType(elem, type) {
    while (elem.parentNode && elem.tagName !== type) {
        elem = elem.parentNode;
    }
    return (type === elem.tagName) ? elem : undefined;
}

/**
 *  top-app-barの状態
 */
function intersectState(entry) {
    let categoryElem = document.getElementById('js-header__category');
    if (entry[0].isIntersecting) {
        entry[0].target.classList.remove('is-hide');
        entry[0].target.classList.add('is-show');
    } else {
        entry[0].target.classList.remove('is-show');
        entry[0].target.classList.add('is-hide');
        if(categoryElem.classList.contains('mdc-menu-surface--open') == true) {
            window.menuElements[0].open = false;
        }
    }
}

/**
 *  headerの透過
 */
function intersectTransparent(entry) {
    let headerElem = document.getElementById('js-top-app-bar');
    if (entry[0].isIntersecting) {
        headerElem.classList.add('is-transparent');
    } else {
        headerElem.classList.remove('is-transparent');
    }
}

/**
 *  画像の表示アニメーション
 */
function intersectShow(entries) {
    const entriesArray = Array.prototype.slice.call(entries, 0);
    entriesArray.forEach(function(entry) {
        if (entry.isIntersecting) {
            entry.target.classList.add("is-show");
        }
    });
}

/**
 *  scroll to top
 */
function scrollToTopFloating() {
    let target = $('#js-scroll-to-top');
    if ($(window).scrollTop() > 500) {
        target.addClass('is-show');
        target.removeClass('is-hide');
    } else {
        target.addClass('is-hide');
        target.removeClass('is-show');
    }

}

/**
 *  ショッピングカートの追従
 */
function cartFloating() {
    if (document.getElementById("shopping-form") != null) {
        let wrap = $("#js-shopping__layout"),
            main = $("#js-shopping__layout--main"),
            sub = $("#js-shopping__layout--sub"),
            marginAdjust = parseInt($('#js-shopping__summary').css('margin-top'));
        if (window.innerWidth > 479) {
            let min_move = wrap.offset().top + marginAdjust,
                max_move = min_move + main.height() - sub.height();
            let scrollAmount = $(window).scrollTop();
            if (scrollAmount <= min_move) {
                sub.css({
                    'margin-top': 0
                });
            } else if (scrollAmount > min_move && scrollAmount < max_move) {
                sub.css({
                    'margin-top': scrollAmount - min_move
                });
            } else if (scrollAmount >= max_move) {
                sub.css({
                    'margin-top': max_move - min_move
                });
            }
        } else {
            sub.css({
                'margin-top': 0
            });
        }
    }

}

 /**
  *  loadingが完了しない場合
  */
function stopLoading(){
    $('#js-loading').addClass('is-loaded');
}

 /**
  *  Passive Event Listeners polyfill
  */
function isPassiveSupport(){
  if (typeof window === 'object' && typeof window.addEventListener === 'function' && Object.defineProperty) {
    let passive = false;
    const options = Object.defineProperty({}, 'passive', {
      get() {
          passive = true;
      }
    });
    window.addEventListener('test', null, options);
    return passive;
  }
}
