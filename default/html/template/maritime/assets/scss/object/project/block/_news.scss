.b-news {
    @include container;
    margin: 140px auto 100px;
    @include mq() {
        margin: 70px auto 40px;
    }
    &__body {
        display: flex;
        align-items: flex-start;
        @include mq() {
            flex-direction: column;
            align-items: stretch;
        }
    }
    &__title {
        font-size: 1.8em;
        white-space: nowrap;
        font-weight: $font-weight-bold;
        margin: 0 80px 0 0;
        padding: 12px 0;
        letter-spacing: 0.125em;
        flex-shrink: 0;
        @include mq(tb) {
            font-size: 1.5em;
            margin: 0 40px 0 0;
            padding: 16px 0;
        }
        @include mq() {
            text-align: center;
            margin: 0;
            padding: 0 0 16px;
            border-bottom: rgba(0, 0, 0, 0.12) solid 2px;
        }
    }
    &__contents {
        flex-grow: 1;
    }
    &__post {
        padding: 10px 0;
        border-top: rgba(0,0,0,0.12) dotted 2px;
        @include mq() {
            margin: 0;
            padding: 8px 0;
        }
        &:nth-of-type(1) {
            border-top: none;
        }
        &-heading {
            display: flex;
            align-items: center;
            justify-content: space-between;
            padding: 10px 0;
            @include mq() {
                padding: 8px 0;
            }
            &.has-link {
                cursor: pointer;
                &:hover {
                    @include mq(hover) {
                        opacity: 0.8;
                        background: rgba(0,0,0,0.06);
                    }
                }
            }
            &-contents {
                flex-grow: 1;
                display: flex;
                align-items: center;
                justify-content: space-between;
                @include mq(tb) {
                    flex-direction: column;
                    align-items: flex-start;
                    justify-content: flex-start;
                }
            }
        }
        &-date {
            align-self: flex-start;
            flex-shrink: 0;
            font-weight: $font-weight-bold;
            color: rgba($font-color-base, 0.36);
            margin: 0;
            @include mq(tb) {
                margin-bottom: 4px;
                font-size: 0.75em;
            }
        }
        &-title {
            flex-grow: 1;

            @include font(0, false);
            margin-left: 1 + $letter-spacing-base * 1;
            margin-top: 0;
            margin-bottom: 0;
            @include mq(tb) {
                margin-left: 0;
            }
        }
        &-actions {
            flex-shrink: 0;
            @include em-ml(1.5);
            height: 28px;
            display: flex;
            align-items: center;
            &-icon {
                font-size: 20px;
            }
        }
        &-contents{
            padding: 0 0 10px;
            display: none;
            color: rgba($font-color-base, 0.8);
            font-size: 0.875em;
            @include mq() {
                padding: 0 0 6px;
            }
        }
    }
    &__actions {
        @include btn-container(row, center);
        padding: 15px 0;
        border-top: rgba(0,0,0,0.12) dotted 2px;
        &-item {
            @include btn-container__item();
        }
    }
}
